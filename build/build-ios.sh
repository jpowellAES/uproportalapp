#!/bin/bash
cd "$(dirname "$0")"
read -n 1 -p "Build type (D=debug, R=Release) " ans;
case $ans in
    d|D)
        BuildType="debug";;
    r|R)
        BuildType="release";;
    *)
        exit;;
esac
echo -e "\n$BuildType";
export BuildType

read -n 1 -p "Environment (D=dev, M=demo, P=prod) " ans;
case $ans in
    d|D)
        Env="dev";;
    m|M)
        Env="demo";;
    p|P)
        Env="prod";;
    *)
        exit;;
esac
echo -e "\n$Env";

cd ../

if test -e "./platforms/ios/build/$BuildType"; then
    cd "./platforms/ios/build/$BuildType"
    if test -e "$Env-$BuildType.ipa"; then
        rm "$Env-$BuildType.ipa"
    fi
    if test -e "$Env-$BuildType.app"; then
        rm "$Env-$BuildType.app"
    fi
    cd ../../../../
fi

Package=$(node ./build/findinfile.js "widget id=\"([^\"]+)\"" config.xml)

if [ "$Env" != "prod" ]; then
    mv "config.xml" "~config.xml"
    mv "~$Env-config.xml" "config.xml"
    if [ "$Env" == "dev" ]; then
        node ./build/findandreplace.js "com.avalanchees.unitspro.dev" "$Package" config.xml
    fi
    if [ "$Env" == "demo" ]; then
        node ./build/findandreplace.js "com.unitspro.mobile.demo" "$Package" config.xml
    fi
fi

Version=$(node ./build/getconfigvar.js version)
Build=$Version.$(node ./build/getconfigvar.js iosBuild)

cd ./www

node ../build/findandreplace.js "environment: \"\"" "environment: \"$Env\"" config.js
node ../build/findandreplace.js "package: \"\"" "package: \"$Package\"" config.js
cd ../
node ./build/findandreplace.js "{VERSION}" "$Version" config.xml
node ./build/findandreplace.js "{BUILD}" "$Build" config.xml
bash ./build/cordova-build-ios.sh
node ./build/findandreplace.js "$Build" "{BUILD}" config.xml
node ./build/findandreplace.js "$Version" "{VERSION}" config.xml

if [ "$Env" != "prod" ]; then
    if [ "$Env" == "dev" ]; then
        node ./build/findandreplace.js "$Package" "com.avalanchees.unitspro.dev" config.xml
    fi
    if [ "$Env" == "demo" ]; then
        node ./build/findandreplace.js "$Package" "com.unitspro.mobile.demo" config.xml
    fi
	mv "config.xml" "~$Env-config.xml"
	mv "~config.xml" "config.xml"
fi

cd ./www
node ./../build/findandreplace.js "environment: \"$Env\"" "environment: \"\"" config.js
node ./../build/findandreplace.js "package: \"$Package\"" "package: \"\"" config.js

mkdir -p "../platforms/ios/build/$BuildType"
if test -e "../platforms/ios/build/device"; then
    cd "../platforms/ios/build/device/"
    mv "UnitsPro.ipa" "../$BuildType/$Env-$BuildType.ipa"
    mv "UnitsPro.app" "../$BuildType/$Env-$BuildType.app"
fi
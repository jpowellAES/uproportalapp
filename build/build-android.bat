@echo off
choice /C DR /N /M "Build type (D=debug, R=Release)"
IF ERRORLEVEL 1 SET BuildType=debug
IF ERRORLEVEL 2 SET BuildType=release
echo %BuildType%

choice /C DMP /N /M "Environment (D=dev, M=demo, P=prod)"
IF ERRORLEVEL 1 SET Env=dev
IF ERRORLEVEL 2 SET Env=demo
IF ERRORLEVEL 3 SET Env=prod
echo %Env%

cd ../

if exist "./platforms/android/app/build/outputs/apk/%BuildType%" (
  cd "./platforms/android/app/build/outputs/apk/%BuildType%"
  if exist "%Env%-%BuildType%.apk" (
	del "%Env%-%BuildType%.apk"
  )
  cd ../../../../../../../
)

if NOT "%Env%"=="prod" (
    ren "config.xml" "~config.xml"
    ren "~%Env%-config.xml" "config.xml"
)
for /f %%i in ('node %cd%/build/getconfigvar version') do set Version=%%i
for /f %%i in ('node %cd%/build/findinfile "widget id=""([^^""]+)""" config.xml') do set Package=%%i
cd www

node %cd%/../build/findandreplace "environment: \"\"" "environment: \"%Env%\"" config.js
node %cd%/../build/findandreplace "package: \"\"" "package: \"%Package%\"" config.js
node %cd%/../build/findandreplace "package: \"\"" "package: \"%Package%\"" config.js
cd ../
node %cd%/build/findandreplace {VERSION} %Version% config.xml

if exist ".\res\android\colors.xml" (
  del ".\res\android\colors.xml"
)
if exist ".\platforms\android\app\src\main\res\values\upro_colors.xml" (
  del ".\platforms\android\app\src\main\res\values\upro_colors.xml"
)
echo Copying resource files for env-specific values
copy ".\res\android\icon\%Env%\colors.xml" ".\res\android\colors.xml"

call "build\cordova-build-android.bat"
node %cd%/build/findandreplace %Version% {VERSION} config.xml
if NOT "%Env%"=="prod" (
    ren "config.xml" "~%Env%-config.xml"
    ren "~config.xml" "config.xml"
)



cd www
node %cd%/../build/findandreplace "environment: \"%Env%\"" "environment: \"\"" config.js
node %cd%/../build/findandreplace "package: \"%Package%\"" "package: \"\"" config.js
cd "../platforms/android/app/build/outputs/apk/%BuildType%"
ren "app-%BuildType%.apk" "%Env%-%BuildType%.apk"
pause
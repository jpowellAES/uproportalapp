var fs = require('fs');
fs.readFile(process.argv[3], 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  var regex = new RegExp(process.argv[2]);
  var matches = data.match(regex);
  if(matches){
  	if(matches[1]){
  		process.stdout.write(matches[1]);
  	}else{
  		process.stdout.write(matches[0]);
  	}
  	process.exit(1);
  }
  process.exit(0);
});
var fs = require('fs');
 var sanitizeTermForRegex = function(term){
    return term.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};
fs.readFile(process.argv[4], 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }

  var regex = new RegExp(sanitizeTermForRegex(process.argv[2]), 'g');
  var result = data.replace(regex, process.argv[3]);

  fs.writeFile(process.argv[4], result, 'utf8', function (err) {
     if (err) return console.log(err);
  });
});
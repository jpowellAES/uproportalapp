var fs = require('fs');
const path = require('path');

const colors = {
  dev: '#0041F9',
  demo: '#E00B29',
  prod: '#F97C00'
}

var stage = process.argv[2]
var env = process.argv[3]

var deleteFolderRecursive = function(path) {
  if(path.length < 10) return;
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function(file, index){
      var curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        if(fs.existsSync)fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

var copyRecursiveSync = function(src, dest) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (exists) {
    if(isDirectory){
      fs.mkdirSync(dest);
      fs.readdirSync(src).forEach(function(childItemName) {
        copyRecursiveSync(path.join(src, childItemName),
                          path.join(dest, childItemName));
      });
    } else {
      fs.linkSync(src, dest);
    }
  }
};

var replaceInFileSync = function(path, find, replace){
  var data = fs.readFileSync(path, 'utf8')
   data = data.replace(find, replace);
   fs.writeFileSync(path, data, 'utf8');
}
const fromDir = './res/android/icon/'+env;
const toDir = './platforms/android/app/src/main/res'

if(stage == 'pre'){
  fs.readdir(fromDir, (err, files) => {
    for (let file of files) {
      let fileRemove = path.join(toDir, file)
      if(fs.existsSync(fileRemove)){
        deleteFolderRecursive(fileRemove)
      }
      let fileCopy = path.join(fromDir, file)
      if(fs.statSync(fileCopy).isDirectory()){
        copyRecursiveSync(fileCopy, fileRemove)
      }
    }
  })
  replaceInFileSync(toDir+'/values/ic_launcher_background.xml', '{COLOR}', colors[env])
}
if(stage == 'post'){
  replaceInFileSync(toDir+'/values/ic_launcher_background.xml', colors[env], '{COLOR}')
}

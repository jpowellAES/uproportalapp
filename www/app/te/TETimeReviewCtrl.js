(function(){
    'use strict';

    angular.module('app').controller('TETimeReviewController', ['$scope', '$timeout', 'ApiService', 'StorageService', '$rootScope', 'Utils', function($scope, $timeout, ApiService, StorageService, $rootScope, Utils) {

        $scope.loaded = false;
        $scope.working = true;
        $scope.loading = false;
        $scope.module = $scope.$parent.current.module;
        $scope.title = $scope.module.title;
        $scope.jobs = [];
        $scope.infoDialog = {};
        var dialogs = {};
        ons.createDialog('app/dialog/job-info-dialog.html').then(function(dialog){
            dialog._scope.infoDialog = $scope.infoDialog;
            dialog._scope.OrgType = $scope.OrgType;
            $scope.infoDialog.dialog = dialog;
        });

        $scope.loadJobs = function($done){
            var obj = {
                types: 'TimeReview',
                module: $scope.module.key
            };
            ApiService.get('jobs', obj).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var cleanJobs = function(jobs){
            var statuses = ['RJ','RV','SU'];
            var jobStatuses = {}, date = null;
            jobs = Utils.enforceArray(jobs);
            for(var i=0; i<jobs.length; i++){
                var newIndex;
                var job = jobs[i];
                if(job.Date && typeof job.Date == 'string')
                    job.Date = new Date(job.Date);
                if(job.RecordTypeCode == 'JOB'){
                    jobStatuses[job.JobID] = -1;
                }else if(job.RecordTypeCode == 'JD'){
                    date = job.Date;
                    var jdc = Object.assign({},job);
                    jdc.RecordTypeCode = 'JDC';
                    job.StatusCode = '';
                    jobs.splice(i+1,0,jdc);
                }else if(job.RecordTypeCode == 'JDC'){
                    job.Date = date;
                    jobStatuses[job.JobID] = Math.max(statuses.indexOf(job.StatusCode), jobStatuses[job.JobID])
                }
            };
            jobs.forEach(function(job){
                if(job.RecordTypeCode == 'JOB'){
                    if(jobStatuses[job.JobID] > -1){
                        job.StatusCode = statuses[jobStatuses[job.JobID]];
                    }else{
                        job.StatusCode = '';
                    }
                }
            });
            return jobs;
        }
        var recalcJobStatuses = function(){
            var statuses = ['RJ','RV','SU'];
            var jobStatuses = {}, counts = {};
            for(var i=0; i<$scope.jobs.length; i++){
                var newIndex;
                var job = $scope.jobs[i];
                if(job.RecordTypeCode == 'JOB'){
                    jobStatuses[job.JobID] = -1;
                    counts[job.JobID] = 0;
                }else if(job.RecordTypeCode == 'JDC'){
                    jobStatuses[job.JobID] = Math.max(statuses.indexOf(job.StatusCode), jobStatuses[job.JobID]);
                    if(['SU','RV'].includes(job.StatusCode)){
                        counts[job.JobID]++;
                    }
                }
            };
            $scope.jobs.forEach(function(job){
                if(job.RecordTypeCode == 'JOB'){
                    if(jobStatuses[job.JobID] > -1){
                        job.StatusCode = statuses[jobStatuses[job.JobID]];
                    }else{
                        job.StatusCode = '';
                    }
                    job.JobDayCount = counts[job.JobID];
                }
            });
        }

        var result = function(resp){
            if(resp && resp.User){
                $scope.jobs = cleanJobs(resp.User.TimeReview);
            }
            $scope.working = false;
        }

        var getPrompt = function(){
            if($scope.OrgType == 'OW')
                return 'Job #';
            return 'WG Job #';
        }
        $scope.getOrg = function(job){
            if($scope.OrgType == 'OW')
                return job.Workgroup;
            return job.Owner;
        };
        $scope.getOtherJobNumber = function(job){
            if(job.WorkgroupJobNumber == job.JobNumber)
                return '';
            if($scope.OrgType == 'OW')
                return ' - '+job.WorkgroupJobNumber;
            return ' - '+job.JobNumber;
        }
        $scope.getJobNumber = function(job){
            if($scope.OrgType == 'OW')
                return job.JobNumber;
            return job.WorkgroupJobNumber;
        }
        $scope.showJobRow = function(job){
            if(job.RecordTypeCode == 'JDC' || job.RecordTypeCode == 'JD'){
                return job.visible;
            }
            return true;
        };

        $scope.getClass = function(job){
            var cls = 'time-review-row';
            if(job.RecordTypeCode != 'JOB')
                cls = ' te-job-day-row';
            if(job.RecordTypeCode == 'JD'){
                cls += ' time-review-day';
            }
            switch(job.StatusCode){
                case 'AP':
                    cls += ' green';
                break;
                case 'RJ':
                    cls += ' red';
                break;
                case 'SU':
                    cls += ' blue';
                break;
                case 'RV':
                    cls += ' dark-red';
                break;
            }

            return cls;
        }

        $scope.openJobDay = function(jobday){
            if(jobday.RecordTypeCode == 'JOB'){
                var jobId = jobday.JobID;
                for(var i=0; i<$scope.jobs.length; i++){
                    if($scope.jobs[i].RecordTypeCode != 'JOB' && $scope.jobs[i].JobID == jobId){
                        $scope.jobs[i].visible = !$scope.jobs[i].visible;
                    }
                }
            }
        }

        var loadCompletionJob = function(){
            $scope.loading = true;
            var obj = {design: $scope.current.job.ActiveJobDesignID};
            ApiService.get('jobCompletion', obj).then(completionJobResult, completionJobResult)
        }
        var completionJobResult = function(resp){
            $scope.loading = false;
            if(resp && resp.JobDesign){
                $scope.current.job.design = cleanCompletionResponse(resp.JobDesign);
                $scope.current.job.design.lastLocationFetch = new Date().getTime();
                $scope.current.job.design.process = {name:'EndDay', date:$scope.current.jobday.Date, crew: $scope.current.jobday.CrewID};
                mainNav.pushPage('complete-work-job.html');
            }else{
                if(resp && resp.locked && resp.message){
                    ons.notification.confirm(resp.message+" Go directly to End of Day?", {
                        title: "Job Locked",
                        buttonLabels: ["No","Yes"],
                        callback: function(buttonIndex){
                            if(buttonIndex == 1){
                                mainNav.pushPage('te-end-day.html');
                            }
                        }
                    });
                }else{
                    ons.notification.alert("Failed to load job data", {title: "Error"});
                }
            }
        };
        var cleanCompletionResponse = function(design){
            design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
            if(!design.Location)
                design.Location = [];
            if(!Array.isArray(design.Location))
                design.Location = [design.Location];
            design.Location.map(cleanCompletionLocation);
            design.PercentComplete = Utils.fixPercent(design.PercentComplete);
            return design;
        };
        var cleanCompletionLocation = function(location){
            location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
            location.PercentComplete = Utils.fixPercent(location.PercentComplete);
            return location;
        };
        $scope.openJobDayCrew = function(jobday, $event){
            $scope.current.jobday = jobday;
            $scope.current.jobday.CrewID = jobday.JobDayCrewID;
            $scope.current.JobDayCrewIDs = $scope.jobs.reduce(function(ids, job){
                if(job.RecordTypeCode == 'JDC')
                    ids.push(job.JobDayCrewID)
                return ids;
            }, []);
            var jd = $scope.jobs.find(function(jd){
                return jd.JobDayID == jobday.JobDayID && jd.RecordTypeCode == 'JD';
            });
            $scope.current.job = $scope.jobs.find(function(j){
                return j.JobID == jobday.JobID && j.RecordTypeCode == 'JOB';
            });
            $scope.current.jobday.Date = new Date($scope.current.jobday.JobDateInvisible);
            if(jobday.StatusCode == 'PE' || jobday.StatusCode == 'BIP'){
                mainNav.pushPage('te-begin-day.html');
                return;
            }
            if(jobday.UseDesignFlag && jobday.StatusCode == 'EPE' && !jobday.BypassCompleteWorkFlag){
                loadCompletionJob();
            }else{
                mainNav.pushPage('te-end-day.html');
            }
            if($event)$event.stopPropagation();
        }

        $scope.showPage = function(){
            recalcJobStatuses();
            if($scope.current.nextAction &&  $scope.current.nextAction.name == 'nextJDC'){
                var jdc = $scope.jobs.find(function(j){
                    return j.JobDayCrewID == $scope.current.nextAction.jdc && j.RecordTypeCode == 'JDC';
                });
                if(jdc){
                    $scope.openJobDayCrew(jdc);
                }
                delete $scope.current.nextAction;
            }
        }

        $scope.viewReports = function(jobday){
            $scope.loading = true;
            var obj = {
                action: 'reports',
                view: 'TimeReview',
                crew: jobday.JobDayCrewID
            }
            ApiService.get('teDayAction', obj).then(viewReportsResult,viewReportsResult);
        }
        var viewReportsResult = function(resp){
            if(resp && resp.Reports){
                if(resp.Reports.length){
                    dialogs.reports.openDialog(resp.Reports);
                }else{
                    Utils.notification.alert('No reports available for this item.');
                }
            }
            $scope.loading = false;
        }

        var removeJobDayCrew = function(jdc){
            $scope.loading = true;
            var obj = {
                action: 'delete',
                crew: jdc.JobDayCrewID
            }
            ApiService.post('teDayAction', obj)
            .then(function(resp){removeJobDayCrewResult(jdc)})
            .finally(function(){ $scope.loading = false; });
        }
        var removeJobDayCrewResult = function(jdc){
            var index = $scope.jobs.findIndex(function(j){ return j.JobDayCrewID == jdc.JobDayCrewID && j.RecordTypeCode == 'JDC' });
            var removeDay = ($scope.jobs[index-1].RecordTypeCode=='JD')&&(index==$scope.jobs.length-1||$scope.jobs[index+1].RecordTypeCode!='JDC');
            var removeJob = ($scope.jobs[index-2].RecordTypeCode=='JOB')&&(index==$scope.jobs.length-1||$scope.jobs[index+1].RecordTypeCode == 'JOB');
            $scope.jobs.splice(index, 1);
            --index;
            if(removeDay){
                $scope.jobs.splice(index, 1);
                --index;
                if(removeJob){
                    $scope.jobs.splice(index, 1);
                    --index;
                }
            }
            recalcJobStatuses();
        }

        $scope.jobOnLongPress= function(job){
            if(job.RecordTypeCode == 'JOB'){
                navigator.vibrate(10);
                $scope.infoDialog.job = job;
                $scope.infoDialog.title = getPrompt()+': '+$scope.getJobNumber(job);
                $rootScope.$emit('JobInfoDialog', {type:'init', job: job, joblist:'TimeReview', endpoint: 'teJob', reportField: 'TimeReviewJobReportsButtonText'});
                Utils.openDialog($scope.infoDialog.dialog);
            }else if(job.RecordTypeCode == 'JDC' && job.StatusCode == 'PE'){
                navigator.vibrate(10);
                Utils.notification.confirm('Remove <b>'+Utils.date(job.Date,'M/d/yyyy')+'</b> for <b>'+job.Foreman+'</b>?', {title: $scope.getJobNumber(job)}).then(function(button){
                    if(button){
                        removeJobDayCrew(job);
                    }
                });
            }
        };

        ons.createDialog('app/dialog/reports/reports-dialog.html').then(function(dialog){
            dialog.openDialog = function(reports){
              dialog._scope.reports = reports;
              dialog._scope.openingDialog = false;
              Utils.openDialog(dialog);
            };
            dialogs.reports = dialog;
       });

        var saveStateFields = ['working', 'jobs'];
        $scope = StorageService.state.load('TEDayJobsController', $scope, saveStateFields);
        $scope.$on('$destroy', function() {
            StorageService.state.clear('TEDayJobsController');
            if($scope.infoDialog.dialog.visible){
                $scope.infoDialog.dialog.hide();
            }
            $rootScope.$emit('JobInfoDialog', {type:'destroy'});
        });
        if(!$scope.jobs.length)
            $scope.loadJobs();

    }]);

})();
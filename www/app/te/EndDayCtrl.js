(function(){
    'use strict';

    angular.module('app').controller('EndDayController', ['$scope', 'CommentDialog', 'ApiService', 'Utils', '$rootScope', '$timeout', '$filter', 'Rentals', function($scope, CommentDialog, ApiService, Utils, $rootScope, $timeout, $filter, Rentals) {
        $scope.job = $scope.$parent.current.job;
        $scope.job.changed = false;
        var needRefreshOnNav = false;
        $scope.jobday = $scope.$parent.current.jobday;
        $scope.jobdaycrew = {};
        $scope.module = $scope.$parent.current.module;
        $scope.working = false;
        $scope.loading = false;
        $scope.hasReports = false;
        var steps = ['AT','PC','AC','AE','RS'];
        var data = {}, dialogs = {};
       

      var getDayCrewObj = function(){
        return {job: $scope.job.JobID, crew: $scope.jobday.CrewID||$scope.jobday.JobDayCrewID, date: dateTimeValue($scope.jobday.Date, 'M/d/yyyy')};
      }
      var openEndDay = function(){
        $scope.working = true;
        var obj = getDayCrewObj();
        ApiService.post('teEndDay', obj).then(openEndDayResult,openEndDayResult);
      };

      var openEndDayResult = function(resp, skipCrew, skipNav){
        $scope.working = false;
        if(resp && resp.Job && resp.Job.JobDayCrew){
          if(resp.Reports){
            resp.Reports = Array.isArray(resp.Reports)?resp.Reports:[resp.Reports];
            dialogs.reports.setReports(resp.Reports);
            $scope.hasReports = resp.Reports.length > 0;
          }
          if(!skipNav)$scope.canNav.navToStep(resp.Job.PositionStep);
          $scope.job.TypeCode = resp.Job.TypeCode;
          $scope.job.UseDesignFlag = resp.Job.UseDesignFlag;
          $scope.job.ContractDepartmentCode = resp.Job.ContractDepartmentCode;
          $scope.job.ContractAllowDoubleTimeFlag = resp.Job.ContractAllowDoubleTimeFlag;
          $scope.jobday.Status = resp.Job.JobDayCrew.StatusMeaning;
          $scope.jobday.StatusCode = resp.Job.JobDayCrew.StatusCode;
          $scope.jobday.DefaultStartHour = resp.Job.DefaultStartHourCode;
          $scope.jobday.DefaultStartMinute = resp.Job.DefaultStartMinuteCode;
          $scope.jobday.JobTypeCode = resp.Job.TypeCode;
          if(!skipCrew){
            cleanJob(resp.Job);
          }
          if($scope.jobdaycrew.Task.length == 1 && !$scope.jobdaycrew.Task[0].Duration){
            delete $scope.jobdaycrew.Task[0].Duration;
            delete $scope.jobdaycrew.Task[0].EndTime;
            openNewTaskDialog($scope.jobdaycrew.Task[0], true);
          }
        }else{
          Utils.notification.alert("Failed to load job day", {title: "Error"});
          mainNav.popPage();
        }
      };

      var updateJobTaskIDs = function(task){
        $scope.jobdaycrew.Task.forEach(function(t){
          if(task.JobTaskID && t.JobTaskID == task.TempJobTaskID){
            t.JobTaskID = task.JobTaskID;
          }
          if(task.TempJobTaskID && t.JobDayCrewTaskID == task.TempJobDayCrewTaskID){
            t.JobDayCrewTaskID = task.JobDayCrewTaskID;
          }
        });
        if(!task.JobTaskID)
          return;
        if(dialogs.pickTask._scope.data){
          if(dialogs.pickTask._scope.data.UncompleteTasks){
            dialogs.pickTask._scope.data.UncompleteTasks.forEach(function(t){
              if(t.JobTaskID == task.TempJobTaskID){
                t.JobTaskID = task.JobTaskID;
              }
            });
          }
          if(dialogs.pickTask._scope.data.WGPhaseCodeSection){
            dialogs.pickTask._scope.data.WGPhaseCodeSection.forEach(function(t){
              if(t.JobTaskID == task.TempJobTaskID){
                t.JobTaskID = task.JobTaskID;
              }
            });
          }
          if(dialogs.pickTask._scope.data.CompleteTasks){
            dialogs.pickTask._scope.data.CompleteTasks.forEach(function(t){
              if(t.JobTaskID == task.TempJobTaskID){
                t.JobTaskID = task.JobTaskID;
              }
            });
          }
        }
      }
      var cleanTasks = function(tasks){
        if(!Array.isArray(tasks))
          tasks = tasks?[tasks]:[];
        tasks.forEach(function(task, i){
          task.StartTime = new Date(task.StartDate+' '+task.StartHour1Meaning+':'+task.StartMinute1Meaning);
          task.EndTime = new Date(task.EndDate+' '+task.EndHourMeaning+':'+task.EndMinuteMeaning);
          task.index = i;
          if(!Array.isArray(task.PhaseCode))
            task.PhaseCode = task.PhaseCode?[task.PhaseCode]:[];
          if(!task.JobDayCrewTaskID)
            task.isNew = true;
          if($scope.jobdaycrew.Task){
            if(task.MasterFlag && (task.TempJobTaskID || task.TempJobDayCrewTaskID))
              updateJobTaskIDs(task);
          }
        });

        return tasks;
      };
      var cleanJob = function(job){
        $scope.jobdaycrew = cleanJobDayCrew(job.JobDayCrew);
        $scope.jobdaycrew.CurrentStep = job.CurrentStep;
        $scope.jobdaycrew.HideApprove = job.HideApprove===true?true:false;
        $scope.jobdaycrew.HideReject = job.HideReject===true?true:false;
        $scope.jobdaycrew.HideReviewed = job.HideReviewed===true?true:false;
        $scope.jobdaycrew.showComments = $scope.jobdaycrew.LastComment && $scope.jobdaycrew.LastComment.Comments && ($scope.jobdaycrew.StatusCode == 'RJ'||$scope.jobdaycrew.StatusCode == 'SU');
        $scope.jobdaycrew.canResubmit = $scope.jobdaycrew.StatusCode == 'RJ'&&(job.IsGeneralForeman||job.IsForeman||job.IsProjectManager)
        $scope.jobdaycrew.canReopen = $scope.jobdaycrew.StatusCode == 'SU'&&(job.IsGeneralForeman||job.IsForeman||job.IsProjectManager) && $scope.jobdaycrew.HideReject;
        console.log($scope.jobdaycrew.StatusCode,job.IsGeneralForeman||job.IsForeman||job.IsProjectManager,$scope.jobdaycrew.HideReject)
        $scope.jobdaycrew.showPhasecodeSummary = job.IsProjectManager||job.IsGeneralForeman;
        $scope.jobdaycrew.showSummaryButtons = !$scope.jobdaycrew.HideApprove||!$scope.jobdaycrew.HideReject||!$scope.jobdaycrew.HideReviewed;
      }
      var cleanJobDayCrew = function(jdc){
        if(!Array.isArray(jdc.JobDayCrewMember))
          jdc.JobDayCrewMember = jdc.JobDayCrewMember?[jdc.JobDayCrewMember]:[];
        if(!Array.isArray(jdc.JobDayCrewEquipment))
          jdc.JobDayCrewEquipment = jdc.JobDayCrewEquipment?[jdc.JobDayCrewEquipment]:[];
        if(!Array.isArray(jdc.NextJob))
          jdc.NextJob = jdc.NextJob?[jdc.NextJob]:[];
        if(!Array.isArray(jdc.JobTask))
          jdc.JobTask = jdc.JobTask?[jdc.JobTask]:[];
        jdc.JobDayCrewEquipment.map(cleanEquipment);
        jdc.Task = cleanTasks(jdc.Task);
        return jdc;
      };

      var dateTimeValue = function(date, filter){
        return $filter('date')(date, filter)
      }

      $scope.canNav = {
        left: false, 
        right: true, 
        screen: 0,
        pages: {TASKS:0,PHASECODES:1,MEMBERS:2,EQUIPMENT:3,SUMMARY:4},
        navToStep: function(step){
          var index = steps.indexOf(step);
          if(index > -1){
            $scope.canNav.screen  = 0;
            navStep(index);
          }
        }
      };
      var navStep = function(increment){
        $scope.canNav.screen += increment;
        if($scope.canNav.screen == $scope.canNav.pages.PHASECODES && ($scope.job.UseDesignFlag && !$scope.jobdaycrew.showPhasecodeSummary)){
          $scope.canNav.screen += increment;
        }
        $scope.canNav.left = $scope.canNav.screen > 0;
        $scope.canNav.right = $scope.canNav.screen < steps.length-1;
      }
      var navStepPost = function(increment, resultCallback){
        var obj = getDayCrewObj();
        obj.step = increment;
        switch($scope.canNav.screen){
          case 0: // tasks
          case 1: // phase codes
            obj.action = 'tasks';
            if($scope.jobdaycrew.allocationChanged){
              obj.allocationWhich = $scope.jobdaycrew.allocationWhich;
              obj.STAllocation = $scope.jobdaycrew.STHours;
              obj.OTAllocation = $scope.jobdaycrew.OTHours;
              if($scope.job.ContractAllowDoubleTimeFlag)
                obj.DTAllocation = $scope.jobdaycrew.DTHours;
            }
            obj.Task = getTaskData();
            if($scope.canNav.screen == 1 && $scope.job.UseDesignFlag && !$scope.phaseCodesChanged){
              obj.IgnorePhaseCodes = true;
            }
            var changedCompletion = getCompletionChangedJobTasks(true);
            if(Object.keys(changedCompletion).length){
              console.log(changedCompletion)
              obj.TaskCompletionChanged = Object.keys(changedCompletion).map(function(key){return key+','+(changedCompletion[key]?'Y':'N')}).join('¶');
            }
            ApiService.post('teEndAction', obj).then(resultCallback);
          break;
          case 2:
            obj.action = 'members';
            obj.Member = getMemberData();
            ApiService.post('teEndAction', obj).then(resultCallback);
          break;
          case 3:
            obj.action = 'equips';
            obj.Equipment = getEquipmentData();
            ApiService.post('teEndAction', obj).then(resultCallback);
          break;
        }
      };
      $scope.navStep = function(increment){
        if(needRefreshOnNav || $scope.job.changed || (increment>0&&['AT','AC','AE'].indexOf($scope.jobdaycrew.CurrentStep)==$scope.canNav.screen)){
          $scope.loading = true;
          navStepPost(increment, function(resp){
            navStepResult(resp, increment);
          });
        }else{
          navStep(increment);
        }
      }
      var navStepResult = function(resp, increment){
        $scope.loading = false;
        $scope.job.changed = false;
        $scope.phaseCodesChanged = false;
        $scope.jobdaycrew.allocationChanged = false;
        needRefreshOnNav = false;
        if(resp){
          switch($scope.canNav.screen){
            case 0:
            case 1:
            case 2:
            case 3:
              if(resp.Job){
                cleanJob(resp.Job);
              }
            break;
          }
          navStep(increment);
          $scope.jobdaycrew.CurrentStep = resp.Job.CurrentStep;
        }
      }

      /*   TASKS   */
      var getTaskDataOne = function(t){
        var task = {StartDate: dateTimeValue(t.StartTime,'M/d/yyyy'), StartHour: dateTimeValue(t.StartTime,'H'), StartMinute: dateTimeValue(t.StartTime,'m'), Duration: t.Duration, Complete: t.Complete, Comment: t.Comment };
        if(t.isNew){
          if(t.SectionID){
            task.SectionID = t.SectionID; //WG tasks
            task.SectionTypeCode = t.SectionTypeCode;
          }
          if(t.JobTaskID<=0 && !t.SectionID){
            task.JobTaskTaskNumber = t.JobTaskTaskNumber;
            task.JobTaskDescription = t.JobTaskDescription;
          }
        }
        task.JobDayCrewTaskID = t.JobDayCrewTaskID;
        task.JobTaskID = t.JobTaskID;
        task.PhaseCode = t.PhaseCode;
        task.MasterFlag = t.MasterFlag;
        return task;
      }
      var getTaskData = function(){
        return $scope.jobdaycrew.Task.map(getTaskDataOne);
      };

      var getDefaultStartTime = function(){
        var date = new Date();
        date.setSeconds(0);
        date.setMilliseconds(0);
        console.log()
        if($scope.jobday.DefaultStartHour){
          date.setHours($scope.jobday.DefaultStartHour);
          date.setMinutes($scope.jobday.DefaultStartMinute||0);
          return date;
        }
        date.setHours(7);
        date.setMinutes(0);
        return date;
      }

      var generateNewTempJobTaskId = function(task){
        var lowest = $scope.jobdaycrew.Task.reduce(function(lowest, item){
          return Math.min(lowest, item.JobTaskID||0);
        }, 0);
        task.JobTaskID = lowest - 1;
        task.MasterFlag = true;
      };
      var generateNewTempTaskId = function(task){
        var lowest = $scope.jobdaycrew.Task.reduce(function(lowest, item){
          return Math.min(lowest, item.JobDayCrewTaskID||0);
        }, 0);
        task.JobDayCrewTaskID = lowest - 1;
      };
      $scope.addTask = function(){
        //TODO: require valid last task before adding
        var index = $scope.jobdaycrew.Task.length;
        var StartTime = index ? new Date($scope.jobdaycrew.Task[index-1].EndTime.getTime()) : getDefaultStartTime();
        var newTask = {index: index, StartTime: StartTime, isNew: true, Comment:''};
        $scope.jobdaycrew.Task.push(newTask);
        openNewTaskDialog(newTask);
      };
      var openNewTaskDialog = function(newTask, defaulted){
        var index = $scope.jobdaycrew.Task.length-1;
        generateNewTempTaskId(newTask);
        dialogs.task.openDialog(function(ok, task){
          if(!ok){
            $scope.jobdaycrew.Task.pop();
          }else{
            $scope.jobdaycrew.Task[index] = Object.assign(newTask, task);
            updateCompleteFlags($scope.jobdaycrew.Task[index]);
            updateTotalTime();
            $scope.job.changed = true;
          }
          return true;
        }, newTask, index, {defaultedTask: defaulted});
      }
      var checkForPhaseCodes = function(task){
        return !$scope.job.UseDesignFlag||task.JobTaskSystemCode!='UN';
      }
      $scope.removeTask = function(){
        var index = $scope.jobdaycrew.Task.length-1;
        var task = $scope.jobdaycrew.Task[index];
        $scope.jobdaycrew.Task.pop();
        $scope.job.changed = true;
        updateTotalTime();
      };

      var HOURS = 60*60*1000;
      var updateTaskTime = function(whichTime, task, time){
        task[whichTime].setTime(time.getTime());
        if(whichTime == 'StartTime' && task.Duration){
          task.EndTime = new Date(time.getTime() + task.Duration*HOURS);
        }
        if(whichTime == 'EndTime'){
          task.Duration = Math.round(((task.EndTime.getTime()-task.StartTime.getTime())/HOURS)*20)/20;
        }
        $scope.job.changed = true;
      };
      var isJobTaskNonComp = function(jobTaskId){
        if($scope.jobdaycrew.JobTask && $scope.jobdaycrew.JobTask.length){
          var jobTask = $scope.jobdaycrew.JobTask.find(function(jt){
            return jt.ID == jobTaskId;
          });
          if(jobTask){
            return jobTask.NonCompensatedFlag;
          }
        }
        if(dialogs.pickTask._scope.data && dialogs.pickTask._scope.data.WGPhaseCodeSection){
          var jobTask = dialogs.pickTask._scope.data.WGPhaseCodeSection.find(function(jt){
            return jt.JobTaskID == jobTaskId;
          });
          if(jobTask){
            return jobTask.TypeCode == 'NC';
          }
        }
        return false;
      }
      var updateTotalTime = function(){
        var grandTotal = 0, nonCompTotal = 0, compTotal = 0, st = 0, ot = 0, dt = 0, tempJobTasks = {};
        for(var i=0; i<$scope.jobdaycrew.Task.length; i++){
          var task = $scope.jobdaycrew.Task[i];
          grandTotal += task.Duration;
          if(typeof tempJobTasks[task.JobTaskID] == 'undefined'){
            tempJobTasks[task.JobTaskID] = isJobTaskNonComp(task.JobTaskID);
          }
          if(tempJobTasks[task.JobTaskID]){
            nonCompTotal += task.Duration;
          }else{
            compTotal += task.Duration;
            for(var j=0; j<task.PhaseCode.length; j++){
              st += task.PhaseCode[j].STHours;
              ot += task.PhaseCode[j].OTHours;
              dt += task.PhaseCode[j].DTHours;
            }
          }
        }
        $scope.jobdaycrew.GrandTotalHours = grandTotal;
        $scope.jobdaycrew.NonCompHours = nonCompTotal;
        $scope.jobdaycrew.TotalHours = compTotal;
        $scope.jobdaycrew.STHours = st;
        $scope.jobdaycrew.OTHours = ot;
        $scope.jobdaycrew.DTHours = dt;
      };
      $scope.changeTime = function(which, task, $index){
        if($scope.loading || ($scope.jobdaycrew.StatusCode != 'EPE' && $scope.jobdaycrew.StatusCode != 'RJ'))
          return;
        if((which=='StartTime'&&$scope.jobdaycrew.Task.length!=1)||(which=='EndTime'&&$index!=$scope.jobdaycrew.Task.length-1))
          return;
        dialogs.timePicker.openDialog(function(ok,time,timeDialog){
          if(!ok) return true;
          if(which == 'EndTime' && time <= task.StartTime){
            Utils.notification.alert({title: 'Error', message: 'End time must be later that Start time.'});
            return false;
          }
          var tempTask = Utils.copy(task);
          if(checkForPhaseCodes(task)){
            updateTaskTime(which, tempTask, time);
            dialogs.task.openDialog(function(ok, t){
              if(ok){
                task.PhaseCode = t.PhaseCode;
                updateTaskTime(which, task, time);
                updateTotalTime();
                $scope.job.changed = true;
              }
              return true;
            }, tempTask, $index, {phaseCodes:true});
          }else{
            updateTaskTime(which, tempTask, time);
            timeDialog._scope.loading = true;
            dialogs.task.updatePhaseCodes(tempTask, function(t){
              timeDialog.hide();
              task.PhaseCode = t.PhaseCode;
              updateTaskTime(which, task, time);
              updateTotalTime();
              $scope.job.changed = true;
            });
          }
          return true;
        }, which=='StartTime'?'Start':'End', task[which]);
      };

      $scope.changeDuration = function(task, $index){
        if($scope.loading || ($scope.jobdaycrew.StatusCode != 'EPE' && $scope.jobdaycrew.StatusCode != 'RJ'))
          return;
        if($index!=$scope.jobdaycrew.Task.length-1)
          return;
        dialogs.duration.openDialog(function(ok, duration, durationDialog){
          if(!ok) return true;
          if(duration <= 0){
            Utils.notification.alert({title: 'Error', message: 'Duration must be greater than 0.'});
            return false;
          }
          var tempTask = Utils.copy(task);
          if(checkForPhaseCodes(task)){
            tempTask.Duration = duration;
            tempTask.EndTime.setTime(tempTask.StartTime.getTime()+duration*HOURS);
            dialogs.task.openDialog(function(ok, t){
              if(ok){
                task.PhaseCode = t.PhaseCode;
                task.Duration = duration;
                task.EndTime.setTime(task.StartTime.getTime()+duration*HOURS);
                updateTotalTime();
              }
              return true;
            }, tempTask, $index, {phaseCodes:true});
          }else{
            tempTask.Duration = duration;
            tempTask.EndTime.setTime(tempTask.StartTime.getTime()+duration*HOURS);
            durationDialog._scope.loading = true;
            dialogs.task.updatePhaseCodes(tempTask, function(t){
              durationDialog.hide();
              task.PhaseCode = t.PhaseCode;
              task.Duration = duration;
              task.EndTime.setTime(task.StartTime.getTime()+duration*HOURS);
              updateTotalTime();
              $scope.job.changed = true;
            });
          }
          return true;
        }, task.Duration);
      };

      var updateCompleteFlags = function(task){
        $scope.jobdaycrew.Task.forEach(function(t){
          if(t.JobTaskID == task.JobTaskID && t.Complete != task.Complete){
            t.Complete = task.Complete;
          }
        });
        if(dialogs.pickTask._scope.data){
          var jobtask;
          if(task.Complete){
            jobtask = dialogs.pickTask._scope.data.UncompleteTasks.findIndex(function(t){
              return t.JobTaskID == task.JobTaskID;
            });
            if(jobtask > -1){
              var tsk = dialogs.pickTask._scope.data.UncompleteTasks.splice(jobtask,1)[0];
              if(tsk){
                tsk.CompleteChanged = !tsk.CompleteChanged;
                dialogs.pickTask._scope.data.CompleteTasks.push(tsk);
              }
            }
          }else{
            jobtask = dialogs.pickTask._scope.data.CompleteTasks.findIndex(function(t){
              return t.JobTaskID == task.JobTaskID;
            });
            if(jobtask > -1)
              var tsk = dialogs.pickTask._scope.data.CompleteTasks.splice(jobtask,1)[0];
              if(tsk){
                tsk.CompleteChanged = !tsk.CompleteChanged;
                dialogs.pickTask._scope.data.UncompleteTasks.push(tsk);
              }
          }
        }
      };
      $scope.editTask = function(task, $index){
        task.index = $index;
        dialogs.task.openDialog(function(ok, editTask){
          Object.assign(task, editTask);
          updateCompleteFlags(task);
          updateTotalTime();
          $scope.job.changed = true;
          return true;
        }, task, $index);
      }

      /* PHASE CODES */
      $scope.getPhaseCodeScreen = function(tasks){
        if(!tasks) return;
        var taskCount = tasks.length, phases = [];
        for(var i=0; i<taskCount; i++){
          var task = tasks[i];
          phases.push(task);
          task.recordType = 'TASK';
          task.totals = {ST:0,OT:0,DT:0};
          task.pcCount = 0;
          for(var j=0; j<task.PhaseCode.length; j++){
            var pc = task.PhaseCode[j];
            phases.push(pc);
            pc.recordType = 'PC';
            pc.taskIndex = task.index;
            pc.index = j;
            task.totals.ST += pc.STHours;
            task.totals.OT += pc.OTHours;
            task.totals.DT += pc.DTHours;
            task.pcCount++;
          }
        }
        var task;
        for(i=0; i<phases.length; i++){
          if(phases[i].recordType == 'TASK'){
            task = phases[i];
          }else{
            phases[i].PCClassST = (task.totals.ST==0||task.pcCount<2)?'te-task-change-button no-edit':'te-task-change-button';
            phases[i].PCClassOT = (task.totals.OT==0||task.pcCount<2)?'te-task-change-button no-edit':'te-task-change-button';
            phases[i].PCClassDT = (task.totals.DT==0||task.pcCount<2)?'te-task-change-button no-edit':'te-task-change-button';
          }
        }
        return phases;
      }
      var getPhaseCodeEntryOrder = function(which, pcIndex, phaseCodes){
        var top = 0, sequence = phaseCodes.length, sort = [];
        for(var i=0; i<phaseCodes.length; i++){
          var order = phaseCodes[i]['EntryOrder'+which];
          if(order > top)
            top = order
          if(i == pcIndex){
            order = 999999999
            sequence -= phaseCodes.length;
          }else if(typeof order == 'undefined'){
            order = 0;
          }
          sort.push({
            order: order,
            sequence: sequence,
            id: phaseCodes[i].ID
          });
          ++sequence;
        }
        phaseCodes[pcIndex]['EntryOrder'+which] = top + 1;
        return sort.sort(Utils.multiSort('order','sequence')).map(function(item){ return item.id });
      }
      var allocateStep = function(index, phaseCodes, which, hoursToAllocate){
        var allocate = hoursToAllocate;
        if(phaseCodes[index][which+'Hours']+allocate < 0){
          allocate = -phaseCodes[index][which+'Hours'];
        }
        hoursToAllocate -= allocate;
        phaseCodes[index][which+'Hours'] += allocate;
        return hoursToAllocate;
      }
      var changePCAllocation = function(pcIndex, which, newHours, task, taskIndex){
        var phaseCodes = task.PhaseCode, maxDuration = task.Duration;
        var phaseCodeOrder = getPhaseCodeEntryOrder(which, pcIndex, phaseCodes);
        newHours = Math.min(newHours, maxDuration);
        var hoursToAllocate = phaseCodes[pcIndex][which+'Hours']-newHours;
        phaseCodes[pcIndex][which+'Hours'] = newHours;
        var i = 0;
        while(hoursToAllocate != 0 && i < phaseCodeOrder.length){
          var index = phaseCodes.findIndex(function(pc){ return pc.ID == phaseCodeOrder[i]});
          hoursToAllocate = allocateStep(index, phaseCodes, which, hoursToAllocate);
          ++i;
        }
      }
      $scope.changePCTime = function(pc, which){
        if($scope.loading || pc.recordType != 'PC' || (which=='DT'&&!$scope.job.ContractAllowDoubleTimeFlag) || ($scope.jobdaycrew.StatusCode!='EPE'&&$scope.jobday.StatusCode!='RJ') || pc['PCClass'+which].match('no-edit'))
          return;
        var task = $scope.jobdaycrew.Task[pc.taskIndex];
        dialogs.duration.openDialog(function(ok, hours, durationDialog){
          if(!ok) return true;
          changePCAllocation(pc.index, which, hours, task, pc.taskIndex);
          durationDialog.hide();
          $scope.phaseCodesChanged = true;
          $scope.job.changed = true;
        }, pc[which+'Hours'], {title: pc.DisplayNumber+' - '+which, allowZero: true});
      }

      $scope.toggleHoursWhich = 0;
      $scope.toggleHours = function(){
        $scope.toggleHoursWhich = $scope.toggleHoursWhich==0?1:0;
      }

      var editAllocationExecute = function(which, hours){
        var obj = getDayCrewObj();
        obj.STAllocation = $scope.jobdaycrew.STHours;
        obj.OTAllocation = $scope.jobdaycrew.OTHours;
        obj.DTAllocation = $scope.jobdaycrew.DTHours;
        obj[which+'Allocation'] = hours;
        obj.allocationWhich = which;
        obj.action = 'tasks';
        obj.Task = getTaskData();
        obj.noSave = true;
        ApiService.post('teEndAction', obj).then(function(resp){editAllocationResult(resp, which)}, editAllocationResult);
        $scope.loading = true;
      }
      var editAllocationResult = function(resp, which){
        if(resp && resp.Job){
          openEndDayResult(resp, false, true);
          $scope.job.changed = true;
          $scope.jobdaycrew.allocationChanged = true;
          $scope.jobdaycrew.allocationWhich = which;
        }
        $scope.loading = false;
      }
      $scope.editAllocation = function(which){
        if($scope.loading)
          return;
        dialogs.duration.openDialog(function(ok, hours, durationDialog){
          if(ok)
            editAllocationExecute(which, hours);
          return true;
        }, $scope.jobdaycrew[which+'Hours'], {title:which+' Hours', allowZero: true})
      }

      /*  MEMBERS  */
      var getMemberDataObj = function(m){
        return { JobDayCrewMemberID:m.ID, CraftID:m.CraftID, ClassID:m.ClassID, StatusID:m.MemberStatusID, InactiveReasonCode:m.InactiveReasonCode, Permanent:m.permanent };
      }
      var getMemberData = function(member){
        if(member){
          return getMemberDataObj(member);
        }
        return $scope.jobdaycrew.JobDayCrewMember.filter(function(m){
          return m.localChanges;
        }).map(getMemberDataObj);
      };
      var cleanMemberSmartpicks = function(sp){
        if(!Array.isArray(sp.Craft))
          sp.Craft = [sp.Craft];
        if(!Array.isArray(sp.Class))
          sp.Class = [sp.Class];
        if(!Array.isArray(sp.JobStatus))
          sp.JobStatus = [sp.JobStatus];
        if(!Array.isArray(sp.InactiveReason))
          sp.InactiveReason = [sp.InactiveReason];
        if(!Array.isArray(sp.Person))
          sp.Person = [sp.Person];
        return sp;
      };
      var loadMemberData = function(){
        ApiService.get('teBeginMember', {
          action:'smartpicks', 
          organization: $scope.jobdaycrew.ForemanOrganizationID,
          department: $scope.job.ContractDepartmentCode
        }).then(function(resp){
          if(resp && resp.Smartpicks){
            var data = cleanMemberSmartpicks(resp.Smartpicks);
            dialogs.addMemberDialog._scope.data = data
            dialogs.activateMemberDialog._scope.data = data;
          }
        });
      };
      var addMemberExecute = function(person, craft, clss, permanent){
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = 'add';
        obj.person = person.ID;
        obj.craft = craft.ID;
        obj.class = clss.ID;
        if(permanent)
          obj.permanent = true;
        ApiService.post('teEndMember', obj)
        .then(addMemberResult,addMemberResult)
      };
      var addMemberResult = function(resp){
        if(resp && resp.JobDayCrewMember){
          $scope.jobdaycrew.JobDayCrewMember.push(resp.JobDayCrewMember);
          sortMembers();
          needRefreshOnNav = true;
        }
        $scope.loading = false;
      };
      $scope.addMember = function(){
        dialogs.addMemberDialog.openDialog();
      };
      $scope.openMember = function(member, $event){
        event.stopPropagation();
        dialogs.activateMemberDialog.openDialog(member);
      };
      var activateMemberExecute = function(mem, craft, clss, jobStatus, reason, permanent){
        var member = $scope.jobdaycrew.JobDayCrewMember.find(function(m){
          return mem.ID == m.ID;
        });
        if(member){
          if(member.MemberStatusDraftCrewTimeFlag != jobStatus.DraftCrewTime){
            member.temporaryHours = jobStatus.DraftCrewTime?$scope.jobdaycrew.TotalHours:0.00001;
          }
          member.localChanges = permanent||(member.CraftID!=craft.ID||member.ClassID!=clss.ID||member.MemberStatusID!=jobStatus.ID||((typeof reason=='undefined'&&member.InactiveReasonCode!="")||(typeof reason!='undefined'&&member.InactiveReasonCode!=reason.Code)));
          member.CraftID = craft.ID;
          member.CraftCraft = craft.Craft;
          member.CraftSortOrder = craft.Sort;
          member.ClassID = clss.ID;
          member.ClassClass = clss.Class;
          member.ClassSortOrder = clss.Sort;
          member.MemberStatusID = jobStatus.ID;
          member.MemberStatusCode = jobStatus.Code;
          member.MemberStatusCrewMemberActiveFlag = jobStatus.Active;
          member.MemberStatusDraftCrewTimeFlag = jobStatus.DraftCrewTime;
          member.MemberStatusDescription = jobStatus.Description;
          if(reason){
            member.InactiveReasonCode = reason.Code;
            member.InactiveReasonMeaning = reason.Meaning;
          }
          member.permanent = permanent;
          $scope.job.changed = $scope.job.changed||member.localChanges;
        }
        sortMembers();
      }
      var changeMemberTimeResult = function(resp){
        $scope.loading = false;
        if(resp && resp.JobDayCrewMember){
          needRefreshOnNav = true;
          var member = $scope.jobdaycrew.JobDayCrewMember.find(function(m){
            return m.ID == resp.JobDayCrewMember.ID;
          });
          if(member){
            Object.assign(member, resp.JobDayCrewMember);
          }
          if(!Array.isArray(resp.WGPhaseCodeSection))
            resp.WGPhaseCodeSection = typeof resp.WGPhaseCodeSection == 'undefined'?[]:[resp.WGPhaseCodeSection];
          return {member: member, WGPhaseCodeSection: resp.WGPhaseCodeSection};
        }
      }
      $scope.changeMemberTime = function(member){
        if($scope.loading || member.StatusCode == 'AP' || !member.ActiveFlag || ($scope.jobdaycrew.StatusCode != 'EPE' && $scope.jobdaycrew.StatusCode != 'RJ'))
          return;
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = '';
        obj.getMemberTasks = true;
        obj.Member = getMemberData(member);
        ApiService.post('teEndMember', obj).then(changeMemberTimeResult,changeMemberTimeResult).then(function(data){
          if(data)dialogs.memberTimeDialog.openDialog(data);
        });
      };
      $scope.changeMemberPerDiem = function(member){
        if($scope.loading)
          return;
        dialogs.memberPerDiem.openDialog(member);
      };
      var sortMembers = function(){
        $scope.jobdaycrew.JobDayCrewMember.sort(Utils.multiSort('CraftSortOrder','ClassSortOrder','PersonFullName'));
        for(var i=0; i<$scope.jobdaycrew.JobDayCrewMember.length; i++){
          $scope.jobdaycrew.JobDayCrewMember[i].Sort = null;
        }
        var sort = 0, active = true;
        for(var i=0; i<$scope.jobdaycrew.JobDayCrewMember.length; i++){
          var member = $scope.jobdaycrew.JobDayCrewMember[i], next;
          if(member.MemberStatusCrewMemberActiveFlag==active && !member.IsForeman && member.Sort == null){
            member.Sort = ++sort;
            while((next = $scope.jobdaycrew.JobDayCrewMember.find(function(m){
              return (m.MemberStatusCrewMemberActiveFlag==active && !m.IsForeman && m.Sort == null && m.PersonID==member.PersonID)
            }))){
              next.Sort = ++sort;
            }
          }
          if(i == $scope.jobdaycrew.JobDayCrewMember.length-1 && active){
            active = false;
            i = -1;
          }
        }
        $scope.jobdaycrew.JobDayCrewMember.sort(Utils.multiSort({name:'MemberStatusCrewMemberActiveFlag',reverse:true},{name:'IsForeman',reverse:true},'Sort'));
      }

      
      /*    EQUIPMENT   */
      var getEquipmentDataObj = function(e){
        return { JobDayCrewEquipmentID:e.ID, ActiveFlag:e.temporaryActive, InactiveReasonCode:e.temporaryInactiveReasonCode, Permanent:e.temporaryPermanent };
      };
      var getEquipmentData = function(equip){
        if(equip){
          return getEquipmentDataObj(equip);
        }
        return $scope.jobdaycrew.JobDayCrewEquipment.filter(function(e){
          return e.localChanges;
        }).map(getEquipmentDataObj);
      };
      var cleanEquipmentSmartpicks = function(sp){
        sp.SmartCodes = Utils.enforceArray(sp.SmartCodes);
        sp.Equipment = Utils.enforceArray(sp.Equipment);
        sp.Category = Utils.enforceArray(sp.Category);
        return sp;
      };
      var loadEquipmentData = function(){
        ApiService.get('teBeginEquip', {
            action:'smartpicks', 
            organization: $scope.jobdaycrew.ForemanOrganizationID
          }).then(function(resp){
            if(resp && resp.SmartCodes){
              var data = cleanEquipmentSmartpicks(resp);
              dialogs.activateEquipmentDialog._scope.data = data;
              dialogs.addEquipmentDialog._scope.data = data;
              console.log(dialogs.addEquipmentDialog._scope.data);
            }
          });
      };
      var activateEquipmentExecute = function(equip, active, reason, permanent){
        var equipment = $scope.jobdaycrew.JobDayCrewEquipment.find(function(e){
          return equip.ID == e.ID;
        });
        if(equipment){
          if(equipment.ActiveFlag != active){
            equipment.temporaryHours = active?$scope.jobdaycrew.TotalHours:0.00001;
          }
          equipment.localChanges = permanent||(equipment.ActiveFlag != active||((typeof reason=='undefined'&&equipment.InactiveReasonCode!="")||(typeof reason!='undefined'&&equipment.InactiveReasonCode!=reason.Code)));
          equipment.temporaryActive = active;
          equipment.temporaryPermanent = permanent;
          if(reason){
            equipment.temporaryInactiveReasonCode = reason.SmartCode;
            equipment.temporaryInactiveReasonMeaning = reason.Meaning;
          }else{
            delete equipment.temporaryInactiveReasonCode;
            delete equipment.temporaryInactiveReasonMeaning;
          }
          $scope.job.changed = $scope.job.changed||equipment.localChanges;
        }
      };

      $scope.activateEquipment = function(equipment, $event){
        $event.preventDefault();
        $event.stopPropagation();
        dialogs.activateEquipmentDialog.openDialog(equipment);
        return false;
      }

      $scope.addEquipment = function(){
        dialogs.addEquipmentDialog.openDialog();
      };
      $scope.addRental = function(){
        Rentals.show({
          id: $scope.jobdaycrew.ID,
          which: 'JobDayCrew',
          orgId: $scope.jobdaycrew.ForemanOrganizationID
        }, function(equip){
          if(equip && equip.ID){
            addEquipmentExecute(equip);
          }
        });
      }

      var addEquipmentExecute = function(equipment, permanent){
        if($scope.jobdaycrew.JobDayCrewEquipment.find(function(e){
          return e.EquipmentID == equipment.ID;
        })){
          return;
        }
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = 'add';
        obj.equipment = equipment.ID;
        if(permanent)
          obj.permanent = true;
        ApiService.post('teEndEquip', obj)
        .then(addEquipmentResult,addEquipmentResult)
      };
      var cleanEquipment = function(equipment){
        equipment.temporaryActive = equipment.ActiveFlag;
        return equipment;
      }
      var addEquipmentResult = function(resp){
        if(resp && resp.JobDayCrewEquipment){
          $scope.jobdaycrew.JobDayCrewEquipment.push(cleanEquipment(resp.JobDayCrewEquipment));
          needRefreshOnNav = true;
        }
        $scope.loading = false;
      };
      var changeEquipmentTimeResult = function(resp){
        $scope.loading = false;
        if(resp && resp.JobDayCrewEquipment){
          var equip = $scope.jobdaycrew.JobDayCrewEquipment.find(function(e){
            return e.ID == resp.JobDayCrewEquipment.ID;
          });
          if(equip){
            Object.assign(equip, resp.JobDayCrewEquipment);
          }
          needRefreshOnNav = true;
          return {equipment: equip};
        }
      }
      $scope.changeEquipmentTime = function(equipment){
        if($scope.loading || equipment.StatusCode == 'AP' || !equipment.ActiveFlag || ($scope.jobdaycrew.StatusCode != 'EPE' && $scope.jobdaycrew.StatusCode != 'RJ'))
          return;
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = '';
        obj.Equipment = getEquipmentData(equipment);
        ApiService.post('teEndEquip', obj).then(changeEquipmentTimeResult,changeEquipmentTimeResult).then(function(data){
          if(data)dialogs.equipmentTimeDialog.openDialog(data);
        });
      };
      

      /*  GENERAL FUNCTIONS  */
      var loadCompletionJob = function(){
          $scope.loading = true;
          var obj = {design: $scope.current.job.ActiveJobDesignID};
          ApiService.get('jobCompletion', obj).then(completionJobResult, completionJobResult)
      }
      var completionJobResult = function(resp){
          $scope.loading = false;
          if(resp && resp.JobDesign){
              $scope.current.job.design = cleanCompletionResponse(resp.JobDesign);
              $scope.current.job.design.lastLocationFetch = new Date().getTime();
              $scope.current.job.design.process = {name:'EndDay', date:$scope.current.jobday.Date, crew: $scope.jobdaycrew.ID};
              mainNav.replacePage('complete-work-job.html');
          }else{
              if(resp && resp.locked && resp.message){
                  ons.notification.alert(resp.message+" Cannot complete work at this time.", {
                      title: "Job Locked",
                  });
              }else{
                  ons.notification.alert("Failed to load job data", {title: "Error"});
              }
          }
      };
      var cleanCompletionResponse = function(design){
          design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
          if(!design.Location)
              design.Location = [];
          if(!Array.isArray(design.Location))
              design.Location = [design.Location];
          design.Location.map(cleanCompletionLocation);
          design.PercentComplete = Utils.fixPercent(design.PercentComplete);
          return design;
      };
      var cleanCompletionLocation = function(location){
          location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
          location.PercentComplete = Utils.fixPercent(location.PercentComplete);
          return location;
      };
      $scope.completeWork = function(){
        $scope.loading = true;
        navStepPost(0, function(resp){
          loadCompletionJob();
        });
      }
      $scope.filterSummary = function(prop, rental){
        if(typeof rental != 'undefined'){
          return function(item){
            return item[prop] && item.Rental===rental;
          };
        }else{
          return function(item){
            return item[prop];
          };
        }
      };
      var weekTotalsResult = function(resp){
        $scope.loading = false;
        var top = resp.Person || resp.Equipment;
        if(resp && top){
          if(!Array.isArray(top.CalcDay))
            top.CalcDay = typeof top.CalcDay == 'undefined' ? [] : [top.CalcDay];
          top.CalcDay.map(function(row){
            row.HiddenDate = new Date(row.HiddenDate);
            row.Date = row.Date?new Date(row.Date):'';
            if(row.Job == '[-]'){
              row.Job = '';
              row.expandable = true;
              row.expanded = false;
            }
            if(!row.Date){
              row.visible = false;
            }
            return row;
          });
        }
        return resp;
      };
      $scope.viewComments = function(){
        Utils.notification.alert($scope.jobdaycrew.LastComment.Comments, {title: "Comments"});
      }
      var getWeekTotals = function(which, id, clssId, date){
        $scope.loading = true;
        var obj = {action: 'week-time', date: dateTimeValue(date||$scope.jobday.Date, 'M/d/yyyy')};
        if(which == 'member'){
          obj.person = id;
          obj.class = clssId;
          return ApiService.get('teEndMember', obj).then(weekTotalsResult,weekTotalsResult);
        }else{
          obj.equip = id;
          return ApiService.get('teEndEquip', obj).then(weekTotalsResult,weekTotalsResult);
        }
        
      };
      $scope.viewWeekTotals = function(which, item){
        if($scope.loading)
          return;
        getWeekTotals(which, which=='member'?item.PersonID:item.EquipmentID,which=='member'?item.ClassID:null).then(function(resp){
          dialogs.weekTime.openDialog(resp);
        });
      };
      $scope.viewReports = function(){
        dialogs.reports.openDialog();
      }
      var commentDialog = function(code){
        var buttons = {'RJ':'Reject','SU':'Resubmit'};
        CommentDialog.show({
            title: buttons[code],
            accept: function(comment){
              $scope.complete(code, comment);
              CommentDialog.hide();
            },
            button: ['accept', buttons[code]],
            requireComment: code=='RJ'
        });
      }
      $scope.review = function(code){
        if(code == 'RJ'){
          commentDialog(code);
          return; 
        }
        $scope.complete(code);
      }
      $scope.complete = function(code, comment){
        if(!code && $scope.jobdaycrew.StatusCode == 'RJ'){
          commentDialog('SU');
          return;
        }
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = 'finish';
        if(code)
          obj.status = code;
        if(comment)
          obj.comment = comment;
        ApiService.post('teEndAction', obj).then(function(resp){finishResult(resp, code)}, finishResult);
      };
      var finishResult = function(resp, code){
        if(resp && resp.JobDayCrew){
          $scope.jobday.Status = $scope.jobdaycrew.StatusMeaning = resp.JobDayCrew.StatusMeaning;
          $scope.jobday.StatusCode = $scope.jobdaycrew.StatusCode = resp.JobDayCrew.StatusCode;
          if(['RJ','RV','AP'].includes(code) && $scope.jobdaycrew.NextJob.length){
            var jdc = $scope.jobdaycrew.NextJob.find(function(nj){
              return $scope.current.JobDayCrewIDs.includes(nj.JobDayCrewID);
            });
            if(jdc){
              $scope.current.nextAction = {
                name: 'nextJDC',
                jdc: jdc.JobDayCrewID
              }
            }
          }
          if(code == 'EPE'){
            $scope.canNav.navToStep('AT');
          }else{
            closeJob(false);
          }
        }else if(resp && resp.data && resp.data.message){
          Utils.notification.alert('An error occured that must be resolved in the desktop version of UPRO.', {
            title: 'Error', 
            buttonLabels: ["OK","Details"],
            callback: function(buttonIndex){
              if(buttonIndex){
                Utils.notification.alert(resp.data.message, {title: 'Error Detail'});
              }
            }
          });
        }
        $scope.loading = false;
      };
      var closeJob = function(save){
        if(save){
          $scope.loading = true;
          navStepPost(0, function(resp){
            closeJob(false);
          });
          return;
        }
        mainNav.popPage();
      };
      $scope.closing = false;
      $scope.closeJob = function(){
          if(!$scope.job.changed){
              closeJob();
              return;
          }
          if($scope.closing || (mainNav.pages[mainNav.pages.length-1].name != 'te-end-day.html')) return;
          $scope.closing = true;
          Utils.notification.confirm("Save changes?", {
              callback: function(ok){
                if(ok == 2){
                  $scope.closing = false;
                  return;
                }
                closeJob(ok==0);
              },
              buttonLabels: ["Save","Discard","Cancel"]
          });
      };
      $scope.hidePage = function(){
        Rentals.reset();
        ons.enableDeviceBackButtonHandler();
        document.removeEventListener('backbutton', $scope.closeJob);
      }
      $scope.showPage = function(){
        ons.disableDeviceBackButtonHandler();
        document.addEventListener('backbutton', $scope.closeJob, false);
      }
      var init = function(){
        openEndDay();
        $timeout(function(){
            ons.findComponent('#end-day-back-button')._element[0].onClick = $scope.closeJob;
        },1);
      };

      /*  DIALOGS  */

      $scope.$on('$destroy', function(){
        Object.keys(dialogs).forEach(function(d){
          if(dialogs[d].visible){
            dialogs[d].hide();
          }
        });
        Rentals.hide();
        CommentDialog.hide();
      });

      //Edit time
      ons.createAlertDialog('time-picker-dialog.html').then(function(dialog){
        dialog._scope.time = new Date('00:00');
        dialog.openDialog = function(done, title, time){
          dialog._scope.title = title||'Pick Time';
          dialog._scope.loading = false;
          dialog._scope.hours = true;
          if(time)
            dialog._scope.time = new Date(time.getTime());
          dialog._scope.done = function(ok, time){
            if(done(ok, time, dialog))
              dialog.hide();
          }
          Utils.openDialog(dialog);
        };
        dialogs.timePicker = dialog;
      });
      // Edit duration
      ons.createAlertDialog('te-duration-dialog.html').then(function(dialog){
        dialog.openDialog = function(done, duration, options){
          if(duration){
            dialog._scope.duration = new Number(duration);
          }else{
            dialog._scope.duration = '';
          }
          dialog._scope.options = options||{};
          dialog._scope.loading = false;
          dialog._scope.done = function(ok, newDuration){
            if(done(ok, newDuration, dialog))
              dialog.hide();
          }
          Utils.openDialog(dialog);
        };
        dialog._scope.roundHours = function(){
          console.log(dialog._scope.options)
          if(dialog._scope.duration) dialog._scope.duration = Math.abs(Math.round(dialog._scope.duration*4)/4);
        }
        dialogs.duration = dialog;
      });
      // Add/edit task
      var updateTaskCompleteFlags = function(resp){
        var completes = {}, completeLength = resp.CompleteTasks.length;
        for(var i=0; i<$scope.jobdaycrew.Task.length; i++){
          if(typeof completes[$scope.jobdaycrew.Task[i].JobTaskID] == 'undefined'){
            completes[$scope.jobdaycrew.Task[i].JobTaskID] = $scope.jobdaycrew.Task[i].Complete;
          }
        }
        for(i=resp.UncompleteTasks.length-1; i>=0; i--){
          if(typeof completes[resp.UncompleteTasks[i].JobTaskID] != 'undefined' && completes[resp.UncompleteTasks[i].JobTaskID]){
            var task = resp.UncompleteTasks.splice(i,1)[0];
            resp.CompleteTasks.push(task);
          }
        }
        for(i=completeLength-1; i>=0; i--){
          if(typeof completes[resp.CompleteTasks[i].JobTaskID] != 'undefined' && !completes[resp.CompleteTasks[i].JobTaskID]){
            var task = resp.CompleteTasks.splice(i,1)[0];
            resp.UncompleteTasks.push(task);
          }
        }
      }
      var cleanAddTasks = function(resp){
        if(!Array.isArray(resp.WGPhaseCodeSection))
          resp.WGPhaseCodeSection = resp.WGPhaseCodeSection?[resp.WGPhaseCodeSection]:[];
        if(!Array.isArray(resp.CompleteTasks))
          resp.CompleteTasks = resp.CompleteTasks?[resp.CompleteTasks]:[];
        if(!Array.isArray(resp.UncompleteTasks))
          resp.UncompleteTasks = resp.UncompleteTasks?[resp.UncompleteTasks]:[];
        if(!Array.isArray(resp.PhaseCode))
          resp.PhaseCode = resp.PhaseCode?[resp.PhaseCode]:[];
        updateTaskCompleteFlags(resp);
        return resp;
      }
      ons.createAlertDialog('te-task-dialog.html').then(function(dialog){
        dialog._scope.parent = $scope;
        var getTaskPhaseCodes = function(allTasks){
          allTasks = allTasks||$scope.jobdaycrew.Task;
          var me = allTasks[dialog._scope.task.index];
          if(me){
            if(!Array.isArray(me.PhaseCode))
              me.PhaseCode = me.PhaseCode?[me.PhaseCode]:[];
            return me.PhaseCode;
          }
          return dialog._scope.task.PhaseCode;
        }
        var setPhaseCodes = function(){
          var allTasks = $scope.jobdaycrew.Task;
          var me = allTasks[dialog._scope.task.index];
          if(me){
            me.PhaseCode = dialog._scope.task.PhaseCode;
          }
        }
        dialog.openDialog = function(done, task, taskIndex, options){
          dialog.refreshPhaseCodes = false;
          dialog.taskIndex = taskIndex;
          dialog._scope.page = 0;
          dialog._scope.options = options||{};
          dialog._scope.JobTypeCode = $scope.jobday.JobTypeCode;
          dialog._scope.editable = ($scope.jobdaycrew.StatusCode == 'EPE' || $scope.jobdaycrew.StatusCode == 'RJ');
          dialog._scope.canEditStart = dialog._scope.editable && task && $scope.jobdaycrew.Task.length == 1;
          dialog._scope.canEditEnd = dialog._scope.editable && task && task.index == $scope.jobdaycrew.Task.length-1;
          dialog._scope.task = Utils.copy(task);
          dialog._scope.task.PhaseCode = getTaskPhaseCodes();
          dialog._scope.title = 'Add Task';
          dialog._scope.button = 'Add';
          dialog._scope.editing = false;
          if(task.JobTaskID && !dialog._scope.options.defaultedTask){
            dialog._scope.title = 'Edit Task';
            dialog._scope.button = 'Ok';
            dialog._scope.editing = true;
          }else if(!dialog._scope.data){
            ApiService.get('teEndAction', {action:'tasks', jdc: $scope.jobdaycrew.ID}).then(function(resp){
              dialog._scope.data = cleanAddTasks(resp);
            });
          }
          dialog._scope.done = function(ok, newTask){
            if(done(ok, newTask)){
              if(ok)
                setPhaseCodes();
              dialog.hide();
            }
          }
          dialog._scope.effectiveTitle = dialog._scope.title;
          if(dialog._scope.options.phaseCodes){
             dialog.refreshPhaseCodes = true;
            dialog._scope.nav(1);
          }
          Utils.openDialog(dialog);
        };
        dialog._scope.changeTime = function(which){
          if(!dialog._scope.editable || (which == 'StartTime' && !dialog._scope.canEditStart) || (which == 'EndTime' && !dialog._scope.canEditEnd))
            return;
          dialogs.timePicker.openDialog(function(ok,time){
            if(ok){
              if(which == 'EndTime'){
                if(time <= dialog._scope.task.StartTime){
                  Utils.notification.alert({title: 'Error', message: 'End time must be later than Start time.'});
                  return false;
                }
                if(!dialog._scope.task.EndTime)
                  dialog._scope.task.EndTime = new Date();
              }
              dialog.refreshPhaseCodes = true;
              updateTaskTime(which, dialog._scope.task, time);
            }
            return true;
          }, which=='StartTime'?'Start':'End', dialog._scope.task[which]);
        };
        var updateDuration = function(duration){
          dialog._scope.task.Duration = duration;
          if(!dialog._scope.task.EndTime)
            dialog._scope.task.EndTime = new Date();
          dialog.refreshPhaseCodes = true;
          dialog._scope.task.EndTime.setTime(dialog._scope.task.StartTime.getTime()+duration*HOURS);
        };
        dialog._scope.changeDuration = function(){
          if(!dialog._scope.editable || !dialog._scope.canEditEnd)
            return;
          dialogs.duration.openDialog(function(ok, duration){
            if(ok){
              if(duration <= 0){
                Utils.notification.alert({title: 'Error', message: 'Duration must be greater than 0.'});
                return false;
              }
              updateDuration(duration);
            }
            return true;
          }, dialog._scope.task.Duration);
        };
        dialog._scope.chooseTask = function(){
          dialogs.pickTask.openDialog(function(task){
            dialog._scope.task.JobTaskID = task.JobTaskID;
            dialog._scope.task.SectionID = task.ID;
            dialog._scope.task.SectionTypeCode = task.TypeCode;
            dialog._scope.task.JobTaskTaskNumber = task.Task;
            dialog._scope.task.JobTaskDescription = task.Description;
            dialog._scope.task.JobTaskSystemCode = task.SystemCode;
            dialog.refreshPhaseCodes = true;
            if(task.JobTaskID < 0 || !dialog._scope.task.JobDayCrewTaskID){
              dialog._scope.task.PhaseCode = task.PhaseCode;
            }else{
              dialog._scope.task.PhaseCode = getTaskPhaseCodes();
            }
            dialog._scope.task.MasterFlag = null==$scope.jobdaycrew.Task.find(function(t){
              return t.JobTaskID == task.JobTaskID
            });
            dialog._scope.task.Complete = task.CanComplete?(task.Complete||false):'';
            if(!dialog._scope.Duration && dialog._scope.task.JobTaskTaskNumber.match(/meal|break/i)){
              updateDuration(0.5);
            }
          }, dialog._scope.data);
        };
        dialog.calcPhaseCodeClasses = function(){
          var hours = [0,0,0], pcCount = dialog._scope.task.PhaseCode.length;
          for(var i=0; i<pcCount; i++){
            hours[0] += dialog._scope.task.PhaseCode[i].STHours;
            hours[1] += dialog._scope.task.PhaseCode[i].OTHours;
            hours[2] += dialog._scope.task.PhaseCode[i].DTHours;
          }
          ['ST','OT','DT'].forEach(function(which, index){
            dialog._scope.task['PCClass'+which] = (pcCount < 2 || hours[index] == 0) ? 'no-edit' : '';
          });
        }
        dialog._scope.canChangePCClass = function(which){
          if(typeof dialog._scope.task['PCClass'+which] == 'undefined'){
            dialog.calcPhaseCodeClasses();
          }
          return dialog._scope.task['PCClass'+which];
        }
        dialog._scope.changePCTime = function(pc, which, pcIndex){
          if(dialog._scope.task['PCClass'+which] == 'no-edit')
            return;
          dialogs.duration.openDialog(function(ok, hours, durationDialog){
            if(!ok) return true;
            changePCAllocation(pcIndex, which, hours, dialog._scope.task);
            durationDialog.hide();
          }, pc[which+'Hours'], {title: pc.DisplayNumber+' - '+which, allowZero: true});
        }
        var reloadPhaseCodes = function(){
          dialog._scope.loading = true;
          var obj = getDayCrewObj();
          obj.action = 'tasks';
          obj.noSave = true;
          obj.Task = getTaskData();
          if(dialog._scope.options.remove){
            obj.Task.pop();
          }else{
            obj.Task.splice(dialog.taskIndex, 1, getTaskDataOne(dialog._scope.task));
          }
          obj.IgnorePhaseCodes = true;
          return ApiService.post('teEndAction', obj).then(function(resp){
            if(resp && resp.Job){
              resp.Job.JobDayCrew.Task = cleanTasks(resp.Job.JobDayCrew.Task);
              if(dialog._scope.task.JobTaskID < 0){
                resp.Job.JobDayCrew.Task.forEach(function(t){
                  if(t.JobTaskID && t.TempJobTaskID == dialog._scope.task.JobTaskID)
                    dialog._scope.task.JobTaskID = t.JobTaskID;
                });
              }
              dialog._scope.task.PhaseCode = getTaskPhaseCodes(resp.Job.JobDayCrew.Task);
              dialog.calcPhaseCodeClasses();
            }
            dialog.refreshPhaseCodes = false;
          }).finally(function(){
            dialog._scope.loading = false;
          });
        }
        dialog.updatePhaseCodes = function(task, done){
          dialog._scope.task = task;
          dialog._scope.options = {};
          reloadPhaseCodes().then(function(){
            done(dialog._scope.task);
          });
        }
        dialog._scope.nav = function(dir){
          var phases = checkForPhaseCodes(dialog._scope.task);
          dialog._scope.page += dir*(phases?1:2);
          switch(dialog._scope.page){
            case 0:
              dialog._scope.effectiveTitle = dialog._scope.title;
            break;
            case 1:
              dialog._scope.effectiveTitle = dialog._scope.task.JobTaskTaskNumber+' - Phase Codes';
              if(dialog.refreshPhaseCodes){
                reloadPhaseCodes();
              }
            break;
            case 2:
              if(!phases && dialog.refreshPhaseCodes){
                reloadPhaseCodes();
              }
              dialog._scope.effectiveTitle = 'More Details';
            break;
          }
        };

        dialogs.task = dialog;
      });
      // Pick task
      var getCompletionChangedJobTasks = function(clear){
        if(!dialogs.pickTask._scope.tabs)
          return {};
        var tasks = {}, tabs = dialogs.pickTask._scope.tabs;
        for(var i=0; i<tabs.length; i++){
          var jobTasks = tabs[i].tasks;
          for(var j=0; j<jobTasks.length; j++){
            if(jobTasks[j].CompleteChanged){
              tasks[jobTasks[j].JobTaskID] = tabs[i].label=='Complete';
            }
            if(clear)
              jobTasks[j].CompleteChanged = '';
          }
        }
        return tasks;
      }
      var updateTaskCompleteFlagsFromDialog = function(tabs){
        var changedJobTasks = getCompletionChangedJobTasks();
        for(var i=0; i<$scope.jobdaycrew.Task.length; i++){
          if(typeof changedJobTasks[$scope.jobdaycrew.Task[i].JobTaskID] != 'undefined'){
            $scope.jobdaycrew.Task[i].Complete = changedJobTasks[$scope.jobdaycrew.Task[i].JobTaskID];
          }
        }
        if(Object.keys(changedJobTasks).length){
          $scope.job.changed = true;
        }
      }
      var buildTaskTabs = function(data){
        var tabs = [];
        if(data.UncompleteTasks&&data.UncompleteTasks.length)
          tabs.push({label: 'Active', icon:'ion-arrow-graph-up-right', tasks: data.UncompleteTasks});
        if(data.WGPhaseCodeSection&&data.WGPhaseCodeSection.length)
          tabs.push({label: 'Workgroup', icon:'ion-settings', tasks: data.WGPhaseCodeSection});
        if(data.CompleteTasks&&data.CompleteTasks.length)
          tabs.push({label: 'Complete', icon: 'ion-flag', tasks: data.CompleteTasks});
        return tabs;
      }
      ons.createDialog('te-pick-task-dialog.html').then(function(dialog){
        addTaskTabbar.on('prechange', function(e){
            $timeout(function(){
                dialog._scope.tabIndex = e.index;
            });
        });
        dialog.openDialog = function(done, data){
          dialog._scope.data = data;
          dialog._scope.JobTypeCode = $scope.jobday.JobTypeCode;
          dialog._scope.tabs = buildTaskTabs(data);
          dialog._scope.mode = 'Choose Task';
          dialog._scope.tabIndex = 0;
          $timeout(function(){addTaskTabbar.setActiveTab(0);});
          dialog._scope.done = function(task){
              task.Complete = task.Complete||addTaskTabbar.getActiveTabIndex()==2;
              done(task);
              dialog.hide();
          }
          Utils.openDialog(dialog);
        };
        dialog._scope.selectTask = function(task){
          if(!task.JobTaskID)
            generateNewTempJobTaskId(task);
          dialog._scope.done(task);
        };
        dialog._scope.createTask = function(){
          Utils.focusInput('newTaskTaskNumber');
          dialog._scope.newTask = {Task: '', Description: '', Complete: false, PhaseCode: [], Comment:'', CanComplete: true};
          if(dialog._scope.data.PhaseCode.length > 0){
            dialog._scope.newTask.PhaseCode = dialog._scope.data.PhaseCode.filter(function(pc){
              return pc.Default;
            });
          }
          dialog._scope.mode = 'Create Task';
        };
        var validateNewTaskNumber = function(num){
          var regex = new RegExp('^'+Utils.sanitizeTermForRegex(num)+'$', 'i');
          var exists = dialog._scope.data.UncompleteTasks.find(function(t){
            return t.Task.match(regex);
          })||dialog._scope.data.WGPhaseCodeSection.find(function(t){
            return t.Task.match(regex);
          })||dialog._scope.data.CompleteTasks.find(function(t){
            return t.Task.match(regex);
          });
          return !exists;
        };
        dialog._scope.saveNewTask = function(){
          if(!validateNewTaskNumber(dialog._scope.newTask.Task)){
            Utils.notification.alert('A task named <b>'+dialog._scope.newTask.Task+'</b> already exists.', {title: 'Error'});
            return;
          }
          generateNewTempJobTaskId(dialog._scope.newTask);
          if(dialog._scope.newTask.Complete){
            dialog._scope.data.CompleteTasks.push(dialog._scope.newTask);
          }else{
            dialog._scope.data.UncompleteTasks.push(dialog._scope.newTask);
          }
          dialog._scope.done(dialog._scope.newTask);
        };
        dialog._scope.addPhaseCode = function(){
          dialogs.pickPhaseCode.openDialog(function(pc){
            dialog._scope.newTask.PhaseCode.push(pc);
          }, dialog._scope.data.PhaseCode.filter(function(p){return dialog._scope.newTask.PhaseCode.indexOf(p) == -1}));
        };
        dialog._scope.transferTask = function($index){
          var tabLabel = dialog._scope.tabs[dialog._scope.tabIndex].label;
          if(tabLabel == 'Workgroup' || !dialog._scope.tabs[dialog._scope.tabIndex].tasks[$index].CanComplete)
            return;
          navigator.vibrate(10);
          var newTabLabel = tabLabel=='Active'?'Complete':'Active', 
              newTabIcon = tabLabel=='Active'?'ion-flag':'ion-arrow-graph-up-right',
              newTabIndex = dialog._scope.tabs.findIndex(function(t){return t.label == newTabLabel});
          if(newTabIndex < 0){
            newTabIndex = tabLabel=='Active'?dialog._scope.tabs.length:0;
            if(newTabLabel == 'Active'){
              dialog._scope.data.UncompleteTasks = [];
              dialog._scope.tabs.splice(newTabIndex,0,{label: newTabLabel, icon: newTabIcon, tasks: dialog._scope.data.UncompleteTasks});
            }else{
              dialog._scope.data.CompleteTasks = [];
              dialog._scope.tabs.splice(newTabIndex,0,{label: newTabLabel, icon: newTabIcon, tasks: dialog._scope.data.CompleteTasks});
            }
            if(newTabIndex <= dialog._scope.tabIndex)
              ++dialog._scope.tabIndex;
          }
          var task = dialog._scope.tabs[dialog._scope.tabIndex].tasks.splice($index, 1)[0];
          task.CompleteChanged = !task.CompleteChanged;
          dialog._scope.tabs[newTabIndex].tasks.push(task);
          if(!dialog._scope.tabs[dialog._scope.tabIndex].tasks.length){
            dialog._scope.tabs.splice(dialog._scope.tabIndex, 1);
            if(newTabIndex > 0)
              newTabIndex--;
          }
          dialog._scope.tabIndex = newTabIndex;
          $timeout(function(){addTaskTabbar.setActiveTab(newTabIndex)});
          updateTaskCompleteFlagsFromDialog();
        }
        dialogs.pickTask = dialog;
      });
      ons.createDialog('te-pick-phasecode-dialog.html').then(function(dialog){
        dialog.openDialog = function(done, phasecodes){
          dialog._scope.PhaseCode = phasecodes;
          dialog._scope.done = function(pc){
              done(pc);
              dialog.hide();
          }
          Utils.openDialog(dialog);
        };
        dialogs.pickPhaseCode = dialog;
      });

      //Add Member dialog
      ons.createAlertDialog('te-add-member-dialog.html').then(function(dialog){
        dialog._scope.selects = {craft:{},class:{},person:{}};
        dialog.openDialog = function(){
          dialog._scope.selects = {};
          dialog._scope.permanent = false;
          dialog._scope.hidePermanent = false;

          if(!dialog._scope.data){
            loadMemberData();
          }
          Utils.openDialog(dialog);
        }
        dialog._scope.tagHandler = function(tag){
            return null;
        };
        dialog._scope.filterSearch = function(prop, search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                return item[prop].match(regex);
            };
        };
        dialog._scope.filterName = function(search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                return item.FullName.match(regex);
            };
        };
        dialog._scope.selectCraft = function($item, $model){
            delete dialog._scope.selects.class;
        };
        dialog._scope.selectPerson = function($item, $model){
          dialog._scope.selects.craft = dialog._scope.data.Craft.find(function(crft){
            return crft.ID == $item.CraftID;
          });
          if($scope.jobdaycrew.JobDayCrewMember.findIndex(function(m){return m.PersonID==$item.ID}) < 0){
              dialog.existingClasses = [];
            dialog._scope.selects.class = dialog._scope.data.Class.find(function(clss){
              return clss.ID == $item.ClassID;
            });
          }else{
            dialog.existingClasses = $scope.jobdaycrew.JobDayCrewMember.reduce(function(classes, member){
              if(member.PersonID == $item.ID){
                classes.push(member.ClassID);
              }
              return classes;
            }, []);
          }
          if(dialog.existingClasses.length){
            dialog._scope.hidePermanent = true;
            dialog._scope.permanent = false;
          }else{
            dialog._scope.hidePermanent = false;
          }
        };
        dialog._scope.filterClass = function(prop, search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                if(dialog._scope.selects.craft && dialog._scope.selects.craft.ID == item.CraftID && !dialog.existingClasses.includes(item.ID)){
                  return item[prop].match(regex);
                }
                return false;
            };
        };
        dialog._scope.done = function(ok, person, craft, clss, permanent){
            if(ok){
                addMemberExecute(person, craft, clss, permanent);
            }
            dialog.hide();
        };
        dialogs.addMemberDialog = dialog;
      });

      //Activate member dialog
      ons.createAlertDialog('activate-member-dialog.html').then(function(dialog){
        dialog._scope.selects = {craft:{},class:{},jobStatus:{},inactiveReason:{}};
        dialog.openDialog = function(member){
          dialog._scope.member = member;
          dialog._scope.hasChanges = member.PermanentButton!='';
          dialog._scope.permanent = {value:member.permanent||false};
          dialog.existingClasses = $scope.jobdaycrew.JobDayCrewMember.reduce(function(classes, m){
            if(m.PersonID == member.PersonID && m.ID != member.ID){
              classes.push(m.ClassID);
            }
            return classes;
          }, []);
          dialog._scope.hidePermanent = false;
          if(dialog.existingClasses.length){
            dialog._scope.hidePermanent = true;
            dialog._scope.permanent.value = false;
          }
          dialog._scope.selects = {
            craft: {ID: member.CraftID,Craft:member.CraftCraft},
            class: {ID: member.ClassID,Class:member.ClassClass, CraftID: member.CraftID},
            jobStatus: {Code:member.MemberStatusCode,Description:member.MemberStatusDescription,Active:member.MemberStatusCrewMemberActiveFlag,ID:member.MemberStatusID,DraftCrewTime:member.MemberStatusDraftCrewTimeFlag},
            inactiveReason: member.InactiveReasonCode?{Code:member.InactiveReasonCode,Meaning:member.InactiveReasonMeaning}:undefined
          };
          dialog._scope.calcHasChanges();
          if(!dialog._scope.data){
            loadMemberData();
          }
          Utils.openDialog(dialog);
        }
        dialog._scope.tagHandler = function(tag){
            return null;
        };
        dialog._scope.filterSearch = function(prop, search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                return item[prop].match(regex);
            };
        };
        dialog._scope.selectCraft = function($item, $model){
            delete dialog._scope.selects.class;
            dialog._scope.calcHasChanges();
        };
        dialog._scope.calcHasChanges = function(){
          var selects = dialog._scope.selects, member = dialog._scope.member;
          if(member.PermanentStatus == ''){
            dialog._scope.permanentLabel = 'Permanently Add To Crew'
            dialog._scope.hasChanges = true;
            return;
          }
          dialog._scope.permanentLabel = '';
          if(member.PermanentStatus != 'F' && selects.class && (member.PermanentCraftID != selects.craft.ID || member.PermanentClassID != selects.class.ID)){
            dialog._scope.permanentLabel = dialog._scope.permanentLabel?'Permanent':'Permanently Change Craft/Class';
            dialog._scope.hasChanges = true;
          }
          if(member.PermanentStatus == 'Y' && !selects.jobStatus.Active){
            dialog._scope.permanentLabel = dialog._scope.permanentLabel?'Permanent':'Permanently Inactive';
            dialog._scope.hasChanges = true;
          }else{
            if(['','N',false].includes(member.PermanentStatus) && selects.jobStatus.Active){
              dialog._scope.permanentLabel = dialog._scope.permanentLabel?'Permanent':'Permanently Active';
              dialog._scope.hasChanges = true;
            }
          }
          dialog._scope.permanentLabel = dialog._scope.permanentLabel||'Permanent';
          if(dialog._scope.hasChanges) return;
          dialog._scope.hasChanges = false;
        }
        dialog._scope.filterClass = function(prop, search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                if(dialog._scope.selects.craft.ID == item.CraftID && !dialog.existingClasses.includes(item.ID)){
                  return item[prop].match(regex);
                }
                return false;
            };
        };
        dialog._scope.done = function(ok, craft, clss, jobStatus, reason){
            if(ok){
                activateMemberExecute(dialog._scope.member, craft, clss, jobStatus, reason, dialog._scope.hasChanges&&dialog._scope.permanent.value);
            }
            dialog.hide();
        };
        dialogs.activateMemberDialog = dialog;
      });

      //Member time dialog
      ons.createAlertDialog('member-time-dialog.html').then(function(dialog){
        dialog.openDialog = function(data){
          dialog._scope.member = Utils.copy(data.member);
          dialog._scope.taskData = {WGPhaseCodeSection: data.WGPhaseCodeSection, disableAdd: true};
          dialog._scope.loading = false;
          Utils.openDialog(dialog);
        }
        dialog._scope.filterPhaseCodeTime = function(){
            return function( item ) {
                return item.IsTask===true||item.IsMemberTask===true;
            };
        };
        var checkTaskTime = function(pct, save){
          var obj = getDayCrewObj();
          obj.action = 'task-time';
          obj.Member = getMemberData(dialog._scope.member);
          if(save)
            obj.save = true;
          if(Array.isArray(pct)){
            obj.Member.PhaseCodeTime = pct.map(function(time){
              return Utils.pick(time, 'ID','IsTask','IsMemberTask','SectionID','TypeCode','ST','OT','DT');
            });
          }else{
            obj.Member.PhaseCodeTime = Utils.pick(pct, 'ID','IsTask','IsMemberTask','SectionID','TypeCode','ST','OT','DT');
          }
          return ApiService.post('teEndMember', obj);
        }
        dialog._scope.changeTime = function(task, which){
          if(dialog._scope.loading) return;
          dialogs.duration.openDialog(function(ok, hours, durationDialog){
            if(!ok) return true;
            durationDialog._scope.loading = true;
            var tempTask = Object.assign({},task);
            tempTask[which] = hours;
            checkTaskTime(tempTask).then(function(resp){
              if(resp && resp.JobDayCrewMember && resp.JobDayCrewMember.MemberPhaseCodeTime){
                if(!Array.isArray(resp.JobDayCrewMember.MemberPhaseCodeTime))
                  resp.JobDayCrewMember.MemberPhaseCodeTime = [resp.JobDayCrewMember.MemberPhaseCodeTime];
                Object.assign(dialog._scope.member, resp.JobDayCrewMember);
                if(task.SectionID)
                  delete task.ID;
              }
              durationDialog._scope.loading = false;
              durationDialog.hide();
            });
          }, task[which], {title: which+' Hours', allowZero: true});
        };
        dialog._scope.addTask = function(){
          dialogs.pickTask.openDialog(function(task){
            dialog._scope.member.MemberPhaseCodeTime.push({
              SectionID: task.ID,
              IsTask: false,
              IsMemberTask: true,
              TypeCode: task.TypeCode,
              Code: task.Task,
              ST: 0,
              OT: 0,
              DT: 0,
              Total: 0
            });
          }, dialog._scope.taskData);
        };
        dialog._scope.showAddButton = function(phaseCodeTime){
          if(!phaseCodeTime || !dialog._scope.taskData) return false;
          return dialog._scope.taskData.WGPhaseCodeSection.filter(function(task){
            return !phaseCodeTime.find(function(pct){
              return pct.SectionID?pct.SectionID == task.ID:pct.ID == task.ID;
            });
          }).length;
        };
        dialog._scope.done = function(ok){
            if(ok){
              dialog._scope.loading = true;
              checkTaskTime(dialog._scope.member.MemberPhaseCodeTime, true)
                .then(changeMemberTimeResult)
                .finally(function(){
                  dialog._scope.loading = false;
                  dialog.hide();
                });
            }else{
              dialog.hide();
            }            
        };
        dialogs.memberTimeDialog = dialog;
      });

      //Member per diem dialog
      ons.createAlertDialog('member-pd-dialog.html').then(function(dialog){
        var convertReasonToList = function(text, prepare){
          return prepare?Utils.prepareForHTML(text):text;
        }
        var copyPDValues = function(member){
          dialog._scope.data = {
            PerDiemFlag: member.PerDiemFlag,
            PerDiemAmount: $filter('number')(member.PerDiemAmount, 2),
            PerDiemBillableAmount: $filter('number')(member.PerDiemBillableAmount, 2),
            NoPerDiemReason: convertReasonToList(member.NoPerDiemReason, true)
          }
        }
        dialog.openDialog = function(member){
          dialog._scope.loading = false;
          dialog._scope.member = member;
          copyPDValues(member);
          Utils.openDialog(dialog);
        }
        dialog._scope.round = function(){
          dialog._scope.data.PerDiemAmount = $filter('number')(dialog._scope.data.PerDiemAmount, 2);
        }
        var perDiemResult = function(resp){
          dialog._scope.loading = false;
          if(resp && resp.JobDayCrewMember){
            
            if(!resp.JobDayCrewMember.PerDiemFlag && dialog._scope.data.PerDiemFlag){
              copyPDValues(resp.JobDayCrewMember);
              if(dialog._scope.data.NoPerDiemReason){
                Utils.notification.alert(convertReasonToList(resp.JobDayCrewMember.NoPerDiemReason), {title: 'Not Applied'});
              }
            }else{
              needRefreshOnNav = true;
              var member = $scope.jobdaycrew.JobDayCrewMember.find(function(m){
                return m.ID == resp.JobDayCrewMember.ID;
              });
              if(member){
                Object.assign(member, resp.JobDayCrewMember);
              }
              dialog.hide();
            }
          }
        }
        dialog._scope.done = function(ok){
          if(ok && (dialog._scope.data.PerDiemFlag != dialog._scope.member.PerDiemFlag || dialog._scope.data.PerDiemAmount != dialog._scope.member.PerDiemAmount)){
            var obj = getDayCrewObj();
            obj.action = 'per-diem';
            obj.member = dialog._scope.member.ID;
            obj.perdiem = dialog._scope.data.PerDiemFlag;
            obj.amount = dialog._scope.data.PerDiemAmount;
            dialog._scope.loading = true;
            ApiService.post('teEndMember', obj).then(perDiemResult,perDiemResult);
          }else{
            dialog.hide();
          }
        }
        dialogs.memberPerDiem = dialog;
      });

      //Activate equipment dialog
      ons.createAlertDialog('activate-equipment-dialog.html').then(function(dialog){
        dialog._scope.select = {inactiveReason:{}};
        dialog.openDialog = function(equipment){
          dialog._scope.equipActive = equipment.temporaryActive==null?equipment.ActiveFlag:equipment.temporaryActive;
          dialog._scope.originalActive = dialog._scope.equipActive;
          dialog._scope.equipPermanent = {value:equipment.temporaryPermanent||false};
          dialog._scope.equipment = equipment;
          dialog._scope.hasChanges = false;
          dialog._scope.selects = {};
          if(equipment.temporaryInactiveReasonCode||equipment.InactiveReasonCode){
            dialog._scope.selects.inactiveReason = {SmartCode: equipment.temporaryInactiveReasonCode||equipment.InactiveReasonCode, Meaning: equipment.temporaryInactiveReasonMeaning||equipment.InactiveReasonMeaning};
          }
          dialog._scope.calcHasChanges();
          if(!dialog._scope.data)
            loadEquipmentData();
          Utils.openDialog(dialog);
        }
        dialog._scope.activate = function(){
          if(dialog._scope.equipActive){
            dialog._scope.selects = {};
          }else{
            dialog._scope.selects.inactiveReason = dialog._scope.data.SmartCodes.find(function(sc){return sc.SmartCode == 'ID';});
          }
          dialog._scope.originalActive = dialog._scope.equipActive;
          dialog._scope.calcHasChanges();
        }
        dialog._scope.calcHasChanges = function(){
          var equip = dialog._scope.equipment;
          if(equip.PermanentStatus == ''){
            dialog._scope.hasChanges = true;
            return;
          }
          console.log(equip.PermanentStatus)
          if(equip.PermanentStatus == 'Y' && !dialog._scope.equipActive){
            dialog._scope.hasChanges = true;
            return;
          }else if(['','N',false].includes(equip.PermanentStatus) && dialog._scope.equipActive){
            dialog._scope.hasChanges = true;
            return;
          }
          dialog._scope.hasChanges = false;
        }
        dialog._scope.tagHandler = function(tag){
            return null;
        };
        dialog._scope.filterReason = function(prop, search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                return item[prop].match(regex);
            };
        };
        dialog._scope.done = function(ok, active, reason, permanent){
            if(ok){
                activateEquipmentExecute(dialog._scope.equipment, active, reason, permanent);
            }
            dialog.hide();
        };
        dialogs.activateEquipmentDialog = dialog;
      });
      //Add equipment dialog
      ons.createAlertDialog('te-add-equipment-dialog.html').then(function(dialog){
          dialog._scope.selects = {equipment:{}};
          dialog.openDialog = function(){
            dialog._scope.selects = {};
            dialog._scope.permanent = false;
            if(!dialog._scope.data)
              loadEquipmentData();
            Utils.openDialog(dialog);
          }
          dialog._scope.tagHandler = function(tag){
              return null;
          };
          dialog._scope.filterSearch = function(search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  return (!dialog._scope.selects.category||item.CategoryID==dialog._scope.selects.category.ID) && $scope.jobdaycrew.JobDayCrewEquipment.findIndex(function(e){return e.EquipmentID==item.ID})<0 && (item.Number.match(regex)||item.Description.match(regex));
              };
          };
          dialog._scope.selectCategory = function(){
            delete dialog._scope.selects.equipment;
          }
          dialog._scope.selectEquip = function(equip){
            var category = dialog._scope.data.Category.find(function(category){return category.ID == equip.CategoryID});
            if(category){
              dialog._scope.selects.category = category;
            }
          }
          dialog._scope.done = function(ok, equipment, permanent){
              if(ok){
                  addEquipmentExecute(equipment, permanent);
              }
              dialog.hide();
          };
          dialogs.addEquipmentDialog = dialog;
      });
      //Equipment time dialog
      ons.createAlertDialog('equipment-time-dialog.html').then(function(dialog){
        dialog.openDialog = function(data){
          dialog._scope.equipment = data.equipment;
          dialog._scope.loading = false;
          Utils.openDialog(dialog);
        }
        dialog._scope.filterPhaseCodeTime = function(){
            return function( item ) {
                return item.IsTask===true;
            };
        };
        var checkTaskTime = function(pct, save){
          var obj = getDayCrewObj();
          obj.action = 'task-time';
          obj.Equipment = getEquipmentData(dialog._scope.equipment);
          if(save)
            obj.save = true;
          if(Array.isArray(pct)){
            obj.Equipment.PhaseCodeTime = pct.map(function(time){
              return Utils.pick(time, 'ID','IsTask','Total');
            });
          }else{
            obj.Equipment.PhaseCodeTime = Utils.pick(pct, 'ID','IsTask','Total');
          }
          return ApiService.post('teEndEquip', obj);
        }
        dialog._scope.changeTime = function(task, which){
          if(dialog._scope.loading) return;
          dialogs.duration.openDialog(function(ok, hours, durationDialog){
            if(!ok) return true;
            durationDialog._scope.loading = true;
            var tempTask = Object.assign({},task);
            tempTask[which] = hours;
            checkTaskTime(tempTask).then(function(resp){
              if(resp && resp.EquipmentPhaseCodeTime){
                Object.assign(task, resp.EquipmentPhaseCodeTime);
              }
              if(resp && resp.message){
                Utils.notification.alert(resp.message, {title: 'Notice'});
              }
              durationDialog._scope.loading = false;
              durationDialog.hide();
            });
          }, task[which], {title: 'Hours', allowZero: true});
        };
        dialog._scope.done = function(ok){
            if(ok){
              dialog._scope.loading = true;
              checkTaskTime(dialog._scope.equipment.EquipmentPhaseCodeTime, true)
                .then(changeEquipmentTimeResult)
                .finally(function(){
                  dialog._scope.loading = false;
                  dialog.hide();
                });
            }else{
              dialog.hide();
            }            
        };
        dialogs.equipmentTimeDialog = dialog;
      });
      //Week time dialog
      ons.createAlertDialog('week-time-dialog.html').then(function(dialog){
          var calcPeriod = function(data){
            var end = 0;
            var hours = data.CalcDay.reduce(function(hours, row){
              if(row.Date){
                end = row.Date;
                if(dialog._scope.person){
                  hours.STHours += new Number(row.STHours);
                  hours.OTHours += new Number(row.OTHours);
                  hours.DTHours += new Number(row.DTHours);
                  hours.PerDiem += new Number(row.PerDiem);
                }
                hours.TotalHours += new Number(row.TotalHours);
              }
              return hours;
            }, {STHours:0,OTHours:0,DTHours:0,PerDiem:0,TotalHours:0});
            data.period = {
              date: dateTimeValue(data.CalcDay[0].Date,'M/d/yyyy')+' - '+dateTimeValue(end,'M/d/yyyy'),
              hours: hours
            };
          }
          dialog.openDialog = function(data){
            dialog._scope.person = data.Person?true:false;
            dialog._scope.data = dialog._scope.person?data.Person:data.Equipment;
            calcPeriod(dialog._scope.data);
            Utils.openDialog(dialog);
          }
          dialog._scope.filterRows = function(){
              return function( item ) {
                  return item.Date || item.visible;
              };
          };
          dialog._scope.step = function(direction){
            dialog._scope.loading = true;
            var newDate = new Date(dialog._scope.data.PeriodDate);
            newDate.setDate(newDate.getDate()+(direction*7));
            getWeekTotals(dialog._scope.person?'member':'equipment',dialog._scope.data.ID, dialog._scope.person?dialog._scope.data.ClassID:null, newDate).then(function(resp){
              dialog._scope.loading = false;
              dialog._scope.data = resp.Person?resp.Person:resp.Equipment;
              calcPeriod(dialog._scope.data);
            })
          };
          dialog._scope.expandCollapse = function(row){
            if(!row.Date || !row.expandable) return;
            var dateTime = row.Date.getTime();
            dialog._scope.data.CalcDay.forEach(function(job){
              if(!job.expandable && job.HiddenDate.getTime() == dateTime)
                job.visible = !row.expanded;
            });
            row.expanded = !row.expanded;
          }
          dialog._scope.done = dialog.hide;
          dialogs.weekTime = dialog;
      });

      //Reports dialog
      ons.createDialog('app/dialog/reports/reports-dialog.html').then(function(dialog){
        dialog.openDialog = function(){
          dialog._scope.openingDialog = false;
          Utils.openDialog(dialog);
        };
        dialog.setReports = function(reports){
          dialog._scope.reports = reports;
        }
        dialogs.reports = dialog;
      });

      init();
    }]);

})();
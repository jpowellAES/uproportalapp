(function(){
    'use strict';

    angular.module('app').controller('TEDayJobsController', ['$scope', '$timeout', 'ApiService', 'StorageService', '$rootScope', 'Utils', '$anchorScroll', function($scope, $timeout, ApiService, StorageService, $rootScope, Utils, $anchorScroll) {

        $scope.loaded = false;
        $scope.working = true;
        $scope.loading = false;
        $scope.module = $scope.$parent.current.module;
        $scope.title = $scope.module.title;
        $scope.tabs = [];
        var dialogs = {}, userData = {};
        $scope.tabIndex = 0;
        $scope.infoDialog = {};
        ons.createDialog('app/dialog/job-info-dialog.html').then(function(dialog){
            dialog._scope.infoDialog = $scope.infoDialog;
            dialog._scope.OrgType = $scope.OrgType;
            dialog._scope.hideDollars = true;
            $scope.infoDialog.dialog = dialog;
        });

        $scope.loadJobs = function($done){
            var obj = {
                types: 'JobDay',
                module: $scope.module.key
            };
            ApiService.get('jobs', obj).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var cleanJobs = function(jobs){
            jobs = Utils.enforceArray(jobs);
            jobs.forEach(function(job){
                if(job.Date)
                    job.Date = new Date(job.Date);
            });
            return jobs;
        }

        var createTab = function(job){
            var tab = {
                StatusCode: job.OnHoldFlag?'ZZ':job.StatusCode,
                title: job.OnHoldFlag?'On Hold':job.StatusCode=='PS'?'Pending Start':'Active',
                jobs: []
            }
            $scope.tabs.push(tab);
            return tab;
        }

        var setUserData = function(resp){
            userData = Utils.copy(resp.User);
            userData.JobStartDateDefault = Utils.date(userData.JobStartDateDefault);
            delete userData.JobDay;
        }
        var result = function(resp){
            if(resp && resp.User){
                setUserData(resp);
                if(resp.User.JobDay){
                    var jobs = cleanJobs(resp.User.JobDay), tab=null, position = $scope.tabs.length==0;
                    jobs.forEach(function(job){
                        if(job.RowTypeCode == 'JOB'){
                            tab = $scope.tabs.find(function(tab){return tab.StatusCode == (job.OnHoldFlag?'ZZ':job.StatusCode)});
                            if(!tab)
                                tab = createTab(job);
                        }
                        tab.jobs.push(job);
                    });
                    Utils.sortOnProperty($scope.tabs, 'StatusCode');
                    if(position && $scope.tabs.length)
                        $timeout(function(){teJobsTabBar.setActiveTab(0)});
                }
            }
            $scope.working = false;
        }

        var getPrompt = function(){
            if($scope.OrgType == 'OW')
                return 'Job #';
            return 'WG Job #';
        }
        var getNumber = function(job){
            if($scope.OrgType == 'OW')
                return job.JobNumber;
            return job.WorkgroupJobNumber;
        };
        $scope.getOrg = function(job){
            if($scope.OrgType == 'OW')
                return job.Workgroup;
            return job.Owner;
        };
        $scope.showJobRow = function(job){
            if(job.RowTypeCode == 'JDC'){
                return job.visible;
            }
            return true;
        };
        $scope.getOtherJobNumber = function(job){
            if(job.WorkgroupJobNumber == job.JobNumber)
                return '';
            if($scope.OrgType == 'OW')
                return ' - '+job.WorkgroupJobNumber;
            return ' - '+job.JobNumber;
        }
        $scope.getJobNumber = function(job){
            if($scope.OrgType == 'OW')
                return job.JobNumber;
            return job.WorkgroupJobNumber;
        }

        $scope.getClass = function(job){
            var cls = '';
            if(job.RowTypeCode == 'JDC'){
                cls = 'te-job-day-row';
                switch(job.StatusCode){
                    case 'AP':
                        cls += ' green';
                    break;
                    case 'RJ':
                        cls += ' red';
                    break;
                    case 'SU':
                        cls += ' blue';
                    break;
                    case 'RV':
                        cls += ' dark-red';
                    break;
                }

                return cls;
            }
        }

        var startJob = function(job, date){
            $scope.loading = true;
            var obj = {job: job.JobID, date: Utils.date(date, 'M/d/yyyy')};
            ApiService.get('teBeginJob', obj).then(startJobResult, startJobResult)
        }
        var startJobResult = function(resp){
            $scope.loading = false;
            if(!resp || !resp.User || resp.message){
                if(resp.message){
                    try{
                        var data = JSON.parse(resp.message);
                        if(data && data.message){
                            Utils.notification.alert(data.message, {title: data.title});
                        }
                    }catch(e){}
                }
                return;
            }
            result(resp);
            if(!Array.isArray(resp.User.JobDay))
                resp.User.JobDay = [resp.User.JobDay];
            var jobId = resp.User.JobDay[0].JobID;
            highlightActiveJob(jobId);
        }
        var highlightActiveJob = function(jobId){
            $timeout(function(){teJobsTabBar.setActiveTab(0)});
            Utils.scrollTo('anchor-tejob-'+jobId, 500);
            for(var t=$scope.tabs.length-1; t>=0; t--){
                var tab = $scope.tabs[t];
                for(var i=0; i<tab.jobs.length; i++){
                    if(tab.StatusCode == 'PR'){
                        if(tab.jobs[i].RowTypeCode == 'JDC'){
                            if(tab.jobs[i].JobID == jobId){
                                tab.jobs[i].visible = true;
                            }else{
                                tab.jobs[i].visible = false;
                            }
                        }
                    }else{
                        if(tab.jobs[i].JobID == jobId){
                            delete tab.jobs[i];
                        }
                    }
                }
                if(!tab.jobs.length)
                    $scope.tabs.splice(t,1);
            };
        }
        $scope.openJobDay = function(jobday){
            if(jobday.RowTypeCode == 'JOB'){
                var jobId = jobday.JobID, tab = $scope.tabs[$scope.tabIndex];
                if(jobday.StatusCode == 'PS'){
                    if(jobday.OriginalScheduleDefinedFlag || jobday.UseDesignFlag!==true || jobday.ContractType == 'BL'){
                        dialogs.startDialog.openDialog(jobday, function(date){
                            startJob(jobday, date);
                        });
                    }else{
                        Utils.notification.alert('This project requires an accepted schedule. Please contact your project manager to start this job.', {title: 'Cannot Begin Job'});
                    } 
                }else{
                    for(var i=0; i<tab.jobs.length; i++){
                        if(tab.jobs[i].RowTypeCode == 'JDC' && tab.jobs[i].JobID == jobId){
                            tab.jobs[i].visible = !tab.jobs[i].visible;
                        }
                    }
                }
            }
        }

        var loadCompletionJob = function(callback){
            $scope.loading = true;
            var obj = {design: $scope.current.job.ActiveJobDesignID};
            ApiService.get('jobCompletion', obj).then(completionJobResult, completionJobResult)
        }
        var completionJobResult = function(resp){
            $scope.loading = false;
            if(resp && resp.JobDesign){
                $scope.current.job.design = cleanCompletionResponse(resp.JobDesign);
                $scope.current.job.design.lastLocationFetch = new Date().getTime();
                $scope.current.job.design.process = {name:'EndDay', date:$scope.current.jobday.Date, crew: $scope.current.jobday.CrewID};
                checkMoveOnHold();
                mainNav.pushPage('complete-work-job.html');
            }else{
                if(resp && resp.locked && resp.message){
                    ons.notification.confirm(resp.message+" Go directly to End of Day?", {
                        title: "Job Locked",
                        buttonLabels: ["No","Yes"],
                        callback: function(buttonIndex){
                            if(buttonIndex == 1){
                                mainNav.pushPage('te-end-day.html');
                            }
                        }
                    });
                }else{
                    ons.notification.alert("Failed to load job data", {title: "Error"});
                }
            }
        };
        var cleanCompletionResponse = function(design){
            design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
            if(!design.Location)
                design.Location = [];
            if(!Array.isArray(design.Location))
                design.Location = [design.Location];
            design.Location.map(cleanCompletionLocation);
            design.PercentComplete = Utils.fixPercent(design.PercentComplete);
            return design;
        };
        var cleanCompletionLocation = function(location){
            location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
            location.PercentComplete = Utils.fixPercent(location.PercentComplete);
            return location;
        };
        $scope.openJobDayCrew = function(jobday, $event){
            $scope.current.jobday = jobday;
            $scope.current.job = $scope.tabs[$scope.tabIndex].jobs.find(function(j){
                return j.JobID == jobday.JobID && j.RowTypeCode == 'JOB';
            });
            if(jobday.StatusCode == 'PE' || jobday.StatusCode == 'BIP'){
                checkMoveOnHold();
                mainNav.pushPage('te-begin-day.html');
                return;
            }
            if(jobday.UseDesignFlag && jobday.StatusCode == 'EPE' && !jobday.BypassCompleteWorkFlag){
                loadCompletionJob();
            }else{
                checkMoveOnHold();
                mainNav.pushPage('te-end-day.html');
            }
            if($event)$event.stopPropagation();
        }

        var removeJobDayCrew = function(jdc){
            $scope.loading = true;
            var obj = {
                action: 'delete',
                crew: jdc.CrewID
            }
            ApiService.post('teDayAction', obj)
            .then(function(resp){removeJobDayCrewResult(jdc)})
            .finally(function(){ $scope.loading = false; });
        }
        var removeJobDayCrewResult = function(jdc){
            var jobs = $scope.tabs[$scope.tabIndex].jobs;
            var index = jobs.findIndex(function(j){ return j.CrewID == jdc.CrewID && j.RowTypeCode == 'JDC' });
            var removeJob = (jobs[index-1].RowTypeCode=='JOB')&&(index==jobs.length-1||jobs[index+1].RowTypeCode == 'JOB');
            jobs.splice(index, 1);
            --index;
            if(removeJob){
                jobs.splice(index, 1);
                --index;
            }
        }

        var checkMoveOnHold = function(){
            if($scope.tabs[$scope.tabIndex].StatusCode != 'ZZ')
                return;
            var jobs = $scope.tabs[$scope.tabIndex].jobs;
            var count = 0;
            for(var i=jobs.length-1; i>=0; i--){
                if(jobs[i].JobID == $scope.current.job.JobID){
                    jobs[i].OnHoldFlag = false;
                    ++count;
                }
                if(jobs[i].RowTypeCode == 'JOB'){
                    var active = $scope.tabs.find(function(t){return t.StatusCode == 'PR'});
                    if(!active){
                        active = createTab(jobs[i]);
                    }
                    var jobId = jobs[i].JobID, spliceJobs = jobs.splice(i, count), args = [active.jobs.length, 0];
                    for(var j=0; j<spliceJobs.length; ++j){
                        args.push(spliceJobs[j]);
                    }
                    Array.prototype.splice.call(active.jobs, args);
                    highlightActiveJob(jobId);
                    break;
                }
            }
        }

        $scope.jobOnLongPress= function(job){
            if(job.RowTypeCode == 'JOB'){
                navigator.vibrate(10);
                $scope.infoDialog.job = job;
                $scope.infoDialog.title = getPrompt()+': '+getNumber(job);
                $rootScope.$emit('JobInfoDialog', {type:'init', job: job, joblist:'JobDay', endpoint: 'teJob', reportField: 'JobDayJobReportsButtonText'});
                Utils.openDialog($scope.infoDialog.dialog);
            }else if(job.RowTypeCode == 'JDC' && job.StatusCode == 'PE'){
                navigator.vibrate(10);
                Utils.notification.confirm('Remove <b>'+Utils.date(job.Date,'M/d/yyyy')+'</b> for <b>'+job.Foreman+'</b>?', {title: $scope.getJobNumber(job)}).then(function(button){
                    if(button){
                        removeJobDayCrew(job);
                    }
                });
            }
        };

        $scope.viewCrew = function(){
            $scope.loading = true;
            var obj = {
                action: 'crew'
            }
            ApiService.get('teDayAction', obj).then(viewCrewResult,viewCrewResult);
        }
        var viewCrewResult = function(resp){
            if(resp && resp.CrewMember){
                if(!Array.isArray(resp.CrewMember)){
                    resp.CrewMember = [resp.CrewMember];
                }
                dialogs.crew.openDialog(resp.CrewMember);
            }
            $scope.loading = false;
        }

        $scope.viewModuleReports = function(){
            $scope.loading = true;
            var obj = {
                action: 'reports',
                view: 'JobDayTop'
            }
            ApiService.get('teDayAction', obj).then(viewReportsResult,viewReportsResult);
        }

        $scope.viewReports = function(jobday){
            $scope.loading = true;
            var obj = {
                action: 'reports',
                view: 'JobDay',
                crew: jobday.CrewID
            }
            ApiService.get('teDayAction', obj).then(viewReportsResult,viewReportsResult);
        }
        var viewReportsResult = function(resp){
            if(resp && resp.Reports){
                if(resp.Reports.length){
                    dialogs.reports.openDialog(resp.Reports);
                }else{
                    Utils.notification.alert('No reports available for this item.');
                }
            }
            $scope.loading = false;
        }

        var addCrewToJobExecute = function(job, data){
            var obj = {
                action: 'add',
                job: job.JobID,
                department: data.department.Name,
                crew: data.crew.ID
            }
            ApiService.post('teJobAction', obj).then(function(resp){addCrewToJobResult(resp,job)},addCrewToJobResult);
            $scope.loading = true;
        }

        var addCrewToJobResult = function(resp, job){
            if(resp && resp.User){
                result(resp);

                for(var i=0; i<$scope.tabs.length; i++){
                    var tab = $scope.tabs[i];
                    if(job.status == tab.title){
                        tab.jobs.forEach(function(j){
                            if(j.JobID == job.JobID){
                                j.visible = true;
                            }else{
                                j.visible = false;
                            }
                        });
                        $timeout(function(){teJobsTabBar.setActiveTab(i)});
                        Utils.scrollTo('anchor-tejob-'+job.JobID, 500);
                        break;
                    }
                    
                }
            }
            $scope.loading = false;
        }
        $scope.addCrewToJob = function(){
            dialogs.addJob.openDialog(function(job, data){
                addCrewToJobExecute(job, data);
                dialogs.addJob.hide();
            });
        }

        ons.createAlertDialog('app/dialog/pending-start-begin-job-dialog.html').then(function(dialog){
            dialog._scope.model = {
                start: '2017-01-01'
            };
            dialog._scope.jobNumber = '';
            dialog.doneFunction = function(){};
            dialog.openDialog = function(job, done){
                var startDate = userData.JobStartDateDefault?userData.JobStartDateDefault:new Date();
                dialog._scope.model.start = Utils.date(startDate, 'yyyy-MM-dd');
                dialog._scope.jobNumber = $scope.getJobNumber(job);
                dialog.doneFunction = done;
                Utils.openDialog(dialog);
            }
            dialog._scope.done = function(start, date){
                if(start && date){
                    var checkDate = Utils.date(date)
                    var limitDate = new Date(Utils.date(new Date(), 'M/d/yyyy'));
                    limitDate.setDate(limitDate.getDate()-userData.JobPriorStartDayLimit);
                    if(checkDate < limitDate){
                        Utils.notification.alert('Cannot start job earlier than <b>'+Utils.date(limitDate, 'M/d/yyyy')+'</b>.',{title:'Bad Date'});
                        return;
                    }
                    dialog.doneFunction(checkDate);
                }
                dialog.hide();
            }
            dialogs.startDialog = dialog;
        });

        ons.createDialog('app/dialog/add-te-job-dialog.html').then(function(dialog){
            var init = function(){
                var elem = angular.element(dialog._element);
                if(elem){
                    var scope = elem.scope();
                    if(scope){
                        console.log("resetting")
                        scope.reset();
                    }
                }
            }
            dialog.openDialog = function(done){
              init();
              dialog._scope.selectItem = done;
              Utils.openDialog(dialog);
            };
            dialogs.addJob = dialog;
        });

        
        //Crew dialog
        ons.createDialog('te-crew-member-dialog').then(function(dialog){
            dialog.openDialog = function(members){
              dialog._scope.CrewMember = members;
              dialog._scope.loading = false;
              Utils.openDialog(dialog);
            };
            dialog.openMemberResult = function(resp){
                if(resp && resp.Reports){
                    if(resp && resp.Reports){
                        if(resp.Reports.length){
                            dialogs.reports.openDialog(resp.Reports);
                        }else{
                            Utils.notification.alert('No reports available for this member.');
                        }
                    }
                }
                dialog._scope.loading = false;
            }
            dialog._scope.openMember = function(member, event){
                // nothing
            }
            dialog._scope.openReports = function(member, $event){
                $event.preventDefault();
                dialog._scope.loading = true;
                var obj = {
                    action: 'crew',
                    reportsFor: member.PersonID
                }
                ApiService.get('teDayAction', obj).then(dialog.openMemberResult,dialog.openMemberResult);
            }
            dialogs.crew = dialog;
        });

        //Reports dialog
        ons.createDialog('app/dialog/reports/reports-dialog.html').then(function(dialog){
            dialog.openDialog = function(reports){
              dialog.setReports(reports);
              dialog._scope.openingDialog = false;
              Utils.openDialog(dialog);
            };
            dialog.setReports = function(reports){
              dialog._scope.reports = reports;
            }
            dialogs.reports = dialog;
        });

        var init = function(){
            $scope.loaded = true;
            teJobsTabBar.on('prechange', function(e){
                $timeout(function(){
                    $scope.tabIndex = e.index;
                })
                
            });
            if(!$scope.tabs.length)$scope.loadJobs();
        };

        var saveStateFields = ['working', 'tabs'];
        $scope = StorageService.state.load('TEDayJobsController', $scope, saveStateFields);
        $scope.$on('$destroy', function() {
            StorageService.state.clear('TEDayJobsController');
            if($scope.infoDialog.dialog.visible){
                $scope.infoDialog.dialog.hide();
            }
            Object.keys(dialogs).forEach(function(d){
              if(dialogs[d].visible){
                dialogs[d].hide();
              }
            });
            $rootScope.$emit('JobInfoDialog', {type:'destroy'});
        });

        $timeout(init);
    }]);

})();
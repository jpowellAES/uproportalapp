(function(){
    'use strict';

    angular.module('app').controller('ManagedContractsController', ['$scope', 'ApiService', function($scope, ApiService) {

        $scope.loaded = false;
        $scope.openingContract = false;
        $scope.working = true;

        var loadContracts = function($done){
            var obj = {
                types: 'WGManagedContract',
                module: $scope.$parent.current.module.key
            };
            ApiService.get('jobs', obj).then(result, result);
        };

        var cleanContracts = function(contracts){
            if(!Array.isArray(contracts))
                contracts = [contracts];
            return contracts.map(function(c){
                return c;
            });
        }

        var result = function(resp){
            if(resp && resp.User && resp.User.WGManagedContract){
                $scope.hasJobs = true;
                $scope.contracts = cleanContracts(resp.User.WGManagedContract);
            }
            $scope.working = false;
        }

        $scope.addJob = function(contract){
            if($scope.openingContract){
                return;
            }
            mainNav.pushPage('app/addjob/add-job.html', {contract: contract});
        }

        $scope.contractOnLongPress = function(contract){
            if($scope.openingContract){
                return;
            }
        };

        loadContracts();

    }]);

})();
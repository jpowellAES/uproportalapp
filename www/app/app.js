(function(){
    'use strict';
    Element.prototype.ripple = function(){
        var ripple = this.getElementsByTagName('ons-ripple');
        if(!ripple.length)
            return;
        var rect = ripple[0].getBoundingClientRect(),
            ev = new MouseEvent('mousedown', {clientX: rect.left, clientY: rect.top+rect.height/2,bubbles: true}),
            ev2 = new MouseEvent('mouseup', {clientX: rect.left, clientY: rect.top+rect.height/2,bubbles: true});
        this.dispatchEvent(ev);
        setTimeout(function(){
           this.dispatchEvent(ev2);
        }, 10);
    };

    document.addEventListener('deviceready', function(){
        if(device.model.match(/iPhone/) || (device.isVirtual && device.platform == 'iOS')){
            Keyboard.shrinkView(true);
            Keyboard.onshow = function(){ window.scrollTo(0,0) }
        }
    })

    Storage.prototype.setObject = function(key, value) {
        this.setItem(key, JSON.stringify(value));
    };

    Storage.prototype.getObject = function(key) {
        var value = this.getItem(key);
        try {
            return JSON.parse(value);
        } catch (e){
            return null;
        }
    };

    HTMLElement.prototype.offset = function(){
        if (!this.getClientRects().length)
        {
          return { top: 0, left: 0 };
        }

        var rect = this.getBoundingClientRect();
        var win = this.ownerDocument.defaultView;
        return (
        {
          top: rect.top + win.pageYOffset,
          left: rect.left + win.pageXOffset
        });   
    }
    HTMLElement.prototype.height = function(){
        var rect = this.getBoundingClientRect();
        return rect.height;   
    }
    
    String.prototype.hashCode = function() {
      var hash = 0, i, chr;
      if (this.length === 0) return hash;
      for (i = 0; i < this.length; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
      }
      return hash;
    };

    Date.prototype.addDays = function(days) { this.setDate(this.getDate() + parseInt(days)); return this; };
    Date.prototype.addHours = function(hours) { var hoursFloat = parseFloat(hours); var hoursInt = Math.floor(hoursFloat); this.setHours(this.getHours() + hoursInt); if(hoursFloat!=hoursInt){this.setMinutes(this.getMinutes()+Math.round(60*(hoursFloat-hoursInt)))} return this; };
    Date.prototype.clearTime = function(){ this.setHours(0); this.setMinutes(0); this.setSeconds(0); this.setMilliseconds(0); }
    HTMLElement.prototype.findParentByTag = function(tag) {
        var el = this, tagName = tag.toUpperCase();
        while ((el = el.parentElement) && el.tagName != tagName);
        return el;
    }

    var updateRowGroup = function(row, groupCells){
        row.classList.add('active-row-group');
        for(var b=0; b<groupCells.boxes.length; ++b){
            var elements = row.querySelectorAll(groupCells.boxes[b]+':not(.ng-hide)'),
            started = false,
            last = null;
            for(var e=0; e<elements.length; ++e){
                elements[e].classList.remove('cell-row-start');
                elements[e].classList.remove('cell-row-end');
                if(b==0) elements[e].classList.add('cell-group-primary');
                if(!started && (b==0||!elements[e].classList.contains('timesheet-blocked'))){
                elements[e].classList.add('cell-row-start');
                started = true;
              }
              if(started){
                if(b==0||!elements[e].classList.contains('timesheet-blocked')){
                  elements[e].classList.add('cell-row-middle');
                  last = elements[e];
                }
                if(e==elements.length-1 && last){
                  last.classList.add('cell-row-end');
                }
                if(groupCells.soft){
                  elements[e].classList.add('soft');
                }
              }
            }
        }
    }
    var updateColumnGroup = function(cell, groupCells){
        for(var b=0; b<groupCells.boxes.length; ++b){
            var box = groupCells.boxes[b], row = cell.findParentByTag('TR'), element, elements = [];
            var currentRow, started = false;
            if(box.dir == 'both'){
                if(element = row.querySelector(box.select)){
                    elements[0] = element;
                    started = true;
                }
            }
            if(box.dir == 'both' || box.dir == 'up'){
                currentRow = row;
                while((currentRow = currentRow.previousElementSibling) && currentRow.tagName == 'TR'){
                    if(element = currentRow.querySelector(box.select)){
                        elements.unshift(element);
                        element.classList.remove('cell-column-start');
                        element.classList.remove('cell-column-end');
                        started = true;
                    }else{
                        if(box.end){
                            if(currentRow.querySelector(box.end)){
                                break;
                            }
                        }else if(started){
                            break;
                        }
                    }
                }
            }
            if(box.dir == 'both' || box.dir == 'down'){
                currentRow = row;
                while((currentRow = currentRow.nextElementSibling) && currentRow.tagName == 'TR'){
                    if(element = currentRow.querySelector(box.select)){
                        elements.push(element);
                        element.classList.remove('cell-column-start');
                        element.classList.remove('cell-column-end');
                        started = true;
                    }else{
                        if(box.end){
                            if(currentRow.querySelector(box.end)){
                                break;
                            }
                        }else if(started){
                            break;
                        }
                    }
                }
            }
            if(box.keep){
                for(var e=elements.length-1; e>=0; --e){
                    if(elements[e].classList.contains(box.keep)) break;
                    elements.splice(e, 1);
                }
            }
            var kept = false;
            for(var e=0; e<elements.length; ++e){
                if(b==0) elements[e].classList.add('cell-group-primary');
                if(box.keep && !kept && !elements[e].classList.contains(box.keep)){
                    elements.splice(e, 1);
                    --e;
                    continue;
                }
                kept = true;
                if(e==0){
                    elements[e].classList.add('cell-column-start');
                }
                elements[e].classList.add('active-column-group');
                if(groupCells.soft){
                    elements[e].classList.add('soft');
                }
                elements[e].classList.add('cell-column-middle');
                if(e==elements.length-1){
                    elements[e].classList.add('cell-column-end');
                }
            }
        }
    }
    var updateTableHighlight = function(element, trigger, groupCells){
        if(typeof groupCells == "string") groupCells = looseJsonParse(groupCells);
        var cell = document.querySelectorAll('td.timesheet-active-cell')
            for(var i=0; i<cell.length; i++){
                cell[i].classList.remove('timesheet-active-cell');
            }
            cell = document.querySelectorAll('td.active-column-group,td.cell-group-primary')
            for(var i=0; i<cell.length; i++){
                cell[i].classList.remove('active-column-group');
                cell[i].classList.remove('cell-group-primary');
            }
        var parent = element[0];
        while(parent.tagName != 'TR'){
            if(parent.tagName == 'TD'){
                parent.classList.add('timesheet-active-cell');
            }
            parent = parent.parentNode;
        }
        var row = document.querySelectorAll('tr.timesheet-active-row');
            for(var i=0; i<row.length; i++){
                row[i].classList.remove('timesheet-active-row');
                row[i].classList.remove('active-row-group');
            }
        if(parent){
            parent.classList.add('timesheet-active-row');
            if(trigger == "focus" && groupCells){
                if(groupCells.format == 'row')updateRowGroup(parent, groupCells);
                if(groupCells.format == 'column')updateColumnGroup(element[0], groupCells)
            }
        }
        var table = document.querySelectorAll('table.timesheet-main');
            for(var i=0; i<table.length; i++){
                table[i].classList.add('focused');
            }
    }
    function looseJsonParse(obj){
        return Function('"use strict";return (' + obj + ')')();
    }

    var app = ons.bootstrap('app', ['ui.select', 'ngSanitize', 'onsen'])

    app.config(['uiSelectConfig','$compileProvider', function(uiSelectConfig, $compileProvider) {
      uiSelectConfig.theme = 'selectize';
      uiSelectConfig.skipFocusser = true;
      //Onsen requires this to work, do not remove debug info
      //$compileProvider.debugInfoEnabled((config.environment||"dev")=="dev");
    }]);

    app.constant('environment', config.environment||"dev");
    app.constant('packageName', config.package||"com.avalanchees.unitspro.dev");
    app.constant('updateTableHighlight', updateTableHighlight);

    var dateTestRegex = /^(\d{4})-(\d{2})-(\d{2})$/;
    app.constant('strToDate', function(str){
      var match, date;
      if (match = str.match(dateTestRegex)) {
        date = match[2]+'/'+match[3]+'/'+match[1]
        if(!isNaN(Date.parse(date))){
          return new Date(date);
        }
      }
    })

    app.constant('States', [
        {Name:"Alabama",Code:"AL"},
        {Name:"Alaska",Code:"AK"},
        {Name:"Arizona",Code:"AZ"},
        {Name:"Arkansas",Code:"AR"},
        {Name:"California",Code:"CA"},
        {Name:"Colorado",Code:"CO"},
        {Name:"Connecticut",Code:"CT"},
        {Name:"Delaware",Code:"DE"},
        {Name:"Florida",Code:"FL"},
        {Name:"Georgia",Code:"GA"},
        {Name:"Hawaii",Code:"HI"},
        {Name:"Idaho",Code:"ID"},
        {Name:"Illinois",Code:"IL"},
        {Name:"Indiana",Code:"IN"},
        {Name:"Iowa",Code:"IA"},
        {Name:"Kansas",Code:"KS"},
        {Name:"Kentucky",Code:"KY"},
        {Name:"Louisiana",Code:"LA"},
        {Name:"Maine",Code:"ME"},
        {Name:"Maryland",Code:"MD"},
        {Name:"Massachusetts",Code:"MA"},
        {Name:"Michigan",Code:"MI"},
        {Name:"Minnesota",Code:"MN"},
        {Name:"Mississippi",Code:"MS"},
        {Name:"Missouri",Code:"MO"},
        {Name:"Montana",Code:"MT"},
        {Name:"Nebraska",Code:"NE"},
        {Name:"Nevada",Code:"NV"},
        {Name:"New Hampshire",Code:"NH"},
        {Name:"New Jersey",Code:"NJ"},
        {Name:"New Mexico",Code:"NM"},
        {Name:"New York",Code:"NY"},
        {Name:"North Carolina",Code:"NC"},
        {Name:"North Dakota",Code:"ND"},
        {Name:"Ohio",Code:"OH"},
        {Name:"Oklahoma",Code:"OK"},
        {Name:"Oregon",Code:"OR"},
        {Name:"Pennsylvania",Code:"PA"},
        {Name:"Rhode Island",Code:"RI"},
        {Name:"South Carolina",Code:"SC"},
        {Name:"South Dakota",Code:"SD"},
        {Name:"Tennessee",Code:"TN"},
        {Name:"Texas",Code:"TX"},
        {Name:"Utah",Code:"UT"},
        {Name:"Vermont",Code:"VT"},
        {Name:"Virginia",Code:"VA"},
        {Name:"Washington",Code:"WA"},
        {Name:"West Virginia",Code:"WV"},
        {Name:"Wisconsin",Code:"WI"},
        {Name:"Wyoming",Code:"WY"},
        {Name:"District of Columbia",Code:"DC"}
    ]);
    
    app.constant('urls', (function() {
      var apiUrl = 'https://portal.unitspro.com/api/';
      return {
        base: apiUrl,
        media: apiUrl + 'media',
        upload: apiUrl + 'upload/{route}',
        logout: apiUrl + 'account/logout',
        login: apiUrl + 'account/login',
        heartbeat: apiUrl + 'heartbeat',
        resetPassword: apiUrl + 'account/resetpassword',
        signUp: apiUrl + 'account/signup',
        profile: apiUrl + 'user/profile/me',
        specdocs: apiUrl + 'specdocs/{action}',
        orgdocs: apiUrl + 'orgdocs',
        jobs: apiUrl + 'user/jobs',
        jobInspection: apiUrl + 'inspect/job',
        locationInspection: apiUrl + 'inspect/location',
        locationDetailInspection: apiUrl + 'inspect/detail',
        inspectLocation: apiUrl + 'inspect/location/{action}',
        inspectLocationDetail: apiUrl + 'inspect/detail/{action}',
        jobCompletion: apiUrl + 'complete/job',
        locationCompletion: apiUrl + 'complete/location',
        locationSearch: apiUrl + 'complete/location/search',
        locationDetailCompletion: apiUrl + 'complete/detail',
        completionFieldComplete: apiUrl + 'complete/field-complete',
        completeLocationDetail: apiUrl + 'complete/detail/{action}',
        completionSpecActions: apiUrl + 'complete/spec-actions',
        completePhoto: apiUrl + 'complete/photo',
        completionBegin: apiUrl + 'complete/begin-job',
        completionClose: apiUrl + 'complete/close',
        teJob: apiUrl + 'te/job',
        teJobAction: apiUrl + 'te/job/{action}',
        teDayAction: apiUrl + 'te/day/{action}',
        teBeginDay: apiUrl + 'te/begin',
        teBeginJob: apiUrl + 'te/begin/job',
        teBeginEquip: apiUrl + 'te/begin/equipment/{action}',
        teBeginMember: apiUrl + 'te/begin/member/{action}',
        teBeginDayFinish: apiUrl + 'te/begin/finish',
        teEndDay: apiUrl + 'te/end',
        teEndAction: apiUrl + 'te/end/{action}',
        teEndMember: apiUrl + 'te/end/member/{action}',
        teEndEquip: apiUrl + 'te/end/equipment/{action}',
        rentalEquip: apiUrl + 'dialog/rental-equip/{action}',
        postalSearch: apiUrl + 'dialog/postal',
        addJob: apiUrl + 'addjob/{action}',
        designJob: apiUrl + 'design/job',
        designAction: apiUrl + 'design/{action}',
        designLocation: apiUrl + 'design/location',
        designLocationAction: apiUrl + 'design/location/{action}',
        designLocationDetail: apiUrl + 'design/detail',
        designLocationDetailAction: apiUrl + 'design/detail/{action}',
        designSpecActions: apiUrl + 'design/spec-actions',
        fte: apiUrl + 'fte/{route}',
        gt: apiUrl + 'timesheet/{route}',
        receipts: apiUrl + 'receipts/{route}',
        receiptsJobSearch: apiUrl + 'receipts/search',
        vendors: apiUrl + 'dialog/vendors/{action}',
        dispatch: apiUrl + 'dispatch/{action}',
        geocodeORS: 'https://api.openrouteservice.org/geocode/search?api_key='+config.openRouteServiceKey,
        geocodeGM: 'https://maps.googleapis.com/maps/api/geocode/json?key='+config.googleMapsKey,
        hotels: apiUrl + 'hotels/{route}',
        showup: apiUrl + 'showup/{route}',
        daily: apiUrl + 'dailyreport/{route}',
        log: apiUrl + 'log/{route}'
      }
    })());

    app.constant('pinVariants', {
        pin: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAGeCAYAAAAufzwLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAC0swAAtLMBr763ewAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAAALccHJWV3ic7ZpvUuMwDMWldNlhGA6xV+AmewU+bPnKdXoZDsD0LnuDLXWihPyxHTt5sqBr0c7ATJvf07Ms2wnv/97+0iu9Xi6X8/l8Op2OxyPVqFGjxg0Hs3szN4cfdz/vHx6t9dxGsLWAryChKrAXYK6Aq4AqwFiANf+/F8DWAqz5VwMatWtzIxH/lA7dcbmPhoPrDasYMMCpfbW/+fEaBrDQqfvpKEEFaAOYZnh5+QXgK7DLnkb4XoPfgaAzG8PhSV6D/0Ly85EGyNjTxAB5+Q3A8sV8j/8UFADkf+KnBvRvnwCkAYvB5wnWi6d4h8zFD3QeJx7ZcQPzH+OX/gdUgPnkKb8gG8vnZp59j4+2GLc+QaLhmfXsGf8Fi7X4s+IPhFsBQAPQeCov1vpEAIrvCmnc8pLoRPj8p5Uf670iG8pfph8PLH866YbKj48/qP7b8Z+veqvXxvH7nr9gx66Ps1/2Uv62E/kScP1bVF3C7APyhwEQdMq4unvcYH7y2AsfuAHr97jpFQ3F9xU4aFjNHswf7zfSPo8c/u6CaZkPeGz+eQ7ssj+8oqXXH9z+QUEqHm1/loJdkx+wal3xGvbLxdc/4I7LioDVC2imn4DfW307Ddg/+XYJ0Jt86XxDOqT69syeG0h/hwGo9DcKYNve4+56wW67bPkOLv1NA4B1P1cCQ1vfBgNMi4/hrSfTAfzcyxHQdj7rdQ91x6O/ZCYeXnwZAnTcTxag1PhzTjw6Uz9JASsd+DIOXAdw7Wco0J36KectLffTwuEVO9/ak2RlPK3c6C3Q91eec8H7fo6AMjve6GOWArUf4rsNd5Gp5xfQ7ve1Bz8soMWX6jz+Z9zlGt/yBmzhOx1efMkdJ8/+Kjj4QhwrMDhqjx9DmZz0P/PXX/RCArqnIFaHHXkI58w34zs82+BJ/u/RKHsRUHrizwTY4mvUqFGjRo0aNb5z/KZneqE/9IuerKXUMIgP3/ITFGpRKAgAAABIbWtCRvreyv4AAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACmkM6EAAEgbbWtUU3ic7X1Zd9xGki6mu21Zshare07Pw7zwnDv33CdXY18eWSwWyTaX6ipSJvWiU6vFsSx5KIrdHh789xsRmQkgs7AlWMXNICWiCksC+CIy4ovISODgh+7V9Zvh6Px6HL8ZHpxfW3F/uJVZDP9xCIupHdr+bBaf7m8trs34jC1+3Ostrh3TjHf3jhfXruXGw+3R4tqyAq9jhqYdD0cn57BfdweaWtBPPNjfv7ruDuDP1ubx52vjG2NiTI13RtcYG5+Nc2Ma7x0ewPrnsP4jrN+F9RfGzNgw9mHrR2MeD3tHE2x085Da3oRrd+Zh3O3tnV+7cfcAbmIOC7ql7mibdhr18V66o1361t1nix9osXXIG9ju0/fhMe3b79K3/pAWh2zlaAD7TuPuMdt4zFo/HrGTHLD22GJvE6/yEK/KjHtHFl5O78jGZnpHDi36sNKGhc0WDi7iGti8WsJmaHwyvsC6mTE3ZjdFyHr4CAntGQEuC+PyJtpjLdzG+mPdEB1rvfqTonND/VEx0tGgO8boOcdoE/C5ACS68PcLoPWeY/UNxyrFsAwdvO4MPL7L8KHtlfiEroSPKeHj2DJCsxv2MZshZDOEXIaQyxBy49HgLZPqaAQfphNYccRuYzQ6ohU6GH7LMTwG7foX6NsX2F6lZ46dp2jlQFoRh9KeTDWgnIYMSlq/NjDtqB6YLzmYW6BwH+D33PgJ4Bobvxrvjd84oE8zSvkLfP5kfCoF0+K91nJqm33LdPO7rVnSbT2TIUn2AJFcjGv3XDusjaUT2gxLx5ppY1feoYMZgy6cMuTmKyIUZpm7tBsau3qQNQXoH9BPz3EvCSDXYwBZE0W3Fhwik2E0LeuoqBbVukUGMYMTWlPEiZRqDUAt61bSUZt20CFsnVAH/ViqZla0Wj1brVNdj559zTH6EXzBZS46gaJiCuko42V4qISPfef4jAZdZv1H3SWj/22C13vi9FPQGEBO0aYt8pto8n+rZe45XpY9zUVMMfjBVF+lXJ9BNuaYOTO3tu9s3inhhLY3ZkCiq9FHckDG/xLQ/KCF5MKt4TkXwnMin6gJZGLbOJDESVaPI8I3EmwEjRujJeIDIst4ifgw1Ib4iRxc6MBLPqMynCB11sZ3zpkJHowAR/6qAR4CrvZ8zjBTnUY5Zk0Icr5RtL1Sgjy19ft4Qo89m0HHMKyHnTWvbRoTxbMXEnRPE+jQg/xGvrRJbCaHZhQiNMx91OnIdshZCgUlNdFy7Dy0fIaWz9DyWVdmBhA/TBZKV0YfM9zVwPFJwlbGxs8VOZKQoRgxFImiZVA0b4qiy1BkKpYLozdmONolOHomTwNEPA8QcSS54vlc83y3CEy2JgMmkE49pTyGng4qafys04traWU99pyvltSZ0VFPVm7/yMFQHxZADrOehvmeavXsUWd+T/ZPTldlttTu9LlZhHuQrrIapquKUfouF6UeJfYmQMHLu/V9xcm+JZyOoR+fQ3jyUHFyVo7T0wSnT8BILm8xMVxmx6Kp2zBI4y7BZNCYDBqTQWMyaMya0LzMVSE+GqOvPrKtN29/9KXMGHkMIY8h5N3IZO9S9HpVEb3e1xEGjhJ4bILJYzB5DKYxg2nMYBrncv8+hps03HKMtiZHmS6Afm0YB/zT3LioQ8V0qIPlzesMNpi5Qb52l8snDpzOhgyqkJF/NtZQnBgpx26XEkzveaLpPVkrGTtkjZS2xAiIpGlKRJbFTUtENrczki7WzYiP14pdfahEj9xPICpWNDX/Ns3rkZi1LjRbaYzeIJm0Nj3L8vo0k4RMlTJJ4VwDzycSzf+42jH3mgNZimu8hW6agIU4ZsH6moN1hEMGHKJnSXiO3ArTbVXsaty0OAGjmSqYHCW0FKGQSLWRQi8FllaNdAauJYbFwbJDFvtQpLNJARspn++zyMfFJaUTKzF8ncEQM0ZY7DEinjrmrKzcR4xzE2w1IMVMVsnQzHTMQLU5qFNbya/NeI6IOe6KCBNUl8EaclxDBuw0ZMBOObDTUI3X8cNQfJA6ONuUfBAJpsGQx6TDoUhxjpbTdXmiEEm6W4HfmdWCX0nRJeiXjrsW5Epczvtchj0QJMKelg4tCWmGYi7S5MQRYFL0IVP8uvA+S+C9pJwKFqN8rsN/xKC2KLawxvMa+NbhP55iMWTLivZ2RBVzxeD6rh64AjhrkeRICEC72lL8JT+HzMsx6tnfoKH9reWmeGpvMVZqBDic7kQyv26e+RVwqnbC43GcxwM5WMpFFywLNWSAjobcNPPvCLS1yDXJIsIZAJQf4dOyulrG97D1HCCvHEuSx8kbZ5/LYj8NDiAratg0cbCMzH/kIQNquAlrL+H79/AJiTpGhpWDHKvFbLW8aYWYCTe/BU7+A/wXXTe7Z144Q2FLIcUc6ycWNAhmfRdTCyjqp2j/ln0LMk/cMmLLepAKd72TFB6co8NeT/nwtD5N16o2U9gnN39jBuSYARkuEXVOPbPVZhKgWQBfcAB/JEYz50OTpIPKgEa97I1St5fE1fUSODjEqKmNCechynSjYclcJJMx83SgjXIV4sPwKOO2TeZNFCWtj3G9QSM7zNNTlgBaIcJizMgR475efYDzY0onb8yIenZR39eDUScoX3D2uJDZ42JcA8Q64eZNE0D5KmrnIcii8mwUlAy2qRWSwl4OOcP+ZPyi2Et0QR+NBRZlobvOw9DyXIahL2M41oDQz9XDXFLDhn6bsBqhhbLBtLka0tKhJeu9Pu++9UETCscGScrDFSev0yplkvnexazPrsneLmM1casHzZNYRRk0RwNLeLEPDvvA+HU45/waPwwEhiMR+g2FjWRJj3qYirrAPoYweYhOXCm4FtqnE12X+mvCMK8YIT/+45g6eZhOw/yAJeLZtYgXdUQ+yxRRWCLGyLtpmCLGzJXseDGIaTT9PwDjmAhluXp6TbmPhkdhMV8m96ZRhQXxYa564vo+X99n6xMoyVsH3FkHTEMFpKSguyw01OvsZ4RnuXeRPXStukEdLsk1c5I7GKpE0rnepSgxUVQhI1dgclWkiLlgLsgJQPWRHPUXIuCCln/FQXSMXoPytqDOPJo6IY0jUxwZQFCVnFREvbSZHebkgneZj9bB6VmKE8SBO0Rz/lmOWK6DqTf1SEBmVevcNNfBWPM8Y2jdKmTPEwbznoqkMe0wXyq9V2Hzm445WPVHAZNYxfH1iwIFEayK+nYFdd5dps5V0L1OfO4HmvWhO8yQP0J4c9WjevLMOMM4l00L5auTiVU9ca7yCUYzTAYLlglM3Q58BOsvKeFVNf10FR3Yq5+AVTCMqp2GKE9V6PSKuq/QQazNP6dc4dp1cKmS0slVwiiUB7sWuV4jf/xQKfCd5pvAPD6opnHSYS7OYrJ5bKaucIOBx9yyr6etL5LI+ROFMu8BeD5FolxnV1cQV+anw9wpcpgHySBu64zAFGkt4zej5SCwCsGXGQT/RUmcDcpUaGNIiUGNya4Jjm51KN14fABj5RpOiJIQ0yCnIJ2KfGiF+CCGXwacTOItsgHZDLtURw6rpPBXLoU3NOI1perXzzT4gBYYc8QbqYHRNydsKlVdxc61JrJiK2yKUm3LtkSLgTo8R+Tw1DosyZZw8WT924ALR0pmkrQyQ2WhPFKGfD/u7/eurvvZJw0sSCwjSsidZ4pIFySOQxrT+IUEdVK4hYujzwDpM9vQZzj0t0mL+8Me7TIcsm27bHGKi7ifjeLYBfHHPmDEplxSdstJ4ZZml2SzS4LFTnJFr+F6psl4xIxr42VmyOdzYounvOYDXd/U+BnsiBi96O+8AeAPt1jje/B5Z4CPYumzR6yY9BNnNlliE3/+Cm47w23mzduxGjYhNsF3gi6WRPctF90WPU1gCh34Q474hhzEZY3KbmkmPoeJz2nF10B8L7n4hgDQFG4asyg/KUJ8mYgqb5+TGvs0E+yYCXbcCraBYJ8l/RIHCJDgZIOdRWbwQGw7KdnWTIAuE6DbCvAGPZMJ4pK40YWATemZ+fuc1NjnRibXslrJNpBsSr/GVGKTzkVc8FEAsf6kYH0zqXlMal4rtBsIbUB0c5p54tSC50HE+pOC9c2EFjChBa3QbiC0PgEzS2ARwknXnxSsbya0kAktbIXWQGgvuNC2+bNofiWjl+UvL7iY8vY4qdyjmUgjJtKoFWkDkT7hIu3SWOznZIR2kTwQ4iLpg+raZuKaMnFNW3E1ENfTJCjEnsOmWavxfLpFjefTLc1EN2Oim7Wiu4HH+5GKP+dLHi9df1KwvpnQ5kxo81ZoN4jVB+nYWRIUPEt4ZHbbScm2ZgJcMAEupAt7nmjT3JgYPZLIexraE2P4QnvU7ScV25tdpMWzx7jsWRlg+z1b+uZI31zp2zETwA4lxW+irYmOpnpbpa1ClcpVpFpb67WzOm1thNNLjhNumSD1okHiLhUtZPu1n395Tijfodmxw3SrHY0DeavrpVsnvnqsX3ZoUHKopV6RKqCHd/l3rBevuF4MadrMDlU74r7LmuHk6756gWYnAwD+L7z6PPmt6yT3BGUcW/1EtVWXxhGfq/lTtV+1I3PsRQX+MJjWtlT12rljrH6H/KMRTq8zOEFPNc6oao+KHat6bye9lolpj8cFXWsxW8BmeWOkHlnag1d8ojtG/DuO+DZVSFLtBJX7JVazyo/apjNVDVnqiKxJFFiTIkdkzd0FfMt3RP54PjWtIke03LClXlEtP3qvL/+e6IbojcyfZvpklSc1I+ALdlEHWfKAWYOUK8F1neSOcf6W44zljWx0G1+Y8EtVz8ujEHZ2o8rx3PS67XEJAVTJh9kJM6AVMRNxObW63f299jvWhWdcFzKPpIZtlZrg+1jhmW/EQjNchjPpIb6vRnoZPPHAQNkYeFlA5dNmqRf8q60M9/jy71gfnmZi3F/JPlxktMHNu6gocH1LoSVOcmeTWThVfFaYbPWn9tzKh2S+mE2ms2WJ3s0l3JNeyjziMjMtyKkUXxb3YSq4N2jnnujtiF4acqXoba4ZiJwIrr/IDLg2/haZgYk3cSZWgRnw/WX7kpqB2Rx/c7FA0xPWdWn3+vJXmFfe7veurrf7meHUOenEHtVgYwzRhb9X9AAcMQ43T6YTjY3LeHswurrubW3jnx9IV/iENoiKcbLRpfE+7m29gS3/ZsAppT2Tto1j4ze0hwV7fgt7fqGkNVYljahi8LJw76ewxweysPOKdp9lrkC92j8Y1tJV7JL+syxu0kv4/k+M/2Ms0l/lyG1ay/jgAT3tbc/o8SP/r3FtBLTVh3PCWQ3b+B4+T2ENfsJ1M3rkTgjrAthi0q9Hewbw14It+C1W7i59IOQm5XE+JEj8EY5xDE+5yhHNrpuR9PLuz0x/lTPhkXjUkTEx/pvdJz/uK7gzjEM/w7mzx7wATGaYW6InOtAcZmoDItYCab2SUNyD/dlMgHP+DGx21J8441bPliCQvBrvI53zc3I2RzkiRa/Hn7E1puKcLIYqEq8yundAFa2X/NnT52Q1xbks5VypHorKuwvGGRNtDBRpPadHfH42fi68H/XK2Gw1qU/nSnkGreVJ+SU9CuGfPPeKVmC2dPxTWUtAxxbKne5S/ylvIdOPllp4RdkNfO0S3CtYoTH1k6rrUO/ldWKlmDW5KJSyiuP/g7P/DOfvky2Y08jOBbcJR3DkB7A47Eltv4DcP5EcL2BdVndPYP9DNo+Vn+XbjMXdyNhcMtENrPOQCsl+emDW2Wutc2udW+vcWudHap2fJtb5C50P+8XDstB+a6FbC91a6NZCP3ILPYK2+dMSHpiFtloL3Vro1kK3FvqRWuhn3EK/Jd19C+f4ybAfmI12Wxvd2ujWRrc2+pHaaMGiMzb6gVlop7XQrYVuLXRroR+phf7zcp6D70/VPQZ7J/DDstl2a7Nbm93a7NZmP2ibnSPJW628s9ZkndvKu9Y6t9a5tc73zzqn0lmFdV5v5d26rHNbedda59Y6t9b5sVrn26u8W5eFbivvWgvdWujWQj92C73+yrt1Wei28q610K2Fbi30Y7XQt1l5ty4b3VbetTa6tdGtjX6sNvr2Ku/WZaHbyrvWQrcWurXQj9VC32Xl3bpsdlt519rs1ma3Nvth2+we7IU6lumvSm0H14kl61xticagaZHhwu8M7jJciSUqtyyqrHxlBPEm/f6ldOZ8CaOlV/1C9ij2nM2074Yl97ZsZx1oXa1ZyR5RXn24Lm0TGrQh6ZCutr3i2pa+X+CdtNdD1L6xogvr1r5/MwIN3XN/J7r3kute1tar/PQbQ1RKjAnZddVJ2Armt19nvHwNLRNtmejDYaKqRW2Z6P1loi9Se2rgG1ky93gDG41ZA2xxXXNBnNZGtza6tdGtjV6JjbYUfbhvNvp5ak9LLfQrqY9u0LWyt2x9kKK4EeFxTtuyR3Twt0EUN4EYzoStEdmZOdknlzRF2CeM7MbwuwCbJOIu3DuE7wvQ3hnsL9un/4QzdQGrBSHJNPMdYHZB2on95Z/w/TLBGXv6/yaIfUVn3sC/UqvfGLOaOYGvjLFiFf8A1+gp+0wr9FPtJevRjyqJNtGSZ9LcULFNx5ursWuVFtn0dtUQtAFkAT37e9IO1BehRbhuQno0S/puSH5xQfqGPTleoVeotru+tt11G9rdh8In8/WmiQY+l1q6WZ7J47xoQYwIeVMAvy7s31y3dPNM3so0s80zFfnIPG2Rde853M8MWOgXwmwjIzemdV9nRzwbaNoMtMsDjUEbFhEvR42bgSapXD1MNAM1EnVxBv+RFUW34gvXIwcZPz3sv4XtXyiK2wB+Lazh50ZSMAHROeyDSDMpWHCvbo4U7DuVwkvY7xP2RWKq77gtfwfffzImhbGOfMx7HovIR/2RNFHup09hO0rrF/irnsMq3XOeibzU+QTZPScGe0tZ0ch+dt+fDPH21/y9Xxfep3pk3r1+V4Ds8lnN0uPSs8r3lnfOdeXQ8/uFzDxmxDQuJR2aF8bLxXpXfMyLzDmyOpcekYfJy9yjqrF8mXNHVbIrusLyXvEq96g6OpaPerOeW47ii5wzqX2t/FwlR2lZ6aewBlu9outpGjM6YGvRxk6IZ83JOkac96eMDLfjfZsUV5rcp6I/9ch/3oaFfp5qArX2yTinWOBzvDMA0HYGx1fXp/tb+Ka7M7aI03W257G1+EHlj1wSK231GdO/lbeJ2eFVtrkeS5mnm7ranbGwjbVb5h92wj+8ln+0/OPB8Y+8PtFyj5Z73D73eG3sksf8G0T4OMb3hTQYbR7m51bDRWYJF7HvIRd5T33wXZp5Vo7LH097BscxpjGm6u4v6MMLZIdWbxnbOkd+RzloNirG8561jluP1aqjK3ra91eQ6MXSHf5NzQ6vSA+niR5a904PX5H+Ze+5nmZNcvC7Ow3Rk6eerjxL18BWdt0fcsZP0rFOxGBWaFHrcM4ZZR7HnEHiiJtN9ZVyjhs1aixVCdA4KlVgzlc4fvIEJPMrjcCinH5L/MvyuPhXhfUSeSMe6xq3yJOXLPOvoH2cFTPPSLlP8RCrnGEVC03ylQuq3TBBNigJ7O8LGh9NxydMihdQQncbL6wL/Twcq9D/jvdUMT9pg+fINuEqfsURgAaSsEkGNvWlMTGAKSxdGsHORm4ejQ3Vq7V5SJKoxlSWyp+SsSQmk/R7k15gwbYFcUY3ydqL7NB9iprXg32KXTnGL6lmDysZkOttiK03yFYg7g6N67mk8za1jzrvEv/wyJsguigdD7ZFxFZQEgtCfnoruP+FkBR3LuIJbPl/YDk2Pki+5g+oJdLxfy48fpmD/AHu8DY8T7k0yzXhabL3Bkn2IqcqXrff2b+7fpeHYlUPxP1ELFHVA/+ddCuNpFKtK65G0mV/HsggJA/kQ/s+MYgFH3d2SZoL6sMW9VWPKrawphSzDCHtMV5if+uR4etCNJYrrqJb6oFl0izXhOfGWwNnS/+yAjsc8XlQKDU/ifVs6oc4R2pCMkQp+8TzJyS/BTF/m/g9spDbkOF3sA+7a10b+jr3yHrWe12VI0USLJf8d8YOtPOFYsZz0t1qO/CTckQWA+GZviTVYn8GKXcoKij+9bWjxOzIhJmMTDi/E1tfLTVZ6k94LdEFVW1/TObdymv1+/qU0MUYD+seWcTH6veXI77gEcpBRbAO6s/ltTfiPFjXZ9N/j9dpRWR7g6V+ICoCHxf+xVjWkcQraPEjzclgWzaSutd8y/dX8grp/u9oBO4zzSC+rDnDQ5fHuveYx/6lBI88L+opWJQdvwo/sq469XKtUfM9h1RtgGMZov9vEtfZSLc05lpzkrpJ1b0TyrNNSR8c4lpT7hHxr0ezoUQtOjKsOfE0jIVvh2uN6a7fAXbirutqSd6RX/hSnbtQNHa0LvtTJEvV/rBZh3JtuHjuxS5d8acbzEsom2WoVkW3b5z6Pc4zVEe+1jnPEGMedez9fs009JUzVc94Uesdfs8zDW8yeye8Bav8p4IZO/lWWDzT85DaRwaSN8JZbInV2rDWEreWuLXE9S2x3tzD5TmurS1uaotvIy9ZZIu/hjv7QNx/BrIVdXF4pay1C0IREdiQ9mw2l21OMyV9iHamZIVw1C+tKAkpRsKRhMjIzuvG/wva93bGFNZVQVaNqmwxp8mW5fgK+5GIth3leRJPqd/+VnIUXgUsa+jC1zeU+oJywy7Pek352EOUmTvHZvOboBd3O5t/XTMY7498/0z1ML9xrWPzpH+Dzy7HHeeDbSds7IDuj7zfDWoBQvJic5Imy0uHNKaYzUv7VP/i0Lgh/mXfcTmjdQ9X+nkoNpdJ1o/jOAPzUHclm+iB2+MyNJvL6JUy+ntMV4PXfXd9yHvQFrQKUVlWf6HasnODjUKM4HrO+SdkyRjZZaX1TVofuGb5BCCVgCqiAqqMwr8+cSGPPOLDlc8yhrJEviXs51S5i7GaqIYVVfADiswuqf+9N9izPTE2uqL+lj13E/6B4zVzijsXFMXiyNycjhDSGRP/CKivmPw5CjZnKxFsweqJJnXMRbHpn4gHTjOxl5oLWFd9ug7S+HMwAiHGb+nvYPP46rq7tX9+veA/cZ99M+kn7g8yEl9QfmcL2sY5O1+o56XPxMhUqMUnOeuGvaPJNbR43D3HxXafFqOD82sbvh2fX1txf9ijXYZDtm33/NqBxSktdpIreQ1nZ5kmMY41p+g3rc7+nPjWqSE/jfhHmtOEmaD+zpur6/7hFp1/Zw8+7wxwbmp/ZwsBmE3xN85u6kSRCz9iDw4Y7nKGu5gra84qa4lLpqQlsQd8Jzzh5/i0e3XNMPmK4wYWFHThh6vrHwcAcGjGu3x5PHoLwoAGjvcA8uO93vl1sJi5C95QfzUNxdunA7j+g2O8za39IS4G+6QGg03Szf1D1IIBbsJGBsf8O6iRFW8O9tlihHe4ublF3zZ7tBhBM3PYs4cH7GCjZvz3wT/Orz1cjtjXI7YY4PE7/T1c/H2E+4xhuc2+HmNzfx91SSv3B6SVh3hxO6N9XLc/OsFFjy32RwT31ugAD9veGuHNHJ6N8Nv+iL7tHh9gI7vHrEf1yPZjf/4nLamiOj7t076nB3T9x0NqDo7ExWlvkxrvn0IDRnx44F5dw5/zaz+mxYItLLYwlQUs+7g/9D0vpgX4ksORydoaWXxp86VDy21URSs+3tynyxn8iItTvBEr3uqe0D5bXeqyW91NWtvbpG+9g6vr/f4xaqgXHx8N2IfhHl/TPeIf4q1Tgjg+OITLOzjsUZvxYOeQrMjAGJMn2gDPt3dAAhvs7bMF7vpfFE2xmU7ogz2KRyfU47/ndYsR5UKxIg7zow557IDmSLEaZbQjDkgJrjje22fCPQNJ729C3+7+sIMrToakc/s8lPoRDp6QsR0TjbyI9/cJooMR7XewRc309kgBtvbRfm1jk1s/4PrtfTxXHL/Zg3t+w3aK46Xzmfx8mHhmJgwnfrJh3Zl0RrPWGQfHu8mK1vy35v+uzH93OCBbf8xEf3RMGppNW50A1TkBmrkN1E9oWXbdQ3zEm4/pmxUNUKz6EW95w1eP6yFvD0VjsgMZ41t+9ck6Hwq4PIB4v/UlHh720ITZlhWZNjCY08X195YPH84WQLgGW330s4MRrLZM+D46ww9hPNjGNSEQ2m1YY5tuPOiLffqwxgniQRcs3aALFHhvRNy1O9pHKz8YgA2H/bvi+ThBxB6PYwF3PRvghzDq2JYd2E58NsLvltkJwsAOgnhr+AaboquOXNjhcNTDHayO58At2HH3LZy1+5b4XHfzLfGP9FR0Cn7O04Hc9OlIPrVyKls5lVNxKtu21NtKz3VWfi5L81zJbeFJTxUIT8shNGudC862heJEtsTFSXIMcZknyMFwBB/iN9so845le3HvhEhPKrqjQ2pHaqBjRrYTurwd1+84ru9YTnVzrtKO4/ueb4t27E7gml7oVbfjKe2EVmj64r6cqGMBiKFf3Y6fbSc0Oy5coivuy/E6fmSGYVDdTiC1Y3Vsx7Pt5Hrsju15IXyvbCeU2rE7VmRFQcjbscNOGPoeCrOqnUhqx+k4Zgh6JtrxOl4Y+FYNfMZSO27HCyLXT67HBvk5ThhWtzOR2vE7pmnZUcDbsbAThKBC1e1MpXaCjudHKc6W37FghV9DXjOpHTh/EIamsASWA/rjBGYNnOfZdiILd/F8T7RjAu6OadaQ+0JqB85veqYr7suE+4Qe71Xj45hSOx4Zl0jcl+l24LbIC1S1I/X3KIB+4TqOL9oBffYt06rG2bGldqJOEHnQNVg76dfKZpxsM5nzn8mXV9mOK7eT4HEmw1XZjie3k8jnTBZfZTuS+cnoy5msTpXtBHI7if6eyepd2U4ot5P0pzO5u1W2E8ntJP37TO7+le1I5idjb85kc1TZzkRuJ7F/Z7J5rGxnKreT2OMz2VxXtiOZn4x/OJPdR2U7c7mdxF+dye6ssp2F3E7iP89k91rVjiuZn4w/F+1wd1/ZjiW3k/AL0Q6nH5XtSOYnyyorDbKdw3ssz+n4nuMLgOtfiC0bMC8AgUeupQ2MzJ8s3wTJ2IHl6gpK5k+W70IPiCwBcH3FkfmT5QcgcLgtX1eRZf5kBWD5LMc1Hd2OJfMnK3A7rpUS1fodXeZPVhB0fJCQIFD1DY/Mn0A2wO4B7UDXEMr8CWxpJwRf7zi6hlnmTxZcAFyOHUW6jkLmTxjFgJuIBNGo77hk/gSBYccF/fVsXUcq8ycLPDncVhD52o5dNmDALAInjEJHm2hIdsM2zQ6wjCBwtImPLbdjdzzA1RcBigYRc+SGXAozzQbM0JUbAg2K3MgTIYEGVfXkhsCXgu1w9LmzTKJsjKAxTaJP5mUWBRrTAX0IgqSh2tGFTKNsuIIoCgNhzTTCHZlH2ZbXcW3Tc5I4rnb8JRMpG27Fj8AaiSuqHxDKTMq2gk5ohmYSgdWPUGUqBXh0ArCLCdj1Q2aZS9kArucFnnBBGjG8TKZsG/YJrchNGqqdVJDZFDYEQkKl0M1yyHQKG4rMwLeF1OqnXWQ+JaW8Ki2rlUOEli4lJUY1iFUZOCkz0qNUS+JKqZEep1pSoJQb6ZGqJZVOyZEeq1rqZCk70qNVS90+pUd6vGrJEKX8SI9YLZnGlCDpMaslY50yJD1qteQ+Uoqkx62WHFrKkfTI1ZKLTUmSHrtacvopS9KjV0s0JKVJevxK5UWZ73oESyVqmSvUY1gqc8xgppmjUqhsRoqaSSqFW2f0SjNLpZD9jKZrpqmU6CPT9zTzVEo4lLEGmokqJT7L2CfNTJUSMGYspmaqSolgMzZcM1elhNQZr6KZrFJi/Iyf08xWKUmHjOfVTFcpWZAMF9DMVylpmQw70UxYZcfnKm2imZchUlJVGpdiKw3JuTMNcGSGpSbzNMSlJK2U7KKGAilZKyXdqaHSStpKyb9qdDIlb6UkhDW6vZK4UjLUGoZIyVwpKXMN06ikrpQcvoaxVnJXyqCChvtQklfKKIeGQ1OyV8qwi4aLVdJXyjiQhtNX8lfKwJQGDVEGAOWBMh1eJA8AKgN3GkRNHgBUBhLrM0dlAFAZ2KxPZWV2pQ601ufWMrlSB37rk32ZW6kD0fWjD5laqQPj9cMhmVmpA/X14zOZWKmFA/UDRplXqYUM9SNYmVaphRX1Q2qZVamFHvVjfJlUqYUn9ZMOMqdSC2HqZ0FkSqUW5tRPy8iMqmGFUBwfdQ9pmvA2TsGiR0fg9KTe8ebVdbx9REVqmzs0fSDeO9hJyr9Pj/rQeHzGFnQJyRvaqJjXAtNP1bxWNGflvGG2mjc0XXMexbyydxr4U/F5tgCE+GfXDCYx398yx1Px2ZzOZ2KfwJ4l7USR74jP8wV0Jra/DcptivX+OEr2mdrRJH78lcPh0kND1lsHulwNvNra4YdWC/pQdCZbO/zYdOah1ZtzExzXn+XzJJ1NRNOdcub3WJoziuKdYe/qegdrl814ByuXYYEly0CCdrBiGZdi1g+bg7LTA13f6dE5d3o/ZDbt9HZx9lvvDZ7oaERzeY5GNNsrHvS24LTD0fn1OH4zPGAzerYyi+E/DmExtUPbn81Uf7PXgysxzXgXZ4e54MuH2+gBrcDrYHAZD0cneJZuOlsmHiAA3XSK0jf0lM53NFEKlXqaPBN0kjwJ8oKeJ7lP88fnYlJSd5OmFnY34dqdeRh3e3tYhN09OMB5hN0DuqXuaJt2GtGsuS7OToJv3X22+IEWW4e8ATa9qTukyS3dPuHU7ZMD7x6ylaMB7DuNu2xCVPeYtX48Yic5YO2xxd4mXuUhXpUZ944svJzekY3N9I4cWvQtnC/U69ts4eAiroHNqyVshmJymYFv/LshQtbDR0hoj3hi1g20x1q4jfXHuiE61nr1J0XnhvqjYqSjQXeM0XOO0SZNkdygN8fhk3TfJ8+lmPDpiALDMnTwujPw+C7Dh7ZX4gOxSBYfU8LHsWWEZjfsYzZDyGYIuQwhlyHkxqPBWybVEQYQ0wmsOGK3MRod0QodDL/lGKKb/pfB3thYpWeOnado5UBaEYfSnkw1oJyGDEpavzYwIRKvBeZLDuYWPbKCvXgTWQ2+pOu35JFQqVKyB+R9KgXT4r0Wova6Zt8y3fxua5Z0W89kSJI9QCQX49o91w5rY+mENsPSsWba2JV36GDGoAunDLn5igiFWeYu7YbGrh5kTQH6B/RT9nyYLECuxwCyJopuLThEJsNoWtZRUS2qdYsMYgYntKaIEynVGoBa1q2kozbtoEMK07CDfixVMytarZ6t1qmuR8++5hjhIxkuc9EJFBVTSEcZL8NDJXzsO8dnNOgy6z/qLhn9bxO82DsUppSa+KBo01by5JHfapl7jhdmBPMQUwx+MNVXKddnkI05Zs7Mre07m3dKOKHtjRmQ6Gr0kRwY7A3Ac8W6VSG5cGt4zoXwnMgnagKZ2DYOJHGS1eOI8I0EG0HjxmiJ+IDIMl4iPgy1IX4iBxc68JLPqAwnSJ218Z1zZoIHI8CRv2qAh4CrPZ8zzFSnUY5ZE4KcbxRtr5QgT239Pp7QY89m0DEM62FnzWubxkTxcIA3A93TBLoLSrN9St5WpBebyaEZhQgNcx91OrIdcpZCQUlNtBw7Dy2foeUztHzWlZkBxA+ThdKV0ccMdzVwfJKwlbHxc0WOJGQoRgxFomgZFM2bougyFJmK5cLojRmOdgmOnsnTABHPA0QcSa54Ptc83y0Ck63JgAmkU08pj+mpRVPjZ51eXEsr67HnfLWkzoyOerJy+0cOhvqwAHKY9TTM91SrZ48683uyf3K6KrOldqfPzSLcg3SV1TBdVYzSd7ko9Sixx0aKHiJO9i3hdEyjSr8+WJycleP0NMHpE72S5fYSw2V2LJq6DYM07hJMBo3JoDEZNCaDxqwJzctcFeKjMfrqI9t68/ZHX8qMkccQ8hhC3o1M9i5/XmZ59HpfRxg4SuCxCSaPweQxmMYMpjGDaZzL/fv0gPNLenA22JocZboA+rXBH1T/Mz2dsgYV06EOljevM9hg5gb52l0unzhwOhsyqEJG/tlYQ3FipBy7XYO9wOeDIV7ko2KHrJHSlhgBkTRNiciyuGmJyOZ2RtLFuhnx8Vqxqw+V6JHpu46KFU3Nv03zeiRmrQvNVhqjN0gmrU3Psrw+zSQhU6VMUjjXwPOJRPM/rnbMveZAluIab6GbJmAhjlmwvuZgHeGQAYfoWRKeX/DKoyp2NW5anIDRTBVMjhJailBIpNpIoZcCS6tGOgPXEsPiYNkhi30o0tmkgI2Uz/dZ5OPiktKJlRi+zmD4iaqUsi9QWLZz5YhOw9qQYiarZGhmOmag2hzUqa3k12Y8R8Qcd0WECarLYA05riEDdhoyYKcc2Gmoxuv4YSg+SB2cbUo+iATTYMhjUnxgMUtxjpbTdXmiEEm6W4HfmdWCX0nRJeiXjrsW5Epczvtchj0QJMKelg4tCWmGYi7S5MQRYFL0IVP8uvA+S+Blr8T7SK9pqsF/xKC2KLawxvMa+NbhP55iMWTLivZ2RBVzxeD6rh64AjhrkeRICEC72lL8JT+HzMsx6tnfoKH9reWmeGpvMVZqBDic7kQyv26e+RVwqnbC43GcxwM5WMpFFywLNWSAjobcNPPvCLS1yDXJIsIZGOwtccvqim+ZGcD6f1WPJcnj5I2zz2WxnwYHkBU1bJo4WEbmP/KQATXcpAe+n8Na9l6gMb0bvGKQY7WYrZY3rRAz4ebTx+GzrpvdMy+cobClkGKO9RMLGgSzvoupBRT1U7R/y74FmSduGbFlPUiFu95JCg/YOwTWUj48rU/TtarNFPbJzd+YATlmQIZLRJ1Tz2y1mQRoFsAXHMAfidHM+dAkm6AgD2jUy94odXtJXF0vgYNDjJramHAeokw3GpbMRTIZM08H2ihXIT4MjzJu22TeRFHS+hjXGzSywzw9ZQmgFSIsxowcMe7r1Qc4P6Z08saMqGcX9X09GHWC8gVnjwuZPS7GNUCsE27eNAGUr6J2HoIsKs9GQclgm1ohKezlkDPsT8Yvir1kr6XGuTMY4JznYWh5LsPQlzEca0Do5+phLqlhQ79NWI3QQtlg2lwNaenQkvVen3ff+qAJhWODJOXhipPXaZUyyXzvYtZn12Rvl7GauNWD5kmsogyao4ElvNgHh31g/Dqcc36NHwYCw5EI/YbCRrKkRz1MRV1gn6ad5SA6caXgWmifTnRd6q8Jw7xihPz4j2Pq5GE6DfMDlohn1yJe1BH5LFNEYYkYI++mYYoYM1ey48UgptH0/wCMYyKU5erpNeU+Gh6FxXyZ3JtGFRbEh7nqiev7fH2frU+gJG8dcGcdMA0VkJKC7rLQUK+znxGe5d5F9tC16gZ1uCTXzEnuYKgSSed6l6LERFGFjFyByVWRIuaCuSAnNMFyzBMRaanbVxxEx+g1KG8L6syjqRPSODLFkQEEVclJRdRLm9lhTi54l/loHZyepThBHLhDNOef5YjlOph6U48EZFa1zk1zHYw1zzOG1q1C9jxhMO/ZbF+aMayW3quw+U3HHKz6o4BJrOL4+kWBgghWRX27gjrvLlPnKuheJz73A8360B1myB8hvLnqUT15ZpxhnMumhfLVycSqnjhX+QSjGSaDBcsEpm4HPoL1l5Twqpp+uooO7NVPwCoYRtVOQ5SnKnR6Rd1X6CDW5p9TrnDtOrhUSenkKmEUyoNdi1yvkT9+qBT4TvNNYB4fVNM46TAXZzHZPDZTV7jBwGNu2dfT1hdJ5PyJQpn3ADyfIlGus6sriCvz02HuFDnMg2QQt3VGYIq0lvGb0XIQWIXgywyC/6IkzgZlKrQxpMSgxmTXBEe3OpRuPD6AsXINJ0RJiGmQU5BORT60QnwQwy8DTibp1bHzUGaX6shhlRT+yqXAnpEyperXzzT48IG/c3YjNTD65oRNpaqr2LnWRFZshU1Rqm3ZlmgxUIfniByeWocl2RIunqx/G3DhSMlMklZmqCyUR8qQ78f9/d7VdeZFw9/wFw2PKCF3nikiXZA4DmlM4xcjfdFw3pZmrxs22euG8fW62SiOXRB/7IORvvVYnDi75aRwS7NLstkl2bf+BuTlFw1bjV59XK8dzXceWyUvO6790mohpOVXV+dteWAvsH4M4nvJxTfkT18a0xOhZCG+TESVt89JjX2aCXbMBDtuBdtAsM+SfokDBEhwssHOIjN4ILadlGxrJkCXCdBtBXiDnskEcUnc6ELApvTM/H1OauxzI5NrWa1kG0g2pV9jKrFJ5yIu+CiAWH9SsL6Z1DwmNa8V2g2ENiC6Oc08cWrB8yBi/UnB+mZCC5jQglZoNxBan4CZJbAI4aTrTwrWNxNayIQWtkJrILQXXGjpo5YvFf7ygospb4+Tyj2aiTRiIo1akTYQ6RMu0i6NxX5ORmgXyQMhLpI+qK5tJq4pE9e0FVcDcT1NgkLsOWyatRrPp1vUeD7d0kx0Mya6WSu6G3i8H6n4c77k8dL1JwXrmwltzoQ2b4V2g1h9kI6dJUHBs4RHZredlGxrJsAFE+BCurDniTbNjQk9kfySpoeLzNDzRHvU7ScV25tdpMWzx7jsWRlg+z1b+uZI31zp2zETwA4lxW+irYmOpnpbpa1ClcpVpFpb67WzOm1thNNLjhNuwWfws2m57An12X7t51+eE8p3aHbsMN1qR+NA3up66daJrx7rlx0alBxqqVekCujhXf4d68UrrhdDmjazQ9WOuO+yZjj5uq9eoNnJAID/C68+T37rOsk9QRnHVj9RbdWlccTnav5U7VftyBx7UYE/DKa1LVW9du4Yq98h/2iE0+sMTtBTjTOq2vuQefNIYe/tpNcyMe3xuKBrLWYL2CxvjNQjS3vwik90x4h/xxHfpgpJqp2gcr/Ealb5Udt0pqohSx2RNYkCa1LkiKy5u4Bv+Y7IH8+nplXkiJYbttQrquVH7/Xl3xPdEL2R+dNMn6zypGYEfMEu6iBLHjBrkHIluK6T3DHO33KcsbyRjW7jCxN+qep5eRTCzm5UOZ6bXrc9LiGAKvkwO2EGtCJmIi6nVre7v9d+x7rwjOtC5pHUsK1SE3wfKzzzjRi+2XcJzqSH+L4a6WXwxAMDZWPgZQGVT5ulXvCvtjLc48u/Y314molxfyX7cJHRBjfvoqLA9S2FljjJnU1m4VTxWWGy1Z/acysfkvliNpnOliV6N5dwT3op84jLzLQgp1J8WdyHqeDeoJ17orcjemnIlaK3uWYgciK4/iIz4Nr4W2QGJt7EmVgFZsD3l+1LagZmc/zNxQJNT1jXpd3ry19hXnm737u6zryr9QVxlneZdyh24e8VPQBHjMPNk+lEY+Ny6T2cT8WENoiKcbLRpfG+4P2PT9O2K94U+S3s+YWS1liVNKKKwcvCvZ/CHh/Iws4r2n2WuQL1avPehrlL+s+yuOq7Pp/I7/pUjqx6N2lAW316Dyl7N6kP+87pE3s3KT5yJ4R1AX+LqWl4tGdAbyUN6Vus3F36QMg673stf5fpE/n9tzlv/cSjjoyJ8d/sPvlxX8GdYRz6WXl/6QvAZIa5JXqiA81hpjYgYi2Q1k3elvoiRSB5Nd5HOufn5GyOckSKXv7bVf9IslKvMdW9A6poveTPnj4nqynOZSnnSvVQVN5dMM6YaGOgSOs5PeLzs/Fz4f2oV8Zmq0l9OlfKM2gtT8ov6VEI/+S5V7QCs8q3JDtKT3hBfeiyooXsO3PVFl5RdgNfuwT3ClZoTP2k6jrUe3mdWClmTS4KpXw7b639VnprbWpzY+lN2tXW+Sm3zm9Jd9/COX56YBbaaS10a6FbC91a6EdqoZ8tW2jDfmA22m1tdGujWxvd2uhHaqNFjmNI0zEeGoP2WuvcWufWOrfW+ZFaZ5HjGIKs8XzYLx6WhfZbC91a6NZCtxb6kVroP3MLPYK2xTPH2P40dmywN04+LJtttza7tdmtzW5t9iO12YJVZ2z2A7PQVmuhWwvdWujWQj9oC50jyVutvLPWZJ3byrvWOrfWubXO9886p9K5iXW+vcq7dVnotvKutdCthW4t9GO10LdZebcuG91W3rU2urXRrY1+rDb6dirv1mWd28q71jq31rm1zo/VOt9e5d26LHRbedda6NZCtxb6sVrou6y8W5fNbivvWpvd2uzWZj9Wm317lXfrstBt5V1roVsL3Vroh22he7AX6limvyZPx2cWOn3i+ztpL9VaV1umMWheZLjwO4O7DldimcotjSq7scKrb2IHXkpnLpJ4kGPdxDHsqZtpT3ZL7mzZ6jqAiJp7zx5RXou4Lt0T+rQh6Yqu7okREW6PHqSu+Urebd26hixDjRrLtC38nWjbS65tWVuvstFvuL5hzgDYxdrqjB0F89uvM1YzCy0TbZnoQ2Kiqv/+PTNRS9GH+8ZEn6f2FCx05g5vYKGHcIZz6nfrsdCqdWwtdGuhWwvdWujHmSt4kdpTY1Zqo19JvXSDrpa9ZetDJm57Js36E9t0LLUaA1dZMJvemxmCnkL0A1JDa2XDr5lYMFyH++C1C6mEZPMWFB2iHYtX2OOr+5Sv3afchn3qoehhvt400cDnUks3y1d53OctyNuhTwzg14X9m+uWbr7KW5lmtvmqIoaapy1NdO8V9d8PZNOk1owO/jbQwIkRgRZM4S/yrDnxM5c8pdBAzGyh/i3I5jGdwb3RCqLezmB/WQP/E87UBawWhCSzIu8AswuyJMgX/gnfLxOcUbv+N0HsKzrzBv6VWv3GmNXMSn1ljBXd+ANco6fsM63wz6pFW49+VElU1pLnoPUz4KFfCIeNTO8Wb8nbJLv9CbbtJ3b7cwO9QMviwvYFcXOLLJMFd+8u8faQ68WYLBjarhn8R4YU3YpevIT9PqHVIL70jnudd/D9J2NSyLjlY95zRiwf9Ue4H0/Rm6ewHeX3C/xVz2GV7jnP8H91ZDe758Rgb8oqGl3O7vuTId5Amr/368L7VI/Mu9fvCpBdPqtZelx6Vvne8s65rjxufr+QOdKMONGlpEPzwqitWO+Kj3mROUdW59Ij8jB5mXtUNZYvc+6oSnZFV1jeK17lHlVHx/JRb9Zzy1F8kXMmta+Vn6vkKC0r/VewghdJDM6ZuPE3lc039ukOWGG0vhPiioxLRoCIJbFK3I6ImOT30XZHFPfPyLerOZf12O5X5IWz9/yO+i2i/jGJZ1S5vIYrX8avzpHrsS568tTTlWeZ9wZu8Ov+kBPvpnkHxGBWqNN1PD9qgEs8ZEpR7piycZESk6BGjaWMHeU0aDR0vsJ49wlI5lfKhqCcfivJUX1VmLvMi1DXFWfmyUtP5k9hDVqdK9Kk1ViBeWIFnHtnBZ6nnoJa+2ScU7/5HO8MALSdwfHV9en+Fr6N8Ywt4nSd7XlsLX5QI2FuqVfa6jPmn1beJubHVtnmevQ7Tzf1tPu1sUsy+RtEOzjW8YXOjDqD1nI12j5LtN2+h9r+nkYN3qURqHJc/rjCMziO6XIdD/k5B9s6R35Hseh98ax1dEVP+77OztpoFBfb5B0trjvfU+ZuZvg5cbHIltxNXLweicj46fq1TOzVuKfLmQk7yUx4bWaizUw8uMxEXp9osxJtVmL1WYmvQEtx3uI8scff8QhVzGHc4BmyTWjpV8xEN7DO6BUd+IuMakzWeQpLl0YRstbZozGsevUeD8k/VmNaJZVndEUf+Wg5qyRpksFfUE2NSfJYEOoLGrdJxxZNkgRG63frJ9cViefhKKP/p2TEkWGffm+CtwXbFtQr3WTERETe94mXrAfvFLtyjJ+K73BlB9T6cqWdLtr27w7tPBTLcf/O2IEWvlBEd04jpBtJG/lc/N+J0WWPEF7mgjzbmEZkvyRVJH8GdDtkcYp/fYUz6rF9M2H7zu9EztVSK5f6S0JVRMzVEv8ls3dW2sVj77r5ZQ8kEpKv96F9n/zSgkfQLvXhBeWcLapL8Kg+AStIkbOFtMfYUPPL65Ho60I0lusLolvJh5RLs0oThrzHYnapShPq9Ey0wA5V3TALbFP7IckRR5s8kiP2vwVJckZcxKW+uqC+Ob0VOf4lsVV451k5/g8sx8YHaWThD2hHpOP/XHj8cl7sD3CHt6MJZdIs14TnxlsDZ8T/sgItiPj8N+y/fpJjtckP49y4CfVm7O8+jSlNqCcvaJTJpqwsMv/b0ILvYB9217oSfJ17ZD3dWVdVWZEEZck/4RVFF1S3/TGZ+yiv1Zf6lHwtRhNYHctiC1bBvxxbBI/QK6sI1kH9ubz2RuzXomq8BeUTWBY6ol4YLLEiUTf6uPAvxrKOJF5Bix9pVgbbspFUR+dbwb+SfUj3f0c5zs80X/2y5hwP3YjGvccRzV9K8Mizp56CRdnxq4gq1lWpWa41al7nkEZycRRP9P9N8nob6ZbGXndOUse6cfSwU8qymcS1vqctE16j4QD2FvFtNmMBfe2cPDZystvxumO663eAnbjrulqSd+QXvlSrd4tGTddlf4pkKWvB18YWXeUX2PdzMgKZXdck87GgOIjZ9SnnWVGmMpdVbJsg67ut2F7XCGQWP9nGTpMtyxqCtUDCXjjKnLin1I9/KzlqQdnTqIZ8X9PMB3YHF+Sd0KJv3FDqM+LUAXGsiKTuUywVSVKfUKwcSVLH/wva93ai5nVVAlSjere68GfKM//Gr4rN3/gNPrtcGlh5vM11JDuHCxk8m/PXPBIPKVsypx7O2HhI2ZUsG/dpzMWhDAr+Zd9d0qeHrRtlaDaX0VPy7xesQvHOZDOjdQ9XNnkoNpfJKyX/dUxXgdd7d/LxHrQ3rUJUltVfaBzznMdcI7iec/4JZ4KPabwzldY36bjbmuUTgFQCykMGlI/Evz75So/Y0cOVzzKGskS+JeznVB2N/FdUHIuZBgOKSy/JJuKnX0lC52QtN6RzN2ElGJ3OiacvaNwe8xBzOkJIZ0xcNKC+YvKaOJsz1wi2YNawSa14Ed//E/GEaSauUEco1jUHQAdpNUvBnk4iz/UUzy89pGvACDmv7r94nrtaHVb8RBJ1lm37btrf4zNJ1Jk563wmCebJ1Tqo+/VUEt0nKCzPQm6fSyKPodyv50H8qeAZEPm2WDwtcpd616cbPG+ktcOtHW7tsI4dNrXssMp7Wivc1AqHt22F48Hm8dV1d2v//HrBf+I++2bST9wfZOKeBdnpLWgTK6G/UPyZzvLJVEfEJznrhr2jyTW0eNw9x8V2nxajg/NrG74dn19bcX/Yo12GQ7Zt9/zagcUpLXaSK3kNZ2ceQ4xdzUmT03mgn5Os39SQn3f9I1WKo1Xv77y5uu4fbtH5d/bg884A5/r1d7YQgNkUf+Pspk4UufAj9uCA4S5nuIu5suasspa4ZEpaEnvAd8ITfo5Pu1fXDJOvOG7nxsf4YPTD1fWPAwA4NONdvjwevQVhQAPHewD58V7v/DpYzNwFb6i/mobi7dMBXP/BMd7m1v4QF4N9UoPBJuwOXw5RCwa4CRsZHPPvoEZWvDnYZ4sR3uHm5hZ92+zRYgTNzGHPHh6wg42a8d8H/zi/9nA5Yl+P2GKAx+/093Dx9xHuM4blNvt6jM39fdQlrdwfkFYe4sXtjPZx3f7oBBc9ttgfEdxbowM8bHtrhDdzeDbCb/sj+rZ7fICN7B6zHtUjj4T985+0pBr2+LRP+54e0PUfD6k5OBIXp71Narx/Cg0Y8eGBe3UNf86v/ZgWC7aw2MJUFrDs4/7Q97yYFuAfDkcma2tk8aXNlw4tt1EVrfh4c58uZ/AjLk7xRqx4q3tC+2x1qctudTdpbW+TvvUOrq73+8eooV58fDRgH4Z7fE33iH+It04J4vjgEC7v4LBHbcaDnUOyIgNjTB58A7zZ3gEJbLC3zxa4639RhSNWwPnEXGY0OutTTup7ylD5VKcR0qi9R+smNHbvUQWdR2MSpjEFKcEVx/tnIOL9TejU3R928DQnQyZtPlugy20JzmthY6qzeH+f0DlgenGwRYveHsl+ax9N1zY2uvUDrt/eh9MMjneTFa0dbu3wXdnh7nBARveYif7omDQ0O+J6AhziBPjLNsQSQsuy6x7iU/h8HE1cEetf9VP48mLCx/UcvoeiMdn4YHzL735Y53Mbl6Py+60v8fCwhybMtqzItIFKnC6uv7d8+HC2AOYz2Oqjnx2MYLVlwvfRGX4I48E2rgmBWW7DGtt040Ff7NOHNU4QD7pg6QZd4KJ7IyKR3dE+WvnBAGw47N8VD/4IIvbcDwtI5NkAP4RRx7bswHbisxF+t8xOEAZ2EMRbwzfYFF115MIOh6Me7mB1PAduwY67b+Gs3bdErLqbb4F0xJlT0Sn4OU8HctOnI/nUyqls5VROxals21JvKz3XWfm5LM1zJbeFJz1VIDwth9CsdS442xaKE9kSFyfJMcRlniAHwxF8iN9so8w7lu3FvRMiPanojg6pHamBjhnZTujydly/47i+YznVzblKO47ve74t2rE7gWt6oVfdjqe0E1qh6Yv7cqKOBSCGfnU7frad0Oy4cImuuC/H6/iRGYZBdTuB1I7VsR3PtpPrsTu254XwvbKdUGrH7liRFQUhb8cOO2HoeyjMqnYiqR2n45gh6Jlox+t4YeBbNfAZS+24HS+IXD+5Hhvk5zhhWN3ORGrH75imZUcBb8fCThCCClW3M5XaCTqeH6U4W37HghV+DXnNpHbg/EEYmsISWA7ojxOYNXCeZ9uJLNzF8z3Rjgm4O6ZZQ+4LqR04v+mZrrgvE+4TerxXjY9jSu14ZFwicV+m24HbIi9Q1Y7U36MA+oXrOL5oB/TZt0yrGmfHltqJOkHkQddg7aRfK5txss1kzn8mX15lO67cToLHmQxXZTue3E4inzNZfJXtSOYnoy9nsjpVthPI7ST6eyard2U7odxO0p/O5O5W2U4kt5P07zO5+1e2I5mfjL05k81RZTsTuZ3E/p3J5rGynancTmKPz2RzXdmOZH4y/uFMdh+V7czldhJ/dSa7s8p2FnI7if88k91rVTuuZH4y/ly0w919ZTuW3E7CL0Q7nH5UtiOZnyyrrDTIdg7vsTyn43uOLwCufyG2bMC8AAQeuZY2MDJ/snwTJGMHlqsrKJk/Wb4LPSCyBMD1FUfmT5YfgMDhtnxdRZb5kxWA5bMc13R0O5bMn6zA7bhWSlTrd3SZP1lB0PFBQoJA1Tc8Mn8C2QC7B7QDXUMo8yewpZ0QfL3j6BpmmT9ZcAFwOXYU6ToKmT9hFANuIhJEo77jkvkTBIYdF/TXs3UdqcyfLPDkcFtB5Gs7dtmAAbMInDAKHW2iIdkN2zQ7wDKCwNEmPrbcjt3xAFdfBCgaRMyRG3IpzDQbMENXbgg0KHIjT4QEGlTVkxsCXwq2w9HnzjKJsjGCxjSJPpmXWRRoTAf0IQiShmpHFzKNsuEKoigMhDXTCHdkHmVbXse1Tc9J4rja8ZdMpGy4FT8CaySuqH5AKDMp2wo6oRmaSQRWP0KVqRTg0QnALiZg1w+ZZS5lA7ieF3jCBWnE8DKZsm3YJ7QiN2modlJBZlPYEAgJlUI3yyHTKWwoMgPfFlKrn3aR+ZSU8qq0rFYOEVq6lJQY1SBWZeCkzEiPUi2JK6VGepxqSYFSbqRHqpZUOiVHeqxqqZOl7EiPVi11+5Qe6fGqJUOU8iM9YrVkGlOCpMeslox1ypD0qNWS+0gpkh63WnJoKUfSI1dLLjYlSXrsasnppyxJj14t0ZCUJunxK5UXZb7rESyVqGWuUI9hqcwxg5lmjkqhshkpaiapFG6d0SvNLJVC9jOarpmmUqKPTN/TzFMp4VDGGmgmqpT4LGOfNDNVSsCYsZiaqSolgs3YcM1clRJSZ7yKZrJKifEzfk4zW6UkHTKeVzNdpWRBMlxAM1+lpGUy7EQzYZUdn6u0iWZehkhJVWlciq00JOfONMCRGZaazNMQl5K0UrKLGgqkZK2UdKeGSitpKyX/qtHJlLyVkhDW6PZK4krJUGsYIiVzpaTMNUyjkrpScvgaxlrJXSmDChruQ0leKaMcGg5NyV4pwy4aLlZJXynjQBpOX8lfKQNTGjREGQCUB8p0eJE8AKgM3GkQNXkAUBlIrM8clQFAZWCzPpWV2ZU60FqfW8vkSh34rU/2ZW6lDkTXjz5kaqUOjNcPh2RmpQ7U14/PZGKlFg7UDxhlXqUWMtSPYGVapRZW1A+pZValFnrUj/FlUqUWntRPOsicSi2EqZ8FkSmVWphTPy0jM6qGFUJxfNQ9pLlN2/hEAJqPhfPResebV9fx9hEVqW3uUB3/3sFOUv19etSHtuMztpCuAJasmnfOq3nnrJo3lIt57cV4GovPnm2KzzPbdeLjg6SuOebXEledP3nzFZ3dAs9Dp7eivPOHpmvOI3HOaeAn1zJbgID4Z9cMJjHf3zLF9YamOZ3PxD6BPUvaiSLfEZ/nC+jLbH+4oyi5P38cJftM7WgSP/7C5XBpIuB6y1CXi5FXW7r80EpRH4rOZEuXH5vOPLRyd251V2lzg8ViaiZ2cxz4Y/E5XMwT++jZYWpbZxAECdvqmwthf51FtEj2MRdWsg8Q94wddxJ7PY+8fJ8CP2/2+lfXb9i0pzgunEP1xNgHeH7jYFl5s6es8tlT1eey+LmegnjnIO6N9JzS+axas7WWzhfv9MAQ7PR2cTZg7w3ucTSiKVVHI5r9Fv9/SKkKRE+kMAYAAAC+bWtCU3icXU7LDoIwEOzN3/ATAIPgEcrDhq0aqBG8gbEJV02amM3+uy0gB+cyk5mdzcgqNVjUfESfWuAaPepmuolMYxDu6SiURj8KqM4bjY6b62gP0tK29AKCDgxC0hlMq3Kw8bUGR3CSb2QbBqxnH/ZkL7ZlPslmCjnYEs9dk1fOyEEaFLJcjfZcTJtm+lt4ae1sz6OjE/2DVHMfMfZICftRiWzESB+C2KdFh9HQ/3Qf7ParDuOQKFOJQVrwBaemX1kg7QRYAAAEP21rQlT6zsr+AH8lIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeJzt2+tN60AQhmFKoISUQAkpgRIoISWkBEpICSkhJaQEEHfCJdzv+MxGXmu9WTscCXvQ7BvpEYLzZ6RvZnZtOFtFUWwBAAAAAAAAAAAAAAAAAAAAAAAAAIA/ZbBYLLRrgJ754eGhdg3QUxwdHY3pgWy5/Ivj4+Ph6empdi1QyP/k5MSZC+1aoJC/zL03lj2gXQ96zv/8/Lw4OzvzBre3t9o1ocf8Ly8vVz1wcXHhvk6lB7RrQo/5X19fr3rA5V8ayvfadaGn/G9uboqrq6tVD5R9MJM9oF0Xesp/uVzWemCxWLivQ94LZqG4v79P9cAB+WeheHh4WOuB0i49YF7x9PRU9YA8+63ug47kPxPa9aHj/J+fn6seuLu7q/WAGMpO0K4RHeb/8vKy6oHHx8fqHHA94M4CMZEe0K4RHefvuB3gesDtgKgHtuXftetER/m/vr6uhDsg7AEx4gwwq8o/3AFhD4g5vxMwa5X929tbtQPiu0DZBzvyVbtWdJD/+/t7lb+/C4bPA6V9doBJVf5xD7gd4N8LSP4HsgO0a0VH+Yc7IL4LBD0wkO+160WH+Yd7wOUf3gfFiB1gTpV/vAf8PcDvADHlDmjOWv6+B/x7IZe/JztAu150mP/Hx0ftLPD3gGAP7AjtmtFj/v4cKHtgxA4wZS1/3wNO/E5A8p8y/6asZR/mH94Dyh6Yk78pyfw37ADtmtFx/p+fn7W7QLQDhvSAGcnZj/MPd4DY4wwwY2P+iR0wZv7NSO7+VP7BDpiQvxnJ2Q97wIl+NzSTO4B23egh/3APhPnLDtCuGx3kH2fvRWfAkvzNaMz/6+urln+4A8jfjB/ln3gW1K4bv5x/vPPJPwuNs++l8hfadeMX80/Nfpx/1APadaPH/BM7QLtudJR/mH3LGaBdN34p/7bZT50B5G9Ka/bf39/kbxv5561174f5+x6Q/Jfkb0br7KfylzvgjPzN+FH+0TMA+dvRmn1D/hPe/5qxMf/4Dijn/5j8zdg4+3H+Mv975G/Gf+cv8z8kfzNa974X/R2Qds3Qy39O/qY0Zu8+4f2/PPunPPuZkszffxJ3vxH5m9I6+4n8d8jflGT2Lflr1wu9/Kfc/cxJZu9/Hr375ey3p3X2o2e/AfNvTuPsR7v/gLPfpLXs/c+i/PeZfZMaZz/Mn+c+s9Y+idmfs/vNqn0afuc/YvebVfs07P5t5t+s1uxl9idkb9qm/IfsftOS2Zf5z5h985L5l7O/y+yb1zT7vO/LA+d+3jj388bs5y2e/Smzn5V49gfkn5Xw/3ePyT47Pv+5zL52LVDInztf1tj7eWPv5437PgAAAAAAAAAAAADY8A/GGUHYRhbiNAAAFrtta0JU+s7K/gB/L6wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7Z1HcxzFG8b5CPoIOnDF6MqBsi7YR4Q/AVUUobigguIAFGWRqgzYIILJQeQMS84gcoYlBwMWOcOSwYT59zP/fYZne3t2Znq6Z0a2XfXWSvLu7G7/3tzvzOyRJMkeu4jMHnDAAQcedNBBC0cdddTSwsLC8ubNmyGDCy+8MLn00ktTueyyy5LLL798RK644grKwPy+vHXr1uXzzjtv6Ywzzlg45ZRTDjz22GNnjz766La/3275T2bWrl07f9hhh/VOOumkvuGUXHTRRcnFF1+cXHLJJRnjpaWllOuVV16ZXHXVValcffXVqVx77bUjct11102U66+/HtI3x+1dcMEF85s2bZrZuHFj2+uwq8j03nvvPX/ooYf2TjzxxOT8889PYNPkDbsmb7ImY7K78cYbU7npppuSm2++OZVbbrklk1tvvTVX9HkQvnZ4nJ75DPNbtmyZPvfcc9tep51JZvbdd99F43dXzjzzzJS5sbvUzpU37Bu8bdbgDHa33XZbKr1eL7njjjtSufPOO5O77rorufvuu5N77rnHKfw/POK5FLyWx8ExIXwPIys33HDDovmMMyZ+tL1+q1Gm9txzz/lDDjmkf+qppyZmDTPm9Ov059dcc80Ib9giGNx+++2pkPG9996b3Hfffcn999+fyoMPPpg89NBDqTzyyCMj8uijj44I/4bHhx9+OBW8Dsfg8XB81RPRj77RwXnjp6Yef/zxtte16zK7fv36JZNnJcaHZv5dbR3M4dPB3NhYZt+0a6w9OID1Aw88kHEGu+Xl5eSxxx5LDIfkiSeeSOXJJ59MnnrqqeTpp58ekWeeeSYT/Tuei9dAcBwcDwL9sPUCn0H9iJElo6+z5nu0vc5dk7kNGzYsmxwu5Q57Z1ynrcO3w9Zh58ocdoa1JW+sPziANTmDGQQsn3322eS5555Lnn/++eTFF1/M5KWXXkpefvnlMcHf+X945PNfeOGFVHAcHBMC/VC9UJ2gPkCMr1g2n3/O6HLb6962wN5XTj/99BHu6uNp6+rbYefKHGsM+1beYKGcyfCVV15Jpd/vJ6+99lomr7/+evLGG2+MyJtvvjn2NzwPwte9+uqr2TGpH3hPvDf0DZ9F9YH+AZ/byIrRhVnz/drm0Dj3devWLSOHP+ecc7L4rvbOuA7uyLERy8EccRa2BOZYT6wtbA68YYPkTdbkTG5g+tZbb6Xy9ttvJ++8807y7rvvOuW9995LxfV/fB0eeTwcG+8BnaBeUB/oHxh34J+gs8McZNl8t1mj221ziS3Ta9as6R1//PHJ2Wefndo88zpwRx7Peo0+Xm2dcRy2pMzhh2F7Nm/YKxjbnMF027Ztyfvvv5988MEHI/Lhhx+OyPbt2zPRv/P5OAZEdQXvhfeETuAzQBfwuaCT+Kx5scJIz3zXaeMz2uYUQxYOP/zw5KyzzkptnrkdfL2LO+0dtk7uWCvmZmrnWF8wp8+2WStnMl1ZWcnk448/zuSTTz4ZkU8//XTsbxB9zUcffZQdC8emXkDH8P70Efhs+JzUBXwH5pfwCdQF87hgvnvbvELJ7H777dc/+eSTR2wevp75PLgjl6efZ/4O7rALrI3aOtaOdq7MyV1tG7zBBYwgyhlsXfLZZ59l8vnnnxcKnqevp05A6DeoD/QN1AWNEcwXhnVJ33z3WbMGbfMLZvOI86zlGOORzyOvQy7POt3mjlwKa6T+Heunfh1rq/bt4q3MbbZffPFFJl9++eWIfPXVV6no7/Zz9LV4xDH5Xnhf+gjqgvoF5gyIYeoThnXLgskX2+ZYVab32muviTbP3A75PGo4cEdezJzO5o71wToxb7PtnD6d3CHK27ZZmzUZU77++utMvvnmm7HfbcHf+TpbP6gP9BPQR/UL+C74TswV4N/wveHvhjVr3/iDafP/bXMtI3P777//YMuWLSPs1ebV1zPGM6+DztPPF3G3mdt2bvtw28bzeNuMv/3220zs3/OEOqG6QX1Q38AYAR2GT6AewMdRD4Z17MA8zhmf2DbfSbI4Pz+fLC4ujvl7xnnYPH29HeOh6xrfXdw1ppN9We55fj3P5vN0APLdd9855fvvvx/7G/Vhki7gO+D72Hqg/mAoi8Y3ts3ZFvTre8cdd1zKnr1b+nv07hjnafPof2hODx1H/CN31ulVuPvG9br8wTxP9P9d+kBdYHzAd1E90BqSPSajDz2zXlPmc7bNnez76N262KOug79nLa9xHjavOT2+I74rvjNyI9ZuWodrveay+Un8i3SgbAwo4v/DDz9kor/zZ5cu8DPgM+I70B8wP9A8ETZidKFvdKFtHZjZZ599BujfIscne9b0iPXw92CPHA/+Hn07xHnN71y+Xus3u4az63Nl76rZYvO3dUD5QwaDgfNn9Qs8LuOD6oHmiawbh73ngZEZYyuts9+6detInsdeDmM9czz4e43zavOa22n/Tf29q3YPxb9KDKjKn/Ljjz9mP6s+2LqgesA8EeuB9WFMGO5DDMzjjFm7xtlv3rw5Y8/ZOrJnrEcvB7Ee/W70O8kesUzrePZktU+nsd5l92V1YFIMiJEDTOJPHaAeqD6oLqg/wOdkbgB7YP+AemBkYNZxxqxf4+y5d6M5vrJHrAd71nXs0zPH0ziv/ZtJPZyQ/CfFgNj8IT/99NPI79QHvhbH1jxRfQHrhOH+08DIjFnDxny+zR49HbDXPI/sEevV35M9/b3meVV686FjQIwcsIi/6oD+bPsDxgR8H9YJ2j8yMjBrOmPWMQZ75PnOeG+zZ13PPA89TdZ29Pf23ozmea5+fZEP6DJ/Vw5g87dFdQGv4XHxvnZegPWDHQ33vAZGpsx7hmaf1nh57NHPo92DPexe2eflea76Tnu4ZflXiQG+OWAT/H/++eexnzVHUF+g8QBrx/1G89g3jyF1YKy3k+fzXXbPfTqyt/dklb3N39aBXYE/xdYHCGMC8wI7N5SZhF6gfHDxiCOOSDB7rfU98nyyR67nYs94j1xPfb6d56vfr8K/SgwIUQPU7QFU5Z+nC+oLqAP4TswJqAdGFmvqAPZyUvac2WBvh3l+nt3n1fZ2fe/K+Xbz/09++eWXsZ/VF9g5Afeaub9oHufMmvqwxx5uto+HXJ89XfT1XDWe3ce3e7n2zJXOVu3mn8/fFvUJ1AG8P2tE7R+btR148s/277mPR/bcw9PeDu1e+7m2z3fNYe3mX57/r7/+OqYLmh9SB9g7pi8wUpV9NreDmI/eHuYzuZeDmQ3s4dn1vb1/p+xdMX83/+r8qQOqC3wt80J8btUBs25V2GNeL8v1tc7DHi73ctDPt9nrrIb6/CL2u/lX46+iMQHHs2uD4XyBl9+3c33U+GSP/Vv08227d7G3ezy7+dfn/9tvv435A+oAYwF7BOb7VvL7YI8eD/I95vrYy2NvD3u4nNexfT7Z2zF/0ly9st9d/5fnrzpAPWBeiPdkXWC+Y6l8f82aNWMxH7M7mu+hzuPMDnM9zfMn1fh1bX9X42/nei7+1AE+5sSCUj0+npej/T3EfOZ7zPV1/962e1eeX5Z9Hf47S//fh7+tCxoLhrVhYc63bt26rLePWk/38TXmI99jf4fnWPmyr+P7VxP/Mvt/dfn//vvvY3ogfqCI//IJJ5wwVuvB77O3y5jPPXzk+nqule7nxGC/M+//l+39FPG3dUD6A4W2T7+PnA+2r34fdT56u/D7GvM5t6Pc8/o7Vdk3xb/N+Z9Y/PkosWAS/5XTTjstm+Hi3CbzfdvvY3bHtY87qbYvYl/V9lf7/J8Pf7vuz+OvIn4gd39nw4YN6d4O+zzI99HjY76vfh8xnzN7Puxd3GPbflfmf5vk/8cff4z8bp6fG/dxDQb291nrs8+j+b7O79iz+UXs87j7sG8q9red+5ep/fNsH/wtHXDG/fXr12c9XrvWR86HeW30+JjvM+bb8/llOJfhHpp9W+f/dIn/8NHFf+mYY47J9nXV9jGvj5yPs/rM9xnz8+a2QrNv0vbbzv3q1n42e0ts9pjnG7F91nu0feZ8nNWn7dvn2ddlHot9U76/S7m/i/+ff/7p4j9/8MEHj/X4sbfHeg+2z5yP+zrq95V9Edsm2If0/U3E/pi5Xwn+/U2bNmU5P/f10evB/o7avuZ8dsyvy97FvQz70LbfRuxvgj/YD0XZ43q6qe+3c36N+1rvwfaR79vnZfnyz+PuYh/D9rvm+2PkfhP441rKqe1jbx97PKz3mfPn2X7e+Zh1medxD8E+9nUfuhT7S/Jf4XXydZaT9T5zftq+nfNNOjfHV8pwD8W+iu13wffXzf0s/rh+flbz6UwXen1q+8j51fbtfC8W9zrs69p+131/AP64pvpIr1fnejjHy3N2uL/DfF+5D+cKgzGvyr5t2+86f2Gv/HvY56Xv5/4+aj7M9aDPj3k+1vsa913X3AkpLu4x2Ye0/S74/pL8s5lOne1h3gfbh+/XXh/ZV7HhENzrsK/r92PafguxP9mxY0da961du3ak36d7vNzfZ7+HeZ/yV16xmE/i7su+bs5fJ+/rQOwnf9wnK93jp+9nzU/fz/197u8y71O/n8esDm8fmw/FPrTtd9D3k396zyzO89u+H3k/Znvg+5n3wfbLsA8hVWy+SfZt2X4E/n2cy8m8n7N9PI+H+3zw/VrzgT/Zg0VTzGOwD237dfK+Bn0/+We+X3s+7Pcy72fNj5rPjvsuPqF5T+K+mtjH9P0e/HFP1JG6j3t9mO9iz4e+n3kffX9VdnUkj3sV9jH8/ir2/RDcD9cZ+7HXpz0fXpOLvt/HdpvgHpJ927Yf2fdDFo488siR+U7GftR92OvjNfkY++n7yZ4smmIei30I2y/K+duy/Qn8cR/sbK8fdX9e7Ge/1/b9eXxC8w7FPRb7urYfes67hO+HLOO6fa66n/Od2OvlPj98P9lDqnCrI3nMu8w+pu0H8v0pf1yv1Z7zYu6n+z2M/eRPLl3kXod9mZjftO1HyPsoA9R+vI6Dnfux58ueH2M/fbaLSUzeZbiHZh/C9kOyr2P7Dv5jsz7c70Xfh/t94I/Yz32eSfxjig/3ttm3afsFvj/566+/Uv6Tcn/2/FH3276/C8zbZN+03w/s+1P+en4P+GPWy97zYc/f5q/r3wbzqtzrsI9h+0X1foR+n5O/7vkqf+T+3POxc7+yfEJxLsu9is23yb4Ltg/hvZjQ9+ecN2o/7fsy99O6rw67GMyr2nzX2Me0/SL+3PcBf9R+yl/7fur7sd5dYD6JexV/H5N907ZfJu+z+bP3w9ofe/7s+4M/e/7K35amePtyj8G+Db8fyvYhep6Pq/fD2q8M/6bEh3td9m35/Zi2b/PndT1c/Jn7MXfrEnNfm2+afdN+34c/530n8Yef1nVvi3domw/Jvomcz7fmo/z9998D3ftR/tr7LeLvklisyzD3sfk67Lvo98vYvuG/jPN90fvn3o/aP/mj98NZr7L8Y0kd7nX9fZvs69b7DttP+eNaD7yGN/jzHH+bP/v+zNGx1l3gXYd7E+zLxPyWbB+yhNlfzf/z7J/8mfvlcWiCdQjubbFvo97LsX3IAmb/af9a//vyb0KKmPvY/GphH9D2IQfiWl8837OIP/u+zNm6yNzH5ndR9pBZXPOD9+vT/i/7f5r/kz/Zc73b5l3E3Id7Wfa++V4M/kV+3+b/zz//pOd/kD9mPzn7p/1f9v/Y+7P5tym+3KvafEi774jtk38f87+8ZyP2/7D/i/0fmz/jf9v8i5j7cq9j811jX8L2yb+Hc7/Jn/M/2P/X/T/y1/qva8zrcI/BPlbMD2T75D+Pa/7xvp0875Oz/5j9xuwv9n9t+9d1b4N1We4+Nt8E+xb9vvKf2bhxYzr/o/OfOv+H+R/u/2v958sqlBQxD2nzq4W9B/80ByR/7gHwmh82f+SAbfEvw7sM86o2v5OyV/49zAFj/t+eAeH8N+b/sAdA+2+CfVneIbhX8fdtsA/s923+8zgPSO/rwxkw9gC4B6T2X4ZRSL6+zH24V2VfJs8vy74h20/+/fff7PqP2AcCf73ep6sGYA9I/X8IjjGY+3Kv6+9XA3uLf3r9V5wDotd8ZA6IGsDmjxqgKf5VeJfhHsrm22Dv4/dzbN/mv4jrgPC+froPaOeA5E/2uu5Nc67C3MfmY7H3yfcC+n0X/xlc/xf2z/v5Mgew50DsHDAEu5jMQ3Nvgn1kv+/in/aCcR6Y3uuDOQDPA9F9IM0BVxvzSdyr2vwqZe/iP4/zwVEDsg/oygHYB9YYEFMHyvKOyX21sS/w+6k47v01BZsHf73+I3IA9AF4HmBeDsD1b4JxVeZ1uFfx93VyPV/2nrbv4p/e/w3nA6EGQA5g14HYC7RzALX9uhxDMy/i3obN12EfyO8nw3/O+z9iJhz2r/f84bnA2AvSGGDnAF3gXYa5L/em2EeM+Yn8y73/K+bB9R7PeTHAlQM2yTk096o23yX2ZWJ+Sf5zuCYQ+oCcB8N+MOsAzoNxL1hjQF1+MXiXYR7S5rvCvsD2J/FP+4GIAcwBeE4QekE6D6A+QG2/C8xjcW+bfQC/X4b/LOYCkQOgDuR+IHtBjAExfUAVzlWZ+3Kv6u87zL6If5oHoBZEHch7APF6kNoL0jpA2fvyi8m8iHtsm2+DfR5/R/9nzAdgTxD8MRNonxfkygPV/rvCOyb3GP6+CfYl+aezIegHg799TVjdE4ztA3x4l2Hu6+tj+fs67Cv4/bz+r0umEfvBHzGAc2FaCyIP1JkQOweIzdiHeR3usfx9w+zL8ocsYC4E/PWeEPABrloQ/JV9KK6xmftyb5J9zXzPl3+6N8j9AN4TSGcDXXmA2n5bvGNzr+LvO8Ze5//KyCx6AeSv9wFnTzgvD2gihvswj8F9tbD34J/GAfQEwR+1IPMAzoaoD7B7AXk64Mu2DvMy3H18fQh/Xyfel4z5ZO/DP40D7AXYPoA9QdsHKPu2eJdlvhpsvip7h99PxRzfh/+0sfkB+fM6UdwbZi4IHbDzgCb8uS9zX+5Vbb5t9sJ/YN7Xhz9kDj4A/WDkgagF9DrhmA/Q+ZCqOuDDOQZzH+6rgT2Ob953bsf/7//oK4vsB2I+DOeJFPWE7BhQl7Mv81jcQ8T6JtgbWazJPusN8jxh+AD7eoGqA7YPaMqnV2Eeg3sVm2/A5+M9eoHYQ6aMj+/DByAPBH/Gge1yvqAdB+wYEJpzaO4hfX3L7Pvm/adqxP08HRjA/hEDUA9yVpzXjNaakHHAZh+KdVneZZiHtvnY/r6A/SACe8qMyf8HsH/wRx6g1w3nnAhzAcaBkDpQlnmXuDfJ3siM+ewx2Gc6YJgPyJ/3jLSvHWrnAj46UIV1KOZF3LvKHnVeA+wzHdi2bdsA7BEDeM9o+/qBeTWBrQM+nKsy7xL3VWr3Yzpg/P4A7BEDeN9g9AbZF7B7Q8q/jp2X5V2GeR3uoWy+ap7XAfaZDhibH5A/fYCeP+yqC6v6gCq8Q3L3sfmm2ZvP0hZ7ypRh2yd/1AOoCVkT2HOjk3TAh3MM5qFtPpS/b6DG89YBE+N74I8YQB+AOKDnkKofsGNBjHjeVe41bZ69na6wV1nk/cPpA3gtIVc+oH7AjgN1eYdi7uvrY7Af8g/V040lc4b9APbP+4irDmgssOsC1YHYzFch90GNvZz/AR8be65bObOYAAAFVG1rQlT6zsr+AH9AewAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeJztnU2oTHEYxsdX8lHKgq7iLmRhIRvKRgllI0uKxJaFxFoWPkKRusnNysLHSjbKgpSVIrJQLNxCVyFKCffkzjBvd6amMR9nzvn/53mec963fhHXvb3P7+k/152ZcyqVSuV4kiR/nYGoFYS6/soYQZ5KoJ2F9n+PIFMl0M5C+39FkKkSaGeh/f8gyFQJtLOQLCPIUwm0r9BsJMhUCbSv0OwkyFQJtK/QHCTIVAm0r9D4z37K7f8MQaYqoF3F4BpBriqgXcXgDkGuKqBdxeAxQa4qoF3F4CVBriqgXcXgNUGuKqBdxWCCIFcV0K5iMEmQqwpoVzH4TJCrCmhXMfhOkKsKaFcx+E2QqwpoVzGYJshVBbSrGFQJclUB7SoGUwS5qoB2FQN/7V+5/X8jyFUFtKsYfCLIVQW0qxi8JchVBbSrGDwnyFUFtKsYPCLIVQW0qxjcJchVBbSrGFwnyFUFtKsYnCfIVQW0qxgcI8hVBbSrGOwlyFUJtK/QbCXIVAm0r9CsJchUCbSv0Cxs/IrOVQW0r9DY9T/8OYBy+39CkKsSaGeh/d8iyFQJtLPQ/k8TZKoE2llo//sJMlUC7Sy0/w0EmSqBdhba/+LG79G5KoH2FtK/zXuCTJVAewvt/z5BpkqgvYX2f44gUzWqBP5C+d9DkKcaaHch/a8hyFMR9TOg6X9W4u8FL7N/G78W2ODUxDvQ6v8CQZ6KVIU70Op/F0GWiiifAa3+lzb+DJ2nIqpnQKVt/F5A2WieAWodaPc/TpClKkXwv48gR1UUz4B2/6MEOSpTTbQ60O7f5gNBjqqonQGd/PvrAfOhdAZ08n+YIEN1VDrQyb+/Jyg/rY8DzB3o5N/Grwufn2rC34Fu/q8Q5KdO+xnA2IFu/v2+oOXoQDf/C+p/94sgvyLQ7p+pA9382/hrQovfgV7+jxDkVhQ6PQ4wdKCX/9UEuRUJxg708m/zhiC3IsHWgX7+LxFkVjQ6+Ud1oJ//7QR5FRGWDvTzP7f+MV8I8ioiDB3o59/G7xFf3A6k8e+PAfEwB9PADqTxPyfx+4TGpts5ELsHafzbXCXIqOh0OwdidiCt/y0E+ZSBXudAjB6k9T+7/rEfCfIpA8PsQFr/NmME2ZSF2pB6MIj/zQS5lIlhdGAQ//YYMEmQS9no14E8PRjEv81FgjzKiDnu9f+DrD0Y1P86gizKSq3RgX49iOnf5hlBFmWm2YMQZ0EW/4cIMig7ze8N8/Ygi/8l9X/3kyADJ/1jQrcuZPFvc5Ngdyd/D7L630aws/M/zceEtD3I6t+uFzhBsK/TmdbzoFcXsvq3OUmwp9O/B73OhDz+RxufA72jk47WHjS7kMe/zQOCvZzBaD0T8vrfTbCPk528/ucl/pyQMnn925wg2MPB+V+ezAx6Fwfj3+YGwS4Ozv8mgl0cnH+bpwT7ODj/Bwj2cXD+5yf+PiE1Qvq3OUuwk4Pzv6r+Of8Q7OVg/NvcJtjLwflfn8w8x4DezcH4t3lIsJuD87+DYDcH59/mBcF+Ds6/30uKn5j+7dph7wh2dDD+bY4S7Ojg/C+qf42vBHs6GP82pwj2dHD+7fVBfi8JTobh3+Yywa4Ozv9I4mcAI8Pyb+PXD+NjmP5HEj8D2Bimfxu/ryAXw/a/sv41pwj2djD+bcYJ9nZw/u01YgnB7g7Gv43fU4QDlH8/AzhA+bfxMwAP0v+KxH8egOYfGwUd9gQC1qEAAAcRbWtCVPrOyv4Af1KNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO3dPYicRRgH8KgR8QMEC4OCSSEWKcRGwUYQDdgESwWDaKuFiKnFwg9UUIQjGK6ySGIlNoKFIqQSFMVC0MID9fZud2/v+2Nvb3fvXt//4ZzjZObdd/fm3Zl3nn/xkIQ0uf3995ln5p3dnMiy7GKv18v6/X42GAychb83a29vrzaFn9FjHdS5dnd3VZ3I/Wfw+ozrH9ozoH9wP8/+X9FftP+vo/zr3vvpX+i/RX+x/vfiZ6K/HHvD/zHln/LsT3+n//lJ/ENb0t+b/8t50V+u/0X4p77209/p/y5+LvrLsTf8Z+kv2v8L+ov2v05/0f6/KH9XBuiftP9v4/qHdqS/V/85+svz1zLQoL9o/zb9Rfuv42fD62PzT2X2o78zA7v0F+0/pL9o/336y/XPq6ee/9JfpP8W/UX7r8C/1/vvGYCeA/on79+iv2j/P7rd7pG/XvQX4f+T8sdrRH9x/t/RX7T/lzs7O/SX6/8Z/PU9IP1F+X+wvb2dmWcA9Bfj/8bW1pbV31WhHenvtV6Av2sPSP/kM/CU7q96AP3F+J+lv2j/O/Craw+Qkr+nDIT28l34/o+W2gP0SvSA0Ib09+7/PfzLrgGhDQP7p5YB+F+jv2j/dzY3Nw/9y8wAoQ3p793/xY2NjYwzoFj/R3X/HtcAaf534bMgnAFEZgD+qL/oL9r/a8yArhnA9ue6Fv2t/u8XzYAp+XvMwH4Efr78n19fX89cawD9k+0Byv8hm78rA6H9IvFPoQco/5uGw+E6ngW69oHsAUn7o65jBjTvg9F/5BpQ5wzo/h9KmgE894C6ZkD3f3ZtbS1T90FUD1Du+u/pn0wP0P3vye0P9HOAntYD6J9kD9D9D/8vIJwDcA2YuAfULQOm/+WifSB7QHI9wPS/sLq6mun7wNT9hfcA0/8M3v+SZoCKekBdMmD6o/52zQD6a5aSv+AeYPO/hh4gaR8ouAfY/F81ZwBbD0jNX2gGbP5n4a/OgrkGHHsdiDkDNn/UnNoH6t8PYVZq/hX2gFgz4PK/pHrAqDUgtFfEGTB7QIwZcPmfX15eztQ+AP7698SxBySTAZf/7bl3F/5qDlTvf7NS9PecAdM/pgy4/A/vhLrOguifTAaK/F/DGqDvA1xrQKrl0d+2DsSQgSL/BzED6muAtB4gIANF/qjfy6wBqVfCGRjl/7FrDQhtUlN/1ywQKgOj/M91Oh3xa0DCGRjlf3IwGCzpa4B5L0RSJZiBUf6o2ZWVlaMeIHUNSDQDZfzPLS0tZZwDK8kADIYBM1DG/5Z8bW8rf9vdQIk1pT5QdQ7K+KM+1edA6WtARRlw9YEqM1DW/0msAfh8iP4ZMfp79R/VB6rIQVn/m4fD4QL8zbuB0jNQQQ6mmYGy/qgZrAHqbmDRHCgxE54zcDClHIzj/4S5BnAOrH0GxvHHGtBwPRNiDirJQJn14Dg5GMcf9ZF5FmDeEddfh9AWiWWgaH8waQ7G9X9YrQGcA6eegYN/MzAqB1X6o37EebDtPNDMADNRaQ589IJJ/F9pt9s39ACuAVPPgI81YRL/u3P3HcyBtr0ge8DUMjDOmuDKwiT+qKv6XpBzYG1zMKn/081mM9P3gvozAfaAIBnItDWhbA4m9cf3Bc7hbpjtuSD9g+dA7wdFWZjUH/UW5kDVA9RnBbkGRJeDop5wHP8z+Qywr/aCnAOjzoG5Ngw9+KO+MZ8JuJ4L0T+aHOg94bj+zy0uLmZl9oK2nsAKmoPMg/+t+b+zgTnQfC7IHlCLLBzXH/WmuRfU74mzB0SdAx/+p/L3/x7mQPaA2mXBhz/qSqvVuqEHcA6IPg++/B/HHGjrAfSPOg++/FE/YC9oOw9iBuIsz/4vLSwsZObdAPaAqMun/225dRt3hG1zgDkLMgNRlE9/1HtqDmAPqEX59j/dbrcHOA+yPRvWnw3pc0gEr4PU8u2P+hznQdwL1KKq8H8kXwMO1Jlw0WdG2QOCVxX+qG/VeRB7QNRVlf8zjUYjc80B7AHRVFX+qJ9xP0jfC7ieC9A/Sf8L8/Pz7AFxV5X+J/v9/p9legD9k/RHvc45IOqq2v/O3HxZ7wHwZw+Ipqr2R71t6wG2e6LMQJL+pzqdThfPhjkHRFfT8Ed9wh4QZU3L/778/c8eEF9Nyx81g/OAMvcDmIEk/f/XA4ruCtM/SX/UJcwBqgeYnxliBpL3f6DZbPbG7QHMQDL+qMv6HMAeIM7/dKvV2sOZ4KjPDbIHJOmPmlU9wPb9AewByftbe4D52VHTnxlIxv+oB5hnQvp3ibEHJO1/f94DurgnaDsTYg+YSv0D9QiHELJ6LasAAAq1bWtCVPrOyv4Af1e6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2djZHbOAxGU0gaSSEpJI2kkBSSRlJIbpCbd/PuC0jJWa8d23gzntXqh6QIEqIAkPr5cxiGYRiGYRiGYRiGYXhJvn///tvvx48f/x27J1WOe5fh2fnw4cNvv69fv/6q99q+Z/1XOaoMw/uBvM/i9vCW/rm7to7Vbyd/rkdXDXs+fvzY1tVK/u7/bH/69OnX32/fvv388uXLf/qi9he1r/IpKi/O5RjnkU79XK7az7Hab/mTdp1baVpf1bFhz0rOnf4vOvl//vz51zb1T/8tuZQMkDkyYj/nVP7IFJnX/mwX9GvOJT+3E9oC5Rv27ORfMvL4r+jkzzHkQn+1DJFztRX3WeTHNeA+vjqGPgDKYz0x7NnJ/6z+T/l37wzoeeRef6stINfatiz9zFjJ33oA6PuVnnXD0HNN+SPXklVd6z5IX/eYwHn4WZLHdroh24n1jOVfbcRpDP9SdeL+c7QfXc1YnG0fp19n+ylZWd4pD/pt5l3XeSyXsqxt2iB6hjHJ6pphGIZhGIZheEUYx9+TR7DXp//zby/vWfLd+h5c6mu6NvWueITL6O1qB8/mZ0id8Jb2vruW9/Od/M/Y8Y98hnme93W+xC69lfz/hv7zFlz+9LNhz8Omjk0m/Xfp28MX5GvpI53PkPokP85d+QNN52+kjFyP/ci+LNsv7d/apZfytx/iUdtAyt9+Nh9zPyl9ic4suSAbbL7s55z0C9hnWCAj7HYF51HntA+T9me3HdoM90KemRby7uzZmV7K33X0qOOBrv8DdWi94L5tP459e12M0C5+yH3Qdl/3/0o763jnb8xnSvbr9Fldkt6z639AtukDLuyrKZnhb3F/Q5b8v5M/fd8+QMf7WJ/Azt+Y8ict/ADk08n/KL1XkT/P9vqbsrG8i/TF2xfn+t7pBvSJ2wm6xboYdv7GlL/P6+RPnMqZ9FL+nNf5w/527FtLP1tBfaU/Lf139u3ltdRt0dWR/X08R8hj5UuElb8xfYi8p3Xl8XjmTHreph4eVf7DMAzDMAzDUGNb7Jv8PD6/Z1w99oAZY78ftn3xs02+iwu9FX/D/MNnZ2fT6vzg1gnoDseE59zA9C1CXuvza19nP8zyoK9GP5yjs6sg/5Xd13YwfHzYjtAb2H89x6dIv1DG7ttn53Pst+Mvx2gf2JHxSQ3HdP3cfhfXe5Hy5/puXqd9gbbvWub4D7p5RJ7rl/PP7LfzNeiI6f/nWMl/pf9XdvD0padPHRsp7SL7sWMwzhzLdlngk9jFCwz/51ry73x+4LlfJS/PBSzO9H9wXIDLybl5zrDnWvIv0MnpOy94hhfW4c5z9fxf6Qa3OT//HatQzNyvNd27XO1bveN5fN7ZAhjD5/XEjTid1M/d+J9nAOT7v8vKsUx75D8MwzAMwzAM5xhf4GszvsDnhj60kuP4Ap8b29zGF/h65BqryfgCX4Od/McX+PxcU/7jC3w8rin/YnyBj8XK5ze+wGEYhmEYhmF4bi61lXTrhhxhfxI/bMT3XkPjld8RdmutrNi9I67g/dx+ZfuQ7in/tDM8M17XB9sbtrnCa/CsZGz5Y3/BJrdqSyubnOVvfyJl8vo8LuPKnmCbwepeKDN6zPLP9uh1Cp/BpmzbKza7+t92tO6bPJmG1xDDr4cNvms3Xf8vbNNjG1tg/U/a9vnQbn291+fymoSr7wuRR8rf646xBprXxHp0kBG4Xnbf5DIpfz87V23GcvU1nfwdb+Rj9h+zn/5Jeuw/+r6Yj5FP7vd6ePeMe7km2Mch+4VluXou/qn8u/2d/NMX1MUi0a/R7aR/9A253TH8FNbz5MHxR2fX/+17K9KPA7eSf9cebPt3PAH9PX1H3b3s2kbGqJBe+ikf9Z2Btux6SR1w5Ee/lfwLr+NL7ACs1pzOe8172cnfZcjvC/uaR5V/kTEy6cfbra/Pca+nmWl1bWYXl5M+vy6/1f7dfayuzevynK5+nmHsPwzDMAzDMAywmlt1tL+bK/A3+FN2cazD7+zm1q32ec6F5wodvT/egpF/j30YtqHlnBpY+ed37cW2kdp2zD/f5bDfqfD3RPD/gY/5WtuT8C1xL5Y/37PxPb/qPBHLzH62jJuHI/3f2eat/9nmuz6209lGa/+M2yJx/vh6sAFyrb9R6G8JOcbEcqYs+IjuraduzVlbOxztp2/mOgEpf0APuC1g16ct2DeL/Ch7zhux36+bU9Ltp936u0CvwrXl3/WfS+TvOR/o7vzWoL/JuJN/Pg86n27BM+kV5wpfW/9fKn/rbXSwY23sw0M+5HGk/1P+tI1Mk/gQxwg8sj/nEjxuoo/Rr24h/8I+Pffn3TzyvDbHfzv548er9HP89+j+3GEYhmEYhmEYhnvgeMuMmVzFf96K3fvqcB1457Y/MNeLvBcj/zWe3+D4eubH0Y+Zg2O/XaazsqF4Dl766myH8ryglQ/QxygT12b5sf86fh+fpsvT2aNeAWygaQ/Fbuc1Gjmvs6kXnlfHz363XDsU2z92/m6Ol+279ueSNmXMcqXf0f2/81ViU352+af+o16591UMTzdPKOl8Oyv5U8/pR/T8NHw/2GbtH7T/0Pe2Kj/Hco6X91d+zzLPb8VO/pbZn8p/pf9T/jn/135kjmGr55jn8u7Wh9zJ320USIs29uxtwFj/W//dSv6F/ZB+znMu4xLaA3mc0f+QbYM02bZP3O3vFXxCHv+tZPye8vf4L+f42QeY/sFiNf7byb/Ief7d+O9V5D8MwzAMwzAMwzAMwzAMwzAMwzAMwzC8LsRQFpd+DwQf/irWzjFAR1zin7/k3EvK8N4Q33JLWP+YtXMyf+KxKN+l8ue6jkrr7LcWujiUjownPuKSWEDilrwOzlGs+1H9GmKj4Npx9I6d8nd4iQvsYvcpk7/r7rhfykt8lY+Rds4XIN7cMeeO1U28NhBrCGWfZS0yx5vv+jX5nzmX8x0/S16ORbqkfok58s+xUe+xrlmu10a5OJbrfxEPTj/lfjs6PUo8l+/b3/6hLex0APG6xJJ5TkHeG8fpZ7v+Q/6OCVzh+0794ljKS+qXcykn6V5L/2dcfuLnMn2bNu191LO/t+HvKbke3G5dT7v7ct4dXhvM97Nqh36GIrfuex9w5rni+TI5d4A2lBzVL9AuHJ96LXbtOvsr/cf/o/OyTXveV5ce/Y/7Slm5r1r3rcrqtaJgJbeMDe3SpGw5j4W8EueV7Z62mRzVr88jT89VeivowVX/Pzvu/RP5c47n3GSafh528eBOt5uHRJ3nNyouWeerGyt2OtN5ZTv0+DjLfaZ+6f/dfIW3sivDkd6FTv45f6Pg3cB9lXtCxp4jdAav6ZjXeO6Q49Wtc49Yyb9rr4xTrB9W7Zv8L9Xnu3VKPW/qDEf9v/A8i9W7TCf/o7LzTKzyOg/kRF2yNtxqrGadmfJnTJjrBHqdL68r2L1be46Z3x26cvDdQ/RNrlnXcaZ+4ehbuxx7j3mLvKOu8s15GgljBch6Qb+n3vS79JHeO9Pud++Eq7GAxzmXrBN6yXN6V7+U+0iunPPs81aHYXgz/wCggvog4L8lowAAAh5ta0JU+s7K/gB/apMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7dq9y41xHMdxz8VEeZgVimSymMwKkf9BJgYT/4NisvgHDBYpA4PFwsBE9yAPk7LoTuonunyvDovSfV/n+l3n+zt3r0+96kxnuN51zvXUlVK6DeRHOB022bqW3au2y9oPWnavmu5oP3jZzWp5GrbpP3jZ3Wp4H/ZqP9ey2421Go5rP/ey+43xK5zXftSyG45xQ/vRy244r/ths/6jl91xHi/DTu2rLLvlUB/CAe2r7WsDTderP9c/oX3VnYnj+bOBtmvpz/XPaT/JrjfQdy3XtJ90dxto/D/3tJ9828vsHnp26389Czv0X8j2xHFeaaD5X2/Dbu0XuoNxvD830P5LOKR9yk7Gcf+W2P57OKV96s6WnOvC/jrvovZN7EpC/6vaN7XbC2x/S/vmtiWaPFhA+4dhq/5Nrn/W9nzC9i/CLu2bXv9+3coE7d+F/dovxY6UuvcG+u86rP1S7Vj0+lSh/cdwVPulXP/+xeMR7R+Ffdov/S5Fw9cDur8KF3TfcOv/E26W2fPDN2X2ns7qn89Pyuw9Xb/1ja7rOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjyGy7Hbg+u12aPAAAMp21rQlT6zsr+AH9w8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeJztnWdsVckVx7NKos2mKVkpmyjJlzQpi4SoRlokOjIgJIgsUGJWlAgwRsgYMBbudEzvvffee+fRe++9994h0ebkzZPvahhNu/fd+2bse1Y60n5k3s9z5pz/+c9c+PjxI2CU7vjw4cMn8f79e3j37h28efMGXr16Bc+ePYNHjx7BnTt34Pr163D+/Hk4fvw47Nu3z/i/HcNf9g7/t2/fwuvXr+HFixfw5MkTuH//Pty6dQsuX74Mp0+fhkOHDsHOnTuN//sx/Gfv7P2XL1/G9v7Dhw9je//atWtw7tw5OHbsGOzduxe2bt1qfA0Y/vF32Dt7//nz5/D48WO4d+8e3Lx5Ey5dugSnTp2CgwcPQiQSgY0bNxpfA4Y/7Hl7/+nTp9/v/atXr8b2/tGjR2HPnj2wZcsWWLNmjfF1YPjDn977pOaj9/6NGzfg4sWLcPLkSThw4ADs2LEDNmzYAMuWLTO+Doz42bM1n7P3Hzx4ALdv347t/bNnz8KRI0dg9+7dsb2/evVqWLhwofG1YMTPn+33yN4n/d7du3dje//ChQtw4sQJ2L9/P2zfvh3Wr18PS5cuhblz5xpfC0Z87GX9Htn7V65cgTNnzsDhw4dh165dsHnzZli5ciUsWLAAZsyYYXw9GPGzZ/s9kdazbds2WLt2LSxZsgTmzJkDU6ZMMb4mDO/82X5PpfVs2rQJVqxYAfPnz4fp06fD+PHjja8Jwxt7Vb/H03pIv7d48WKYPXs2TJ48GcaMGWN8XRje+Mv6PaL1kH6P1npIv7d8+XKYN28eTJs2Lbb3R44caXxdGO7Z82o+Xr9Haz2k31u0aBHMnDkTJk2aBKNHj4Zhw4YZXxuGN/a8+R7b7xGtx+n3iNZD+r2pU6fCuHHjYMSIETB48GDj68Nwx5+wl833eP3eqlWrYloP6fcmTpwIo0aNgqFDh0JxcbHx9WHos9eZ76n6vbFjx8Lw4cNh0KBB0L9/f+NrxNDnr6r5nPme0++R+R7b75G9P2TIkNje79u3r/E1Yuixl2n8svke6fdmzZoVq/lIv0fv/T59+hhfJ4Y+e5nGz5vv0f0eqflIv0f2/oABA2J7v1evXsbXiqHmL6v5aI2fnu85NR/p95yaj/R7AwcOhH79+kHv3r2Rv4Xhtubjafzr1q2Lzffoms/p98jeJ3k/yv5VQUGB8fViyPnr1Hysxu/M90jNN2HChE/6PWrvd0tJSTG+Xgwxe92aT6XxszVflP2l7Ozsz5G/PRFEzedo/GzNF41GderU+QFE/zO9bgw+f9lsV7fmczR+puZb2bFjR8Ie+VsSCaz5PhYVFf2tcuXKyN+SkLHX1flc1Hz9UlNTHfbI34JwM9tldT5S89E6n6Lmu5OTk/PzGjVqIH9Lwsts16vOF41vk5OTafbI31L2Mj+faLar0Pl2de7c+bNIJIL8LQlZ3hf5+TzWfP/t2bNn+aSkJJY98reMPdvry/x87GyXrflI3i/Z+8UtWrTgsUf+lvAX5X3Hz0d6fef+Fu3no2e7pNfn1Hw3OTUf8reIvU6vz7u/xfr5SM3n5H2q5mtat25dEXvkbxl7t/Md1s/H1HwbOnXqJGOP/A3z5+V9Wa/vYr7zrrCw8M8VK1ZE/paEbt4X+bhV8x2m5stv3ry5ij3yt4A9q/G66fXZ+Q4z20X+loTKz8XO9Xl393g+brrXd2q+aK9fr2bNmjrskb8B9m40XlGv7/i4OfOdOe3bt9dlj/wNslfd32HfaiF39+hen+R9puZ7kpeX91WlSpWQvyWhm/d1NF7e3T2m12/VoEEDN+yRfwLZ6/q5eBovb67P9Po7MjIyePMd5G8Zex0/F0/jVfT6f9Xo9ZG/If5u8r5K46Xn+iV7v0ezZs28sEf+CWCvq/GK/FxE45Xk/VNZWVk/btu2LfK3ILzmffatDk2N97tor/9N9D+v7JF/wPxVeZ/28bJ5n+fnYjTeES1btoyHPfIPkL0feV/i5yJz/V9Ur14d+VsQCc77RON17u8gf0vZ6+R9nqdDY7Y3o127dn6wR/4B8vc775fs/fu5ublfVqlSBflbEEHmfUfjZfJ+Sr169fxij/wDYO9V59HI+wvS0tL8ZI/8A+AvmuvGmfcfe5jtIX8D7B0/D2+uy3q4ZXmfur9B8v6/6tev7zd75O8j+wDz/mrqvj7yt5S/ys/jzHVd5v0X+fn5f/A420P+CWRP+zhFfh4dnYfJ+20aNmwYFHvk7yN7nn9f5OcRzXWZvL8mwLyP/ONkr+PfF/k4NfI+8fL9LsC8j/zj5C/T+Gj/vsjHqdD3/xlQvY/8fWTPu6/Lvssn83EK8v7y9PT0RLBH/h7Zs+808O7rOm+0qO7qM3PdRwHpPMjfR/4693XZe1u0f1+S9/3W95F/AOx5Gh/73SXRvS3eXf2SvT/dx7ku8g+QvUzjc2b69H1d0RstVN4nb7P9umrVqsjfQvY6vR5vtkPf15Xc2/pfNO83lLzRgvwN8xed+SqNj+R9ovHJ7utGY2ybNm1MsEf+LtjLND7S6zkan2i2Q393icr757Ozs3+qeKMH+Rtkz8712O9r8zQ+2WyHyvvkXb5qPnq5kL+P7HV6Pef72qTXozU+3p09Tt7XfaMF+Rvgr+r1aB+f0+u50Pj2ZmZm/jD6/8jfcvb0me+112M0vjcFBQX0G/zI3zL2PH3XS68n0Pj+3ahRI9Pckb9i7+vM9dz0epxvr9gQxhnYyF70Di9vrifq9Tga38O8vLzfJmCmj/x9YM+7syWa62n0ekTja+TiXTbkn0D2Mg+nbK4n8nNwer3hPtzVRv4B7336zOd59507W+xcT9HrkTc6ftK4cWPTrEPPP14PJ+3dd97h1ej1/p5APwfyd8lepu+yHk7au897h9fyXi+0/IPQdx3vvqTXW9yhQwfTfJG/ZO/T7EX6Lv0Gs0rfZd5nMeHnQP4u2KvOfLf6LjXX+6ZatWqm2Yaev9szn72v50XfjUZuHO8xIv8A2eue+ey9Dfq+nkLfjWRkZNgw10P+Lvt8Vt8lZz57X0+h75I3Gn5foUIF00xDzz/emS79vR0X+m6T2rVrm+YZev5+nfmyexscfXdUq1atTLMMPX+/z3z2ezsCffd09+7dv/Dw7QUbwjgzE+xFZz57R58302X03beFhYVfG/ZwIn8Bfzd9Pnvmkz5fceYTfbedpXOdUPHXnec793S9nPnO2yzUmb80gfe0kb8L9mzed9jztH1en88785k+/3qPHj1+lZSUZJpfqPl7PfNZ77bLPv8/RUVFpUXfDR1/1dsc7Dzfw5nfqUmTJqa5hZ6/Vw8fb57PavuSM39RKZjplnn+Xnz7rIfPg7ZPvqv8yzi/t2NbGGfpB3uZxsPz8PHm+Yoz/320z69ksY8rFPx12Yt8+zwPHzvPF5z5tvu4QstfNdehffvOHW3Ww8fO8+k3WKMxLY7v69kexpn6yV51V4/27Wt6+Ii2b/J9BuQvYc+r90RzHeeuHvHtkzNf5NunzvzXBQUFX5cvX940o1Dz93OuQ9/V43n4mDP/2+TkZNN8Qs1fxV6l8dBzHd5dPYmHb3Tr1q1Ns0H+Hus9kcbD3tUTnPmHunbt+nlKSoppNqHmH4TGw77ByTnzn+Xn5/+pDPb5pYq/Fx8P/Q6frsbD+PaJh+8fZbjWLxX8/ar3PGg8xampqaZ5hJq/m3pP5uNh3+ETaTzUXGd7RkbGjzIzM03zQP4uZ3o6Ph6FxnMrJyfnNyE6863kr2Lvtt6Tvc1A1XsfCgsLk0qxf7NM8JfV+qp6jzfTU3l3y5B/s9Tzj7feo2d69B1d1sfD0XgmGHx325awnr1I3yMzPV69x/PxcO7r7O/SpUtYNB4r+but9Vl9jzfTI/WexkzvQW5u7h9L2T3NUPBXzXN9qPfI2wy1ypiHKy7+5He3hb1snku/veqm3mM0ns5NmzY1/ZvbFN///rbkfZ5/T0ffo2d6gnpvXlpamunf27b4hIEt7HXnuTr1XkneP5mVlVWWfTy+8A/ybyCeeo/273nQ98hM7y/lypUz/VvbGFwmptjT2i7r3xPVe4p57neWvrtsSxjjL6v1Rf49591dzXkuqfds+MaOzRHjEOTfgE6tr6PtOv490TyXU+8tS09P/ywSiZj+jW0OIX8//gb80nZFbzHx5rklef94t27dflarVi3Tv6/tEeMQxN+AX7U+e19Dw79H3mELk4crnvg/oHw2QLH1OVEAAAS2bWtCVPrOyv4Af3LZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2dS4iOURzGZ1xHiAlNkzRyW4iV0lhaWLmsSNkIkaXLwkIZ68lkIVE2hkLIQuwokZSElUsWGhIy1CjMYcL/LCbTZGa+75v3/Z7nnP/zq2d/zvPbfN95z6WhAUtXCOGPAgtYf8NcG0MfQQ9eg/Yf6SDowWvQ7iMzbBwfCbrwGLT7QfYTdOExaO+DNNlYegj68Ba096HsIujDW9DOhzLRxvOMoBNPQTsfzhaCTjwF7Xs4jTam+wS9eAna9/9YY+P6TdCNh6Bdj8RVgm48BO15JBbZ2PoJ+sk9aM+jcZygn9yDdjwazTa+XoKOcg7a8VgcIOgo56D9jsUUG+Mrgp5yDdpvJWhNyLf/yD2CrnIM2multAetCXn2H+km6Cu3oJ1WQ0vQXkHP/iOHCDrLKWif1RL/D74k6C2XoH3WwgaC3nIJ2mWt3CToLoegPdbKkqDvg579RzoJ+ks9aIfjYaaN/x1BhykH7XC8bCfoMOWg/Y2XuF/0DkGPqQbtrwhW2Dx+EnSZYtDuiuIYQZcpBu2tKOJvwbcEfaYWtLci2UzQZ2pBOyuaGwSdphS0r6JZbHP6QdBrKkH7KoMOgl5TCdpVGUy1eb0g6DaFoF2VxTqCblMI2lOZXCDolz1oR2US7xb8RNAxc9COymYnQcfMQfspm/h96BZBz6xB+6kHS4PWBDz7jxwm6JoxaC/1YpLN9QlB32xBe6knq22+AwSdMwXtpN6cIOicKWgf9Ub7BHz7j+j8kG//kfME3TME7QHFHJv7e4L+0UF7QLKJoH900A7QXCZwIP844jdCz28PoftnYBuBB/nHco3AhfzjaLUuvhD4kH8cOwh8yD8Wb+dH0H2z0WqdfCbwIv84PP0fQHfNipd1IXTPrMR1oQ8EfuQfh4fvA+iO2TlH4Ej+ccyyjt4QeJJ/HPEsaa5vT6C7TYXTBK7kH8f0kOe98+heU2JVyO+eQXSnqXGUwJn844jnyB4QeJN/HPFt8q8E7uQfxx4Cd/KP5TqBP/nHMS+kf4YE3WHqbAxprw2i+8uBlM+Uo7vLgXjfaKp3i6C7y4Xl1uU3Ap/yjyPF/4ToznLjIoFT+ccx2zp9TeBV/nHEe8ZS+U6I7ipXjhC4lX8cE6zb2wR+5R/HfOu3l8Cx/ONYH7jXh9H9eOAkgWf5x9FkPT8lcC3/OFjXh9G9eGIvgW/5x3KJwLn842i2znsIvMs/jrg+HAjcyz+OfQTu5R/LFfl3TXyL5Ln8u2alOfgu/67ZLf/uOSv/rpkWMN8I0PMW/1hmPvrk3zVb5d89p+TfNfE82SP5d01bqM899Oh5ipGJ79SWvXcQPUcxOl3y75rJ5uiu/LumJZT3Zj16bqIy2s1Vv/y7poz9o+g5ieo4I/+uiWtDD+XfNQtCcW+Wo+ciamOtufsl/645KP/u6ZZ/18R9Q4/l3zULQ+33jKDHLoohvlE3IP+uqeXOMfSYRXE0ms9qz5ejxyyKJf4erGbvGHq8onjaQuXrg38Bt5FXeSbh5nAAAAWpbWtCVPrOyv4Af31nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO3dW0/jMBAF4P//23jhgQckkEDc79fm0pa22xSHdRzbcdLGM/EcpPOyYovX3xk7dLXLuiiKtSOrwLh+vzVlWUrMykj1axtX5vN5rHSa53nuDHqQlH+QeUAPgvvAwIXKf8XMP3jmB3QBHbB3gKW/w/XHkT7ngbMLDGwo/NcT8He5d3ah7zMjAx/4N/27vJddPejz/QMDn9j+zjuAq3+WZUszXecBOpCEf+Vs2i9Ulp6zYJfQDjAwiu2/noq/YW5N3ROzD9vX1ePsAAMj+If5z43oHbD2IKQD2p6kGJu/9Q6g9Nc64Lr7df/S7IHyb3XA8Ld2wJiL1JKS/0Lz17PrgdaBvx5Y/FsdKNtnY0qx2f8w8e+8AwL9S+Xf6sD2dTs7YNkfajOR/gPugEJl1wHl3+iA8vd2wLFH1G6S/fucAXndge3ntjqwfc3ODjj2KPUOrJn473sH5HUHlH+jA8pf70CrBx7/VDowGf+Bd0CuPlod2L7evKsD5e95mHIHJu/veC9A988sHSiVv7cDyt/XAWq/MTrAyb/Pe8GtOyD//6F3YPtSRani7QD8efn7zgCVhXbHl8q7+phpHdgV4AD+1G4i/VUHfP4Lzb8+A7LaX7MvffaC/M0OcPMPOQN8/o0zYPv78xD7gOc/ajOx/gFngO0OqOxnHf4S7Sfpb+mA9w5Q7plx92P22x0g9a++fo8OBPkr87oD5t0Pe2b+jg5Y/eu4OqB51x0oiuZzv+2ZT5r9VP0bHdAs63u9dq79XWe/pPd7uvxJ//5HX1PfM8DTAf0M6PKXaM/S39KBPv6uDpjP/bBvdoCV/54dsN0Drdkv5bzP09s/or3T3+hAyHOArwON2dfs4f8blv4BHQi5Bxrv9wTYw5+Rv9aBvmeA3oPaPsSf2gL+B+6AcsfsB9hz9Vdx/rsdSwd+YJ+c/9pjtzKMfYH9dP337QBm3+Mf2X6of1cHXD2AfTr+IR3Qe+D7HGoDFvYT9A/tAOzT9d+nA9T7T51U/If0gHrvOYTa/tD+oT2g3ncO4TD7Y/n7ekC971wiwd/WBep95xJp/gg/e/jDn3ovJAb+csPJHv7wp94PSWnZw19UuNnDH/7UeyIlHO3hD3/qfZEQjs998Cf0Z+AOfyJ7+IsJd3v4w596j1LNFOzhH9GfgTX8iezhLyZTsoc//Kn3K6VMzR7+su3hP6I9/MVkqvbwl20Pf9n28D+wPfxFJBV7+Mu2h79se/jLtoe/bHv4y7aHv2x7+Pd0T8we/rLt4S/bHv4B9gyM4I+Zhz/s4R/BXZC9NH+4y/TvdBdqX4Xi/+WN9TXhHu5PHbjL9j9EF+Cehn/fHsB9eEJ/RhuH9DaHe2f057G+P7PRTOjP/xzSs17mcA+PsW8hfTh0fLPeyxzue/vb+rAu/XO+DIzLfbA1zEf3D+1H6Pf3+349mPPyjxrqvUox1KawlutP/WeXHhjIDvzlBmew7MBfbvAMJjvwlxt8DyY78JcbvAcjO6Z9URTka0Jo7OEvKzb7PM/J14XEt6/9sywjXxsS37+e/dlsRr42JK69Pvvf39/k60Pi+uuz//n5Sb4+JK69Pvvv7+/ka0Ti+uuz//r6Sr5GJK69PvvPz8/k60Ti+uuz//j4SL5OJJ59NftfX19/s393d0e+VmRce9fsPzw8bG5ubsjXi4zr75r929vbzeXlJfl6kfHsfbN/fX29OT8/J18zMp6/bfafnp7+Zv/09JR8zcg49ub7vB8fH63ZPzk5IV83Mo6/fu5Xs//29vY3+xcXF7vZPz4+Jl83cnh72+y/vLxs7u/vN1dXV5uzs7Pd7B8dHf0Dhd3WjtmBWM8AAApBbWtCVPrOyv4Af4/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2cS2yU1xmGI5w0UjZZdNVFL4smi6zYtlUXSFWlrlo1y0pZRUQii4igCkFRASGgEAoYg2cGGzyG8f0yhrHBY3tsLgZjwFyNuV/MxTb3O5hW5PS8o/nQ0a//H3uYf+bUM2+kZ5dI/znvyfe932X8gVLqA0IIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhPjK+r6+PtvfQOyhjh8//o9EImH7O4gl/U+ePKmOHTv29+7ubtvfQizof/bsWXXixIkfjx49+k1XV5ft7yF51v/8+fPq9OnTamho6O3g4ODfOjs7bX8TyaP+ly9fViMjI+rUqVPwAv8dGBj4Uzwet/1dJE/6X79+XV26dEkNDw8nvYDOA88OHTo0u7e31/a3kTzof+vWLXXt2jV18eJF8QLqyJEjd/bv3/8LHQ9sfx/Jsf7j4+Pq5s2b6urVq++8gNYdb2BYx4BP9duw/Y0kh/rfu3dPjY2NqdHRUXXlyhV17ty5pBfQNaE6fPhwp/YCHz5//tz2d5Ic6f/w4UN19+5ddefOHXXjxo13XiCVB1R/f3+oo6PD9neSHOn/+PFj9eDBg2QeuH37dtILXLhwQZ05cyaZB3QMUAcOHJgbjUZtfyvJgf5Pnz5Vjx49Uvfv30/mAXgB5AGpCXU9oHQ98J99+/b9LhaL2f5e4rP+Orcn3wDyALyAmQfgBSQPHDx4cKynp+dn2hfY/mbio/4vX75MvoEnT54k88DExEQyD6Av4MwDuibsbW9vpx8sHNSrV6/Uixcv1LNnz5JeAHlAakKXPKD6+vp+aGlpsf3dxCf9X79+nYwBeAPiBSQPoCY064GBgQF4wR8TicRfGhsbbX878Ul/xABnHkBNKHkAvUHkAfQEEAO0F3wUj8d/rvOB7e8nWeo/OTn57g145QH0BpEHMB8YHByEF1S9vb1dbW1ts/R/Z/sMJEv9nW/AmQekHsB8YGhoKOkFdQxQ3d3d39fW1to+A/FRfzMPoCaUegB9IZkPwAumYsCkzgOz2RuasbzTP10eMPtC0hNIeUHEgJHdu3d/omOE7bOQLPX3ygMyHzC9IGJAf38/6kGlY8AG5oEZiaf+znoAXhC7AqYXRF9Q1wCIAW/37Nnzm7q6OtvnIVnq75UHzDmxeEH0BVEPah+AGHCmpaXlJzpW2D4TyUD/N2/eeL4BiQEyH4AXRB4QL4i+oBEDlI4BS8LhsO0zEZ/0lzzg5gWxN4q+oMQA+IDOzs5J7QW/aG5utn0ukoH+U70BpxeUvqDEAPSEUrUAYsDh+vr6Wfrd2D4byUB/tzcA/c08YHpBsx5ET0h8gI4BKhaLfV1RUWH7bCRL/d28oPQFEQPQF8SMGD0hRwy4q73gp7pGsH0+koH+0/GCbjEAPkBiAHzA3r17lfYBP1RWVto+H8lQf6884FYPymwAPSGJAVILdHR0vGltbf2soaHB9hlJlvp7xQDUg9ITcvoAxIBdu3btrq6utn1GkqH+6WKAWQ9KDJC+MGKA9AO6urpUe3u70jHgj4FAwPY5SZb6O/OAOR90xgDMBRADtA9UbW1tw5FIpET/N7bPSjLQ/31jgPQEsR+AGKB9oNK1wFdlZWW2z0oy1D/dG5C+sFkLmD1BxICenh74QBWNRq/U1NR8pN+K7fMSn/SXGGD2A2RnHDEA+wGpnjB8oGpqavq6tLTU9nlJhvpPFQOc/QCJAdgVlX5QLBZDDhjVPuBj/UZsn5n4pL9bDJDZIPYDsCcotaD2gaqxsfHbjRs32j4zyVB/rzfg1hM09wOwI4QYID6wubl5bOfOnZ+MjIzYPjfxQX/nXEBmg85+UCKRSPpAnQNUQ0PDvPXr19s+N8lQ/+nEANkPMHvCqAXFB6ZywNVwOFyi44XtsxMf9Df3A8x+kMyGUQtiV1x8oM4Bqra29q9r1661fXaSof7pYoBbP0j2Q+ADkQPQD2xtbVX19fWD3A/4v8AX/c2esFkLmj4QM4F4PJ7sBegcoGpqan67bt062+cvdjLW3+0NOGtBpw/E70WkH4iZUCoHtLAnbB1f9M/EByIHRKNRVVdX91bXgr8OhUK276CYeS/9nW8gnQ9EPxA5AP1AmQtLDohEIv+iD5z5+nv5QOkH4vdCyAHSCzDqgInt27d/pP2i7XsoVnzV3zkTkN8LueWAVB2gduzY8eWKFSts30Ox8t76e/lAr16AzIXRD5Y6oKGhQWkPsJc+sDD0T5cD0AswcwDqgKamJtSBb8Ph8K/Ky8tt30UxkpX+6Xxguhwgu2HIAdoDKK3/8pUrV9q+i2LEV/2nkwPMXhDmASkPcFPXgbP0m7F9H8WG7/pPVQfITBDzAMyEUQdqD6B0HfD75cuX276PYiNr/b1ygFcdILthMhNGHag9gKqqqtq8atUq2/dRbPiuf7ocIL0g5ADsB2MvCL1AeIDq6uqJYDBYMj4+bvtOiglf9HeLAenmAVIHSi8QdaD2AKqysnLOkiVLbN9JMZEz/c0cgHmA/FYQ+8HYDYQHwDwIvUDxANu2bQuyDpj5+jtzgHM3EH8/THqBpgfQHvBeIBD4UP97tu+lWPBN/+nkALMOlHkQ9sLEA4TDYVVRUfGHRYsW2b6XYiHn+iMHeNWBshcGD4A+gPaA8AD/XrZsme17KRZyqr/TA8hOgPxGCHthpgdIecAR/kZgZurvfAPOOtDcC3PzAJFIBB5QhUKhX65Zs8b23RQDOdXf6QGcvWB4APQB4AFkFlBVVQX9v1m4cKHtuykG8qL/VB4AfYDUTljSA27dunXX6tWrbd9NMeC7/uYbyKQPgFkA+kDwgLoGeFFeXv4x/37YzNZ/qj4A5sEyC5B9gJQHVMFgcM6CBQts30+hk3P9JQd4zQLwN8NkJ8z0gIFA4J/sAxSO/l6zALMPBA+Y6gPCA8Y5D5yZ+nt5gHQeUPZB4AFTNcDTsrKyEl0v2r6jQibn+osHmMoDSh/QqAGU9oCz6QEKQ3/TA7rtg0gf0KgBoP+38+fPt31HhUxe9EcOcOsDwgNKHxA7oWYNAP21B6xdunSp7TsqZHKmv9MDpOsDyj6Q1ADYBUANqPUf5W+EC09/t30g1ADYB0MNgBowpb/avHnzT7ds2WL7ngqVvOk/3RoAfWCpAYPBIPSfM2/ePNv3VKjkRX+vGkD+Roj8fQDUAGYNmNL/O/2P7XsqVHKqv/kG3H4XJH8fQHYBzF0Q1IChUEjp2F+5ePFi2/dUqORNf7MG8JoDOOdAKf2Pcie0MPR3zgG8akDsAkF/9IC0/q82bNhQor2C7bsqRPKuv1sNKL8JkV0g2QWD/jr/q02bNn1OD1BY+ksN6NwFQg8APSD0ANAD0v//q9LS0j/PnTvX9l0VIv8DiNoxtNSpbeAAAA7XbWtCVPrOyv4Af5KBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2djZEcKQyFHYgTcSAOxIk4EAfiRBzIXunqPte7Z0lAz8/+WK9qame7aRASCNCDnpeXwWAwGAwGg8FgMBgMBoPB4D/8+vXr5efPn3984jr3qufic6WsAGX498H/Uen5iv4zfP/+/eXTp09/fOI69zJ8+fLl388uvn379jvvsDdlBPT7R0bU+7SelZ5P9b8CNtH+rvZf9VH6dpWmk9ft3/mdXVTyrOQEXRq9XqXLrmftvHs+cGrnq3rr7B/la991ubRvex6aD3kFqv6veWX1jvufP3/+93voLdL9+PHj9714hrqoLwtEOr0e6TNE/p4m8oi8uRdlq15IF9f1eeqgaSMvT0cd9Hr8jc+q/8ffr1+//n7uCjr7c01l0fIjTZTPM1mfIz33Mvu7DFGe2wibx9/QmaaJ74xbXHM9RRqd8zi0fUU+pEcXyKnpVO74oAvassod11Qfqmctn/F91/76zBWs/H9WZtb/6X+dvIHM/upvqFNWd+wcelZ90S7igy/QPqh+gTxWcna6QD7KIT/3FVWd/fmQz8vfGf/vMRe4xf7oPPoj9e7kpf6V/X0d4sC22D3+Rlsgf/73foas9FHai0LzoU6ZLvC3LivtkbleZX9k1Oe9/ExvK1tcxS32px1ru+/kDWT2V3+H7836KH3d/Y/qNu5x3f0kviOzP3rQNpbpQtOpzWkXyO/2xz/yTPzlGc03riHjM+xPX1F90J8BdfXv6m8Z3xyaHpnpW/o9nqUPdGulyIv7+E3A/5HG7yEnfS8D9caHZLrQcjL5yV/HQ/qH/++yqPw6l6n06bodDAaDwWAwGAw6OPeX3X/N8m/BPbiEKzgt8zR9xduewmPlxKVYz2RxgXtiVf7q2RWf1nGYj8Kpzq7ouOJt7yGrxrarZyrOqvIfVVx6t/xb+bRHQeXWPRNepytydfH8e7XrTFbl1fz+CedVpT8p/1Y+rdKT84bOKfoeBed4kIV8nANZ6azSgcYVu2ceaX/045xcxXlp3F5j5lX60/Jv4dMqPRGjC8CzwvMh88r+xO1UFpWz01mlA7U/cmbyZ/7/yh6aE/tXnJdz1sq9VhzZbvnU9SqfVtkf7lj5I+UUPf/MRsjc/X+qA8+rkn+XK1uhGqvgRvR+xXkFSKtcTJd+t/xb+bTOT9KHo4xoD/Q1nt21v44ZnvZUB6f2vxXqb+AalHevfFNmF6773MHTn5R/K5/W6Smzt847GRe07MxGAeUWs7Q7OngN++vYycf34ikviE9Tzgt5sutV+pPyb+HTMt7OZQPKKVZlMyd3rpTnkWdHZ5mOPe9K/q5eg8FgMBgMBoPBCsS+iPmcgnUga5hVLKpLE3PbHf7nHtiRNYBuHlnmriz3BudiWHd7DH8F4h+sv3fWJt369Zn7GTOuUdeUgfhOrPBRZXbXHwmPXQeor8a3uvavZ2NIr/rLnucZ7mm9nfeKe+6X9MxBpjOe6fRJf/M4hsdos/J38spkzNJ113fLyPS4g1UcSffkV+dxlIPwOK3u1dfnSaM+B50rl6PxQOXslA9wmfQcUcWf4fPIR2P+Wpeq/J3yXMaqzOr6jrzEG1XGE6zs3523BF3M0vkv+Drt/+jKzzNk5zvJqzpnQjnIUp2NyPTvfEdXfpWX7td3Gasyq+s78mZ6PEHHj5Hfimfs7F/pf+dsEfn6p8sXedD9js/S/p7F4rPyPa+ds4RVmdX1HXkzPZ4gG/+VW/Q2X+37udr/M11V/V/L7uzvHPSq/2veXf+v5n9d/9eyqzKr6zvy3mr/gI4tPobhn3R86fgrl2k1/qvcbv+AnuGrzp9nulrNWXw89TFOecWsfEU3/mv6qszq+o6897A/9a7W/3ova5vc1z7kPJrP/z2NzpF9Tp/N5bsYgc6F+Z4BGfw+5XXlV3mtZKzKrK6v0mR6HAwGg8FgMBgMKujcXD9XOMBHo5LL1x8fAc/iAlm7+x7M1TqC/dLPRBVnq/Zjvmc8iwvM9jIrsriA7tnV/f8n61e1FbE2vZ5xbtife54Hcuh15yJ3uDzSVGv0zi6ZHvRcoHKklb5u5RtP4Pvv1T5V7I+YE35jhyNUP6PxK67rnnn273u8UfnCLI8sXp1xRh0vWMX7dji6LtapZxPh1zN97ci44gJPUPl/7I8Mfm4l42hVB95HNA6n5/goX/uFc258V31UZyZ4XmPr9JMsRu39hbbH+RWww9GtuA7yq/S1K+OKCzzByv8jK30v41V3OELOUmhfz8rv5NF8uzMzIQ9tlnJcN1U5jG3q3yh7xdGdcJ2ZvnZl3OUCd9DpW/us+niv6w5HqO+1zPq/jt9d/9+xP2c79Sznbt/SvQPab3c4ul2us9LXlf6vz99if/f/yO7jP/rHT1bpvD35uFrZX/POxv8d+6Mjv3Zl/D/h6Ha5zk5fV8b/nbOOFar1v3LeWUyA69pvO44Q+bCfzjGzZ7I5cFZelUe1fj6ZW1/h6Ha4Tk+3U/cdGZ8VMxgMBoPBYDAYvH/A5+ja71G4kre+W+Me777X2MAJdmV/T1wUa144ANaUj6gDdjwB61pierqvstsHXAGO4RQaT+xwpY6vBWIWvm4kfhbwfay+Dsdv6HqVMxjx0ZgNbUvjC+ir43ZVxs7+XV67abROug/e5bhXHUH2uyO093iO65Sr6QKR5mrfynTE9ewcC3ELjbM6B6O/z0U90A16JdaF33H5KUNj8dVZAbVFxdHtpHGZtK7KeVJH/S2hK3UMKA9LXA/7aKxQ0xEnpdwqXtihsr9er+yv8XHaPW0SPXl8S/Py+HbFq2X8idtc/ZhyyIqdNAG1n8cfPY6b8XtX6rj63THS+/sEnTs93bfl8ngc2usTcPs7b0A++puUyJjpBlRc1I79Kx5DsZMGPSrvmcmrfJi/R/BKHU+4Q8rlA1dd+ZYVeI4xLrOZ77WgDzlfRZ/QsaniDb39Vv1xx/4B9X/K4yl20ijnqOOgypF9z+y/W0flBPH5HXeonJ/ux7oCHdv043st4oNv9L0c3FMdZNeVX8ue787Xg8r++DLl1B07aVQmn3cq3853+oe3mZM6BtQGuqfHx2fXrbaTU/5PoeMHc8zs3mqP3eq67yVajVt+X8uvZOnWrrek8bIrnZzW8fS5zHdd2f83GAwGg8FgMPi7oOsYXc/cax7Z7UmMdZC+K2WnTF2rEu/O1oLvAW9BXo/nsO47PUdSobM/nADpduyvsRbWOzz3FvR5grcgbxaPJE7uMRvntIg9Ot+lUO5W4xUBnnWfozy0xyA8Jqv8v+ozS6t5E0OpuBgvF/k0lqMccscpaT21/iovfM6OXpBdy1G5TtCdMXGOR7kIjaV3PsO5e+WV4Qs8Rqr18/ONzsFW/p9ysjK9btnebG//2I3Yp8d8sW22b5u2AificWLsre2i04vL7nKdYGV/7OplZrH/FY/oNgowB6hsepKfc0HeX7K8qxiw7g/SeDex1uy3oyruVX2N7q1SriXzGSu9uL9DrhOs/L/bX+cJt9qffklc/VH2136xa3/8BnmpzyNft/9qbwd+RHlV5Q/Arl6q+p5gNf+jnnCMugflFvtrue6Hb7U/OqQc1cuu/clDxw61ue532ckHf678n8vrPj/TS3bP5TpBtv7zfUU6t8jOX6tuHCt70f51/8M97K/zv+rccqCzm/dxzZO+zLNdPj7/y2TRfRgrvfj8z+UafEy8hfXi4PUw9v+7Mfz+YDAYDO6FbP23imWAt/Su+Y5nOoWu17rxtoqdnmBX1/csM8tP4z+rvZEBXZe+BVw5+1CB+Nfufs1bsKNrT/8I+1f5aexHYxV+xinjCB3ELTyeDnemvC79jzNxzH2VD+Oefyd2qnXwdyRWsZKsbhqT0Xbh8iiycrK6wv+4rjWO7zKpvYhTO1e4i8r/a4xfz0vRz5TzrThCLwfdwZ1o+ehFz9WgH5cniznqdz9/SzvSeDryeBvwugU8lux8QLYP22OzxM+9rhWHp/lW+uB54sYVB7tjf/f/QNuWjlMed804QgcclfJxrsPu/137oxc9j+kyB/Rsj0LTZTZWfWX297mInq2r8lL9KLfY6cPL4d4JVv7fZcr2WlQcoeuENN37H+9hf2SirWUyB96S/Stu8Vn2z+Z/+EL1l7qPAp9UcYSuU/x/1/8Du/4O35TpPJvD7/h/rVsmzz38f2b/jlt8hv/3D/X3c7B67lDnKRlH6OXo2cGqfXta14XOM6uzmW43xWr+F3D7V/O/zndm5XT277hFv3fP+d9bx73XO4P3hbH/YGw/GAwGg8FgMBgMBoPBYDAYDAaDwWDw9+ERe9HZ+/SRwX4T/6z2vbPH0t9pEWBvTPZ5hD51b6nD32lccYnsS/N8ff8I7wDSD/s3nslTdnU5zUf37fGp7K+/Y8K+I/bZ6T63LM9qb/Ct8nd79dWG+h4Qh9Yb3bKHTPsE+T2rbVfo6vLIMnVfpPaNrP842K+W5emfam+eP7vaG7Jrf97LRPr439+xofZ/bbyG/f13B9Q+9MMO7COuoH2p28sW1/W3RTqs7E/boU87PP+s/3Od/HmXm+6h1H2bAdqbvmuJfX76jO6x1Xy1TZKG7yc4GUNUF/6uoaxvK6hbV576gsz2jL34hlWZ5Knv71GZ9f1yJ/b3ve5c53+tJ+eSdJxUWbjPd/SKzHouRPOlPajcV3zTyX5xPV+hvgB5qr5Nu9zx59nZAc3H95av5MePa/4BdKfvYlM9Mub7fKXSsc95tE7aX31Pr+5l1/mU5pG924/24P3wdEzgnFM2n3FgQ//tzGocZv20M5Yjy+ncsLM/etUxC//p7Ujtr/5d95qT54n99Vwi7VfLzN5d5fOsyv78Tzu+MidAvuzjQH50RxvO/Dq6q/yq53vl3XWByv7qNwFtMYsV6JlRXd9QV50fVucbMvtTro7lel3PpXqf0nMfnf2RydvXM9DFXXbnFpHuqtzdeHfSnvTdOtqXPtp5isFg8KHxD4gkaqLrd70WAAACjW1rQlT6zsr+AH+VZgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeJzt2btL1XEYBvCCqKmlrbl/QBESbHZxMgdBlBbBKTouKih4vxzv97tHVNRQQQcdXASn+hcaGpoborWh3jyiIhK0dPxC5zN89gceHvh931+cnZ3FyclJHBwcxNbWVqysrMTs7GxMTEzE8PBwDAwMRE9PT3R3d2fr6+sfRAT/jzg9PY2jo6PY3d2N9fX1WFhYiKmpqRgdHY3BwcHo6+vLd/+9o6PjWVlZWeq8/OP+j4+PY39/PzY3N2N5eTlmZmZifHw8stls9Pf3X2+/vba2NnVWCtD/4eFh7OzsRC6Xi/n5+cvtj4yM3N7+1/b29qcVFRWps1KA/vf29v62/bfV1dWpc1Kg/re3t2Ntbe1y+5OTkzfb7+3tzXf/pa2t7UlNTU3qnBSo/42NjVhaWorp6ekYGxu7u/03VVVVqTNSwP5XV1djbm7uZvv5997V9j+1tLQ8ymQyqTNSwP4XFxdvtj80NHT5zXe1/deVlZWp81Hg/u/eeq62/7G5ufnh+fl56nwUuP/89vO3nuvtX3T/68Ir772i8Kc77/umpqbUubin/u/ceX90dna+KC0tTZ2Le+r/zntvpKGhIXUm7rH/W7eeb/7xFJ3b23/nzlt0rrf/ubW19XFdXV3qPNxz/249RS3f/YdMJuPWU5x+dnV1vSwvL0+dgzRyjY2NqTOQzvOSkpLUGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACA/9Nvowb3ROzSmpQAAAwJbWtCVPrOyv4Af5YPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2dV2xVyRnHQ7IPu4oURUrykKbNU6I8QYgooi0CAYkFEvgBRDclFhGINQgsrm/humMw2BhM7733XkzvppheDaZjDKYYG5LdTM5cnbkaj2fOOde+93z3zJmV/tonHs738zfztfku+vLlC4qlPn/+HFJdXR369OkT+vDhA3r79i169eoVevr0KXrw4AG6ceMGKi0tRceOHUN79+5FmzZtQqtWrUKLFi1CxcXFqKio6PsxY8b8BCGkFF3FlD39N4D519bWoo8fP6Lq6mpUWVkZ4l9eXo5u3ryJLl68iI4fP4727duHNm/ejFavXh3iP2fOHMy/cMKECdC2klG28cdi+T979gw9fPgQ3bp1K8T/xIkTaP/+/WjLli0h/1+8eDGaO3cu5r/N5/NB20pGgfB/9+4dev36dT3+ly5dQidPnkQHDhxAW7duDfk/5o/9f9asWWXZ2dnQtpJRtvOvqakJ83/+/Dl69OgRun37Nrp8+TI6deoUOnjwINq2bRtas2YNWrJkScj/Nf4f8vPzoW0lo8D4V1VV1eN/5coVdPr06RD/7du31+Ovnf9oxowZv1JngBz8379/H+ZfUVGB7ty504D/2rVr0dKlS9G8efOw/6OCgoK/Dx8+HNpesslW/jgHoPm/ePEizL+srAydOXMGHTp0CO3YsQOtW7cuxF8//zH/fyYlJUHbSzbZwp/8DYj43717N8z/8OHDXP7a+T94xIgR0PaSTWD837x5E+L/+PHjMP+zZ8+G+O/cuTPMnzr/U5KTk6HtJZts549rgDz+V69ercd//fr1LP9MVQOUi//Lly9D/O/duxfmX1JSIuI/d9y4cdD2kk1xxf/cuXP1+C9btizMX7v/N6SmpkLbSzaB8Mc9ICP+u3bt4vEvUTVgV/O/FAwGoe0lmxzBX6//XU9PT4e2l2yKy/uf5k/Vf+9lZmZC20s2xRV/Nv5n+FdkZWVB20s2gfK3kv8T/tOnT3+p+j/u4U/Xf3X+1Yq/M/mb9X/M6v8zZ85E+fn5n3Jzc6HtJZvigr+o/4f7/3j+R+f/Q15eHrS9ZJOt/CPt/yv+cvI3m/8h8194/g/PfxcWFqJp06bVqvNfHv5W5v8Y/u9V/Od8/pHM/+L5/9mzZ+PeH+ZfpfJ/OfiL3n+w8/8LFy4M8586derzjIwMaHvJJtvYm73/we+/6Pc/K1euDPHXez+Y/x2/3w9tL9lkK3+j938XLlyo9/5vxYoVaMGCBaT2i7TY/7zH44G2l2wC5f/kyRN0//59dP36dXT+/Hl09OhRtGfPHrRx48YQ//nz55PaH+Z/aOLEidD2kk0g/EnvB/PHvZ9r166Fen9HjhwJ9f42bNgQ7v3i3B/znzJlyqaUlBRoe8kmW/mLZn+Nej967QfzX6LmP53L36z2R9d+6dqfnvuj3NzcHDX/7yz+hH1Taj967of5fz9w4EBoe8km285+OveLpPaj534oJyenb58+faDtJZtsjf2s5v6k9kNyfy32x/w79ezZE9pesslW/mzuT3I/nPuT3I/N/fXYH2VnZ/85ISEB2l6yyZa7nzf3x8v9du/eHcr9li9fHs799Nj/f1lZWV9rcSC0vWSTrbGf2dwPL/fTY/9ngUAA2lYyylb+othfNPeBcz899jup9n85kz+v72Ml9ie5H479tLt/5ejRo6FtJaPAYj9676NR30/nnzF06FBoW8ko22I/wp/Efjj2x7GfUd9Hj/0w/wG9e/eGtpWMsjX2E9X9eW/+qNgPabF/i27dukHbSkbZGvsZzXzy6v567PdjZmbmN6r36zz+UYr97nu9Xmg7ySpb737Rzm9R7Kff/TvHjh0LbSdZFXP+9N1vVPfDsR+u+9Gxn373Z6nY35n82buf997LpO6HtLs/UfV9nMGf9X2jni/d8yf7fun3Hnrsh/n/qXPnztB2klW23f10z5eu+/Dm/am6T3UgEGim/c1A20lW2cKfvvt5dR9ez1e/+0vUzj9n8o+058fWffSzP2fIkCHQNpJZMb/72bd+7LyfqOennf0oIyMjoUePHtA2klm23P2ing9999Pzfvrd/6PG/5dq5s+Z/I3yfot3f5na9+oM/ryzn635G+X9grt/tvq9D2fyZ2d9eTV/0Z4H6u7vr+o+zuVv9s4X1/zpvJ+u+WtnP+b/x44dO0LbR3ZFlb2Vfj/Z8WpS83+YlpYGbRs3KOZ5n2jWk7fjj7r7V4waNQraNm5QTM9+ox0vBv1+fPYnJyYmQtvGDYrp2W+W9+F+P8778N1P3vlovo/S09P/0r59e2jbuEExO/tFb7wt5H3las+LM/mTs1+034vMepnkfUXDhg2DtotbFPOzn93vg/c7iWa99LzvH126dIG2i1sU1bPfSs2P7Pdh33jqsz61wWDwG+X/zuPPe9/Pq/kZ9fs0/jvUO6/45x/p2U/X/Oj9DnS/Tz/7/63e+TiLP+/sZ2c96Dc+5OzHNT/m7Md537cdOnSAtomb1GTfj8bZr+d911S/13n8o3j256k5f2fxN6v5iOJ+wdn/nfYftD3cpqic/byaTyRnv+b7lT6f76uCggJoe7hNUYn7eGc/Xe8nNR/eXjf97C9WOX988+f5vqjXy9b7eTud1dkfF4rK2W/U62Xr/ZwZ7xder/dnWlwAbQs3KqpnP/ktX/bsJ71eQb2/KCkpCdoOblVUcn7yvoOd88H7vHm9Xubsb9+pUydoO7hVTcr5rbzvwHM+vH3e+tn/JC0t7afaGQFtB7eqyXEfW+8lO53ovQ68fb762V+g3vc5jz8v56frveRtn9GMp8Yen/1tVL3fOfzNcn623su+7aNnPDX2jzweTzPt30DbwM1qdNxnNec3qPfmDRo0CPr73a6I4z7e2x5Rzm9Q70XBYPCvbdu2hf5+tyviuI/t87Nve3Dcx5vvpnN+zfdPq70ecaEmxX3su0427sM5P2efF+b/r169ekF/u5KAv5HvW437RG97NPY1fr//F127doX+dqUI+Dc27qN3Oui+v3zkyJHQ361kkT/t+6J6H6/XYxD3fady/rhRVOp9EcR95ZMmTWpWWloK/d1KFvmzOR9vxsdKvU8/+739+vWD/mYlAX+rcZ+oz2tS7/tBO/v/0KpVK+hvVoqAP53z8fb40X1eg3dd+N7fq36/O+7U5JxP1OdlZnww/0T1Gy5xp0bnfGS2m97lI+rzamd/hcfj+Ur7P/T3KnH4m/m+lZyPrfUzfV5P3759ob9VKQL+tO/T7zqMcj7ebLfGvm7y5Mm/adeuHfS3KnH4G/m+aLaXftchyvmouG+ZqvfFrSzXe0jOR3yffdchyPkw/1Zt2rSB/k4lC/wJe6P5LrK73cpsr8b+TEpKCvQ3KkXI32yHm1Gfj8n5BiUkJEB/o5IF/iLfF9V7RH0+Kuer9Hq9Xw8YMAD6G5Ui4N+Yeg+vz6fxz+zfvz/09ylZ4E/7vuhND9vjZ3+rm/H9/+Jaf/PmzaG/TykC/iLfp9/0WKn3aOxXJScnQ3+bkkX+PN/nzffwar2cHj/m/7fWrVtDf5uSBf4i32ff8/Fqvbwev8Z+n9rh5xhx3/KyfR6yvw37vkmtF/PvqnY5OEYNfF/U5yE7fExqvVfGjx+v5rucI67v0/u7yFtets/D/FYf8f1+3bt3h/4mpQj4m/k+vb+L9X2m1luempqqevzOEtf36b2txPdFuxupWu9otbvXceL6Pv17DazvC/o8VWlpaT9XcZ/j1CjfZ36rCfMPqLluRyoavl/j8/l+3bJlS+hvUWoE/yj4/rTBgwdDf4dSI/kbxfz0bJfA9+v8fv9vW7RoAf0dSo3kb5TvW/D96cr3HS1L+b5gbyOe6/2d6vE6Wg3q/BH4fqH6vQbHq0GPz0qtT/f936t73/Fq0OMzq/Prvl+kfF8KNejvW6jzf9Z8X812yaEGsz2i/j7l+8VqX7s0Cs/00rM9Bv39/wQCgW/VvS+NwjO9ZK6P3d3D+P4c5ftSKTzPz870cny/1u/3q3tfLv0fd94G8idimYkAAAPtbWtCVPrOyv4Af5ljAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO3dS4iNARjGcRMa91AuYeNWLCwsqFlZ2LBBNtSUKDtlYSPZqtkRqVlhIYmQWNghJWUjLJSNSLkmuX80xvvKaMYcZ87t+5738jz125/e/2bmnDPfDBZFMUhERERERERERERERERERERERERERETmfTDwGghnn4HXQBiPRbeB10EYG8U4Gfp1UPUu/2nP/vnolhfsn9WhYe3ZP5fnYhr7p9X7T3v2z+OW6GL/lH6IVTXas38Off9pz/7xPS1G/8zH/nlsrtOe/WO7NkZ79o/ri1jC/mkdbKA9+8c09Nku++e0vsH27B/P6Sbas38sb8Vc9k9rR5Pt2T+OG0Xtz3fYPz79XX9ZC+3ZP4b9LbZnf/8eiInsn9KA6GmjPfv7dqTN9uzvl36uP5390xr6+x32z+dUh9qzvz8vxGz2T2trB9uzvy9nO9ye/f14UzT/2R77x7G9hPbs78OVktqzv33vxUL2T2tnie3Z37arJbdnf7v0u3zz2T+tbRW0Z3+bLlXUnv3teV2U8z4P+/vQ6ff32d+PkxW3Z3879Nlss9g/pZ9iA6A9+9twHNSe/fEeiSnsn5I+l28tsD37YzX6jBb2j+e2GM/+KX0qRj6DHzn0LTLaZaS9Dn2LbIb/7xULQ98jk1diHvunpO/xdepv9jo59F2yOGywvQ59lwz0GR2T2D8l/V1vhdH2OvR9orP0u16toe8T2Xnj7XXoG0Wlz2dBfJ+j2aHvFJF+rtfjoL0OfauIDjhpr0PfKpqbhY3P9Rod+l6R6DMaFjhqr0PfLAp9f3eTs/Y69N2iOOqwvQ59twgeisnsn9JnsdJpex36ft7tdtxeh76fZxect9ehb+jVEzGT/VP6LnoCtNehb+nRniDtdehbenMuUHsd+p6e6P9VnsH+KX0Vq4O116Hv6oX173G1OvRdPTgRtL0OfVvr9L195PMZyh76vpZ9LHy/t9/I0De2rDd4ex36xlYdS9Beh76zRXdFN/un9E4sTtJeh763Jfodvi2J2uvQN7ekL1l7HfrmVlwXE9g/pWdiTsL2OvTt0b6JNUnb69D3R/P+/c12h74/Un/y9jp0A5Q7RZ73eOoN3QHhpVjE9r+HblE1fTbDOrb/O3SPqu1l+xFD96jSGbYfNXSTqtwvYn+Pp9Whu1RBP9NbyvY1h25TtoHC5nOXrQzdp2wW/seO5aH7lOmi6GL/ukM3Kss9MZXtxxy6Uxn0OWyZvsPVzn4BfRFNEntUQk8AAAR5bWtCVPrOyv4Af6I2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2aiW3rMBAFXUgaSSEpJI2kkBSSRlKIPzb4YzxsSNmxZPiaBwx0kOKxy0Mitd8rpZRSSimllFJK/df39/f+6+trSoXfg7Iel0z7EulfU1Wf3W435fPzc//6+vpzfst1px5V1i1Vvn95eTnYY+v0r630//v7+y9Kdax6P6P/afvP4P+ZPj4+ftoAcwFto64rjHbBdYXVkfgVzr1ZmnXMOLO0+rN1ThnSP6RXUD7KMUpzpIpXaVb/5/yR/V91S/BFH/+Jz7iIL3KczPmjwohf4ppnS5VXXdexnpnNRVke8mNsyvMsW6afVJxZG0i7VL7P4P8Otpv5/+3t7fCOiH14pvfHTCN9QZsgvNLinPZH/J5WHcs3vJeRXvd9PpNp0p66si3nHPjo/p9p5v/sO32eTEr4sOxY7SbHVMpQ9zP9VN4jr/TfqB1n/67wSh8f1vlsDiAeZeT9J+89itb4P4XNmG/p5/lugO2xYfbr7Jv0vXw3GI0V+T6a/T/HkPRVliXLO6vvEo+irfyPL/Ft9rWeTn8v6ONJjrXZ92bzUdaD/Hp7yPE802TM6TbpZJlu+Tvor9rK/6WyUb4Dlm37e3v3Ne0k/cD7BGnRpnjmFP9nPMYk8iLNXr4lPer8r5RSSimlnlOX2ufNdO9lL/nWlOsgl7BhfRvNvmv699RftfZ5tT+sOdSayWzNeo3S/31tI7/zR9/8S2shrJv082soyznqR/zjMbu/lN7oepbXLK1RvybubM1pVua/iv2y3PsjX9Y88pz2wjO5zp5tJPdeOWcNl3s5JrB3sya82zrLmeuJdY/1Ztaa+rpShfc61r1MK21Xx/QZkFdeox6nxHol90mXve6lMp+j7pdsb6P+z1obtmY/vms09le83Mct6COs860JP1Yv7JdjXv+3IfchEHsZdcy1yrRVptnzGtm3/xNBnNH9kf9HZT5Hff4/xf8Zf/b+kHbinL0Zjvgz/8lYE35qvfqcl3sC+HpUp/RBt09ez/LKsNE+E/ezP3OdeY/KfK628H/fRymfUKY8LzHWMX4yltGe14afUi/CGDf4jwAb074Qc233fx9zco/ymP/5fyLzKPX73f+zMp+rY/7PuR079H6SdS318Sl9g7+Iyzy2Vfgxu2cYtuT9OudhxnDiYue0NXud+DP3KI+Vg39r8SFtJ23KntnI/6Myn/MuyH5b1il9R9/OumKP0VhF3Eyv59f92fvBmnDCluqVYdSDuaT7N+fy0TcYz/fnRnn1MNpA34tMGxM/856Vufe1S2hpvUA9vvS/UkoppZRSSimllFJKXU07EREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREZE75B+Hl45q2TuOnAAAAVNta0JU+s7K/gB/pYUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7dbhaYNgFIZRB3ERB3EQF3EQB3ERB7G8gQu3piH/ignngUObT/vrTWzOU5IkSZIkSZIkSZIkSZIkSZIkSR/RcRznvu9P5znLtXf3v7pP929d13Mcx3OapsfP7Bj9LPfUvXUWy7I8XscwDH++h3TvsmOVfbNhdq3N+z21f9U3v/6N7l+263tWOeuf5XqdffvG2b+6XtP9y3O+71//1+d5fto/1+z/fWXbeu7X79u2/frM9+e//b+v+h7X96v3QK7Vd/ucRdWfHddrkiRJkiRJkiRJ+vcGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD4QD8K+ay4UtoqZgAACD9ta0JU+s7K/gB/p2oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7ZpNaFxVFMeLTauuulBQEIoILsSFaBcKrkRwUSy6cVldKYhCg5vuRFFoNwpZBbfFZTfqRozQJsEuSjLTzkySmWSmmUynSdN8tPlqPprM9X+Hd8vl8mbmvTfvzbn3zVn8SUgmyZv7++Wcc+97x4QQx4y8uL+/f2Zvb+8sch4ZRH7e3d0dxseryDVkAikiFS/rXrYRIYPXb6uv4/OKlyIy8fjx42vIVWR4Z2fnJ2Rwe3v7/NbW1lnkDPICvmZeFyf+DIL1EPInkkM2EaHn4OAgcMyfNaPcMAMnngZOqGzCgRy8+AMZghODm5ubn2xsbLyOPHN4eEi9dmmIL8cnT560TRgnwrri54VyAj40Ax/2kSk4cQVOXHz06NG5hw8fvoTPqdfTtXRknVSC+tGpTmhOCPhQhQO/oz58Ax/eQo7jb1Gvsc0h5+xXczrVik41wnNBZgM+/I368P36+voHa2trJ/Hz1GtuU6z83w/bP0wfTBeUD3BhB7VhBD5cWF1dfRVOUK8/dcjqf9xemC606xPwoBl4MI0e8QvqwocPHjwY6MNeYQ2/uGpCp9nBmBeaHqAmSBfWkCuoC+eWl5f7xQUnuHbbH1r1CNMF6YHnwip6w5WVlZVz9+/fP47fR82pJ/ypmXXDOIoLeo9oURME+kMN/eFH1ITTqA3UvGLnT80wqf/3Vl74uWDOCn4eoB4cwYMRzAmfLS4unkjJPoKcWy9dMJ0IOicY9UC6sITecAkevIb+QM2QhH+nNU4qSVynnwvm3sE7T9DrQbMmoCf8BQfer9fr1Cxj50/FOAk3wv4u5YD86He2pHuAviBQDybgwee1Ws2ledFJzt16EdaDdvtHHw/m4MHXd+/efc6Be5jkbCidCMO/lQf6nChnA+kAeoLAnLi8tLR0EfXAZg/IObiUdmcIei1QHqAWCOwba5gTL1Sr1WfxemrezD9BD/S9grdPeOoBekL13r17X925c2fAovmAfC1tjWQb5DVqRvTbM+q1wOsJAj2hgNngo0qlQs2e+Qf0QKXd91udJerzoXTAqwUCPWEEPeFNhPk7kDg8MOYCWQsO6vX6EGrBKXyd+TsQcz8QxAP9PFmvBbIfyFqAuWB5YWHhy1Kp1OvnGsnX09X4Pcdqft+vFujzoXRAxusHAnPBf5gP3+jhbEC+ji6n1fPM5vf97i3pewStHwj0gwPUgsvFYvFkD84NyNcwDWnlgXLBrxaY50ZGPxC1Wi1fLpffRU9g/o6knQdBzgu0s4KmA+gHh/Pz879OT08/j9cwfwcSxAHzvMCcDVU/8GYCgT3i9Ozs7NszMzPM35FErQWmA3ImkA5gJjjAbPhDPp+Pc49Avk5pThQHzLMC78xQzoXSAQEH/kU/eAV9gfk7kk78dQ9aOaBmAs+BFcyFn966dYv5O5IgdcBvf6D3A21vIPD/L+bm5n67ffv2Cfwc83ckUR0w9waaA6OFQuHliP2AfD36MUEc0PtBu72B50AdM8F72WyW+TuUsA7oZ0XKAVkHMA9IB/awP7xw8+ZN5u9QguwN9H4gHVD3j2QvkHsD1QtmZ2cFHBiemJgYCPisEfn750RzwKwDshd4dUA68E8mkzkFN5i/Iwl6RmA6IOdCcx6QdQDzQA77w9P4yPwdStC9gZwJzDqgeoGsA54Di9gfvoN+wPwdShgH1N7Abyb0HNhCHfj4xo0bzN+hhHWg1TxQKpXE1NTUIerAF+Pj48zfoYS9d2Q6IM+KK5WKcqCBOvDt2NgY83cocTpQKBQa2Wz2u9HRUebvUOJwoFwui2KxKPL5vIADlz0HyN8bJ14H1D0D5YB+31A6MDMzI3K5nMhkMpeuX79O/r44yTugnh+oVqvN8yHsCQTmQTE5OUn+njjxeWA6oM6K5T0j9RyROh/CPCgwD5K/F07vHNDPCL15kPx9cJJzQH+WSDognyPSz4ekA9TvgZO8A+resX6vQO0Lqa+fk4wDrZ4hUQ7IPYGcB6mvnZMMf90Bv3sFcl8o9wTU185J3gG/e4ZqT0B93ZzeOqA/OyAdoL5mDo0D6nlS6uvl9N4BfRagvlYOrQPU18nprQPmnoD6Gjk0DqhZoN09JapQr1na0s4BatbsBA1/5QA1V/aBzgGX+bML8TiQtlCvse2h5sMeMH92gR1gD9gB9oAdYA+YP3WoObAD9KHmwPztCDUP5k8faibMnz7UXJi/HaHmw/zpQ83IAv6NhEPOuF8dIGDtrBPUrBLiT83ZKR+oecUcaqZOumABt37nb4ULFvBj/oQeWMCP+RO7YAFD5k/sgQUcmT+xBxawZP7EHljAk/kTe2ABU+bPDjD/zjlKygMLuDJ/doD5B3fgqE8doF57W6IcaPSZA9TrblMSccACxsw/vAOx9gMLODN/doD5EzpgAWvmzw4w/+gONJh/XybWOmABc+ZP6IAFzJk/O8D8u+PflQMWcGf+7ADz796BBvPvy6TFAep1dDWx9AHm73TS4AD1Grock3+D+fddunaA+Tsdk3+kPsD8nY7LNYB67dIQvxoQygHm73y67gPM3+m4WgOo1y1N6aoGMH/n01UNYP6piGs1gHq90hbXagD1eqUxkWsA809NItUA5p+auFIDqNcpzXGhBlCvUZoTqQZQ8N/d3e0qFqy1rQldA3rNv1v27ALzZwcC8+/YA9LAnx1o60CjH/izD9H499iBoPyOfMIORONvUw/4H78UgzWD0IBAAAABHG1rQlT6zsr+AH/CmwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeJzt0IEJACAAwzD/f1qfEMZGCnmg55R17+WjttK/1rSV/rWmrfSvNW2lf61pK/1rTVvpX2vaSv9a01b615q20r/WtJX+taat9K81baV/rWkr/WtNW+lfa9pK/1rTVvrXmrbSv9a0lf61pq30rzVtpX+taSv9a01b6V9r2kr/WtNW+teattK/1rSV/rWmrfSvNW2lf61pK/0LAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+OYBXPbhun8jyAkAAAQXbWtCVPrOyv4Af8dsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO3cTYhNYRzH8cm7lQVFKUlZyEJYUFZSFjKxsRxWlCiysRNRMxvKSraynA02Qk0WdlbempKSRPJSxiiPzOX/dM/UnTtzrzt3zjm/5+V76rMRc5/z+/96zr33HDMwMPtY45zbaQ6YIXPWXDE3zKgZM0/NuHlT+FaYNH8Lky1/Pv33xot/O1b8LP8zLxevMVS8pn/t1WaOpXGUfPjcr5u75pmZaJmf2kSxpjvFGv1aD5nNZhH9KOVQz7hf/nhpbpnzZtCspRPzPtRzLNtbc9ucMtvMYjrR9VDPq2rfzX1zwew1y+jDjEM9n7r9NA/NGbORLmQ3/3avzFWzzyzJsA/q/EPy1TXfTw5m1AV15qH60tKFlN9DqnOOwTtzyWxIsAfqbGMy5ZrvHY+YpYl0QZ1prD6aYbMp8h6oc4yd3xPumT2R9kCdX0r8fa2jLq73i+rMUvTanDQrIuiBOquUfXLNe1Mh90CdUQ7850f/ffPyAHugziYn/t7kCRfWd4vqTHL0wuwPpAPqLHLmv0vaKu6BOoPc/XbNZ9tWiXqgPn80+c8Kx139zzWqzxszPTFbauyA+nwxm78mjLh6nlVTnys6e252VdwB9Tmiuz/mmllZUQ/U54fe+OcUt1fQAfV5oXf+fcFFV+5nBPU5Yf4emfUldUB9LujPZ3O4hA6ozwMLc9Mt7FlE9fqxcI/Nuj47oF47yvHe7O6jA+p1ozy/XPM5E+afN/87VXp9xkS9VlTjgevtnrJ6naiO/905//s/a+o1olofzI4uHVCvD9X7YQ526IB6baiHv494bI4OqNeF+jTM6bYOqNeE+jtwrqUD6vVAY6TogHod0BkOYA0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAgPlpVEx9fqhv1nQiLOo50wdmTxeYP11g/vSA+dMF5k8PmD89YP70gPnTA+ZPB5h/2aYcPVDPQC33DqjzD8FUQT0L5q/vQCOAmTB/OsD8tR3I5XqgzjtEOXVAnXWocumAOueQ5dABdcaha+1AI4B5MX9tB9TzYv50gPnTAeavnX8qHVBnGpvUOqDOM0YpfSZQZxmrVDqgzjFWqVwH1DnGLIUOqDOMWfv8GwHMk/nTAeavm39s1wF1fimIeQ9QZ5eCufaAWDqgzi4VsV4H1LmlItY9QJ1bSmLcA9SZpSTGPUCdWWpi2wPUeaUmtj1AnVeKYtoD1FmlKpY9QJ1TqmLZA9Q5pSyGPUCdUcpi2APUGaUu9D1AnU/qmH/eQr8GqPPJQch7gDqbHKQw/07fa6K/7NRzn/YPVeP+BhIFuYUAAAMrbWtCVPrOyv4Af9JDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO3ZiVHbUBQF0JSQElwCJbiElJASXAIlpARKcAmU4BLMvoPZd37eZ/gZRZGEzWJCcjJzJjNYlmXu/U8LX1JKX+aot7e3NxqPx2l1dTWtra2l9fX1tLGxkba3t9POzk46ODhIh4eHaTKZpJOTk3R2dpbOz8/TxcVFury8fHR1dfXo+vq6083NzUxub2873d3dtbq/v+/08PDQqOtf0/Zlf0+fO4rj6sWxzzPDt7C4srLyK/vNzc20tbX12IHd3d20v7//Wwem7cGseU+T+V+ae7b4CXOv6kfuo9yBnH9WZkCWO5BnwdHRUTo+Pk6np6ePSg9ek/80mT+X+3PZt+XelX1X7rU134/v+9H5vdksKB0oMyDnX+ZAtQNlDtRnQdP54L0yf+s137ZtQ+75+D77mm/Ti4yHOf+y/uM64Y8O5PNBtQe5A109mNdaf8manzH3YXyXz3ien1U/Ml8u1wG5Azn/ageqPXjufFCfA++x1mfJvWu7ltyX47j/pVk/dQ8i+3G1Azn/6r1BmQPVDpQedHXhra7rpsm96/W2zJ9yH/+nudd9i/yXS/45+6zMgOr9QbUH9XuEtmvDpnnw0tyf60R5rb6vSuZlvX+T+x/6kftSuS+snwfaZkFTD+p9eO01flsvmrav77v0Lo5jyXqfytfIchCzYNSUf1MH2npQvz54q3v/8vO291QyH4VBHMPX/+C67j0sRO4/ogfj+vVA6UDbLJjm2cFr7xea3hP7zef1H/F5C/HZH/37+5f0IutB9GBYPxfUO9DWg6bnyS95dlS2qbxnGPsaxP575vvcLET+g+jBMP4ftZ0Tmp4lv/TvCk+vjcIw9pHztsb/Lv3ow/fowmJYCsvRg2xS7UPXfAiTsPxkKV5fDN9DP3z09wMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgHn7CUV7P+nr+yroAAAqF21rQlT6zsr+AH/U8AAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeJztfSu47CjW9pJILBKJxCKRSCQWGYnEIiORWCQSGYmNjIyMjSyZf1H7nO6enun51P/UiFpz6T5Ve9cJsC7vuy5U5ud9Np4A2rORLcbXRmCHo8MCcF3FBWsA34V+w/NiHeCBChABtgob0J4kwXfdk9mxhMHxzXxz/PFDbwCVm91BK9VxM7a+bE8VFOB4nUDhKOkw8GG5rys/9wkSoNxULKRfjQOvN4G4c1yd297PmF5CbDEp/EMR85XSDw8r1GvbKO5GeU4iOPWWZFBDzF85FsjSPk8GMCZsqzS4j0ltK/7u626Qd+7bRxePcsWw+I2Z4AE8UaqDcQkP0RQJK6+XsoVIk30M+qGuwWWhtx1/cY+5rn5+glspLqM1Y4OJNizW+rjFwMGCbQ6eHQR1T6D476g5cFz40/08LxsPLz+8/Le4TsQ6Ep6TTcKbBXApthUgFfbEnC0c1R4ycMAnD4d1S3FgAr60zV+34NrmwB/VL7iZ+zb8NB08fgCFC7QeNzdT6huBx+FO3dVCUdfh1u3z66eolHVN4Pd7j477NcglLkKmTsmKCxhrOhgJa5tOwLxtgTnYD/znAiqndYFVxXwyqIbZJTvR7xGBm6sduF1G4WHXkinPC6pSRSVIV2MwTWcDxj67+CkvdGlok2aY9dUJ0bhYhj7KyhyoEstFy8Xy4ykVltQ8DyzpNvZzNMXhwH/WNZt64GLwP6SiSh/w4PZcGzwZTxxNJU8jKDfkNuA6pxY9CZ2q6v3TiwdZQaP3woNIDbarCJBMoHM2m84DTYNY6sj5QmNYnSbHmEq9E3QEZbsuyvYS/KjPCTMuGGplKJTPP9Q8P50tMekkcJ1PAT0A/X94FBoSjAv/2v0JH108SnTCobdWZ5uaYHxJLDzkuJV94EbzDlFqXdBvJVtQYdH9AYg2/RhYElm/zTzhF6o/EKhZb2gAgEaeF/wwNjfhga0fNkpG8ZdHW/CFBXd2KZrPNz8sHORAd44KjQZuTeDHpt0TbcwFyms+P/XoyUzaau8PsxU9gN0P2iV3g1qIaXpGVHgGgRD0hCQRga9rUPY4m0W3kG3y+AlqQU+Z8dTX+t6Aq54cPn7+kobl3ODYhpG6BulCOfq14gmGC9akAjhVratLHA5Dw3a0amLrD0wL6OXnQ7wC74B5rwWhC+cejTukbRdqC1Au1AUgl/jj70Rr8RXC6nf+oVX/RcLCgDP03KjBlJGVkjh461XAhUrK/LlzEo+jEomeXISzCA7oyZ+OKzsGfQcEc60YRhDjHVEoHktJre73pljdm4TGqAq5MQvL+v4rS4/6qOhkWIwfXTtKxKOO72MIiHgknadE0de33g8QnqITWGBp1x4g7Kjr0RBAbMyP+3JusG0kgajGXtc5zoTvekJHz56gUT0Vxm5mEORrhETq9qxlOwo8qP34FmHT/D4steKinptqxu9rhzBCn1twKPXiJL8dALqHx6CR2/bMcP00DG7LGctxYJRYxpP5Cfp2z7X26BjZLnj1SG6M+41vcp9KvoDPNazxweD/SOAcdamJ8errh5ePC2bgpxYM7dfXYewYlYaJW1oXGTo+PMdNQEqjOfMC/QKs4iTTcV0VAaEAfT1IhRYMawTQ/jPGyhi646/56bK6dL9Rkz1/ggEsCTfGxwa137v97Orncw3EPpDjojP4tu/e3DZbptFnlaiXDFJMjdiNqqj5Ea0/F7coDI0md90uN0MjfkJ7CIJdr9MK1+KXVdRXArIMN5nSMX9qa36CZZRjR7u/chbLx/kf0ONE2C4bEj30y0u03O7rCMVA3Vfdx7FNEgP7MOWAkAPj++3o5LwwzlwG2vJ4f5DzrnbPcd9OWqILPiMExg2DhIzgQkWXCZmlKZWCuiZ52EF9dOU/QvvqC1nsbSjCV0lw4YHJsfKA8Qu4fL0ylyvo/eBcMrf2IO3eKZBs3Di31nRsGAUcwUBaLkK9gKPvGASVZfGFi42DUlPf9IHGg20+ZJhJgen+bP708idODWGGZMSiRzO5JY2GvCOrKT/ovM8kBQFzHxzfCQNfNT0Tsu1ZHMdCUiMtayJxR1At0GUS/iLnZq3BCMLhJdapLc+TMx436tDkzMg41E05mRmBz4oZiiwbrOjkXypuO0iCwfrGRRZCxrGGHdZjbL9++M7usecNy51bg44vc2GfZ7hJFRdFCDLlLHoD0jHaF3SBGzqSa0zG0+fOwQahze0cyJkID+Wji0cp5hzUexI3ym/wy8VuZKj4pOi38OGVe0By9VCYPhDGa8J3jGvXvb6hCyO4D2tYF2Z5kRLWRcf3mshBqc1CwjnCdU0QsNveNFA9uV8E02ySkMZnV4+u2IfdTpUU1SOWX26Zh0fvpHADcAssWoUeEv9VdZs2yJP3w1amm9OwuOUwRUuRNyp8t/0YXa97nfw3NUZc6dS2u/p6UdgVoHoh4YLHBwl1FUiAPu7/86Z1cJqy2vb1VNmju28zUCmI+LRb4F7VNuPW2vPjYCAtmmQmEuEqPbYlxMDKZlmSPL9ekoPYt2BfNp2o38h8aB24zOsFM9ihPoCEMiAZULoQ+nH/1zcHFc+Oswv91Q78LE5zvmq7Rpk9QrWK/GALqO2Bs5VDp/L2BGmOVZIpAVLpkI9ATMXfBtKuhIv/iR0Ct8enbWI8MhNGSJNScbCyHMO5Rr0e5eP491gcummN5I6y9U9trEdB/d0Qt/TSfTq2Khq+yxN1DMRmBdg6HUDKq1JImS4D8tnvirA2wvG8scM2jmqQ5QGnY+ZHT3BPLQ0Q+q02HUgX0v363Mp/S53JSubbVcDO7BY6ukrHg76div3Jdjxneo7jjOgE8SDx/wgxRipxbZktO5MNSfKNFAA3DT8D3h7iT+woWXIN2WRlxwrPyUYGyhcN5ZkJ0vrRpf+WcsXYSJYQH8vBYezHx9uh6KU+GMYQACyhlbivM/+LG0TsWgiLuUXxysauAdJxcfDs2DdwG4E/uIPIjN5LrAaQ98UlDsinJIE7D+K8Px79UaxyGI02s3BQAzdgvGGZhrjpXj2EB4T9yVLntl8XhvWZsylto4THPsBEMyMewqMMvF8nDedJ/sIdya11D82LQ8HKLVKNbhBl46+Es7LP8x9zc5XA7kzPzDzIrS8TteAbUil8THRfMbvp8sE8dfV9RQpEyHpswvEkFEjTEQ4r704IHV8VMuy/cwdjAduvLfJySJFWLqAZs6WI9Br/ztTWjyeAke+MmYUofQvgbwmy7Tpd6Kyn2zanRmhsd7GGvECM0nrGeza6UF+ZPwwBtg1F6xvS3RjQaLOi9t+5o4PDdqLmS6sML/tC6SJN0v6yaDvA1/Hx+hfnBNCxoW+/6ylnUgJtIMMkrDW/LCCURYN4/Cg/qjoTtmfAVeu1hRdGvDSemerAIAno4BYI87XfucNFNIyBBiGWs3E/EGzkmAeQ9UGu9Q6InxZZdrTuczptUh6qKEcH/7Ba33naR3GEK3cwESlOevv25+F1iFn0LcUmlaeP6MAiolkQCT0nSYb9zh2DOPC36Bh7u5ltiBtML36EuY8Zg8Ih/o/H+/8u40LvruDY0cxBPaie+Oe8sVmZywx8egT08DpmiRsjwqx/b2i5MlhqgfjHvEl8MdbYaTMTQSh8+ad2EGYxxQMTpdYNTkuAiJpMwM2rGtoun+vT6z/Sctldw3FCU6BeI28W8v4ubIAlBHoC4uKBiw2vxPdZ0uN+aYjklINQrgCIcRAe63UmNyiEBRz5VTtCAqGSbCB6Rut4144Gs4Gii02b98vyCyx8UGYMVvXWoPZrgpEnm0669GLMlC+hJEVOlbmqCkgDQddp3vtRCz2CdS0fL1TmUUFEOZOjqNJn1exX7fDgJVla765cgJ/aYdSlpOM1kE+tanKoD8vR8an4dSI549ZC2Hpwg8ys1nZspa1sPQuDEI8eFcm4Wezox3mfFdy+NXQD/YWm0hEL121Fg4F6niv8qh3vTRuxvos+qEy/a8c9i3JyDDSNA/ns6qf8FC9n/Q+aRcByEv7AflCGGKZuQt9boK5cZ1sVe6Grh5JnGqPjWdsDdlKfVycbhocKe0ZlsG0x794BjHsLAt13vgcDTP/VO5AdN6gmJJHn/nj6Y9r4w9AwnwuBjp5u3faJ8+0mEfradcVANXND6BRD1bFtnPEfOEgYg+NlZvHvucZ0DJLOPFBKWv/0jrBAg4/vkPnI3P/oHaG7FjSdS3yujyNgDhd9F2GfaxFSTuL/oCeXfklVIcJr8lcBgIFMjJta1/VEmAROS5XBpQX3zKFV4wYMo5zPxPf93Tu0mmfMEu9MfmEoXeWv3iFCanboKNFm8sf1H6O/ufRct/NC5QV9kkF1SPdSoaSgEQbOAgDVZ+v3mO4aTR/uC6g8N4cMT4u3Osjtylv3bTZ17Xb2jt3HOzOO5rU9yPzudx3pp3eMbh7o+6//+PqPlwSkpDNwS/7OTaKktqWDqKt78y4AdAuuIqED8250mho/E+DrjWRp8bBizEM2s/M9sMpFCbMZoB6tHtUOhSyApRvRrk/ICrKc9TC5aP52h8tHF4+SOx49uu/1TVYlpRP295vKqohy/KcAwOTCNJ1IGA0dOHLk2dQGS+yNgMl4uu1BHPQ6yjIN2hFlwC6prAHX3Z8wTjxnnevkg/iZJ4imyu7NNqPphyXBw0fMMdbWt2197qFeaq5u7dK901P9MAxDegGLx+1MWIYz/ZzIVYP2hE07XgXi/l4VflhjsL2OgAFhARrodgNHSAV1IuHnDTGK82tO10v9VII/LIjZ53KDPe7cjoZYfTZDQhBXNtu7AJBG3xeoXO4zlm17NCFdOf/hu63X3Eo0bukU2BM1StNzhHeC3F4MqkSf92ioD4KN9Ix69oK7tqPf/Tj/leAcUOuUXZd6nRfw87oxtht4peJ+FwD8tUo4I2O+JYHPvhOut2NGe2Tzlxvd3wMdur1vHfeIQHfFMIlRc1Cv47kSml8VzIHOID8IM3lCMsSQe3y+/wU1s6e4h33LPnh7cShhv7Lb0YJhoT8FgI7Q/lGTJfKnzGzBrPY09IKkz4J4bVdJ14aAR+2vpkPoGtL07DES6hKSCNsSa9dR1v2MM2lKaBvcLMf/gPrj+okaS7qaUoj3xcTwohXEwsj2yE8BYPrI54XKsruGjzwh841bEJ64TnfZ9LZhxNz4tqJagI7AeIlcUnR2mgHSXlpK7d1hXCgByh7IWplQRZaP6//uIDGKmt6jBaFojuD3nex5BjD3UwCQTCHIeQ7NUQNQD8yeEO0jUkDTsSY0r2GfORACJzLJAZ7Ei+C2SRWsRcc4WMn4SXLVxAo0qBOWKnme/WIfz3+Ly7zTGi8jiQ14sN3R3DvGMlJ+FwCqiwH14hnW4U83z+2iaO+T1ZhVjvNeCKdrBPQNu5ql46co5L6gLKWInzIYh/zXKc9DB/c6KNmQO5ccUTM+vf404Sn6JYj51GI27hdCOAH9XKAUH7MAcLX1msnsq2U86rrtU+m5EJCC2OzaK9Nqc/DEcIyEuAjfJTwmGXR7Mz+MowisfE4GKXA3EWKZ1AJ/7uPpP9RhpGnkRBO1V2wIf5IWAaG98IhYl58CwFraPjt1+J0ppGtvAykjV+HIzVOabq5jUr149JR7W8BzWHYxpKw5NYkRX6warDBL6Rj1wRiKEbbVmTfaPp4AVHChNYeLuNm0pGwaM6VT/CLYnepM7r2IWJDqheedq1vhNW32ofgODLq/UQA9InV99pHGcM+YKniNYvbVibru45fjI2lNK7P5QLtaIZAJ/rfPrn5q4NJZlN2sFRiRobTSJB4/NYqVoG0GdOp1iF0ghyWOQI733YU6DjRoONuDuJihu3R17BczwDv6Cs6RT6QxQS9yi78EvpkFChvGEc9SKjXAx/v/y+xp3CZqIwRZHjI6uiRaCChhrWTmQN8+J3oKnhQGhNdMEKyvs6zbAhfrh7apvTZakNHAOHxgG8Y23SIC5YxYATHfX4APegUnEA3uRi2p97vRj/s/sPpYXgLyC0E6PzEIogc72MxoL0sYnlZCJ/UHDPx2T24SHxnPBEZT8oK8yQz1Bsak6rDvzN5Rez1raDeZwBdN5a/Ad1hR+XD8XHbvzZPOTy//ti7F9trxuQr0jU4zt81IS1LwyWyKS5Yim3EdD/KUHoleV9wEs2iBvDF3dPke46ALaEAHAqes0TPwZRIfNv5OfJaSF7bBqYtJO3nuj/M/HwM4dFsGg1vpIZEL+qW1JCwfzq5MrbdlliKPBXqm5SVJ3oZB6mvczBcRUuRsITN1+jjg2oF5E9/rPxNfnlfF6b0pg0FiQ9L16fVP+SFyer+EYaKkNVOxzW7Wl6OziBEjwhQ8/TQzeY/cNiKqFaDSUv3q0fTfg0OBglEE5b8mPrhbj7wjCkIASM3Hvd97dqFl4AXXa0/D11TJbHEoj1VIA/DNtWiPDwy73ZQ4ELosQHSwtfbIw9WCTNt7cAi0GZX8H4kv2CrLTCKNFGRfeQwf73+fayw07gtHzJb90WJEPizBzy5vaxIi/UQ7hnw3llsuFRy1RNZD7RdBnJ8R5COJacfm6Wz//K+Jz5+hSdas0BbyCOLz3h9Ev3G9XSveGGVFCZXyll+rLS2gmYOmC9qwY6kcm7Po54Be+L+lTPQSmHGxMX4R6xBDkN9Dk/+U+J5DkzmhjghnTo0R5PP9//sak/VIyAQ4QhZraOrnq0rBjiNapC1g+laBb6eZTcthIDlyGBEXJAAT7tW6FANaLbxo82to8h8KHz9DkyS3CftelvF0xI/3vzlkKJE4FlDdhV3atpqj13dbEqIBd2wY6c87tYxkldRul9eG9G/OS6vojWT5DEgapt6EKET6r4Wvn6FJbvxJzCBN7+P8XygA+YG8DhnwGpySGO7wNSk2Ekgv9vXMWc0xh7ggsVFS5oxrHyxuy9b7WEi9rQbKifAOkYPKyz8UPv8YmmRmkwQB5yY2s3/8/L1eRX8VSpZtixIUqul03sh7pUOXtZu9zEOsAmNgve7ZMMqFdh41HcPCeDzkg/NcOVkCt93/Y+H719DkfTHaMDYi17Qh1o/zn+s56mRsOieWDPsxSCLBPEhOtgImXQvENc/2jza2OcchFkntMTsikMke+O5ZeEHP10stl3n1f218aH8fmgxkHA2iIl3wz9f/2+u5CFW5LmFrq2diYncyNKyNpv2Yg8BqLbkgUQ6qzMIAT2SWLdYE1sE6TooUCWRHp5fLpU3Z/qXx5fj3oUkJVvhHPbNX+H8hAXI26Zt30Ugz87EYuxb70nAi8R3X24sXDAG5oYKjI2c2KnilOR/wroTva3tIkK48V5Co9gjt3EIWUd+NT+e/D01WBBH5hXtLaPWfXjzMRn8ViVcNHTzktUzAhsf9OnckfLBvWYCcLVFdPBPKq83aIeEh5Z65+/BGzx5xQBB9M2ahUvglHbuYjW8VxL8PTY6j0AZyr0T18vH+DyvLTnzsWc1Z/JmONv1qG5dyAzHRMRVrNPj6aSdYyRn8ZoNcOtxlrt689yDcfrlQOZrl0jHt342Pswr2H4YmN444UaFhcGX1x/Hvhuj2iDUgOW9zpk3aeZcJ9UsELdHbdYqkdRY55twHQmR4N0iHVpm+1tgmpl8PqK+dIUPyo2wBGGdMDiD/MDSJsX+3eVP3AqV9fP5x2bPea9Dw7AHZ+sxirnM6AWa6Jy/Q/ILADh3jvLNAIf5dJbmD3Hoj1z3ESqRzx2Azl39XIGV6PI1QSUfyD0OTgq77MKhA6DTtx/u/CwPV3h77NbgCNWe1lXj/Y47tVL9H9Nz7VRn0I69S1BtDQ8Y/dGR4xxz0hvhMYIzGgTin9evpZGdzVOI/D002fSwMAl+dmpMgH5ZcgmvZrATe+J5sdM6EbK9zoIs6bSIy1+M1t2IBZVxdCFzyDMub3OR7eGHfTG+5i1HTf2xQd0s3jezpPw9N7qWJAF5hLNUfX/5sYijUwDGHP/G/64MG7fMOzzOTHYTdjF43otv2OvAQhcveg8PDXrp1c6zPmnFCuTgqwY3oaIBHeIwfsFn+D0OTbTUCg01+7XtTH2fAOW7okVJYlh1DfVv5q4sXn2gHT850Q5uXMSNXM+gHKpr7Oju9Jl8Yh0cU29uCtCacSHyJ3dDgweg1gkyRif88NMmD7/JcYgWm+8f7v4YRl0Q/XWZNe1Y2KoJT5DyHm9nbZZmNMCygIavYDUG0y9i+vOf2heSh9oxLuAifbaScbZ3Bxt+Nw3KLnb1P929Dk62kmvy8MokKCB/3f9bhI4PDcCcktEaQy79AIdJ7MJ4XVoQRpllXqdjCb2WtLKmKJ6qLSCe6v/dg53L9Mc7i2ugVgyOazb8PTVJTlhrdEBNZuo/ff5JaQh3QaMR8lniyt0jzQA0221l6aVcfbIR3URPBDBEc4X2CeXEPF3PgreyzIWCrsx9+eSOiLU8Y3QvVkar2t6FJoliV95Bt1ssRFH+8/gfxqMx5z/GB0fWffO/8KjBvQKKBG13bk4leKGBQDxHKce2rwoN2tq1lZrcB6c927ieaT0E9QoD7HoyD3YJw5O9Dk0ojCryoEAzWnp6Pp/9xleY1sQ1S0cPuF7qA64F3VibthSkM1KmD2W5AcG/vjeeyXd3MezOsdrY6C/oOGMf6tYbew1mR6M1mKmFX79JfhyYnCkprMG6liaKvRLh46I/7fwuUXC9Ik9zMyUQM4XUDznEPWpZc2oxHK+WVtVgLf+xapVQ+eicRN/lRh4FxEZuEuY6+ucmM7QIjS+JSLvIvQ5O7B1bW3GfHUdfIrKjl6ePzH1wL4hDsYLi3P2Tc2xcxebOU5XVN2zbGtThaWF04w/hecIWqd1HrFkW+5w0mCO+Mh60xFmZyE1KaA8FLafvx59AkEEekFs4T0/DU3Zydj9vHAdCVGB6Mr/BoMyeBwK7C+JS3kwbHe7wcFAGxmh4eOzvWfkag9kvuMzfQa5oUlsx1PAhw9rVkyo7l6IgrQ6h/GZqkCJkMjVLhD5H3TXq5xo/nvzcbKW4A0oAIqeYE9tQgbEUDDkcdG3nNbL2HOhLMkf9Jjd7tkm8fsULsPEFcjoyaXDaPZPDo/Uam4HEf4M+hyYVRiVvitTE8a6ju3U7DPt7/l1MlfOuCztCV73MBVHXGbGXB9ZJimkF9Qbjr5u0Wns20/jHj/RswwEF7H8lL+ZPKmBsU07q8dGrRB/LH0GQWTEk9cp4JEQ+iUFJn8/vH819MYrhSs6PpDcWe6xBsP6vikJSeKSGw1luriUbC5ghv1ucLd2kmAmtelENWKHRAcPxXMtP3sg7ze2jSeIFIl0dSbrIEzYmMZREEQ2L6eAXUibCBquk2R8GzqfcdkayNUYXWZDI3XMzYq2ScU5EbyT1cu0YCp2YqvDDpkR0D26MA3A5PUAOQ+sc1KHKEWt+ZE3hRkRBaFj4IpX5HoEFlHk4t9eP5/2pZ9Nw3l9K+bjv6bj/TuSJQt6940n0Wh7eVGhYQHS/gTuT2GADeVzrdiia0l9e+htk6eCIM6q2l0YMQO4bEUucU7Y6UuRcMga5j5JuF0Zn1sfHcFf38/RdFbG1HwqdhPY8LF2gI8hbCqEJHX+Z1hbPXWW5a7KutRllzIPRV6bUiFXpNGybLOsvdR264Ac917S71RFiJGoPJNVhuFByawaH2Aps73n221KslWE8/vX4yJvnd2BzuuAdGcmpqohEYoh2FOIibC3lBysbkFyqxVxAJEaGzE4mAqdIQSZDSEZj3BJM5L7mndYJiKfWBWrNsGDrrDHPhvA65IDiyCDXAwEr1mj5+/2m0gZyBkNDzmEk8kGud7Q7Ctg2I2aTjXqJT13iaW4voB7LWcw6ArUdEF7jhFsDjKIYAK4mXIkWjubNIbtaGQV+b4VxGsAta+b3ZGSXSzBuLksTSP97/NGC1BKysd53XHl972TehHBwSuRAi9N0wq1ntBvGuQJNmfZiltsn/58VQRWqvbcjadjrvUcgeHYi/BO/S3nJOvq9bd8z0nXrgKvaxijUcCItjP6JqH5//5RiUrJRmnTe1tZc/S1/RGlCd0ScsIHNaKG9UDXyR6sOTXC0l6uiUkvtohJLseYPB+MXzylwJY0svFwnLp1lH1LvakP6GjRLReiZjIgwqxygs39F/3P+3ee1Fn3EomnkHmFv1vLIccWDlYaA3WMS83eB+EP/B/qS+Uq6l0C/myXtokmiF8cwipmf4wxoRPXcImI733aD71ZeIioQ/+tPp/8y2kXUSTh1oe9xnFw/z+j90caqeiG3tLOWidaJb91nC89pvdP8GoSv0gBQhq2hm2ucuMl3s3bk/hyaVnHdB4VKItL5Gw8S+67a+EVVlrYKrByX9nWTPy2wCG7Np+IGL2v5x/pdNcybnNplYm3cWLSbOHhZZ7b6FMyilrZlHOZGse2PXgczWrMe/D03m3Tujoq3pHHbe8PqAboEil84IAe1itR25KQS9PIPXvs3c8YdlX/AxthUd/Jxw6Oj35333qzEx9N1GI5HfWViDgXAVpHEUGl2X3HOOfx+aLFvCJSomHKEGsUCDHUS8ZvPD0rlBh9mZZnOUDL3LLKiD3j6//jNZzxzUlRcIO+c6I2hFTKzXnVsBUk9ki8oRXkfpmkGNy6lm335ZIf3L0ORF5eoY8QhuF7cO9Pwwr37F4C+rQQ7d8oEKlkvlbfeCAbEQPl7/3VdZonGGIrUBEhOl4jwYCNGGRoqyzusqYwe5vToaeNt3hHykzZ53rZcl/WVoUmew5dj6Aebc5mS/Oee0/MyVqsvDdp4zwHYNRGeZjWjnPj4///Iz6Ylon1lEa5BnQ+MoA8q5EMKDqtSVjfTXU8kBt4as1Jx86A0RMlHB/Dk0qSjxvT9PRxSVUTM0hQ1m62Njs7ZQb3ADVIBZYYOWVyijPh/H/0CtdONYNIhg8ExHptmecJUIi8mE42Hv45rFsGweXKRbOYJj+zI28+JVDn8MTTZmLLqK8rzLACebF6QRhQaeQ9DW8TT4aTxE924Esu+hI/h4/JfQsw1IejXnvg9bqgqyX6nPwbfoG7RRdJzBbYl2TstDX8zxYKCHeOjR/OJ+DU1iCA1zABbXFFBFeLuGx9iHO+LA92NXwReMKm5cApjWP5n/j9e/doM6Twj1sTNAZr4fg8LSUs8mxmXb8vXzHRXvx20Flltt2ZxDB4SH6jVmFyj8DE3W5NbZTmkDv45ZWNB40KgTpebVPac0CnnESBhPkTzknjB8mo/nfxwTM/SlzBAIzFv/9kIJOn9kMZEiWtlPJCtLePdpzJI973OY5Uq4/oDUZ6aIyAwFft9pW1J6J4YYvJoHxkcVniOvdpGXfdo+pT9XfnAfr3PPoD+e/2uz3kH310vDcsW1xMXOa0CWSfB8Pl548HO4P/1c1fBgLEQb6OT1zJIBqYywjvs1rwfpnVcDF4/b/MleoxPo+Od3C4BE0xm1TQeI4Rb4WGZfODwlfB4AEzhf7JmJcBJQ8zGGhePuhFf+wGxt34OYk4pmPzSe/by7Or3yzIEPk1+j1JR2IPuPHftN4DtrnjpwzdZ/sh8O4hyNX9b54XNq2I5xd10kRoejfRz/ohW7easN19f7LGIYJ9XosE6Hzv491G+59tb01DAsCvWox/+6u+J+lsZNix6DxPsKWZVStImlNOI2KyGPlH1AfnWHarBjdJ1D1Prg9VAuxVko/Xj/146PoL3XerU/NxIwxldYRtyjvm8bA4wbvbevizN6DouBioAwCH+wFq4QwWM4qFKj6kexomcfmzDg9hMMAqZUl1XrGvjyhL27BIudd60iLzSz3taPj/e/vu5DvlFgWwV7T7OTBLpjyG6vXZUDtiuVe9t7ree83tXOC04RIYEzlYE8rt7HVu2C7Hl46SwhQwrmmWKyLqDqCGxm1tflwfgnDoTSwVwg15/Oz+3j62d1LBDOvLe4mnctLxb03zPbpfm68e1OsO3iWCibYw2DjtPib/VNEUTwkXPKGaJhtyP8IzB7Yw3ByMDwJbV1RFdDQgETpVqAQenNWja7LNiP5/t4/QsoWiWHsbXY53eA0cDhikhiBmhUYjL5/jwk98YqY8C85ghua/ezlF/315CV8KvQ978je0QrQhA8mSHix/xTL7xn/wPDj2D4OZStLl4HXZ+Pw5+ZxkPtzCs+mewz74MrlQX9NcbrXaQGcZ2HhMRwpmonCnKvObW8RkTIrCl+Ogzj6BO6n5c5R23c7JN4MpKl+S0/cwaWcmFHInl2VbOBcGE7Ug8PAqvn4/j3xIOcFyDMQZ9cJhf6uZMK/z+NI8QH7G4J2+0w2mVljb20k2R+b5Jx5batryEAIceyUF5IKT6+b7XryJEursS8CJHUtj1IebsZN7RTtC1NAr0K4T/e//Q4eaNjts4Rmd+ncROEfNwjCN41Ivky0JELh2y1bSOX/VWJ0coOu+z9ZfzOpM5Whs7IYhdNkBSDpM2YBfdqQcxjNwa+Wh8K5F0+CzS9Z2L2CsQV/fH1cwkyV1JzFUtnA+023gjm5w0nczhxHxt68VRUW5RSm1t3xADNKUmLlzn4NXiljtxXav3aDSOUIW5OK3pQksTalBPiCcFLEGfissHeVEWMLAfCAcH5x+s/s6V76V5Sf6hE3aU9tARSpXVeesOuY6+Sp7PMB6UmRA68BIknaTc0+FMVy0q9HN+Uj+0mSKXmVakbR+C7HFsR+4LhY3IIw82mgYo8+pKLoR7Xv34e/ok0fdqFGJ7taKKwzjuv/PJscEFa8LQlkljUWhY7dK5RP4QTsff3HQ6e83mZ72sxK8azdTbCHVurqczW6IYM4UT1mWM0v8ac2vPQ3SpkhJVCIyF93v9lPsdzYW1oobn/6kczY17nHuaXOHU587y1lRviuIjfgs9V6XmHh0I7ZgsiWZBpPdZEpws9yuIcgsE0ke2KJqGOkt7XfL5D/ZPSM7vE95pnXdh+/P6bV2dqBmhTSVhVDpORIjFBNUYef3I0BtcSe/zh3OtB5JfpbGqfd7hU8M7hlt10Njwd7y9OwaAgjVz7pPXzq1KldMf7DphhfAzGaajMzT6JVC6aV28+Pv94jJXPr7xZvObIe+e3twBtLAdKsntnZ33Jdn4p6l0PF9HmcyE/d/jo91ibiYHm6JgeR5dGsKVsITeOhlWc1nxDbuEWZu+zhTouQG1xJa7B6IeUsX/c/9NSBhd1Pwculo86r+hhQuu81rrMzA9FI0ccg2cneVirROX/dYdTV7rkmceKRCmMmDIx19G1GYlWtYhhZ1es4FCOs7Jxjb3nq8/Iks8LA80Wc5QfP3/CtpVA5WciKartquepc1zWVPLi9HveAeqrZjNn94lvAtH+zx1eEHc6Xuu8IgCV3Xu5GKpkI7MVGCHPhnTgfaksbsZ5V0ZLdgiPwoRYlBI0loN8PPuNQisEoOiuwjiIaT2PLTu0CLNYCTUcbD0veGzq8453lZbl9x1us13sIAoZ4CtT29O8LHvVngCvL9CU4lYAofu7Kzw8DdjMCKSuwG8gHp/i3ufo1IdlTnD5Xk///ha82fmOT3YLcVK2IKMTd0gBRjP73YHfPW/9jzv8YH5rklLPA3dD38/tspR1wqbjGWuhakWYE3z7iXHPqY7UFASCS1Yszwvgzyo/3v/+eGvh3H1RkHjBVbnpEwacL03b/N4DxMLhgT2dC6TVsHD9vsrmPeeKkAgezl54+kIWy4/3F97aS3irp9NA8FuQ8s5Jmb7UWUJdFlSqpuKekAeZj+f/+tFLcQXJLgLhvYBQ1tt3G/+8w9NBR1z0mlfCz4uB2OI5+eMOzzJTHrOX5UFc6JNZXJzfeT3HqPBHave+zOnH9dWiwk3uQBrijHTUgraEdgNEf778gw56ziuy2cxCDsS6XLefrPy8w9WshffZ6zbL22uZNkz+uMqm2lLfX3L9bp1sfFVBz68QPBEKornLfKayIYK4O7oSwTiZXzHcZ+lz3o35xkOfh/+/5CALPupWQol+5iy2ua4ZoMuYX/8mZpnk1Wpw8S9X2dSNyndhAPlPILyasEgMEjPJ2/v+vgFJYJjI8nXY+RW79bgx6s2kyfu3CMjP9/9/5Stf+cpXvvKVr3zlK1/5yle+8pWvfOUrX/nKV77yla985Stf+cpXvvKVr3zlK1/5yle+8pWvfOUrX/nKV77yla985Stf+cpXvvKVr3zlK1/5yle+8pWvfOUrX/nKV77yla985Stf+cpXvvKVr3zlK1/5yle+8pWvfOUrX/nKV77yla985Stf+cpXvvKVr3zlK1/5yle+8pWvfOUrX/nKV77yla985Stf+cpXvvIVgP8H3ZoZmXcppvcAAAGsbWtCVPrOyv4Af98zAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO3XSWpiURiG4WRQkHFG2UJRS8gmsoVspJZRO8g2Mg0oKjaIfd+jyBVBCilO9JJINuA9pHgGz/yF8w3OfxNCuOG/81goFP42m83Q6XTCcDgM0+k0LBaLsF6vw3a7DUmShP1+H7uT63nO5/PhvIFutxtGo1G6geVyednAbreL3ch1/SkWi+kGer1euoHZbPZ1A7H7uK4fh8PhtVKphFarlW5gPB6H+XyebmCz2cTu4/ruT2/eqdVqod1uh36/f9nAarWK3UY2flar1aRer182MJlMzv/B2F1k5ymXy/1rNBrpTTAYDM7/wdhNZOv3503wsYHYPWTr9ng8vpRKpc+7MHYP2btLkuTt4yaI3UIcD6d/4PB0E8TuIJ5f5XI5dgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADf0zt0mmtC0h49BwAABHhta0JU+s7K/gB/3z4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7Z09r0xhFIUnPhONQqXwUaBQaRGFRCQqQilRCQmFRKIiEp1E0ChI1DoFjY9CKaEUuRXFlRAUEiLiJYx3F5OMe+fMnJk556y117tX8vyB59nFvefM3NvrOVu/3w8axNvQvtTwNrQvNbwN7UsNb0P7UsPb0L7U8Da0LzW8De1LDW9D+1LD29C+1PA2tC81vA3tSw1vQ/tSw9vQvtTwNrQvNbwN7UsNb0P7UsPb0L7U8Da0LzW8De1LDW9D+1LD29C+1PA2tC81vA3tSw1vQ/tSw9vQvtTwNrQvNbwN7UsNb0P7UsPb0L7U8Da0LzW8De1LDW9D+1LD29C+1PA2tC81vA3tSw1vQ/tSw9vQvtTwNrQvNbwN7UsNb0P7UsPb0L7U8Da0LzW8De1LjBsppZ7hZQTOlOjn9hejf7FYf+OClxsgcKbEoP/fzGkPN0DgTIlBf+NP5jj7DRA4U2K4v/E7c4j5BgicKbG0v/Ets4v1BgicKTGqv/E+s5nxBgicKVHV33idWc92AwTOlBjX33icWcV0AwTOlJjU37gT/WWp0984xXIDBM6UqNv/V2Yvww0QOFOibn/jQ2Yj+gYInCkxTX/jGfrnQQJnSkzb37gW/WWYpb+9KzqCugECZ0rM0t/4ktmEuAECZ0rM2t94mlnR9Q0QOFNinv7G+ejvmnn7/0wdvyskcKbEvP2Nhcy6rm6AwJkSTfQ3bkZ/lzTV3z47truLGyBwpkRT/Y1XmTVt3wCBMyWa7G9civ6uaLq//T6ws80bIHCmRNP9jeepxedCBM6UaKO/cTL6u6Ct/p9SS58dJXCmRFv9jVbeExM4U6LN/rbtTd8AgTMl2uxvPIz+1LTd3zjY5A0QOFOii/72PaKVTd0AgTMluuhvnIj+lHTV/01mdRM3QOBMia76G408EyJwpkSX/Rcza+e9AQJnSnTZ3zgb/anour99h2yuz4oROFOi6/7GmehPA6L/2zTH8wACZ0og+htHoz8FqP4voj8FqP7GnllugMCZEsj+96M/HGR/+87AtmlvgMCZEsj+xtXoX3T/j2nK90IEzpRA9zeORf+i+z+K/kX3t58Dt9a9AQJnSqDbD7gS/Yvu/y7V/M4YgTMl0N2H2Rf9i+5/K/oX3d+eBUx8L0zgTAl086Xsj/5F978d/Yvu/zlN+PviBM6UQPcexYHoX3T/69G/6P4L0b/o/saWqhsgcKYEunMVlf+LnMCZEujOVTyI/kX3/54qvitK4EwJdOdxjHwWSOBMCXTjcVyO/kX3fxL9i+7/NY14H0jgTAl040ks+99CBM6UQPedxLK/F0LgTAl030nci/5F91+M/kX3NzYM3wCBMyXQbevw33MgAmdKoNvW4Vz0L7r/3ehfdP+X0b/o/j/S0HNAAmdKoNvWZUf0L7r/4ejfCv8AVXzGd/KhAucAAAMXbWtCVPrOyv4Af+9oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2Zoa1UYRQGISGgMDg0PWCoggaogFAAFRB6QNMJtIBAoBEEuwKW+wuSl5dl3wqSOTN7J5kG5nPnHA+Hw/FC323eozkej7v/z0u3/7n5ZN8/56X7v52w/WJAs5KXbP998/G+f9JL9n89ZfvFgGYl79r+2+ajff+sd+3/atL2iwHNSp7b/svmg33/tOf2fzlt+8WAZiX/tf3nzfv7/nlPbf9788XE7RcDmpU8tf/HqdsvBjQreXv7xbN9/6vx9v7vJ2+/GNCs5M3tf0z58ZxjQLOSN/d/M337xYBmJf9u/3Xz4b7/1Tn61nOKAc1Kru0/Tb31nGJAs5K/tt2fW7ZfDGhW8oNp+8WAZiWf7vtftzboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1q2qB71bRB96ppg+5V0wbdq6YNuldNG3SvmjboXjVt0L1i/gEzUrdpD2X40gAABh5ta0JU+s7K/gB//A0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7d1NiJVlFAdw606mFWFQCLoxzEiJ8COohW5EU7AgJIMWBbYYxREH2tgq0Ra6CHF0JSKC4MZdC43EpIXRyiCIAjH6sMQUF2Yx98798jzNTI23uc993+frnPc5/4E/sxq48/w45/m478ecbrc7B0EQNak1m83nG43Gm/V6nfuzIHGzYGJiYi1ZD9PvMcplyl+U7lS4Px8SLrVWq7WSTEcoZyi/zHD+J1T3veH+zIh7HiXv9eS6jxw/p99/Wpz7hft/QMplCfkOk9tZyl0Hb/hXK0PtdnsjOR2m/ODhDP/q5OFOp7OWbMYoNyOYw19eZprfiGwOfzlZQgYHKNcTmsOfNzWa0zfQ2t2s4Voc7tj/s2QpmR9KMKfbvHvDPSYasobcTzcnfwqb0d/E8IZ/mpj13BtkcVlAjcM/XeZT3Y6Qx7UUte5oDv/wmUtrOnMu97vQOv83jUZjZrjHrep5hNzfI5cfY9Z6QG/4h4mZ37eRzdVY7pG84e+fzeTzvST3kubwd8syc2ZT1DO2u4c5/MtlgTm3odRDuzOZw79YzBy/gzxvZegOf3ueI89LRUxjuUc0h3//mP3c3tC9XqA7/P+fVeR5hcM9sTn8H4w5sz1CGfhdbEbu8J/Mi2T6baiar5C7dv+HaJ4fLTLPh6x5Ad7w73YXkun5UGv7irpr9d9Kpnfgrs7fXEdvzvA6KewFuML/vzxNphcHuSqqeU3+5tq7n1HzKv2HybXha5+pe87+5gz3RIp+L8AP/g/mCXI952uvwD1H/0Xk+g36vUr/l8j115j9XoAV/GfPa2R7FzWv0v+dya/t4tgL8IF//5j9XRv2Kv1HBp3lwj5b/70++zvFc30O/odi2QuwgL89B2PdEy/AAf72fGSzx1yftf8o7NX6v29b57vaCxh3KeH2teVd2/4e9ln7v267Hh/2Wfuvbrfb90Kv8wWMtcRwW/dmMdW287MwYV9p/ycH3YvjcrYjYIwlh9t8OuaarQuu+zzYV97/JOzV+o/AXq3/q7ZrtGGftb+5D/M32Kv0N/fjfdnPGPZhU6/X+4XL/0jI56Byj6+kWKyl+L/lst6DfRBvbv/Ftvvvy/Z97rGvqDmXv3me4heY81m9Of0/hL0Y99T+q132+VrtE7in9H+M9np9310Kexb3lP7HQvV9bp+M3FP5v9J0eMehFntG9xT+5p1I36Hvi3RP4b+/bN+HfTb+LxR9hrqWvi/AOpW/Oef5KkTf5zbL3D6W/y70/UrYx/B/ioxvo++Ld4/lPxai9rntlNiH9l9OxhPa+74AUy7/z0K885DbT5F9SP8t2vu+AEsu/yHb9zsa+r4Ax7LpTCWE/3bNe30BlmW8e+Nrb874f4K92PRzD+W/U+teX4Ctj3sI/3lU+7Peq5177Qvwtbm3E/mPalzzCTC2uZex9/F/nGr8prbaF+Bscy9r7+O/R9ucL8Da5u5i7+pfo3n/GmpfjL2Lu4//NtS+CHdfe1f/rzXVvgD3Xntfcx//dZpqX4B7N5K7q/+nqP2kNR87ZeyXkan1fSuwr5R9Wf9PtNS+Evsy/uZ7nj9Q+1nZl/F/W8u6T5F9Gf+LGuwZ/Tnsi/ovJVfru9Zy8VdmX9T/YNF1H+wrZV/Ev0bON1D7WdoX8V+vYd2n1L6I//Hc7Rn8uc2L+g9p2PMrth/kvwnrvqztB/mfzL324d/Xf67tOa25+Cu3t/lvzt0+oT+3sYv/0dz9YW/1v+rqz+0Kf2//Z6ctc639RP7ctq7+u4vYV9kf9lb/c67+3K7w9/afR65/o/ersJ/Nf2Puc39k+6r7f4zer8Z+Nv9LOdd+ZH9uS19/833fPfir9X/Zde7nNoV9EP89RfxR+9n6n0XvV2Xf638d/mr9nzHjk/PcD3+r/4Yi/qj9rOxn+n8Af9X+p7D212U/Pj5uMu1/xYwR5n4d/lP20/5DrVZrHGs/tf4rzPjk3Psj+LMbBvTfav4n+Kv0vw+gcSHgEsju0AAAA6tta0JU+s7K/gCADO0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7d3Ni09RHMdxT4uRksLCU6zIjiQLZRY2WM6CiCUli7GVsjCakmQSYydPK1mIBQvNSpSFiI0IkzykKRHDZbjOWZgkxr2/+3DO93zev3r/A/f1u7/b79zzkGdZlhuo1zXJf/I8p/oK7Vq0Afyl/S/jL+1/H39p/w/4S/v7ZvvvQATXLKVCm5ZpFf7S/hvxl/bfgb+0/178pf378Jf2P4W/tP9F/KX9h/CX9r+Lv7T/Q/yl/R/jL+0/jL+0/xv8pf3f4S/t/wl/af8x/PGP4JqlVGjTMo3iL+3/Hn9p/xH8pf1f4S/t/wh/af87+Ev738Bf2v8S/tL+p/GX9u/HX9q/F39p/834S/uvw1/afyn+sv4/XF34y/q/zNj/Q9n/Jv7S/ufxl/Y/iL+0/zb8pf1X4C/r/901HX9Z/ye/7PGX9L+Kv7T/Ifyl/Xvwl/Zfgr+sv1/zPRl/Wf+h3+3xl/Pvx1/afxP+sv5+3G8W/rL+42f+4C/pfwJ/af+t+Ev7L8Jf1v/Z3+zxl/E/h7+0/y78pf2X4S/r//Rf9vhL+B/HX9p/A/6y/n6P1/G5vvjL+V+ZyB7/5P134y/tvxh/Wf8H/7PHP2n/w/hL+3fjL+v/1jUNf1n/k0Xs8U/Wv9BvP/5J+r92TcVf1n/C9z34J++/Fn9Z/xeuKfjL+h8rY49/cv5r8Jf1f579sbYffyn/QuP9+Cfrvxz/4IWyv9WJPf7J+O/EP4pC2H90zcQ/ikL4n+3UHv8k/Lvxj6a27f3artL/+fFPxn9/FXv8TfuPuRbiH1Vt+l+rao+/af8e/KOrLfvhrOD8XvyT9N9Xhz3+Jv0/u+biH2Vt+J+pyx5/k/6r8Y+2pu1v12mPvzn/7fhHXZP2fk1nF/5R16R/X932+Jvx/5bVMNaPv1n/C03Y42/GfyX+JmrC/npT9vib8F+Pv5nqtr+XVZzfhb9p/y1N2uMftb+f21nLO378Tfrvadoe/2j9R1wz8DdXXf4H2rDHP0p/v55vDv4mq8P/SFv2+Efn7+f2zcPfbFX9j7Zpj39U/v7en4+/6ar4D7Rtj380/v7eX4C/+Tr1L7VnM/7R1on9l6yhuV34m/AvfFYH/tFX1v5rVuCMPvzNVNZ/MKQ9/kH9R0M+9/FvpJ9/7EvGdAErrgAAB/Fta0JU+s7K/gCADpUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHic7ZpHbBVXFIbTJbJKJBLWkZJIWBE1IJlNECxQRAhEYQNCbJAgCAiyIooRvffeewfTTK+m2xQbsGkG0zumgwEDgehkvhHXGV7sZz8gmjzzLz65PT977n/Ouf9/Z+zx48dWWFhoT548sadPnxbLs2fPYqKk9+Fv8Lf4mw8fPrT79+/bnTt37MaNG3blyhU7f/685eXl2eHDhy0zM9N27dplmzdvttWrV9uSJUts7ty5Nm3aNBs/fryNHj3ahg0bZoMGDbL+/ftbnz59rHfv3n/27Nnzh8TExPfMTJSOPXr0yN5mDZRV+wcPHhRpf/XqVbtw4YKdOnXKjh49allZWbZ7927bsmWLrVmzxpYuXWrz5s2z6dOn24QJE2zMmDE2fPhwGzx4sK9937590d569erVumHDhmGvaTxhBQUFb60GYtH+7t27dvPmTbt27ZpdvHjRTp8+bceOHbMDBw5Yenq6paWl2bp162zZsmW2YMECmzFjhk2aNOkV7QcMGFCkvceYli1bhr2e8YY/g6kBtAG0KqkGXkd/9368N3WG9vfu3bNbt2752l+6dMnOnDljx48ft0OHDtmePXts69attn79eluxYoUtXLjQZs6caZMnT7axY8faiBEjbMiQIZHap7Vv3/6jjh07hr2e8YY/g6kB+hJ93AyItQaiac978t7UmdP++vXrdvnyZTt79qzl5uZadna27d2717Zv324bNmyw1NRUW7Rokc2ePdumTJli48aNs5EjR9rQoUNt4MCB1q9fP7fnn+/SpUvFGjVqhL2W8YivBbO4uBoo6z5Q2twPan/79m3Lz8/3/d65c+fsxIkTlpOTY/v27bMdO3bYpk2bbOXKlbZ48WKbM2eOTZ061dd+1KhRxWlf0L179++qVKkS9jrGK74WrgaYzbHWQFm1d17faY/XP3nypO/19+/fbzt37ozV6//l+b2f69atG/YaxjO+93Y1QH9SA2X1g6+b8/D65LwjR468iddPbtq0adjrF+/43tvVALOZORBLJojF60fmPOf10X7t2rW+158/f77v9SdOnBjN66e0adPmfW+/CHv94h3ff+HB8eL0JzXAHHidGogl5x08eNAyMjJ8r0/OW758uZ/z8Pou5wW9Pnv+S+2zkpKSKjRo0CDstSsP+L2IDwvWALM61hooSftoOW/btm1FOQ+vP2vWrKKch9dH+wi/d8Hz+pWqVasW9rqVF+6jBzWAHyOPoRX96nJhWWogqH1kxo+W8zZu3OjnPLx+MOdF8fpVqlatGvaalSd+zMzMfEEGC9YAmrkacHOAng7WQJCS+j6ofXE5b9WqVZaSklKU8/D6aF+C128kr/+f8Af5i95kb46sAecH0DU4C4IEM1407aPlPLx+ZM4LeP2OTZo0CXudyjOTyGHo5Gog6AfQ0+0H1IGrBeBzvu/Odni92++jaR/MedG8vqf99FatWoW9PuWdj70ZnoYvYw44T+iyIb0crAN63MHX6M6s4HXke34Pr8d7Oe25lxvM+JE5D69fTM7b0aFDh0+aNWsW9vq8C3zuzes89MITcj5D/5LXmePUAX3NOQG1gN7A1053ep7ZQf3wHng91/dO+2DGLyXnnejUqdNntWrVCntd3iW+8rJ5PjVARnf7AbOAOkBfVwsO1+/oTs9zpsvZDvkenx+pvcv4peS8W926dfs6ISEh7PV4F/neywSPqAHOZ+ljNKWn0ZeZQD0AnwM1Qq1QM/weXoKzHTIePj+a9sXkvMIePXok1q5dO+x1eJf5KT09/QV+wNUB2rKfA1oDn1Mf/JzXcZ5Lz7PXc65Hvifjuf2+JO0jct4v9evXD/v6hdlv9C+aMsupBWBPR2s+8jU/41k9dOc+Dr/DvOdMl7Md8j0+n/3eae/OdyIyPl7/98aNG4d93eIfRnNGy5lNJOjNGS73b9jjeR26c55LvuM8nzNdznbIeHi9YN8Xk/FHtGjRIuzrFa/ywfPnz5fzLB69zVwH9OYMD835GXu8052zfHqeec95Pud67j5uSdp7rG7btu2H3hwI+3rFv6ngZfwMNGZPR2/6nL0dzblvy/M69Du64/HoeTfv3fMbLuO5/T6gfWZSUtKn9erVC/s6RclU9Lx/HnrzbB57O57e6c45Ls9pcpaLx6Pnmffcy0F7d67nfH5A+7OdO3f+snr16mFfnyidb719P58Zj6dnzuPruXfnep59nnt4zHv3vGak9s7reeQnJyd/o+c244oEz+9dIssD2rPPc+/W9T3nuc7jo33wbCfg9S527dq1cs2aNcO+HhE7lbzcv4FMx8znIz6Pe3jMfHcP1+33Qa/3svfXtmvX7os6deqEfR3izfjV6/0cfF5xvc993KDf8/o/29vrmzRq1Cjs/1u8XRJSU1O7eXt9mtfzud7ML/A+Fng5L9fr9y3enE9u3bp15ebNm4f9fwohhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQggh/l/8DeaEaVeLK1y1AAADjG1rQlT6zsr+AIAQrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeJzt3etuEzEQhuH7v9+epqUBRXTVzcaHsdfJjD1vpPcPAgF+vngLSOQiIpdM38pyP57yZ3n9tr8O6jVnB+v5n7FnD23+3w7s9/6j7dlBfQPW9i3+fzKxhX7/ywT+OfezW7C2wL/uX/P+ynw7d4LO38MzoNX/KxH3wfr+KffPn2o70G7B2sfC/zKZ/2els/eCtRH+Ov/jq3UHETeQ+/3O4H/cwPb6kNsXG4jhv23g49D2Su0g+gZyZ+zBX7MBjT8bWNe/9Ax4/4kNrOXfcge8CRvo2cDlQbat/po7oObfsoHUDlbfwOz+tWdAaQMibGA1//0dsPm/yv0GUndBzwas/R6xAU/+mg1ongH7DZSeB7kN4O/XX/MMeJHfDezvgu3V42/thr/+DniV9HNApP/+t3Z71Aa8+Ws2UPJP3QFn7fH35a95BlztX6TsH9F+Vv/erwP3z37e+/dn7NFfcwdongFvcv/sx34Nf+0G3uX3lfOPZj+rv+YOuL6O/yYkwns/d75W9jX/UXdAzT+i/Yr+uQ2I3H7dj/3t+Xr2b91A6jkg0vbetzbBv+7fsoHcex///3n3z21A8xwo/X1P9Lt/dv/aBrYdRPw3ntX8z2yA975P+1H+qQ0cd4D9/P49G0iF/bz+ZzfAe39+/9oGcjvAfh1/zQb2Oyh9H2uDqPZn/bUbwH5d/zMbsD5/66ztR/n37MD67D1kbT/aX7sD63P3kLX7I/1LO7A+dy9Zuz/DP7UF63P3krX7s/3Jnz3++FufRcSszfHHHn/88Y9tjz/+1mcSKWtr/LHHH3/8sccff/xj2+Mf2x5//K3PaNWsXfHHH3/s8ccef/zxxx5/7PHHHn/88cce/9j2+Me2xz+2Pf6x7fGPbY9/bHv8Y9vjH9se/9j2+Md1xx97/GPb4x/bHn/787fO+vyxxx93/HHHH/fnZ/H/8j7r57Q+2xk6+/kdo8I9tv+ILVif5YxZW4/YgfUZzpz2M9o8hPn49l+PtX5m4zHt53/27AxzP3sYXem9bn0e0Tv+mS21i9znfZc+/3vvjvW8tf753vrXS0RERERERERERERERERERERERERERERERERERERE5LV/DhlgnUT7BXMAAAjBbWtCVPrOyv4AgD+fAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2dS2zUVRTGQZFHlCgRQ4wxGHwsiK5MCC5dsBJZSUzYGDUSlj4WLkzAtZG4IEYTN6KJEjUuDO40MRpjYugDCp12Ci3TUtqhD9pC35Nezzfk1svl/memM9M593EWvxB2vee7c893zn38NyilNjBycnBwUN24cUONj4+rqakpNTMzo27fvq3m5ubU/Py8WlhYUIuLi+V/Nfi/0BQ4tQc7x8bGpkdHRxX9qyYnJ9X09LS6deuWmp2dLc8Bl/4yB6LRH5woFAqqWCyW14CbN2/WtAbIHIhG/4foN18cGRkprwETExOreUCvAXoOiP5R6g/eu3r16l1rAPKArAHJ6L+VtC5cv3697AXNNcCeA6J/lPqDtwcGBlbXAO0Fob94wST0v39paal7eHh4dQ3I8oKif5T6g8P9/f0K9SDWAJ0HpB5MRv+NpVLp76Ghoar1oKwDUeoPXiIfsKLrQfgAew2QPBC1/uAn9IWr1YMyB6LVf8+1a9cWsAZIPZik/uAz1IPV9gZkDkSr/w7SfVzXg5IHktMfvK/rQXNvQNaAZPTfTBr3kRdY7QkgD8AHYA7IGhC9/uDw5cuXlewNJKs/+Av1oLkGiBdMSv/95ANWsAa4+oKYA5IHotYfnEY9aJ8TkTUgGf130fo/DS+INQBzQNeDeg7IGhC1/uDDK1eurHrBtewPeRBrH+HWc61spt95r/aC0hdMTn9wUNeD4gWT1B/8ivOilbyg5IGo9X+mUCgsYG9Ae0E7D8gaELX+4JN6vaDMgSj03056D+OsmJkHsAaYd0dE/2j1B2/09fUpnQfsPWLJA9Hrj/Oif8AL2vtDtXhBmQPB6w+e7+/vX9J7xFlnRkX/aPUHn2ovKD2BJPWHFxxCX9B1ZtTMA6554IEOon/jvJbP58teUOcB2R9KSn9wFucF7XrAzgPSG4xW/6epFpi3ewJ2HoDWkgei1B+cQB4wzwlIbzgp/beQxj2unoDdG5Y8EKX+4EBvb69CHnD1BPQckDwQrf7gO5wTcO0RSk8gCf13kgccQ0/A3COstSeQyBzg1mi9eaunp6fsBV3vCUgeiF5/7A/9hp6AvT8gPYEk9AfPkv7zeGPUlQcSrwe4tWkVH+l6wN4fSDwPcOvSKjaRrh24PyS94ST1B/vy+XzJrgdcZwYTmgPcmrSaU+gN675QVh5IaJ+YW49Ws528/5ArD9h9oUTWAG49ODiInoDkgWT1B982kgcimgPcOnDxKOk+gj1Cc38gwTzArQMnh3K5XOp5gFsDbn7QeaBSXyjiOcAdf2520m+/qOsB+02BBPIAd/x94Eh3d/dqHrB7g+Y7gxHOAe7Y+8LPieYB7rj7wuOk/STqAXuf2D43KvpHy5vIA9gndvUGI80D3DH3jbO4T27mAddZgYjel+GOt28gD0ygHqh0n7jSGhDYHOCOt48cuXTp0l15IGIvwB1rXyn3hbJqwojyAHecfQV9oVHkgayaMJLz49xx9plDyAOR14TcMfadb3BWQJ8dzuoNBpwHuOPrOw/Tuj+It2WQB1xvTAXuBbjjGwIHKA+s6DygvUAkNSF3bEPhS5wV0DVh1ndoAvQC3HENhQcp5/fiPnHWfdJA8wB3XEPiRVoDlsyaEH4w6/vUgcwB7piGxseN1oSezQHueIbGJvp9/4PeoN0fDrQm5I5niOyhenAGd8pNL4CaMMD+MHcsQ+VoV1eXyuoPB/S+CHccQ+YX9AareQHP1wDuGIbMY6T7CGpC7QWy7hB4PAe4Yxg6r1IeWDG9APxgQF6AO34xcArnBpEHzPcGA/kOBXfsYmAL/c47zJow6/1xD9cA7tjFwl7ygrPYJwzszBB33GLi6IULFxS8QFZN6OE9Eu6Yxcb38AL6/LD55mSteaDFc4A7XrHxCOX+AdSErrcFPOwPc8crRvZdvHhxKZCakDtWsXK8Wn/YEy/AHadYua9UKv1u9ofNc2MeeQHuOMXME/T7Hzf7w/Y7Qx70BbhjFDuvUE1Y7g+77hV74AW445MCn5MfLHuBrO9TMnoB7tikwFZa6zvx/riHXoA7NqmwN5fLzZpeoJ49gnWYA9xxSYlj58+fV9gjyHpjhMELcMckNc7o/rDpBRjvEXDHIzV2kOYFeAG7N6TvEdhnB9c5D3DHI0X2dXV1Leo9gnrPDjZpDnDHIlXe7ezsdHoB8+3RFuQB7jikzI+4S2TvE7W4N8Qdg5TZTnk/Z3sBvVdoeoF1vFvMHYPUeYHqgTm8OWj2hlroBbjHLyj1jvYCeg643pyrlgfqnAPcYxfu8DXOC+h9ItsLrKMf5B63cIdtlO878caIPj9qnhsye0NN3i/mHrfwP8+R/tO4R+DqDZleoIl+kHvMwt283tHRoVrYG+Ier3AvX+Aegd0bsr9L0yQ/yD1W4V62kMbnsE+k7xOZb47Z36hr0Atwj1Vws5t8wAR6Q7o/WKk31MAc4B6nkM1B8gIr6A2Z94n02TGXF6hjDnCPUajMSZwZMfuD+nu1We/OrdELcI9PqMwDpOmf2CfS/cGs79XW6Qe5xydUZxet/0O4S2KeIzf9YAO9Ie6xCbWxn/LAwjr4Qe5xCbVzrK2tTWk/aN8rrLM/yD0mYW18BT9o9wcb8IPc4xHWBnpD/+I+UZN6xNzjEdbOk/l8vgg/aPaI6+kPEtxjEerj5fb29mXTD9a5X8w9DqF+PqA5UPaDZk3geoe4gh/kHoPQGKd1f9D8Zm2lbxWK/lGxjfJ9ewN+kPvvFxrnKfKC4zg7ZvaIa/SD3H+70BwOtLW1leAHs+4XZ/hB7r9baB7H0R/E+UHXnaKMmoD7bxaax8bl5eUzuEtgnyE160KrR8z9NwvNBX7wHPyga5/A0SPm/nuF5rOb/GAR5wer1QSUC/4DNoxEagEn+8IAAAWabWtCVPrOyv4AgEDpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO2dTahWVRiFb1lZGZKgBDkp1LCI8A9sUBPREkoIycBBQg0qMhSc6EipBjmQ0BpFRBA0cdbAojBpYDhSCKJADPMXUxqkRe1S8335PulyIT0/++y19t5rwTO793Le9fDdc84+P9/EhKIoNWVaCOEh4zkDvS3KsLnXHD9hvGLsMQ4avxv/jkFvnxIv/rleZGw0PjVOTPL8f6C3Weme6eZvhbHD+NK42MC3/OedB8b/y/cav3XwLf955TZztMp41/gxgm/558+t42M2P147N4Bz+efLZOdnB3Yu/zzx/flbxqmEzuUfGz9PWxlGx3CXQd7lP33mWd87E+zT5Z8rS63nT4x/CHzLf5r48dyaMFpvRTuW/3S5K4zWX48RuJX/dLkjjNblzhA4lf90ud063GD8ROBS/tPF9+/rjKMEDuU/bVZbbz8QuJP/tFkQRms2aGfynzZ+D42v2/xF4Ev+08X38a8a5wk8yX/azLduDhD4kf+08fO5raHM//Xyf+Mstj4OEziR/7TxNdvdAX8tVv7T51Hr4DsCD/KfNrfY7JtDPft5+f8v99ncnxN0zwDaReqstZl/JeidBbSPVPH76H0N7ypB50ygvaTIbJtzP0HXjKDdDB2/9+5ngp5ZQfsZMn4/TiDomBm0oyHia7gfEnSbA2hXsXOPzbSPoNdcQPuKmfttniMEneYE2lmsPGaznCToMzfQ3mLkqRDnXQg1gnbXN+sD53NVuYD21yd+fneFoMOcQTvsGn/GSmu5dfrfStBbKaBdts1Ogs5KAu2zUXw7jXcI+ioNtNpGse3cTtBViaDVNslmgp5KBe32Znk56Di/Vv8vBp3f1+r/2VDn/fjyPzGxxLbrEkE3NYB2PTVzA+5dmDWC9j05M0Pdz+LU7N/v2fqKoI/aQHu/no8IuqgRtHfPRoIeagXt/vEwCrqHWkG69+cwTxN0UDMo9/483jcE89cOyv9ugtkFxv/zBHMLjH9f39Pz9zykdO/vU/yaYGaB8b+NYF6B8e/X9ALBvCK9/7vDMN9dKvLw/z7BnALjf3nQfTzMDOnevxPpe4IZBcb/mwTzCYz/hUHvVs2BIdz7Os+3BLMJjP/XCeYSGP+z7G9eIJhLYPzvIZhJYPw/bH/vb4KZBMb/FwTzCIz/ZwhmERj/fi+fru/kxdUxMfy/RDCPaOZ7Kn3d+xr/cYL5RDvvsfy/RjCjaO89hv87g57VZsOdXknkX+9l4uG69zbu+/ifYb97jmDu2pnsva37Pv43EcxeM1O9d3Hf1f80+71jBB3USt/PfF//6wg6qJFYn/m+/g8RdFEbXY/vYvt/kqCL2hjCe1f/nxH0UQtD+O7jf0HQ+1hLct/W/y6CXmoglfs2/v06zy8E3ZROSvdt/L9A0E3ppHbfxv9+gn5KBuG+qf95459Fd1QqKPdN/et7l8p038S/r/WfJeipRNDum/hfQdBTiaC9N/X/AUFXpYF23tS/39etc/5y3d/M/9MEfZUE2nVb//pOhnr9+3qv3tNatvsb+V9N0FkpoB138f8eQW8lgPbb1f9Rgu5KAO23i/8HCXorAbTbrv7fIOgud9Be+/jfR9Bf7qC9dvXvz3T+QdBfzqCd9vG/iqC/3EE77eP/bYL+cgbts6//AwQd5graZV//fr3vEkGPuYJ22df/MoIOcwXtMYZ/PdNft/+9BD3mCNphLP96l1O9/ucQ9JgraIcx/K8k6DFH0P5i+d9C0GWOoP3F8v8xQZe5gXYX0/9hgj5zA+0uln9f9/uToM/cQLuL5f8Rgi5zA+0tpv+1BH3mBtpbLK4B4Y9eO8Ecbd4AAAEHbWtCVPrOyv4AgkOQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4nO3RsQnCYBSF0aQQrFO5gjiCS7hCFskYbuAati5h4wiWFvLnQQawkqD3fHD6C7fr9I8dX0vtg7V36nuN/o/v7P/oNvXx1f/RDfXz3f/R7evrp/+jO9Xfb/9HN/k/ur4+v/g/um39fvN/dLv6/uH/6A7+lyRJkiRJkiRJkiRJkn671hoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsZQb+enFKppz2UQAAMiFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBGaXJld29ya3MgQ1M1IDExLjAuMC40ODQgV2luZG93czwveG1wOkNyZWF0b3JUb29sPgogICAgICAgICA8eG1wOkNyZWF0ZURhdGU+MjAyMC0wMi0wNVQxNzoxNTozNlo8L3htcDpDcmVhdGVEYXRlPgogICAgICAgICA8eG1wOk1vZGlmeURhdGU+MjAyMC0wMi0wNVQxNzoyMTowOVo8L3htcDpNb2RpZnlEYXRlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9wbmc8L2RjOmZvcm1hdD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgCjw/eHBhY2tldCBlbmQ9InciPz5mrTAuAAAgAElEQVR4nO29eZgdxXX3f/ous89oZjTakECbBRK7wX4SGy8h+TlxEvI+/nlP/P5ix7GJibHBxrvzOnZiE+IEYkhs8AYkNrzGC5YNRgjQihYkzQbaENrRaBkhjTSambv1Ur8/NDWqW7equrpvd997+57P8/RzZ3qt6uXb55w6VW0QQgBBWAqFQg8hZD4AzAKA6QAwnRDSYxjGdACYMTmvHQDaACA9uVnX5G8DALQSQsAwjAkAKEzeY2cml5uEkHEAGAOA04SQ1wDgFCHkNCHkNACcBoDhRCJxqKWl5XT4tUVqCQMFq/6wLCvpOM5iQsjlhmEsAICFk9OCyamdrmsYhvZ+3e4l2XJ2Pvf3GCHkEAAcJIQcIoQcJIQcBIBdra2t+5PJpKNdOCQWoGDFHNM0OwkhVxqGcTkAXAEA1wPAtQDQKtvGTaSCuGe8iJfkt0AI2UcI6XMcZychZFcymdza3t4+XHbhkKoFBStG2LaddBznKgC4AQDeDABvAYBL6HIv1lLY6FpjCuur5HdyOkwI2eA4zmYA2NDW1rYjlUrZARcfqRAoWDWMbduNjuPcYBjGWwkhbwaANwFAey0JE4UtM7+N6n+FcNH/zzmOs9lxnE2EkOfb2to2NjQ0FLzXBKkGULBqDNM0FxiG8ccA8P8QQv4EADqqSaB0CNql5P/mxYv9mxCSIYRssizryUQi8Zuurq5DZRcGiQwUrCrHcZyU4zg3AsCfTk5LK1yk0CjnXnSJdxX9Os75WL3jOEAI2e04zgrbtp/q6upal0qlLN+FQEIHBasKIYQkbNt+MwC8DwA+AOfTC6qeSt9Lk6kUyvgXFSvWdWQssRHLsn5n2/Yvuru7V6B4VR8oWFUCJ1LvA4A5FS7SFLV4j7jFvUTuIitmjuOctm37qUnxeiqdTmPgvgpAwaowlmUtAICPAsDfAMC8Spal1u4FtryqOJ5MvESBetHkOM4Ry7IeSiaTP54+ffqrIVQF0QQFqwI4jpMkhNxICLkZAN4NAMkojx/Hay6rEy9kIvHiBWwytsWLFhBCHNu2V1uW9YPp06cvb2hoMEOoCqIABStCbNteTAj5OAB8BCKMS9XbNXarLxUx3ViXSMBs2z5hmubDiUTih7NmzToQRj2QUlCwIsC27esJIbcBwF8CQEpnGzZ47JUormml7huvKRw65RQF6nnLi2tZZP93LMt6yrKsu+bOnbvRU+EQz6BghcRkEP3PAeCLcD7zPOzj1cQ+w8RLHMsNKliGYQhbFlnhor+2bfcVCoX7Zs+e/QgG6cMBBStgbNtuJoR8FAA+AwCLdbbxa00Fde3ifg+4xbFUiOJb9H+FcO0zTfOe7u7uh1paWnIBVQMBFKzAcBynwXGcjwDAPwLARWEdp9zrhdf7PLovCVkLI+8iUrHihOukaZr3TJ8+/V4UrmBAwSoTx3HSjuP8JZwXqkVu6/uxpoLIAEfKR5XLxYoVK16TwnWkUCjcPXPmzAeam5vzlSp/HEDB8slkjOo9APAtAFjitr5XoarmgHu9IxMukYvIWFtg2/bhfD5/57x58x5Mp9OYRe8DFCwfWJb1TgC4BwCWua0bhVDhNQweGnB3W4euJ8iUl8W3wLKsnYVC4bOLFi16Joq6xAkULA/Ytr0EAL5FCHmf27phCxVet2hwy6Z3y5pnxYr+bds2OI4DhULhOQC4ff78+TsjqUwMQMHSwLbtTgD4EiHkdgBoVK3rRahQpGqHIIRLNNm2bebz+fs7Ozu/1t3dPRpNbWoXFCwFhJCE4zgfJ4T8M5z/+IIUFKr6gL8WspQJPulUJVyTbuLJXC73D4sXL/4xjlUvBwVLgm3brwOAHxBCblStF5ZQ4XWpbkTXhx81VWRt8fEsAADLsqb+N01zEyHkY4sWLdodTU1qCxQsDsdx0oSQzxJCvgEBuX8oVPFEdq1EfRXZ9AdZTItpTTSz2ew9F1988ddaWlpwOGcGFCwG27ZfTwj5EQBcp1ovaKHCa1DbqK4fvVd48RKlPXCiBYVCYYdt2x+79NJLt0RVl2oHBQvOd6cBgH8hhNwKiqFeUKgQFbr3hqoVkRUsx3HAsiw7m83eN3fu3K92dHRkI6hGVVP3gmXb9pWEkEcA4GrVejpihUKFeLkHRCNAsNYWFa5Ja2u3bdsfWrp06UCY5a92EpUuQKVwHMewbfs2QkgvKMSKj0fI8PJ2ReKLYRhaQ+AYhgGJRGLql52SyeTUbyqVgmQyCY2NjcsaGxu37Nix4+u2bdftc1uXFpZt27MA4CFCyJ+q1gvKqqrHc4z4cxFl7iE75XK5Vc3NzR9esGDB0QiqUVXUnWDZtv1uQsgPAaBbtg4KFRIkqvuA7dqjimkx+Vo0/eFUNpv9+DXXXLM8wqpUnLoRLMdxUoSQbxJCvgAAUps9CLGql3OK6KN7z8iES2RpWZYFmUzmB5deeumtTU1NdTG+fF0Ilm3bPQDwM0LIH8nWQasKiQI3a4v+ipJMRcJlWRbk8/n1ra2tH1iwYMGJqOpRKWIvWJPjqf8KAObL1kGrCokSHdGif4u68bDCZVkWWJYFpmkezefz77322mtfiKIOlSLWrQ22bd9MCNkEZYiVW8setvwhXlG1JLLz+RZEttWQn9Lp9Nympqa127Ztuy2qelSCWFpYk91rvkcI+ZhsHbSqkGpA92UoytOyLAsIIWCaJg3Eg23bMDEx8cDll1/+qebm5tgNEhg7wbJtuw0AHiOE/JlsHR2rSkXczhlSWfyIliieRQXLsizIZrPPzpkz532zZ8+O1ZA1sRIs27YvIoQ8CQCvl62DYoVUIzqixXagZiea6kDFyjRNsCwLCoXC9oaGhpuWLVv2alT1CJvYCJZt21dPitXFouXluoBxOU9IdSO7z/gWRMKM8sD2PaRixQTjj5umedP111/fH2U9wiIWQXfbtv+YEPI8+BQrncA6gkSBWzCeBuwNw5gKuNNAfCKRgFQqVTSl0+k5qVRq3ebNm2+Ksh5hUfOCZdv2XxJCfgcAHaLl5biA2AKIVAIvosW2ILIClk6np4QsnU63tba2Lt+wYcOHo6xHGNS0YE2mLfwUAFKi5eWKFYJUCpVo8RMvWLx4TU7J9vb2h55//vlbI65KoNSsYNm2/UlCyAMgqQOKFVLrqEZ90BEtamUxk9He3n7f+vXrPxthNQKlJgXLtu0vEkL+CyR9AlVihfEqpJYISrQYa8vo6Oi4e/369XdFWI3AqDnBsm37LkKI9GS7iZUMjFch1YoX0WKD8WwgnrW2kskktLe3f3HdunX/EmE1AqGmBMu27X8hhHxRtY5fsUKQakZXtOiggGwMi+3Ww4nWl9auXXtPhNUom5oRLNu2v0YI+ZJomc4nxf0sQ5Bqwq0PIi9atAWR7X8oEK3PrF69+qsRV8U3NSFYk0MZf0O0DIPrSL2hK1psTIuNY/HuYUdHxzefe+65z0dcDV9Ufaa7bdsfJec/vVVylfyKVbXXGUF0cGtY4sfTYjPgaReeQqFAs+PJ2bNnb3nHO97x/Yir4YmqtrBs2/7/yPnhjFGsEITDzdKiriEfiOdaDel8Y9q0ad9buXLlhyKuhieqVrBs276JEPIQCMqIYoUg53ELxlPXkIoWK1asizgpWolp06Y9/Mwzzyg/zlJJqtIldBznOsdx1gFAm9dtUayQesPtnufHh+fdQtY1nJzG8vn822688cbBKOuhQ9VZWLZtz3Uc5zeAYoUgWuj2PeRTHli3kAvEtyeTyd/19fVdEmU9dKgqwbJtu4MQ8hQAzBMtV5m/KFZIPaPb95BNdRBNVMQaGxsvOnfu3FMHDx6cFnFVlFSNYDmOkwaAX4LkK8x+MthRrJB6wku6A59cKkp3aGpqumLv3r2PjY+PCwcXqARVI1iEkAcIIe8QLUOxQhA9vFpaMtGi3XpaW1v/ZP369d+NuBpSqkKwJkde+KhoGYoVgnhDJ92B/xqPzE1MJBLQ3t5+8/Lly2+OuBpCKt5KaNv27xNC1gFAA78MxQpB/CN6FviWQ/4jFpNjwRdNky2H5tjY2I033XTTxqjrwVJRC8u27VmEkF8CihWCBI7I0hK1HLIWlsw9TCaT6ebm5l/09vbOiboeLBULpjmOkwKAxwBgLr8MxSpYau3cuHVmR/QRPUvs+U0kztssyWQSCCFTvzRvK5VKTeVvpdPpOcPDw4+Mj4//cVtbW0W+eVgxC4sQ8u+EkLdLlsm28TS/HmG/qlKrY3zFoQ7Vjs7gf6KYVmtr643PPPNMxcbRqohg2bb9XkKI8JPasrcripWYenmw66WeYaAzlpboYxb8EMt0eWdn5x2/+MUv3hVhFS6UN+qLb9v2XELISwDQXVIYj65gvd649VpvGehC6iF7hlgXkI7qQIPwtNtOPp+HfD4/FYTP5/Nnmpqarnnb2952JMo6RGphEUISAPA/EIBY1RNoXajBc6OHLAgvcg1FOVpsUmk6ne46c+bMj7PZbKQaEunBHMf5AiHkD/n5GGQXgw+hd/CceUeUn6WKZzFJpe9Yvnz57ZGWNaqLa9v2dYSQzeAhhaFexSru9YsadBmL0XUN+dws6hJS93DSZcxnMpnff9e73hXJyA6RWFiO47QQQh4BFCslaB2EA57XYnRdQz5HS/CdQ0gkEo3JZPLRw4cPt0RR9kgEixDyrwCwlJ/vNhCfYD9BFqtqwAcqGvA8X0AlWjquIStkjY2NyzZt2vStSMod9gW0LOv3AGAjACR1t6mXFsE41qmWqHdX0UurIe8astNky6EzOjr6lg9+8IObwyxzqBaW4zgNAPBjEIiV13yrOIFv+uqg3q+Dl1ZDVTIpHV65oaHh+ydPniwJ+wRJqIJFCPkqAFzBz6/nuFWc6hIX6vmaeE11kIgVJBIJaG5uvmrFihVfCLW8YV0s27aXEkIGAaBRd5s4u4JxqUfcqUc30c01pC2GrGtI3cFcLlfUelgoFPKFQuG697znPbvCKGsoFhYhJEEI+TEIxMqLKxiXhzwu9agH6vFauVlZfABeNLID4zI2Wpb1o0wmE4q2hLJTx3E+AQBv5ud7dQXjQJzrFlfq8ZrpDvrH9zXkhqChCaVvevTRR4UDcpZL4IJl23YXkXxWvp5SGOo9oFvr4PUTj52lGj+LFa7u7u5vDQ4OBv4BizAsrK8DQA8/s55cwVovP3KBerqWfhNK+Q+zGoYB6XR6Zl9f3z8EXsYgL4ht28sIIS8CQLroIHXUKljLZXch7IpVdbS7noLx/D3M5mXR3Czmo6tTwXc60aGV8/l8wTTNK9///vfvDapsQVtY9wAnVgD14wrWctk5iGCK4zG1idG19QxvZfH5WaJUh8nlDblc7u4gyxKYYNm2/eeEkHfy8+vFFazlskP1CkVVlavGr7E2qgA87xLyH2blu/K0t7f/xQMPPPDHQZUtEMFyHCdFCPl30bK4CZOIGq1P1QiBBype5hq91mXDW1eiD7HSDtJ0HhWz9vb2eyYmJrS75inLEcQFsCzrbwDgwZKde4hd1eqNUEPlLqugu3btgnPnzsHExIRB4xQAALlcDhzHAYALNzUL/cgBwPkPHTQ3N4NhGNDY2AjNzc3Q2tpKOjo6YOHCheUUryIBprjHtWSxLDaZlB2VtFAoQDabLYpl5XI5ME0Tjh079uFbb731f8otU9mC5ThOg+M4ewBgQdGOUayqBc+F3L9/PwwNDcHIyIhhmibkcrmStys/AVx4gFmRYufLoMsbGxuhsbERurq6yEUXXQSzZs3yWvRIFSTuggUgFy3btsFxnJLgOx+Ap1nwmUxm/zve8Y5lM2bMMMspT9mCZVnWJwDgft314yJYNVBm7QIeP34c9uzZA2fOnDHOnj07ZcrzIiWaR/8GAKF48X+XFFLwQLAvu/b2dujs7CSLFi2C9vZ2/dpHJF5xFy03K8uyrKlRHNgWQ2ppsSM6HDt27OOf/vSnf1ROecoSLMdxmhzH2QsA84p2GnPrqsrLTGM8yidp//79sGfPHmNkZARM0yzp5Mq3BoniF6w48aIlcg+nCsiIEnsu2f+pm8kuT6VS0NnZCYsXLybd3SWfBZARuqLUq2jxfQxZKyubzUI2m50SsEl38dUbbrjh0gULFuT9lqUswbIs6zYA+I7OunHJuari8rLBaOETNDY2Bn19fXD06FEjn88XtfKwwsSKFt8qBAAlosW6grpuoUi02HwfgAufU+cngPPu44wZM8hll10GyaRWPDdUVYmzaHmxsmgnaIWVdettt932Xb9l8S1Ytm23EkL2A0BRoCHO1lWVlpdvNSt5cnbs2AH79+83XnvttSIxYr+OwjdV883W7MRnPYtiWSILi/1fZVmxf7OiRf/mra9p06bBJZdcQmbOnKlzvkJTlnoTLTaRlFpZ7AB/bACesbKOX3/99a9btmxZxk85fH+qnhDyt8CJFa2I33nVTBWWV9S8X/TE9Pb2wr59+4zx8XFIJBKQTqdLet3zHVplAiayvACgRLBYy0rmFtJzKbKw6C9rZbHCxf5t2zYQQmB8fBx27txpHDhwAC6++GIyd+5ct/NWcq6CgFqN9YJqSGXR8MqGYUBDQ8Ocp59++iPLli37nq9j+nkQHcdJTrYMLuYrgIIVCUqraseOHdDb22tYluV6E6nmGYYx1TeMdwNllpWbYInuETfB4q0r+lan/9O/6fbpdBqWLFlCenpKurTyhKIucRUtNyuL//AqdQv5WFYmkzlw0003XTp9+nTbaxl8CZZlWe8DgJ/rrItiFShKq2r79u2wY8cOY3x8XChG7OfG2YQ/NlvZizsoil25BdzpfNl9QQWNdfvoQwEAUw8Ha3nZ9vn7np0PANDS0gILFixQBehp40SgClNPgsW+NFTBdz7NYWho6D2f//znH/daBr8u4Wf5GTLrqtapojpJg+rHjx+HDRs2GGfOnCly/agA0V82C1kmYqIYFm9R0SC3LG6lil3x//OxLPaXFS12EllYtm1DMpkschnz+Tzs2bPHaG1thaVLl5KGhtKvzMnOaTnE1TXkn3H+2vOuITuaA39PdXV1fREAPAuWZwvLsqy3AsB6nXVr3bqqorI6k78lT8HKlSuNQ4cOlVhIrPVEhUr2mSbq9vGC5ZbGoLKqeNeQRSd0wMe2+BZDPpZF/7csa0rMWFfRMAzo6emBRYsWiS4qnRfoYABxFC2VlUVbC6mVRS0qkZVlmiYcP378hjvuuGOTl+P7sbA+x8+Iq3VVBUjf/tu3b4f+/n4jn8+XjEfE/9I+XqLxi0TWFStSAFBiYYkEi/66uYL839KKczlafOshK2KstZVKpaYsLt59HBkZgfHxcWP+/Plk2rSiseWopeVAgC5iHC0tkZVF6ylKk2HvOfY+MwwDGhsb7wAAT4LlycKyLGsJALwMGm8itK7KL4JsAWtVicRJNAqkyB1kW29kSaOyjHbZr0yYZG4h32JYdAIErYl8bIu1tvi4Fmtx8YH5np4emD9/Pn+O2RdEIEoTN8ECKC/4zgbgLctyMpnMZTfffPM+3WN7tbD+DjixiqN1VQX1ERbg+PHj8Mwzz0itKipKok+Ki8RK5P7Jut9QdJJD+V9+Of2btUBk6Q9sIJ4QMnVMnbiW4ziQTCaLLC7LsoAQAmfOnIGJiQljyZIlJJ2eGsKNWlp0KttFrAcri87jX3SimGgqlQLTNOl1SYyOjn4MAL6kfWzdh3Oyk/MRAHDNzkPrqrzDi2Zu27YNBgcHDfrQUjdP9GFL0VAffJBdJlR89rrqb/aXX84vE607VWHNh1okYKybSK0v1g2kYsUKFp1P6z537lzS1dUluwaBxLXiJloyK4sdxYFmvtPYFY1jZTKZqdbCXC43/Cd/8icXX3TRRVqdorUvhuM47wINsap1qlGsnnjiCaOvr88AON+frqGhYeqLJXSiIx3Qia7T0NAwNVGBY790wltgojiVKIYlmkoqoziXqtYmfmJbKvlYGxuHY4WaTnz92XNHrbWjR48aQ0NDbAXYvx1AtJBZWbLE5MkX76wHH3zwf+kew4tLeLPOSrVuXVWQkpM0NjYGK1asMEZGRoSxKV6AeIuLt674jHU+a50XH53YVFi4pUKwriH9ZWNeiURiKsbFuoU0X4jOo5bYuXPn4NChQ8b8+fPJ5LHYlAcHyrS04uYaqoLvMtHiQxJ0vc7Ozo8BwK+0jqsjJpZlLQaAvaARiKxlwapgOYVi9fjjjxvZbFbY4sd+E44XKjaNQdQCyIoVQOloC/TXawDd67p+l7EPP+8i0nl8trwsEE/n0W0aGhpg0aJFhDke36PAt+rESbAAvLmFfHoD/XtyWBonk8ksvuWWWw65HVPXwvoYcBdK1arjNq8aqSax2rdvH6xatWrKBWQD6bxQ8f+L3masO+UWk+LnCwscgbWgOgYtH/tGp/NZAeMFmVpdiUSiyMqiAgYAYFkW7Nu3z5g/fz5NNGUtLZoZH3idahFZ8J09z7zbLrofASBx5syZvwGAf3Q7pquZa9t2EgA+zM+vFSGqclzFio1ZsfEYVWyG/xovm3Mli0kBuAuVrwpqxrH8oIp5iTrk0nPBn6+GhoaixghCCBw5csSgw0ADxrS0EL30+Gsgy/vr6en5m9HRUVc90smnejsAzCmvKtVNhcRXKFarV68uEivWeuKFig2uy9IYePePT1XQDZiHKTzl7oePnciEi4+nsOeXnlP6IBFC4OjRo4ZpTjVeBSJacX/Ri84/L1zsfUmXp9Ppi++9994b3PavE0j8gE5Ba9kdrABSsSKEFKUjsFYA3/rFPmyizyzJ+gKqWvYCqVwFr7tKuPi4nsitZq1RgWixoKUF4ric27kX5WlNipar1igFy3GcFAC8y391qp8KPFxSN5CKFe3/J2uaZx8wKlaisa4ASh9gVRzFy7koxxqKYjuZe8LGT+h5Er0IaHyFEALHjx+nosWfOF+iFecXOW+585auKMWBrtPd3f3eEydOKIePdROsPwIu90rHfZDNQ8StgWzMin+AWMHi/2ZHY2BNbfZGAVD39avV66RTblGjAu8i8rE+vtWVWlonTpygLer8A1CbJzBARC26urFFVrRSqdSs7373u29THcvNJXw/P6NWb/BqhKYuABS3BoqEin+Y2AeMvxkA5DlVXvASx/Kyn6gRvfVlb37+3LLxv+HhYZFoicYoc6XS5yRsRHEsWaoNe02am5uVbqFUsGzbbgB0BwM9HD9jxYoVRjabFbomoom6f2y/QT5dQUeo4v6wiBClQqhaEvkGDMMwwLIsOH36tEj56++EKpC5hbw7zrcUGoYB3d3d73711Vel6VZSwSKE/CEAuH5LCd1BLUpOyBNPPGGcPn1ammclmtiHiH1jiUTKb85PNVlRYeyTF3L2YRIF5fl8tnw+D2fPnhUlkHqOZ8XpOVG5hbLWWlFuViqVmvHAAw/8gew4KpfwzwKqS1US4c1ScqBt27bB0NCQVKxkAXZVK2AQLmC9oBMY5uOD7Lmm2doC4qNAAaEbx2Lv48bGxj+V7U8lWO/0U8A4vTXC4Pjx4zA4OGjwMRM+VUHUzUZ0gQHkmd+IHF7cRQ8U2wjCChcAwOjoqOE4jigAjw/AJPz5FbngfJwLAKCzs9ObYJmmuZAQssStQChOrpScoGeeecYghBSJkmgkBT5mJQpYolVVPrKHin2hiFIhAADOnDlTdjwrTs8Qn0pCf/kYlszCous0Nzcv+/a3vz1fdAyZhfXncb75I7pJSg6ycuVKI5fLFbkdotgVmwvEd60RZaqzv4h3RKkP7MPFn3+63HEcGB8fFz1D8VGhMnBLb6AWFn9uAQBGRkaEHp5MsKQmmYo4vS2CZvv27XDw4EFhKxSftiAaJoYPsANcGPETRat83ESL71BOz30ulwPTNNE1VKCyskTegmEY0NnZKYyhlwiWZVlNAPAHboVAcVJScnL6+/sN9saXDTTHC5UqbsX+IuUjEi2Z68I+aJlMpizXME7Pkqi1kP7qtBrSdaZNm/ZHg4ODjfz+SwSLEPJWAGgJqT4VpxI3x8qVK4vGYRe5gqpB91hTGaA0KVSEXyGLeruo96l7TD7tgXcH2cm27bJFK4645WOx55M/t4lEovWnP/3pm/l9ilzCt0WZvxNDik7E8ePHgf1uIN/xlm8NFI3DLsoZkr3JRHhZN+7o1F30oLGBd9FwPfl8XjQYZt0/FKLWWFkAnl02OZjiW/n9iQTLdYgHFCcpJSdmw4YNBg0sykYMZeexwUiR6QzgbcTPcghqv7UokKomed4ioL8TExNoZUmQBd5l1msikYD29vYSLSoSrMnRGd4YWS1izvbt22FkZETa5YMVKrpOOp0uajlxS10oVwxqUUwAoim3KuWB/aXr2LZtWJblq3N0nIwA0T0quo9Fws/Gadvb23//yJEjRaM3FAmWbdvXAkCb1wLWyskOuZwlO9++fbvBd/HgXUFR9jrN/eFdPx2xKsf68iICtRIjC1LQZbEY9jrlcjm0shhU509kXbHrJZPJjnvvvfcqdn+8S1gS5OKpFXGqNNu3b4fx8fGit7FIrHjR4nNURKIlC7hXS6yqVq02FezDxDeGUCsrkUjQjzDgEDQcvJWlCsSz97fjOEVuIS9Yb4njzRYBJTdkb2+vwY7CIBp3iZ8ne9OwF1WGl2CyzjZhWWYqqtX6UgXh2Yl2jUIrqxg3K0tksdJ1Ozo6lIL1ptBLXwf09vZOfZXFzRVkhy/x0+VG19pyI6wXVRRCFwW6omUY51u4/MSy4uS9uMWxVOePvaf5wPuUYBUKhRkAMM9rweJ0koNi7969BusK8sLFChi7jsiakj3UMqGSbROkuxhlwLtS27mdQ5F1wF4TQfY7QB1bWSwyl1DkSTQ1NV3yve99bzr9n7WwrnE7UC2LU4hlL9rxjh07YGJioijvih/FkrWq2FEtZe6gaBLhN7bl9f+g1q1V2GvEx1/ofIxlFSO6f2XuIScUm4wAACAASURBVP8s7Ny582q6H1awrgakbPbt2zfVBYe3sFRjWiWT51tvdV1BHi/WVjlUQnSCsg6D2M7thcI+dAK3EKAORUsWw5KdO97KSqfTU9qUYnZU1HyIaFF0842NjcGpU6dKLCqRW8gniOpYUDxu64VhbXkhKMssjOOXu19qsbMPG2vFE0KmviZdr7Dnif5Pf0UxLH4ZnVpbW6e0aUqwCCFXs/3VdKhlFzEMent7wTCKm735D0fotArSv8shCLHyeoygBCIIyzKs7UT7oA+maJ+WZRmpVIp9UMr63H2twt/XbiEP9ly2trYWu4S2bacMw7g80hrEkGPHjk0F20UCJerQzA8ZA1C+VeNHrGT78rqN7r5qFdELReQO0kkQxwKoQ7cQQM+dFjU4tbS0XHnw4MEkwAXBuhQAmlQHQ2uqhKITsn//fshms0VdNtxGC1W1jnhF5Ur6Eady1w/KHSxnXV3KEWa3B44QEskHamsF3osQiT1dBnD+fCSTyeZ77713McCkYBFCllak9DFiz549BuvmiYRKNr4Ve5G8CJdbzKsca8vrQxyGkJRDFDEyNoQiegjp/377F8YZmXCJJkIIZLPZZQAXYlgLK1XwuHD69GmpULFBdt4VBBALSxQxLN15QYqXG1HEr7zg13WmQXhCCEx+rIIXqbqMZfHoxLEMw4CGhoYFAJMWlmEYC7weKO6mqwslY16ZpgmGYZRYUmzLIF3OuxCUIB5AXavKyzydY/pdtxLuYJBCJ4pliR66en1eVOdHNE+0HACgqalpIcCFPCy0sMpgz549RS6eqBVQ5AoCFLuB5cRgvMawvLidXraLwhWrFG6CKXoQ6d/1nkQquwdl4sV/sq6pqWkBwAWXcIHqYPX6dtBlZGTE4F09WcugTKD4N0oQeBEw0Xw/sS4v5SlH3MKyzMqxMNncLPbvyWUit7AukYVAZJapYRjQ3NxcZGEJvwGG6DE6Oioc2I3/m+/GUW7LoAyv1pZovt/Yjdd9+N13FHh1QdmHjU+ElLz0607AdFxokXA1NjaeF6x8Pj8TfAzaV8eUpDOwIsTHsKi1xYoVC2txlYOOW1iOWHktX7muZDnHCus4uvvlxYr+jZ7KBWQWlmgZIQQSiUT7nXfeOT0Fk9ZVnGINUTI0NFT0VuUtK358dpWwiC6UjCAeUl1h0plXrqB52Vcl3EG/riKbAe84jpFMJjHrXWBlidbhp/379y9IAcAsrwfEN8UFRkZGDN7tE41IybuDbhYRQDipDaplQYlV0NZVmOcoSPgHkYLPy3lU94UqhkVxHGdmghDSE01x40mhUCixrkQixbt+OoLlB79CWM48r0Tl/pWzbbmWmeg6TwbeeWKvZl5eSLJ1JzPeexIAMF21Q3w7qMnlclKxUvVEZxG9WXTxsq2uK1rOvLD/VxG2i+elDGG8jOKEKF7lNi+ZTE5PGYbRHWE5a50i9d61a1fRCVW5hXwnZ9UNHYbV5WWZ7vGDFBs/BLn/oMSNdWX4GNbkfMMwjLqNY+ne86L1GhoapicAYEYYBasHzp07p4xRyTp0eh3Gxy9uouhFrIIQpzCtrXLjZH6P49XdQY/lAiqLSkQ6ne5JAcB06RqIkomJCYOPUYliWACl7qAssFgufoTD6/xyjhHV+kGKjl9E1pYAVLBJVNeFEALpdHp6CgAw6O6TXC4HAKXuICtGonk8/HwvAhaE5eBlvp84lu465azvZV9hbas6j/SaooXl/2XT0NDQkwKAaQBQ4msj7uTzeaFQidIW+MTRqALCfm+OKMWqXOsqKnHTPQ5vGXDzDECryhepVKozAQCNXjbCN8QFRC4enfhvDLKE1SVHVjavy/2Kle6+vG4TpoCH9eIQxWdkqwIKmBJG9BtSANBQ2eLULjSlgbWseOtKFnjn/wYo72Wg+3D5sbjCFLByXcMgra2gLTPqCmKwvRi/I7AmEonGFHi0sJAL2LYt/FqzKMCu0zIYptUVtWsomufHfQySIMXMzTLjvxaDwuWOW5zPMIyGBACkIyxT7JC5hPwyAHANvodRNp3jVUKsdI4XpXXlVpZy9+PBRawbRAKl+tswjPMWlizgjm8DNXwAnXULKV4Cs0EQhAtUrbGtcrb3K9rlbov4Q/JMoEtYLjIrhnUD/VoCQY3W4GVbLwIUpLVVrrVU7vpBbytz/+pR3ERWk+hX5hLSZYlEoiEF6BL6hresRG6hbBwsL/sPsqxel1ejWEVpXZW7b36dyYcRWwYZVG6gKOhef5IfIOzNKBqcj4UVtSjc7XJds6CtraBiXeXuI6xgu2x7DK2cR2Q5sX/zk4gUIcQ0DAPdwjJgb2KRi6jzoAZxUwcRv1ItC1qsdAg78B5moB7FSozKimLn8ZPjOIUUAJiAcSxfsMIkEihRbEu1ryjwG7sp1zXU2TZoV1DnmF7WLac8AvGqa7dQJlIqHMcxEwBQkPmQiD66gdhKIGsY4NdRbau7TVCxLT/ubNjWWDllQc7j5gbKpsn1TWphIQGgY1Hx64Txgqhm11A0r9xYWxDre90eBck/XoWKbuM4TiFBCBlnZ7K/iJpUKiWc79UVFLUu6uJne7d1vVpVsvl+3cUwgvVBx7qC2pau4rZCraKrKQqraupv0zTHUgAwShfgW8Mbzc3NU2O6y9xqvyIUBuU+WOWKUlCuYdCuXZDipVpe70F4lSBpuINgWdbolGAh/mGHEBHdkJNJb0WWTbWkNeisF0TAvVKuod/jlHtMdlvFta77wLubSLG/lmWdSxFCULB80tTUBPl8fup/NyuVXS5arxwRC+JB9rI8CrEKyn30sn7Y27vdI/Xg5agsLMdxin7Z9UzTPJsCgFF0B/3R1NQE586dAwAoeTv4IYprUK7VFUZsK6hgvV/RUxGkdSW5N+rmwXNzBVmREllalmWdSxBChtmdonDp09raSnRFio1lRR3H8BOQly0rd34lxSrK/0XXePLc1qULqEpn4IVKJFyEEMhmsydSvGCJDoCImTZtWslNK3s7UHjxqHRag876XoVKtsxvjKqaLE+/6wPU3zMli+eKrCyRK8g/S7lcbjgBACdUDxhSRNFdunDhwqm/RZaWLHhYtEOjNC2h3Em7Mi7r+7W2dK2dsKwtnXXKjVXprq/xXMXepRGlKOi0DvLrZzKZ4ykAOMHuGF1CbzQ2Nkp9b5ZqObd+rRy/y8KeF4Yr6EY1XMdaw80lZCeZl2Ka5okEAByvRAXiQlNT09Tf/AkHKM3JqoQFq2t9uVlUUYqVzjH8CloQx9FZn76k0Hs5Dy9E7PPC/y0Jyp9IpNPpw+Q8Fa5OuIT1Vuzq6iIApWavTLxoOfy4cDr4cRH9iJHbtuWKVRDC42e/QbuS3HOlfMjiZLmp4leqoLsirYHceOONryYaGxszhJCT7A4RfS666CKpCSubx/4CBBvH0kVnG79WVRRiVQlXUIRoH/gMFSOKXfGWlcjCYtcvFArHP/e5z+XoiHMH8SRrU3SHzpo1a+pvkVXFUslzrCts5QiZrgB5WTdKsQrb+hIQH5NKA1nsihcukRWWyWQOAgBMCVYF61HzdHR0CN8a/Enn3cEwCdotdFunXGtLND8osdIhbGusXg0CnVZBHdHKZrMHAQBSAACO4xxIJBJV05JVa3R2dpJMJmMYhlHijwOI3UEdgVDd5GG5M17X8yJgsvlhxMBU64Rx7lxiVxD7ILECmUjJJt4tBAAYHx+/YGERQvbons9aFrSwyr548WIAAGkrB70QAOL4lqq85caqVPvUXdfPMt31w54Xhiuogl5Tw/DWOljLzxWPqr5uoiUKvAMAjI6OvgwwaWEBwG7+YHE6gWHT1tYGqVRKehF4orRkg3CPdJdXm7UVlvWls42GWNXFA+bFDVTFsCYmJnYBTFpYDQ0NL5PzVLZ2tUPJzdbZ2Sl8c/AXZ2oHAVpNsv152Wc5wXa6rNz5fl0+0Xrl7MvLfnX2ic+V2rKybdtNtJxrr712D8CkYDU1NY0DwJGK1qjGWbx4MaFugMrMFU0UlQvoNvlBd3s/rqEfl1Fnnmi+H6tIZz9ez6usZbheLQFZKoOolZAVLf75yOVyB7/97W9nAS60EoLjOLuqsTm+Vuju7oaGhgZPLR+VwKtIBRXDUm0TtFiFFbfSFTSNa1y0YZzCL4LGBmXAnbeueGEDABgbG5sKWSWYHb8YVaUqSYA3hwFc5vLMmTOJzMICECeS8n8HjVdLrFy3MCzXUDQ/LLHyc2zZNXQcp67f9irBomLlJlqjo6NT2sQK1kA1WAA1RtFde9lllwEASN8efFPt1E4CdO38uIpBxK/cttXdRnfdoMRKt7x+qPdniH9Ru6UzyLqznT59up/uk/3sSz9w0NYsGpuJk+kaEAYAOJO/kEwmobOzE8bHx6cEi05uyaTsuY6k4D6D2V6Wh2FV6c7zWz8/++atZZeXfl24g17FSvaMAACMj49PadOUhdXW1raPEDKqY2HV+kkOs/yXXHIJYc8fn0haqXiWF8srTIsrCrGSEWbcSkS9BtspskYm9mUuEiv2+TBN8+xDDz10mO5zSrCSySQhhLxIz3GUb/sapyiWNXPmTGhubi6xsGzbLroYIoK4v4NwDctZJ4iAu2x+kPOCcg1lsUgd66pekDVCqcSK/o6NjQ0sWLBg6mQm2B07jrOFHiDucayAg+8AjGhdfPHFxDAudNOxLEvpGrJlKnfyVHDNbYOwuIJYP6h5Yca2GCvC9eGJs0Gg0zooC5mwz8fp06dfYPeb4A6ygW/Fiqo1q8YpuvPmzp0L6XRaaP6qAowA4Z9jL+JWrsXl1aqSLQt6np/tROt4tK5iTTnxK1WoZHh4eAN7nCLBSqVSG8h5SgqBKCmxspYsWUJkprDMtw/yfPu1wLzGufws87JNNVhbupaQi3UV22C7CF6o+Jc3G78SpTVM4rS0tGxi91skWG1tbSOEEGECaTnBx2ol4DoUiVZPTw+0tLQoTWCZS8j+HbVrqLuuarnX7by4jNXgGgKgdeWGm0uoSvuhv+Pj4zsef/zxs+x+E4IDbRDME/6NFFFypy9cuJAAgFCweNGq1hZDfhuvy/0InO5+yhG1IF1KAG/WVdzg9YF3CUVCpRO/OnXq1PP8sUoEy3Gc5+sh6E4J08rq7u6GtrY2AFCLVtixLL/WVxjBdrftyhWSIF082Xqy6+Q4pSNzeNlvXOCtKla0RO4g24rOTidPntzI77tEsBKJxIawHpw6gL0TCQDA0qVLpz5Socp8F1lZXs591O6h3xgWXa67TVRipbue6Jow163urCsZMndQ9LKWPROjo6Ml3l6JYE2bNu0wIeSIysKKm4iFZGUBAEBDQwP09PQAQLGVZVlWkR/Pnm8/cayyCuxRpMoRqqAFyM+6but5dQMBpNZVyY7iZl3xWuAmVKp4LitWuVzu0Pr160tGkCkRrMmDCtMb/F7YOqPEylq8eDFpaGgAAADLsqbEis/PYi/w1A5CeDl4Fbty3EK35eVaW6p9+JmnqofKFZRYV3WFKHYls67cuqyNjIyUxK8AJILlOM4GvgBxJ2DxZc8rAQBYwGTrUrGik+rCAZR3/v1aY+XGr9yWByFKsvlhixU7z0ugvR5e8DKx4lsGZa4hE78qcQcBJIIFAGvcAu/1IGJlUmRpTZs2rcg15HNTeLdQR7SCdhWDiF+x63jd1qu1pStC5biLAGK3h1LPgXYdd5DGblXWlaiVcHh4eJ3omELB6urq2u04zgG+UHEXqTBjWQBA5s+fP+Ua0jgWtbBElhZbpqDjVqJ96q6rs55smZdtyhW2MOJd7AtE4QrGT51cEL1oVakMMtECAMjlcgc3bNiwR3QcmYUFjuM8TXcgCgjHlbBF69JLLyXJZBIAQHgx6f8iCzdq1zCoOFdQVpVqfZ15XtcFkJ9zdAVLcWsVlE28ZXbixIknZcdQCdYKkYvCF7BeLoZPDOBu4HQ6TebOnVs0/jvbcmhZVkkKhJd4VrmuYZCCFmQMS7YsSrHiHsZ4uxsuqNxB3sWj9zjbyKRK8Tl8+PBTsuNKBau1tXUVAGTpQekNFne3ECB0Kwu6urpIZ2cnAEDRxRQFH/kXhcpF9Ftur9uHEWyny7xsUwmxAoCih4vfpZdjxAXeC9CxsPiuOcx22fnz5wvjVwAKwWpqasoSQtapLCxEm5LzPG/ePKe1tRUALiSV8nEtWfcduo0f/IqczvpBW1t0me76YYoVZdKFQVeQQxa7UuVgiVJ5XnvttecefPDBrOw4UsECALAsawVfGN4NjKuIhXCzlexw/vz5Dv3SDgBom8w6ohWUBVZuoJ0u97pdEKIkm+9HrDDIfgEXV1nZOug2NtzRo0dXqI6tFKxEIvE7neBvXN8kIbiGRTs0DAMWLVrkJJPJqXPsZjKzLwyVe+i7kJr7CcLiUm2nO18lbPx8t/KqxEoRZNcuaxwRuYMyl5AXK9H9ferUKWn8CsBFsLq6uvYTQva4WVFxtbIAohGt+fPnO7TlkIqWzNqiNwNdl/31XSgfgXa/6/i1qsJ0AQHUYgUAYNu27CTXlSsoutd4oeLdP/5eZlsHWc9hYmJix/PPP39YdXylYAEAWJb1FF+oOAtUBJTczQ0NDXDxxRc7icT5yyEzpYMQLT/WWBAWVzluY7nz/YoVxbIsFCsJQbiDTLKo0roC0BAsQshTOkIV5wsVQt1KzjsrWoZhlDQFi/oeqlIeyo1h6W5TjluoWu51Pl2mM4/FzQ30Ilb1Bn+uRK4g3wVN5Q4eOnSofMHq7OxcSwh5ze1NHnerKwrRSqfTMHfuXCeZTIJhGCVmNsBUK5XwBRJF7Ipf120dP8v9zPcTXHdzA72KVZxf2gB67qDIJRSlN7CWFSEETNN87V3velfJ+Fc8roKVSqUs27aX825I3AVKRFSiNWfOnCn3kE+8m3zrSzuN6rqIfiyvICwuuo7XbYOaDyB/8OivH8sq7mLFw4eIeBePtax4C4tPaQAAOHr06C9uv/12y+24roIFAGDb9s8xfnWeEG5MAwCKTmo6nYZ58+Y5tN8hGxdgY1my4CX7G4VrGIT7GJSIeW0JZOcx1iyKFYfKs5K1CspiWKIX7YEDB36uUw4twers7FwDACfZAtYzIbQcJkXHmDVrltPc3DzVjUcU4JRZWHQfXq+XH9dQd59elvndRobMBWSnyYcJxUoT9n6Uxa/4ZGh+XUIIFAqFEx/96EeFw8nwaAlWOp22TdN8nHc56lm4QnIPi06oYRjQ09NDWltbi/oe8lnEKvFycxHDaDHUWc+PtaVa5teqoigy2AFQrIrg7y83C4sVKz7YDnDeHfzrv/5rW+fYWoI1WcjHeL8TCZwSSwsAoLOzk3R1dZFEIsFbAyVNyLx4AYgTff3Ersq1ptyWB22Jqawq+jd1AVGs5KiC7aJAu1vsin+pHjhw4DHdsmgLVnd393oAOCaKk9QrXuNBmgivSVNTE5kxY4aTSqWm5vE3Cm9qA4jHM9O5ZkEH5P0KFV3uZT6AXmCdESvpob0etx4QWVf8i5Mf5030QiWEQD6fH/ryl7+8WffY2oKVTCYdy7Ie5wsrq1A9EZJolew0kUhAT0+P09LSQlhB4a0umcsoMuXZOoRheZUrVEFaVewyjXgVihW4961UdcGR5WCxL9ShoaFfvvOd79R22bQFa7KQP9cRrHq7qAChtR4Kd9rW1kamTZtG2O489JfPexEF6ssqVEAB+XJEToSO+wcwNZ6+ZxfQrcz1BG9d8e6grDsOfx9OuoM/83JsT4LV3d29kRByVPSmrjerSkRIoiW8Rul0mnR2djpNTU0EQDxeE39z0PVUFpfoOgYVaGfXUS3X3U5WZlHjECPmqhsVxUqBLNCucgdVvTNyudzhBx54YKuXMngSrEm38DHRDSG6meqRkG5sep1KTmpLSwvp6upy0uk0occ2DEMoWmygXiZcABeuXdCB9qCsKpmwqoTKJbAOoHAB61WsVOeYD7iLgu0iK4u18o8cOfLYFVdc4UkoPAnWJA/zNziNo7DU60UGCFW0aOpDSfpDW1sbaW1tJXSoGloGNigqEy4RQVhd5bqO7DJVWWRCpdECCIDxKm1k8StZ7pWqKw4hBPbt2/ew1zJ4FqwZM2Zsdxynly8AWyG+gvVISG9mmmRakrMFAJBKpUhbW5vD5m1RWLcQQByHEN1Uflz+IISKLyOPrIysULm4fwDoAkqRPcsisRJZVrLvbtL7a3R0dPPmzZt3ey1Xyn2VUizLejCRSLyBv6Fo/zfkAiLrM4jdQrG1VfR0JZNJ0tbWRizLMkzTNBzHKRIB+kvzuqh4sOux5ee3FRbIg/vIw1rqouPQMqoeIvqrIVIACqGSlRGRW+tu7iD7QqTXat++fQ/5KYMvhWlqanqUEJIRxT7Ymyqkh7XmCOkBoKIldBMBzltczc3NTlNTE0kkElPLRe4Tb6GI4lx88J53C3UsMtn9wp4j0X54y0/w4OhaVEqrCsVK/MIQxa5U6Qx8v1fOIpvIZDLayaIsvgSro6Nj1LKs5aKbDy+4mBAfBt5NLHlok8kkaWpqIi0tLU4qlSIiS1j08uGFi1/XrbXRbeKP7+aO8mJFRYq4vxWVQgWAVpUbIsFSWVei1kF6Dx0/fvxXzz333Dk/5fDtw9m2/aDqZqaglVVMiA8GFS4DJMIFcD4dorGx0WlsbHSo1cVeH5XV7CYq/LXXER+d+QKLSseaoufEVahQrOS4WVciq0rlDhJC4OWXX37Qb3l8xbAAAGbMmLH67NmzBxKJxCJaETpaJqJGJyZUBmwKBJslWnRhDMOAdDpNzheDgG3bxuQNZdDl1GJ2ewHxlrXM0nart+h4k2XyeqJcb0K8T8Woro3KFeQtK1mwPZfLHfzOd76z3m/5fAtWMpkktm3/TyKR+Dofw8Dgux4hCxe1uAgUW1wlT6phGJBKpehyMnlzGpP/FK0vEiNWZFR1kgkZu66m5SQDhSpgVK6gmzvIp9AAABw8ePDH119/ve9rXJayGIbxMCHEkbkCzHroFioI2S2hwXneXZT3+DUMSCaTJJlMklQq5aRSKccwDGIYBrXIplxJUVBVFdtig+RsDGryPnJ0yiepI7p+ZSKynOmvSLSoOJmm6dpvcPLXPnDgwH+XU8ayBGvGjBmHbdteLYo1IN6J4KGiwkU7VxPBJN4wkSCJRIJQIUskEk4ikXCSyaRD/04kEo5hGFMTnHdJhRMrgIrjqspmAApV6HhxBUVJo6xonTp16tmtW7cOlVOesn03y7J+KBMqt/8RMRE8ZKzVRcWLHlBbxASItpVNfsqsJVIAKFReET2rvCEiG/NK9D8rdHTfe/bs+WG55SxbsKZPn/5rx3GEHaJZ8Obxjm6n43IPAxfyuUQCBhCOCLltR8tAy6Z1ElCogsNP/IrvN0i3z+fzr77lLW/5bbll8h10pzQ0NJimaX4/kUj8Ey9WbEsTRRZ4RdSEHKAvOhSUihX7y/8t+l+1b9H//K8n8H4qD5V1JeozyIsUH8PitwEA2L9//wOPPfaY61dx3AikOS+dTv+AEFJw61uIN1b5RGR1FR0Sii0daonRif8/KVlPtC5rPXmqUAXOQ10hcgd1rSu+4cVxnNzQ0NCPgihXIII1ffr0YdM0fyFzCTGWFQ7sQ1vBB5ePLfFunKfYk/JAla9r7NCJXem0DLLWFe8OHj169LGNGze+FkR5A0uYchznv9jguyiOhYRLlQhYIPB1qfX61Bp83IoVLFas6N8i64q6g9u3b/9uUOUKTLDmzJnzgm3b23iXkHcPI4zF1D2ih76aBKCay1YPuFlXvDsoahF0y786e/bsCy+88MK2oMocaEq6aZrfZSsKgMJUzbgJWtgTUp3w7iAhROgKsn+LYlcAAK+88kpg1hVAwILV1dX1M8dxTvLDkYhaDwFQzBCkUui0DMosK53MdkIImKb5Wmdn5y+DLHeggtXc3Jw3TfPHvEBhKgOC1AZuwXYqUCLrih+z/cCBA99/5JFHckGWL/Beyslk8gFCiMVaWQCl/QspaGUhSLTotgyKUhhM0xS2EAosLGvXrl3fD7rsgQvWzJkzXzVN85esS0h/Rb36EQSpDkQtg6w7aJqm0NoSdXo/duzYY4ODg2X1GxQRyjgwhJC7yKQyqRJJmfXDKAaCIBwyL0fmClL3j1pV9G82nUE0jMzg4ODdYZQ/FMGaM2fOi5ZlrRZZWQClKQ4IglQG9pmU5V3JuuGIxIoQAq+99trKLVu2DIRR3tBG2rMs699ESaRoZSFIZVA9e7x1JRIqXqxELiEAwI4dO/4trDqEJlhz585dadv2AG9uylIcEASJFt1OzqLYlawbzrlz5168++67V4dV5lDHMi4UCnfzQqUCrSwECQdd64pvGVTFriTW1b+UMwSyG6EK1qxZsx6zbfuwm5WFIEi0qKwrUbcbUbIo3zKYyWQO3nDDDb8Ks9xlj4elIp1OW6ZpfieZTP6H4zjSsd3Z+ZhkiiDB4tW6YtMYRHlXokH6AABeeeWVu3/961+XPeaVitA/b9Pe3v5D27ZPsxYVWlkIUjn8WFd0ko3KUCgUTp85c+bhsMseumB1dHRMmKZ5P9+ZkhcqflRSBEHKx491xQqUKKtdlHe1f//++9auXTsRdn0i+YBgY2PjfzmOkxUNX4FWFoJERxDWFW9hWZY1sXfv3u9FUf5IBKunp2e4UCj8wC3ojlYWggSHqM8g/Q3Sujp8+PD9fX19p6KoU2SfaG5oaPhXtLIQpLKorCuRQLlZV7Zt57Zv335PVOWPTLBmzJhxPJ/P/5iPY/Gqj1YWgpSP3xEZVBaWyLo6dOjQ9wYGBo5HVa/IBAsAoLGx8U5CSBbzsRCkMgQVu5p8bnMvvfRSKJ2cZUQqWDNmzDheKBQeEo1ICoBWFoIEgcq6Ug3M5yV2RQiBQ4cOfX9wcPBYlHWLVLAAAJLJ5F2EkLyXGBaKFoLo4fYcUdEST2EpuwAAHqJJREFUfQlHtxsOta5efPHF0Do5y4hcsGbPnn0kn88/7MXKQhDEH7rWlSx+Jfv0/KFDh344MDBwNOr6RC5YAADpdPpOx3EKfBwLrSwE8Y9X60o0dIxbn8HJZzY/ODj47QirNkVFBGvWrFmvFgqF/5Zlv6OVhSDlo2oVFPUXFMWwRH0GDx8+/OMwhj/WoSKCBQCQTqe/6ThOQXRSVd0JEAQpRfXM8BaSyrpixYq1xpjJrJR1BVBBwaJWFj9EBQWtLATxjyjvSvQVHFGQnR+vnX02X3311R8NDAwcrlS9KiZYAADpdPrrjuNk2ZPiNtgfWlkIUozXDs6q2BX9FVlXtm1nBwYG7oy4ekVUVLBmzZp1rFAo3M8LFcayEMQ/OlntqtiVbDTRAwcO3Fep2BWlooIFANDc3PwvjuOcE4kWWlkIosaLdSWKXakC7Wyw3TTN0ZdffrlisStKxQWrp6fnVD6fv4ePZblZWShaSL0jegb8WlYqCwsAYO/evd/u7e0dibqOPBUXLACAzs7Oux3HOcm3aOjmaCEIog60q2JXspZBup9CofDa0NDQf1a6fgBVIlgdHR3j+Xz+LjeRQisLQc7jliTqlsbglnPFWle7du36p40bN45FVTcVVSFYAAA9PT3327b9Kt9iiKM6IIg7Ol1weJEqFAqufQaz2ezhRCLxw0rXj1I1gtXa2prL5/P/xJ50Po4FgFYWgrglicrcQZUrKMu72r59+9d+9atf5aOsn4qqESwAgLlz5z5sWdZukYUFAGhlIYgAt9iVyA0sFAoleVd8/GpiYmLPkiVLHq10/VhC/S6hV9LptJ3P5/8xmUz+PJFIgOOc/5Yh+91C/n8A/JYhUj/opDGwrp0o0C5zBfk+g4ODg19+5plnQv3OoFeqysICALjkkkt+aZrmJtu2hQF4mZWFlhcSd3TTGETjXLGWlezDqGy8+OzZsy989atfXV6BaiqpOsFKJpPEsqzPOY5D+HgWBt8R5AL8M8FaWG4dnOkk6oIDAGTr1q2fe/vb3151D1rVCRYAwIIFCzabpvm4qI+hChQyJK7opDG4DR3DW1eyNIZjx449tnHjxo1R1c0LVSlYAACJROILjuPkVWkOCFKvqFxBGmjX7TPIuoOO4xS2bdv2D5Wun4yqFay5c+ceyOVy93vNfEchQ+KGWxqD22gMuomiAAAHDhy4t7+/f3+U9fNC1QoWAEBbW9s/2bZ9WtRlB0B9IREkDrgF2t3SGNgWQZVYTXZwPrN79+67KlBNbapasHp6es7k8/k7RWNloVuI1CN8a7nKumJzrXSGPt65c+fXq6GDs4qqFiwAgNmzZ/+XZVn7aJoDK14AaGUh8UUn58ptNIZCoeAabCeEQCaT2d/c3PxAxFX0TNULVnNzcyGXy31F1GKIAXiknhAF2nU/KkEnkVgBAAwMDHz+0UcfLVS4iq5UvWABACxevPgXlmVt8OoaopAhtYqX/oJ8zhVrVbmJFSEEzpw5s7kak0RF1IRgAQBYlvV5x3GI6ISjaCFxwmugXfUxCdVIopPPkPPCCy/cXo1JoiJqRrAWLlz4Qj6f/4mqYzSCxBFRoJ3vLygaOsZt+BgAgKGhoYdfeOGFrZWsnxdqRrAAAJqbm79g2/Y5r8mkKGZIrVBOoJ2KlCiVge+CQwgBy7LG+vr6qjZJVERNCdasWbOG8/n8nbTFkO+wiQF4JG64BdpF7p9ORjsAwK5du74+MDBwvMJV9ERNCRYAwOzZs//DsqxXREKFVhZSy5QTaPeaxjAxMbEXAL4bbQ3Lp+YEq6WlpVAoFO5gM98xAI/UOuUE2mWtgrJAOwBAb2/vbdU0kqguNSdYAACLFy9+slAoPIUjkyJxRSVW5Qbah4eHf7N69eoVFa6iL2pSsAAADMO4jR3NQdTXUAQKGVJt6Awd4yfQLnIFHccpbNmy5fMRVi9QalawLrnkkn25XO4+tssOuoZIreHmCsoC7bKcK7dA+969e/+tr69vb9T1DIqaFSwAgO7u7n+2bfuYqJ8huoVILcOKlSrQzltYqkB7Lpc7OjQ0VNWjMbhR04LV2dk5JutniFYWUu2Um9EuGo2B7YLDx676+/u/uH79+vGo6xkkNS1YAAALFy78H9M014m67ACoA/AoWkil0BUrfhRRvlVQN9A+MjLy/Fe+8pWq+mSXH2pesJLJJHEc51bHcUyRCY2uIVJLiCwrnVFEVRnthBBr8+bNn6yV/oIqal6wAAAWLly4I5/P36fKy0IrC6kWdAPtXqwrPtBO47oAAHv37v33rVu3bo+6nmEQC8ECAOju7v6GZVlDojQHAHAVLgSpFKq4Fdv9hnX93IaPofvK5XKvHjly5FuVrmNQxEawJgPwnxFZWWyzrugL0ShiSFR4ybliP3YqEirWuqItiHzsauvWrZ+u9UA7S2wECwBgyZIlv8zn87+jF41/26hiWihaSNioXEGRG6jrCtKJ9ypee+21p1etWvWbqOsZJrESLACARCJxm+M4Ob7bDusaGoaBAoVUHFWrIJtzxVtVvDsoShK1bTu7cePGT1a6jkETO8GaP3/+/mw2exf/RVvewkLXEIkSv91vWKtK1bmZ75b28ssvf2tgYOBAVPWLitgJFgDA3Llz77Isaw9tKVFNPChaSND47X4jEi1ZzhV7r09MTOy1bfvfK1DV0ImlYLW0tOTz+fyneCuLt7TQNUQqAZvUzI9zxVtX/CSzrth9bdmy5ZZaHDpGh1gKFgDApZde+mw+n/+/fACeT3NA1xAJE1X+H53cPtXlpfvNsWPHfrpu3bpVUdYxSmIrWAAAra2tn7Zt+5TqI6zoGiJh4af7jSiFQWRd8SkMhBAoFAqne3t776hAVSMj1oI1e/bsU5lM5ov0huBbDdE1RKKEdwVF3W9EozCo4lasddXX1/fZ/v7+k5WsY9jEWrAAAF73utc9VCgUVsnGB8JuO0gY6LiCfIKoLOeKTxLlY7MAAKdPn1775S9/+SdR1rESxF6wkskkAYBbbNsW5mbxIzvwoGghXtF1BUVDHosy2ekwMrKB+Wzbzj7//PMfj0PnZjdiL1gAAAsXLtyby+W+yV5oUesKuoZIGLi5gnxAXdZnUOYl7N69+xsDAwP7KlnHqKgLwQIAuOSSS/7VsqxB2QB/6BoiQaDrCgYVaB8bG9s+bdq0eyKuZsVIVboAUdHU1GSZpvl3yWRyk2EYST6dIZFIFFlZ/HJZCgSCULy4glR8RN8U1Am0T+7P2bRp099t2rTJrEB1K0LdWFgAAEuWLNmay+Xud0soBUCrCikf3VZBXqRkfQX57jcHDhy4b9OmTZsrWceoqSvBAgDo6en5imVZQ3y3HTY3SwaKGCLDjysoiluJPiohsq5yudyr+/fv/1rE1aw4dSdYXV1dY/l8/hZRAB5bDRE/hOUKUuuKjV3R/W3ZsuUTGzduHKtAdStK3QkWAMBll132ZC6X+6lf1xBFC6Go7g8vCaK8K6gaieHo0aMP1+qXm8ulLgULAKCjo+N227ZPqLrtIIhfdFxB0aB8sjQGuq98Pn9iYGAg1t1vVNStYM2aNet0Npv9O/aGQNcQ8UKYriCfWErvya1bt36yt7d3pALVrQrqVrAAAJYuXfrbXC73C/7GQNcQccOPKygSKpkryMetAACOHz/+s+eee+7xSCtaZdS1YAEAtLW1/b1lWSf5WBa6hogfWOuKjVm5JYi6uYKFQuHUtm3bbqt0/SpN3QvWnDlzTmWz2c+IAvB8sBOtLAQgmO8KurmC/P23devWT8V9JAYd6l6wAACWLVv2aC6X+7VItAAARQuZQscVZK0r1fjsKleQWlcAACdPnnzi2Wef/VmkFa1SULAmaW5u/qRt22dkqQ4oSogKmSuoSmHQcQVN0xzdvHnzLZWuX7WAgjXJvHnzjmcymTtEAXh0DREAPVeQTUaWWVeqBFH+fuvr67t9YGDgaAWqW5WgYDEsW7bsoUKh8Ds+BsHekPwoDywoWvHFS9xKFmTP5/PC+BXf64JxBZ9csWLFwxFXtapBweJoamr6uGVZI/x4WaxYqUZtQNGKH27XVDVkjCiFgf9qs6RV8PTmzZs/HlEVawYULI5J1/BTsr6GmPKAAMg/MS9LEFXFrUTjXG3duvWTAwMDJypdz2oDBUvA5Zdf/mg+n5cmlPITDwpZfPCSzU7vFd0UBllfweHh4V8/++yzj0Vd11oABUtCW1vb39u2PazTaoiiFU/cUhh4V5D/fqCqVZB3B+l9VSgUXtuyZcsnoq5rrYCCJWHOnDmnMpnMzTqthkh9IevULEsQpcF2jRFEYcuWLZ/ABFE5KFgKLr/88t/mcrmfuA1Dg65h/NBtFRR1vRG5gnRS9RU8evToQ/XeV9ANFCwXuru7P2VZ1hGRpQUARQF4FK14oCNWfDa77AMSog9JiILsuVzu6ODgYN0OG6MLCpYLM2bMGM3lcn9r2zbhg6tsMzSmOsQD3RQGPpvdzbri3UEuHko2btz4sW3btp2JqJo1CwqWBpdffvmz2Wz2B6oO0pjqEF9UnZqpaKncQNl3Bek9c/jw4e+tX7/+6QpXsyZAwdJk9uzZd5im+YpOmgO6hrWJbgoDb2HJXEG3fCsAgImJiZdfffXVL0Rd11oFBUuTzs7OCdM0/8q2bVP0psRUh9pGZxQGnRQGj66gtXr16g+vWrUqE3V9axUULA8sXbq0L5PJ3CkbVlkn1QFFq/pQXRNRgijb0ifreiNyBfnwwa5du77e29u7Nap6xgEULI8sXLjwm4VC4QVR1x2dVAekNnBLYZDlWqniVvQ+AQA4c+bMpiuvvPKuClez5qibT9UHRXNzs5VMJj9k2/agYRjthmGAYRhTN2IymQTHcSCRSEwJFn72vnrxkm/FJ4fysSpWtFTZ7JZlTTz//PMf6evrsytQ5ZoGLSwfLFq06MDExMTnRG9PkWuI8azqxE/XGy8pDLJs9r6+vk/19fXtjbq+cQAFyydXXnnlD/L5/BMysRIF4pHawIsrqBIrUTb7yZMnf/PUU089VOEq1iwoWGXQ1tb2t5ZlnWCD8KJWQ0x1qD68jsIg6ieYz+elKQyiVuRCoXBy8+bNf1eB6sYGFKwymDt37mvZbPZm27YJ/0bl821QtKoHr3Er1djsfNyKjV3x2ewbNmz4yMDAwHAFqhwbULDK5Morr3wil8t9l+2qoxqKRgSKVnToXAu+n6BsFAYvKQwHDhz4zrp161ZEUcc4g4IVAPPmzftcoVAY5PsailxEFKfqQ2RZiRJE+fQFftgYWQrD2NjY9pGRka9UuJqxAAUrANrb2/OO43zIsqwM+3blW4cA0DWsJF663ugOGcO7gaIUhtWrV7//ySefzFWgyrEDBSsgLrvssl0TExOf4btgiDpIA2CqQ9T46Xqj8yEJNudKlsLQ39//ctT1jSsoWAFy1VVX/SCXy/2Mv3lFnaRloGgFj9+uNyLLis9ol40eCgBw4sSJX2AKQ7CgYAVMd3f3LbZtH2JbifiYFoDcNUSiQTZkDC9Wbt8UVAzI92pfXx+mMAQMClbAzJo162wul/uA4zgmb2WxrYgAGM+KAi/5VqJPdLGBdd2uN5OjMHwQB+QLHhSsELjiiiu2TkxMfFPUQRrjWdHhNW4lahX0MyDfzp07v7Zly5bNkVa2TkDBColly5Z9M5/PrxY1dWM8K3z8xK28pjCIut6cPn163VVXXfXtqOpZb+BoDSGRTCadxsbGv7Ys60XDMKYbhgGJRAJs2y4aqSGRuPDOwBEcwkWVyc6nMMhiVi5db06tX7/+rwYHB3EUhpBACytEFixYcDSTyXzYtm3CdtfA/Kxw8ZJvxWay63RqVnW92bRp098ODg4eq0CV6wYUrJC56qqrfpfJZO6XxbEwnhUs5eRbsdaVSMBUXW8OHjz4n2vWrPltpJWtQ1CwIuDiiy++o1AovMj3/sd4VrAEGbcSjSAq63ozPj6+Y3h4+EtR1bOeQcGKgI6OjpzjOH8l67qj298QRUuO6pzpxq1U7iC7DfuysW07s2bNmvc//fTT2YirXJegYEXE0qVLd01MTNzBJxrygVsATCoNCrd+gqpx2flWQf660X329/ff1tvbu7vSda0XULAi5Oqrr34gn88/xgsVPywNAAbhveAWZGetWN6yksWt2Ex3uj4ftxoeHv7Vk08++aOo61vPoGBFTFdX1y2FQuFVPo+Hdw1VoGhdQOdc8bFD3g1k41Z8NrssbpXNZg/19fV9LIo6IhdAwYqY2bNnnykUCu+zbbsgyoSnDxjGs9zRjVupXEFRvhVvWfEpDI7jmGvWrPmrrVu3no24ynUPClYFuPLKK7eOj49/UZSAyGdOYzzLG7r9BEUiRd1Cl3wrGBwc/Ax2vakMKFgV4pprrvlOLpf7pcjl4GMlGM8qRTdupdtHUOQKiuJWJ06c+PlvfvOb70ZdX+Q8KFgVZMaMGR81TfNlVSdpPhuepx5Fyy05lI9buX2iS9UiyMatJiYm9r700ksfj7SySBEoWBVkxowZY7Ztv9+yrKzIwuKHopFRT6KlkxzKW1ciV5BPEHXrJ2jbdu655577wKZNm85FWF2EAwWrwixbtmz7xMTEbXyroWjYknoPwnsJsstaBN2y2WVxq97e3r/v7+8fiLjKCAcKVhVwzTXX/DCTyfy3SKh041n1iizI7qWfID9kDOuaAwAMDQ09iEMdVwcoWFXCxRdffEs+n39RNn6WTvedOAuZl+RQXqz4HCtZP0E2bkWPNz4+vuPgwYOfirq+iBgUrCph2rRpWQB4v2VZ50Rdd0TB93oRLT9BdpUryMeuZOOyW5Y1vnLlyvevWrUqE3WdETEoWFXE0qVLX5mYmLiZt7Bk8SwZcRItL0F2UdcbPseKWlqyPoLsy2Hr1q2feOmll7CfYBWBglVlXHvttY9lMpkHVKKlk1QaB9FyC7KrkkNFCaFehow5dOjQf65cufKRKOuLuIOCVYXMnz//9nw+38daC7Js+HoLwssC7LIOzaK4ldu47KOjo9vGxsY+X+GqIgJwTPcqpKOjIz88PPwey7L6DcPoBoCi8eANwwDHcSCZTBaJFT8mPCGkZseJ141buVlXbpnsvCtomuaZtWvXfqC/vz8fdZ0Rd9DCqlKWLFlymI4HL3IN2aRSSlyC8F6TQ93Gt/LwPUGyefPmj/b39x+MsLqIB1Cwqphrr732yUwm8x+iQf8ED5t0P7UkWrrJoapuN24pDLJA+yuvvPKvq1atWh5xlREPoGBVOZdeeumXCoXC8+zDOdlVpGgoGoDaD8J7CbKrRg5V9ROUdWoeGRlZM3PmzP8TZX0R72AMq8ppamoyW1pa3pfP53sNw5hnWRYAACSTyan4FP/L/13LyEZgoFaS6iMSqrgV2yKYy+WObNiw4QP9/f1WhauLuIAWVg2wYMGC4Xw+/z7TNPNsDIvvwgNQ/IDzVLOV5ZbJLhuBwc0NdMu3sm07/+yzz76nv7//tQpUG/EIClaNcPXVV78wPj5+O/vAyoLwtZYJ71ZOVZBdZ/RQRZAd+vv7b+3t7d0WZX0R/6Bg1RDXXXfdA5lM5keypFLWzamVILxbOXWC7KJMdt66EvUWOHz48AP4EYnaAgWrxliwYMGtuVxuGxvDUQ38V81BeJ0WQVkmu5dOzSKxOnv27Avnzp27Pcr6IuWDQfcao6OjI9/Q0PAey7J6DcOYSRNJAQD4v1lEQfhKJpbqtgiqMtndkkNZUWetz3w+P7xmzZr3DQ4OYnJojYEWVg2yZMmSI5lM5oOWZVm8a8haJfzDX+24DRfDuoLUsmInnVwrQoi1du3aDwwODg5Vur6Id1CwapTXv/71ayYmJr7ENvG7jexQLUF4P8PFsC2CXoLsvIs8ODh4x6ZNm9ZFXWckGFCwapjXv/71d2cymf+ppeFoyul2I4tbyca24hsijh8//ujy5cvvi6quSPCgYNU4l1xyySfy+fwAb2nxUzUE4XW73Yg+IKFjWYlaBOkxx8bGXtq3bx9+8abGwaB7jdPZ2ZkdHh5+t2mavYZhTAeAqgzCex3bSjYIH5++ILKu+CFjTNM8s2LFinfv3LkTRw6tcdDCigGXXXbZoUwm85fnY/C20CXSDcKHYWnpiJWq241b7MolyO48//zzH9q5c+f+wCuGRA4KVky47rrrnh0fH/8nfgRNP6IVBTKxcrOuVJaVKJN9165dX1u3bt2KilUUCRQUrBhx9dVX/3Mmk3mMf3hFMZ2oWg51WgT55FDR1268xK1okH14ePjxT37yk3cGVhmk4qBgxYhUKkXmz5//N4VCoU+WCR9l9x3dFkG3bjciK0s2cig95rlz5wZ37979129/+9urPwEN0QYFK2Z0dnZmU6nUe0zTPClKd5C1HoooR7S8tgjqDMQnG9uKtyALhcKpNWvWvHvt2rUTviuAVCUoWDHksssuO5zNZt9t23ZBFoDXGY6GLvdKOS2COkH2QqFQYlkxGf7mmjVr3ovDHMcTFKyYct111208d+7cJ0SthmG2HAbVIqjqeiOzrAgh0NfXdytmsscXFKwY84Y3vOGhiYmJ+0XZ36KHvVzR8ipWohZBXqBEfQTZQDtrLR48ePC+J5988gf+zxhS7aBgxZxly5bdls/n14g+ZCFyD9nfIBD1EZS1CMq+dsN/qVnU7ebUqVOrurq67gis4EhVgpnuMaepqcns6up67+jo6FbDMBbTTPZUKgW2bU+tR7Piaba7KOtdlQmv26GZfjzCT5Bd1u0mm80e2rRp0wf7+vpwTPaYgxZWHTBv3rwRQshfmKZ5TpQRTi0edlgaGV7ztmRuoK5Y8SMw8CkMlmWNrVix4i/6+vpOBXKykKoGBatOuPLKK3ePj49/2LZtx637jpd0By/pC6x1pfsdQVWLICHE2bBhw/9+8cUXd4Ry0pCqAwWrjnjjG9+4fGxs7BtsoFskWgB66Q5e0xdYK0knMdStRXDnzp3/sGbNmt+GdsKQqgMFq8649tpr/zmTyTzKZ5f7aTkUIWoR5AWLF6lcLicUK1WL4IkTJ35+66233hXWeUKqExSsOiOVSpFFixb9bT6ff4F3tfhfr6LlN9dKFLdS9REcHR3tPXTo0Eew2039ga2EdUhHR0euubn5/83n81saGhouUY2VlUgkpMtYVLlWOomhoq/diCyrbDZ7eM2aNTcNDAxkAz4tSA2AFlad8rrXve6Ebdt/ZprmqCoTXsfSEsWsvPQRZH9VfQQtyxp7+umn/9fAwMBwBU4ZUgWgYNUxV1999c6JiYm/tG3bFiVlunXjcesbKItZqcRKNRDfhg0bPjQ4OPhSpc8bUjlQsOqcN77xjSvOnTv3RVZgZBaXaLQHWY4VL1aiLje6QxwTQuDFF1/87Jo1a56o9PlCKosRZDcMpHbp7e29v62t7ROpVApSqRQkk8mp30QiAYlEApLJZMkY8ayF5dY/kLYG8r9u2exDQ0M//tGPfvSxSp4fpDrAoDsCAABXXHHFp19++eUlTU1Nf8S7elSwHMeBRCJRFHgXuYM6WeyqD5+yfQRHRkbWZTKZv6/UeUGqC7SwkCmGhoa6Tp8+/UI6nb6UtbSolWUYxtQvFS1qBbFuIz+uVS6Xm/rlLSu+2w3dBwDAxMTEy2vXrn3T1q1bz1byvCDVAwoWUsTOnTsXWpa1OZVKzRK5hVS0KLxYyb5045YcKho1dPny5W/auXPnvgqeDqTKQMFCSti2bdsbGhoa1iYSiVbWwuKtK3rv0D6CKsGSiZUoyG7bdnblypV/tGXLls2VPA9I9YGChQjZuHHjTS0tLcsTiUSSihW1rPiAOxUbdphjOsk6NYvcQCZ94b3PPffcrytZf6Q6QcFCpKxdu/YT7e3t9/NfkKbjZfGtg26dm6nlReNcbJCe7mtwcPC25cuX31fhqiNVCgoWomTNmjXfaWlpuY2fr2od5N1C1uIStQZSV3D//v33/OQnP8FRQxEpmNaAKHnrW9/62XXr1l3c2Nj4btVwx3yfQX5iY1yiDs0nT558YsmSJV+oZF2R6gctLMSVkZGR5t7e3udSqdSb+bgVH3DnR2ZgLSrRSKcAAKOjo9tefPHFP1i1alWmwlVFqhwULESL/fv397zyyisbDcO4VPblG77VjxcyfihmAIBMJnPg2WeffVN/f//JClcRqQGwLyGixeLFi091d3f/hWmawzK3j+9iw1pYfF9EAIB8Pj/83HPPvRPFCtEFBQvR5vd+7/de6e7u/kPTNI/wiaJs8iidzw8KyLYs5nK5V5955pkb+/r69la6XkjtgIKFeOKGG27Ydfnll78xm80+zY6mIBrVQTQ0DQDAqVOnfrd+/fo39Pb27q5wdZAaA2NYiG8eeeSR9+Tz+f9jGMY1ovgVP9zy2NjY4O7du7/x29/+dnmly47UJihYSNksX7788oMHD76LEPKHhJCLDMOYZ9s2ZLPZoWw2e3RkZGT1+Pj48kceeQQtKqQs/n8rZo1xDQ75CQAAAABJRU5ErkJggg==',
        pin_notice: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAGeCAYAAAAufzwLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAC0swAAtLMBr763ewAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAACAASURBVHic7Z15nBzVde9P9TL7jGZGEpKQQJsFkhACA36JjZdgPztOQvJ4eI9fjIM3CNhg490OsRMbAwnEkGBkDIjEhhgwtrAxArSM9mU0mwAtILRLSKN9NDPdXV11674/1CXu3Lm3lu6u/Xw/n/r0qLvVfW8tvz7n3HNOKZRSQBCWYrE4jlI6FQAmAMBYABhLKR2nKMpYABhfeq4ZAJoAIFv6b22lxxoAaKSUgqIowwBQLJ1jJ0uva5TSIQAYBIDjlNKjAHCMUnqcUnocAI4DQH8qldrT0NBw3PvZIlFCQcFKHrqupw3DmEkpnasoyjQAmF7appW2ZvO9iqI4/ly7c0n2Ovs89/cgpXQPAOymlO6hlO6mlO4GgK2NjY070+m04XhwSCxAwYo5mqa1UkrnKYoyFwAuAoDLAeBSAGiU/R87karGOeNGvCSPRUrpG5TSbsMwtlBKt6bT6c7m5ub+igeHhBYUrBhBCEkbhnExAFwJAO8CgHcDwPnm626sJa9xao1ZWF+jHkvbXkrpGsMw1gPAmqamplczmQyp8vCRgEDBijCEkFrDMK5UFOU9lNJ3AcA7AaA5SsJkwo6Z/z9W/7YQLvPfpw3DWG8YxjpK6eqmpqa1NTU1RfczQcIAClbE0DRtmqIoHwKA/00p/XMAaAmTQDmh2i4l/zcvXuzflNIcpXSdruvPpVKpZ9va2vZUPBjEN1CwQo5hGBnDMK4CgL8obbMDHpJnVHIu2sS7RjwaxplYvWEYQCndZhjGYkLI821tbSszmYxe9iAQz0HBCiGU0hQh5F0A8DEA+AScSS8IPUGfS6VUCsv4lylWrOvIWGIndF3/IyHk6fb29sUoXuEDBSskcCL1MQCYFPCQzhLFc8Qu7iVyF1kxMwzjOCHk+ZJ4PZ/NZjFwHwJQsAJG1/VpAHA9APw9AEwJcixROxfY8VrF8WTiJQrUizbDMPbrur4wnU4/Mnbs2H0eTAVxCApWABiGkaaUXkUp/SIAXAsAaT+/P47HXDYnXshE4sULWCm2xYsWUEoNQshyXdcfGjt27KKamhrNg6kgFqBg+QghZCal9AsA8FnwMS6VtGNsN19TxJzGukQCRgg5rGnaY6lU6hcTJkzY5cU8kNGgYPkAIeRySuktAPApAMg4+T9s8NgtfhzToM4btykcTsYpCtTzlhe3ssj+29B1/Xld1++cPHnyWleDQ1yDguURpSD6XwHAt+BM5rnX3xeJz/QSN3EsO0zBUhRFuLLICpf5SAjpLhaL90+cOPFxDNJ7AwpWlSGE1FNKrweArwLATCf/p1xrqlrHLu7ngF0cywpRfMv8t4VwvaFp2r3t7e0LGxoaClWaBgIoWFXDMIwawzA+CwD/BADnevU9lR4vPN5ncPojIVth5F1EU6w44Tqiadq9Y8eOvQ+FqzqgYFWIYRhZwzA+BWeEaobd+8uxpqqRAY5UjlUuFytWrHiVhGt/sVi855xzzllQX1+vBjX+OICCVSalGNVHAODHADDL7v1uhSrMAfekIxMukYvIWFtACNmrquodU6ZMeTSbzWIWfRmgYJWBrusfBoB7AWCO3Xv9ECo8htXHDLjbvcd8nyBTXhbfAl3XtxSLxa/NmDHjJT/mEidQsFxACJkFAD+mlH7M7r1eCxUeN3+wy6a3y5pnxcr8mxAChmFAsVhcCgC3Tp06dYsvk4kBKFgOIIS0AsC3KaW3AkCt1XvdCBWKVHSohnCJNkKIpqrqg62trbe3t7cP+DOb6IKCZQGlNGUYxhcopf8CZ26+IAWFKhnwx0KWMsEnnVoJV8lNPFIoFL4/c+bMR7BXvRwULAmEkLcBwEOU0qus3ueVUOFxCTei48N3TRVZW3w8CwBA1/Wz/9Y0bR2l9PMzZszY5s9MogUKFodhGFlK6dcopT+EKrl/KFTxRHasRLWKbPqDLKbFrCZq+Xz+3vPOO+/2hoYGbOfMgILFQAh5O6X0YQC4zOp91RYqPAbRxur4mecKL16itAdOtKBYLL5KCPn8BRdcsNGvuYQdFCw4U04DAD+hlN4MFq1eUKgQK5yeG1ariKxgGYYBuq6TfD5//+TJk7/X0tKS92EaoSbxgkUImUcpfRwA5lu9z4lYoVAhbs4BUQcI1toyhatkbW0jhHx69uzZvV6OP+ykgh5AUBiGoRBCbqGUdoGFWPHxCBlufl2R+KIoiqMWOIqiQCqVOvvIbul0+uxjJpOBdDoNtbW1c2praze++uqrPyCEJPa6TaSFRQiZAAALKaV/YfW+allVSdzHSHkuosw9ZLdCobCsvr7+umnTph30YRqhInGCRQi5llL6CwBol70HhQqpJlbnAVvaYxXTYvK1zPSHY/l8/guXXHLJIh+nEjiJESzDMDKU0h9RSr8JAFKbvRpilZR9ijjH6TkjEy6RpaXrOuRyuYcuuOCCm+vq6hLRXz4RgkUIGQcAv6aUfkD2HrSqED+ws7bMR1GSqUi4dF0HVVVXNTY2fmLatGmH/ZpHUMResEr91J8BgKmy96BVhfiJE9Ey/xaV8bDCpes66LoOmqYdVFX1o5deeukGP+YQFLFebSCEfJFSug4qECu7lT1c+UPcYrWSyD7PryCyq4b8ls1mJ9fV1a3YtGnTLX7NIwhiaWGVymt+Rin9vOw9aFUhYcDpj6EoT0vXdaCUgqZpZiAeCCEwPDy8YO7cuV+ur6+PXZPA2AkWIaQJAJ6klP6l7D1OrCor4rbPkGApR7RE8SxTsHRdh3w+v2TSpEkfmzhxYqxa1sRKsAgh51JKnwOAt8veg2KFhBEnosUWULObmepgipWmaaDrOhSLxVdqamqunjNnzj6/5uE1sREsQsj8klidJ3q9UhcwLvsJCTey84xfQaRMlwe29tAUKyYYf0jTtKsvv/zyHj/n4RWxCLoTQj5EKV0NZYqVk8A6gviBXTDeDNgrinI24G4G4lOpFGQymRFbNpudlMlkVq5fv/5qP+fhFZEXLELIpyilfwSAFtHrlbiAuAKIBIEb0WJXEFkBy2azZ4Usm802NTY2LlqzZs11fs7DCyItWKW0hV8BQEb0eqVihSBBYSVa/MYLFi9epS3d3Ny8cPXq1Tf7PJWqElnBIoTcRCldAJI5oFghUceq64MT0TKtLGZTmpub71+1atXXfJxGVYmkYBFCvkUp/U+Q1ARaiRXGq5AoUS3RYqwtpaWl5Z5Vq1bd6eM0qkbkBIsQcielVLqz7cRKBsarkLDiRrTYYDwbiGetrXQ6Dc3Nzd9auXLlT3ycRlWIlGARQn5CKf2W1XvKFSsECTNORctsCsjGsNiyHk60vr1ixYp7fZxGxURGsAght1NKvy16zcktxct5DUHChF0NIi9a5goiW38oEK2vLl++/Hs+T6VsIiFYpVbGPxS9hsF1JGk4FS02psXGsXj3sKWl5UdLly79hs/TKIvQZ7oTQq6nZ269NeoolStWYZ8zgjjBbmGJ76fFZsCbJTzFYtHMjqenTp268YMf/ODPfZ6GK0JtYRFC/o6eaWeMYoUgHHaWluka8oF4btXQfF4ZM2bMz1588cVP+zwNV4RWsAghV1NKF4JgjChWCHIGu2C86RqaosWKFesilkQrNWbMmMdeeukly5uzBEkoXULDMC4zDGMlADS5/b8oVkjSsDvn+f7wvFvIuoalbVBV1fdeddVVfX7Owwmhs7AIIZMNw3gWUKwQxBFOaw/5lAfWLeQC8c3pdPqP3d3d5/s5DyeESrAIIS2U0ucBYIrodSvzF8UKSTJOaw/ZVAfRZopYbW3tuadPn35+9+7dY3yeiiWhESzDMLIA8BuQ3IW5nAx2FCskSbhJd+CTS0XpDnV1dRft2LHjyaGhIWFzgSAIjWBRShdQSj8oeg3FCkGc4dbSkomWWdbT2Nj456tWrXrA52lICYVglTovXC96DcUKQdzhJN2BvxuPzE1MpVLQ3Nz8xUWLFn3R52kICXyVkBDyp5TSlQBQw7+GYoUg5SO6FviVQ/4mFqVe8CO20sqhNjg4eNXVV1+91u95sARqYRFCJlBKfwMoVghSdUSWlmjlkLWwZO5hOp3O1tfXP93V1TXJ73mwBBZMMwwjAwBPAsBk/jUUq+oStX1jV8yOOEd0LbH7N5U6Y7Ok02mglJ59NPO2MpnM2fytbDY7qb+///GhoaEPNTU1BXLPw8AsLErpv1FK3yd5TfZ/XD2fRNi7qkS1x1cc5hB2nDT/E8W0Ghsbr3rppZcC66MViGARQj5KKRXeUlv264piJSYpF3ZS5ukFTnppiW5mwbdYNl9vbW297emnn77Gxym8NV6/Dz4hZDKl9GUAaB81GJeuYFJP3KTOWwa6kM6QXUOsC2h2dTCD8GbZjqqqoKrq2SC8qqon6+rqLnnve9+73885+GphUUpTAPDfUAWxShJoXViD+8YZsiC8yDUU5WixSaXZbLbt5MmTj+TzeV81xNcvMwzjm5TS9/PPY5BdDF6E7sF95h5RfpZVPItJKv3gokWLbvV1rH4dXELIZZTS9eAihSGpYhX3+fkNuowjceoa8rlZpktouocll1HN5XJ/es011/jS2cEXC8swjAZK6eOAYmUJWgfegPt1JE5dQz5HS3CfQ0ilUrXpdPqJvXv3Nvgxdl8Ei1J6FwDM5p+3a8Qn+JxqDis04AXlD7if38JKtJy4hqyQ1dbWzlm3bt2PfRm31wdQ1/U/AYC1AJB2+n+SsiIYxzlFiaS7im5WDXnXkN1KK4fGwMDAuz/5yU+u93LMnlpYhmHUAMAjIBArt/lWcQJ/6cNB0o+Dm1VDq2RSs71yTU3Nz48cOTIq7FPVMXt5wAghP6SU3j7qSxMctwp6LkuWLIGlS5cGOoZyueuuuzz77CRbW7Jr0SzJMdsqa5oGhJAR1lWhUBiRn3XkyJF/vO66637k1Vg9s7AIIbOp5C7NSYxbJf3XPOzg8RkNf1PWdDoNiqJIu5WmUilob2///jPPPDPXqzF5IliU0hSl9BEAqOVfc+MKxuUEiss8kkASj5Wda8gH4EWdHRiXsVbX9YdzuZwn2uLJhxqGcQMAvIt/3q0rGAfiPLe4ksRj5rTpH19ryLWgMRNK3/nEE08IG3JWStUFixDSRiW3lU+SK4guRrTB4yfunWXVP4sVrvb29h/39fVV/QYWXlhYPwCAcfyTSXIFoz5+5C2SdCzLTSjlb8yqKApks9lzuru7v1/tMVZVsAghcyilN/LPJ8kVjOOckk6SjqkT15BPc2Az4FkRa29v/8pTTz01q5rjq7aFdS8AZPknk+IKRnnsiDVJPra8lcULlygvq/R6TaFQuKeaY6maYBFC/opS+mH++aS4glEeO+KMpBxjKyuLdwn5G7PypTzNzc1/vWDBgg9Va2xVESzDMDKU0n8TvRY3YRIRt/mEGMpswQwgoceat65EN2I13UPzOVPMmpub7x0eHnZcmmdFtQTr70BS3OyUqJ4IERp3oBe6BwQmXklYQbRrq8zf25APwJv/TqVSUF9ff9HChQs/XY1xVSxYpXrBispvonrwIzLuuAmViCTM0Xd40ZKtGMpuxso+397efvvRo0dHxbfdUg3Buh4ApvHPR+RiLpsIzC+JF7GvVlcEzgHP4Et2eLFi41ipVApqampm/s///M91FX9vJf/ZMIw6APge/3zcA+0hHzMFACPoQYQAX4Qr5OdCxTi1smQ9s9jg/NixY/9xz549o8r13FCpYH0JAKbwz8dFmESEeB6mUFEASG7rgdF4LlwhPic8ReQSikTLfKypqTn/2Wef/XxF31nufySENALAd/jn4xxoD+l4WaECQLGS4alwhfTcqAoyK8v828rKYvOySlbW97Zt21Z2O+WyBYtS+jkAmCB4XvReR8+FmRCOlxcqABQrJ3gmXCE8RzzFqqWyqL2yoihQU1Mz6YUXXvhsud9ZlmAZhpEGgK+IJoD4An/RKYBi5RYULRe4XTEUZL2fFa329vbbjh8/XlZeVrmCdS0AzOSfR+vKc9Cqqi78vkRcYhWAF60allYMZzz66KP/p5zvK9cl/Jpo4HEkZGLFxqnQqqocBTxwEUN0zlSVcvOyWCvLfL2trU3YjdgO14Kl6/p7AOBP+efjaF2FaKzs6h+KVHUxRauqqSAhOnc8h2+l7CTNoaGh4X/dc889o5p82lGOhfV10YARTzB//VGovMXct1V1EeMoWiIry3zkS3V4K4sVLEVRoLa29ja33+9KsHRdnwUAV/PPo3XlzRCCHkDCMK/EJFYIVIRMtPjGfqxrqCgKtLa2XvPQQw+9zc13ubWwvsT/nzhaVyhWiYUVraq4iCE4l6qOm5tWiKwtRrRSAwMDrhJJHQtWqcj57/jn42hdBQzuqGBhr0YULQfIgu9sCxreyjK3cePGffbNN990XBTtRrCuAYBzyppRhAj45Ir3mR0dqi5aScCuxlC0WpjJZCY8+uijf+P0O9y4hF908ia0rsoGd1K4qKpoxe0asAq+W+Vl8ZnvpViWY7fQkWDpuj4TAN7vYj6RJMCTKl5nc3zgRQuPkwUyt5AvjuZdw+bm5g89+OCD05x8h1ML6/PALauLAm9Rtq5QrBAJ7Ile0bGKyrXgFFnwXZSTxZfnsKuFAJA6efLk3zv5TlvBIoSkAWBU46247fyAwJ0YDTCm5QBWwKxuc8/3ySoF3/9+YGDAVo9s30ApfR8ATKpsKuEmIPFFsYoWVRGtuP/QW7mFrEvIilXpxqvn3XfffVfafb4Tl/ATTgYaZXcwAHDHRB+0tMBdThZvWbF/l0TLVmssBcswjAwAXFP+dMJPAKKKYhVd+KuzLNGK8w+51WqhSLjYmFZ7e/tHDx8+bNl2xk6wPgBc7lXcgu0+gzsl+vAXQOKPqV0XB5Fw8cF4RVEgk8lMeOCBB95r9V12LuHH+SdQiBBk1Mqh64si7teRXdY77w6aW319vaVbKBUsQkgNoDtY1a/z88sQX8FjyyBzC9l0BtFKYakb6bX79u3LyD5bKliU0vcDQLvd4NAddERodkh9fX1oxuKGtra2oIfAU3E8K07XiZVbKLpLtCw3K5PJjF+wYMGfyb7HyiX8yyrNJZT4eLKE6qw899xzgx5CWYRQsESE6liHAadxLNY1rK2t/QvZ51kJ1ofLGWCcfjXiSEQu/FHU1dUFPQQRogA8XgAleNdQtFrIx7kAAFpbW90JlqZp0ymls+wGhOJkS+h2UFQFK0KWoatjHqdriM90Nx9F5ToiC8t8T319/Zy77757qug7ZBbWX8WxMZ+JTydJaM/EKIpWiGNvogslrGP1Fbv0BtPC4kULAODEiRNCD08mWFKTzIo4/VrEmSgKVoQsLAB0DUdgZWWxPd7ZrbW1VRhDHyVYuq7XAcCf2Q0CxcmSUO+cKApWyKnIyorTteSmT5aVcI0ZM+YDfX19tfznjxIsSul7AKDBo/kETpxOjnKJomDNmDEj6CGUQ6JPNrt8LFFxNPO+xl/96lejbgMmcgnfW078CoXgLKHfEW1tbaEfYwTBWJYAU4DYv2UBePY1SinU1NS8h/88kWDZtnhAcZISiR3T3m6bDxwqImpdmUTinPAau3wsgYUFzc3No7RohGCVujO8w7dZIIEQRZcwIpRtZcXJCLArhmafk7mFAADNzc1/un///hHdG0YIFiHkUgBocjvAqOxsj8cZjZ0A0ROsiFtYABE6N6qNqAupzMLixSydTrfcd999F7Ofx7uEtve6j4o4IdZMmhTrJrJBgrEsAbKcLFFMi7XEDMMY4RbygvXuOCeMekjkTsj6+vqgh+CYc889N2r7F0WLwc7K4kWLtbRaWlosBeudno8eCQVRSsSMkrhaYCtYcfJeym3qx8e6+MD7WcEqFovjAWCK24HFaScniZAWEwuJqPuKVpYEmUvIW1gAAHV1def/7Gc/G2v+m7WwLrH7oiiLk4djj+ROiZKbFRMLCyCi50o1sFoplCSOnn3fli1b5pufwwrWfEASQ1REIGormhxoZYE8hmUVfGf/TzabHS1YiqKMWD5EHBHZk2/ixIlBD8EREResROO2rpB/zdwaGxvPatNZwaKUzne7QhhlFzHJUErRwgqORF40rFiZj3abSWNj40gLixCSURRlrq8zQAInCsHsGAgWuoUl7MRKlEAKANDQ0DBv9+7daYC3BOsCALBcNkJrahSR3yFRsLKSVqgd9+vMKujOvwZwZn+k0+n6++67byZASbAopbMDGT0SKFEoeYlaobZD4q1KDpAJl2ijlEI+n58D8FYMa3pQA0cQK6KUL2YBuoUSnMSxFEWBmpqaaQAlwVIUZZrbL4q76WpDLCY/Y8aM0M8jShn5yGisUhpEz4leBwCoq6ubDoAWFhJiYmJdyQj9j0U1EWUgWIkXpXTEa3V1ddMA3hKsaVZflnBrKraEPYYVM+sKuwqU4NMW2Od4ETOFq76+foSFJbwHGIIgnpI4S8Bp1jsvXLW1tWcES1XVc6CMpn0JJlYnWZitrDCPDakMmYUleo1SCqlUqvmOO+4Ym4KSdeU2yx2JBzGPE4WdWP34OUVkZYnew287d+6clgKACW6/EGNa8SHMcaKZM2fiiRYjZLWF5t+yGJaJYRjnpCil4/wZLhJG6urqUBSCJfb7385744VLRCnjfVwKAMZafSBaU/EmzBZWFGodkfIQxavsnkun02NTiqLEsvbBI2Kn3mGOYUWh1rEKxO6cskJmHFm5iyY1NTVjUwAw3ouBIdEgrBYWWlfxx8qiEpHNZs+4hF4PDAk3YbSyEmJdASTMwrLCysqilEI2mx2bAgAMuiecMFpZpZ7zeDHHEDdBeJaamppxKQAYA4DB9SQTRguLGROemAgAAGQymdYUANS6+U8obPEjjBYW12k0LiedyHRQID7z8wRTcxRFqUkBQE2ww0H8hje5w9jVU9C4L3RjRMrDjdHDvjeVStW6trCQ+BHGvukSEUXRijGmOMkEzbSwsj6OCQkhIRWsoIfgFVi0W0IkUFZ/K4pyxsKSKRrGq+IL6xaGTRzCuAiA+ItEe9AlRM4QJtEqpTSIQOskgoisJtGjzCU0X0ulUugSImcIk2AlwMJC4WWwcgNFQXfceQklrG5hGNMskMoRWU7s3/wmIkUp1TwdJRIJwiRYEvCHNSZYWVHsc/xmGEYxBQAoWEiocrFmzpwZ9BD8INECLBMpKwzD0FIAUJT5kEj8Md3CkFtYib6444SdGyjbSu/X0MJCQFGUULVz4W7wimIVM9wKlfl/DMMopiilQ+yT7COSHBLUziWMxFaUnWqKhVV19m9N0wZTADDg5AOReBMWK4u7tVdsL+QkYiVIDtxB0HV94KxgIUjIrKwkiFUS5ijFiUixj7qun05RSlGwEAAIR/5TKcs9sRdyEu4PamVhGYYx4pF9n6Zpp1IAMIDuIAIQjgzzMIzBZ+KvUCXsXEFWpESWlmlh9bMfmgSFR8RY1PD5OYagh4B4gFU6Ay9UIuGilEI+nz88SrBEX4AkgzDEsMIwBqQ6iDREZmWJXEHe0ioUCv0pADgsMr8QIbE2P8OwShjyBFYviPU5BSDvyOAyaRRyudyhFAAc5j8YSSZhsG4SKFiJwM4lZDdZDEvTtMMpADgUxASQcMLlQfkKilW84YWIFSj+b0lQ/nAqm83upWcIeDregosJ4SfpghWnc9QqfmUVdLdIa6BXXXXVvlRtbW2OUnqE/UAkuQRpYSUwpSERiGJXvGUlsrDY9xeLxUNf//rXC6nSZ+5GoXJMfH4GQ0YCUxoSdS7JYle8cImssFwutxsA4KxgBTgPJETMnDkzsF+u+vp6/NWMGU5WBZ2IVj6ff0uwDMPYxX44ggRBAi2sRCATKdnGu4UAAENDQ28JFqX0NadiFeXAYJTH7hcYwwqGOJ2bVlpiJ1qiwDsAwMDAwHaAt1zCbfwHIsklKOFImIUVH4WywI0baBXDGh4e3gpQEqyamprtSUhtqCKxPtkSJhyID1hZVoQQO9EyLr300tcASoJVV1c3BAD7A50REhqCsLCCdEURb5ClMohWCVnR4q2rQqGw++67784DvOUSgmEYW2XuIFpeyQItLM8ZYaHHOX5lF3DnrStR0ujg4ODZkFWG+eDNAPBhn+YVGIqiVEuAFQAwIIbuYVtbG50xY4av87r88svxVzGGWAmWKVZ2ojUwMLDZ/DxWsHrZDwWIl/J7RCx30BVXXAFXXHEFCghSEaIcLKt0Bt4lNP/v8ePHe8zPTDGf3wMcrHChWyhEAQDcMYgbEuEOuhUrWQwLAGBoaGi0YDU1Nb1BKR3g3ywi6js56uNHkChgldJACBG6hLw7qGnaqYULF+41P/OsYKXTaUop3cxaVXhhOwKtLMQpibygrPKuZGJlPg4ODvZOmzbt7PXFuoRgGMZG8wvinkBaRTE2Pyi+OwvxhDgbBE5WB9lNZmEdP358A/u5Ke5L1rC+J/vI/42MIL5nHoI4oJL4lVWWe39//xr2e0YIViaTWcNmvKNAOQatLMSO2AbbRYiSQ3nLio9lse8tYTQ0NKxjP3eEYDU1NZ2glAoTSPkdHIcdXuU5oGghSAk7l9CqJMd8HBoaevW3v/3tKfZzU4IvWiN4Tvg3MoLoKzjiFbE+N3h9EJXklBO/Onbs2Gr+u0YJlmEYq5MQdDdBKwvxmzh4J1bwVhWf3c67g+bGW2RHjhxZy392hn8ilUqt4RUz7ju4irApDhRC/Mt68uRJOHnyZNDDcEyEi6NDew54iVU5jihuJYphDQwMjPL2RgnWmDFj9p4+fXo/pfQ8mVjFTcSqnMkfibysrq4uZenSpUEPwzF33XVX6PepgFEXSZyuGwD7YmcnlpWoYV+hUNizatWqUR1kRrmEpS8VpjfEMfDuAexOieJFhiBlIYpdyawrUfyK3U6cODEqfgUgESzDMNbwA4g7VRZfdr/Gf+chImJvXYmQiZWoQ4MspaEUvxrlDgJIBAsAOuwCPVEIiwAAIABJREFU70kQsQpBSws5SxzFyok7SCkVWlZ2KQ39/f0rRd8pFKy2trZthuBOOnEXKY9WDE3ivfMQlvipkw122e1ORQsAoFAo7F6zZs1rou+RWVhgGMYL5gewllYcfylYULSQCkmkKwggFiuZUFmlMxw+fPg52XdYCdZiURIYP8CkHIwyUQBFC4khVu4g7+KZwqXruq1LCACwd+/e52XfKxWsxsbGZQCQN7/UbDcTd7cQwHMrCwBFK64k0roSZbo7cQVFq4SGYeSnTp0qjF8BWAhWXV1dnlK60srCQhwj2s+4M+NFIsXKxK5mULY6yL4fAODo0aNLH3300bzse6SCBQCg6/pifjC8GxhXEfPgZENLK74kR5lA7g7y7p3IsmLdQlEO1sGDBxdbfbelYKVSqT+K0hv4Acf1l8QD1xBFKyHE9ZrgEbmDMpeQFyuRaB07dkwavwKwEay2tradlNLX7KyouFpZAChaiC2JcgVF1zovVLz7xwfb2dVBNtg+PDz86urVq/eO+gIGS8ECANB1/Xl+UHEWKB+Qnc24U6NHosRKRDXcQSZZ1NK6AnAgWJTS550IVZwPlAdzk+13FK3oEN8T3iFWyaKsSJmbnTu4Z8+eygWrtbV1BaX0qF1dYdytLhQthEF4MsT5RxvAmTsocgllva9YsdI07eg111wzqv8Vj61gZTIZnRCyyPwCdpBJA0ULgYSKFY+obpB18VjLirew+JQGAICDBw8+feutt+p232srWAAAhJCnMH51Bo/SHUQ7Ndk7OpwkVqysPCunpTimaIm6M+zatespJ+NwJFitra0dAHCEHWCS8WDlMC15jQIKV1hIrFjJ4LsyiOJXbBxLlCxKKYVisXj4+uuvF7aT4XEkWNlslmia9lu72sIk4ZF7KNuhyd3R4QDFioF3B+0sLFas+GA7wBl38DOf+Qxx8t2OBKs0yCd5vxOpOjJLCwCtraBIvFhZBdtFgXa72BWf3b5r164nnY7FsWC1t7evAoA3+dXCpFtZPgbiTZK7w/0n8WIlQmRd8blXrDto1V1UVdUD3/nOd9Y7/W7HgpVOpw1d13/LD1Y2oSThkWhZfShaW94iq0hInFiJ6gbNR1mDPj52JUsYBQA4cODAbz784Q87dtkcC1ZpkE85EaykHVQAz1YP7T4URav6SPd5Es9rEValODLLis29YkVr165dv3bz3a4Eq729fS2l9KAo6JY0q0qER6LlxEXEnV8dUKwssMq9srKwZPGrQqGwd8GCBZ1uxuBKsEpu4ZOi1UL+gCZVwDw6sc3jZLVTUbgqQ+oCJlWs3GS2i4LtIiuL7X21f//+Jy+66CJX56wrwSrxGG9ViTqRJvUgA3gqWmbqg51wIc7BeJVDZPErWe6VVSkOpRTeeOONx9yOwbVgjR8//hXDMLr4AbAT4ieYRDz6ZTaTTK1ytgDQ2nIKuoASnAbbZWU4vJXFx64GBgbWr1+/fpvbcZVjYYGu64+KYlhJP8giPNonbGwLhcs9lgsaeB6Lsctql7mD5vtY4+aNN95YWM4YyhKsurq6JyilOd415IPvSblphR0ei5ZTNxHFy4FQoVg5u0GqXTqD+chbYyWLbDiXyzlOFmUpS7BaWloGdF1f5CT4jpzBw4uBdxPtRCmJwmWbIoLnrTVug+2i1UEz2H7o0KFnli5derqccZQlWAAAhJBH2QnwEzNBK2skHl4YpnCZ3R+cCFfccSRUKFZy7KwrkVVl5Q5SSmH79u2PljuesgVr/Pjxyw3D2MWn5yP2eHyRpGC0xZW0A4NCVSZW1StWriBvWcmC7YVCYfdPf/rTVeWOr2zBSqfTlBDy36xQoVvoDo8vGtZVdGp1RR0Uqipj5QrauYNsFrypEbt3737k8ssvL/s8LFuwAAAURXmMUmrwbiC6he7wQbhMqyuuwoVCVQWsUhlEomWKk6ZptnWDpUeya9eu/6pkjBUJ1vjx4/cSQpbzE0JxKg8fLirWXYwa/EmlAAqV57hxBUVJo6xoHTt2bElnZ+eBSsZT8Ymr6/ovZEKFiaTl4cNF5qSwOmwo4FCkAFCo3OIklUHW80r0b77WEADgtdde+0Wl46xYsMaOHfs7wzCEBdEsePK4x7zocN8BgEOBxf1VPcqJX4luNEEpBVVV97373e/+faVjqliwampqNE3Tfi4TLLSyqgNeiHJQ2CvDyroS1QzyIsXHsPj/AwCwc+fOBbfffrvtXXHsyFT6AQAA2Wz2IUrp9w3DqDED7OyjCQbfK4fdn5XsyyuuuAJmzpxZjSG5HURVVAXFyVtE7qBT64pPZTAMo3DgwIGHqzGuqgjW2LFj+/v7+5+uqan5tKi2kBcuTH2oDpW09Glra4O2traqDKMaH+Loi/CcqTpuy3BkVhVrXfHu4MGDB59cu3bt0WqMt2qrRYZh/CcbfBfFsRBvYV2jqF/c/FyiPp+owcetWMFixcr8W2Rdme7gK6+88kC1xlU1wZo0adIGQsgmWTG0+chaXYi3iC76MAlAmMeWBOysK94dFK0I2uVfnTp1asOGDRs2VWvMVc3H0TTtAb6+EIUpvNgJmtcbEk54d5BSKnQF2b9FsSsAgNdff71q1hVAlQWrra3t14ZhHOGyW0e5iGhlIUiwOFkZlFlWTjLbKaWgadrR1tbW31Rz3FUVrPr6elXTtEdEqQ34i4og4ccu2G4KlMi64nu279q16+ePP/54oZrjq3qJRjqdXkAp1VkrC0B+a3u0shDEX5yuDIpSGDRNE64QCiwsfevWrT+v9tirLljnnHPOPk3TfsO6hOYjm4eFFheChAvRyiDrDmqaJrS2+JVBSim8+eabT/b19VVUNyjCkyJYSumdtKRMVlnvzPu9GAaCIBwyL0fmCprun2lVmX+z6QyiNjJ9fX33eDF+TwRr0qRJm3VdXy6ysgBGpzggCBIM7DUpy7uSleGIxIpSCkePHn1x48aNvV6M17M2I7qu/6soiRStLAQJBqtrj7euRELFi5XIJQQAePXVV//Vqzl4JliTJ09+kRDSy5ubshQHBEH8xWmRsyh2JSvDOX369OZ77rlnuVdj9rSRW7FYvIcXKivQykIQb3BqXfErg1axK4l19ZNKWiDb4algTZgw4UlCyF47KwtBEH+xsq5EZTeiZFF+ZTCXy+2+8sorn/Fy3FXp1iAjm83qmqb9NJ1O/7thGNL2MuzzmGSKINXFrXXFpjGI8q5ETfoAAF5//fV7fve731Xc88oKz3t7Nzc3/4IQcpy1qNDKQpDgKMe6MjdZV4ZisXj85MmTj3k9ds8Fq6WlZVjTtAf5YkpeqKrVmA5BkLcox7piBUqU1S7Ku9q5c+f9K1asGPZ6Pr7cPaW2tvY/DcPIi9pXoJWFIP5RDeuKt7B0XR/esWPHz/wYvy+CNW7cuP5isfiQXdAdrSwEqR6imkHzsZrW1d69ex/s7u4+5secfLs/XU1NzV1oZSFIsFhZVyKBsrOuCCGFV1555V6/xu+bYI0fP/6QqqqP8HEsXvXRykKQyim3I4OVhSWyrvbs2fOz3t7eQ37Ny9c7ANfW1t5BKc1jPhaCBEO1Ylel67bw8ssve1LkLMNXwRo/fvyhYrG4UNSRFACtLASpBlbWlVVjPjexK0op7Nmz5+d9fX1v+jk3XwULACCdTt9JKVXdxLBQtBDEGXbXkSlaojvhOC3DMa2rzZs3e1bkLMN3wZo4ceJ+VVUfc2NlIQhSHk6tK1n8Snbr+T179vyit7f3oN/z8V2wAACy2ewdhmEU+TgWWlkIUj5urStR6xi7msHSNav29fXd7ePUzhKIYE2YMGFfsVj8L1n2O1pZCFI5VquConpBUQxLVDO4d+/eR7xof+yEQAQLACCbzf7IMIyiaKdalRMgCDIaq2uGt5CsrCtWrFhrjNm0oKwrgAAFy7Sy+BYVJmhlIUj5iPKuRHfBEQXZ+X7t7LW5b9++h3t7e/cGNa/ABAsAIJvN/sAwjDy7U+ya/aGVhSAjcVvgbBW7Mh9F1hUhJN/b23uHz9MbQaCCNWHChDeLxeKDvFBhLAtBysdJVrtV7ErWTXTXrl33BxW7MglUsAAA6uvrf2IYxmmRaKGVhSDWuLGuRLErq0A7G2zXNG1g+/btgcWuTAIXrHHjxh1TVfVePpZlZ2WhaCFJR3QNlGtZWVlYAAA7duy4u6ur64Tfc+QJXLAAAFpbW+8xDOMIv6LhNEcLQRDrQLtV7Eq2Mmh+TrFYPHrgwIH/CHp+ACERrJaWliFVVe+0Eym0shDkDHZJonZpDHY5V6x1tXXr1n9eu3btoF9zsyIUggUAMG7cuAcJIfv4FUPs6oAg9jgpweFFqlgs2tYM5vP5valU6hdBz88kNILV2NhYUFX1n9mdzsexANDKQhC7JFGZO2jlCsryrl555ZXbn3nmGdXP+VkRGsECAJg8efJjuq5vE1lYAIBWFoIIsItdidzAYrE4Ku+Kj18NDw+/NmvWrCeCnh+Lp/cldEs2myWqqv5TOp1+KpVKgWGcuZche99C/t8AeC9DJDk4SWNgXTtRoF3mCvI1g319fd956aWXPL3PoFtCZWEBAJx//vm/0TRtHSFEGICXWVloeSFxx2kag6jPFWtZyW6MysaLT506teF73/veogCmaUnoBCudTlNd179uGAbl41kYfEeQt+CvCdbCsitwNjdRCQ4A0M7Ozq+/733vC92FFjrBAgCYNm3aek3TfiuqMbQChQyJK07SGOxax/DWlSyN4c0333xy7dq1a/2amxtCKVgAAKlU6puGYahWaQ4IklSsXEEz0O60ZpB1Bw3DKG7atOn7Qc9PRmgFa/LkybsKhcKDbjPfUciQuGGXxmDXjcFpoigAwK5du+7r6enZ6ef83BBawQIAaGpq+mdCyHFRyQ6A9YFEkDhgF2i3S2NgVwStxKpU4Hxy27ZtdwYwTceEWrDGjRt3UlXVO0S9stAtRJIIv1puZV2xuVZOWh9v2bLlB2EocLYi1IIFADBx4sT/1HX9DTPNgRUvALSykPjiJOfKrhtDsVi0DbZTSiGXy+2sr69f4PMUXRN6waqvry8WCoXvilYMMQCPJAlRoN3pTSXMTSRWAAC9vb3feOKJJ4oBT9GW0AsWAMDMmTOf1nV9jVvXEIUMiSpu6gX5nCvWqrITK0opnDx5cn0Yk0RFREKwAAB0Xf+GYRhUtMNRtJA44TbQbnUzCatOoqVryNiwYcOtYUwSFREZwZo+ffoGVVV/aVUYjSBxRBRo5+sFRa1j7NrHAAAcOHDgsQ0bNnQGOT83REawAADq6+u/SQg57TaZFMUMiQqVBNpNkRKlMvAlOJRS0HV9sLu7O7RJoiIiJVgTJkzoV1X1DnPFkC/YxAA8EjfsAu0i989JRjsAwNatW3/Q29t7KOApuiJSggUAMHHixH/Xdf11kVChlYVEmUoC7W7TGIaHh3cAwAP+zrByIidYDQ0NxWKxeBub+Y4BeCTqVBJol60KygLtAABdXV23hKmTqFMiJ1gAADNnznyuWCw+j51JkbhiJVaVBtr7+/ufXb58+eKAp1gWkRQsAABFUW5huzmIag1FoJAhYcNJ65hyAu0iV9AwjOLGjRu/4eP0qkpkBev8889/o1Ao3M+W7KBriEQNO1dQFmiX5VzZBdp37Njxr93d3Tv8nme1iKxgAQC0t7f/CyHkTVGdIbqFSJRhxcoq0M5bWFaB9kKhcPDAgQOh7sZgR6QFq7W1dVBWZ4hWFhJ2Ks1oF3VjYEtw+NhVT0/Pt1atWjXk9zyrSaQFCwBg+vTp/61p2kpRyQ6AdQAeRQsJCqdixXcR5VcFnQbaT5w4sfq73/1uqG7ZVQ6RF6x0Ok0Nw7jZMAxNZEKja4hECZFl5aSLqFVGO6VUX79+/U1RqRe0IvKCBQAwffr0V1VVvd8qLwutLCQsOA20u7Gu+EC7GdcFANixY8e/dXZ2vuL3PL0gFoIFANDe3v5DXdcPiNIcAMBWuBAkKKziVmz5Dev62bWPMT+rUCjs279//4+DnmO1iI1glQLwXxVZWeyyrugO0ShiiF+4yblib3YqEirWujJXEPnYVWdn51eiHmhniY1gAQDMmjXrN6qq/tE8aPyvjVVMC0UL8RorV1DkBjp1Bc2N9yqOHj36wrJly571e55eEivBAgBIpVK3GIZR4Mt2WNdQURQUKCRwrFYF2Zwr3qri3UFRkighJL927dqbgp5jtYmdYE2dOnVnPp+/k7+jLW9hoWuI+Em55TesVWVV3MyXpW3fvv3Hvb29u/yan1/ETrAAACZPnnynruuvmSslVhsPihZSbcotvxGJliznij3Xh4eHdxBC/i2AqXpOLAWroaFBVVX1y7yVxVta6BoiQcAmNfN9rnjrit9k1hX7WRs3brwxiq1jnBBLwQIAuOCCC5aoqvo/fACeT3NA1xDxEqv8P3Ozu1WXm/KbN99881crV65c5ucc/SS2ggUA0NjY+BVCyDGrm7Cia4h4RTnlN6IUBpF1xacwUEqhWCwe7+rqui2AqfpGrAVr4sSJx3K53LfME4JfNUTXEPET3hUUld+IujBYxa1Y66q7u/trPT09R4Kco9fEWrAAAN72trctLBaLy2T9gbBsB/ECJ64gnyAqy7nik0T52CwAwPHjx1d85zvf+aWfcwyC2AtWOp2mAHAjIUSYm8V3duBB0ULc4tQVFLU8FmWym21kZI35CCH51atXfyEOxc12xF6wAACmT5++o1Ao/Ig90KLVFXQNES+wcwX5gLqsZlDmJWzbtu2Hvb29bwQ5R79IhGABAJx//vl36breJ2vwh64hUg2cuoLVCrQPDg6+MmbMmHt9nmZgZIIegF/U1dXpmqZ9KZ1Or1MUJc2nM6RSqRFWFv+6LAUCQUzcuIKm+IjuKegk0F76PGPdunVfWrdunRbAdAMhMRYWAMCsWbM6C4XCg3YJpQBoVSGV43RVkBcpWa0gX36za9eu+9etW7c+yDn6TaIECwBg3Lhx39V1/QBftsPmZslAEUNklOMKiuJWoptKiKyrQqGwb+fOnbf7PM3ASZxgtbW1DaqqeqMoAI+rhkg5eOUKmtYVG7syP2/jxo03rF27djCA6QZK4gQLAODCCy98rlAo/Kpc1xBFCzGxOj/cJIjyrqBVJ4aDBw8+FtU7N1dKIgULAKClpeVWQshhq7IdBCkXJ66gqCmfLI3B/CxVVQ/39vbGuvzGisQK1oQJE47n8/kvsScEuoaIG7x0BfnEUvOc7OzsvKmrq+tEANMNBYkVLACA2bNn/75QKDzNnxjoGiJ2lOMKioRK5grycSsAgEOHDv166dKlv/V1oiEj0YIFANDU1PQPuq4f4WNZ6Boi5cBaV2zMyi5B1M4VLBaLxzZt2nRL0PMLmsQL1qRJk47l8/mvigLwfLATrSwEoDr3FbRzBfnzr7Oz88tx78TghMQLFgDAnDlznigUCr8TiRYAoGghZ3HiCrLWlVV/ditX0LSuAACOHDnyhyVLlvza14mGFBSsEvX19TcRQk7KUh1QlBArZK6gVQqDE1dQ07SB9evX3xj0/MICClaJKVOmHMrlcreJAvDoGiIAzlxBNhlZZl1ZJYjy51t3d/etvb29BwOYbihBwWKYM2fOwmKx+Ec+BsGekHyXBxYUrfjiJm4lC7KrqiqMX/FVF4wr+NzixYsf83mqoQYFi6Ouru4Luq6f4PtlsWJl1bUBRSt+2B1Tq5YxohQG/q7NklXB4+vXr/+CT1OMDChYHCXX8MuyWkNMeUAA5LeYlyWIWsWtRH2uOjs7b+rt7T0c9DzDBgqWgLlz5z6hqqo0oZTfeFDI4oObbHbzXHGawiCrFezv7//dkiVLnvR7rlEABUtCU1PTPxBC+p2sGqJoxRO7FAbeFeTvH2i1Ksi7g+Z5VSwWj27cuPEGv+caFVCwJEyaNOlYLpf7opNVQyRZyIqaZQmiZrDdQQdR2Lhx4w2YICoHBcuCuXPn/r5QKPzSrg0Nuobxw+mqoKj0RuQKmptVreDBgwcXJr1W0A4ULBva29u/rOv6fpGlBQAjAvAoWvHAiVjx2eyyG0iIbiQhCrIXCoWDfX19iW0b4xQULBvGjx8/UCgUPkcIoXxwlV2GxlSHeOA0hYHPZrezrnh3kIuH0rVr135+06ZNJ32aZmRBwXLA3Llzl+Tz+YesCqQx1SG+WBU1m6Jl5QbK7itonjN79+792apVq14IeJqRAAXLIRMnTrxN07TXnaQ5oGsYTZymMPAWlswVtMu3AgAYHh7evm/fvm/6PdeogoLlkNbW1mFN0/6WEKKJfikx1SHaOOnC4CSFwaUrqC9fvvy6ZcuW5fyeb1RBwXLB7Nmzu3O53B2ytspOUh1QtMKH1TERJYiyK32y0huRK8iHD7Zu3fqDrq6uTr/mGQdQsFwyffr0HxWLxQ2i0h0nqQ5INLBLYZDlWlnFrczzBADg5MmT6+bNm3dnwNOMHIm5VX21qK+v19Pp9KcJIX2KojQrigKKopw9EdPpNBiGAalU6qxg4W3vw4ubfCs+OZSPVbGiZZXNruv68OrVqz/b3d1NAphypEELqwxmzJixa3h4+OuiX0+Ra4jxrHBSTumNmxQGWTZ7d3f3l7u7u3f4Pd84gIJVJvPmzXtIVdU/yMRKFIhHooEbV9BKrETZ7EeOHHn2+eefXxjwFCMLClYFNDU1fU7X9cNsEF60aoipDuHDbRcGUZ2gqqrSFAbRKnKxWDyyfv36LwUw3diAglUBkydPPprP579ICKH8Lyqfb4OiFR7cxq2serPzcSs2dsVns69Zs+azvb29/QFMOTagYFXIvHnz/lAoFB5gS3WsWtGIQNHyDyfHgq8TlHVhcJPCsGvXrp+uXLlysR9zjDMoWFVgypQpXy8Wi318raHIRURxCh8iy0qUIMqnL/BtY2QpDIODg6+cOHHiuwFPMxagYFWB5uZm1TCMT+u6nmN/XfnVIQB0DYPETemN05YxvBsoSmFYvnz5x5977rlCAFOOHShYVeLCCy/cOjw8/FW+BENUIA2AqQ5+U07pjZMbSbA5V7IUhp6enu1+zzeuoGBVkYsvvvihQqHwa/7kFRVJy0DRqj7llt6ILCs+o13WPRQA4PDhw09jCkN1QcGqMu3t7TcSQvawq0R8TAtA7hoi/iBrGcOLld09BS0a8u3r7u7GFIYqg4JVZSZMmHCqUCh8wjAMjbey2FVEAIxn+YGbfCvRLbrYwLrT0ptSF4ZPYkO+6oOC5QEXXXRR5/Dw8I9EBdIYz/IPt3Er0apgOQ35tmzZcvvGjRvX+zrZhICC5RFz5sz5kaqqy0VL3RjP8p5y4lZuUxhEpTfHjx9fefHFF9/t1zyTBnZr8Ih0Om3U1tZ+Rtf1zYqijFUUBVKpFBBCRnRqSKXe+s3ADg7eYpXJzqcwyGJWNqU3x1atWvW3fX192IXBI9DC8pBp06YdzOVy1xFCKFuugflZ3uIm34rNZHdS1GxVerNu3brP9fX1vRnAlBMDCpbHXHzxxX/M5XIPyuJYGM+qLpXkW7HWlUjArEpvdu/e/R8dHR2/93WyCQQFywfOO++824rF4ma++h/jWdWlmnErUQdRWenN0NDQq/39/d/2a55JBgXLB1paWgqGYfytrHTHab0hipYcq33mNG5l5Q6y/4f9sSGE5Do6Oj7+wgsv5H2eciJBwfKJ2bNnbx0eHr6NTzTkA7cAmFRaLezqBK36svOrgvxxMz+zp6fnlq6urm1BzzUpoGD5yPz58xeoqvokL1R8WxoADMK7wS7IzlqxvGUli1uxme7m+/m4VX9//zPPPffcw37PN8mgYPlMW1vbjcVicR+fx8O7hlagaL2Fk33Fxw55N5CNW/HZ7LK4VT6f39Pd3f15P+aIvAUKls9MnDjxZLFY/BghpCjKhDcvMIxn2eM0bmXlCoryrXjLik9hMAxD6+jo+NvOzs5TPk858aBgBcC8efM6h4aGviVKQOQzpzGe5Q6ndYIikTLdQpt8K+jr6/sqlt4EAwpWQFxyySU/LRQKvxG5HHysBONZo3Eat3JaIyhyBUVxq8OHDz/17LPPPuD3fJEzoGAFyPjx46/XNG27VZE0nw3Pk0TRsksO5eNWdrfosloRZONWw8PDO15++eUv+DpZZAQoWAEyfvz4QULIx3Vdz4ssLL4VjYwkiZaT5FDeuhK5gnyCqF2dICGksHTp0k+sW7futI/TRThQsAJmzpw5rwwPD9/CrxqK2pYkPQjvJsguWxG0y2aXxa26urr+oaenp9fnKSMcKFgh4JJLLvlFLpf7L5FQOY1nJRVZkN1NnSDfMoZ1zQEADhw48Ci2Og4HKFgh4bzzzrtRVdXNsv5ZTsp34ixkbpJDebHic6xkdYJs3Mr8vqGhoVd37979Zb/ni4hBwQoJY8aMyQPAx3VdPy0q3REF35MiWuUE2a1cQT52JevLruv60IsvvvjxZcuW5fyeMyIGBStEzJ49+/Xh4eEv8haWLJ4lI06i5SbILiq94XOsTEtLViPI/jh0dnbe8PLLL2OdYIhAwQoZl1566ZO5XG6BlWg5SSqNg2jZBdmtkkNFCaFuWsbs2bPnP1588cXH/ZwvYg8KVgiZOnXqraqqdrPWgiwbPmlBeFmAXVbQLIpb2fVlHxgY2DQ4OPiNgKeKCMCe7iGkpaVF7e/v/4iu6z2KorQDwIh+8IqigGEYkE6nR4gV3xOeUhrZPvFO41Z21pVdJjvvCmqadnLFihWf6OnpUf2eM2IPWlghZdasWXvNfvAi15BNKjWJSxDebXKoXX8rF/cTpOvXr7++p6dnt4/TRVyAghViLr300udyudy/i5r+CS426edESbScJodald3YpTDIAu2vv/76XcuWLVvk85QRF6BghZwLLrjg28VicTV7cZZKRUa0ogGIfhDeTZDdqnOoVZ2grKj5xIkTHeecc84/+jlfxD0Ywwo5dXV1WkOkQZtqAAAJHElEQVRDw8dUVe1SFGWKrusAAJBOp8/Gp/hH/u8oI+vAYFpJVjeRsIpbsSuChUJh/5o1az7R09OjBzxdxAa0sCLAtGnT+lVV/ZimaSobw+JLeABGXuA8Ybay7DLZZR0Y7NxAu3wrQoi6ZMmSj/T09BwNYNqIS1CwIsL8+fM3DA0N3cpesLIgfNQy4e3GaRVkd9I91CLIDj09PTd3dXVt8nO+SPmgYEWIyy67bEEul3tYllTKujlRCcLbjdNJkF2Uyc5bV6Jqgb179y7Am0hECxSsiDFt2rSbC4XCJjaGY9X4L8xBeCcrgrJMdjdFzSKxOnXq1IbTp0/f6ud8kcrBoHvEaGlpUWtqaj6i63qXoijnmImkAAD83yyiIHyQiaVOVwStMtntkkNZUWetT1VV+zs6Oj7W19eHyaERAy2sCDJr1qz9uVzuk7qu67xryFol/MUfduzaxbCuoGlZsZuTXCtKqb5ixYpP9PX1HQh6voh7ULAiytvf/vaO4eHhb7NL/HadHcIShC+nXQy7IugmyM67yH19fbetW7dupd9zRqoDClaEefvb335PLpf77yi1o6mk7EYWt5L1tuIXIg4dOvTEokWL7vdrrkj1QcGKOOeff/4Nqqr28pYWv4UhCO+07EZ0AwknlpVoRdD8zsHBwZffeOMNvONNxMGge8RpbW3N9/f3X6tpWpeiKGMBIJRBeLe9rWRN+Pj0BZF1xbeM0TTt5OLFi6/dsmULdg6NOGhhxYALL7xwTy6X+9SZGDwRukROg/BeWFpOxMqq7MYudmUTZDdWr1796S1btuys+sQQ30HBigmXXXbZkqGhoX/mO2iWI1p+IBMrO+vKyrISZbJv3br19pUrVy4ObKJIVUHBihHz58//l1wu9yR/8YpiOn6tHDpZEeSTQ0V3u3ETtzKD7P39/b+96aab7qjaZJDAQcGKEZlMhk6dOvXvi8VitywT3s/yHacrgnZlNyIrS9Y51PzO06dP923btu0z73vf+8KfgIY4BgUrZrS2tuYzmcxHNE07Ikp3kK0eiqhEtNyuCDppxCfrbcVbkMVi8VhHR8e1K1asGC57AkgoQcGKIRdeeOHefD5/LSGkKAvAO2lHY77ulkpWBJ0E2YvF4ijLisnw1zo6Oj6KbY7jCQpWTLnsssvWnj59+gbRqqGXK4fVWhG0Kr2RWVaUUuju7r4ZM9njCwpWjLniiisWDg8PPyjK/hZd7JWKlluxEq0I8gIlqhFkA+2stbh79+77n3vuuYfK32NI2EHBijlz5sy5RVXVDtGNLETuIftYDUQ1grIVQdndbvg7NYvKbo4dO7asra3ttqoNHAklmOkec+rq6rS2traPDgwMdCqKMtPMZM9kMkAIOfs+MyvezHYXZb1bZcI7LWg2bx5RTpBdVnaTz+f3rFu37pPd3d3Ykz3moIWVAKZMmXKCUvrXmqadFmWEmxYP25ZGhtu8LZkb6FSs+A4MfAqDruuDixcv/uvu7u5jVdlZSKhBwUoI8+bN2zY0NHQdIcSwK99xk+7gJn2Bta6c3kfQakWQUmqsWbPm/23evPlVT3YaEjpQsBLEO97xjkWDg4M/ZAPdItECcJbu4DZ9gbWSnCSG2q0Ibtmy5fsdHR2/92yHIaEDBSthXHrppf+Sy+We4LPLy1k5FCFaEeQFixepQqEgFCurFcHDhw8/dfPNN9/p1X5CwgkKVsLIZDJ0xowZn1NVdQPvavGPbkWr3FwrUdzKqkZwYGCga8+ePZ/FspvkgauECaSlpaVQX1//f1VV3VhTU3O+Va+sVColfY3FKtfKSWKo6G43Issqn8/v7ejouLq3tzdf5d2CRAC0sBLK2972tsOEkL/UNG3AKhPeiaUlilm5qRFkH61qBHVdH3zhhRf+pre3tz+AXYaEABSsBDN//vwtw8PDnyKEEFFSpl0Zj11toCxmZSVWVo341qxZ8+m+vr6Xg95vSHCgYCWcd7zjHYtPnz79LVZgZBaXqNuDLMeKFytRyY3TFseUUti8efPXOjo6/hD0/kKCRalmGQYSXbq6uh5samq6IZPJQCaTgXQ6ffYxlUpBKpWCdDo9qkc8a2HZ1Qeaq4H8o102+4EDBx55+OGHPx/k/kHCAQbdEQAAuOiii76yffv2WXV1dR/gXT1TsAzDgFQqNSLwLnIHnWSxW934lK0RPHHixMpcLvcPQe0XJFyghYWc5cCBA23Hjx/fkM1mL2AtLdPKUhTl7KMpWqYVxLqNfF+rQqFw9pG3rPiyG/MzAACGh4e3r1ix4p2dnZ2ngtwvSHhAwUJGsGXLlum6rq/PZDITRG6hKVomvFjJ7nRjlxwq6hq6aNGid27ZsuWNAHcHEjJQsJBRbNq06YqampoVqVSqkbWweOvKPHfMGkErwZKJlSjITgjJv/jiix/YuHHj+iD3AxI+ULAQIWvXrr26oaFhUSqVSptiZVpWfMDdFBu2zbG5yYqaRW4gk77w0aVLl/4uyPkj4QQFC5GyYsWKG5qbmx/k7yBt9sviVwftiptNy8uMc7FBevOz+vr6blm0aNH9AU8dCSkoWIglHR0dP21oaLiFf95qdZB3C1mLS7QaaLqCO3fuvPeXv/wldg1FpGBaA2LJe97znq+tXLnyvNra2mut2h3zNYP8xsa4RAXNR44c+cOsWbO+GeRckfCDFhZiy4kTJ+q7urqWZjKZd/FxKz7gzndmYC0qUadTAICBgYFNmzdv/rNly5blAp4qEnJQsBBH7Ny5c9zrr7++VlGUC2R3vuFX/Xgh41sxAwDkcrldS5YseWdPT8+RgKeIRACsJUQcMXPmzGPt7e1/rWlav8zt40tsWAuLr0UEAFBVtX/p0qUfRrFCnIKChTjmT/7kT15vb29/v6Zp+/lEUTZ51HyebwrIriwWCoV9L7300lXd3d07gp4XEh1QsBBXXHnllVvnzp37jnw+/wLbTUHU1UHUmgYA4NixY39ctWrVFV1dXdsCng4SMTCGhZTN448//hFVVf9RUZRLRPErvt3y4OBg37Zt2374+9//flHQY0eiCQoWUjGLFi2au3v37msope+nlJ6rKMoUQgjk8/kD+Xz+4IkTJ5YPDQ0tevzxx9GiQiri/wMlEo9w+LdTUAAAAABJRU5ErkJggg==',
        pin_locate: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAGeCAYAAAAufzwLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAC0swAAtLMBr763ewAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAACAASURBVHic7J15eBTXlfbf6kUr2iUkJIGEBAIBAmyMw2aW2HEMGOPYBi+ZLJPYsZ2Mk0mcTNbPE89kMpnMJONkkthOxsaTOLHBGNsYQ4JtwqKNRUiYTSAkIdC+7+qlqu73ByqlVKqqrpZq6eX+nqeehu7SrXu7q98+59xzz2UIIaBQxHg8nlRCSA6AdAApAFIIIakMw6QASBt9Lg7ANADO0T9LGn2MABBLCAHDMEMAPKP3WM/o615CyCCAAQBdhJAOAJ2EkC5CSBeALgBtNpvtakxMTJfxo6UEEwwVrPCDZVk7z/P5hJAFDMPkApg9euSOHnHCuQzDaG7X172k9Lr4ecm/BwghVwHUE0KuEkLqCSH1AC7ExsbW2u12XnPnKCEBFawQx+v1JhJCFjEMswDAQgDLACwFEKv0N75ESo97xh/xUnj0EEKuEEIqeJ4/Twi5YLfbT8TFxbVNuXOUgIUKVgjBcZyd5/kiAKsBrAKwBsAs4XV/rCWj0WqNqVhfEx5HjwZCSDHP82UAiqdNm3bO4XBwOnefYhFUsIIYjuMieZ5fzTDMbYSQVQBWAogLJmESEPdZ+jdq/1cRLuH//TzPl/E8X0oIOTZt2rSSiIgIj/8joQQCVLCCDK/Xm8swzJ0A7iCEfBJAfCAJlBb0diml/5aKl/jfhJBhQkgpy7L7bDbbO0lJSVen3BmKaVDBCnB4nnfwPL8BwMbRY77FXTKMqdyLPuJd4x55/kasnud5EEIu8jx/gOO4/UlJSUccDgc76U5QDIcKVgBCCLFxHLcKwDYAD+JGekHAY/W9NJpKoRr/EsRK7DqKLLFulmXf4zjujeTk5ANUvAIPKlgBgkSktgGYYXGXxgjGe8RX3EvOXRSLGc/zXRzH7R8Vr/1Op5MG7gMAKlgWw7JsLoAvAPh7ANlW9iXY7gVxf9XieEriJReolzt4nr/OsuwOu93+UkpKyjUDhkLRCBUsC+B53k4I2UAI+RKA+wDYzbx+KH7mSmOSCpmceEkFbDS2JRUtEEJ4juMOsSz725SUlLcjIiK8BgyFogIVLBPhOC6fEPIYgM/DxLhUuH3GvsYriJjWWJecgHEc1+r1el+x2Wy/S09PrzNiHJSJUMEyAY7jlhFCvgbgYQAOLX8jDh77ixmfqVX3jb8pHFr6KReol1pekplF8f95lmX3syz7k6ysrBK/OkfxGypYBjEaRN8M4Nu4kXlu9PWCok0j8SeO5QtBsBiGkZ1ZFAuX8MhxXIXH4/llRkbGH2mQ3hioYOkMx3HRhJAvAPg6gHwtfzNZa0qvzy7U7wFfcSw15OJbwv9VhOuK1+v9eXJy8o6YmBiXTsOggAqWbvA8H8Hz/OcB/DOATKOuM9XPi37eN9D6I6E0wyh1EQWxkghXu9fr/XlKSsovqHDpAxWsKcLzvJPn+YdxQ6jyfJ0/GWtKjwxwytRRy+USi5VYvEaF67rH4/nZ9OnTX4iOjnZb1f9QgArWJBmNUd0P4N8AzPV1vr9CFcgB93BHSbjkXESRtQWO4xrcbvePs7OzX3Y6nTSLfhJQwZoELMveBeDnAAp9nWuGUNHPUH+EgLuvc4TzZDLlleJbYFn2vMfj+UZeXt5BM8YSSlDB8gOO4+YC+DdCyDZf5xotVPRzMwdf2fS+subFYiX8m+M48DwPj8fzAYB/zMnJOW/KYEIAKlga4DguEcB3CCH/CCBS7Vx/hIqKVPCgh3DJHRzHed1u9/OJiYnPJCcn95kzmuCFCpYKhBAbz/OPEUL+FTc2X1CEClV4IP0slFImpEmnasI16ia2u1yuH+Tn579Ea9UrQwVLAY7j5gD4LSFkg9p5RgkV/VwCG7nPR1o1Vc7aksazAIBl2bH/e73eUkLIo3l5eRfNGUlwQQVLAs/zTkLINwghz0In948KVWii9FnJrVUUpz8oxbREs4nekZGRn8+cOfOZmJgYWs5ZBBUsERzH3UQI+V8AN6udp7dQ0c8guFH7/IR7RSpecmkPEtGCx+M5x3HcowUFBcfNGkugQwULN5bTAPh3Qsg/QKXUCxUqihpa7w21WUSxYPE8D5ZluZGRkV9mZWV9Pz4+fsSEYQQ0YS9YHMctIoT8EcBitfO0iBUVKoo/94BcBQixtSUI16i1dZHjuE/Pnz+/0sj+Bzo2qztgFTzPMxzHfY0QcgoqYiWNRyjhz68rJXRhGEZTCRyGYWCz2cYexYfdbh97dDgcsNvtiIyMLIyMjDx+7ty5H3IcF7bf27C0sDiOSwewgxCyUe08vayqcHyPKZNzEZXcQ/Hhcrk+jI6O/lxubm6TCcMIKMJOsDiOu48Q8jsAyUrnUKGi6InafSBe2qMW0xLlawnpD50jIyOPLVmy5G0Th2I5YSNYPM87CCE/IoT8EwBFm10PsQqX95SiHa33jJJwyVlaLMtieHj4twUFBf8QFRUVFvXlw0KwOI5LBfA6IeR2pXOoVUUxA1/WlvAol2QqJ1wsy8Ltdh+NjY19MDc3t9WscVhFyAvWaD31NwHkKJ1DrSqKmWgRLeHfcst4xMLFsixYloXX621yu90PLF26tNyMMVhFSM82cBz3JUJIKaYgVr5m9ujMH8Vf1GYSxc9LZxDFs4bSw+l0ZkVFRR0+efLk18wahxWEpIU1urzmN4SQR5XOoVYVJRDQ+mMol6fFsiwIIfB6vUIgHhzHYWho6IUFCxY8FR0dHXJFAkNOsDiOmwZgJyFkk9I5WqwqNULtPaNYy2RESy6eJQgWy7IYGRl5f8aMGdsyMjJCqmRNSAkWx3GZhJB9AG5SOoeKFSUQ0SJa4gXU4kNIdRDEyuv1gmVZeDyesxEREXcXFhZeM2scRhMygsVx3OJRsZop9/pUXcBQeZ8ogY3SfSadQSSiKg/itYeCWImC8S1er/fuZcuWnTZzHEYREoLFcdydhJA3AMTLvU6tqqnT1taG9957b2z921Sx2WzYvHkz0tPTdWkvlPBHtJSsLPHh8XgGh4eHH165cuU+M8dhBJq2TQ9kOI57mBDyeyiMZSpiRYXqbwwPD+Py5cu6CtaGDaq1EcMWpXtWeF48k2iz3Zjot9vtE84FbtzDTqdzWmxs7NvFxcVfXLNmzf8Z2HXDCeq0htG0hVdBxYoSYqilPUgPIeVB7nA4HMJhj4uL23Hs2LF/MHkouhK0gsVx3FcIIS9AYQxUrCjBjlrVBy2i5XQ6xYIFh8PBxMXF/fLo0aPfMHEYuhKUgsVx3LcJIb+CwppANbHSkghKoQQKeomWyNpi4uPjf3b06NGfmDgM3Qg6weI47ieEEMU325dYKUEz1imBij+ixTDMOMGy2WxwOBzjrC273Y64uLhvHzly5N9NHIYuBJVgcRz374SQb6udM1mxolACGa2iJRQFFMewxMt6JKL1ncOHD//cxGFMmaARLI7jniGEfEfuNS1bik/mNQolkPC1BlEqWsIaRPH6QxnR+vqhQ4e+b/JQJk1QCNZoKeNn5V6jwXVKuKFVtMQxLXEcS+oexsfH/+iDDz74lsnDmBQBL1gcx32BEPLfcq9NVqxovIoS7GhNe5ATLSEYLw7IJyQk/Mf777//uMnD8JuAFiyO4z5DbpQznvDpTEWsKJRQwJdoCa6hNBAvmTUUnmcSEhJ+85e//OXTJg/DLwJWsDiOu5sQsgMyfaRiRaHcwFcwXnANBdESi5XYRRwVLVtCQsIrBw8eVN2cxUoCUrB4nr+ZEPIaFDY1pWJFoagjCJlSTEsagBdZWo5p06bt/Otf/7rU4iHIEnCCxXFcFs/z7wCY5u/fUrGihCO+qpcqpTyIRUsSiI+z2+3vVVRUzDJzHFoIKMHiOC6eELIfQLbc62rmLxUrSjjjTxBeSHWQOwQRi4yMzOzv799fX1+fYPJQVAkYweJ53glgNxR2YZ5MBjsVK0o44U+6gzS5VC7dISoqamFNTc3OwcHBgKnqEjCCRQh5gRDyCbnXqFhRKNrw19JSEi1hWU9sbOwnjx49+muTh6FIQAjWaOWFL8i9RsWKQvEPLekO0t14lNxEm82GuLi4L7399ttfMnkYslguWBzHrSCEyK5nomJFoUwOLWsPxaIliJNSXCspKelX+/btW23iEGSxVLA4jksnhOwGECF9jYoVhTI15ERLbuZQbGEpuYd2u90ZHR39xqlTp2aYPQ4xlgkWz/MOADsBZElfo2KlL+Ia4JM9IiMjfS4y9weGYRAZGSl7LYp+KImWXCxLXNVBSCYVW1lOp3NGW1vbH60Mwtt/+MMfWnJhnud/Tgh50J+/oWLlG73eC47j0N/fj66uLly/fh3nz5/HtWv67hblcDjGtqXieR5Op3OsRrkUPcWSoq3CidwPid1un33u3LmYwsLC983opxRLds3hOO6B0V1uJqBkXVGxkkeP8Y+MjKCrqwvt7e1oaWlBW1sbent70dPTA4/Ho0MvfRMREYGkpCQkJiYiPT0dM2bMQHp6OlJSUhAVFTXhfCpg/qH2neJ5fmzbMOnehm63Gx6PBx6PB263G16vF16vl3R1dd23bdu2t80eh+mCxXFcFiHkIwDJEzrjpysYrmI11XH39/ejtbUVDQ0NaGpqQmtrK/r6+nTbEUcvRkufICMjA1lZWcjJyUFGRgbi48fv5kbFSxtK3yHhEPY4FLYMGxWnMbESxMvr9cLtdvdERUUtWbt27XUzx2CqL0oIsQH4PXQQq3BiquPneR5NTU2or69HXV0dmpqa0N/fr1PvjIPjOPT09KCnpwcXL14EAMTHxyMrKwt5eXmYPXs2srKyxrmRVLyUkfuOid8vcRCeEAK73Q6e5+FwOMZt1jrqvif19PS8NDIycld0dLRpv3SmWlgcx32HEDKhjjQNssszlTESQnD16lVcvnwZly9fRktLCziO07F31mO32zFjxgwUFBSgoKAAubm542bBKBPxVSNOvJO04BqKrSzxo9frRUdHx9MPP/ywaWWWTRMsjuNuJoSUwY8UhnAVq6mMr62tDWfPnsW5c+fQ0tKiY68CnxkzZmDRokUoKioat6M0Fa/xaHUNBbdwdPfocYIlime5h4eHV9x7771VZvTdFMHieT6G5/kKAPMndICK1RiTHZvL5cLly5dRUVGBK1eugGVZnXsWXDgcDsydOxdLlixBYWHhWNCeCtffUBMtwe2TBuDFYiUIltvtxsjIyMXFixffkpOTM2x0v00RLI7j/ocQMmHHWRpkv8Fkx9XZ2YnKykqcOXMGHR0dOvcqNEhLS8OSJUuwdOlSpKWlAaDCJeBLtOSsLEG0XC7XmFvo8XjQ1dX13MMPP/x1o/tsuGCxLPsxACVQKMYnR7iI1WTH1NLSgtLSUpw7dw7Dw4b/qIUE0dHRKCoqwsqVK5GZmQmACpc/s4ZKVpYonsX39fWteeihh8qM7LOhgsXzfATP86cBLJxw4TB2BSc7lsbGRhQXF+PcuXPwer069yo8cDgcKCoqwpo1a5CdfaPsWjgLl1bXUBAswQ2UHl6vF4ODg2fXrFlzy/Tp0w1L3jNUsDiOe5YQ8syEi1Kx8ovW1lYcPXoUp0+fDqn3wkoYhsFNN92EdevWISMjg4qWzHOCYPE8PyZWHMeNEyqXyzVu1rC9vf3/fe5zn/uRUX01TLA4jptPCKkCEKn1b0LZFZzMOHp7e1FcXIwTJ07A7XYb0CtKZGQkbr31VqxZswaJiYlhKVy+XEOlDHipYI0+uj0ez83333//BSP6aohgEUJsHMcdA7BqwgX9sK7CVaxYlkVZWRmOHTuGvr4+g3pFEZOQkIDbbrsNK1euhNPptLo7puNPAF44BLGSuob9/f1lW7ZsWRMTE6N7QqkhgsVx3JcJIROqFIajK+jvGOrq6nDgwAHdFxpTtDFr1ixs3LgR+fn5VnfFdKT3qnitoTQAL04mdblcE1zD1tbWxx599NH/1buPugsWx3FJhJDLAFK1/k0oWlf+9n9wcBAffPABysoMnWShaGTlypW44447EBcXZ3VXTENLAJ5l2bHZQpZlx4RKbG15vV6MjIy05+XlFSxdulRXF0H38jKEkP8AsEH6vFJsgIoVcOHCBfzxj3/E5cuXDeoRxV8aGxtx/vx5JCQkjMuaD2XUCv6JEce3BDETP44uAYu9cuWK4+abb9a1DI2uFhbHcYWEkDMAxgUBwskV9KfvLpcLBw8eRElJiYE9okyV1atX46677kJkpOb5o6BGzjWUS3MQDrFbKCSUjrqLHq/Xu2j79u01evVN72oNP4dErAD/vsThIlbXrl3D22+/jaamJgN7RNGDkpISNDQ04N5778WsWQG3t6jhSMsqi6s6CNUcxAfHccLrEQMDAz8DcI9ufdFLIDiO20wI2TfhAmEyK+hP34uLi/GXv/zFtOJ4FH1wOp3YuHEj1qxZY3VXDEfJyhKnOYiD7x6PBy6XCyMjI+My4D0eD9ra2j75xBNPHNSjX7rUdOd53kEI+S+510JNmOTQOh6Xy4U33ngD7777LhWrIMTr9WLv3r3YuXMnXC6X1d0xFal1JbcRq7QWvFAvPi4u7udDQ0Oal+apoZdgfQYKlRi0EqwiprXfHR0dePnll3Hq1CmDe2Qcem9CEaxUVFTgpZdeQnt7u9VdMQxf24RJ9zaU7roj3josOjp64Y4dOz6tS7+mKhSj6wUvAcgd13AYuIJa+11bW4tdu3aht7fX4B7pg8PhQFJSEjIyMjB9+nRkZmaSwcFB7N27l9GrCKDdbsc999xD4uLi0NzczLS1taG1tRXd3d1BU2gwMTER27dvx5w5c6zuimEouYZC9VFp8F0agBdcw+Hh4dpPfOIThWlpaVNaBDvloDvP81+ARKyA4BUhrWgdX0VFBd56662AXqzscDiQnp6O2bNnIycnh2RmZiIhIWFcxndTU5OunykhBDNnzkRWVhYWLlxIgBsuV19fH5qbm9HQ0MDU1dWhra0tYAWst7cXO3bswKc+9SnccsstVnfHVGw2GwghY8F3juPGAu6ivQzHtg+LiIjIf+211z731a9+dUrJpFMSLJ7nowB8X/p8qFtXWvt89OhRvPfeewb3ZnJERUUhNzcX8+fPJ3l5eT5zjYyIuUnbdDqdSE1NRWpqKhYvXkyAGxVU6+vrcenSJebq1asBV07H6/Vi165dGBwcxPr1663uju5Iv8tyNeDFM4bSvQwFwbLZbEhJSfl/V69e/UNubu6kF8ZOVbAeB5AtfT5UhEkOreM4cOAADh8+bGxn/CQyMhJ5eXmYP38+mTNnDlJTNS9GsIz09HSkp6djxYoVpKenB1euXMH58+eZ+vr6gAp879+/H8PDw9i0aZPVXTENOStLGohnWXbM8oqIiJj1zjvvPPq1r31twrI9rUxasDiOiwXwXenzalVEpQSbiGnpLyEEe/fuRWlpqQk90sb06dNRVFSExYsXk4yMDKu7M2mSkpKwfPlyLF++nHR0dODMmTM4e/Ys09raanXXAACHDx+Gx+PB1q1bg3pSQYqSlUUImWBlyc0csiwrtrK+f/HixR2FhYWTMpUnLViEkC8CmOBHhKorqLW/u3fvDpiZwLlz5+JjH/sYmTdvHiIiJuz9EdSkpaXhjjvuwLp160h1dTWOHz/O1NTollA9aUpLS+HxeLB9+3aru2I40u3uxaIlFTC73Q6GYRARETHjz3/+8+cLCwt/M5lrTkqweJ63A/iq3ACCTYj0gud5vPnmm5aLFcMwKCgowOrVq8m8efMs7YsZOJ1OFBUVoaioiNTV1aG4uJi5cOGCpfehcA888MAD4/ZMDGaUrCwl0RLHscTixXEckpOTn+7q6noxJSXF79mUyQrWfQAm1N8IZ+tq7969lotVYWEh1q5dS/Ly8izth1Xk5eUhLy+P1NXV4ejRo4yw+aoVnDp1Cg6HA/fdd59lfTADaUKpWJykbqFoxjDv5Zdf3vqtb31rj7/Xm6xL+A25jgebEGlBy5gOHDhgaVmY7OxsbNiwgSxatMiyPgQSgnBVV1fjww8/ZKyqLVZeXo6oqKiQCcT7a2WJk0nFz9lsNiQlJX0bgN+C5be9yrLsbQBWSJ8PRetKS1+PHj1q2WzgtGnTcM8995AnnniCipUM8+fPx+OPP07uvfdeMm3aNEv6cPjwYRw5csSSa5uFeLmOWgBeHN+KiYm59Wc/+9mEisS+mIyF9U25DgeTEOlFRUWFZXlWS5YswcaNG0lSUpIl1w8WHA4HVq5ciQULFpCDBw8yVrjt7733HuLi4nDzzTebfm29kbOypLOFSlaWWLAYhkFkZOTTAPyaTvfLwmJZdi6Au6XPh6N1VVtbiz17/LZop0xycjIeeeQR8sgjj1Cx8oOEhARs27aNfOYznyHJycmmX3/37t24cuWK6dc1A7FrKBd8Fz8KzzMMg8TExHt/+9vf+rWuyV+X8HHp34RSvomAL7Hq7OzEzp07Td8SfvHixXjyySfJkiVLTL1uKLFo0SI8+eSTZOnSpaZel2VZ7Ny5E11dXaZe1wiUKpPKBd/lrC2RaNn6+voe9efamgWL5/kIAJ+RPh+K1pUabrcbO3fuNHU3G6fTiXvuuYd8+tOfJvHx8aZdN1SJj4/Hww8/TD71qU8RM3fI6evrw2uvvRZyW7aJxUosWOISNFIrSzhSU1M/39zcrPlD8Eew7gUwfVIjCiJ8CevevXtN3dEmNTUVjz32GFm9erVp1wwXVqxYgccee4ykpaWZds1r167hnXfeMe16ZqKW4iBNKBWVpEl/+eWXNVck9ccl/JKWk0LZuiotLTU112revHl44oknSE5OjmnXDDdycnLw5JNPkgULFph2zVOnTgXU0q3JIHULlcooS/OypJnvo7EszW6hJsFiWTYfwMf9GE9QoiasjY2Nps4Irl69Gp/97GdJOG0zZRWxsbH4u7/7O2Jm6eN9+/bh+vXrpl3PDJTcQrFVJecaxsXF3fn888/narmGVgvrUQDjJFVp+x8tzwUiav10uVx48803TQuyb968mdxzzz3E4dB7jxCKEna7HVu2bCGbNm0y5YZlWRZ79uwJqIoT/qIUfJfLyRJbWmIRG23D1tPT8/darulTsDiOswP4nPT5YBEiPTh48CCam5sNv47NZsM999xD1q5da/i1/EXY5kkvhD3sAo1169bhgQceIGasAWxqasLBg7rszRAQSGtlybmF0n+Lgu9/39fX5/NN9/kTTghZB2DG1IYS2Kh9cS5cuGBKvMHhcGDbtm2mT7drJSoqCtnZ2bqJls1mQ1RUlC5t6c3y5cvhdDrJG2+8wRhtVZeUlGDOnDkwM4ZmJGpuodglFIsVwzBwOp0zf/GLX6x+5plnjqm1r8XneFBLR4PZHVRiaGgI7777ruHjsNvteOihh0hRUZGh15kKmZmZeOqpp4L7A/WDpUuXwmazkddff123OvZyCPXTcnJyEBsba9h1jEJulYuv2UK5PK1R0XoQgKpgqZpgPM87ANw75VEFMGpidPDgQXR3dxt6fZvNhu3btwe0WIUrixcvxvbt2w13D7u7u0PGNVSbLZQTLnFMKzk5+YHW1lbV7cB8CdbtkORehVqwXYmrV6/i+PHjhl9ny5YtAesGUm5YWvfdd5/hN3N5eTmuXr1q9GUMQS7FQe5QWmsoiJbD4Uj/9a9/rRrA9fXTMaFsYrALkRZYlsXevXsNH+vmzZvJqlV+L1inmMzy5cth9Oyh4BqavdzLKHxlvUvdQeGIjo5WDUEpChbHcREIU3ewvLwcTU1Nhl571apVCMTZQIo869atM3yL+sbGRpSXlxt6DTNQcgvF6QxyM4UMwyA5Ofm+a9euKcbWFQWLEPJxAD6XtYeaO9jX12d4/aK5c+fi7rvvDt43KUzZtGkTKSwsNPQahw8fNnWdql6ouYVyu0Qr5WY5HI60F154Yb3SddRcwtAok6iAkqgePXoU/f39hl03JSUFDz30ELHbVWOLlADEbrdj27ZtxMjt0fr7+3HsmOpEWVChNY4ldg0jIyM3KrWnJlh3TaaDwWxdtbe348SJE4a1HxERgYceesiy6peUqRMbG4sHH3yQREZGGnaN8vJytLe3G9a+WfgqoSy2rATBAoDExET/BMvr9c4mhMz11aFgFic5Dh06ZMgOxwJ33XUXmTVrlmHtU8xh1qxZhgbhPR4PDh06ZFTzhiHNdBce5ZbryFlYwjnR0dGFP/3pT2VX/CtZWJtDsTCfgJzQNjU14cyZM4Zds6ioCLRETOiwYsUKGFlIsaqqyvCJH6Pxld4gWFhS0QKA7u5uWQ9PSbAUTTI1gtniOnr0qK5r5cQkJSVh69atwfvmUGS5++67DStTzfM8iouLDWnbbNSsLHGNd/GRmJgoG0OfIFgsy0YBWO+rE8EsTlLa2tpw7tw5w9rftGkTLRMTgsTHx2PLli2GfRHOnDmDtrY2o5o3BH/qZKkJV0JCwu1VVVUTAoUTBIsQchuAGIPGYzlyQnvs2DHDEvYWL16MxYsXG9I2xXoWLlxo2G44LMsGtZXlKx9LbnG06LzYV199dUJWtZxLuHYy8atgtbi6u7sNs65iY2NpvlUYsGnTJmLUwuWPPvrI8PWsRiIIkPjfSgF48WuEEERERNwmbU9OsHxGhoNVnOSoqKjAyMiIIW3fcccdJCEhwZC2KYFDXFwcPvGJTxjypRgZGUFFRYURTZuKr3wsGQsLcXFxE7RonGCNVmdYbtooLMbtdqOystKQtjMzM7F8edi8lWHPrbfeiqysLEPaPn36dFDttONrMbT4OSW3EADi4uJWXL9+fVyG9TjB4jhuKQC/sxqDxeKS9vPSpUuG7RP3iU98wtQtpCjWYrfbceeddxIj0oG6urpw6dIl3ds1A7kqpEoWllTM7HZ7/C9+8YtxdZekLqHP0gHBIk5aOHnypCHtzps3L2QqSFK0M3/+fMybN8+Qts3crckIlHKy5GJaYkuM5/lxbqFUsNaEcsKomPb2dtTW1urert1u+2lbaQAAIABJREFUx8c//vHQUXWKX2zYsMGQgn81NTVBu1zHl5UlFS2xpRUfH68qWCsN732AcPbsWRhR+jY/Px+5ubm6t0sJDnJzcw2xsjiOw/nz53Vv1ygmW9RPGuuSBt7HBMvj8aQByPa3Y8HoIvI8j48++siQtteuXRt8bwhFV9atW2fIPVBVVWXYagwzUXIJpRYWAERFRc36zW9+kyL8X2xh+VwYFYziJCDu+7Vr19Da2qr7NfLz8zF3rs8145QQZ/bs2cjPz9e93ZaWFjQ2NurerhmozRQqJI6OnXf+/PmxzGuxYIVNOrZRMy4rV64MSkUnhEzpoExkxYoVhrwxFy9eNKJZQ1GKYakF38V/43Q6JwoWwzBhsW0Lz/OGCFZqairmz5+ve7tGoafgUPGaSGFhIdLS0nRvt7q6OmjcQn/XFUpfE47Y2NgxbRoTLELIYn9nCIPxBm1paTHEHVyyZAmCJe/KyM8tGO8JI3A6nVi6dKnub0Zra6spu5DrjVishEdfh0BsbOx4C4vjOAfDMGGROFRbW6v77GBERIQhN2ewQkXrBkuXLoXelUk5jkNdXZ2ubZqFL7GSSyAFgJiYmEX19fV24G+CVQBAdd/wULkJjci9ysvLw/Tp032fGEbQONeNMMHs2bN1b7e+vl73Ns1CLegufQ24cR/Z7fboX/ziF/nAqGARQoIn+DIFBgYGDKniWFhYGA7fSCJzaP/jMBWtRYsW6T7w69evY3BwUO9mTUVJuOQOQghGRkYKgb/FsPT/GQhAWlpaMDAwoGubUVFRoZzK4Euc/BKucBStuXPnIiZG3/Jy/f39QRnHkkNLHIthGEREROQCo4LFMEyuvxcKxpuvoaFB9zZzc3ORkpLi+8TgYZxIaUxn0Cxc4eYiJiYmGrLy4dq1a7q3aQRqKQ1yz8m9DgBRUVGzAUDYYTUsLCwjku5CzB0cEylNJxMCl8uFnp4e9PT0MCMjI/B6vQBuzJJFR0cjKSkJycnJiIqKmvC34bJudd68eeTChQu6Dvb69et6Nmc4gmsnfU5JvIT7QyRYucDfBCtX7WKh8Ivocrl0r4/tcDgMyWi2CM0fcl9fHy5fvszU1taiqamJ6evrGxMqKU6nEwkJCcjKykJ+fj6ZN28ehKKG4SJaeXl5sNvtus5Ot7a2wuVyTfghCAakaQvi56QiJtwj0dHR4yws2T3AQonOzk7dtwCfPn26IcmBVqLy40Q6OjpQXl5uO3v2LKM1Fuj1etHZ2YnOzk6cOXOGiYuLQ1FREVauXEnS0tLCQrTS0tKQnp6ua9ypv78fnZ2dyM72e/mvpWjNepcKV2Rk5A3Bcrvd0xmGCfmtiNvb23XPEM7Ly9O1vQCFuN1uHD582FZeXs64XK4pNTYwMIDS0lKcPn2aWblyJdavX08iIyNDWrQYhkFeXp6ugsVxHNrb24NOsASULCy51wghsNlscT/+8Y9TbBi1rkL5hgFuzBDqTW5ubvD7yuqQhoYG5sUXX7QdPnx4ymIlxuVy4a9//SteeOEFxojJkEAjJydH93vFiHvaDOSsLLlzpEdtbW2uDUC6vxcMxpiWEfEro2p4BwpVVVXMyy+/bGtpaTHs16ylpQUvvfQSc/r0aaMuERBkZmbC4XD4PtEPjFhiZiRKawuFfyvFsAR4np9uI4SkmtNd6+A4Dr29vbq2mZSUhPj4eF3bDCRKS0uZXbt22Twej+HX8ng82LVrF1NSUmL4tawiMTEReu8S3dvba0gRSr3x5b1JhUuO0Yz3VBuAFLUGg9GakjI0NISenh5d28zIyND9FzNQqKqqYvbt28eY+dkTQvDuu+8yVVVVpl3TTBwOB9LT03V9Q/v6+jA0NKRnk6YhF6/y9Zzdbk+xMQyTbGI/LWFgYAB6Wwrp6X570kFBY2Mj9uzZY6pYCRBCsHv3biZYi9T5Qu97xuVy6b5yw2iUjCM1d1EgIiIixQYgtOblZejv79e9zaysrOA3PSV4PB7s3r3bppRTZQZerxdvvPEGY4YrajZGzOgZcW+bhZpFJYfT6Ux1AAiodSWtra267sTscDhQU1OjW3vAjTe1v78fDQ0NU06V4Hke0dHRyMzM1Kl3k+fQoUOM3pMTk6G1tRWHDh3CXXfdZXVX0NzcjJGREUx1JxybzYb+/n7ZjO+pUFNTg2nTpoFlWd3ajI6ORkZGhm7taUXNyiKEwOl0pjgABFTQff/+/bh8+bJu7TEMY0iFxr179zLA1GN8hBBkZWXhqaeestRi6+rqYkpLSwMmt6WkpIS59dZb+eTkZEv79OabbzJNTU1TTvsxKm2opKQEZWVluopgQUEBvvCFL+jWnhR/gvBiIiIiUh0AEoDAWSbBcVzAl4AlhOg6OxMIMz0lJSUB5YZ5PB4cO3aM2bp1KwFg2Y3JcVxAL9jW+14EAuN+lMPhcCTaAPhVEtHoDy4QRNNsrB5zf38/zpw5Y2kf5Pjoo4+EJUCWqYXVn40VBNqYBc1hGCbCBiDC2u5QzEZ6Q1ZXVwfk9Pjg4CCqq6uFzgamiUPxG3+MHvG5Npst0m8LixJ6XLlyJbB+UkVIJkyoaIUwgjgpCZpgYQXHVi8UQ3C73YaUjdaLxsZGXdcwUgIHOYFS+zfDMDcsLCVFC9RAI2XqCG5hd3e37mV39KSvr0/3VQqUwEdBe6hLGO50d3frmsOjNyzLoru7W3BZA9Z1pSgjZzXJPSq5hMJrNpuNuoThjp5JukYRDH2kTB41N1Au6E5/tcIUhmF0X2NpBFYuFaLoh5zlJP63lr0sbYQQejdQggH6wxoiqFlR4uekB8/zHhsAKlhhTERE4KfhOZ00ahFqKImUGjzPe20APEo+JCX00XuTTyOIjo62ugsUnfDlBqrtgUkI8VILK8xJTk4O6EKEDocDyckhX7ItrPBXqIS/4XneYyOEDIqfFD9SQp+kpKSxfQIDkYSEBN1LC1PMQ6umqFhVY//2er0DNgB9WhqkhCZRUVEBvZlGdnZ2UG4WSpmImiBpcAfBsmzfmGBRwpe5c+cG7K9VIPeNMjW0iJT4kWXZfhshhApWmFNYWIjY2FiruzGB2NhYzJ8/3+puUHRGzcLieX7co/g8r9fbawPQF0juYCD1xSysHnNcXBwWL15saR/kWLJkCeLi4iztg9WfjRUYNWZfrqBYpOQsLZZl+x2EkHFFvPWuOe0vdrt9yvWzxQglkvUcE8MwY32caruEENjtdj26NSXWrl1LKioqAqbqaEREBG677TbL1cJutyvuTuwPwt8bdS/q2aYR96NaOoNUqOSEixCCkZGR1gmCJXcBM9m0aRM2bNigW3sOhwOVlZUoLi7WrU0AuOeee8iMGTN024TCapKTk7Fq1SocPnzY6q4AAFatWhUQ6Qz3338/0WsTipaWFrz99tu6ZuyvXr0aN910k+6bUOiBnIYoWVlyrqDU0nK5XG0OAK3Ck1ZbVwAM2a1jcHBQV8EihCA+Ph45OTm6tRkI3H777eTixYuW75yTnp6O22+/3XLrCoCuuxkNDQ3p/v0qKCjAzJkzdW1Tb+RSFPzNxSKEYHh4uMUGoFXacKhhxJbyTU1NIbe2LSIiAtu3bydWLoVxOp3Yvn07CYYlQ/7S2Nio+z2TmJiod5OG4MslFB9KMSyv19tqA9BixQDMJC4uTvc1c1ZbIUaRnZ2NBx54gFixEQHDMHjggQeIERuOBgJ63zPR0dEBOburhFSIxAIl/bdCUL7V5nQ6G8gNLB6OccTGxuqeLd3a2hrQhe+mwtKlS7FlyxZTRYthGGzZsoUsXbrUtGuaCcuyugtWfHx8wAqWWvxKLeiuktZANmzYcM327W9/e5gQ0i5uMNSw2+26Lz/p6ekJ6NLCU2X16tV48MEHTXHNIiIi8OCDD5LVq1cbfi2rMKLUc3Jysq4z6kYhF7uSWlZyFpb4fI/H0/LNb37TJYy2PhSFSkx6erqu7bEsi+bmZl3bDDRuuukmPProo2TGjBmGXWPGjBl49NFHyU033WTYNQKB5uZm3S1yIz8XI1CKXUmFS84KGx4ergcA23PPPQcA9dYOxXiMWC939erVkAu8S8nJycETTzxBNmzYoOuavqioKGzYsIF8+ctfJqE22yqHEfdKMAiWlllBLaI1MjJSDwAOAOB5vk5IPgu0XV/1Ii0tDTabbcp5U2Lq6upC+j0TiIqKwl133UWWLVuG0tJS5uzZsxjdkdlv4uLiUFRURFatWkXS0tJC+40TUVdXp2t7drsdqampurZpFEoipXRI3UIAGBwc/JtgEUIuaf3iBUKu1mRIS0tDQkKCrnGE9vZ2dHZ2Ii0tTbc2A5m0tDRs3bqVrF+/HpcuXcKVK1eYxsZG9Pf3K9ZddzqdiI+PR3Z2NpkzZw7mzZtHRuOJYSNW7e3thgTcU1JSdG1TL9T0wZdoyQXeAaCvr68aGBUsABelFws1qyEqKgoZGRm6ChbLsqirqwsbwRJISEjArbfeiltvvZW4XC50d3eju7sbLpdrTLicTieioqKQnJxMkpOTpe5kaN1cPqirqwPHcbq2mZmZGRRld/xxA9ViWENDQxeAUcGKiIio5jiOEEKYUBMqMdnZ2bh48aLvE/3g0qVLzMc+9rHgMzl1IioqCpmZmUoZ4XLvS+jeYApcunRJ9zEHW66ammXFcZwv0eKXLl16CQBsAPCd73xnEMB1S0dkAkYEd+vr69Hb26t7uyECIzrCkt7eXly9elXXNhmGQW5urq5tGoFSKoPcLKFYtKTWlcvlqv/pT386AowKFgDwPH9BKQ8rGGNWcsyYMUP3ciXDw8O4cuWKrm2GKGEpXFeuXMHw8LCubcbHx+uepqMXUq3wFXCXWldySaMDAwNjbpENAJ577jkQQs6YOTAriIuLM8SUPnfuXNh9ESnaMOLeyMrKwrRp0/Ru1hDUBEsQK1+i1dfXN6ZNNlHDldKGQ5G8vDzd26yrq0NnZ6fu7VKCm46ODtTW1ure7pw5c3Rv0wjkcrDU0hmkLqHwt11dXaeFNsV5/achQTxjGCoClp+fr3uBMrfbjaqqKl3bpAQ/Z86cgd4FEZ1OZ8AKllgj/BUrpRgWAAwODk4UrGnTpl0hhPRpsbCCeSYxMzPTkJpbVVVVjFIuEiX88Hq9qKqq0v2Lkp6eHrDxKylqKQ0cx8m6hFJ30Ov19u7YsaNBaHNMsJ5++mlCCDkjtqqCWZiUsNlsKCgo0L3djo4OVFdX694uJTi5ePEiOjo6dG937ty5Qfe9VMu7UhIr4XFgYKAyNzd3zHoat9Sb5/njwgVCOY5VWFhoSLtlZWXBdSdRDKO8vFz3e4FhGCxatEjvZg1By+yg+FCysLq6usrF7Y4J1uhMYbHY9xQ/Sv8dzMyaNcuQhaO1tbW659xQgo/a2lpDgu1ZWVkBmzA6lfiVWpZ7W1vbuNrm4ywsh8NRTG4woROhhM1mQ1FRkSFtHzlyhFpZYU5xcbEh90BRUVHQuINyyaFSy0oayxKfOwofExNTKm53nGD94Ac/6CaEyCaQSt+oYHnjlFi8eDEcDofvE/2kurqaWllhzNWrVw2JZTocDixcuFD3do3Cl0uotiRHeBwcHDy3Z8+ecctIJpQrJIRM2F4mFN3C6dOnIz8/X/d2eZ7HX//61+BWc8qkOXToEKNnCSOBuXPnYvr06bq3qwdSfZBbkjOZ+FVnZ+cx6bXGCdZzzz0HnuePhXrQXeDmm282pN1Lly7h0qVLhrRNCVyqq6tx+fJlQ9pevny5Ie0agdSqkma3S91B4ZBaZO3t7SXStidYWDabrTgULSo5CgsLDakpRAjBwYMHmVDdpIIyEY7j8P777zNGfF/S0tIMScUxErXlOHJxK7kYVl9f3wRvb4Jg/fM//3MDIeS6moUVKiIWFRUFo2qJNzY24uTJk4a0TQk8jh8/jsbGRkPaXrp0qe7b1OmFVAt8CZWSSyi2xgDA5XJdPXr06IQKMrJbbiilN4Ra4B0Ali1bZlghtA8++ICZbClhSvDQ19eHDz/80JAvQ0xMDG655RYjmtYdudiVknUlF78SH93d3RPiV4CMYI3GsYqlHQhVUlJSsHjxYkPaHhwcxP79+w1pmxI47N+/nxkcHDSk7SVLlui+p6aRKImVXIUGpZSG0fjVBHcQULCwAPzVV+A9lERszZo1ui+IFjh9+jRz/vx5Q9qmWM+5c+cMW/jucDgQyHs1anEHCSGylpWvlIa2trYjcteUFaxnn332Is/zddJOhZJIicnIyDB0ycO+ffuYgYGB0Hzzwpi+vj68++67hsVFFi9eHLCpDFJ8ZbdrFS0AcLlc9cXFxbLT7IrbxvI8/2ehAbGlFQpxKznWr19v2C663d3dwo1NRSuEeO+99xijymM7HA6sW7fOkLaNwtesoNIhtcxaW1v3KV1D9hs6Gsc6IJcEJu1gqAhYVlYWlixZYlj7Z86cYU6cOEFFK0QoKyvDmTPGFeldunRpQG+UquYOSl08QbhYlvXpEgJAQ0ODYuBX0aSIjY39EMCIcFGh3EyouoUA8PGPf9zQ6eN9+/Yx165dA6hoBTXXr1/H/v37DfuljoqKwsc//nGjmtcduUx3La6g3Cwhz/MjOTk5svErQEWwvvOd74wQQo6oWVihRnp6Oj72sY8Z1r7b7cbOnTtto5sShPabGaIMDQ3h9ddfZ/SuJCrm1ltvDZpdnQV8rRlUmh0Unw8AHR0dH7z88ssjStdRFKznnnsOLMsekHZG6gaGmoitXbsW8fHxhrXf2dmJXbt2MaMba4bWmxficByHnTt3MkbW709ISMDatWsNa18PlNxBqXsnZ1mJ3UK5HKympqYDatdWjTLbbLb35NIbpB0OlTgWcOOGWb9+vaHXuHjxIiNyKahoBQn79+9njF4jum7dOkN/MPVGzh1UcgmlYiUnWp2dnaqJi6qC9eyzz9YSQi75sqJCzcpasWIFsrKyDL1GcXExI6qdFVpvYAhy+PBhFBfL5jLqRnZ2NlasWGHoNaaK3HddKlRS908abBfPDoqD7UNDQ+eOHTvWMOECIlQFa9Qt3C/tVKgJlBSHw4GtW7cabjnu37+fOXnyJBWtAOfkyZM4cOCAoTcDwzDYunWrITXajEQPd1CULOpzWYjPxCNCyH4tQhVKbiEA5ObmGhqAF9izZw8j2l2FilaAUVVVhT179hh+c69cuRI5OTlGX0ZX1JJFxSIlHL7cwatXr05dsBITEw8TQjp8rSsMRavrk5/8JJKTkw29Bs/z2LVrF3P27FkqWgHGmTNnsGvXLkMK8olJS0vDJz/5SUOvoQda3EE5l1Cp9pVYrLxeb8e99947of6VFJ/2p8PhYDmOe5thmMd4nofdbg/5rHeB2NhYbNmyBf/3f/9n6HU4jsPrr7/OcByHpUuXEtwQrYB6c5ubm/Hmm2/q9uW12Wy4//77SWZmpi7t6U1VVZV4NtdQ7r77bkRHRxt+HT2RWzcodvHElpXUwpKmNABAU1PTG//6r//qs4CcJoeZ47hddrv9Mbm0hlBn4cKFWLVqFUpLS32fPAVYlsXOnTsZr9eL5cuXB5xouVwu3es9uVwuXdvTi5MnT2LPnj2GW1YAsHr1asO2ndMTNc9K61IcQbTkqjPU1dXt0tIPTYvnEhMT/wqgXdzBcGLjxo0wwxLgeR67d++Wzh4GxJvNMIyuay1tNltA/vAdPnwYu3fvNkWssrOzsXHjRsOvYxTSqgxy8StxHEsuWZQQAo/H0/qFL3xB0xSspjvQ6XRyXq93j6+1haFKZGQk7r//ftNmcPbv38+8++67YnckPN5oC+E4Du+++y5j9GyggMPhwH333RewlUTVkLqDviwssVhJg+3ADXfws5/9rCbfW/NPJiFkp9TvDCdmzpyJzZs3m3a94uJi5tVXX2WGhoaEpwLG2go1BgcH8fvf/54xOs9KzN133x2wm6JKUQu2ywXafcWupNntdXV1O7X2RbNgJScnHwXQLJ0tDBcrC7gRb1i2bJlp17tw4QLz/PPP2xoaxuXShc8bbgINDQ148cUXGSP2ElRi+fLlWLVqlWnX0xs560qaeyV2B9Wqi7rd7sbvfve7ZVqvrVmw7HY7z7LsHmlnlQYUqtx7772YNWuWadfr6OjA7373O1t5ebnYVaHWlg6UlZXhd7/7HdPe3m7aNWfNmoWtW7eadr2pIrduUHhUKtAnjV0pJYwCQGNj4+677rpLs8vmVxSVELJLi2AFYjBVLyIjI/Hggw8iISHBtGt6vV689dZbzGuvvcb09fWJX6KiNQn6+vrw2muvMW+//Tbj9XpNu25CQgIefvjhoIxbiVFbiqNkWYlzr8SiVVdX97o/1/ZLsJKTk0sIIU1yQbdQtqqkpKWl4aGHHjJ9GUVVVRXzwgsv2M6dO0etrUly9uxZPP/884xRddiVsNvteOihhwzZB9Ms1HKv1CwspfiVy+VqeOGFF0740we/BGvULdwpN1sotapCXcDy8/Nx//33m37d7u5u/OEPf2DeeOMNOWsrtN/0KdDb24vXX3+defXVV5menh5Tr80wDLZt24b8/HxTrztV/Mlslwu2y1lZ4tpX169f37lw4UK/7tnJmAivEEK+IRYqoRKpWLRC2S0UWLZsGQYHB/Hee++Zfu1Tp04x1dXVzB133EGWL19ORNZeQCWcWg3Lsjh+/DgOHTpk2FZcvti8eTNuvvlmS66tJ0rxK6XcK7WlOIQQXLly5RV/++C3YKWlpZ3t7u4+xTDMLULpZEGspKIVDlnx69atw9DQEA4fPmz6tQcHB/H2228zp0+fZm6//XYyf/584ddKeAztN98H1dXV+PDDD4Wy1Jawfv36gC/IJ4fWYLvSMhyplSWNXfX19ZWVlZVd9LdfkwrCsCz7ss1mu0UavzJq15lAZ9OmTXC73Sgr0zw7qyvXrl3Djh07mMLCQqxduxZ5eXlhLVx1dXU4cuSIqakKcqxZswabNm2ytA964iurXckdFM4Ta8WVK1d2TKYPkxKsqKioP3Ec91+EkBixayhdFC1ndYUqW7duhdfrxalTpyzrw8WLF5nq6uqx9Y/5+flS4QJCWLxqampQVlbGXLhwwfIY6i233IItW7ZY2ofJImddycWu1NIZhEepNTZqkQ0NDw9rThYVMynBio+P7+vo6Hjb6XQ+Ik0gDVcry2azYfv27QBgqWgRQnDu3Dnm3LlzmDt3Lm699VYUFhYSp9M5dsroY0gIl8fjQXV1NU6cOMHU1NRY3R0AN8RKuBdCBX+D7XKzg0KwvaWl5c0PPvigfzL9mPS8PMdxLzscjkeEAQhbvYezlQUA27Ztg8PhQHl5udVdQU1NDVNTU4OMjAxm8eLFZPHixSQtLU14WWqCBNUH1NbWhnPnzuHMmTNMW1ub1d0Z47bbbgtay0oOX9aVnFWl5g4SQlBdXf3yZPszacFKS0s71NvbW2ez2fKEgQTqCnwzYRgG9913HyIjI3HkiOL2aqbS2tqK1tZW5ujRo8zs2bPJwoULMWfOHJKUlCQ+TYsPpfeH65er2tPTg9raWpw/f565cuUKjNxqazKsX78+6GNWaqtX1FxBqWWlFGx3uVz1zz333NHJ9m/SgmW32wnHcb+32Ww/FERKUNBwdQvFbN68GbGxsdi/32fVV9NwuVy4ePEic/HiRcTGxjI5OTmkoKAAs2fPJhkZGVZ3T1a82traUFdXh8uXLzMNDQ0QLQYPKDZv3hx0W8trwVdFBl8zg+KSMgBQX1//0rJlyyYdYJxSqjbDMK8QQp4hhNjEbl+4u4UC69evR1xcHPbs2QMzl4BoYWhoCBcuXGAuXLgAu93OpKenIy8vj+Tk5CAzM5MkJiaOy+Q3YjmJtE2WZdHX14fm5mZcvXoV9fX1TGtrK8yo+jlZnE4n7rvvPlMXxRuFWiqDnGgJ4uT1en2uGxx95Orq6qZUvndKgpWWltbQ0dFxyGaz3SEeEHUN/8ayZcsQHx+PN954A729vVZ3RxaO49Dc3Izm5mamuLgYdrudSU5ORkZGBpk+fTqysrIwMDCg62fKMAyuX7/O9Pb2oqmpCe3t7WhtbWV6enrAsj4r5QYEiYmJePDBB4Mug90f/HEF5ZJGxaLV2dn5/okTJ6ZUtpaZ6vRvS0vL9qioqJ02mw12ux02m22cYClZXeFGR0cHdu3aBUmpmKBCnLoSSG1ZQU5ODrZv3w7RJEZQo5TKILamvF4vvF4v3G43XC4XRkZGxo7h4eGxf7tcLni9Xng8Hni93jHhKikpuf/gwYN7ptLPKQebUlJS3uJ5XnZBtJhwFSqBtLQ0fPGLX8Qtt9xidVcmjZ4CE8xitXz5cjz66KMhI1ZKTCZ+JbfRBCEEbrf72po1a/ZOtU9TLjcQERHh9Xq9L9pstn+RilW4LtdRIioqCtu3b0dmZiYOHDgQcHEtijpOpxMbN27EmjVrrO6KrqglisqtGZSKlDSGJf0bAKitrX1h586dU/b1damP4nQ6f0sI+QHP8xFqawuD3Q3QizVr1mDmzJl45513dN+JhmIMM2fOxNatW00t3mglUtGSq9OuJVF09O9djY2N/6tHv3TJP0hJSWnzer1vKLmESrMP4UxOTg4ef/zxkPu1DkXWrFmDxx9/PCTFyt9lOEpWldi6krqDTU1NO0tKSjr06K9uFeh4nv8VIeTT4goO4ez+aSEyMhL33HMP8vPzsW/fPnR1dVndJYqI6dOnY+PGjVi4cKHVXTEdadxKGnyXCpecdSW4g2fPnv21Xv3STbBmzJhR3tHRcZJhmOXS/A2xe0jFbCILFy5ETk4OPvjgA8M3bKVoY9WqVbjzzjsRExNjdVcMw5d1JXUH5Wq1+8q/6u3tLS8vLz+pV591rfHr9Xp/bbfbXxGvL6TCpI1p06bh3nvvRVFREf785z8HdfpDMJOTk4ONGzciLy/P6q5YhtQdJITIuoLif8vFrgDg8uXLullXgA55WGJGRkYih4YbIqqlAAAgAElEQVSGrtnt9uninCxhqY5gYQE0L0sNjuNQWlqKo0ePQlIGmWIQCQkJWLt2LVatWjW2kD+UUbOuBEtJyLvyeDxwu93jcq5cLtdY7pXL5YLb7R47XxAwj8fTUV9fP+uPf/yjS69+62phRUdHu3t7e1+y2WzfFQfe6fpC/7Db7bjttttQVFSE4uJinDhxAi6Xbp85RUR0dDSWL1+O2267zdSdkAIVX8F2QYzkrCtpzfa6uroX//SnP+l64+pqYQFAe3v7LLvdXmuz2RyClWW328csKUG4xNelVpY6bW1tOHz4ME6fPk1nWHXCZrNh2bJlWLduHaZPn251d0zFV96VIERCprrL5RrLbBdntAuHnHVFCGHfeuut2VVVVbrm7eguWADQ2tr6WkRExEPS5Tpi1xCgbqG/XL9+HeXl5aiqqqJJp5PE6XRiyZIlWL16NbKysqzujiX4uwxHcAelrqDgDgrCJggWz/Nobm7+44svvvh3evfdkI31CCE/IYQ8iFFBFLuGcuJEA/PamDlzJmbOnIk1a9agtLQUZ8+exfDwsNXdCgpiYmKwdOlSrFixAgFQSscy5AwUtbwrcSxLLGRia0qujExVVdXPjOi/IRYWALS1tX0QERFxO8Mw1MoyiM7OTpw+fRpnzpxBR4cueXkhR3p6OhYvXoybb745qDcx1Qut1hXLsj4XObvdbng8nnGLnDmOQ0dHx19+9atf3WVE/w3buphl2f90OBy322w2UCvLGFJTU3HnnXdi7dq1uHTpEk6fPo2ampqgKc9iFA6HAwUFBbjllltQUFAQ9FvD64WScSJnXcmtF5QG2uXSGQDg3Llz/2nUGAyzsACgvb39tMPhuElqYQnpDdTK0p/29nacPXsWH330EVpaWqzujqlkZ2dj4cKFKCoqCrtAuha0WFeCOAnWlRCzUopdeTyecSLW399/5pFHHrlpKlVF1TBUsBobGz8dHR39qjj4LswYygkWQEVLLwghuHbtGqqrq3Hp0iW0tLQEdOXOyeB0OpGeno6CggIsWLAgJNf66YVS7EpqUQmWlHhmUOoOCnlXgiso/C0hBKWlpQ/95S9/mdQWXlowVLC8Xq+jp6fnisPhyFGzsqhgGQvP82hqakJdXR3q6+vR1NQUtAmpCQkJyMrKwpw5c5Cfn4+MjAx6z2hgstaVUoE+6cwgx3EYHh6uj42NLXjmmWcMi0kYFsMCAKfTyXq93ufsdvt/ixdFSxE/T2NZ+mOz2cZmGNetW4f+/n60traioaEBTU1NaGlpQV9f31gMIlCw2+2Ij49HZmYmsrOzkZubi+nTpyMuLs7qrgUVWmNX0vIx0hlBtSJ9AHD58uWfvfXWW4YGUA0VLACIi4v7ncvl+gHDMCnCwIS67wJUoMwlPj4e8fHxKCgoAHBjN53Ozk60t7ejubkZbW1t6O3tRW9vL9xutyl9io6ORnx8PJKSkpCZmYkZM2YgNTUVycnJiI6ONqUP4YJagT65Rc1i4VKqyuDxeLp6enpeMbrvhgtWfHz80MDAwPN2u/0HgpUlzBwqFfejVpa5REVFITs7G9nZ2bj55psB3FjPODg4iIGBAfT29qKurg4lJSW6ZdozDIPVq1djzpw5SE5ORkxMDGJjY8NiHZ+ZTMa6EguUXN0rubyr2traXx4+fNjwPdgMFywAiIyM/BXP808zDBOtVOSPClRgYbfbkZCQgISEBGRnZyMlJQVlZWW6Be5tNhuWL1+OGTNm6NIeRRt6WFdSC4tl2aGamprfmNF/U1Ykp6amtnk8nt8Kb5KcYAHjRYuumQssXC6Xrp8JIYQu6DYYpUq/eltXDQ0Nz1dUVHSaMSbTSihERET8B8/zI3JFwuTEi0Kh6I+adSUnUL6sK47jXGfPnv25Wf03TbDS0tJa3G73S+ICX9IMeGnsiooYhTI51CoyyFlXci6g3G44Uuvq6tWrv6msrDQtQ9nUIlWRkZE/JoSM+HINKRSKMegVuxr93ro++ugjQxY5K2GqYKWlpbV4PJ4dou1/qJVFoeiMr3pXSoX5/IldEUJw9erVF6uqqprNHJvpZUDtdvtPCCFuf2JYVLQoFG34+h6Js9vl6l8plZCRs67OnDlj2CJnJUwXrIyMjOtut/sVf6wsCoUyObRaV0rxK6Wt569evfq7ysrKJrPHY0mhdafT+WOe5z3SOBa1siiUyeOvdSVXOkZuGY405kUIcVdVVf3UxKGNYYlgpaenX/N4PP8nVn1qZVEo+qI2Kyi3XlAuhiW3ZrChoeElvWu1a8WyrWycTuePeJ73yL2passJKBTKRNS+M1ILSc26EouVdCec0cNrlXUFWChYgpUlt/kiQK0sCmUqyOVdSXOulGYHpfXaxd/Na9eu/W9lZaVlu/xaulmg0+n8Ic/zI+I3ReoeSqFWFoUyHn8XOKvFrsQ738hktY9UVlb+2OThjcNSwUpPT2/2eDzPKyWSUiuLQvEfLVntarEruSU4AFBXV/dLq2JXApZvxxwdHf3vPM/3y4kWtbIoFHX8sa7kYldqgXZxsN3r9fZVV1dbFrsSsFywUlNTO91u98+lsSxfVhYVLUq4I/cdmKxlpWZhAUBNTc1PT5061W32GKVYLlgAkJiY+DOe59ulMxpac7QoFIp6oF0tdqU0Myi04/F4OhobG//H6vEBASJY8fHxg263+ye+RIpaWRTKDXwlifpKY/CVcyW2ri5cuPAvJSUlA2aNTY2AECwASE1NfZ7juGvSGUNa1YFC8Y2WJThSkZLb+UaayjAyMtJgs9l+Z/X4BAJGsGJjY11ut/tfxG+6NI4FUCuLQvGVJKrkDqq5gkp5V2fPnn3mzTffNGcnEg2YUtNdK1lZWa90dHQ8zTBMIcMw4Pm/bVqhtEUYxRxiYmJQUFAw5iZMFZvNhpiYGF3aCnd8xa7k3EBhx2Zx3pU0fjU0NHRp7ty5f7J6fGIM3Uh1MtTX12+LiYnZ5XA4wDAM7Hb72K7RauJFc7Uo4YDSzKDUBRSESNgUVbwJqngHZ7lNUQXBKikpue/gwYNvWTBMRQLGJRSYNWvWbq/XW8pxnGwAXimWFWjCS6HojdY0Brk6V2LLSmljVHG8uLe3t/z73//+2xYMU5WAEyy73U5Ylv0mz/NEGs+iwXcK5W9IvxNid9DXAmfhkFuCA4CcOHHim+vWrQu4L1rACRYA5Obmlnm93j1yawzVoEJGCVW0pDH4Kh0jta6U0hiam5t3lpSUlJg1Nn8ISMECAJvN9k88z7vV0hwolHBFzRUUAu1a1wyK3UGe5z0nT578gdXjUyJgBSsrK6vO5XI972/mOxUySqjhK43BVzUGrYmiAFBXV/eL06dP15o5Pn8IWMECgGnTpv0Lx3Fdckt2APUPkkIJBXwF2n2lMYhnANXEanSBc8/Fixd/YsEwNRPQgpWamtrjdrt/LFcri7qFlHBEOluuZl2Jc620lD4+f/78DwNhgbMaAS1YAJCRkfErlmWvCGkOYvECqJVFCV38KR2jFmj3FWwnhGB4eLg2Ojr6BZOH6DcBL1jR0dEel8v1PbkZQxqAp4QTaqVjtJaPkRMrAKisrPzWn/70J4/FQ/RJwAsWAOTn57/Bsmyxv64hFTJKsOLPekFpzpXYqvIlVoQQ9PT0lAVikqgcQSFYAMCy7Ld4nidybzgVLUoo4W+gXW0zCbVKoqPfIb68vPwfAzFJVI6gEazZs2eXu93uP8jlZAFUmCihi1ygXSw8SqVjfJWPAYDGxsZXysvLT1g5Pn8IGsECgOjo6H/iOK7f32RSKmaUYGEqgXZBpORSGaRLcAghYFl2oKKiImCTROUIKsFKT09vc7vdPxZmDKULNmkAnhJq+Aq0y7l/WjLaAeDChQs/rKysbLF4iH4RVIIFABkZGf/NsuxlOaGiVhYlmJlKoN3fNIahoaEaAL82d4RTJ+gEKyYmxuPxeJ4WZ77TADwl2JlKoF1pVlAp0A4Ap06d+logVRLVStAJFgDk5+fv83g8+9UC8FScKMGMmlhNNdDe1tb2zqFDhw5YPMRJEZSCBQAMw3xNXM1Bbq2hHFTIKIGGltIxkwm0y7mCPM97jh8//i0Th6crQStYs2bNuuJyuX4pXrJDXUNKsOHLFVQKtCvlXPkKtNfU1PxnRUVFjdnj1IugFSwASE5O/leO45rl1hlSt5ASzIjFSi3QLrWw1ALtLperqbGxMaCrMfgiqAUrMTFxQGmdIbWyKIHOVDPa5aoxiJfgSGNXp0+f/vbRo0cHzR6nngS1YAHA7Nmzf+/1eo/ILdkB1APwVLQoVqFVrKRVRKWzgloD7d3d3ce+973vBdSWXZMh6AXLbrcTnuf/ged5r5wJTV1DSjAhZ1lpqSKqltFOCGHLysq+EizrBdUIesECgNmzZ59zu92/VMvLolYWJVDQGmj3x7qSBtqFuC4A1NTU/NeJEyfOmj1OIwgJwQKA5OTkZ1mWbZRLcwDgU7goFKtQi1uJl9+IXT9f5WOEtlwu17Xr16//m9Vj1IuQEazRAPzX5aws8bSu3A7RVMQoZuFPzpV4s1M5oRJbV8IMojR2deLEia8Ge6BdTMgIFgDMnTt3t9vtfk/40KS/NmoxLSpaFKNRcwXl3ECtrqBwSL2Kjo6OP3/44YfvmD1OIwkpwQIAm832NZ7nXdJlO2LXkGEYKlAUy1GbFRTnXEmtKqk7KJckynHcSElJyVesHqPehJxg5eTk1I6MjPxEuqOt1MKiriHFTCa7/EZsVaktbpYuS6uurv63ysrKOrPGZxYhJ1gAkJWV9ROWZS8JMyVqhxQqWhS9mezyGznRUsq5Et/rQ0NDNRzH/ZcFQzWckBSsmJgYt9vtfkpqZUktLeoaUqxAnNQsrXMlta6kh5J1JW7r+PHjTwZj6RgthKRgAUBBQcH7brf7NWkAXprmQF1DipGo5f8Jh6+tuvxZftPc3PzqkSNHPjRzjGYSsoIFALGxsV/lOK5TbRNW6hpSjGIyy2/kUhjkrCtpCgMhBB6Pp+vUqVNPWzBU0whpwcrIyOgcHh7+tnBDSGcNqWtIMROpKyi3/EauCoNa3EpsXVVUVHzj9OnT7VaO0WhCWrAAYM6cOTs8Hs+HSvWB6LIdihFocQWlCaJKOVfSJFFpbBYAurq6Dn/3u9/9g5ljtIKQFyy73U4APMlxnGxulrSygxQqWhR/0eoKypU8lstkF8rIKBXm4zhu5NixY4+FwuJmX4S8YAHA7Nmza1wu14/EH7Tc7Ap1DSlG4MsVlAbUldYMKnkJFy9efLaysvKKlWM0i7AQLACYNWvWf7AsW6VU4I+6hhQ90OoK6hVoHxgYOJuQkPBzk4dpGQ6rO2AWUVFRrNfrfdxut5cyDGOXpjPYbLZxVpb0daUUCApFwB9XUBAfuT0FtQTaR9vjS0tLHy8tLfVaMFxLCBsLCwDmzp17wuVyPe8roRSgVhVl6midFZSKlNJaQenym7q6ul+WlpaWWTlGswkrwQKA1NTU77Es2yhdtiPOzVKCihhFicm4gnJxK7lNJeSsK5fLda22tvYZk4dpOWEnWElJSQNut/tJuQA8nTWkTAajXEHBuhLHroT2jh8//kRJScmABcO1lLATLACYN2/ePpfL9epkXUMqWhQBtfvDnwRRqSuoVomhqanplWDduXmqhKVgAUB8fPw/chzXqrZsh0KZLFpcQbmifEppDEJbbre7tbKyMqSX36gRtoKVnp7eNTIy8rj4hqCuIcUfjHQFpYmlwj154sSJr5w6darbguEGBGErWAAwf/78vS6X6w3pjUFdQ4ovJuMKygmVkisojVsBQEtLy+sffPDBHlMHGmCEtWABwLRp077Msmy7NJZFXUPKZBBbV+KYla8EUV+uoMfj6Tx58uTXrB6f1YS9YM2YMaNzZGTk63IBeGmwk1pZFECffQV9uYLS++/EiRNPhXolBi2EvWABQGFh4Z9cLtdbcqIFgIoWZQwtrqDYulKrz67mCgrWFQC0t7e/+/77779u6kADFCpYo0RHR3+F47gepVQHKkoUNZRcQbUUBi2uoNfr7SsrK3vS6vEFClSwRsnOzm4ZHh5+Wi4AT11DCqDNFRQnIytZV2oJotL7raKi4h8rKyubLBhuQEIFS0RhYeEOj8fznjQGIb4hpVUexFDRCl38iVspBdndbrds/Eq66kLkCu47cODAKyYPNaChgiUhKirqMZZlu6X1ssRipVa1gYpW6OHrM1UrGSOXwiDdtVlhVrCrrKzsMZOGGDRQwZIw6ho+pbTWkKY8UADlLeaVEkTV4lZyda5OnDjxlcrKylarxxloUMGSYcGCBX9yu92KCaXSQwoVstDBn2x24V7RmsKgtFawra3trffff3+n2WMNBqhgKTBt2rQvcxzXpmXWkIpWaOIrhUHqCkr3D1SbFZS6g8J95fF4Oo4fP/6E2WMNFqhgKTBjxozO4eHhL2mZNaSEF0qLmpUSRIVgu4YKojh+/PgTNEFUGSpYKixYsGCvy+X6g68yNNQ1DD20zgrKLb2RcwWFQ22tYFNT045wXyvoCypYPkhOTn6KZdnrcpYWgHEBeCpaoYEWsZJmsyttICG3kYRckN3lcjVVVVWFbdkYrVDB8kFaWlqfy+X6IsdxRBpcFU9D01SH0EBrCoM0m92XdSV1ByXxUFJSUvLoyZMne0waZtBCBUsDCxYseH9kZOS3agukaapD6KK2qFkQLTU3UGlfQeGeaWho+M3Ro0f/bPEwgwIqWBrJyMh42uv1XtaS5kBdw+BEawqD1MJScgV95VsBwNDQUPW1a9f+yeyxBitUsDSSmJg45PV6H+E4ziv3S0lTHYIbLVUYtKQw+OkKsocOHfrchx9+OGz2eIMVKlh+MH/+/Irh4eEfK5VV1pLqQEUr8FD7TOQSRMUzfUpLb+RcQWn44MKFCz88derUCbPGGQpQwfKT2bNn/8jj8ZTLLd3RkupACQ58pTAo5Vqpxa2E+wQAenp6ShctWvQTi4cZdITNVvV6ER0dzdrt9k9zHFfFMEwcwzBgGGbsRrTb7eB5HjabbUyw6Lb3gYs/+VbS5FBprEosWmrZ7CzLDh07duzzFRUVnAVDDmqohTUJ8vLy6oaGhr4p9+sp5xrSeFZgMpmlN/6kMChls1dUVDxVUVFRY/Z4QwEqWJNk0aJFv3W73e8qiZVcIJ4SHPjjCqqJlVw2e3t7+zv79+/fYfEQgxYqWFNg2rRpX2RZtlUchJebNaSpDoGHv1UY5NYJut1uxRQGuVlkj8fTXlZW9rgFww0ZqGBNgaysrI6RkZEvcRxHpL+o0nwbKlqBg79xK7Xa7NK4lTh2Jc1mLy4u/nxlZWWbBUMOGahgTZFFixa963K5fi1eqqNWikYOKlrmoeWzkK4TVKrC4E8KQ11d3XNHjhw5YMYYQxkqWDqQnZ39TY/HUyVdayjnIlJxCjzkLCu5BFFp+oK0bIxSCsPAwMDZ7u7u71k8zJCACpYOxMXFuXme/zTLssPiX1fp7BBAXUMr8WfpjdaSMVI3UC6F4dChQ9v37dvnsmDIIQcVLJ2YN2/ehaGhoa9Ll2DILZD+/+2dbWxb1RnHz7VNmmYQmrC2aGrajJK2aZuXRo0QQ4gNvkwTTFtLW14m2IBNBbqVUW3wYSAx0LRN2oZAU6ut27ShoMHeTAlKQhKu06Zpk9iOk+GmCs2SQhhxl4bGL9e+vvfa+wC3nJyec18S+95r+/lJkau2JDmX+p/n/M//eQ5CEHWwmqW03hi5SALPXLEiDMFg8KzV6y1WQLBySENDw+9SqdRfyX+8tCZpFiBauWeprTe0yopMtLOmhyKE0Ozs7N8gwpBbQLByTHV19aOKokzjp0Skp4UQe2sIWANrZAwpVnp3CmoM5Hs/EAhAhCHHgGDlmLVr115KpVL7MpmMRFZZ+CkiQuBnWYGZvBXtii7cWDfaevPpFIZ7YCBf7gHBygPbtm0bSiQSL9AapMHPsg6zvhXtVHApA/nC4fCzg4ODpyxdbIkAgpUn6uvrXxBF8R3aUTf4WflnKb6V2QgDrfXm4sWLfQ0NDb+0ap2lBkxryBNutzuzYsWKB2RZHuU47jqO45DL5UKKoiya1OByffYzAyY45BetJDsZYWB5VjqtN3PHjx+/LxQKwRSGPAEVVh6pra39UBCEBxVFyeLtGpDPyi9m8lZ4kt1IU7NW683AwMDDoVDovzYsuWQAwcozDQ0NbwmCcJjlY4GflVuWk7fCqyuagGm13kxNTb3M8/wxSxdbgoBgWUBNTc2hdDo9Snb/g5+VW3LpW9EmiLJab+Lx+LuRSORpq9ZZyoBgWUBlZWUqk8ncx2rdMdpvCKLFRuuZGfWttLaD+H+D/7BRFEXgeX5vZ2dn0uIllyQgWBaxZcuWM4lE4hAZNCSNW4QgVJor9PoEteayk6eC5P839XMGg8GDfr9/3O61lgogWBbS2Nh4RBTF10ihIsfSIAQmvBn0THa8iiUrK5ZvhSfd1b9P+laRSOQf7e3tR61ebykDgmUxVVVVj6bT6ffJHA+5NdQCROszjDwr0jskt4G4b0Wm2Vm+VTKZnA4EAo9YsUbgM0CwLOb666//OJ1O71EUJU1LwqtvMPCz9DHqW2ltBWl5K7KyIiMMmUxG4nn+vqGhoUsWL7nkAcGyge3btw/F4/GnaAFEMjkNfpY5jPYJ0kRK3Rbq5K1QKBT6IbTe2AMIlk00NTW9mEql/k7bcpBeCfhZV2LUtzLaI0jbCtJ8q9nZ2dffeOON31q9XuATQLBsZPXq1Q9JknRWq0maTMOTlKJo6YVDSd9K74ourRNB3LdKJBLvjY2NfdfSxQKLAMGykdWrV8cURdkry3KSVmGRo2hYlJJoGQmHktUVbStIBkT1+gQVRUn19PTsGxgYiFq4XIAABMtm6uvr/51IJA6Sp4a0sSWlbsKbMdlZJ4J6aXaWb+X3+x8LBoMjFi8ZIADBcgBNTU2/FwThzzShMupnlSosk91MnyA5MgbfmiOE0MzMzB9h1LEzAMFyCDU1NY+KojjKmp9lpH2nmIXMTDiUFCsyY8XqE8R9K/XrxePxd6empr5v9XoBOiBYDuHaa69NIoT2yrIcpbXu0Mz3UhGtpZjsWltB0rtizWWXZTne1dW1t7e3V7B6zQAdECwHsWXLlolEIvE9ssJi+Vksikm0zJjstNYbMmOlVlqsHkH8h8PQ0ND+sbEx6BN0ECBYDqO5ufk1QRCOaImWkVBpMYiWnsmuFQ6lBULNjIyZnp5+uaurq83K9QL6gGA5kA0bNjwhimIArxZYafhSM+FZBjuroZnmW+nNZV9YWBiOxWI/snmpAAWY6e5AKisrxUgksluW5SDHcdUIoUXz4DmOQ5lMBrnd7kViRc6Ez2azBTsn3qhvpVdd6SXZya2gJEkf+3y+fcFgULR6zYA+UGE5lLq6uvPqPHja1hAPlaoUiwlvNhyqN9/KxH2C2VOnTj0UDAanLFwuYAIQLAfT3NzcLgjCb2hD/yhvNubnKSTRMhoO1Wq70YswsIz2iYmJX/T29notXjJgAhAsh7Np06an0+n0CfzN+WmryKJRNAgVvglvxmTXmhyq1SfIamqen5/n16xZ84yV6wXMAx6WwykvL5cqKir2iKLo5zhunSzLCCGE3G73ZX+KfCV/XciwJjCoVZLWJRJavhV+IphKpT7o7+/fFwwGZZuXC+gAFVYBUFtbGxFFcY8kSSLuYZEtPAgtfoOTOLnK0kuysyYw6G0D9fJWiqKI3d3du4PB4P9sWDZgEhCsAqGxsfF0PB5/An/Dskz4QkvC632fWia7kemhGiY7CgaDB/x+/7CV6wWWDghWAdHS0nJEEISjrFApvs0pFBNe7/s0YrLTkuxkdUXrFjh//vwRuESisADBKjBqa2sPpFKpYdzD0Rr852QT3siJICvJbqapmSZWly5dOh2NRp+wcr3A8gHTvcCorKwUy8rKdsuy7Oc4bo0aJEUIIfLXODQT3s5gqdETQa0ku144FBd1vPoURTHC8/yeUCgE4dACAyqsAqSuru4DQRDukWVZJreGeFVCvvmdjt64GHwrqFZW+IeRrFU2m5V9Pt++UCg0Y/d6AfOAYBUoO3bs4BOJxNP4Eb/eZAenmPBLGReDnwiaMdnJLXIoFDo0MDDQZ/WagdwAglXA7Nix41eCIPylkMbRLKfthuVbsWZbkQcRH3300ater/clq9YK5B4QrAJn/fr1+0VRHCErLfLDCSa80bYb2gUSRior2omg+jVjsdjYuXPn4MabAgdM9wJn1apVyUgkskuSJD/HcdchhBxpwpudbcUawkfGF2jVFTkyRpKkjzs6OnaFw2GYHFrgQIVVBGzevHlaEIR7P/HgFeqWyKgJn49Ky4hYabXd6HlXOiZ75sSJE/eHw+HJnC8MsBwQrCKhpaWlOx6P/5ScoLkU0bIClljpVVdalRUtyX7mzJln+/r6OmxbKJBTQLCKiMbGxucFQXiNfPPSPB2rTg6NnAiS4VDabTdmfCvVZI9EIv98/PHHf5azxQC2A4JVRHg8nuyGDRu+k06nA6wkvJXtO0ZPBPXabmhVFmtyqPo1o9FoaHx8/IHbbrvN+QE0wDAgWEXGqlWrkh6PZ7ckSRdocQfW6SGN5YiW2RNBI4P4WLOtyAoynU7P8Ty/y+fzJZa8AMCRgGAVIZs3bz6fTCZ3KYqSZhnwRsbRqH9uluWcCBox2dPp9BWVFZbwl3ievxvGHBcnIFhFSktLy8loNLqfdmqYz5PDXJ0IarXesCqrbDaLAoHAAUiyFy8gWEXMzp07/5RIJA7T0t+0N/tyRcusWNFOBEmBovUI4kY7Xi1OTU291N7e/rulPzHA6YBgFTn19fUHRVHkaRdZ0LaH+GsuoPUIsk4EWbfdkDc109pu5ubmequqqg7l7BsHHAkk3Yuc8vJyqaqq6u6FhYUhjuM2qkl2j8eDFEW5/JnAy4YAAAT5SURBVPfUVLyadqel3rWS8EYbmtXLI5ZisrPabpLJ5PTAwMA9gUAAZrIXOVBhlQDr1q2bz2azd0mSFKUlwtWKBx9Lw8Jsbou1DTQqVuQEBjLCIMtyrKOj465AIDCXk4cFOBoQrBJh+/bt4/F4/EFFUTJ67Ttm4g5m4gt4dWX0HkGtE8FsNpvp7+//1ujo6Lt5eWiA4wDBKiFaW1u9sVjsOdzopokWQsbiDmbjC3iVZCQYqnciGA6Hf8Lz/LG8PTDAcYBglRjNzc3PC4LwKpkuX8rJIQ3aiSApWKRIpVIpqlhpnQjOzs6+fuDAgZ/n6zkBzgQEq8TweDzZG2644WFRFE+TWy3y1axoLTVrRfOttHoEFxYW/NPT09+GtpvSA04JS5DKysrUypUrvymK4mBZWdl6rVlZLpeL+Wc4WlkrI8FQ2m03tMoqmUye53n+zpGRkWSOHwtQAECFVaLceOONs4qifE2SpAWtJLyRSovmWZnpEcRftXoEZVmOdXZ2fn1kZCRiwyMDHAAIVgnT2NgYTiQS9yqKotBCmXptPHq9gSzPSkustAbx9ff33x8Khcbsfm6AfYBglTitra0d0Wj0KVxgWBUXbdoDK2NFihWt5cboiONsNotGR0ef5Hn+TbufF2AvXC7bMIDCxe/3H7766qv3ezwe5PF4kNvtvvzqcrmQy+VCbrf7ihnxeIWl1x+ongaSr3pp9pmZmT8cPXr0ETufD+AMwHQHEEIIbdu27Qdnz56tKy8vv4Pc6qmClclkkMvlWmS807aDRlLsWhef4j2C8/PzfYIgPGbXcwGcBVRYwGVmZmaqLl68ePqqq67ahFdaapXFcdzlV1W01CoI3zaSc61SqdTlV7KyIttu1M+BEEKJROKsz+e7eWho6JKdzwVwDiBYwCLC4fAXZVk+5fF41tK2hapoqZBixbrpRi8cSpsa6vV6bw6Hw+dsfByAwwDBAq5geHh4Z1lZmc/lcn0Or7DI6kr9t6P2CGoJFkusaCa7oijJrq6uOwYHB0/Z+RwA5wGCBVA5efLknRUVFV6Xy+VWxUqtrEjDXRUbfMyx+sFqaqZtA7H4wt09PT3/snP9gDMBwQKY+Hy+/ddcc81h8gZpdV4WeTqo19ysVl6qz4Wb9OrnCoVCB71e70s2Lx1wKCBYgCY8z79YUVFxkPx9rdNBcluIV1y000B1Kzg5OfnrV155BaaGAkwg1gBocuuttz7Z19dXs2LFil1a447JnkHyA/e4aA3NFy5ceLOuru7Hdq4VcD5QYQG6zM/Pr/T7/T0ej+dLpG9FGu7kZAa8oqJNOkUIoYWFheHR0dEv9/b2CjYvFXA4IFiAISYnJz8/MTFxkuO4Taybb8hTP1LIyFHMCCEkCMJ/uru7bw4GgxdsXiJQAEAvIWCIjRs3zlVXV98lSVKEte0jW2zwCovsRUQIIVEUIz09PV8FsQKMAoIFGOamm26aqK6uvl2SpA/IoCgeHlV/nxwKiJ8splKp999+++2vBAKB9+xeF1A4gGABprjlllvObN26tTWZTHbi0xRoUx1oo2kQQmhubu6t48eP7/T7/eM2LwcoMMDDApZMW1vbblEUn+E4ronmX5HjlmOxWGh8fPy5Y8eOee3+3oHCBAQLWDZer3fr1NTUN7LZ7O3ZbPYLHMetUxQFJZPJmWQy+eH8/Pw78Xjc29bWBhUVsCz+D2427N/h/WquAAAAAElFTkSuQmCC'
    })

    var touch = {
        start: 'touchstart',
        end: 'touchend',
        move: 'touchmove'
    }
    app.directive('disableDrag', function() {
        return {
            restrict: 'A',
            priority: 1,
            compile: function($elm, $attrs) {
                return {
                    post: function($scope, $elm, $attrs) {
                        $elm.on('dragstart', function(evt) {
                            evt.stopPropagation();
                            evt.preventDefault();
                            return false;
                        });
                    }
                }
            },
            
        };
    })
    app.directive('onClick', function($timeout) {
        return {
            restrict: 'A',
            priority: 1,
            compile: function($elm, $attrs) {
                if($attrs.onLongPress){
                    return {}
                }
                return {
                    post: function($scope, $elm, $attrs) {
                        var promise;
                        var event;
                        var setEvent = function(evt){
                            if(!evt.evt){
                                if(!event){
                                    event = {}
                                }
                                evt.evt = event;
                            }else{
                                if(!event){
                                    event = evt.evt
                                }
                            }
                        }
                        $elm.on(touch.start, function(evt) {
                            event = null;
                            setEvent(evt);
                            // Locally scoped variable that will keep track of the long press
                            event.longPress = true;
                            event.scrolled = false;

                            // We'll set a timeout for 600 ms for a long press
                            promise = $timeout(function() {
                                promise = null
                            }, 600);
                        });
                        $elm.on(touch.end, function(evt) {
                            setEvent(evt);
                            // Prevent the onLongPress event from firing
                            var click = !!promise && !event.ended;
                            event.longPress = false;
                            event.ended = true;
                            if(promise){
                                $timeout.cancel(promise);
                                promise = null;
                            }
                            // If there is an on-touch-end function attached to this element, apply it
                            var locals = {$event: evt}
                            if(click){
                                $scope.$apply(function() {
                                    $scope.$eval($attrs.onClick, locals)
                                });
                            }
                            return false;
                        });
                        $elm.bind(touch.move, function(evt){
                            setEvent(evt);
                            event.scrolled = true;
                            if(promise){
                                $timeout.cancel(promise);
                                promise = null;
                            }
                        });
                    }
                }
            },
            
        };
    })
    app.directive('onLongPress', function($timeout) {
        return {
            restrict: 'A',
            priority: 1,
            compile: function($elm, $attrs) {
                if($attrs.ngClick){
                    $attrs.onClick = $attrs.ngClick;
                    delete $attrs.ngClick;
                    $elm.removeAttr('ng-click')
                }
                return {
                    post: function($scope, $elm, $attrs) {
                        var promise;
                        var event;
                        var setEvent = function(evt){
                            if(!evt.evt){
                                if(!event){
                                    event = {}
                                }
                                evt.evt = event;
                            }else{
                                if(!event){
                                    event = evt.evt
                                }
                            }
                        }
                        $elm.on(touch.start, function(evt) {
                            event = null
                            setEvent(evt);
                            // Locally scoped variable that will keep track of the long press
                            event.longPress = true;
                            event.scrolled = false;

                            // We'll set a timeout for 600 ms for a long press
                            promise = $timeout(function() {
                                promise = null
                                if (event.longPress && !event.scrolled && !event.ended) {
                                    event.ended = true;
                                    // If the touchend event hasn't fired,
                                    // apply the function given in on the element's on-long-press attribute
                                    var locals = {$event: evt}
                                    $scope.$apply(function() {
                                        $scope.$event = evt;
                                        $scope.$eval($attrs.onLongPress, locals)
                                    });
                                }
                            }, 600);
                        });
                        $elm.on(touch.end, function(evt) {
                            setEvent(evt);
                            // Prevent the onLongPress event from firing
                            var click = !!promise && !event.ended;
                            event.longPress = false;
                            event.ended = true;

                            if(promise){
                                $timeout.cancel(promise);
                                promise = null;
                            }
                            // If there is an on-touch-end function attached to this element, apply it
                            var locals = {$event: evt}
                            if ($attrs.onTouchEnd) {
                                $scope.$apply(function() {
                                    $scope.$eval($attrs.onTouchEnd, locals)
                                });
                            }else if($attrs.onClick){
                                if(click){
                                    $scope.$apply(function() {
                                        $scope.$eval($attrs.onClick, locals)
                                    });
                                }else{
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                            return false;
                        });
                        $elm.bind(touch.move, function(evt){
                            setEvent(evt);
                            event.scrolled = true;
                            if(promise){
                                $timeout.cancel(promise);
                                promise = null;
                            }
                        });
                    }
                }
            },
            
        };
    })
    .directive('imgLoad', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('load', function() {
                    scope.$apply(attrs.imgLoad);
                });
            }
        };
    })
    .directive('helpButton', function () {
        return {
            restrict: 'E',
            template: '<ons-speed-dial position="bottom left" direction="up"><ons-fab class="info-button-float"><ons-icon icon="ion-help"></ons-icon></ons-fab><ons-speed-dial-item ng-click="helpLink($index, link, $event)" ng-repeat="link in links" class="info-button-float"><ons-icon ng-attr-icon="{{link.icon}}"></ons-icon><div>{{ link.label }}</div></ons-speed-dial-item></ons-speed-dial>',
            controller: ['$scope', function HelpButtonController($scope) {
                $scope.links = [
                    {icon: 'ion-person', label: 'Inspector User\'s Guide', link: 'http://www.unitspro.com/Downloads/Demo%20App/UnitsPro%20Android%20App%20Inspector%20User\'s%20Guide.pdf'},
                    {icon: 'ion-person', label: 'Project Manager User\'s Guide', link: 'http://www.unitspro.com/Downloads/Demo%20App/UnitsPro%20Android%20App%20PM%20User\'s%20Guide.pdf'},
                    {icon: 'ion-archive', label: 'Download Guide', link: 'http://www.unitspro.com/Downloads/Demo%20App/UnitsPro%20Android%20App%20Download%20Guide.pdf'},
                    {icon: 'ion-information', label: 'Help / Contact'}
                ];
                $scope.helpLink = function($index, link, $event){
                    switch($index){
                        case 3:
                            mainNav.pushPage('app/sales-info.html');
                        break;
                        default:
                            open(link.link);
                        break;
                    }
                    ons.findParentComponentUntil('ons-speed-dial',$event.target).toggleItems();
                }
                var open = function(url){
                    cordova.InAppBrowser.open(url, '_system');
                };
            }]
        };
    })
    .directive("clockpicker", function () {
        return {
            restrict: "EA",
            replace: true,
            templateUrl: "template/clockpicker.html",
            scope: {
                datetime: "=ngModel",
                selectionMode: "=clockMode"
            },
            controller: function ($scope) {
                $scope.hourOptions = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
                $scope.minuteOptions = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'];
                $scope.periodOptions = ['AM', 'PM'];
                $scope.selectionMode = true;

                var init = function(){
                    if(typeof $scope.datetime != 'date'){
                        $scope.datetime = new Date();
                        $scope.datetime.setHours(0);
                        $scope.datetime.setMinutes(0);
                        $scope.datetime.setSeconds(0);
                    }
                }
                init();

                var addZero = function(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }

                var toggleOnSelection = true, okMinutes = ['00','15','30','45'];
                var currentIndex = function () {
                    if ($scope.selectionMode) {
                        for (var i = 0; i < $scope.hourOptions.length; i++) {
                            if ($scope.hourOptions[i] == ($scope.datetime.getHours()%12||12)) return i;
                        }
                    }
                    else {
                        for (var j = 0; j < $scope.hourOptions.length; j++) {
                            if ($scope.minuteOptions[j] == addZero($scope.datetime.getMinutes())) return j;
                        }
                    }
                };
                $scope.selectValue = function (value) {
                    if($scope.selectionMode){
                        var hours = $scope.datetime.getHours();
                        var pm = hours >= 12;
                        $scope.datetime.setHours(pm ? value==12?12:(value + 12) : value==12?0:value);
                      }else if(okMinutes.indexOf(value) > -1){
                        $scope.datetime.setMinutes(value);
                      }else{
                        return;
                      }
                    if (toggleOnSelection) {
                        $scope.selectionMode = !$scope.selectionMode;
                        toggleOnSelection = false;
                    }
                };
                $scope.selectPeriod = function (value) {
                    var hours = $scope.datetime.getHours();
                    var setPm = value=='PM', isPm = hours >= 12;
                    if(setPm!=isPm){
                        $scope.togglePeriod();
                    }
                };
                $scope.togglePeriod = function () {
                    var hours = $scope.datetime.getHours();
                    $scope.datetime.setHours(hours>12?hours-12:hours+12);
                };
                $scope.lineStyle = function () {
                    var angle = "rotate(" + (currentIndex() * 30 - 180) + "deg)";
                    return "transform: " + angle + "; -webkit-transform: " + angle;
                };
                $scope.$watch("selectionMode", function (value) {
                    $scope.options = value ? $scope.hourOptions : $scope.minuteOptions;
                    toggleOnSelection = value;
                });
            }
        };
    })
    .directive('uproTemplate', [function() {
        return {
           restrict: 'E',
           require: 'ngInclude',
           link: function (scope, element, attrs) {
               if(element.children)element.replaceWith(element.children());
           }
        };
    }])
    .directive('ghost', [function() {
        return {
            restrict: 'A',
            link: function(scope, element, attributes) {
                scope.$watch(attributes.ghost, function(value){
                    element.css('visibility', value ? 'hidden' : 'visible');
                });
            }
      };
    }])
    .directive('onsClass', [function(){
        return {
            restrict: 'A',
            priority: 1001, //before ng-repeat
            compile: function(element, attributes){
                element.addClass(attributes.onsClass);
                var classes = [attributes.onsClass];
                if(ons.platform.isAndroid()){
                    element.addClass(attributes.onsClass+'--material');
                    classes.push(attributes.onsClass+'--material');
                }
                if(attributes.onsModifier){
                    var classes = attributes.onsModifier.split(' ');
                    classes.forEach(function(clss){ element.addClass(attributes.onsClass+'--'+clss); 
                    classes.push(attributes.onsClass+'--'+clss); })
                }
                attributes.class += ' '+classes.join(' ');
            }
        }
    }])
    .directive('hourEntry', ['$compile', '$timeout', function($compile, $timeout){
        var attachToInput = function(tries, scope, element, attributes){
            var input = element.find('input');
            if(!input.length){
                if(tries < 10){
                    $timeout(function(){attachToInput(tries+1,scope, element, attributes)}, 5)
                }
                return;
            }
            input.attr('pattern', '^-?(?:[0-9]+|[0-9]*\.[0-9]+)$');
            input.attr('inputmode', 'numeric');
            input.attr('autocapitalize', 'words');//This fixes the weird samsung no decimal issue
            input.attr('formatted-number-input', attributes.hourEntry);
            input.attr('ng-model', attributes.ngModel);
            input.attr('on-change', attributes.onChange)
            $compile(input)(scope);
        }
        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                $timeout(function(){attachToInput(0,scope, element, attributes)})
            }
        }
    }])
    .directive('numberInput', ['$timeout', function($timeout){
        var attachToInput = function(tries, scope, element, attributes){
            var input = element.find('input');
            if(!input.length){
                if(tries < 10){
                    $timeout(function(){attachToInput(tries+1,scope, element, attributes)}, 5)
                }
                return;
            }
            input.attr('pattern', '^-?(?:[0-9]+|[0-9]*\.[0-9]+)$');
            input.attr('inputmode', 'numeric');
            input.attr('autocapitalize', 'words');//This fixes the weird samsung no decimal issue
        }
        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                $timeout(function(){attachToInput(0,scope, element, attributes)})
            }
        }
    }])
    .directive('inputHelper', ['$timeout', '$compile', function($timeout, $compile){
        var attachToInput = function(tries, scope, element, attributes){
            var input = element.find('input');
            if(!input.length){
                if(tries < 10){
                    $timeout(function(){attachToInput(tries+1,scope, element, attributes)}, 5)
                }
                return;
            }
            var keys = Object.keys(attributes);
            var compile = false
            for(var i=0; i<keys.length; ++i){
                if(keys[i].length > 6 && keys[i].substr(0,6) == 'helper'){
                    input.attr(keys[i].substr(6).split(/(?=[A-Z])/).join('-').toLowerCase(), attributes[keys[i]]);
                    compile = true;
                }
            }
            $compile(input)(scope);
        }
        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                $timeout(function(){attachToInput(0,scope, element, attributes)})
            }
        }
    }])
    .directive('checkboxDisabled', ['$compile', function($compile){
        return {
          restrict: 'E', 
          scope: {
            checked: '=',
          },
          template: '<span class="checkbox-disabled-wrapper"><ons-checkbox ng-model="checked" disabled></ons-checkbox></span>',
          replace: true,
          link: function($scope, elem, attr, ctrl) {

          }
        };
    }]);

    var checkOverflowX = function(el){
       var curOverflow = el.style.overflowX;
       if ( !curOverflow || curOverflow === "visible" )
          el.style.overflowX = "hidden";
       var isOverflowing = el.clientWidth < el.scrollWidth;
       el.style.overflowX = curOverflow;
       return isOverflowing;
    };

    var checkTabbarWidth = function(styles, tb){
        var inner = tb.getElementsByClassName('tabbar'), border = tb.getElementsByClassName('tabbar__border')
        if(inner.length){
            for(var i=0; i<tb.tabs.length; i++){
                tb.tabs[i].setAttribute('style', styles.tab);
            }
            var tabsInner = inner[0].getElementsByClassName('tabbar__label');
            for(var i=0; i<tabsInner.length; i++){
                tabsInner[i].setAttribute('style', styles.tabInner);
            }
            inner[0].setAttribute('style', styles.tabbar);
            if(tb._tabbarBorder)tb._tabbarBorder.style.display = 'none';
            if(!checkOverflowX(inner[0])){
                inner[0].setAttribute('style', '');
                for(var i=0; i<tabsInner.length; i++){
                    tabsInner[i].setAttribute('style', '');
                }
                for(var i=0; i<tb.tabs.length; i++){
                    tb.tabs[i].setAttribute('style', '');
                }
            }
            if(tb._tabbarBorder)tb._tabbarBorder.style.display = 'block';
            tb._onRefresh()
            tb._onScroll(tb.getActiveTabIndex())
        }
    };
    app.directive('dynamicTabbar', ['$timeout', function($timeout){
        return {
          restrict: 'A', 
          link: function($scope, elem, attr, ctrl) {
            var styles = {
                tabbar: 'display: block; overflow-x: auto !important; overflow-y: hidden !important;',
                tab: 'display: inline-block; box-sizing: border-box;',
                tabInner: 'padding: 0px 10px;'
            }
            var connect = {
                refresh: function(){
                    checkTabbarWidth(styles, elem[0]);
                },
                element: elem[0]
            }
            $scope.$parent[attr.dynamicTabbar] = connect
            var change = function(){
                $timeout(connect.refresh,0);
            }
            ons.orientation.on('change', change);
            elem.on('$destroy', function() {
                ons.orientation.off('change', change);
            });
          }
        };
    }])
    .directive('scrollButtons', ['$timeout', function($timeout){
        function findAncestorWithClass (el, cls) {
            while ((el = el.parentElement) && !el.classList.contains(cls));
            return el;
        }
        return {
          restrict: 'A', 
          link: function($scope, elem, attr, ctrl) {
            var container = findAncestorWithClass(elem[0], 'scoll-button-container')
            var up = angular.element(container.getElementsByClassName(attr.scrollButtons+'-up')),
                down = angular.element(container.getElementsByClassName(attr.scrollButtons+'-down'));
            var scroll = function(){
                if(elem[0].scrollTop <= 0){
                    up.attr('disabled','disabled')
                }else{
                    up.removeAttr('disabled')
                }
                if(elem[0].scrollTop >= elem[0].scrollHeight-elem[0].clientHeight){
                    down.attr('disabled','disabled')
                }else{
                    down.removeAttr('disabled')
                }
            }
            elem.bind('scroll', scroll)
            ons.orientation.on('change', scroll);
            $scope.$watch(function () { return elem[0].childNodes.length; },
                function (newValue, oldValue) {
                  if (newValue !== oldValue) {
                    $timeout(scroll)
                  }
                }
              );
          }
        };
      }])
    .directive('onsDateInput', ['$compile', '$timeout', function($compile, $timeout){
        var attachToInput = function(tries, scope, element, attributes){
            var input = element.find('input');
            if(!input.length){
                if(tries < 10){
                    $timeout(function(){attachToInput(tries+1,scope, element, attributes)}, 5)
                }
                return;
            }
            //element.removeAttr('ng-model');
            //input.attr('date-input', '');
            //input.attr('ng-model', attributes.ngModel);
            input.attr('on-change', attributes.onChange)

            $compile(input)(scope);
        }
        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                $timeout(function(){attachToInput(0,scope, element, attributes)})
            }
        }
    }])
    .directive('dateInput', ['strToDate', '$timeout', function(strToDate, $timeout) {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          var isInTable = (element[0].parentNode.tagName=='TD'||element[0].parentNode.parentNode.tagName=='TD')
          ngModel.$parsers.unshift(function (value) {
            var modelValue, date;
            if (date = strToDate(value)) {
                modelValue = date;
                modelValue.clearTime()
                ngModel.$setValidity('textDate', true);
            } 
            if(modelValue == null){
              ngModel.$setValidity('textDate', false);
            }
            return modelValue;
          });

          ngModel.$formatters.push(function (value) {
            var formatted = '';
            if (angular.isDate(value)) {
              var jsonFormat = value.toJSON()
              if(jsonFormat){
                var tokens = jsonFormat.split(/[-T]/)
                formatted = tokens[1]+'/'+tokens[2]+'/'+tokens[0]
              }
            }
            return formatted;
          });
          var unregister = scope.$watch('job.currentDateFilter', function(){
              if(attrs.onChange){
                 $timeout(function(){scope.$eval(attrs.onChange)});
              }
          })
          scope.$on('$destroy', function(){
              unregister()
          })
        }
      }
    }])
    app.directive('clickSelect', [function() {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          var valueAttr = element[0].tagName =='INPUT'?'value':'innerText';
          var isInTable = (element[0].parentNode.tagName=='TD'||element[0].parentNode.parentNode.tagName=='TD')
          var focused = false,
              focusEventFired = false,
              keyPressed = false,
              focusedVal;

          var selectText = function(){
              if(valueAttr == 'value'){
                  element[0].setSelectionRange(0, element[0].value.length)
              }else{
                var range = document.createRange();
                range.selectNodeContents(element[0]);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range); 
            }            
          }
          var blurEvent = function(e) {
            focused = focusEventFired = false;
            e.preventDefault();
            var viewValue = ngModel.$modelValue;
            for (var i=ngModel.$formatters.length-1; i>=0; --i) {
                viewValue = ngModel.$formatters[i](viewValue);
            }
            ngModel.$viewValue = viewValue;
            ngModel.$render();
            if(focusedVal !== ngModel.$modelValue || keyPressed){
              scope.$apply(function() {
                  scope.$eval(attrs.onChange);
              });
            }
          }, focusEvent = function(e){
            if(!focusEventFired){
              focusedVal = ngModel.$modelValue;
              scope.$parent.$previous = focusedVal;
              keyPressed = false;
              if(isInTable)updateTableHighlight(element);
            }
            focusEventFired = true;
          }, clickEvent = function() {
            if (!focused) {
                selectText()
            }
            focused = true;
          }, keyPressEvent = function(e){
            if(!focusEventFired){
              focusEvent();
            }
          };
          element.on('blur', blurEvent);
          element.on('focus', focusEvent);
          element.on('click', clickEvent);
          element.on('keydown', keyPressEvent);
          scope.$on('$destroy', function(){
            element.off('blur', blurEvent);
            element.off('focus', focusEvent);
            element.off('click', clickEvent);
            element.off('keydown', keyPressEvent);
          })
        }
      }
    }])
    .directive('contenteditable', [function() {
      return {
        restrict: 'E', // only activate on element
        require: '?ngModel', // get a hold of NgModelController
        scope: {
            'readonly':'='
        },
        link: function(scope, element, attrs, ngModel) {
          if(typeof attrs.contenteditable == 'undefined'){
              element.attr('contenteditable', true)
          }
          if (!ngModel) return; // do nothing if no ng-model
         
          // Specify how UI should be updated
          ngModel.$render = function() {
            element.text(ngModel.$viewValue);
          };
          // Listen for change events to enable binding
          element.on('blur keyup change', function() {
            scope.$evalAsync(read);
          });
          scope.$watch('readonly', function(newValue, oldValue) {
            element.attr('contenteditable', !newValue);
          });

          //Initial set-up
          ngModel.$modelValue = scope.$eval(attrs.ngModel);
          ngModel.$setViewValue(ngModel.$modelValue);
          ngModel.$render()

          // Write data to the model
          function read() {
            var text = element.text().trim();
            ngModel.$setViewValue(text);
          }
        }
      };
    }])
    .directive('formattedNumberInput', ['$filter', '$timeout', 'Utils', '$parse', function($filter, $timeout, Utils, $parse){
        var numericKeys = ['0','1','2','3','4','5','6','7','8','9','.'];
        
        return {
            require: 'ngModel',
            scope: {
                ngModel: '='
            },
            link: function(scope, element, attrs, ngModel) {
                var options = {
                    decimals: -1,
                    prefix: '',
                    suffix: '',
                    round: 0,
                    formatter: null,
                    parser: null,
                    groupCells: null,
                    focus: true,
                    focusClick: true
                }, prefixRegex = null, suffixRegex = null, focused = false, focusEventFired = false, keyPressed = false, focusedVal, isInTable = (element[0].parentNode.tagName=='TD'||element[0].parentNode.parentNode.tagName=='TD');
                var formatData = function(value){
                   return options.prefix+$filter('number')(value||0, options.decimals)+options.suffix;
                }
                var initGroupCells = function(attrString){
                    if(!attrString) return;
                    options.groupCells = looseJsonParse(attrString);
                }
                var parseOptions = function(attrString){
                    if(!attrString) return;
                    var opts = looseJsonParse(attrString);
                    var keys = Object.keys(opts);
                    for(var i=0; i<keys.length; i++){
                        var opt = opts[keys[i]];
                        if(typeof options[keys[i]] != 'undefined'){
                            if(keys[i] == 'decimals' || keys[i] == 'round'){
                                options[keys[i]] = opt;
                                if(keys[i] == 'decimals' && !keys.includes('round')){
                                    options.round = Math.pow(10, opt);
                                }
                            }else if(keys[i] == 'prefix'){
                                options[keys[i]] = opt;
                                prefixRegex = new RegExp('^\\s*'+Utils.sanitizeTermForRegex(opt)+'\\s*', 'i')
                            }else if(keys[i] == 'suffix'){
                              options[keys[i]] = opt;
                              suffixRegex = new RegExp('^\\s*'+Utils.sanitizeTermForRegex(opt)+'\\s*', 'i')
                            }else if(keys[i] == 'formatter' || keys[i] == 'parser'){
                                options[keys[i]] = function(){
                                    var handler =  $parse(opt);
                                    return function(value){ return handler(scope.$parent, {value:(typeof value=='undefined')?'':value}) };
                                }();
                            }else{
                                options[keys[i]] = opt;
                            }
                        }
                    }
                }
                var transformValue = function(value){
                    if(value !== ''){
                        //value = Math.abs(value);
                        if(options.round > 0){
                            value = Math.round(value*options.round)/options.round;
                        }
                    }
                    return value;
                }
                var update = function(){
                    var originalValue = (typeof ngModel.$$rawModelValue=='undefined'?'':ngModel.$$rawModelValue),
                        newValue = transformValue(options.parser?options.parser(originalValue):originalValue);
                    if(originalValue.toString() != newValue.toString()){
                        ngModel.$modelValue = newValue;
                        scope.ngModel = newValue;
                        scope.$apply();
                    }
                    ngModel.$viewValue = options.formatter?options.formatter(newValue):formatData(newValue.toString());
                    ngModel.$render();
                }
                parseOptions(attrs.formattedNumberInput||'');
                initGroupCells(attrs.groupCells||'');
                if(!options.parser){
                    options.parser = function(value){
                      var val = (value===0?0:(value||'')).toString();
                      if(options.prefix) val = val.replace(prefixRegex,'')
                      if(options.suffix) val = val.replace(suffixRegex,'')
                      val = parseFloat(val.replace(',',''));
                      return isNaN(val)?'':(typeof val=='undefined')?0:val;
                    };
                }
                ngModel.$parsers.push(options.parser);
                ngModel.$formatters.unshift(options.formatter?options.formatter:formatData);
                var blurEvent = function(e) {
                    focused = focusEventFired = false;
                    update();
                    if(e)e.preventDefault();
                    if(isInTable)updateTableHighlight(element, "blur", options.groupCells);
                    if(focusedVal !== ngModel.$modelValue || keyPressed){
                        scope.$apply(function() {
                            scope.$parent.$eval(attrs.onChange);
                        });
                    }
                }, clickEvent = function() {
                    if (!focused && options.focus) {
                        this.setSelectionRange(0, this.value.length)
                    }
                    focused = true;
                }, focusEvent = function(e){
                    if(!focusEventFired){
                        focusedVal = ngModel.$modelValue;
                        scope.$parent.$previous = focusedVal;
                        if(options.focusClick)clickEvent.apply(this);
                    }
                    if(isInTable)updateTableHighlight(element, "focus", options.groupCells);
                    focusEventFired = true;
                };
                element.on('blur', blurEvent);
                element.on('click', clickEvent);
                element.on('focus', focusEvent);
                scope.$on('$destroy', function() {
                    element.off('blur', blurEvent);
                    element.off('click', clickEvent);
                    element.off('focus', focusEvent);
                });
            }
        };
    }])
    .directive('watchChange', ['$parse', '$timeout', function($parse, $timeout){
          return {
            link: function(scope, element, attrs) {
              var handler =  $parse(attrs.watchChange);
              var callback = function(value){ return handler(scope.$parent, {value:(typeof value=='undefined')?'':value}) };
              scope.$watch(attrs.watchModel, callback);
            }
          };
      }])
    .directive('infiniteScroll', [
      '$rootScope', '$window', '$interval', function($rootScope, $window, $interval) {
          var THROTTLE_MILLISECONDS = 250;
        return {
          scope: {
            infiniteScroll: '&',
            infiniteScrollContainer: '=',
            infiniteScrollDistance: '=',
            infiniteScrollDisabled: '=',
            infiniteScrollUseDocumentBottom: '=',
            infiniteScrollListenForEvent: '@'
          },
          link: function(scope, elem, attrs) {
            var changeContainer, checkInterval, checkWhenEnabled, container, handleInfiniteScrollContainer, handleInfiniteScrollDisabled, handleInfiniteScrollDistance, handleInfiniteScrollUseDocumentBottom, handler, height, immediateCheck, offsetTop, pageYOffset, scrollDistance, scrollEnabled, throttle, unregisterEventListener, useDocumentBottom, windowElement;
            windowElement = angular.element($window);
            scrollDistance = null;
            scrollEnabled = null;
            checkWhenEnabled = null;
            container = null;
            immediateCheck = true;
            useDocumentBottom = false;
            unregisterEventListener = null;
            checkInterval = false;
            height = function(elem) {
              elem = elem[0] || elem;
              if (isNaN(elem.offsetHeight)) {
                return elem.document.documentElement.clientHeight;
              } else {
                return elem.offsetHeight;
              }
            };
            offsetTop = function(elem) {
              if (!elem[0].getBoundingClientRect || elem.css('none')) {
                return;
              }
              return elem[0].getBoundingClientRect().top + pageYOffset(elem);
            };
            pageYOffset = function(elem) {
              elem = elem[0] || elem;
              if (isNaN(window.pageYOffset)) {
                return elem.document.documentElement.scrollTop;
              } else {
                return elem.ownerDocument.defaultView.pageYOffset;
              }
            };
            handler = function() {
              var containerBottom, containerTopOffset, elementBottom, remaining, shouldScroll;
              if (container === windowElement) {
                containerBottom = height(container) + pageYOffset(container[0].document.documentElement);
                elementBottom = offsetTop(elem) + height(elem);
              } else {
                containerBottom = height(container);
                containerTopOffset = 0;
                if (offsetTop(container) !== void 0) {
                  containerTopOffset = offsetTop(container);
                }
                elementBottom = offsetTop(elem) - containerTopOffset + height(elem);
              }
              if (useDocumentBottom) {
                elementBottom = height((elem[0].ownerDocument || elem[0].document).documentElement);
              }
              remaining = elementBottom - containerBottom;
              shouldScroll = remaining <= height(container) * scrollDistance + 1;
              if (shouldScroll) {
                checkWhenEnabled = true;
                if (scrollEnabled) {
                  if (scope.$$phase || $rootScope.$$phase) {
                    return scope.infiniteScroll();
                  } else {
                    return scope.$apply(scope.infiniteScroll);
                  }
                }
              } else {
                if (checkInterval) {
                  $interval.cancel(checkInterval);
                }
                return checkWhenEnabled = false;
              }
            };
            throttle = function(func, wait) {
              var later, previous, timeout;
              timeout = null;
              previous = 0;
              later = function() {
                previous = new Date().getTime();
                $interval.cancel(timeout);
                timeout = null;
                return func.call();
              };
              return function() {
                var now, remaining;
                now = new Date().getTime();
                remaining = wait - (now - previous);
                if (remaining <= 0) {
                  $interval.cancel(timeout);
                  timeout = null;
                  previous = now;
                  return func.call();
                } else {
                  if (!timeout) {
                    return timeout = $interval(later, remaining, 1);
                  }
                }
              };
            };
            if (THROTTLE_MILLISECONDS != null) {
              handler = throttle(handler, THROTTLE_MILLISECONDS);
            }
            scope.$on('$destroy', function() {
              container.unbind('scroll', handler);
              if (unregisterEventListener != null) {
                unregisterEventListener();
                unregisterEventListener = null;
              }
              if (checkInterval) {
                return $interval.cancel(checkInterval);
              }
            });
            handleInfiniteScrollDistance = function(v) {
              return scrollDistance = parseFloat(v) || 0;
            };
            scope.$watch('infiniteScrollDistance', handleInfiniteScrollDistance);
            handleInfiniteScrollDistance(scope.infiniteScrollDistance);
            handleInfiniteScrollDisabled = function(v) {
              scrollEnabled = !v;
              if (scrollEnabled && checkWhenEnabled) {
                checkWhenEnabled = false;
                return handler();
              }
            };
            scope.$watch('infiniteScrollDisabled', handleInfiniteScrollDisabled);
            handleInfiniteScrollDisabled(scope.infiniteScrollDisabled);
            handleInfiniteScrollUseDocumentBottom = function(v) {
              return useDocumentBottom = v;
            };
            scope.$watch('infiniteScrollUseDocumentBottom', handleInfiniteScrollUseDocumentBottom);
            handleInfiniteScrollUseDocumentBottom(scope.infiniteScrollUseDocumentBottom);
            changeContainer = function(newContainer) {
              if (container != null) {
                container.unbind('scroll', handler);
              }
              container = newContainer;
              if (newContainer != null) {
                return container.bind('scroll', handler);
              }
            };
            changeContainer(windowElement);
            if (scope.infiniteScrollListenForEvent) {
              unregisterEventListener = $rootScope.$on(scope.infiniteScrollListenForEvent, handler);
            }
            handleInfiniteScrollContainer = function(newContainer) {
              if ((newContainer == null) || newContainer.length === 0) {
                return;
              }
              if (newContainer.nodeType && newContainer.nodeType === 1) {
                newContainer = angular.element(newContainer);
              } else if (typeof newContainer.append === 'function') {
                newContainer = angular.element(newContainer[newContainer.length - 1]);
              } else if (typeof newContainer === 'string') {
                newContainer = angular.element(document.querySelector(newContainer));
              }
              if (newContainer != null) {
                return changeContainer(newContainer);
              } else {
                throw new Error("invalid infinite-scroll-container attribute.");
              }
            };
            scope.$watch('infiniteScrollContainer', handleInfiniteScrollContainer);
            handleInfiniteScrollContainer(scope.infiniteScrollContainer || []);
            if (attrs.infiniteScrollParent != null) {
              changeContainer(angular.element(elem.parent()));
            }
            if (attrs.infiniteScrollImmediateCheck != null) {
              immediateCheck = scope.$eval(attrs.infiniteScrollImmediateCheck);
            }
            return checkInterval = $interval((function() {
              if (immediateCheck) {
                handler();
              }
              return $interval.cancel(checkInterval);
            }));
          }
        };
      }
    ])
    .filter('time', ['TimeEntryService', function(TimeEntryService){
        return function(input){
            return TimeEntryService.formatTime(input);
        }
    }])
    app.directive('inputEvent', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.on('input', function(event) {
					scope.$apply(function() {
						scope.$eval(attrs.inputEvent, { $event: event });
					});
				});
			}
		};
	});
    app.filter('propsFilter', function() {
        return function(items, props) {
          var out = [];
          if (angular.isArray(items)) {
            var keys = Object.keys(props);
            for(var j=0; j<items.length; ++j){
              var itemMatches = true, itemHasAMatch = false, item = items[j], or = !!props.or;

              for (var i = 0; i < keys.length; i++) {
                var prop = keys[i];
              if(prop != 'or'){
                  if(typeof item[prop] == 'undefined') continue;
                  var text = props[prop]||'', itemText = item[prop].toString().toLowerCase(), exact = false;
                  if(typeof props[prop] == 'object'){
                      text = props[prop].prop||'';
                      exact = props[prop].exact;
                  }
                  text = text.toString().toLowerCase();
                  if(exact){
                      if(text != itemText){
                          itemMatches = false;
                          if(!or)break;
                      }else if(or){
                    itemHasAMatch = true;
                  }
                  }else if(text && itemText.indexOf(text) == -1){
                    itemMatches = false;
                      if(!or)break;
                  }else if(or){
                  itemHasAMatch = true;
                }
              }
              }

              if (itemMatches||itemHasAMatch) {
                out.push(item);
              }
            }
          } else {
            // Let the output be the input untouched
            out = items;
          }

          return out;
        };
      })
    .run(["$templateCache", function ($templateCache) {
        $templateCache.put("template/clockpicker.html",
            "\n" +
            "<div class='ui-clockpicker'>\n" +
            "  <div class='ui-clockpicker-selection'>\n" +
            "    <a ng-click='selectionMode = true' ng-class='{selected: selectionMode}'>{{datetime | date:'h'}}</a>:" +
            "<a ng-click='selectionMode = false' ng-class='{selected: !selectionMode}'>{{datetime | date:'mm'}}</a> " +
            "<a ng-click='togglePeriod()'>{{datetime | date:'a'}}</a>\n" +
            "  </div>\n" +
            "  <div class='ui-clockpicker-selector' ng-class='{minute: !selectionMode}'>\n" +
            "     <div class='ui-clockpicker-line' style='{{lineStyle()}}'></div>" +
            "     <ol class='ui-clockpicker-time'>\n" +
            "       <li ng-repeat='option in options' " +
            "         ng-class=\"{selected: selectionMode ? (datetime | date:'h') == option : (datetime | date:'mm') == option }\" " +
            "         ng-click='selectValue(option)'>{{option}}</li>\n" +
            "     </ol>\n" +
            "     <ol class='ui-clockpicker-period'>\n" +
            "       <li ng-repeat='periodOption in periodOptions' " +
            "         ng-class=\"{selected: (datetime  | date:'a') == periodOption }\" " +
            "         ng-click='selectPeriod(periodOption)'>{{periodOption}}</li>\n" +
            "     </ol>\n" +
            "  </div>\n" +
            "</div>\n" +
            "");
    }]);

    ons.ready(function(){
        if(ons.platform.isIOS()){
            universalLinks.subscribe(null, function(event){
                var ref;
                if(event.url && (ref = event.url.match(/https?:\/\/portal.unitspro.com\/reset-password\?t=(.+)/i))){
                    mainNav.resetToPage('app/login/reset-password.html', { animation : 'fade', data: {token: event.params.t} });
                }
            })
            mainNav.resetToPage('app/login/login.html');
        }
        if(ons.platform.isAndroid()){
            if(window.plugins&&window.plugins.webintent){
                window.plugins.webintent.getUri(function(url) {
                    var ref;
                    if(url && (ref = url.match(/https?:\/\/portal.unitspro.com\/reset-password\?t=(.+)/i))){
                        mainNav.resetToPage('app/login/reset-password.html', { animation : 'fade', data: {token: ref[1]} });
                        return;
                    }
                    mainNav.resetToPage('app/login/login.html');
                });
            }
        }
    });

  app.factory('$exceptionHandler', ['$injector', function($injector) {
    var ApiService;
    return function(exception) {
        ApiService = ApiService||$injector.get('ApiService');//avoid circular dependency
        var route = {route:'error'};
        ApiService.post('log', exception.stack||exception.message, null, null, null, route);
    };
  }]);
    
})();
(function(){
    'use strict';

    angular.module('app').controller('HotelsController', ['$scope', '$timeout', 'ApiService', 'Utils', 'Vendors', 'Smartpick', 'UploadDialog', 'ImgOrPdf', 'StorageService', function($scope, $timeout, ApiService, Utils, Vendors, Smartpick, UploadDialog, ImgOrPdf, StorageService) {
        $scope.working = true;
        $scope.job = $scope.$parent.current.job;
        $scope.data = {}
        $scope.editing = true;
        $scope.loading = false
        $scope.pixelWidth = 1;///window.devicePixelRatio;
        $scope.currentHotel = null;
        $scope.myId = StorageService.get('MyPersonID');
        var dialogs = {};
        
        $scope.getTempID = function() {
            if(!$scope.job.lastTemp) $scope.job.lastTemp = 0;
            ++$scope.job.lastTemp;
            return '__temp'+$scope.job.lastTemp.toString(16)+'__';
        };
        $scope.isTempID = function(id){
            return Utils.isTempId(id);
        }

        $scope.inMyCrew = function(id){
            if($scope.myCrew){
                if($scope.myCrew.ForemanID == id) return true;
                for(var m=0; m<$scope.myCrew.JobCrewMember.length; ++m){
                    if($scope.myCrew.JobCrewMember[m].PersonID == id) return true;
                }
                return false;
            }
            return true;
        }
        var cleanJob = function(job){
            job.JobCrew = Utils.enforceArray(job.JobCrew)
            $scope.crew = []
            $scope.myCrew = null;
            $scope.myCrewCount = 0
            var crewIDs = {}
            for(var c=0; c<job.JobCrew.length; ++c){
                job.JobCrew[c].JobCrewMember = Utils.enforceArray(job.JobCrew[c].JobCrewMember)
                if(!crewIDs[job.JobCrew[c].ForemanID]){
                    crewIDs[job.JobCrew[c].ForemanID] = true;
                    $scope.crew.push({
                        ID: job.JobCrew[c].ForemanID,
                        Name: job.JobCrew[c].ForemanFullName
                    })
                    if(job.JobCrew[c].ForemanID == $scope.myId){
                        $scope.crew[$scope.crew.length-1].isMyCrew = true;
                        $scope.myCrew = job.JobCrew[c];
                        $scope.myCrewCount = 1 + job.JobCrew[c].JobCrewMember.length;
                    }
                }
            }
            for(var c=0; c<job.JobCrew.length; ++c){
                if(!$scope.myCrew || job.JobCrew[c].ForemanID != $scope.myId){
                    var index = $scope.crew.findIndex(function(cr){ return cr.ID == job.JobCrew[c].ForemanID})
                    if(index > -1){
                        $scope.crew[index].isMyCrew = $scope.myCrew?($scope.crew[index].isMyCrew||false):true;
                    }
                }
                for(var m=0; m<job.JobCrew[c].JobCrewMember.length; ++m){
                    if(!crewIDs[job.JobCrew[c].JobCrewMember[m].PersonID]){
                        crewIDs[job.JobCrew[c].JobCrewMember[m].PersonID] = true;
                        $scope.crew.push({
                            ID: job.JobCrew[c].JobCrewMember[m].PersonID,
                            Name: job.JobCrew[c].JobCrewMember[m].PersonFullName,
                            isMyCrew: $scope.inMyCrew(job.JobCrew[c].JobCrewMember[m].PersonID)
                        })
                    }else{
                        var index = $scope.crew.findIndex(function(cr){ return cr.ID == job.JobCrew[c].JobCrewMember[m].PersonID})
                        if(index > -1){
                            $scope.crew[index].isMyCrew = $scope.inMyCrew(job.JobCrew[c].JobCrewMember[m].PersonID);
                        }
                    }
                }
            }
            $scope.crewCount = $scope.crew.length;
            job.JobDay = Utils.enforceArray(job.JobDay)
            for(var i=0; i<job.JobDay.length; ++i){
                var day = job.JobDay[i];
                day.JobDate = Utils.date(day.JobDate);
                var date = Utils.date(day.JobDate,'MM/dd/yyyy')
                day.JobDayHotel = Utils.enforceArray(day.JobDayHotel)
                day.HotelExclusion = Utils.enforceArray(day.HotelExclusion)
                day.TotalRoomedPeople = 0
                day.occupied = day.JobDayHotel.length>0;
                day.UnhandledPeople = $scope.crewCount;
                day.UnhandledMyPeople = $scope.myCrew?$scope.myCrewCount:$scope.crewCount;
                for(var h=0; h<day.JobDayHotel.length; ++h){
                    var hotel = day.JobDayHotel[h];
                    hotel.active = true;
                    hotel.HotelRoom = Utils.enforceArray(hotel.HotelRoom)
                    for(var r=0; r<hotel.HotelRoom.length; ++r){
                        var room = hotel.HotelRoom[r];
                        room.active = true;
                        room.HotelRoomOccupant = Utils.enforceArray(room.HotelRoomOccupant)
                        day.TotalRoomedPeople += room.HotelRoomOccupant.length;
                        for(var o=0; o<room.HotelRoomOccupant.length; ++o){
                            var occupant = room.HotelRoomOccupant[o];
                            occupant.active = true;
                            for(var c=0; c<$scope.crew.length; ++c){
                                if($scope.crew[c].ID == occupant.PersonID){
                                    occupant.isMyCrew = $scope.crew[c].isMyCrew;
                                    $scope.crew[c].rooms = $scope.crew[c].rooms||{}
                                    $scope.crew[c].rooms[date] = {hotel:hotel,room:room,occupant:occupant}
                                    day.UnhandledPeople -= 1;
                                    if(occupant.isMyCrew){
                                        day.UnhandledMyPeople -= 1;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                for(var e=0; e<day.HotelExclusion.length; ++e){
                    var exclusion = day.HotelExclusion[e];
                    exclusion.active = true;
                    for(var c=0; c<$scope.crew.length; ++c){
                        if($scope.crew[c].ID == exclusion.PersonID){
                            day.UnhandledPeople -= 1;
                            if($scope.crew[c].isMyCrew){
                                day.UnhandledMyPeople -= 1;
                            }
                            break;
                        }
                    }
                }
            }
           
            $scope.job = job;
            initDates();
        }
        var result = function(resp){
            if(resp){
                if(resp.data && !resp.data.success && resp.data.message){
                    Utils.notification.alert(resp.data.message, {title: 'Error'});
                    mainNav.popPage();
                }else{
                    if(resp.Vendor){
                        $scope.data.Vendor = Utils.enforceArray(resp.Vendor);
                    }
                    if(resp.Job){
                        cleanJob(resp.Job)
                    }
                }
            }
            $scope.working = false;
            $scope.loading = false;
        }
        var load = function(){
            var obj = {
                route: 'init'
            };
            if($scope.job){
                obj.job = $scope.job.JobID;
            }
            ApiService.get('hotels', obj).then(result, result);
        };

        $scope.dayOfWeek = ['S','M','T','W','T','F','S']
        var initDates = function(){
            $scope.dateTabs = $scope.job.JobDay.slice(0,7)
            $scope.calendar = {
                startDay: $scope.dateTabs[0].JobDate.getDay(),
                today: new Date()
            }
            Utils.clearTimeFromDate($scope.calendar.today);
            setUpDates();
        }
        var setUpDates = function(today){
            today = today||$scope.calendar.today;
            setCurrentDate($scope.dateTabs.find(function(jd){ return jd.JobDate.getTime() == today.getTime() }))
            var nextMonth = new Date($scope.dateTabs[0].JobDate.getFullYear(), $scope.dateTabs[0].JobDate.getMonth()+1, 1);
            $scope.calendar.month1Span = 7
            $scope.calendar.month2Span = 0
            if(nextMonth <= $scope.dateTabs[6].JobDate){
                $scope.calendar.month1Span = Utils.daysBetween($scope.dateTabs[0].JobDate, nextMonth)
                $scope.calendar.month2Span = 7-$scope.calendar.month1Span;
            }
        }
        var refreshDateTabs = function(date){
            var startDate = new Date(date.getTime())
            while(startDate.getDay() != $scope.calendar.startDay){
                startDate.setDate(startDate.getDate() -1)
            }
            var newTime = startDate.getTime();
            var added = false;
            for(var d=0; d<7; ++d){
                var time = startDate.getTime();
                var jd = $scope.job.JobDay.find(function(jd){ return jd.JobDate.getTime() == time })
                if(!jd){
                    added = true;
                    $scope.job.JobDay.push({
                        ID: $scope.getTempID(),
                        JobDate: new Date(time),
                        JobDayHotel:[],
                        HotelExclusion:[],
                        HasReceipt: false,
                        occupied: false,
                        TotalRoomedPeople: 0,
                        UnhandledPeople: $scope.crewCount,
                        UnhandledMyPeople: $scope.myCrewCount
                    })
                }
                startDate.setDate(startDate.getDate() +1)
            }
            if(added){
                Utils.sortOnProperty($scope.job.JobDay, 'JobDate')
            }
            var index = $scope.job.JobDay.findIndex(function(jd){ return jd.JobDate.getTime() == newTime })
            $scope.dateTabs = $scope.job.JobDay.slice(index,index+7);
        }
        var currentDate = null;
        var setCurrentDate = function(day){
            currentDate = new Date(day.JobDate.getTime())
            $scope.currentDate = day;
        }
        $scope.changeDate = function(day){
            setCurrentDate(day);
        }
        $scope.newDate = function(){
            //Date picker changed
            var newDate = $scope.currentDate.JobDate;
            $scope.currentDate.JobDate = currentDate;
            refreshDateTabs(newDate);
            setCurrentDate($scope.dateTabs.find(function(jd){ return jd.JobDate.getTime() == newDate.getTime() }))
            setUpDates(newDate);
        }

        $scope.viewCrew = function(hotel, scope){

            dialogs.members.openDialog({
                title: Utils.date($scope.currentDate.JobDate, 'MM/dd/yyyy')+(hotel?' - '+hotel.VendorName:''),
                hotel: hotel,
                scope: scope||$scope
            })
        }

        $scope.addHotel = function(){
            Vendors.show({
                items:$scope.data.Vendor, 
                forceType:{Code:'VE',Meaning:'Vendor'}, 
                forceSubType:{Code:'HT',Meaning:'Hotel'}, 
                title:'Hotels'
            }, function(item){
                $scope.changed();
                $scope.currentDate.occupied = true;
                var hotel = {
                    active: true,
                    ID: $scope.getTempID(),
                    VendorName: item.ReportName,
                    VendorID: item.OrganizationID,
                    OccupantCount: 0,
                    RoomCount: 0,
                    HotelRoom:[]
                }
                $scope.currentDate.JobDayHotel.push(hotel)
                $scope.viewHotel(hotel);
            })
        }

        $scope.viewHotel = function(hotel){
            $scope.current.job = $scope.job;
            $scope.current.day = $scope.currentDate;
            $scope.current.hotel = hotel;
            $scope.current.parent = $scope;
            $scope.current.data = $scope.data;
            mainNav.pushPage('app/hotels/hotel.html');
        }
        $scope.longPressHotel = function(hotel){
            navigator.vibrate(10);
            dialogs.hotelOptions.currentHotel = hotel;
            dialogs.hotelOptions.openDialog({
                title: hotel.VendorName,
                items: [{label:'Edit'},{label:'Delete'}]
            }, function(){

            })
        }
        $scope.deleteHotel = function(hotel){
            if(!$scope.isTempID(hotel.ID)){
                hotel.active = false;
            }else{
                if(hotel.tempPhotoKey){
                    $scope.tempPhotosToDelete.push(hotel.tempPhotoKey);
                }
                var index = $scope.currentDate.JobDayHotel.findIndex(function(ht){ return ht===hotel })
                if(index > -1){
                    $scope.currentDate.JobDayHotel.splice(index, 1);
                }
            }
            for(var i=hotel.HotelRoom.length-1; i>=0; --i){
                if(hotel.HotelRoom[i].active){
                    $scope.deleteRoom(hotel.HotelRoom[i], hotel, $scope);
                }
            }
            $scope.currentDate.occupied = !!$scope.currentDate.JobDayHotel.find(function(h){ return h.active })
            $scope.changed()
        }
        $scope.deleteRoom = function(room, hotel, scope){
            for(var i=room.HotelRoomOccupant.length-1; i>=0; --i){
                if(room.HotelRoomOccupant[i].active){
                    scope.removeOccupant(room, room.HotelRoomOccupant[i], hotel, scope, true)
                }
            }
            room.active = false;
            room.open = false;
            if(!$scope.isTempID(room.ID)){
                $scope.changed()
                var index = hotel.HotelRoom.findIndex(function(r){ return r.ID == room.ID });
                if(index > -1){
                    hotel.HotelRoom.splice(index, 1);
                }
            }else{
                if(room.tempPhotoKey){
                    $scope.tempPhotosToDelete.push(room.tempPhotoKey);
                }
            }
        }        
        $scope.deleteTempPhotos = function(){
            if(!$scope.tempPhotosToDelete||!$scope.tempPhotosToDelete.length){
                return;
            }
            var obj = {route:'deleteTempPhotos',keys:$scope.tempPhotosToDelete.join(',')}
            ApiService.post('hotels', obj);
            $scope.tempPhotosToDelete = [];
        }
        var _updateCounts = function(num, room, hotel, personID){
            room.OccupantCount += num;
            hotel.OccupantCount += num;
            $scope.currentDate.TotalRoomedPeople += num;
            $scope.currentDate.UnhandledPeople -= num;
            if($scope.inMyCrew(personID)){
                $scope.currentDate.UnhandledMyPeople -= num;
            }
        }

        $scope.removeOccupant = function(room, occupant, hotel, scope){
            var date = Utils.date($scope.currentDate.JobDate,'MM/dd/yyyy');
            var index = room.HotelRoomOccupant.findIndex(function(o){ return o.PersonID==occupant.PersonID });
            if(index > -1){
                if(!$scope.isTempID(occupant.ID)){
                    occupant.active = false;
                    $scope.changed()
                }else{
                    room.HotelRoomOccupant.splice(index, 1);
                }
                if(scope.availableOccupants){
                    scope.availableOccupants.push({
                        ID: occupant.PersonID,
                        Name: occupant.PersonFullName,
                        isMyCrew: occupant.isMyCrew
                    })
                }
            }
            for(var i=0; i<$scope.crew.length; ++i){
                if($scope.crew[i].ID == occupant.PersonID){
                    delete $scope.crew[i].rooms[date];
                    break;
                }
            }
            _updateCounts(-1, room, hotel, occupant.PersonID);
        }

        var sortOccupants = Utils.multiSort('isMyCrew','PersonFullName');
        $scope.addOccupant = function(room, occupant, hotel, scope){
            var date = Utils.date($scope.currentDate.JobDate,'MM/dd/yyyy');
            var hrOccupant = room.HotelRoomOccupant.find(function(o){ return o.PersonID == occupant.ID })
            if(!hrOccupant){
                hrOccupant = {
                    ID: $scope.getTempID(),
                    PersonID: occupant.ID,
                    PersonFullName: occupant.Name,
                    isMyCrew: occupant.isMyCrew
                }
                room.HotelRoomOccupant.push(hrOccupant);
            }
            hrOccupant.active = true;
            $scope.changed()
            for(var i=0; i<$scope.crew.length; ++i){
                if($scope.crew[i].ID == occupant.ID){
                    $scope.crew[i].rooms = $scope.crew[i].rooms || {};
                    $scope.crew[i].rooms[date] = {hotel:$scope.hotel,room:room,occupant:hrOccupant}
                    break;
                }
            }
            if(scope.availableOccupants){
                scope.availableOccupants.splice(scope.availableOccupants.findIndex(function(o){ return o===occupant}), 1);
            }
            room.HotelRoomOccupant.sort(sortOccupants);
            _updateCounts(1, room, hotel, occupant.ID);
        }

        $scope.changed = function(day){
            day = day||$scope.currentDate;
            day.changed = true;
            $scope.job.changed = true;
        }

        var hotelOption = function(index){
            if(index==0){
                $scope.viewHotel(dialogs.hotelOptions.currentHotel);
            }
            if(index == 1){
                $scope.deleteHotel(dialogs.hotelOptions.currentHotel);
            }
        }

        var _saveResult = function(resp){
            $scope.loading = false;
            if(resp){
                if(closing){
                    close(false);
                }else{
                    if(resp.replacements && resp.replacements.length){
                        Utils.replace($scope.job, resp.replacements);
                    }
                    if(resp.Vendor){
                        resp.Vendor = Utils.enforceArray(resp.Vendor)
                        for(var i=0; i<resp.Vendor.length; ++i){
                            var vendor = $scope.data.Vendor.find(function(v){ return v.ReportName == resp.Vendor[i].ReportName });
                            if(!vendor){
                                $scope.data.Vendor.push(resp.Vendor[i]);
                            }
                        }
                    }
                    $scope.job.changed = false;
                    for(var i=0; i<$scope.job.JobDay.length; ++i){
                        var day = $scope.job.JobDay[i];
                        day.changed = false;
                        for(var h=day.JobDayHotel.length-1; h>=0; --h){
                            var hotel = day.JobDayHotel[h];
                            if(hotel.active){
                                for(var r=hotel.HotelRoom.length-1; r>=0; --r){
                                    var room = hotel.HotelRoom[r];
                                    if(room.active){
                                        for(var o=room.HotelRoomOccupant.length-1; o>=0; --o){
                                            var occupant = room.HotelRoomOccupant[o];
                                            if(!occupant.active){
                                                room.HotelRoomOccupant.splice(o, 1);
                                            }
                                        }
                                    }else{
                                        hotel.HotelRoom.splice(r, 1);
                                    }
                                }
                            }else{
                                day.JobDayHotel.splice(h, 1);
                            }
                        }
                        for(var e=day.HotelExclusion.length-1; e>=0; --e){
                            var exclusion = day.HotelExclusion[e];
                            if(!exclusion.active){
                                day.HotelExclusion.splice(e, 1);
                            }
                        }
                    }
                }
            }
        }
        var _getPostData = function(done){
            var obj = {
                route: 'save',
                Job: {
                    ID: $scope.job.ID,
                    JD: []
                }
            }
            for(var d=0; d<$scope.job.JobDay.length; ++d){
                var day = $scope.job.JobDay[d];
                if(day.changed){
                    var JobDay = {
                        ID: day.ID,
                        D: Utils.date(day.JobDate,'MM/dd/yyyy'),
                        H: [],
                        E: []
                    }
                    obj.Job.JD.push(JobDay)
                    for(var h=0; h<day.JobDayHotel.length; ++h){
                        var hotel = day.JobDayHotel[h];
                        if(hotel.active){
                            var JobDayHotel = {
                                ID: hotel.ID,
                                VID: hotel.VendorID||'',
                                Cy: hotel.City,
                                Ct: hotel.Cost,
                                N: hotel.Notes,
                                R: []
                            }
                            JobDay.H.push(JobDayHotel);
                            if(!hotel.VendorID){
                                JobDayHotel.VN = hotel.VendorName;
                            }
                            if(hotel.tempPhotoKey){
                                JobDayHotel.PK = hotel.tempPhotoKey;
                            }
                            for(var r=0; r<hotel.HotelRoom.length; ++r){
                                var room = hotel.HotelRoom[r];
                                if(room.active){
                                    var HotelRoom = {
                                        ID: room.ID,
                                        Nm: room.RoomNumber,
                                        Ct: room.Cost,
                                        N: room.Notes,
                                        O: []
                                    }
                                    JobDayHotel.R.push(HotelRoom);
                                    if(room.tempPhotoKey){
                                        HotelRoom.PK = room.tempPhotoKey;
                                    }
                                    for(var o=0; o<room.HotelRoomOccupant.length; ++o){
                                        var occupant = room.HotelRoomOccupant[o];
                                        if(occupant.active){
                                            var HotelRoomOccupant = {
                                                ID: occupant.ID,
                                                PID: occupant.PersonID
                                            }
                                            HotelRoom.O.push(HotelRoomOccupant);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for(var e=0; e<day.HotelExclusion.length; ++e){
                        var exclusion = day.HotelExclusion[e]
                        if(exclusion.active){
                            var HotelExclusion = {
                                ID: exclusion.ID,
                                PID: exclusion.PersonID
                            }
                            JobDay.E.push(HotelExclusion);
                        }
                    }
                }
            }
            done(obj)
        }        

        $scope.save = function(){
            //TODO: check if save is necessary
            $scope.loading = true;
            var obj = _getPostData(function(obj, formdata){
                ApiService.post('hotels', obj, '', '', formdata).then(_saveResult,_saveResult);
            });
        }

        var closing = false;
        var close = function(save){
            if(save){
              $scope.save();
              return;
            }else{
                $scope.deleteTempPhotos();
            }
            mainNav.popPage();
          };
        $scope.close = function(){
            if(closing || (mainNav.pages[mainNav.pages.length-1].pushedOptions.page != 'app/hotels/hotels.html')) return;
            closing = true;
            if(!$scope.job.JobDay.find(function(jd){ return jd.changed })){
                close(false)
                return;
            }
            Utils.notification.confirm("Save changes?", {
              callback: function(ok){
                if(ok == 2){
                  closing = false;
                  return;
                }
                close(ok==0);
              },
              buttonLabels: ["Save","Discard","Cancel"]
          });
        }
     
        load();
        
         var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', $scope.close, false);
            }else{
                document.removeEventListener('backbutton', $scope.close);
                ons.enableDeviceBackButtonHandler();
            }
          }

          $scope.hidePage = function(){
            toggleBackButton(false)
            Utils.clearBackButtonHandlers()
          }
          $scope.showPage = function(){
            toggleBackButton(true)
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })

            ons.findComponent('#hotels-back-button')._element[0].onClick = $scope.close;
          }

        $scope.$on('$destroy', function(){
            var dialogKeys = Object.keys(dialogs);
            for(var k=0; k<dialogKeys.length; ++k){
                var dialog = dialogs[dialogKeys[k]];
                if(dialog.visible){
                    dialog.hide();
                }
            }
            Smartpick.hide();
            UploadDialog.hide();
            Vendors.hide();
        });

        ons.createAlertDialog('app/dialog/radio-button-dialog.html').then(function(dialog){
            dialogs.hotelOptions = dialog;
            dialog._scope.data = {noRadio: true, autoClose:true}
            dialog._scope.selectRow = function(index){
                hotelOption(index);
                dialog.hide()
            }
            dialog.openDialog = function(options, done){
                dialog._scope.data.title = options.title||'Select';
                dialog._scope.data.currentIndex = options.currentIndex||dialog._scope.data.currentIndex||0;
                dialog._scope.data.items = options.items||[];
                dialog._scope.done = function(){
                    dialog.hide();
                };
                Utils.openDialog(dialog);
            }
        });


        var getPersonHotelRoom = function(personID, callback){
            for(var i=0; i<$scope.currentDate.JobDayHotel.length; ++i){
                var hotel = $scope.currentDate.JobDayHotel[i];
                if(hotel.active){
                    for(var j=0; j<hotel.HotelRoom.length; ++j){
                        var room = hotel.HotelRoom[j];
                        if(room.active){
                            for(var o=0; o<room.HotelRoomOccupant.length; ++o){
                                var occupant = room.HotelRoomOccupant[o];
                                if(occupant.active && occupant.PersonID == personID){
                                    return callback(hotel, room, occupant)
                                }
                            }
                        }
                    }
                }
            }
            callback()
        }
        ons.createAlertDialog('app/hotels/hotel-member-dialog.html').then(function(dialog){
            dialogs.members = dialog;
            dialog._scope.data = {}
            dialog._scope.changeNoRoom = function(person){
                var existing = $scope.currentDate.HotelExclusion.find(function(e){ return e.PersonID == person.id })
                if(person.noRoom){
                    if(!existing){
                        existing = { ID: $scope.getTempID(), PersonID: person.id }
                        $scope.currentDate.HotelExclusion.push(existing)
                    }
                    existing.active = true;
                    if(person.hotel){
                        getPersonHotelRoom(person.id, function(hotel, room, occupant){
                            if(hotel && room && occupant){
                                $scope.removeOccupant(room, occupant, hotel, dialog.scope);
                            }
                        });
                    }
                    $scope.currentDate.UnhandledPeople -= 1;
                    if($scope.inMyCrew(person.id)){
                        $scope.currentDate.UnhandledMyPeople -= 1;
                    }
                    person.handled = true;
                    if(dialog.scope.availableOccupants){
                        var index = dialog.scope.availableOccupants.findIndex(function(o){ return o.ID == person.id});
                        if(index > -1){
                            dialog.scope.availableOccupants.splice(index, 1);
                        }
                    }
                }else{
                    if(existing){
                        existing.active = false;
                    }
                    if(dialog._scope.data.type=='hotel'){
                        person.hotel = dialog._scope.data.hotel[0]
                    }  
                    $scope.currentDate.UnhandledPeople += 1;
                    if($scope.inMyCrew(person.id)){
                        $scope.currentDate.UnhandledMyPeople += 1;
                    }
                    person.handled = false;
                    if(dialog.scope.availableOccupants){
                        var index = dialog.scope.availableOccupants.findIndex(function(o){ return o.ID == person.id});
                        if(index == -1){
                            dialog.scope.availableOccupants.push({
                                ID: person.id,
                                Name: person.name,
                                isMyCrew: person.isMyCrew
                            });
                        }
                    }
                    dialog._scope.changeRoom(person);
                }

                $scope.changed();
            }
            dialog._scope.changeHotel = function(person){
                person.room = null;
                person.handled = false;
                if(person.hotel){
                    var rooms = dialog._scope.data.room[person.hotel.index];
                    if(rooms.length == 1){
                        person.room = rooms[0];
                        dialog._scope.changeRoom(person);
                    }
                }
            }
            dialog._scope.changeRoom = function(person){
                if(person.hotel && person.room){
                    var hotel = $scope.currentDate.JobDayHotel.find(function(ht){ return ht.active && ht.ID == person.hotel.id })
                    if(hotel){
                        var room = hotel.HotelRoom.find(function(rm){ return rm.active && rm.ID == person.room.id })
                        if(room){
                            person.handled = true;
                            getPersonHotelRoom(person.id, function(hotel, room, occupant){
                                if(hotel && room && occupant){
                                    $scope.removeOccupant(room, occupant, hotel, dialog.scope);
                                }
                            });
                            $scope.addOccupant(room, {ID:person.id,Name:person.name}, hotel, dialog.scope)
                        }
                    }
                }
            }
            dialog._scope.done = function(){
                dialog.hide();
            };
            var getHotels = function(type, data){
                data.hotel = []
                var names = {}, showCities = false;
                data.room = []
                for(var i=0; i<$scope.currentDate.JobDayHotel.length; ++i){
                    var hotel = $scope.currentDate.JobDayHotel[i];
                    if(hotel.active && (type=='day'||dialog.hotel.ID==hotel.ID)){
                        if(names[hotel.VendorName]){
                            showCities = true;
                        }
                        var hl = {
                            name: hotel.VendorName,
                            city: hotel.City,
                            id: hotel.ID,
                            index: data.hotel.length
                        }
                        data.room[hl.index] = []
                        data.hotel.push(hl);
                        names[hotel.VendorName] = true;

                        for(var j=0; j<hotel.HotelRoom.length; ++j){
                            var room = hotel.HotelRoom[j];
                            if(room.active){
                                var rm = {
                                    num: room.RoomNumber||'<New>',
                                    id: room.ID,
                                    receipt: room.HasReceipt
                                }
                                data.room[hl.index].push(rm);
                            }
                        }
                    }
                }
                if(showCities){
                    for(var h=0; h<data.hotel.length; ++h){
                        data.hotel[h].name += '('+data.hotel[h].city+')';
                    }
                }
            }
            var addPerson = function(type, data, ids, id, name){
                if(!ids[id]){
                    var person = {
                        id: id,
                        name: name,
                        handled: false,
                        isMyCrew: $scope.inMyCrew(id)
                    }
                    ids[id] = person;

                    if($scope.currentDate.HotelExclusion.find(function(e){ return e.PersonID == person.id && e.active })){
                        person.noRoom = true;
                        person.handled = true;
                        data.person.push(person);
                    }else{
                        getPersonHotelRoom(person.id, function(hotel, room){
                            person.hotel = hotel?data.hotel.find(function(ht){ return ht.id == hotel.ID }):null;
                            if(person.hotel){
                                person.room = data.room[person.hotel.index].find(function(rm){ return rm.id == room.ID })
                                if(type=='day'||dialog.hotel.ID==person.hotel.id){
                                    data.person.push(person);
                                }
                                if(person.room){
                                    person.handled = true;
                                }
                            }else{
                                if(!hotel){
                                    if(type=='hotel'){
                                        person.hotel = data.hotel[0]
                                    }  
                                    data.person.push(person);
                                }
                            }
                        })
                    }
                }
            }
            var sortPeople = Utils.multiSort({name:'isMyCrew',reverse:true},'handled','name')
            var buildData = function(type, data){
                getHotels(type,data)
                data.person = []
                data.me = $scope.myId;
                data.myCrew = $scope.myCrew;
                var ids = {}
                for(var i=0; i<$scope.crew.length; ++i){
                    addPerson(type, data, ids, $scope.crew[i].ID, $scope.crew[i].Name);
                }
                data.person.sort(sortPeople);
            }
            dialog.openDialog = function(options){
                dialog._scope.data.title = options.title||'Members';
                dialog.hotel = options.hotel||null;
                dialog._scope.data.type = dialog.hotel?'hotel':'day';
                dialog.scope = options.scope;
                buildData(dialog._scope.data.type, dialog._scope.data);                
                Utils.openDialog(dialog);
            }
        });
    }]);

})();
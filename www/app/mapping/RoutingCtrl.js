(function(){
    'use strict';

    angular.module('app').controller('RoutingController', ['$scope', '$timeout', 'ApiService', 'Utils', 'Smartpick', 'StorageService', 'States', 'pinVariants', function($scope, $timeout, ApiService, Utils, Smartpick, StorageService, States, pinVariants) {
        $scope.loading = false;
        $scope.working = true;
        
        var map, dialogs = {}
        var selectedMarker;
        var markers = new ol.source.Vector({
          features: []
        })

        $scope.colors = {
            complete: '#0683FF',
            uncomplete: '#F97C00'
        }
        var overlay;
        var createOverlay = function(){
            var container = document.getElementById('routePopup');
            overlay = new ol.Overlay({
              element: container,
              positioning: 'bottom-center',
              stopEvent: true,
              offset: [0, -50],
              autoPan: true,
              autoPanAnimation: {
                duration: 250
              }
            });
        }
        var initMap = function(){
            if(!map){
                if(document.getElementById('routing-map')){
                    var source = new ol.source.OSM();
                    var view = new ol.View({
                      center: ol.proj.fromLonLat([-98.5795,39.8283]),
                      zoomFactor: 4,
                      zoom: 1.75
                    });
                    var geolocation = new ol.Geolocation({
                      projection: view.getProjection()
                    });
                    geolocation.on('change:position', function() {
                      var coordinates = geolocation.getPosition();
                      positionFeature.setGeometry(coordinates ?
                        new ol.geom.Point(coordinates) : null);
                    });

                    var accuracyFeature = new ol.Feature();
                    geolocation.on('change:accuracyGeometry', function() {
                      accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
                    });

                    var positionFeature = new ol.Feature();
                    positionFeature.setStyle(new ol.style.Style({
                      image: new ol.style.Circle({
                        radius: 6,
                        fill: new ol.style.Fill({
                          color: '#3399CC'
                        }),
                        stroke: new ol.style.Stroke({
                          color: '#fff',
                          width: 2
                        })
                      })
                    }));
                    var positionLayer = new ol.layer.Vector({
                        source: new ol.source.Vector({
                          features: [accuracyFeature, positionFeature]
                        })
                    })
                    var vectorLayer = new ol.layer.Vector({
                      source: markers,
                      renderOrder: function(a,b){
                          if(a === selectedMarker) return 1;
                          if(b === selectedMarker) return -1;
                          var coords = [a.getGeometry().getCoordinates(), b.getGeometry().getCoordinates()];
                          if(coords[0][1] == coords[1][1]) return coords[1][0]-coords[0][0];
                          return coords[1][1]-coords[0][1];
                      }
                    });
                    var centerMapControl = (function (Control) {
                      function CenterMapControl(opt_options) {
                        var options = opt_options || {};

                        var button = document.createElement('button');
                        button.innerHTML = '<i class="ion-android-locate"></i>';

                        var element = document.createElement('div');
                        element.className = 'ol-unselectable ol-control ol-center-control';
                        element.appendChild(button);

                        Control.call(this, {
                          element: element,
                          target: options.target
                        });

                        button.addEventListener('click', this.handleCenter.bind(this), false);
                      }

                      if ( Control ) CenterMapControl.__proto__ = Control;
                      CenterMapControl.prototype = Object.create( Control && Control.prototype );
                      CenterMapControl.prototype.constructor = CenterMapControl;

                      CenterMapControl.prototype.handleCenter = function handleCenter () {
                        this.getMap().getView().setCenter(geolocation.getPosition());
                      };

                      return CenterMapControl;
                    }(ol.control.Control));
                    var mapSettingsControl = (function (Control) {
                      function MapSettingsControl(opt_options) {
                        var options = opt_options || {};

                        var button = document.createElement('button');
                        button.innerHTML = '<i class="ion-android-settings"></i>';

                        var element = document.createElement('div');
                        element.className = 'ol-unselectable ol-control ol-settings-control';
                        element.appendChild(button);

                        Control.call(this, {
                          element: element,
                          target: options.target
                        });

                        button.addEventListener('click', this.openSettings.bind(this), false);
                      }

                      if ( Control ) MapSettingsControl.__proto__ = Control;
                      MapSettingsControl.prototype = Object.create( Control && Control.prototype );
                      MapSettingsControl.prototype.constructor = MapSettingsControl;

                      MapSettingsControl.prototype.openSettings = function openSettings () {
                        dialogs.mapSettings.open()
                      };

                      return MapSettingsControl;
                    }(ol.control.Control));
                    createOverlay();

                    map = new ol.Map({
                        controls: ol.control.defaults().extend([
                            new centerMapControl(),
                            new mapSettingsControl()
                        ]),
                        overlays: [overlay],
                        target: 'routing-map',
                        pixelRatio: Math.max(2, ol.has.DEVICE_PIXEL_RATIO),
                        layers: [
                          new ol.layer.Tile({
                            source: source
                          }),
                          positionLayer,
                          vectorLayer
                        ],
                        view: view
                    });
                    $scope.working = false;
                    $timeout(fixCanvasSize);
                    geolocation.setTracking(true);
                    var longpress = false;
                    map.on("click", function(e) {
                        if(longpress){
                            return;
                        }else{
                            clearTimeout(pressTimer)
                        }
                        var feature = map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
                            if(feature.get('location')){
                                return feature;
                            }
                        });
                        if(feature){
                            showLocationInfo(feature.get('location'));                            
                        }else{
                            overlay.setPosition(undefined);
                            unselectMarker();
                        }
                    });
                    var pressTimer;
                    map.on('pointerdown', function (e) {
                        longpress = false;
                        clearTimeout(pressTimer)
                        pressTimer = setTimeout(function(){
                            longpress = true;
                            var coords = map.getCoordinateFromPixel(e.pixel);
                            var lonLat = ol.proj.toLonLat(coords)
                            showContextMenu({lon:lonLat[0],lat:lonLat[1],coordinates:coords});
                            navigator.vibrate(10);
                        },500)
                    });
                    map.on('pointerdrag', function (e) {
                        clearTimeout(pressTimer)
                    });
                    $timeout(function(){
                      if(vectorLayer.getSource().getFeatures().length){
                        var extent = vectorLayer.getSource().getExtent();
                        map.getView().fit(extent, {size: map.getSize(), padding:[80,40,40,80], maxZoom:12});
                      }else{
                        map.setView(new ol.View({
                          center: geolocation.getPosition(),
                          zoom: 15
                        }));
                      }
                    });
                }else{
                    $timeout(initMap);
                }
            }
        }

        $scope.changeTab = function($event){
            if($event.index == 0 && (dialogs.mapSettings && dialogs.mapSettings._scope.onlyFiltered)){
                filterMapPins();
            }
        }

        function getSupportedTransform() {
            var prefixes = 'transform WebkitTransform MozTransform OTransform msTransform'.split(' ');
            for(var i = 0; i < prefixes.length; i++) {
                if(document.createElement('div').style[prefixes[i]] !== undefined) {
                    return prefixes[i];
                }
            }
            return false;
        }
        var canvasTries = 0;
        var fixCanvasSize = function(){ //this hack is needed to support old android versions
            if(getSupportedTransform() !== 'transform'){
                var canvases = document.querySelectorAll('.ol-layer canvas')
                if(!canvases.length){
                    if(canvasTries < 5){
                        ++canvasTries;
                        $timeout(fixCanvasSize)
                    }
                    return;
                }
            
                canvases.forEach(function(canvas){
                    canvas.style.width = '100%';
                    canvas.style.height = '100%';
                })
            }
        }

        var markerStyles = {}
        var getMarkerStyle = function(color, variant){
            variant = variant?('_'+variant):'';
            color = (color.indexOf('#')>-1?'#':'')+color.replace('#','').toUpperCase();
            if(!markerStyles[color+variant]){
                markerStyles[color+variant] = new ol.style.Style({
                  image: new ol.style.Icon({
                    anchor: [0.5, 1],
                    color: color,
                    scale: 0.1,
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    src: pinVariants['pin'+variant]
                  })
                });
            }
            return markerStyles[color+variant];
        }
        var setMarkerStyle = function(feature, color){
            var location = feature.get('location');
            feature.setStyle(getMarkerStyle(color?color:location.LocationComplete?$scope.colors.complete:$scope.colors.uncomplete, location.LocationInexactAddressMatch?'notice':''));
        }
        var addLocationToMap = function(location){
            var lonLat = new ol.proj.fromLonLat([location.LocationLongitude, location.LocationLatitude]);
            if(location.pin){
                location.pin.getGeometry().setCoordinates(lonLat);
            }else{
                var marker = new ol.Feature({
                  geometry: new ol.geom.Point(lonLat),
                  location: location
                });
                location.pin = marker;
                setMarkerStyle(marker);
                var showPin = shouldShowLocationPin(location);
                if(showPin){
                    markers.addFeature(marker);
                }
            }
        }

        $scope.locatePin = function(location){
            if(location.status == 'map'){
                map.getView().setCenter(ol.extent.getCenter(location.pin.getGeometry().getExtent()));
                if(routingTabbar.getActiveTabIndex() != 0){
                    routingTabbar.setActiveTab(0);
                }
                showLocationInfo(location);
            }else if(location.status == 'error'){
                $scope.editLocationAddress(location);
            }
        }
        var unselectMarker = function(){
            if(selectedMarker){
                setMarkerStyle(selectedMarker);
                selectedMarker = null;
            }
        }
        var showLocationInfo = function(location){
            $scope.contextMenu = null;
            unselectMarker();
            selectedMarker = location.pin;
            setMarkerStyle(selectedMarker, '#FF0000')
            var coordinates = selectedMarker.getGeometry().getCoordinates();
            $timeout(function(){
                $scope.popupLocation = location;
                overlay.setPosition(coordinates);
            });
            
        }
        var showContextMenu = function(data){
            $scope.popupLocation = null;
            unselectMarker();
            $timeout(function(){
                $scope.contextMenu = data;
                overlay.setPosition(data.coordinates);
            });
            
        }
        $scope.searchCoords = function(){
            if($scope.contextMenu){
                var options = Utils.pick($scope.contextMenu, 'lat', 'lon');
                options.callback = function(newLocations){
                    $scope.loading = false;
                    if(newLocations&&newLocations.length){
                        executefilters();
                        var count = load(newLocations);
                        if(count) Utils.notification.toast('Loaded '+count+' more location'+(count>1?'s':''), {timeout: 2000 });
                    }
                }
                $scope.parent.search(options);
                $scope.loading = true;
                $scope.contextMenu = null;
                overlay.setPosition(null);
            }
        }

        var mapSettings = {}
        var mapSettingsSaveState = {name:'routeMapSettings',fields:['onlyFiltered']}
        StorageService.state.load(mapSettingsSaveState.name, mapSettings, mapSettingsSaveState.fields)
        if(typeof mapSettings.onlyFiltered == 'undefined'){
            mapSettings.onlyFiltered = true;
        }
        var shouldShowLocationPin = function(location){
            if(dialogs.mapSettings){
                if(dialogs.mapSettings._scope.onlyFiltered && !$scope.filteredLocations.find(function(l){return l.ID==location.ID})){
                    return false;
                }
            }else{
                if(mapSettings.onlyFiltered && !$scope.filteredLocations.find(function(l){return l.ID==location.ID})){
                    return false;
                }
            }
            return true;
        }
        var filterMapPins = function(){
            var features = markers.getFeatures()
            if(dialogs.mapSettings._scope.onlyFiltered){
                for(var i=features.length-1; i>=0; --i){
                    if(!$scope.filteredLocations.find(function(l){return l.ID==features[i].get('location').ID})){
                        if($scope.popupLocation && $scope.popupLocation.ID == features[i].get('location').ID){
                            overlay.setPosition(undefined);
                            unselectMarker();
                        }
                        markers.removeFeature(features.splice(i,1)[0]);
                    }
                }
                for(var l=0; l<$scope.filteredLocations.length; ++l){
                    if($scope.filteredLocations[l].pin && !features.find(function(f){return f.get('location').ID==$scope.filteredLocations[l].ID})){
                        markers.addFeature($scope.filteredLocations[l].pin);
                    }
                }
            }else{
                for(var l=0; l<$scope.job.DispatchLocation.length; ++l){
                    if($scope.job.DispatchLocation[l].pin && !features.find(function(f){return f.get('location').ID==$scope.job.DispatchLocation[l].ID})){
                        markers.addFeature($scope.job.DispatchLocation[l].pin);
                    }
                }
            }
        }

        $scope.model = {
            filterList: ''
        }
        var filterKeys = [];
        var setUpFilters = function(){
            filterKeys = Object.keys($scope.parent.filters);
            if(!filterKeys.includes('LocationAddress')){
                filterKeys.push('LocationAddress');
            }
            if(!filterKeys.includes('LocationLocationNumberChar')){
                filterKeys.push('LocationLocationNumberChar');
            }
            filterKeys.push('LocationPostalCode');
            executefilters()
        }
        var executefilters = function(){
            var filter = $scope.filterLocations($scope.filterList);
            $scope.job.DispatchLocation.forEach(filter);
        }
        $scope.filterLocations = function(filterList){
            $scope.filterList = (filterList||'').trim();
            $scope.filteredLocations = []
            $scope.filteredCounts = [0,0,0,0]
            var regex = $scope.model.filterList?$scope.parent.buildWildcardRegex($scope.model.filterList):null;
            var completeDate = $scope.job.CompleteDate?$scope.job.CompleteDate.getTime():null
            return function(item){
                if(item.deleted) return false;
                $scope.filteredCounts[1]++;
                if(item.status == 'map'){
                    $scope.filteredCounts[2]++;
                    if($scope.job.mappedFilter.Display == 'Unmapped') return false;
                }else{
                    $scope.filteredCounts[3]++;
                    if($scope.job.mappedFilter.Display == 'Mapped') return false;
                }
                if(Utils.isTempId(item.ID) || item.forceVisible){
                    $scope.filteredCounts[0]++;
                    $scope.filteredLocations.push(item);
                    return true;
                }
                var time = item.WorkDate.getTime();
                if(time!=completeDate && (($scope.job.doneFilter.Display == 'Not Done' && item.LocationComplete) || ($scope.job.doneFilter.Display == 'Done' && !item.LocationComplete))) return false;
                if($scope.job.hasForemenAssigned && $scope.job.foremanFilter.ID!='ALL' && $scope.job.foremanFilter.ID!=item.LocationAssignedForemanID) return false;
                if(regex){
                    for(var k=0; k<filterKeys.length; ++k){
                        if(item[filterKeys[k]].match(regex)){
                            $scope.filteredCounts[0]++;
                            $scope.filteredLocations.push(item);
                            return true;
                        }
                    }
                    return false;
                }
                $scope.filteredCounts[0]++;
                $scope.filteredLocations.push(item);
                return true;
            }
            $timeout(function(){
                if(!$scope.filteredCounts[2]||$scope.filteredCounts[3]){
                    if(!$scope.filteredCounts[2] && !$scope.filteredCounts[3]){
                        $scope.job.mappedFilter = $scope.parent.mappedFilterOptions[0];
                    }else if(!$scope.filteredCounts[2]){
                        $scope.job.mappedFilter = $scope.parent.mappedFilterOptions[2];
                    }else{
                        $scope.job.mappedFilter = $scope.parent.mappedFilterOptions[1];
                    }
                }
            })
        }

        $scope.clearFilter = function(){
            $scope.model.filterList = '';
            document.querySelector('#routeLocationFilter').focus();
        }

        $scope.nav = function(location){
            var url;
            var address = encodeURI(location.LocationLatitude+','+location.LocationLongitude);
            if(device.platform == 'Android'){
                if(location.LocationAddress && location.LocationPostalCode){
                    address = encodeURI(location.LocationAddress+', '+location.LocationPostalCode);
                }
                url = 'geo:0,0?q=' + address;
                window.open(url, '_system');
            }else{
                url = 'maps://?q='+address;
                window.open(url, '_system');
            }
        }

        $scope.editLocation = function(location){
            dialogs.location.open(location);
        }
        $scope.editLocationAddress = function(location){
            dialogs.address.open(location);
        }

        var lonLatRegExp = [/^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/,/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/]
        var validateLonLat = function(location){
            if(   (location.LocationLongitude && location.LocationLongitude.toString().match(lonLatRegExp[0]))
               &&  (location.LocationLatitude && location.LocationLatitude.toString().match(lonLatRegExp[1]))){
                return true;
            }
            return false;
        }
        var geocodeORS = function(location, callback){
            var address = location.LocationPostalCode;
            if(location.LocationAddress){
                address = location.LocationAddress+', '+address;
            }
            ApiService.get('geocodeORS', {text: address}).then(function(resp){
                if(resp && resp.features){
                    location.status = 'map';
                    location.updatedLonLat = true;
                    location.LocationLongitude = resp.features[0].geometry.coordinates[0];
                    location.LocationLatitude = resp.features[0].geometry.coordinates[1];
                    addLocationToMap(location);
                }
                callback();
            });
        }

        var getAddressComponent = function(components, componentType){
            var component;
            if(component = components.find(function(c){ return c.types.indexOf(componentType) > -1 })){
                return component;
            }
            return {short_name:'',long_name:''}
        }
        var mapGMResults = function(item){
            var c = item.address_components;
            var address = getAddressComponent(c, 'street_number').long_name+' '+getAddressComponent(c, 'route').short_name,
                postal = getAddressComponent(c, 'postal_code').long_name,
                city = getAddressComponent(c, 'locality').short_name,
                state = getAddressComponent(c, 'administrative_area_level_1').short_name
            return {
                Lat:item.geometry.location.lat,
                Lon: item.geometry.location.lng,
                Address: address,
                PC: postal,
                City: city,
                State: state,
                Part: item.partial_match||(!address||!postal||!city||!state)
            } 
        }
        var geocodeGM = function(location, callback){
            var req = {address: (location.LocationAddress||'')};
            if(location.LocationCity){
                req.address += (location.LocationAddress?', ':'')+location.LocationCity;
            }
            if(location.LocationPostalCode){
                req.address += ', '+location.LocationPostalCode;
            }
            ApiService.get('geocodeGM', req).then(function(resp){
                if(resp){
                    if(resp.status == 'OK'){
                        if(resp.results && resp.results.length){
                            var results = resp.results.map(mapGMResults);
                            if(results.length > 0){//== 1){
                                location.status = 'map';
                                location.updatedLonLat = true;
                                location.LocationLongitude = results[0].Lon;
                                location.LocationLatitude = results[0].Lat;
                                location.LocationPostalCode = results[0].PC;
                                location.LocationCity = results[0].City;
                                location.LocationStateCode = results[0].State;
                                location.LocationInexactAddressMatch = results[0].Part;
                                addLocationToMap(location);
                           //TODO (maybe): add to list, have user choose best match
                           // }else{
                           //     location.pendingAddresses = results;
                            }
                        }
                    }
                }
                callback();
            });
        }
        var geocodeAddress = function(location, callback){
            geocodeGM(location, callback);
        }
        var saveLonLats = function(){
            var locations = $scope.job.DispatchLocation.reduce(function(locations, loc){
                if(loc.updatedLonLat && loc.LocationID){
                    loc.updatedLonLat = false;
                    locations.push($scope.parent.getLocationAddressObj(loc));
                }
                return locations;
            }, [])
            if(locations.length){
                ApiService.post('dispatch', {action:'updateLonLat',dispatch:$scope.job.ID,job:$scope.job.JobID,Location:locations})
            }
        }
        var load = function(locations){
            locations = locations||$scope.job.DispatchLocation;
            var lookup = []
            var count = 0
            for(var l=0; l<locations.length; ++l){
                if(!locations[l].pin){
                    ++count;
                    if(validateLonLat(locations[l])){
                        locations[l].status = 'map';
                        addLocationToMap(locations[l]);
                    }else{
                        if(locations[l].LocationPostalCode||locations[l].LocationCity){
                            lookup.push(locations[l]);
                            locations[l].status = 'loading';
                        }else{
                            locations[l].status = 'error';
                        }
                    }
                }
            }
            if(lookup.length){
                var count = lookup.length;
                var done = function(){
                    --count;
                    if(!count){
                        saveLonLats();
                    }
                }
                for(var l=0; l<lookup.length; ++l){
                    geocodeAddress(lookup[l], done);
                }
            }
            return count;
        }

        ons.createAlertDialog('app/mapping/route-location-dialog.html').then(function(dialog){
            dialogs.location = dialog;
            dialog.open = function(location){
                dialog._scope.parent = $scope.parent;
                dialog._scope.job = $scope.job;
                dialog._scope.location = location;
                Utils.openDialog(dialog);
            }
            dialog._scope.close = function(){
                dialog.hide();
            }
            dialog._scope.editAddress = function(){
                $scope.editLocationAddress(dialog._scope.location);
            }
            dialog._scope.delete = function(){
                var pin = dialog._scope.location.pin;
                $scope.parent.deleteLocation(dialog._scope.location, function(success){
                    if(success){
                        if(pin){
                            markers.removeFeature(pin);
                        }
                        dialog.hide();                        
                    }
                });
            }
        });


        $scope.openPostalSearch = function(callback){
           if(Smartpick.ok()){
                Smartpick.show();
            }else{
                Smartpick.show({
                    endpoint: 'postalSearch',
                    itemView: 'PostalCode',
                    itemTemplate: 'postal-code.html',
                    title: 'City / Zip',
                    filters: [
                        {key: 'Country', from: 'ID', to:'StateCountryID', display:'Name'},
                        {key: 'State', from: 'ID', to:'StateStateID', display:'Name', dependencies:{'Country':{from:'CountryID',to:'ID'}}}
                    ]
                }, callback);
            }
        }
        ons.createDialog('app/mapping/route-address-dialog.html').then(function(dialog){
            dialogs.address = dialog;
            dialog._scope.data = {
                State: States
            }
            dialog.open = function(location){
                dialog._scope.form = {
                    address: location.LocationAddress||'',
                    city: location.LocationCity||'',
                    postal: location.LocationPostalCode||'',
                    state: location.LocationStateCode?dialog._scope.data.State.find(function(s){return s.Code==location.LocationStateCode}):null
                }
                dialog._scope.loading = false;
                dialog._scope.items = []
                dialog._scope.showItems = false;

                dialog._scope.location = location;
                Utils.openDialog(dialog);
            }
            dialog._scope.openPostalSearch = function(){
               $scope.openPostalSearch(function(item){
                    dialog._scope.form.postal = item.PostalCode;
                });
            }
            dialog._scope.executeSearch = function(){
                var form = dialog._scope.form;
                if(!form.address.trim() || (form.postal.length<5&&!form.city.trim()&&!form.state)){
                    return;
                }
                dialog._scope.loading = true;
                var address = form.address;
                if(form.city) address += ', '+form.city;
                if(form.state) address += ', '+form.state.Code;
                var obj = {address: address}
                if(form.postal){
                    obj.components = 'postal_code:'+form.postal
                }
                ApiService.get('geocodeGM', obj).then(function(resp){
                    if(resp){
                        dialog._scope.showItems = true;
                        if(resp.status == 'OK'){
                            if(resp.results && resp.results.length){
                                dialog._scope.items = resp.results.map(mapGMResults);
                            }
                        }
                    }
                    dialog._scope.loading = false;
                });
            }
            dialog._scope.selectItem = function(item){
                var location = dialog._scope.location;
                var pin = location.pin;
                location.status = 'map';
                location.updatedLonLat = true;
                location.LocationLongitude = item.Lon;
                location.LocationLatitude = item.Lat;
                location.LocationAddress = item.Address;
                location.LocationCity = item.City;
                location.LocationStateCode = item.State;
                location.LocationPostalCode = item.PC;
                location.LocationInexactAddressMatch = item.Part;
                addLocationToMap(location);
                saveLonLats();
                dialog.hide();

                if(pin){
                    setMarkerStyle(pin, '#FF0000')
                }
                if(routingTabbar.getActiveTabIndex() == 0){
                    $scope.locatePin(location);
                }
            }
        });

        ons.createAlertDialog('app/mapping/route-map-settings-dialog.html').then(function(dialog){
            dialogs.mapSettings = dialog;
            dialog.saveState = mapSettingsSaveState
            var keys = Object.keys(mapSettings)
            for(var i=0; i<keys.length; ++i){
                dialog._scope[keys[i]] = mapSettings[keys[i]]
            }
            dialog.open = function(){
                Utils.openDialog(dialog);
            }
            dialog._scope.close = function(){
                dialog.hide();
            }
            dialog._scope.changeMapFilter = function(){
                filterMapPins()
            }
        });



        $scope.hidePage = function(e){
            if(e.srcElement.id == 'routingPage'){
                
            }
        }
        $scope.$on('$destroy', function(){
            var dialogKeys = Object.keys(dialogs);
            for(var k=0; k<dialogKeys.length; ++k){
                var dialog = dialogs[dialogKeys[k]];
                if(dialog.visible){
                    dialog.hide();
                }
                if(dialog.saveState){
                    StorageService.state.save(dialog.saveState.name, dialog._scope, dialog.saveState.fields)
                }
            }
            for(var l=0; l<$scope.job.DispatchLocation.length; ++l){
                if($scope.job.DispatchLocation[l].pin){
                    $scope.job.DispatchLocation[l].pin = null;
                    delete $scope.job.DispatchLocation[l].pin;
                }
            }
            Smartpick.reset();
            Smartpick.hide();
        });

        var init = function(){
            $scope.parent = mainNav.topPage.pushedOptions.parent;
            $scope.job = $scope.parent.job;
            $scope.job.DispatchLocation = Utils.enforceArray($scope.job.DispatchLocation);
            $scope.filteredLocations = $scope.job.DispatchLocation
            setUpFilters();
            
            initMap();
            load();
        }

        $timeout(init);
    }]);
    
})();
(function(){
    'use strict';

    angular.module('app').controller('CompleteJobController', ['$scope', 'ApiService', 'Utils', '$rootScope', '$timeout', '$anchorScroll', 'TECompletionService', 'Smartpick', 'UploadDialog', function($scope, ApiService, Utils, $rootScope, $timeout, $anchorScroll, TECompletionService, Smartpick, UploadDialog) {
        $scope.job = $scope.$parent.current.job;
        $scope.job.changed = false;
        $scope.adding = false;
        $scope.design = $scope.job.design;
        if(!$scope.job.ActiveJobDesignID){
            $scope.job.ActiveJobDesignID = $scope.design.ID;
        }
        $scope.module = $scope.$parent.current.module;
        $scope.showLoadMore = $scope.design.Location.length==$scope.design.TotalCount?false:true;
        $scope.infoDialog = {};
        var uploadsDialog;
        var currentLocationRow = null;
        var locked = false;
        ons.createDialog('app/dialog/location-info-dialog.html').then(function(dialog){
            $scope.infoDialog.dialog = dialog;
        });
        Smartpick.reset();

        var getDataObj = function(){
            var obj = {}
            if($scope.job.ActiveJobDesignID){
                obj.design = $scope.job.ActiveJobDesignID
            }else{
                obj.job = $scope.job.JobID;
                obj.getDesign = true;
            }
            return obj;
        }
        var loadCompletionJob = function(includeCompleted){
            $scope.working = true;
            var obj = getDataObj()
            if(includeCompleted)
                obj.complete = true;
            ApiService.get('jobCompletion', obj).then(result, result)
        }

        var cleanLocation = function(location){
            location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
            location.PercentComplete = Utils.fixPercent(location.PercentComplete);
            return location;
        };
        var cleanResponse = function(design){
            design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
            if(!design.Location)
                design.Location = [];
            if(!Array.isArray(design.Location))
                design.Location = [design.Location];
            design.Location.map(cleanLocation);
            design.PercentComplete = Utils.fixPercent(design.PercentComplete);
            if($scope.job.Incomplete !== '' && (design.IsComplete||design.IsComplete===false)){
                $scope.job.Incomplete = !design.IsComplete;
            }
            return design;
        };

        var result = function(resp){
            $scope.working = false;
            if(resp && resp.JobDesign){
                var p = $scope.design.process;
                $scope.job.design = cleanResponse(resp.JobDesign);
                $scope.job.design.process = p;
                $scope.design = $scope.job.design;
                $scope.showLoadMore = $scope.design.Location.length==$scope.design.TotalCount?false:true;
                $scope.design.lastLocationFetch = new Date().getTime();
                if(!$scope.job.ActiveJobDesignID){
                    $scope.job.ActiveJobDesignID = $scope.design.ID
                }
                locked = true;
            }else{
                if(resp && resp.locked && resp.message){
                    ons.notification.alert(resp.message, {title: "Job Locked"});
                }else{
                    ons.notification.alert("Failed to load job data", {title: "Error"});
                }
                mainNav.popPage();
            }
        };

        $scope.loadMore = function(){
            loadCompletionJob(true);
        }

        $scope.goToLocation = function(index, location, $event){
            if($scope.adding)
                return;
            currentLocationRow = $event?$event.currentTarget:document.getElementById('anchor'+location.JobLocationID);
            $scope.current.design = $scope.design;
            $scope.current.showPhotoUpload = showPhotoUpload;
            Smartpick.reset()
            mainNav.pushPage('app/completework/complete-work-location.html', { locationIndex: index });
        };

        $scope.getLocationClass = function (location){
            var cls = '';
            if(location.FailedInspectionFlag){
                cls += ' red';
            }else if(location.ActiveFlag && !location.Complete){
                cls += ' blue'
            }
            if(!location.ActiveFlag || $scope.adding){
                cls += ' faded'
            }
            return cls;
        };

        var navigateToNewItem = function(location){
            var locationID = 'anchor'+location.JobLocationID;
            $timeout(function() {
                $anchorScroll(locationID);
                Utils.flash(locationID)
            });
        };
        var addLocationFinalResult = function(resp){
            $scope.adding = false;
            if(resp){
                if(resp.JobDesign){
                    if(!Array.isArray(resp.JobDesign.Location))
                        resp.JobDesign.Location = [resp.JobDesign.Location];
                    resp.JobDesign.Location.forEach(function(l){
                        $scope.design.Location.push(cleanLocation(l));
                    });
                    $scope.design.PercentComplete = Utils.fixPercent(resp.JobDesign.PercentComplete);
                    $scope.goToLocation($scope.design.Location.length-1, resp.JobDesign.Location[0])
                }else if(resp.locked){
                    ons.notification.alert(resp.message, {title: "Error"});
                }
            }
        }
        var addLocationFinal = function(type, form){
            $scope.adding = true;
            var obj = {
                design: $scope.job.ActiveJobDesignID,
                NumberChar: form.NumberChar,
                ReferenceNumber: form.ReferenceNumber,
                Description: form.Description,
                TypeCode: type.SmartCode
            };
            ApiService.post('locationCompletion', obj).then(addLocationFinalResult, addLocationFinalResult)
        };
        $scope.addLocation = function(){
            $scope.addLocationDialog.openDialog();
        }

        $scope.findLocation = function(){
            if(Smartpick.ok()){
                Smartpick.show()
            }else{
                Smartpick.show({
                    endpoint: 'locationSearch',
                    endpointData: {
                        design: $scope.job.ActiveJobDesignID,
                    },
                    itemView: 'JobLocation',
                    itemTemplate: 'completion-find-location.html',
                    title: 'Find Location',
                    minSearchLength: 1
                }, function(item){
                    var index = $scope.design.Location.findIndex(function(l){ return l.JobLocationID == item.JobLocationID })
                    if(index == -1){
                        index = $scope.design.Location.length
                        $scope.design.Location.push(item);                   
                    }
                    var location = $scope.design.Location[index]
                    $timeout(function(){
                        navigateToNewItem(location)
                        if(location.ActiveFlag&&!location.Complete){
                            $scope.goToLocation(index, $scope.design.Location[index])
                        }
                    }) 
                });
            }
        }


        $scope.locationOnLongPress = function(location) {
            if($scope.adding)
                return;
            navigator.vibrate(10);
            location.JobBillingUponCode = $scope.design.JobBillingUponCode;
            $rootScope.$emit('LocationDialog', {type:'init', location: location, endpoint: 'locationCompletion'});
            Utils.openDialog($scope.infoDialog.dialog);
        }

        $scope.getLocationNumber = function(location){
            return location.NumberChar;
        }

        $scope.getReferenceNumber = function(location){
            return location.ReferenceNumber?(location.ReferenceNumber):'';
        }

        var uploadResult = function(resp){
            $scope.adding = false;
            uploadsDialog.setOpening(false)
            if(resp && resp.Key){
                $scope.design.DesignPhoto.push({String:resp.Key,Description:resp.Description});
                var uploads = $scope.design.DesignPhoto.map(function(photo){
                    return {Key: photo.String, Description: photo.Description}
                });
                if(uploadsDialog.visible){
                    uploadsDialog.setUploads(uploads);
                }else{
                    uploadsDialog.open(uploads);
                }
                
            }
        }
        var showPhotoUpload = function(precall, callback, locationID){
            UploadDialog.show({save: function(data){
                var obj = {design: $scope.job.ActiveJobDesignID}
                if(data.description) obj.description = data.description;
                if(locationID) obj.location = locationID;
                ApiService.getFormData(obj, {photo:data.photoUrl}).then(function(formdata){
                    precall()
                    ApiService.post('completePhoto', obj, '', '', formdata)
                    .then(callback, callback);
                });
                UploadDialog.hide();
            }});
        }
        var uploadPrecall = function(){
            $scope.adding = true;
            if(uploadsDialog.visible){
                uploadsDialog.setOpening(true)
            }
        }
        $scope.openPhotos = function(){
            if(!$scope.design.DesignPhoto.length){
                showPhotoUpload(uploadPrecall, uploadResult);
            }else{
                uploadsDialog.open($scope.design.DesignPhoto.map(function(photo){
                    return {Key: photo.String, Description: photo.Description}
                }));
            }
        }

        var init = function(){
            locked = true
            $scope.design.DesignPhoto = Utils.enforceArray($scope.design.DesignPhoto);
            $timeout(function(){
                ons.findComponent('#back-button')._element[0].onClick = $scope.closeJob;
            },1);
        };

        $scope.endDay = function(){
            $scope.closing = true;
            closeJob();
        }
        var closeJobResult = function(resp){
            locked = false;
            if($scope.design.process && $scope.design.process.name == 'EndDay'){
                TECompletionService.checkNextJob($scope, $scope.design.process, function(){
                    $scope.loading = false;
                    $scope.working = false;
                });
            }else{
                mainNav.popPage();
            }
        };
        var closeJob = function(skipActuals){
            $scope.working = true;
            var obj = {design: $scope.design.ID, job: $scope.job.JobID};
            if(skipActuals)
                obj.skipActuals = true;
            ApiService.post('completionClose', obj).then(closeJobResult,closeJobResult);
            $scope.closing = false;
        };
        $scope.closing = false;

        $scope.closeJob = function(){
            if($scope.design.process && $scope.design.process.name == 'EndDay'){
                $scope.endDay();
                return;
            }
            if(!$scope.job.changed){
                closeJob(true);
                return;
            }
            if($scope.closing || (mainNav.pages[mainNav.pages.length-1].pushedOptions.page != 'app/completework/complete-work-job.html')) return;
            $scope.closing = true;
            Utils.notification.confirm("Close job and update actuals?", {
                callback: function(ok){
                    if(ok){
                        closeJob();
                    }else{
                        $scope.closing = false;
                    }
                }
            });
        };

        var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', $scope.closeJob, false);
            }else{
                document.removeEventListener('backbutton', $scope.closeJob);
                ons.enableDeviceBackButtonHandler();
            }
        }
        $scope.hidePage = function(){
            toggleBackButton(false);
            Utils.clearBackButtonHandlers()
        }
        $scope.showPage = function(){
            toggleBackButton(true);
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })
            
            if(currentLocationRow){
                currentLocationRow.ripple();
                currentLocationRow = null;
            }
            if($scope.current.nextAction == 'closeJob'){
                delete $scope.current.nextAction;
                $scope.closing = true;
                closeJob();
            }
        }

        $scope.$on('$destroy', function(){
            if($scope.infoDialog.dialog.visible){
                $scope.infoDialog.dialog.hide();
            }
            if($scope.addLocationDialog.visible){
                $scope.addLocationDialog.hide();
            }
            if(locked){
                ApiService.post('completionClose', {design: $scope.job.ActiveJobDesignID});
            }
            Smartpick.hide()
            Smartpick.reset()
            UploadDialog.hide();
            uploadsDialog.hide();
            $rootScope.$emit('LocationDialog', {type:'destroy'});
        });

        init();


        //Uploads dialog
        ons.createDialog('app/dialog/uploads/uploads-dialog.html').then(function(dialog){
            dialog._scope.canEdit = true;
            dialog._scope.title = 'Job Photos';
            dialog.open = function(uploads){
              dialog._scope.openingUpload = false;
              dialog.setUploads(uploads);
              Utils.openDialog(dialog);
            };
            dialog.setUploads = function(uploads){
              dialog._scope.uploads = uploads;
            }
            dialog.setOpening = function(opening){
                dialog._scope.openingUpload = opening;
            }
            dialog._scope.addUpload = function(){
                showPhotoUpload(uploadPrecall, uploadResult);
            }
            dialog._scope.captionEdited = function(upload){
                var photo;
                if(photo = $scope.design.DesignPhoto.find(function(dp){ return dp.String == upload.Key})){
                    photo.Description = upload.Description;
                }
            }
            uploadsDialog = dialog;
        });

        ons.createAlertDialog('app/completework/create-location-dialog.html').then(function(dialog){
            $scope.addLocationDialog = dialog;
            var addLocationFetchResult = function(resp){
                if(resp && resp.SmartCodes){
                    $scope.addLocationDialog.setData(resp);
                }else{
                    if(resp.locked){
                        ons.notification.alert(resp.message, {title: "Error"});
                    }else{
                        ons.notification.alert("Failed to load location data", {title: "Error"});
                    }
                    dialog.hide()
                }
            }
            $scope.addLocationDialog.openDialog = function(){
                dialog._scope.title = 'New Location';
                $scope.addLocationDialog._scope.form = {
                    NumberChar: '',
                    Description: '',
                    ReferenceNumber: '-'
                };
                if(!dialog._scope.data){
                    var obj = {design: $scope.job.ActiveJobDesignID};
                    ApiService.post('locationCompletion', obj).then(addLocationFetchResult, addLocationFetchResult);
                }
                Utils.openDialog(dialog)
            }
            $scope.addLocationDialog.setData = function(data){
                $scope.addLocationDialog._scope.data = data;
                $scope.addLocationDialog.defaultSelection();
            }
            $scope.addLocationDialog.defaultSelection = function(){
                $scope.addLocationDialog._scope.form = {
                    NumberChar: '',
                    Description: '',
                    ReferenceNumber: '-'
                };
                if($scope.addLocationDialog._scope.data){
                    var type = $scope.addLocationDialog._scope.data.SmartCodes.find(function(t){
                        return t.SmartCode == 'S';
                    });
                    if(type){
                        $scope.addLocationDialog._scope.selects.type = type;
                    }
                }
            }
            $scope.addLocationDialog._scope.tagHandler = function(tag){
                return null;
            };
            var getLocationNumber = function(num){
                if($scope.addLocationDialog._scope.data.Prefix != '')
                    return $scope.addLocationDialog._scope.data.Prefix + num;
                return num;
            }
            
            $scope.addLocationDialog._scope.selects = {type:{}};
            $scope.addLocationDialog._scope.done = function(add, type, form){
                if(add && type){
                    addLocationFinal(type, form);
                }
                $scope.addLocationDialog.hide();
            };
        });
    }]);

})();
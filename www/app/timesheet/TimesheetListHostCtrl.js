(function(){
    'use strict';

    angular.module('app').controller('TimesheetListHostController', ['$scope', '$timeout', 'ApiService', 'StorageService', '$rootScope', 'Utils', function($scope, $timeout, ApiService, StorageService, $rootScope, Utils) {
        var staticData = {
            'TimeSheet': {title:'General', icon:'ion-person', borderClass: 'tab-underline-blue', toolbarClass:'gt'},
            'ForemanTimesheet': {title:'Foreman', icon:'ion-hammer', borderClass: 'tab-underline-orange'}
        }
        $scope.data = {
            loaded: false,
            openingTimesheet: false,
            working: true,
            hasTimesheets: false,
            tabs: []
        }
        $scope.styles = {
            tabbar: 'display: block; overflow-x: auto !important; overflow-y: hidden !important;',
            tab: {'display': 'inline-block','box-sizing': 'border-box'},
            tabInner: 'padding: 0px 10px;'
        };
        $scope.module = $scope.$parent.current.module;
        $scope.title = $scope.module.title;
        $scope.tabIndex = 0;

        $scope.loadTimesheets = function($done){
            var obj = {
                types: $scope.module.options.lists.join(','),
                module: $scope.module.key
            };
            ApiService.get('jobs', obj).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var cleanTimesheets = function(user, tabKey){
            var now = new Date();
            $scope.data.hasTimesheets = $scope.data.hasTimesheets||user[tabKey].length > 0;
            var data = {
                crewNameMap: {},
                timesheets: Utils.enforceArray(user[tabKey]),
                dates: {}
            }
            for(var t=0; t<data.timesheets.length; ++t){
                data.timesheets[t].StartDate = Utils.date(data.timesheets[t].StartDate)
                data.timesheets[t].EndDate = Utils.date(data.timesheets[t].EndDate)
                if(now >= data.timesheets[t].StartDate && now <= data.timesheets[t].EndDate){
                    data.timesheets[t].isCurrent = true;
                }
                data.crewNameMap[(data.timesheets[t].CrewName||data.timesheets[t].FullName)] = true
                if(data.timesheets[t].ForemanID && data.timesheets[t].ForemanID != user.MyPersonID){
                    data.notMyTimesheet = true
                }
                data.dates[data.timesheets[t].EndDate.getTime()] = (data.dates[data.timesheets[t].EndDate.getTime()]||0)+1
                data.timesheets[t].title = data.timesheets[t].isMyTimesheet?(Utils.date(data.timesheets[t].StartDate, 'MM/dd/yyyy') +' - '+ Utils.date(data.timesheets[t].EndDate, 'MM/dd/yyyy')):(data.timesheets[t].CrewName||data.timesheets[t].FullName);
            }
            return data;
        }
        var precleanTimesheetData = function(data){
            data.MyTimesheet = Utils.enforceArray(data.MyTimesheet);
            for(var t=0; t<data.MyTimesheet.length; ++t){
                data.MyTimesheet[t].isMyTimesheet = true;
                if(data.MyTimesheet[t].TimesheetType == 'TE'){
                    if(!data.ForemanTimesheet) data.ForemanTimesheet = []
                    data.ForemanTimesheet.push(data.MyTimesheet[t]);
                }else{
                    if(!data.TimeSheet) data.TimeSheet = []
                    data.TimeSheet.push(data.MyTimesheet[t]);
                }
            }
            delete data.MyTimesheet;
        }

        var setTabDisplayOptions = function(tab, data, user){
            if(data.notMyTimesheet){
                for(var t=0; t<tab.timesheets.length; ++t){
                    tab.timesheets[t].title = (tab.timesheets[t].CrewName||tab.timesheets[t].FullName);
                }
                tab.showCrewName = true
            }else if(Object.keys(data.dates).find(function(dateKey){ return data.dates[dateKey] > 1})){
                for(var t=0; t<tab.timesheets.length; ++t){
                    tab.timesheets[t].title = tab.timesheets[t].CrewType;
                }
                tab.showCrewName = true
            }
        }
        

        var result = function(resp){
            if(resp && resp.User){
                if(resp.User.MyTimesheet){
                    precleanTimesheetData(resp.User);
                }
                var keys = Object.keys(staticData);
                for(var i=0; i<keys.length; i++){
                    if(resp.User[keys[i]]){
                        var data = cleanTimesheets(resp.User, keys[i]);
                        if(data.timesheets.length){
                            var tab = addTab(keys[i], staticData[keys[i]].title, staticData[keys[i]].icon, $scope.module.options.detailPage, false);
                            tab.borderClass = staticData[keys[i]].borderClass;
                            tab.toolbarClass = staticData[keys[i]].toolbarClass;
                            tab.timesheets = data.timesheets;
                            if($scope.module.key =='MyTimesheets')setTabDisplayOptions(tab, data, resp.User)
                        }
                    }
                }
            }else{
                $scope.data.hasTimesheets = false;
            }
            $scope.data.working = false;
        }

        var checkOverflowX = function(el){
           var curOverflow = el.style.overflowX;
           if ( !curOverflow || curOverflow === "visible" )
              el.style.overflowX = "hidden";
           var isOverflowing = el.clientWidth < el.scrollWidth;
           el.style.overflowX = curOverflow;
           return isOverflowing;
        };

        var checkTabbarWidth = function(){
            var tb = tabbar._element[0];
            var inner = tb.getElementsByClassName('tab-bar');
            if(inner.length){
                var tabsInner = inner[0].getElementsByClassName('tab-bar__label');
                for(var i=0; i<tabsInner.length; i++){
                    tabsInner[i].setAttribute('style', $scope.styles.tabInner);
                }
                inner[0].setAttribute('style', $scope.styles.tabbar);
                if(!checkOverflowX(inner[0])){
                    inner[0].setAttribute('style', '');
                    for(var i=0; i<tabsInner.length; i++){
                        tabsInner[i].setAttribute('style', '');
                    }
                    $scope.styles.tab = {};
                }
            }
        };

        var addTab = function(key, title, icon, page, active){
            var tab = {key: key, title: title, icon: icon, page: page, active: active, timsheets:[]};
            $scope.data.tabs.push(tab);
            return tab;
        }

        $scope.getStatusClass = function(code){
            switch(code){
                case 'SB':
                case 'Submitted':
                    return 'timesheet-blue';
                break;
                case 'RJ':
                case 'Rejected':
                case 'IP':
                case 'In Process':
                    return 'timesheet-red';
                break;
                case 'ZH':
                case 'Zero Hours':
                case 'AP':
                case 'Approved':
                    return 'timesheet-black';
                break;
                case 'RV':
                case 'Reviewed':
                    return 'timesheet-green';
                break;
                case 'PE':
                case 'Pending':
                    return 'timesheet-gray';
            }
            return '';
        }


        $scope.openTimesheet = function(timesheet){
            if($scope.data.openingTimesheet)
                return;
            $scope.current.timesheet = timesheet;
            $scope.current.tab = $scope.data.tabs[tabbar.getActiveTabIndex()];
            mainNav.pushPage($scope.module.options.detailPage[$scope.current.tab.key]);
        }


        var getTabKey = function(){
            return $scope.data.tabs[tabbar.getActiveTabIndex()].key;
        }

        var updateBorderColor = function(index){
            var border = angular.element(tabbar._element[0]._tabbarBorder);
            var title = angular.element(document.getElementById('timesheet-toolbar'));
            if($scope.data.tabs && index < $scope.data.tabs.length){
                border.removeClass('tab-underline-blue');
                border.removeClass('tab-underline-orange');
                border.addClass($scope.data.tabs[index].borderClass);
                title.removeClass('gt');
                if($scope.data.tabs[index].toolbarClass){
                    title.addClass($scope.data.tabs[index].toolbarClass);
                }
            }

        }
        $scope.init = function(){
            $timeout(updateBorderColor(0))
            tabbar.on('prechange', function(event){
                updateBorderColor(event.index)
            })
        };

        var saveStateFields = ['tabs','data', 'tabIndex', 'hasTimesheets'];
        $scope = StorageService.state.load('TimesheetListHostController', $scope, saveStateFields);
        if(!$scope.data.hasTimesheets)
            $scope.loadTimesheets();
        $timeout(function(){
            $scope.loaded = true;
        });
        
        ons.orientation.on('change', function(){
            $timeout(checkTabbarWidth,0);
        });

        $scope.$on('$destroy', function(){
            StorageService.state.clear('TimesheetListHostController');
            ons.orientation.off('change');
        });
    }]);

})();
(function(){
    'use strict';

    angular.module('app').service('TimeEntryService', ['Utils', '$filter', 'ApiService', 'FTESaveService', '$timeout', function(Utils, $filter, ApiService, SaveService, $timeout){
        
        
        var _pcInfoCache = {};


        var _dialog = null;
        var _setDialog = function(dialog){
          _dialog = dialog;
        }

        var _showMessage = function(msg, title, buttons){
            $timeout(function(){
                if(_dialog){
                    _dialog.open({
                        title: title||'Notice',
                        content: msg,
                        buttons: buttons||[{
                            label: 'Ok'
                        }],
                        cancelable: true
                    })
                }
            });
        }
        var _showHTMLMessage = function(msg, title, buttons){
            $timeout(function(){
                if(_dialog){
                    _dialog.open({
                        title: title||'Notice',
                        html: msg,
                        buttons: buttons||[{
                            label: 'Ok'
                        }],
                        cancelable: true
                    })
                }
            });
        }

        var _changes = function(workweek, changes){
            if(!changes){
                workweek.changes = false;
            }else if(!workweek.changes && !['SB','AP','RV'].includes(workweek.WorkDay[workweek.currentDay].StatusCode)){
                workweek.changes = true;
            }
        }

        var _isDayEditable = function(day, workweek){
            return workweek.AllowEdit && ['PE','IP','RJ','ZH'].includes(day.StatusCode);
        }
        var _currentDay = function(workweek){
            return workweek.WorkDay[workweek.currentDay];
        }
        var _currentMember = function(workweek){
            var day = _currentDay(workweek);
            return day.members[day.currentMember||0];
        }
        var _currentStep = function(workweek, day){
            day = day||_currentDay(workweek);
            return day.DayProcessCompletion[day.currentStep];
        }

        var _dayByDate = function(workweek, date){
            var dateTime = date.getTime();
           for(var i=0; i<workweek.WorkDay.length; ++ i){
               if(Utils.date(workweek.WorkDay[i].WeekdayDate).getTime() == dateTime){
                   return workweek.WorkDay[i];
               }
           }
        }

        var _cleanDialog = function(workweek, restore){
            if(!workweek.dialog1 || !workweek.dialog1.visible) return;
            if(workweek.dialog1.which == 'memberInfo'){
                if(restore){
                    workweek.dialog1.member = _currentDay(workweek).members.find(function(dm){ return dm.PersonID == workweek.dialog1.member.PersonID && dm.ClassID == workweek.dialog1.member.ClassID });
                }else{
                    workweek.dialog1.member = Utils.pick(workweek.dialog1.member, 'PersonID', 'ClassID');
                }
            }
            if(workweek.dialog1.which == 'equipInfo'){
                if(restore){
                    workweek.dialog1.equip = _currentDay(workweek).equipment.find(function(de){ return de.EquipmentID == workweek.dialog1.equip.EquipmentID });
                }else{
                    workweek.dialog1.equip = Utils.pick(workweek.dialog1.equip, 'EquipmentID');
                }
            }
        }

        var _cleanPhaseCode = function(pc, task, jdc, step){
            pc.active = true;
            pc.EarnedHours = 0;
            pc.TotalHours = _total(pc);
            if(jdc.cwPhaseCodes[pc.ID]){
                pc.EarnedHours = jdc.cwPhaseCodes[pc.ID].earned;
            }
            if(step < 3 && !_isLocationJob(jdc) && jdc.UseDesignFlag && task.JobTaskSystemCode == 'UN' && pc.EarnedHours == 0 && jdc.hasEarnedHours && jdc.StatusCode == 'EPE'){
                return false;
            }
            return true;
        }
        var _setTaskPerDiemEligible = function(task, jobTask, isMember){
            var eligible = true;
            if(isMember){
                if(task.PhaseCodeSectionID){
                    eligible = task.MemberTaskPhaseCode[0].PhaseCodePerDiemEligibleFlag;
                }
            }else{
                if(jobTask && jobTask.PhaseCodeSectionID){
                    eligible = jobTask.JobTaskPhaseCode[0].PhaseCodePerDiemEligibleFlag;
                }
            }
            task.perDiemEligible = eligible;
        }
        var _cleanTask = function(task, jdc, workweek){
            var step = _currentDay(workweek).currentStep;
            task.StartMinute = task.StartMinute||0;
            task.EndMinute = task.EndMinute||0;
            task.StartTime = task.StartTime?Utils.date(task.StartTime):_dateFromHourAndMinute(task.StartDate, task.StartHour, task.StartMinute);
            task.EndTime = task.EndTime?Utils.date(task.EndTime):_dateFromHourAndMinute(task.EndDate, task.EndHour, task.EndMinute);
            task.PhaseCode = Utils.enforceArray(task.PhaseCode);
            for(var p=task.PhaseCode.length-1; p>=0; --p){
                if(!_cleanPhaseCode(task.PhaseCode[p], task, jdc, step)){
                    task.PhaseCode[p].active = false;
                }
            }
            if(jdc.UseDesignFlag && task.JobTaskSystemCode == 'UN'){
                for(var id in jdc.cwPhaseCodes){
                    var existing = task.PhaseCode.find(function(pc){ return pc.ID == id});
                    if(!existing){
                        task.PhaseCode.push(_getPhaseCodeFromJTPC(jdc.cwPhaseCodes[id], workweek));
                    }
                }
            }
            _sortPhases(task, 'PhaseCode');
            if(task.LocationID){
                _updateTaskPhoto(task, workweek);
            }
            var jobTask = jdc.JobTask.find(function(jt){ return jt.ID == task.JobTaskID });
            if(jobTask){
                task.JobTaskNonCompensatedFlag = jobTask.NonCompensatedFlag;
            }
            _setTaskPerDiemEligible(task, jobTask);
            task.active = typeof task.active == 'undefined'?true:task.active;
        }
        var _setUpJobTaskEarnedHours = function(jobTask, jdc, workweek){
            if(!jdc.UseDesignFlag || jobTask.SystemCode != 'UN') return;
            for(var p=jobTask.JobTaskPhaseCode.length-1; p>=0; --p){
                var pc = jobTask.JobTaskPhaseCode[p];
                if(jdc.cwPhaseCodes[pc.PhaseCodeID]){
                    pc.earned = jdc.cwPhaseCodes[pc.PhaseCodeID].earned;
                }
            }
            for(var id in jdc.cwPhaseCodes){
                var existing = jobTask.JobTaskPhaseCode.find(function(pc){ return pc.PhaseCodeID == id});
                if(!existing){
                    jobTask.JobTaskPhaseCode.push(Object.assign({ID: _getTempID(workweek)}, jdc.cwPhaseCodes[id]));
                }
            }
            _sortPhases(jobTask, 'JobTaskPhaseCode');
        }
        var _sortPhases = function(obj, key){
            var sectionField = key=='PhaseCode'?'SectionSortOrder':key=='MemberTaskPhaseCode'?'PhaseCodeSectionSortOrder':'PhaseCodeSectionSortOrder2',
                phaseField = key=='PhaseCode'?'SortOrder':'PhaseCodeSortOrder';
            obj[key].sort(function(a,b){
                if(a[sectionField] != b[sectionField]) return a[sectionField]-b[sectionField];
                return a[phaseField]-b[phaseField];
            });
        }
        var _cleanJobTask = function(jobTask, jdc, workweek){
            jobTask.JobTaskPhaseCode = Utils.enforceArray(jobTask.JobTaskPhaseCode);
            _setUpJobTaskEarnedHours(jobTask, jdc, workweek);
        }
        
        var _cleanMemberPhaseCodeTask = function(workweek, mtpc){
            if(typeof mtpc.active == 'undefined'){
                mtpc.active = true;
            }
        }
        var _cleanMemberTask = function(workweek, memberTask){
            if(typeof memberTask.active == 'undefined'){
                memberTask.active = true;
            }
            memberTask.StartMinute = memberTask.StartMinute||0;
            memberTask.EndMinute = memberTask.EndMinute||0;
            memberTask.StartTime = memberTask.StartTime?Utils.date(memberTask.StartTime):_dateFromHourAndMinute(memberTask.StartDate, memberTask.StartHour, memberTask.StartMinute);
            memberTask.EndTime = memberTask.EndTime?Utils.date(memberTask.EndTime):_dateFromHourAndMinute(memberTask.EndDate, memberTask.EndHour, memberTask.EndMinute);
            memberTask.MemberTaskPhaseCode = Utils.enforceArray(memberTask.MemberTaskPhaseCode);
            for(var p=0; p<memberTask.MemberTaskPhaseCode.length; ++p){
                _cleanMemberPhaseCodeTask(workweek, memberTask.MemberTaskPhaseCode[p]);
            }
            _setTaskPerDiemEligible(memberTask, null, true);
        }
        var _isTimeEntryBilled = function(te){
            return te.BatchID || te.BatchDetailID || te.StatusCode == 'BI'
        }
        var _cleanTimeEntry = function(workweek, te, member){
            if(te.TimeDate) te.TimeDate = Utils.date(te.TimeDate);
            if(te.AcceptedDateTime) te.AcceptedDateTime = Utils.date(te.AcceptedDateTime);
            if(!member.MemberStatusDraftCrewTimeFlag){
                te.TimeInHours = te.BillableHours = 0;
            }
        }
        var _cleanExpenses = function(member, day){
            member.ExpenseEntry = Utils.enforceArray(member.ExpenseEntry);
            member.PerDiemAmount = 0;
            member.PerDiemBillableAmount = 0;
            member.PerDiemContractAmount = 0;
            member.Meals = 0
            for(var e=0; e<member.ExpenseEntry.length; ++e){
                var entry = member.ExpenseEntry[e];
                if(entry.AcceptedDateTime)entry.AcceptedDateTime = Utils.date(entry.AcceptedDateTime);

                if(entry.TypeCode == 'PD'){//Per Diem
                    if((entry.active||(typeof entry.active == 'undefined'))){
                        entry.active = true;
                        member.PerDiemAmount = entry.Amount;
                        member.PerDiemBillableAmount = entry.BillingAmount;
                        member.PerDiemContractAmount = entry.ContractAmount;
                    }else{
                        entry.active = false;
                    }
                }else if(entry.TypeCode == 'ME'){//Meals
                    if((entry.active||(typeof entry.active == 'undefined'))){
                        entry.active = true;
                        member.Meals = entry.Qty;
                    }else{
                        entry.active = false;
                    }
                }
                if(day && !day.HasBilled && _isTimeEntryBilled(entry)){
                    day.HasBilled = true;
                }
            }
        }
        var _cleanMember = function(workweek, member, crew, day){
            _mapSmartpick(workweek, member, 'jobStatus', _spMappings.jobStatus);
            _mapSmartpick(workweek, member, 'craft', _spMappings.craft);
            _mapSmartpick(workweek, member, 'class', _spMappings.class);
            _mapSmartpick(workweek, member, 'inactiveReason', _spMappings.inactiveReason, 'memberInactiveReason');
            _mapSmartpick(workweek, member, 'noPerDiem', _spMappings.noPerDiemReason, 'noPerDiemReason');
            if(member.MemberTask){
                member.MemberTask = Utils.enforceArray(member.MemberTask);
                for(var t=0; t<member.MemberTask.length; ++t){
                    _cleanMemberTask(workweek, member.MemberTask[t]);
                }
            }
            member.TimeEntry = Utils.enforceArray(member.TimeEntry);
            for(var te=0; te<member.TimeEntry.length; ++te){
                _cleanTimeEntry(workweek, member.TimeEntry[te], member);
                if(!day.HasBilled && _isTimeEntryBilled(member.TimeEntry[te])){
                    day.HasBilled = true;
                }
            }
            _cleanExpenses(member, day);
            if(crew && crew.active && crew.OrderNumber == 1){
                _calcHasPermanentMemberChanges(member, workweek, crew);
            }
        }
        var _cleanEquip = function(workweek, equip, crew, day){
            _mapSmartpick(workweek, equip, 'inactiveReason', _spMappings.inactiveReason, 'equipmentInactiveReason');
            equip.EquipmentTimeEntry = Utils.enforceArray(equip.EquipmentTimeEntry);
            for(var te=0; te<equip.EquipmentTimeEntry.length; ++te){
                if(equip.EquipmentTimeEntry[te].AcceptedDateTime){
                    equip.EquipmentTimeEntry[te].AcceptedDateTime = Utils.date(equip.EquipmentTimeEntry[te].AcceptedDateTime);
                }
                if(!day.HasBilled && _isTimeEntryBilled(equip.EquipmentTimeEntry[te])){
                    day.HasBilled = true;
                }
            }
            if(crew && crew.active && crew.OrderNumber == 1){
                _calcHasPermanentEquipChanges(equip, workweek, crew);
            }
        }
        var _cleanCompletedWork = function(jdc, workweek){
            jdc.CompletedWork = Utils.enforceArray(jdc.CompletedWork);
            if(!jdc.CompletedWork.length && jdc.cwPhaseCodes) return;
            jdc.cwPhaseCodes = jdc.cwPhaseCodes||{};
            jdc.cwLocations = {};
            for(var cw=0; cw<jdc.CompletedWork.length; ++cw){
                var spec = jdc.CompletedWork[cw];
                var id = Utils.number(spec.SpecificationActionPhaseCodeID);
                if(!jdc.cwPhaseCodes[id]){
                    jdc.cwPhaseCodes[id] = {
                        earned: 0,
                        locations: {},
                        ActiveFlag: true,
                        PhaseCodeID: spec.SpecificationActionPhaseCodeID,
                        PhaseCodeDisplayNumber: spec.PhaseCodeDisplayNumber,
                        PhaseCodeCodeNumber: spec.PhaseCodeCodeNumber,
                        PhaseCodeDescription: spec.PhaseCodeDescription,
                        PhaseCodeSortOrder: spec.PhaseCodeSortOrder,
                        PhaseCodeSectionSortOrder2: spec.PhaseCodeSectionSortOrder,
                        PhaseCodeSectionOrgDepartmentID2: spec.PhaseCodeSectionOrgDepartmentID,
                        PhaseCodeSectionOrganizationID2: spec.PhaseCodeSectionOrganizationID,
                        PhaseCodeSectionSectionNumber2: spec.PhaseCodeSectionSectionNumber,
                        PhaseCodeOverrideNumber: spec.PhaseCodeOverrideNumber,
                        PhaseCodeOverrideTypeCode: spec.PhaseCodeOverrideTypeCode,
                        OverrideTypeMeaning2: spec.OverrideTypeMeaning,
                        PhaseCodeIncludeInOTCalcFlag: spec.PhaseCodeIncludeInOTCalcFlag
                    }
                }
                if(spec.LocationDetailJobLocationID){
                    spec.LocationDetailJobLocationID = Utils.number(spec.LocationDetailJobLocationID);
                    if(!jdc.cwLocations[spec.LocationDetailJobLocationID]){
                        jdc.cwLocations[spec.LocationDetailJobLocationID] = true;
                        if(!workweek.jobLocations[jdc.JobCrewJobID]) workweek.jobLocations[jdc.JobCrewJobID] = [];
                        if(!workweek.jobLocations[jdc.JobCrewJobID].find(function(loc){ return loc.ID == spec.LocationDetailJobLocationID})){
                            workweek.jobLocations[jdc.JobCrewJobID].push({
                                ID: spec.LocationDetailJobLocationID,
                                NumberChar: spec.JobLocationLocationNumberChar,
                                SortOrder: spec.JobLocationSortOrder
                            });
                        }
                    }
                    if(typeof jdc.cwPhaseCodes[id].locations[spec.LocationDetailJobLocationID] == 'undefined'){
                        jdc.cwPhaseCodes[id].locations[spec.LocationDetailJobLocationID] = 0
                    }
                    //if(spec.LocationDetailEstimatedQuantity > 0){
                        jdc.cwPhaseCodes[id].locations[spec.LocationDetailJobLocationID] += spec.EarnedHours;
                    //}
                }
                //if(spec.LocationDetailEstimatedQuantity > 0){
                    jdc.cwPhaseCodes[id].earned += spec.EarnedHours;
                //}
            }
            for(var id in jdc.cwPhaseCodes){
                jdc.cwPhaseCodes[id].earned = Utils.round(jdc.cwPhaseCodes[id].earned,2);
                if(jdc.cwPhaseCodes[id].earned > 0){
                    jdc.hasEarnedHours = true;
                }
            }
            jdc.cwLocations = Object.keys(jdc.cwLocations).map(function(lid){ return Utils.number(lid) });
            delete jdc.CompletedWork;
        }
        var _sortMembersOnJob = function(job, scope){
            job.JobDayCrewMember.sort(function(a,b){ 
                if(a.ActiveFlag != b.ActiveFlag) {
                    return a.ActiveFlag?-1:1;
                }
                if(a.CraftSortOrder != b.CraftSortOrder){
                    return a.CraftSortOrder-b.CraftSortOrder;
                }
                if(a.CraftCraft != b.CraftCraft){
                    return a.CraftCraft.localeCompare(b.CraftCraft);
                }
                if(a.ClassSortOrder != b.ClassSortOrder){
                    return a.ClassSortOrder-b.ClassSortOrder;
                }
                if(a.ClassClass != b.ClassClass){
                    return a.ClassClass.localeCompare(b.ClassClass);
                }
                return a.PersonFullName.localeCompare(b.PersonFullName);
            })
            job.JobDayCrewMember.forEach(function(m,i){ m.index = i; });
        }
        var _sortEquipOnJob = function(job){
            job.JobDayCrewEquipment.sort(function(a,b){ 
                if(a.ActiveFlag != b.ActiveFlag) {
                    return a.ActiveFlag?-1:1;
                }
                if(a.CategorySortOrder != b.CategorySortOrder) {
                    return a.CategorySortOrder-b.CategorySortOrder;
                }
                if(a.CategoryCategory != b.CategoryCategory) {
                    return a.CategoryCategory.localeCompare(b.CategoryCategory);
                }
                return a.EquipmentEquipmentNumber.localeCompare(b.EquipmentEquipmentNumber);
            });
        }
        var _determineMemberDuplicity = function(member, personMap){
            if(member != null){
                if(!personMap[member.PersonID]) personMap[member.PersonID] = {parent:null,children:[]}
                if(member.duplicate === true) return;
                if(member.duplicate === false || (member.JobCrewMemberID&&!Utils.isTempId(member.JobCrewMemberID))) return personMap[member.PersonID].parent = member;
                personMap[member.PersonID].children.push(member);
            }else{
                var ids = Object.keys(personMap);
                for(var p=0; p<ids.length; ++p){
                    var person = personMap[ids[p]];
                    if(person.parent && person.children.length){
                        person.parent.duplicate = false;
                        for(var c=0; c<person.children.length; ++c){
                            person.children[c].duplicate = true;
                        }
                    }
                }
            }
        }
        var _getJobBucket = function(job, workweek, phaseId){
            var jobBucket;
            for(var b=0; b<workweek.TimeBucket.length; ++b){
                if(workweek.TimeBucket[b].OverheadJobID == (job.JobCrewJobID||job.ID)){
                    if(!workweek.TimeBucket[b].WGPhaseID){
                        jobBucket = workweek.TimeBucket[b];
                    }else if(!phaseId||workweek.TimeBucket[b].WGPhaseID == phaseId){
                        return workweek.TimeBucket[b];
                    }
                }
            }
            return jobBucket;
        }
        var _getBucketJob = function(bucket, workweek){
          return workweek.WorkDay[0].Job.find(function(j){ return bucket.OverheadJobID == j.ID });
        }

        var _calcTimeBucket = function(workweek, jdc, phaseId, dayIndex, hours){
          var bucket = _getJobBucket(jdc, workweek, phaseId);
          if(bucket){
            bucket.dayTotals[dayIndex] += hours;
          }
        }
        var _resetBuckets = function(workweek){
          for(var b=0; b<workweek.TimeBucket.length; b++){
            workweek.TimeBucket[b].totalPeriod1Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period1Hours;
            workweek.TimeBucket[b].totalPeriod2Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period2Hours;
          }
        }
        var _resetBucketDay = function(workweek, dayIndex){
          for(var b=0; b<workweek.TimeBucket.length; b++){
            workweek.TimeBucket[b].dayTotals[dayIndex] = 0;
          }
        }
        var _sumDayBuckets = function(workweek, dayIndex){
            for(var b=0; b<workweek.TimeBucket.length; b++){
                if(workweek.WorkDay[dayIndex].WeekdayDate > workweek.TimeBucket[b].Period1End){
                    workweek.TimeBucket[b].totalPeriod2Hours -= workweek.TimeBucket[b].dayTotals[dayIndex];
                }else{
                    workweek.TimeBucket[b].totalPeriod1Hours -= workweek.TimeBucket[b].dayTotals[dayIndex];
                }
            }
        }
        var _validateTimeBuckets = function(workweek, scope){
            var dayIndex = workweek.currentDay;
            for(var b=0; b<workweek.TimeBucket.length; b++){
                var which = 1;
                if(workweek.WorkDay[dayIndex].WeekdayDate > workweek.TimeBucket[b].Period1End){
                    which = 2;
                }
                if(workweek.TimeBucket[b]['totalPeriod'+which+'Hours'] < 0 && workweek.TimeBucket[b]['Period'+which+'Hours'] <= workweek.TimeBucket[b].HourLimit){
                    var msg = 'Hours for '+workweek.TimeBucket[b].Name+' cannot exceed '+$filter('number')(workweek.TimeBucket[b].HourLimit, 2)+' '+(workweek.TimeBucket[b].IntervalMeaning.toString().toLowerCase())+'.';
                    _showMessage(msg,'Error');
                    return false;
                }
            }
            return true;
        }
        var _cleanBuckets = function(workweek){
            workweek.TimeBucket = Utils.enforceArray(workweek.TimeBucket);
            for(var b=0; b<workweek.TimeBucket.length; ++b){
              workweek.TimeBucket[b].Period1Start = Utils.date(workweek.TimeBucket[b].Period1Start);
              workweek.TimeBucket[b].Period1End = Utils.date(workweek.TimeBucket[b].Period1End);
              workweek.TimeBucket[b].totalPeriod1Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period1Hours;
              if(workweek.TimeBucket[b].Period2Start){
                workweek.TimeBucket[b].Period2Start = Utils.date(workweek.TimeBucket[b].Period2Start);
                workweek.TimeBucket[b].Period2End = Utils.date(workweek.TimeBucket[b].Period2End);
                workweek.TimeBucket[b].totalPeriod2Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period2Hours;
              }
              if(!workweek.TimeBucket[b].dayTotals){
                  workweek.TimeBucket[b].dayTotals = []
                  for(var d=0; d<7; ++d){
                    workweek.TimeBucket[b].dayTotals[d] = 0;
                    var day = workweek.WorkDay[d];
        
                    var jobs = _activeJobs(workweek, day.WeekdayDate);
                    var editable = _isDayEditable(day, workweek);
                    var useCrewTime = false;
                    if(editable && day.currentStep < 3){
                        useCrewTime = true;
                    }
                    for(var j=0; j<jobs.length; ++j){
                        if(workweek.TimeBucket[b].OverheadJobID == jobs[j].JobCrewJobID){
                            var foremanHours = 0;
                            if(!useCrewTime){
                                for(var m=0; m<jobs[j].JobDayCrewMember.length; ++m){
                                    var member = jobs[j].JobDayCrewMember[m];
                                    if(member.PersonID == workweek.PersonID){
                                        if(!member.ActiveFlag){
                                            useCrewTime = true;
                                            break;
                                        }
                                        for(var te=0; te<member.TimeEntry.length; ++te){
                                            if(member.TimeEntry[te].TimeInHours && (!workweek.TimeBucket[b].WGPhaseID || workweek.TimeBucket[b].WGPhaseID == member.TimeEntry[te].PhaseCodeID)){
                                                _calcTimeBucket(workweek, jobs[j], workweek.TimeBucket[b].WGPhaseID, d, parseFloat(member.TimeEntry[te].TimeInHours)||0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                 }
              }else{
                  workweek.TimeBucket[b].totalPeriod1Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period1Hours;
                  workweek.TimeBucket[b].totalPeriod2Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period2Hours;
              }
            }
            for(var d=0; d<7; ++d){
                _sumDayBuckets(workweek, d);
            }
            _hideBuckets(workweek);
        }
        var _hideBuckets = function(workweek){
          for(var b=0; b<workweek.TimeBucket.length; ++ b){
              if(workweek.TimeBucket[b].VisibleOnFTFlag){
                var job = _getBucketJob(workweek.TimeBucket[b], workweek);
                if(!job){
                  workweek.TimeBucket[b].VisibleOnFTFlag = false;
                }
              }
            }
        }
        var _updateJobPhoto = function(jobDayCrew, job, workweek){
            if(workweek.Photos.Job[job.ID]){
                jobDayCrew.photos = true;
            }else{
                if(job.Design){
                    jobDayCrew.photos = false;
                }else{
                   delete jobDayCrew.photos; 
                }
                
            }
        }
        var _updateJobPhotos = function(jobId, workweek){
            var job = _currentDay(workweek).Job.find(function(j){ return j.ID == jobId });
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                for(var j=0; j<day.JobDayCrew.length; ++j){
                    if(day.JobDayCrew[j].JobCrewJobID == jobId){
                        _updateJobPhoto(day.JobDayCrew[j], job, workweek);
                    }
                }
            }
        }
        var _updateTaskPhoto = function(task, workweek){
            delete task.photos;
            delete task.documents;
            if(workweek.Photos.Location[task.LocationID]){
                for(var p=0; p<workweek.Photos.Location[task.LocationID].length; ++p){
                    if(workweek.Photos.Location[task.LocationID][p].TypeCode == 'PH') task.photos = true;
                    if(workweek.Photos.Location[task.LocationID][p].TypeCode == 'DO') task.documents = true;
                }
            }
        }
        var _updateTaskPhotos = function(taskOrLocationId, workweek){
            var locationId = typeof taskOrLocationId == 'object' ? taskOrLocationId.LocationID : taskOrLocationId; 
            console.log(locationId)
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                for(var j=0; j<day.JobDayCrew.length; ++j){
                    for(var t=0; t<day.JobDayCrew[j].Task.length; ++t){
                        console.log(day.JobDayCrew[j].Task[t].LocationID)
                        if(day.JobDayCrew[j].Task[t].LocationID == locationId){
                            _updateTaskPhoto(day.JobDayCrew[j].Task[t], workweek);
                        }
                    }
                }
            }
        }
        var _setTaskLocationInfo = function(jdc, workweek){
            if(!_isLocationJob(jdc)) return;
            var locations = [];
            for(var t=0; t<jdc.Task.length; ++t){
                jdc.Task[t].canEditLocation = _isLocationTask(jdc,jdc.Task[t]);
                if(jdc.Task[t].LocationID){
                    jdc.Task[t].canEditLocation = false;
                    _updateTaskPhoto(jdc.Task[t], workweek);
                    var hasCompleteWork = (jdc.cwLocations.indexOf(jdc.Task[t].LocationID) > -1),
                        isFirst = (locations.indexOf(jdc.Task[t].LocationID) == -1)
                    if(!hasCompleteWork || !isFirst){
                        jdc.Task[t].canEditLocation = true;
                        
                    }
                    if(hasCompleteWork){
                        locations.push(jdc.Task[t].LocationID);
                    }
                }
            }
        }
        var _cleanJobDayCrew = function(jdc, workweek, day){
            if(jdc.StartTime == ''){
                jdc.StartTime = _getJobStartTime(day, jdc);
            }
            jdc.originalStart = jdc.StartTime;
            jdc.StartTime = Utils.date(jdc.StartTime);
            if(jdc.EndTime == ''){
                jdc.EndTime = new Date(jdc.StartTime.getTime());
                jdc.EndTime.addHours(jdc.GrandTotalHours?Math.abs(jdc.GrandTotalHours):0);
            }
            jdc.originalEnd = jdc.EndTime;
            jdc.EndTime = Utils.date(jdc.EndTime||jdc.StartTime);
            jdc.JobDayCrewMember = Utils.enforceArray(jdc.JobDayCrewMember);
            var hasInactiveMember = false, hasInactiveEquip = false, hasInactiveRental = false, personMap = {};
            for(var m=0; m<jdc.JobDayCrewMember.length; ++m){
                _cleanMember(workweek, jdc.JobDayCrewMember[m], jdc, day);
                hasInactiveMember = hasInactiveMember || !jdc.JobDayCrewMember[m].ActiveFlag;
                _determineMemberDuplicity(jdc.JobDayCrewMember[m], personMap);
            }
            _determineMemberDuplicity(null, personMap);
            jdc.JobDayCrewEquipment = Utils.enforceArray(jdc.JobDayCrewEquipment);
            for(var e=0; e<jdc.JobDayCrewEquipment.length; ++e){
                _cleanEquip(workweek, jdc.JobDayCrewEquipment[e], jdc, day);
                if(jdc.JobDayCrewEquipment[e].EquipmentTypeCode == 'SM'){
                    hasInactiveEquip = hasInactiveEquip || !jdc.JobDayCrewEquipment[e].ActiveFlag;
                }else{
                    hasInactiveRental = hasInactiveRental || !jdc.JobDayCrewEquipment[e].ActiveFlag;
                }
            }
            _cleanCompletedWork(jdc, workweek);
            jdc.JobTask = Utils.enforceArray(jdc.JobTask);
            for(var jt=0; jt<jdc.JobTask.length; ++jt){
                _cleanJobTask(jdc.JobTask[jt], jdc, workweek);
            }
            jdc.Task = Utils.enforceArray(jdc.Task);
            for(var t=0; t<jdc.Task.length; ++t){
                _cleanTask(jdc.Task[t], jdc, workweek);
            }
            _setTaskLocationInfo(jdc, workweek);
            jdc.Task.sort(function(a,b){ return a.OrderNumber - b.OrderNumber });
            jdc.hasInactiveMember = hasInactiveMember;
            jdc.hasInactiveEquip = hasInactiveEquip;
            jdc.hasInactiveRental = hasInactiveRental;
        }
        var _cleanOtherData = function(workweek){
            var jtCache = {}
            workweek.jobLocations = workweek.jobLocations||{};
            for(var d=0; d<7; d++){
                for(var j=0; j<workweek.WorkDay[d].JobDayCrew.length; j++){
                    var jdc = workweek.WorkDay[d].JobDayCrew[j];
                    if(jdc.JobTask){
                        if(!jtCache[jdc.JobCrewJobID]){
                            jtCache[jdc.JobCrewJobID] = jdc.JobTask
                        }
                    }else if(jtCache[jdc.JobCrewJobID]){
                        jdc.JobTask = Utils.copy(jtCache[jdc.JobCrewJobID])
                    }
                    _cleanJobDayCrew(jdc, workweek, workweek.WorkDay[d])
                }
                workweek.WorkDay[d].JobDayCrew.sort(function(a,b){ return a.OrderNumber - b.OrderNumber });
            }
            _cleanBuckets(workweek);
            if(workweek.contractData && workweek.contractData.contracts){
                for(var c=0; c<workweek.contractData.contracts.length; ++c){
                    workweek.contractData.contracts[c].Start = Utils.date(workweek.contractData.contracts[c].Start);
                    workweek.contractData.contracts[c].End = Utils.date(workweek.contractData.contracts[c].End);
                }
            }
        }
        var _copyJobReferenceData = function(workweek, job, jobDayCrew){
            jobDayCrew.ContractAllowDoubleTimeFlag = job.ContractAllowDoubleTimeFlag;
            jobDayCrew.ContractID = job.ContractID
            jobDayCrew.Description = job.Description
            jobDayCrew.NumberChar = job.NumberChar
            jobDayCrew.TypeCode = job.TypeCode
            jobDayCrew.Type = job.Type
            jobDayCrew.OwnerID = job.OwnerID
            jobDayCrew.UseDesignFlag = job.UseDesignFlag
            jobDayCrew.WorkgroupJobNumber = job.WorkgroupJobNumber
            jobDayCrew.RequireTEStartEndTimeFlag = job.RequireTEStartEndTimeFlag
            jobDayCrew.BypassCompleteWorkFlag = job.BypassCompleteWorkFlag
            jobDayCrew.LocationPercentCompleteFlag = job.DesignLocationPercentCompleteFlag
            jobDayCrew.CaptureTimeAtCode = job.CaptureTimeAtCode
            jobDayCrew.CanMeals = job.Meals;
            jobDayCrew.HrsMeal = job.HrsMeal;
            jobDayCrew.PDSpread = job.PDSpread;
            jobDayCrew.js = job.StatusCode;
            _updateJobPhoto(jobDayCrew, job, workweek);
        }
        var _fixOnHoldJob = function(job, workweek){
            if(job.OnHoldFlag){
                job.StatusCode = 'OH';
                job.StatusMeaning = 'On Hold';
            }
            var status = workweek.jobStatuses.find(function(js){ return js.Code == job.StatusCode });
            if(!status){
                workweek.jobStatuses.push({Code:job.StatusCode, Meaning:job.StatusMeaning});
            }
        }
        var _cleanJob = function(job, workweek){
            if(job.JobPhoto){
                var photos = Utils.enforceArray(job.JobPhoto);
                for(var i=0; i<photos.length; ++i){
                    photos[i].TypeCode = 'PH'
                }
                workweek.Photos.Job[job.ID] = photos;
                delete job.JobPhoto;
            }
            if(job.ActualStartDate){
                job.ActualStartDate = Utils.date(job.ActualStartDate);
            }
            _fixOnHoldJob(job, workweek);
        }
        var _cleanJobs = function(workweek, addJobs){
            workweek.Photos = workweek.Photos||{Job:{},Location:{}}
            workweek.Job = Utils.enforceArray(workweek.Job);
            workweek.jobStatuses = [{Code:'ALL',Meaning:'All'}];
            for(var j=0; j<workweek.Job.length; j++){
                _cleanJob(workweek.Job[j], workweek);
            }
            for(var d=0; d<7; d++){
                var jobs;
                if(!workweek.WorkDay[d].Job){
                    jobs = Utils.copy(workweek.Job);
                    workweek.WorkDay[d].Job = jobs;
                }else if(addJobs){
                    jobs = Utils.copy(addJobs);
                    var args = [workweek.WorkDay[d].Job.length,0].concat(jobs);
                    Array.prototype.splice.apply(workweek.WorkDay[d].Job, args);
                }else{
                    jobs = workweek.WorkDay[d].Job;
                }
                var types = [];
                for(var j=jobs.length-1; j>=0; j--){
                    var selected = false;
                    if(!addJobs){
                        for(var i=0; i<workweek.WorkDay[d].JobDayCrew.length; i++){
                            var jdc = workweek.WorkDay[d].JobDayCrew[i];
                           jdc.active = (typeof jdc.active == 'undefined')?true:jdc.active;
                            if(workweek.WorkDay[d].Job[j].ID == jdc.JobCrewJobID){
                                if(jdc.active){
                                    selected = true;
                                    workweek.WorkDay[d].Job[j].selected = true;
                                    workweek.WorkDay[d].Job[j].selected2 = true;
                                    workweek.WorkDay[d].Job[j].order = jdc.OrderNumber;
                                    workweek.WorkDay[d].hasDoubleTime = workweek.WorkDay[d].hasDoubleTime||workweek.WorkDay[d].DTHours>0||workweek.WorkDay[d].Job[j].ContractAllowDoubleTimeFlag;
                                    workweek.WorkDay[d].showJobTimes = true//workweek.WorkDay[d].showJobTimes||workweek.WorkDay[d].Job[j].RequireTEStartEndTimeFlag;
                                    workweek.WorkDay[d].showJobTimesOnCrew = workweek.WorkDay[d].showJobTimesOnCrew||workweek.WorkDay[d].Job[j].RequireTEStartEndTimeFlag;
                                    workweek.WorkDay[d].hasMeals = workweek.WorkDay[d].hasMeals||workweek.WorkDay[d].Job[j].Meals;
                                }
                                _copyJobReferenceData(workweek, workweek.WorkDay[d].Job[j], jdc);
                            }
                        }
                    }
                    if(!selected){
                        if(jobs[j].ActualStartDate && jobs[j].ActualStartDate > workweek.WorkDay[d].WeekdayDate){
                            jobs.splice(j, 1)
                            continue;
                        }
                    }
                    if(!types.includes(jobs[j].Type)){
                        types.push(jobs[j].Type);
                    }
                }
                if(!workweek.WorkDay[d].currentJobType){
                    if(types.includes('AJ')){
                        workweek.WorkDay[d].currentJobType = 'AJ';
                    }else if(types.includes('DJ')){
                        workweek.WorkDay[d].currentJobType = 'DJ';
                    }else if(types.includes('OT')){
                        workweek.WorkDay[d].currentJobType = 'OT';
                    }else if(types.includes('OJ')){
                        workweek.WorkDay[d].currentJobType = 'OJ';
                    }
                }
            }
        }
        var _cleanProgress = function(day, workweek, dayIndex){
            var currentIndex = day.DayProcessCompletion.findIndex(function(step){ return !step.CompleteFlag });
            if(currentIndex == -1) currentIndex = day.DayProcessCompletion.length-1;
            day.DayProcessCompletion[2].subSteps = { current: currentIndex>2?1:((day.DayProcessCompletion[2].subSteps?day.DayProcessCompletion[2].subSteps.current:0)||0), last: 1};
            day.currentStep = currentIndex;
            day.stepLimit = workweek.AllowEdit?10:day.currentStep;
            day.equipParsed = day.equipParsed||day.currentStep>=4||false;
            if(currentIndex > 2){
                _markJobsWorkComplete(day)
            }
            if(!dayIndex){
                workweek.displaySteps = []
                for(var s=0; s<day.DayProcessCompletion.length; ++s){
                    if(s == 2){
                        workweek.displaySteps.push({step: s, substep:0, label: 'Job Time'})
                        workweek.displaySteps.push({step: s, substep:1, label: day.DayProcessCompletion[s].ProcessStepName})
                    }else{
                        workweek.displaySteps.push({step: s, substep:0, label: day.DayProcessCompletion[s].ProcessStepName})
                    }
                }
            }
        }
        var _cleanReferenceData = function(workweek){
            workweek.sp = workweek.sp || {};
            if(workweek.referenceData){
                workweek.sp.timeWorkWeek = Utils.enforceArray(workweek.referenceData.TimeWorkWeek);
                delete workweek.referenceData;
            }
        }
        var _cleanWorkweek = function(workweek, scope){
            _cleanReferenceData(workweek);
            workweek.PayrollPeriodEndDate = new Date(workweek.PayrollPeriodEndDate);
            workweek.PayrollPeriodStartDate = new Date(workweek.PayrollPeriodEndDate);
            workweek.PayrollPeriodStartDate.addDays(-6);
            workweek.filters = workweek.filters || {};
            workweek.Code = (workweek.Code||workweek.CrewWorkWeekCode||'F8');
            workweek.timeWorkWeek = workweek.sp.timeWorkWeek.find(function(tww){ return tww.SmartCode == (workweek.Code||'') });
            scope.workweek = workweek;
            if(typeof workweek.currentDay == 'undefined' && workweek.InitDate){
                workweek.currentDay = workweek.WorkDay.findIndex(function(day){ return day.WeekdayDate == workweek.InitDate; })||0;
            }else{
                workweek.currentDay = workweek.currentDay||0;
            }
            for(var d=0; d<workweek.WorkDay.length; d++){
                var day = workweek.WorkDay[d];
                day.WeekdayDate = Utils.date(day.WeekdayDate);
                day.index = d;
                Utils.clearTimeFromDate(day.WeekdayDate);
                day.JobDayCrew = Utils.enforceArray(day.JobDayCrew);
                if(['PE','IP','RJ'].includes(workweek.StatusCode)&&workweek.MyPersonID==workweek.PersonID&&['PE','IP','RJ'].includes(day.StatusCode)){
                    day.addingJob = (typeof day.addingJob == 'undefined')?(day.JobDayCrew.length==0):day.addingJob;
                }
                _cleanProgress(day, workweek, d);
                if(d == workweek.currentDay){
                    scope.editable = ['PE','IP','RJ'].includes(day.StatusCode) && workweek.AllowEdit;
                    scope.disableRight = day.currentStep < day.stepLimit ? false : true;
                }
            }
            _cleanJobs(workweek);
            _cleanOtherData(workweek);
            scope.original = Utils.copy(workweek);
            var day = _currentDay(workweek);
            var step = _currentStep(workweek);
            var callback = scope.checkCallback();
            if(!callback || ((step.ProcessStepCode == 'MT' && callback != 'initMemberTimeResult')|| step.ProcessStepCode == 'ET' || step.ProcessStepCode == 'RS')){
                _steps.init(step, workweek, scope);
            }
            scope.checkCallback('afterInit');
            _cleanDialog(workweek, true);
            if(workweek.FromCompleteWork){
                _markJobsWorkComplete(day)
            }
            workweek.FromCompleteWork = false;
            delete workweek.jobScreen;
            day.initDone = true;
            _checkApproveAll(workweek);
            return workweek;
        }

        var _checkChangeTimeCode = function(workweek){
            workweek.canChangeTimeCode = workweek.AllowEdit;
            for(var d=0; d<workweek.WorkDay.length; ++ d){
                if(!['PE','IP','ZH'].includes(workweek.WorkDay[d].StatusCode)){
                    workweek.canChangeTimeCode = false;
                    return;
                }
            }
        }
        var _checkZeroAll = function(workweek){
            var pendingCount = 0;
            if(!workweek.AllowEdit) return;
            for(var d=0; d<workweek.WorkDay.length; ++ d){
                if(workweek.WorkDay[d].StatusCode == 'PE'){
                    pendingCount++;
                }
            }
            if(pendingCount){
                workweek.zeroAll = 'Zero All Pending ('+pendingCount+')';
            }else{
                workweek.zeroAll = '';
            }
        }
        var _checkApproveAll = function(workweek){
            _checkChangeTimeCode(workweek);
            _checkZeroAll(workweek);
            if(!workweek.AllowApproval && (!workweek.UseReviewer || !workweek.AllowReview)) return;
            var reviewCount = 0, approveCount = 0;
            for(var d=0; d<workweek.WorkDay.length; ++ d){
                if(workweek.WorkDay[d].StatusCode == 'SB'){
                    reviewCount++;
                    approveCount++;
                }else if(workweek.WorkDay[d].StatusCode == 'RV'){
                    approveCount++;
                }
            }
            if(workweek.AllowApproval){
                if(approveCount > 0){
                    workweek.approveAll = 'Approve All ('+approveCount+')';
                    workweek.approveAllCode = 'AP';
                    return;
                }
            }else{
                if(reviewCount > 0){
                    workweek.approveAll = 'Review All ('+reviewCount+')';
                    workweek.approveAllCode = 'RV';
                    return;
                }
            }
            workweek.approveAll = '';
            workweek.approveAllCode = '';
        }

        var _resetJobTabLimits = function(scope){
            if(!scope.addJobTypes) return;
            for(var i=0; i<scope.addJobTypes.length; ++i){
                if(scope.addJobTypes[i].limitTo){
                    scope.addJobTypes[i].limitTo = 10;
                }
            }
        }
        var _initDay = function(scope){
            _resetJobTabLimits(scope)
            var day = _currentDay(scope.workweek);
            day.initDone = true;
            scope.disableRight = day.currentStep < day.stepLimit ? false : true;
            var step = _currentStep(scope.workweek);
             _steps.init(step, scope.workweek, scope);
             if(day.contractErrorJobs && day.contractErrorJobs.length && !scope.workweek.shownErrorContracts){
                _showContractError(day, scope.workweek);
            }
        }

        var _resetJob = function(job){
            job.init = true;
            job.GrandTotalHours = 0;
            job.NonCompHours = 0;
            job.TotalHours = 0;
            job.STHours = 0;
            job.OTHours = 0;
            job.DTHours = 0;
            job.JobComment = '';
            delete job.StartTime;
            delete job.EndTime;
        }
        var _addJobToDay = function(day, job, workweek){
            var jobDayCrew = {
                ID: _getTempID(workweek),
                JobCrewID: _getTempID(workweek),
                JobCrewJobID: job.ID,
                StatusCode: 'PE',
                QuickAddJob: job.QuickAddJob,
                Task: []
            }
            _copyJobReferenceData(workweek, job, jobDayCrew);
            day.JobDayCrew.push(jobDayCrew);
            return jobDayCrew;
        }
        var _updateJobStatuses = function(changedJobs, workweek, dayIndex){
            if(!changedJobs.length) return;
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var jobs = workweek.WorkDay[d].Job;
                if(d==dayIndex) jobs = workweek.Job;
                for(var j=0; j<jobs.length; ++j){
                    if(changedJobs.indexOf(jobs[j].ID) > -1){
                        var job = workweek.WorkDay[dayIndex].Job.find(function(jb){ return jb.ID==jobs[j].ID });
                        jobs[j].StatusCode = job.StatusCode;
                        jobs[j].StatusMeaning = job.StatusMeaning;
                        jobs[j].Type = job.Type;
                    }
                }
            }
        }
        var _getJobStartTime = function(day, job){
            var refJob = day.Job.find(function(jb){return job.JobCrewJobID == jb.ID});
            var time = _parseTime(refJob.DefaultStart, day.WeekdayDate);
            return _dateFromHourAndMinute(day.WeekdayDate, time.getHours(), time.getMinutes());
        }
        var _addRemoveJobs = function(workweek, dayIndex){
            if(typeof dayIndex == 'undefined') dayIndex = workweek.currentDay;
            var day = workweek.WorkDay[dayIndex], hasDoubleTime = false, showJobTimesOnCrew = false;
            var firstActiveJdcBefore = day.JobDayCrew.find(function(jdc){return jdc.active });
            var startTime = firstActiveJdcBefore ? firstActiveJdcBefore.StartTime : null;
            var changedJobs = [];
            for(var j=0; j<day.Job.length; j++){
                var index = day.JobDayCrew.findIndex(function(jdc){return jdc.JobCrewJobID == day.Job[j].ID});
                var job = index > -1 ? day.JobDayCrew[index] : null;
                if(day.Job[j].selected){
                    if(day.Job[j].Type == 'OT' || ['OH','PS'].includes(day.Job[j].StatusCode)){
                        if(day.Job[j].Type == 'OT'){
                            day.Job[j].Type = 'AJ';
                        }
                        if(['OH','PS'].includes(day.Job[j].StatusCode)){
                            day.Job[j].StatusCode = 'PR';
                            day.Job[j].StatusMeaning = 'In Process';
                        }
                        changedJobs.push(day.Job[j].ID);
                    }
                    if(!job || !job.active){
                        if(!job){
                            job = _addJobToDay(day, day.Job[j], workweek);
                        }else{
                            day.JobDayCrew.push(day.JobDayCrew.splice(index, 1)[0]);
                        }
                        _changes(workweek, true);
                        _resetJob(job);
                    }
                    hasDoubleTime = hasDoubleTime||job.ContractAllowDoubleTimeFlag;
                    showJobTimesOnCrew = showJobTimesOnCrew||job.RequireTEStartEndTimeFlag;
                    job.active = true;
                    job.OrderNumber = day.Job[j].order;
                    if(firstActiveJdcBefore){
                        job.StatusCode = firstActiveJdcBefore.StatusCode;
                        job.StatusMeaning = firstActiveJdcBefore.StatusMeaning;
                    }
                }else{
                    if(job && job.active){
                        job.active = false;
                        _changes(workweek, true);
                        _resetJob(job);
                    }
                }
            }
            day.JobDayCrew.sort(function(a,b){ 
                if(a.active != b.active){
                    return a.active?-1:1;
                }
                return (a.OrderNumber||0)-(b.OrderNumber||0);
            });
            var firstActiveJdcAfter = day.JobDayCrew.find(function(jdc){return jdc.active });
            if(firstActiveJdcAfter){
                startTime = _getJobStartTime(day, firstActiveJdcAfter);
                if(startTime){
                    firstActiveJdcAfter.StartTime = startTime;
                }
                firstActiveJdcAfter.GrandTotalHours = day.weekdayHoursBeforeJobChange||0;
                //Copy crew over
                if(firstActiveJdcBefore){
                    //Sync other crews
                    for(var i=0; i<day.JobDayCrew.length; i++){
                        if(!day.JobDayCrew[i].active || day.JobDayCrew[i] === firstActiveJdcBefore) continue;
                        _moveCrew(firstActiveJdcBefore, day.JobDayCrew[i], workweek);
                    }
                }
            }
            day.hasDoubleTime = hasDoubleTime;
            day.showJobTimes = true;
            day.showJobTimesOnCrew = showJobTimesOnCrew;
            _updateJobStatuses(changedJobs, workweek, dayIndex);
            _totalDay(workweek, dayIndex);
            _calcIfNextIsCompleteWork(workweek);
            _calcShowUpSites(workweek);
            _checkContractErrors(workweek);
        }

        var _inactivateCrew = function(workweek, job){
            if(job.selected){
                _selectStartJob(workweek, job);
            }
            for(var d=0; d<workweek.WorkDay.length; ++d){
                for(var j=0; j<workweek.WorkDay[d].Job.length; ++j){
                    var jb = workweek.WorkDay[d].Job[j];
                    if(jb.ID == job.ID){
                        jb.Type = 'OT';
                    }
                }
            }
            _changes(workweek, true);
        }

        var _selectStartJob = function(workweek, job){
            var day = _currentDay(workweek);
            var prior = job.selected, order = job.order||0;
            job.selected = !job.selected;
            job.selected2 = job.selected;
            if(prior){
                delete job.order;
            }
            for(var j=0; j<day.Job.length; ++j){
                var jb = day.Job[j];
                if(jb.selected){
                    if(prior){
                        if(jb.order > order){
                            --jb.order;
                        }
                    }else{
                        order = Math.max(order, jb.order||0);
                    }
                }
            }
            if(!prior){
                job.order = order+1;
                if(day.StatusCode != 'PE' && day.currentStep == 0){
                    _setDayStatus(day, workweek, 'PE');
                }
            }
            if(day.currentStep == 0){
                _changes(workweek, true);
                _totalDay(workweek);
            }
        }
        var _activeJobs = function(workweek, date){
            var jobs = [], index = workweek.currentDay;
            if(date){
                if(date == 'all'){
                    for(var d=0; d< workweek.WorkDay.length; ++d){
                        for(var j=0; j<workweek.WorkDay[d].JobDayCrew.length; j++){
                            if(workweek.WorkDay[d].JobDayCrew[j].active){
                                jobs.push(workweek.WorkDay[d].JobDayCrew[j]);
                                jobs[jobs.length-1].index = j;
                            }
                        }
                    }
                    return jobs;
                }
                index = workweek.WorkDay.findIndex(function(wd){ return wd.WeekdayDate.getTime() == date.getTime() });
            }
            for(var j=0; j<workweek.WorkDay[index].JobDayCrew.length; j++){
                if(workweek.WorkDay[index].JobDayCrew[j].active){
                    jobs.push(workweek.WorkDay[index].JobDayCrew[j]);
                    jobs[jobs.length-1].index = j;
                }
            }
            return jobs;
        }
        var _activeTasks = function(job, stats){
            var tasks = [], stats = stats||{compensatedCount:0};
            for(var t=0; t<job.Task.length; t++){
                if(job.Task[t].active){
                    tasks.push(job.Task[t]);
                    tasks[tasks.length-1].index = t;
                    tasks[tasks.length-1].OrderNumber = t+1;
                    stats.compensatedCount += (job.Task[t].JobTaskNonCompensatedFlag?0:1);
                }
            }
            return tasks;
        }
        var _activePhases = function(task, view){
            view = view||'PhaseCode';
            var pc = [];
            for(var p=0; p<task[view].length; p++){
                if(task[view][p].active){
                    pc.push(task[view][p]);
                    pc[pc.length-1].index = p;
                }
            }
            return pc;
        }
        var _fixTaskHours = function(job, workweek){
            var tasks = _activeTasks(job);
            var jobHours = job.GrandTotalHours, taskHours = tasks.reduce(function(hours, task){ hours += task.Duration; return hours; },0);
            if(taskHours != jobHours){
                var remaining = (jobHours-taskHours), i;
                for(i=tasks.length-1; i>=0; --i){
                    var newValue = tasks[i].Duration+remaining;
                    tasks[i].Duration = Math.max(newValue, 0);
                    if(newValue < 0){
                        remaining = newValue-tasks[i].Duration;
                    }else{
                        break;
                    }
                }
                _updateTaskHours(job, i, workweek);
            }
        }
        var _fixMemberTaskHours = function(job, workweek){
            var tasks = job.Tasks;
            var jobHours = job.Member.TotalHours, taskHours = tasks.reduce(function(hours, task){ hours += task.TotalHours; return hours; },0);
            if(taskHours != jobHours){
                var remaining = (jobHours-taskHours), i;
                for(i=tasks.length-1; i>=0; --i){
                    var newValue = tasks[i].TotalHours+remaining;
                    tasks[i].TotalHours = Math.max(newValue, 0);
                    if(newValue < 0){
                        remaining = newValue-tasks[i].TotalHours;
                    }else{
                        break;
                    }
                }
                _updateMemberTaskHours(job, i, workweek);
            }
        }
        var _changeJobHourRatio = function(job, which, workweek){
            var types = ['ST','OT'];
            if(job.ContractAllowDoubleTimeFlag){
                types.push('DT');
            }
            if(job[which+"Hours"] === ''){
                job.RatioOverridenFlag = false;
            }else{
                job[which+"Hours"] = Math.min(job.GrandTotalHours,Math.max(0,job[which+"Hours"]));
                var allocate = job.GrandTotalHours-_total(job);
                var index = types.findIndex(function(type){ return which == type });
                while(allocate != 0){
                    ++index; if(index == types.length) index = 0;
                    var newValue = job[types[index]+"Hours"]+allocate;
                    job[types[index]+"Hours"] = Math.max(newValue, 0);
                    if(newValue < 0){
                        allocate = newValue-job[types[index]+"Hours"];
                    }else{
                        break;
                    }
                }
                job.RatioOverridenFlag = true;
            }
            _updateJobHoursDistribution(job, workweek);
            _totalDay(workweek);
        }
        var _changeJobMeals = function(job, workweek){
            if(!job.Meals && job.Meals.toString() !== "0"){
                job.Meals = job.calcMeals;
            }
            _totalDay(workweek);
        }
        var _changeMemberJobMeals = function(job, dayMember, workweek){
            if(!job.Member.Meals && job.Member.Meals.toString() !== "0"){
                job.Member.Meals = job.Member.calcMeals;
            }
            _changeExpenseEntryQty(job.Member, job.Member.Meals, workweek);
            _totalMemberTime(workweek, dayMember);
        }
        var _changeMemberMeals = function(dayMember, workweek){
            if(!dayMember.Meals && dayMember.Meals.toString() !== "0"){
                dayMember.Meals = dayMember.calcMeals;
            }
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var memberJob = dayMember.Jobs[j];
                if(memberJob.Job.CanMeals){
                    memberJob.Member.Meals = dayMember.Meals;
                }
            }
            _divideMeals(dayMember, workweek);
            _totalMemberTime(workweek, dayMember);
        }
        var _updateJobHoursDistribution = function(job, workweek, tasks){
            var totals = {job:0,day:{ST:0}}, times = [0,0,0];
            if(!tasks) tasks = _activeTasks(job);
            for(var t=0; t<tasks.length; ++t){
                _parseOutTaskTime(job, tasks[t], totals, workweek);
                times[0] += parseFloat(tasks[t].STHours);
                times[1] += parseFloat(tasks[t].OTHours);
                times[2] += parseFloat(tasks[t].DTHours);
                totals.job = (times[0]+times[1]+times[2]);
                totals.day.ST += parseFloat(tasks[t].STHours);
            }
            job.STHours = times[0];
            job.OTHours = times[1];
            job.DTHours = times[2];
        }
        var _updateMemberJobHoursDistribution = function(job, workweek, tasks){
            var totals = {job:0,day:{ST:0}}, times = [0,0,0];
            if(!tasks) tasks = job.Tasks;
            for(var t=0; t<tasks.length; ++t){
                _parseOutMemberTaskTime(job, tasks[t], totals, workweek);
                times[0] += parseFloat(tasks[t].STHours);
                times[1] += parseFloat(tasks[t].OTHours);
                times[2] += parseFloat(tasks[t].DTHours);
                totals.job = (times[0]+times[1]+times[2]);
                totals.day.ST += parseFloat(tasks[t].STHours);
            }
            job.Member.STHours = times[0];
            job.Member.OTHours = times[1];
            job.Member.DTHours = times[2];
        }
        var _totalNonComp = function(tasks){
            var total = 0;
            for(var t=0; t<tasks.length; ++t){
                if(tasks[t].JobTaskNonCompensatedFlag){
                    total += tasks[t].Duration;
                }
            }
            return total;
        }
        var _updateJobHours = function(workweek, index, skipFirst){
            var jobs = _activeJobs(workweek);
            if(index > 0 && jobs[index].StartTime.getTime() != jobs[index-1].EndTime.getTime()){
                jobs[index].StartTime = new Date(jobs[index-1].EndTime.getTime());
            }
            var start = jobs[index].StartTime;
            for(var i=index; i<jobs.length; i++){
                if(i==index && skipFirst){
                    start = new Date(jobs[i].EndTime.getTime());
                    continue;
                }
                var job = jobs[i], times = [0,0,0], tasks = _activeTasks(job);
                if(job.OrderNumber != (i+1)){
                    job.OrderNumber = (i+1);
                    var jb = workweek.WorkDay[workweek.currentDay].Job.find(function(jb){return jb.ID == job.JobCrewJobID });
                    if(jb) jb.order = job.OrderNumber;
                }
                job.StartTime = start;
                job.GrandTotalHours = job.GrandTotalHours;
                job.EndTime = new Date(job.StartTime.getTime());
                job.EndTime.addHours(job.GrandTotalHours||0);
                if(tasks.length){
                    tasks[0].StartTime = new Date(start.getTime());
                }
                if(i==index){
                    job.RatioOverridenFlag = false;
                    _fixTaskHours(job, workweek);
                    _updateTaskHours(job, 0, workweek);
                }else{
                    if(tasks.length){
                        _updateTaskHours(job, 0, workweek);
                    }
                }
                _updateJobHoursDistribution(job, workweek, tasks);
                job.NonCompHours = _totalNonComp(tasks);
                job.TotalHours = job.GrandTotalHours - job.NonCompHours;
                if(job.CanMeals){
                    job.Meals = job.calcMeals = Math.floor(job.TotalHours/(job.HrsMeal||5));
                }
                start = new Date(job.EndTime.getTime());
            }
            _totalDay(workweek);
        }
        var _memberTasks = function(memberJob){
            return memberJob.Tasks;
        }
        var _updateMemberJobHours = function(workweek, index, skipFirst){
            var member = _currentMember(workweek);
            var jobs = member.Jobs;
            if(index > 0 && jobs[index].StartTime.getTime() != jobs[index-1].EndTime.getTime()){
                jobs[index].StartTime = new Date(jobs[index-1].EndTime.getTime());
            }
            var start = jobs[index].StartTime;
            for(var i=index; i<jobs.length; i++){
                if(i==index && skipFirst){
                    start = new Date(jobs[i].EndTime.getTime());
                    continue;
                }
                var job = jobs[i], tasks = _memberTasks(job);
                job.StartTime = start;
                job.EndTime = new Date(job.StartTime.getTime());
                job.EndTime.addHours(job.Member.TotalHours||0);
                if(tasks.length){
                    tasks[0].StartTime = new Date(start.getTime());
                }
                if(i==index){
                    job.RatioOverridenFlag = false;
                    _fixMemberTaskHours(job, workweek);
                    _updateMemberTaskHours(job, 0, workweek);
                    if(job.Job.CanMeals){
                        var total = job.Tasks.reduce(function(total,t){ if(!t.Task.JobTaskNonCompensatedFlag) total+=t.TotalHours; return total; }, 0);
                        job.Member.Meals = job.Member.calcMeals = Math.floor(total/(job.Job.HrsMeal||5));
                        _changeExpenseEntryQty(job.Member, job.Member.Meals, workweek);

                    }
                }else{
                    if(tasks.length){
                        _updateMemberTaskHours(job, 0, workweek);
                    }
                }
                _updateMemberJobHoursDistribution(job, workweek, tasks);
                start = new Date(job.EndTime.getTime());
            }
            _totalMemberTime(workweek, member);
        }
        var _updateTaskHours = function(job, index, workweek){
            var tasks = _activeTasks(job), jobs = _activeJobs(workweek);
            var totals = {job: 0, day:{ST:0}}
            if(!tasks||!tasks.length) return;
            var start = tasks[index].StartTime;
            for(var j=0; j<jobs.length; ++j){
                if(jobs[j].JobCrewJobID == job.JobCrewJobID) break;
                totals.day.ST += jobs[j].STHours;
            }
            for(var i=0; i<tasks.length; i++){
                if(i < index){
                    totals.job += tasks[i].Duration;
                    totals.day.ST += parseFloat(tasks[i].STHours);
                    continue;
                }
                if(tasks[i].OrderNumber != (i+1)){
                    tasks[i].OrderNumber = (i+1);
                }
                tasks[i].StartTime = start;
                tasks[i].Duration = tasks[i].Duration;
                tasks[i].EndTime = new Date(tasks[i].StartTime.getTime());
                tasks[i].EndTime.addHours(tasks[i].Duration||0);
                _parseOutTaskTime(job, tasks[i], totals, workweek);
                start = new Date(tasks[i].EndTime.getTime());
                totals.job += tasks[i].Duration;
                totals.day.ST += parseFloat(tasks[i].STHours);
            }
        }
        var _updateMemberTaskHours = function(job, index, workweek){
            var tasks = job.Tasks, jobs = _currentMember(workweek).Jobs;
            var totals = {job: 0, day:{ST:0}}
            if(!tasks||!tasks.length) return;
            var start = tasks[index].StartTime;
            for(var j=0; j<jobs.length; ++j){
                if(jobs[j].Job.JobCrewJobID == job.Job.JobCrewJobID) break;
                totals.day.ST += jobs[j].STHours;
            }
            for(var i=0; i<tasks.length; i++){
                if(i < index){
                    totals.job += tasks[i].TotalHours;
                    continue;
                }
                tasks[i].StartTime = start;
                tasks[i].EndTime = new Date(tasks[i].StartTime.getTime());
                tasks[i].EndTime.addHours(tasks[i].TotalHours||0);
                _parseOutMemberTaskTime(job, tasks[i], totals, workweek);
                start = new Date(tasks[i].EndTime.getTime());
                totals.job += tasks[i].TotalHours;
                totals.day.ST += parseFloat(tasks[i].STHours);
            }
        }
        var _updateTaskHoursEntry = function(workweek, job, index, jobIndex, previousValue){
            var originalTasks = _activeTasks(job);
            var tasks = originalTasks.map(function(item){ return item; }), top = 0;
            for(var i=0; i<originalTasks.length; ++i){ 
                var task = originalTasks[i];
                task.index = i;
                if(!task.EntryOrder) task.EntryOrder = 0;
                if(task.EntryOrder > top) top = task.EntryOrder;
            }
            var task = originalTasks[index];
            originalTasks[index].EntryOrder = top + 1;
            tasks.sort(function(a,b){  return (a.EntryOrder==b.EntryOrder)?(a.index-b.index):(a.EntryOrder - b.EntryOrder); });
            var total = parseFloat(job.GrandTotalHours);
            var myHours = Math.max(0,Math.min(total, task.Duration));
            task.Duration = myHours;
            task.changed = true;
            var remaining = previousValue-myHours;
            for(var i=0; i<tasks.length, remaining != 0; ++i){
                var hours = parseFloat(tasks[i].Duration);
                var amount = Math.max(hours+remaining, 0);
                tasks[i].Duration = amount;
                tasks[i].changed = true;
                remaining -= (amount-hours);
            }
            total = 0;
            var stage = 0;
            for(var i=0; i<originalTasks.length; ++i){
                if(originalTasks[i].changed && !stage){
                    stage = 1;
                }
                if(stage == 1){
                    _updateTaskHours(job, i, workweek);
                    stage = 2;
                }
                delete originalTasks[i].changed;
            }
            if(job.CanMeals){
                job.calcMeals = job.Meals = Math.floor(total / (job.HrsMeal||5));
            }
        }
        var _updateMemberTaskHoursEntry = function(workweek, job, index, jobIndex, previousValue){
            var originalTasks = job.Tasks;
            var tasks = originalTasks.map(function(item){ return item; }), top = 0;
            for(var i=0; i<originalTasks.length; ++i){ 
                var task = originalTasks[i];
                task.index = i;
                if(!task.EntryOrder) task.EntryOrder = 0;
                if(task.EntryOrder > top) top = task.EntryOrder;
            }
            var task = originalTasks[index];
            originalTasks[index].EntryOrder = top + 1;
            tasks.sort(function(a,b){  return (a.EntryOrder==b.EntryOrder)?(a.index-b.index):(a.EntryOrder - b.EntryOrder); });
            var total = parseFloat(job.Member.TotalHours);
            var myHours = Math.max(0,Math.min(total, task.TotalHours));
            task.TotalHours = myHours;
            task.changed = true;
            var remaining = previousValue-myHours;
            for(var i=0; i<tasks.length, remaining != 0; ++i){
                var hours = parseFloat(tasks[i].TotalHours);
                var amount = Math.max(hours+remaining, 0);
                tasks[i].TotalHours = amount;
                tasks[i].changed = true;
                remaining -= (amount-hours);
            }
            total = 0;
            var done = false;
            for(var i=0; i<originalTasks.length; ++i){
                if(originalTasks[i].changed && !done){
                    _updateMemberTaskHours(job, i, workweek);
                    done = true;
                }
                delete originalTasks[i].changed;
            }
            if(job.Job.CanMeals){
                job.Member.calcMeals = job.Member.Meals = Math.floor(total / (job.Job.HrsMeal||5));
                _changeExpenseEntryQty(job.Member, job.Member.Meals, workweek);
            }
            _totalMemberTime(workweek, 'current');
        }

        var _changeJobDescription = function(workweek, jdc, description){
            if(description == jdc.Description) return;
            for(var d=0; d<workweek.WorkDay.length; ++d){
                for(var j=0; j<workweek.WorkDay[d].Job.length; ++j){
                    var jb = workweek.WorkDay[d].Job[j];
                    if(jb.ID == jdc.JobCrewJobID){
                        jb.Description = description;
                    }
                }
                for(var j=0; j<workweek.WorkDay[d].JobDayCrew.length; ++j){
                    var jdc2 = workweek.WorkDay[d].JobDayCrew[j];
                    if(jdc.JobCrewJobID == jdc2.JobCrewJobID){
                        jdc2.Description = description;
                    }
                }
            }
            _changes(workweek, true);
        }

        var _moveJob = function(workweek, index, dir){
            var jobs = _activeJobs(workweek);
            if((index == 0 && dir < 0) || (index == jobs.length-1 && dir > 0))
                return;
            var newIndex = index + dir;
            var smaller = Math.min(newIndex, index), larger = Math.max(newIndex, index);
            jobs[larger].StartTime = jobs[smaller].StartTime?new Date(jobs[smaller].StartTime.getTime()):'';
            workweek.WorkDay[workweek.currentDay].JobDayCrew.splice(jobs[index].index, 0, workweek.WorkDay[workweek.currentDay].JobDayCrew.splice(jobs[newIndex].index, 1)[0]);
            if(smaller==0)_copyCrewState(jobs[smaller], jobs[larger]);
            _updateJobHours(workweek, smaller);
        }
        var _calcJobOrTaskTotalHours = function(item, totalField){
            var start = (item.StartTime.getHours()*60)+item.StartTime.getMinutes(),
                end = (item.EndTime.getHours()*60)+item.EndTime.getMinutes();
            if(end < start) end += 24*60;
            var diff = end - start;
            var total = Math.floor(diff/60)+(diff%60)/60;
            if(totalField) item[totalField] = total;
            return total;
        }
        var _updateJobTime = function(workweek, index, which){
            var jobs = _activeJobs(workweek);
            if(which == 'EndTime'){
                _calcJobOrTaskTotalHours(jobs[index], 'GrandTotalHours');
            }
            _updateJobHours(workweek, index);
        }
        var _updateMemberJobTime = function(workweek, index, which){
            var dayMember = _currentMember(workweek);
            var jobs = dayMember.Jobs;
            if(which == 'EndTime'){
                _calcJobOrTaskTotalHours(jobs[index].Member, 'TotalHours');
            }
            _updateMemberJobHours(workweek, index);
        }
        var _updateTaskTime = function(workweek, job, index, which, jobIndex){
            var tasks = _activeTasks(job).map(function(task, index){ return task; });
            tasks.forEach(function(task){ task.EntryOrder = tasks.length + 1 });
            var myTime = tasks[index][which].getTime();
            var priorValue = tasks[index].Duration;
            tasks[index][which] = new Date(Math.max(Math.min(myTime, job.EndTime.getTime()), job.StartTime.getTime()));
            if(which == 'EndTime'){
                if(tasks[index].EndTime.getTime() < tasks[index].StartTime.getTime()){
                    tasks[index].StartTime = new Date(tasks[index].EndTime.getTime());
                    for(var i=index-1; i>=0; --i){
                        var diff = (tasks[i+1].StartTime.getTime()-tasks[i].StartTime.getTime())/1000/60;
                        var total = Math.floor(diff/60)+(diff%60)/60;
                        if(total < tasks[i].Duration){
                            tasks[i].EndTime = new Date(tasks[i+1].StartTime.getTime());
                            if(tasks[i].EndTime.getTime() < tasks[i].StartTime.getTime()){
                                priorValue += tasks[i].Duration;
                                tasks[i].Duration = 0;
                                tasks[i].StartTime = new Date(tasks[i].EndTime.getTime());
                            }else{
                                priorValue -= (total-tasks[i].Duration);
                                tasks[i].Duration = Math.max(0, _calcJobOrTaskTotalHours(tasks[i]));
                                tasks[i].StartTime = (new Date(tasks[i].EndTime.getTime())).addHours(-tasks[i].Duration);
                            }
                        }else{
                            break;
                        }
                    }
                }
                var i = index + 1, order = 0;
                while(i < tasks.length-1){
                    tasks[i].EntryOrder = order++;
                    ++i;
                }
                _calcJobOrTaskTotalHours(tasks[index], 'Duration');
            }
            _updateTaskHoursEntry(workweek, job, index, jobIndex, priorValue);
        }
        var _updateMemberTaskTime = function(workweek, job, index, which, jobIndex){
            var tasks = job.Tasks.map(function(task, index){ return task; });
            tasks.forEach(function(task){ task.EntryOrder = tasks.length + 1 });
            var myTime = tasks[index][which].getTime();
            var priorValue = tasks[index].TotalHours;
            tasks[index][which] = new Date(Math.max(Math.min(myTime, job.EndTime.getTime()), job.StartTime.getTime()));
            if(which == 'EndTime'){
                if(tasks[index].EndTime.getTime() < tasks[index].StartTime.getTime()){
                    tasks[index].StartTime = new Date(tasks[index].EndTime.getTime());
                    for(var i=index-1; i>=0; --i){
                        var diff = (tasks[i+1].StartTime.getTime()-tasks[i].StartTime.getTime())/1000/60;
                        var total = Math.floor(diff/60)+(diff%60)/60;
                        if(total < tasks[i].TotalHours){
                            tasks[i].EndTime = new Date(tasks[i+1].StartTime.getTime());
                            if(tasks[i].EndTime.getTime() < tasks[i].StartTime.getTime()){
                                priorValue += tasks[i].TotalHours;
                                tasks[i].TotalHours = 0;
                                tasks[i].StartTime = new Date(tasks[i].EndTime.getTime());
                            }else{
                                priorValue -= (total-tasks[i].TotalHours);
                                tasks[i].TotalHours = Math.max(0, _calcJobOrTaskTotalHours(tasks[i]));
                                tasks[i].StartTime = (new Date(tasks[i].EndTime.getTime())).addHours(-tasks[i].TotalHours);
                            }
                        }else{
                            break;
                        }
                    }
                }
                var i = index + 1, order = 0;
                while(i < tasks.length-1){
                    tasks[i].EntryOrder = order++;
                    ++i;
                }
                _calcJobOrTaskTotalHours(tasks[index], 'TotalHours');
            }
            _updateMemberTaskHoursEntry(workweek, job, index, jobIndex, priorValue);
        }
        var _inactivateAllMembers = function(jdc){
            for(var m=0; m<jdc.JobDayCrewMember.length; ++m){
                jdc.JobDayCrewMember[m].active = false;
            }
        }
        var _inactivateAllEquipment = function(jdc){
            for(var e=0; e<jdc.JobDayCrewEquipment.length; ++e){
                jdc.JobDayCrewEquipment[e].active = false;
            }
        }
        var _addCrewMember = function(person, workweek, scope){
            var jobs = _activeJobs(workweek), day = _currentDay(workweek), currentStep = _currentStep(workweek), duplicateMember = false;
            var member = jobs[0].JobDayCrewMember.find(function(mem){ return mem.PersonID == person.ID && !mem.duplicate }), existing = false;
            if(!member){
                member = _newCrewMember(person, jobs[0], workweek);
                jobs[0].JobDayCrewMember.push(member);
            }else if(currentStep.ProcessStepCode == 'MT'){
                existing = _shouldHideMember(member, workweek);
                if(!existing){
                    member.duplicate = false;
                    member = _newCrewMember(person, jobs[0], workweek);
                    jobs[0].JobDayCrewMember.push(member);
                    duplicateMember = true;
                }
            }
            var personCopy = Object.assign({
                InactiveReasonCode: '',
                InactiveReasonMeaning: ''
            }, person);
            _resetCrewMember(member, workweek.sp.jobStatus||[{}]);
            _updateMemberData(member, personCopy);
            _addPersonToComms(workweek, person.ID);
            if(duplicateMember && currentStep.ProcessStepCode == 'MT'){
                member.ClassID = '';
                _mapSmartpickBack(workweek, member, 'class', _spMappings.class);
                member.duplicate = true;
            }
            _cleanMember(workweek, member, jobs[0], day);
            for(var i=1; i<jobs.length; ++i){
                _moveCrewMember(member, jobs[0], jobs[i], workweek, true);
            }
            if(currentStep.ProcessStepCode == 'MT'){
                _initMemberTime(workweek, scope, true);
                day.currentMember = day.members.find(function(dayMember){ return dayMember.Jobs[0].Member === member }).index;
                $timeout(function(){membersCarousel.setActiveIndex(day.currentMember)})
                _changes(scope.workweek, true);
                scope.dialogs.addMemberDialog.hide();
                if(duplicateMember){
                    scope.dialogs.memberInfo.open({
                        member: day.members[day.currentMember],
                        memberJob: day.members[day.currentMember].Jobs[0],
                        jobIndex: 0
                    });
                    scope.dialogs.memberInfo._scope.data.error = 'Select a craft/class';
                }else{
                    _parseTimeToOneMember(day.members[day.currentMember], scope.workweek);
                    if(!existing)_loadMemberWeekData(scope, member);
                }
                return;
            }
            scope.dialogs.addMemberDialog.hide();
            _changes(scope.workweek, true);
            return member;
        }
        var _addCrewEquipment = function(equip, workweek, scope){
            var jobs = _activeJobs(workweek), day = _currentDay(workweek), existing = false;
            var equipment = jobs[0].JobDayCrewEquipment.find(function(eq){ return eq.EquipmentID == equip.ID });
            if(!equipment){
                equipment = _newCrewEquip(Object.assign(equip,{TypeCode: 'SM'}), workweek, jobs[0]);
                jobs[0].JobDayCrewEquipment.push(equipment);
            }else{
                existing = true;
            }
            _resetCrewEquip(equipment);
            _updateEquipData(equipment, equip);
            _cleanEquip(workweek, equipment, jobs[0], day);
            for(var i=1; i<jobs.length; ++i){
                _moveCrewEquip(equipment, jobs[0], jobs[i], workweek, true);
            }
            if(_currentStep(workweek).ProcessStepCode == 'ET'){
                _initEquipmentTime(workweek, scope, true);
                day.currentEquip = day.equipment.find(function(dayEquip){ return dayEquip.Jobs[0].Equipment.ID == equipment.ID }).index;
                $timeout(function(){equipCarousel.setActiveIndex(day.currentEquip)})
                scope.dialogs.addEquipmentDialog.hide();
                _changes(scope.workweek, true);
                _parseTimeToOneEquipment(day.equipment[day.currentEquip], scope.workweek);
                if(!existing)_loadEquipWeekData(scope, equipment);
                return;
            }
            scope.dialogs.addEquipmentDialog.hide();
            _changes(scope.workweek, true);
            return equipment;
        }
        var _trackRentalCompany = function(name, workweek){
            var test = name.toLowerCase().trim();
            var existing = workweek.sp.rentalCompanies.find(function(rc){ return rc.Name.toLowerCase().trim() == test });
            if(!existing){
                workweek.sp.rentalCompanies.push({
                    ID: '',
                    Name: name
                });
            }
        }
        var _addCrewRental = function(equip, workweek, scope){
            var jobs = _activeJobs(workweek), existing = false;
            var equipment = jobs[0].JobDayCrewEquipment.find(function(eq){ return eq.EquipmentID == equip.ID });
            if(!equipment){
                var obj = equip.ID?Object.assign(equip,{TypeCode: 'SR'}):Object.assign(equip,{
                    ID:_getTempID(workweek),
                    CategoryCategory:equip.category.Category,
                    CategorySortOrder:equip.category.SortOrder,
                    CategoryID: equip.category.ID,
                    RentalCompanyID: equip.company.ID||'',
                    RentalCompanyName: equip.company.Name,
                    TypeCode: 'SR'
                });
                if(equip.isNewCompany){
                    _trackRentalCompany(equip.company.Name, workweek);
                }
                equipment = _newCrewEquip(obj, workweek, jobs[0]);
                jobs[0].JobDayCrewEquipment.push(equipment);
            }else{
                existing = true;
            }
            _resetCrewEquip(equipment);
            _updateEquipData(equipment, equip);
            _cleanEquip(workweek, equipment, jobs[0], _currentDay(workweek));
            for(var i=1; i<jobs.length; ++i){
                _moveCrewEquip(equipment, jobs[0], jobs[i], workweek, true);
            }
            if(_currentStep(workweek).ProcessStepCode == 'ET'){
                var day = _currentDay(workweek);
                _initEquipmentTime(workweek, scope, true);
                day.currentEquip = jobs[0].JobDayCrewEquipment.findIndex(function(eq){ return eq.ID == equipment.ID });
                _changes(scope.workweek, true);
                _parseTimeToOneEquipment(day.equipment[day.currentEquip], scope.workweek);
                if(!existing)_loadEquipWeekData(scope, equipment);
                return;
            }
            _changes(scope.workweek, true);
        }
        var _newCrewMember = function(jobCrewMember, job, workweek, isForeman){
            var isJobCrew = typeof jobCrewMember.JobCrewID != 'undefined';
            var member = {
                ID: _getTempID(workweek),
                PersonID: isForeman?jobCrewMember.ForemanID:(jobCrewMember.PersonID||jobCrewMember.ID),
                PersonFullName: isForeman?jobCrewMember.ForemanFullName:(jobCrewMember.PersonFullName||jobCrewMember.FullName),
                PersonCraftID: isForeman?jobCrewMember.PersonCraftID:(jobCrewMember.PersonCraftID||jobCrewMember.CraftID),
                PersonClassID: isForeman?jobCrewMember.PersonClassID:(jobCrewMember.PersonClassID||jobCrewMember.ClassID)
            }
            if(isJobCrew){
                member.JobCrewMemberID = jobCrewMember.ID;
                member.JobCrewMemberActiveFlag = jobCrewMember.ActiveFlag;
                member.JobCrewMemberInactiveReasonCode = jobCrewMember.InactiveReasonCode;
                member.JobCrewMemberCraftID = jobCrewMember.CraftID;
                member.JobCrewMemberClassID = jobCrewMember.ClassID;
            }else{
                if(!isForeman){
                    if(!job.JobDayCrewMember.find(function(jdcm){ return jdcm.JobCrewMemberID&&jdcm.PersonID==member.PersonID })){
                        member.JobCrewMemberID = _getTempID(workweek);
                        member.JobCrewMemberActiveFlag = member.JobCrewMemberID+'_activeFlag';
                        member.JobCrewMemberInactiveReasonCode = member.JobCrewMemberID+'_inactiveReason';
                        member.JobCrewMemberCraftID = member.JobCrewMemberID+'_craftID';
                        member.JobCrewMemberClassID = member.JobCrewMemberID+'_classID';
                    }
                    
                }
            }
            return member;
        }
        var _newCrewEquip = function(jobCrewEquip, workweek, job){
            var isJobCrew = typeof jobCrewEquip.JobCrewID != 'undefined';
            var e = {
                ID: _getTempID(workweek),
                JobCrewEquipmentID: _getTempID(workweek),
                EquipmentID: jobCrewEquip.EquipmentID||jobCrewEquip.ID,
                EquipmentDescription: jobCrewEquip.EquipmentDescription||jobCrewEquip.Description,
                EquipmentEquipmentNumber: jobCrewEquip.EquipmentEquipmentNumber||jobCrewEquip.Number,
                CategoryID: jobCrewEquip.EquipmentCategoryID||jobCrewEquip.CategoryID,
                CategoryCategory: jobCrewEquip.CategoryCategory,
                CategorySortOrder: jobCrewEquip.CategorySortOrder,
                RentalCompanyID: jobCrewEquip.EquipmentRentalCompanyID||jobCrewEquip.RentalCompanyID,
                EquipmentTypeCode: jobCrewEquip.EquipmentTypeCode||jobCrewEquip.TypeCode
            }
            if(jobCrewEquip.RentalCompanyName){
                e.RentalCompanyName = jobCrewEquip.RentalCompanyName;
            }
            if(isJobCrew){
                e.JobCrewEquipmentID = jobCrewEquip.ID;
                e.JobCrewEquipmentActiveFlag = jobCrewEquip.ActiveFlag;
                e.JobCrewEquipmentInactiveReasonCode = jobCrewEquip.InactiveReasonCode;
            }else{
                if(!job.JobDayCrewEquipment.find(function(jdce){ return jdce.JobCrewEquipmentID&&jdce.EquipmentID==e.EquipmentID })){
                    e.JobCrewEquipmentID = _getTempID(workweek);
                    e.JobCrewEquipmentActiveFlag = e.JobCrewEquipmentID+'_activeFlag';
                    e.JobCrewEquipmentInactiveReasonCode = e.JobCrewEquipmentID+'_inactiveReason';
                }
            }
            return e;
        }
        var _resetCrewMember = function(member, memberStatuses){
            member.ActiveFlag = true;
            member.StatusCode = typeof member.StatusCode=='undefined'?'PE':member.StatusCode;
            member.TotalHours = typeof member.TotalHours=='undefined'?0:member.TotalHours;
            member.STHours = typeof member.STHours=='undefined'?0:member.STHours;
            member.OTHours = typeof member.OTHours=='undefined'?0:member.OTHours;
            member.DTHours = typeof member.DTHours=='undefined'?0:member.DTHours;
            member.WeekTotalHours = typeof member.WeekTotalHours=='undefined'?0:member.WeekTotalHours;
            member.WeekSTHours = typeof member.WeekSTHours=='undefined'?0:member.WeekSTHours;
            member.WeekOTHours = typeof member.WeekOTHours=='undefined'?0:member.WeekOTHours;
            member.WeekDTHours = typeof member.WeekDTHours=='undefined'?0:member.WeekDTHours;
            member.PerDiemAmount = typeof member.PerDiemAmount=='undefined'?0:member.PerDiemAmount;
            var activeStatus = memberStatuses?memberStatuses.find(function(st){ return st.Code == 'AC'}):null;
            if(activeStatus){
                member.MemberStatusCode = activeStatus.Code;
                member.MemberStatusCrewMemberActiveFlag = activeStatus.CrewMemberActiveFlag;
                member.MemberStatusDescription = activeStatus.Description;
                member.MemberStatusDraftCrewTimeFlag = activeStatus.DraftCrewTimeFlag;
            }else{
                member.MemberStatusCode = 'AC';
                member.MemberStatusCrewMemberActiveFlag = true;
                member.MemberStatusDescription = 'Active';
                member.MemberStatusDraftCrewTimeFlag = true;
            }            
            member.active = true;
        }
        var _resetCrewEquip = function(equip){
            equip.ActiveFlag = true;
            equip.StatusCode = 'PE';
            equip.TotalHours = 0;
        }
        var _updateMemberData = function(member, jobCrewMember, isForeman, isCrew){
            member.InactiveReasonCode = jobCrewMember.InactiveReasonCode;
            member.InactiveReasonMeaning = jobCrewMember.InactiveReasonMeaning;
            member.ClassID = isForeman?jobCrewMember.ForemanClassID:jobCrewMember.ClassID;
            member.ClassClass = isForeman?jobCrewMember.ForemanClassClass:jobCrewMember.ClassClass;
            member.ClassShortDescription = isForeman?jobCrewMember.ForemanClassShortDescription:jobCrewMember.ClassShortDescription;
            member.ClassCraftID = isForeman?jobCrewMember.ForemanClassCraftID:(jobCrewMember.ClassCraftID||jobCrewMember.CraftID);
            member.ClassOrganizationID = isForeman?jobCrewMember.ForemanClassOrganizationID:jobCrewMember.ClassOrganizationID;
            member.ClassSortOrder = isForeman?jobCrewMember.ForemanClassSortOrder:jobCrewMember.ClassSortOrder;
            member.CraftID = isForeman?jobCrewMember.ForemanCraftID:jobCrewMember.CraftID;
            member.CraftCraft = isForeman?jobCrewMember.ForemanCraftCraft:jobCrewMember.CraftCraft;
            member.CraftOrganizationID = isForeman?jobCrewMember.ForemanCraftOrganizationID:jobCrewMember.CraftOrganizationID;
            member.CraftSortOrder = isForeman?jobCrewMember.ForemanCraftSortOrder:jobCrewMember.CraftSortOrder;
            if(isCrew){
                member.JobCrewMemberID = jobCrewMember.ID;
                member.JobCrewMemberActiveFlag = true;
                member.JobCrewMemberClassID = jobCrewMember.ClassID;
                member.JobCrewMemberCraftID = jobCrewMember.CraftID;
                member.JobCrewMemberInactiveReasonCode = jobCrewMember.InactiveReasonCode;
            }
        }
        var _updateEquipData = function(equip, jobCrewEquip, isCrew){
            if(isCrew){
                equip.JobCrewEquipmentID = jobCrewEquip.ID;
                equip.JobCrewEquipmentActiveFlag = true;
                equip.JobCrewEquipmentInactiveReasonCode = jobCrewEquip.InactiveReasonCode;
            }
        }
        var _moveCrewEquip = function(equip, fromJdc, toJdc, workweek){
            var existingEquip = toJdc.JobDayCrewEquipment.find(function(eq){ return eq.EquipmentID == equip.EquipmentID });
            if(!existingEquip){
                existingEquip = _newCrewEquip(equip, workweek, toJdc);
                toJdc.JobDayCrewEquipment.push(existingEquip);
            }
            _resetCrewEquip(existingEquip);
            _updateEquipData(existingEquip, equip);
            existingEquip.ActiveFlag = equip.ActiveFlag;
            existingEquip.InactiveReasonCode = equip.InactiveReasonCode;
            existingEquip.hasPermanentChanges = equip.hasPermanentChanges;
            existingEquip.permanentMessage = equip.permanentMessage;
            _cleanEquip(workweek, existingEquip, toJdc, _currentDay(workweek));
            _mapSmartpickBack(workweek, existingEquip, 'inactiveReason', _spMappings.inactiveReason);
        }
        var _moveCrewMember = function(member, fromJdc, toJdc, workweek, onlyIfNew){
            var currentStep = _currentStep(workweek).ProcessStepCode;
            var existingMember = toJdc.JobDayCrewMember.find(function(mem){ return mem.PersonID == member.PersonID && (currentStep!='MT'||mem.ClassID == member.oldClassID)});
            if(!existingMember){
                existingMember = _newCrewMember(member, toJdc, workweek);
                toJdc.JobDayCrewMember.push(existingMember);
            }else if(onlyIfNew){
                return true;
            }
            _resetCrewMember(existingMember, workweek.sp.jobStatus);
            _updateMemberData(existingMember, member);
            existingMember.ActiveFlag = member.ActiveFlag;
            existingMember.MemberStatusCode = member.MemberStatusCode;
            existingMember.InactiveReasonCode = member.InactiveReasonCode;
            existingMember.NoPerDiemReasonCode = member.NoPerDiemReasonCode;
            existingMember.permanentMessage = member.permanentMessage;
            existingMember.hasPermanentChanges = member.hasPermanentChanges;
            existingMember.duplicate = member.duplicate;
            _cleanMember(workweek, existingMember, toJdc, _currentDay(workweek));
            _mapSmartpickBack(workweek, existingMember, 'jobStatus', _spMappings.jobStatus);
            _mapSmartpickBack(workweek, existingMember, 'inactiveReason', _spMappings.inactiveReason);
        }
        var _syncRosterItem = function(item, view, workweek, noSort, jobs){
            var step = _currentStep(workweek).ProcessStepCode;
            jobs = jobs||_activeJobs(workweek);
            if(view == 'JobDayCrewMember'){
                if(typeof item.oldClassID == 'undefined')item.oldClassID=item.ClassID;
                for(var j=1; j<jobs.length; ++j){
                    _moveCrewMember(item, jobs[0], jobs[j], workweek);
                }
                if(step == 'MT' && !noSort)_sortDayMembers(workweek);
                delete item.oldClassID;
            }else if(view == 'JobDayCrewEquipment'){
                for(var j=1; j<jobs.length; ++j){
                    _moveCrewEquip(item, jobs[0], jobs[j], workweek);
                }
                if(step == 'ET' && !noSort)_sortDayEquips(workweek);
            }
        }
        var _copyCrewState = function(fromJdc, toJdc){
            toJdc.hasInactiveMember = fromJdc.hasInactiveMember;
            toJdc.hasInactiveEquip = fromJdc.hasInactiveEquip;
            toJdc.hasInactiveRental = fromJdc.hasInactiveRental;
            toJdc.hasChanges = fromJdc.hasChanges;
            toJdc.hasChangesSM = fromJdc.hasChangesSM;
            toJdc.hasChangesSR = fromJdc.hasChangesSR;
        }
        var _moveCrew = function(fromJdc, toJdc, workweek){
            toJdc.JobDayCrewMember = Utils.enforceArray(toJdc.JobDayCrewMember);
            for(var m=0; m<fromJdc.JobDayCrewMember.length; ++m){
                var member = fromJdc.JobDayCrewMember[m];
                _moveCrewMember(member, fromJdc, toJdc, workweek);
            }
            toJdc.JobDayCrewEquipment = Utils.enforceArray(toJdc.JobDayCrewEquipment);
            for(var e=0; e<fromJdc.JobDayCrewEquipment.length; ++e){
                var equip = fromJdc.JobDayCrewEquipment[e];
                _moveCrewEquip(equip, fromJdc, toJdc, workweek);
            }
            _copyCrewState(fromJdc, toJdc);
        }
        var _addCrewForeman = function(jobCrew, job, workweek, day){
            var existingMember = job.JobDayCrewMember.find(function(mem){ return mem.PersonID == jobCrew.ForemanID});
            if(!existingMember){
                existingMember = _newCrewMember(jobCrew, job, workweek, true);
                job.JobDayCrewMember.push(existingMember);
            }
            _resetCrewMember(existingMember, workweek.sp.jobStatus);
            _updateMemberData(existingMember, jobCrew, true);
            _cleanMember(workweek, existingMember, undefined, day);
        }
        var _draftCrews = function(scope, workweek, crewData){
            var jobs = _activeJobs(workweek);
            if(!crewData){
                scope.loadingDialog();
                _load(scope, 'fte/crew', 'GET', {job: jobs.map(function(j){return j.JobCrewJobID}).join(','), crew: workweek.CrewID, date: Utils.date(_currentDay(workweek).WeekdayDate,'MM/dd/yyyy')}, 'loadCrewResult');
                return;
            }
            _loadCrewReferenceData(workweek, scope, crewData);
            if(crewData.error || (!crewData.JobCrew && !crewData.Comm)){
                _showMessage(crewData.error||'Failed to load crew for roster.', 'Error');
                return;
            }
            if(crewData.JobCrew){
                var day = workweek.WorkDay[workweek.currentDay];
                var jobs = _activeJobs(workweek);
                crewData.JobCrew.JobCrewMember = Utils.enforceArray(crewData.JobCrew.JobCrewMember);
                crewData.JobCrew.JobCrewEquipment = Utils.enforceArray(crewData.JobCrew.JobCrewEquipment);
                for(var j=0; j<jobs.length; j++){
                    var job = jobs[j], hasInactiveMember = false, hasInactiveEquip = false, hasInactiveRental = false;
                    if(j == 0){
                        job.JobCrewID = crewData.JobCrew.ID||_getTempID(workweek);
                        job.JobCrewCrewID = crewData.JobCrew.CrewID;
                    }
                    job.JobDayCrewMember = Utils.enforceArray(job.JobDayCrewMember);
                    _inactivateAllMembers(job);
                    _addCrewForeman(crewData.JobCrew, job, workweek, day);
                    for(var m=0; m<crewData.JobCrew.JobCrewMember.length; ++m){
                        var member = crewData.JobCrew.JobCrewMember[m];
                        if(member.ActiveFlag){
                            var existingMember = job.JobDayCrewMember.find(function(mem){ return mem.PersonID == member.PersonID });
                            if(!existingMember){
                                existingMember = _newCrewMember(member, job, workweek);
                                job.JobDayCrewMember.push(existingMember);
                            }
                            _resetCrewMember(existingMember, workweek.sp.jobStatus);
                            _updateMemberData(existingMember, member, false, j==0);
                            _cleanMember(workweek, existingMember, job, day);
                            hasInactiveMember = hasInactiveMember || (!existingMember.ActiveFlag && !_shouldHideMember(existingMember, workweek));
                        }
                        delete member.JobCrewID;
                    }
                    job.JobDayCrewEquipment = Utils.enforceArray(job.JobDayCrewEquipment);
                    _inactivateAllEquipment(job);
                    for(var e=0; e<crewData.JobCrew.JobCrewEquipment.length; ++e){
                        var equip = crewData.JobCrew.JobCrewEquipment[e];
                        if(equip.ActiveFlag){
                            var existingEquip = job.JobDayCrewEquipment.find(function(eq){ return eq.EquipmentID == equip.EquipmentID });
                            if(!existingEquip){
                                existingEquip = _newCrewEquip(equip, workweek, job);
                                job.JobDayCrewEquipment.push(existingEquip);
                            }
                            _resetCrewEquip(existingEquip);
                            _updateEquipData(existingEquip, equip, j==0);
                            _cleanEquip(workweek, existingEquip, job, day);
                            if(existingEquip.EquipmentTypeCode == 'SM'){
                                hasInactiveEquip = hasInactiveEquip || (!existingEquip.ActiveFlag && !_shouldHideEquip(existingEquip));
                            }else{
                                hasInactiveRental = hasInactiveRental || (!existingEquip.ActiveFlag && !_shouldHideEquip(existingEquip));
                            }
                        }
                        delete equip.JobCrewID;
                    }
                    _sortMembersOnJob(job);
                    _sortEquipOnJob(job);
                    job.hasInactiveMember = hasInactiveMember;
                    job.hasInactiveEquip = hasInactiveEquip;
                    job.hasInactiveRental = hasInactiveRental;
                }
            }

            _initSafety(workweek, crewData)
        }

        var _initSafety = function(workweek, data){
            var _indicators = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789'
            var day = _currentDay(workweek);
            var jobs = _activeJobs(workweek);
            day.showSafety = false;
            _initSafetyData(workweek, data);
            day.Comm = data.Comm;
            for(var i=0; i<day.Comm.length; ++i){
                day.Comm[i].Person = Utils.enforceArray(day.Comm[i].Person);
                day.Comm[i].people = {}
                for(var p=0; p<day.Comm[i].Person.length; ++p){
                    day.Comm[i].people[day.Comm[i].Person[p].ID] = day.Comm[i].Person[p];
                    if(!day.showSafety && !day.Comm[i].Person[p].ReviewedFlag){
                        day.showSafety = true;
                    }
                }
                delete day.Comm[i].Person;
                var members = jobs[0].JobDayCrewMember, allDone = true;
                for(var m=0; m<members.length; ++m){
                    if(!_shouldHideMember(members[m], workweek)){
                        if(!day.Comm[i].people[members[m].PersonID]){
                            day.Comm[i].people[members[m].PersonID] = {
                                ID: members[m].PersonID,
                                ReviewedFlag: false,
                                ReviewedDate: ''
                            }
                        }
                        if(!day.Comm[i].people[members[m].PersonID].ReviewedFlag){
                            allDone = false;
                        }
                    }
                }
                day.Comm[i].indicator = _indicators.charAt(i);
                if(allDone){
                    day.Comm.splice(i,1);
                    --i;
                }
            }
            day.hasSafety = day.Comm.length>0;
            if(!day.hasSafety) day.showSafety = false;
        }
        var _initSafetyData = function(workweek, data){
            var dayCom;
            data.Comm = Utils.enforceArray(data.Comm);
            for(var i=0; i<data.Comm.length; ++i){
                data.Comm[i].Person = Utils.enforceArray(data.Comm[i].Person);
            }
            for(var d=0; d<workweek.WorkDay.length; ++d){
                if(d !== workweek.currentDay){
                    var comms = workweek.WorkDay[d].Comm;
                    if(comms && comms.length){
                        for(var c=0; c<data.Comm.length; ++c){
                            if((dayCom = comms.find(function(com){ return com.ID == data.Comm[c].ID }))){
                                data.Comm[c].reviewed = dayCom.reviewed||false;
                                var ids = Object.keys(dayCom.people);
                                for(var i=0; i<ids.length; ++i){
                                    var person = data.Comm[c].Person.find(function(p){ return p.ID == ids[i]});
                                    if(!person){
                                        person = { ID: ids[i], ReviewedFlag: false, ReviewedDate: '' };
                                        data.Comm[c].Person.push(person);
                                    }
                                    if(dayCom.people[ids[i]].ReviewedFlag){
                                        person.ReviewedFlag = dayCom.people[ids[i]].ReviewedFlag;
                                        person.ReviewedDate = dayCom.people[ids[i]].ReviewedDate;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        var _changedSafetyData = function(workweek, com, person){
            var day = workweek.WorkDay[workweek.currentDay];
            var dayCom;
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var comms = workweek.WorkDay[d].Comm;
                if(comms && comms.length){
                    if(d !== workweek.currentDay){
                        if((dayCom = comms.find(function(comm){ return com.ID == comm.ID }))){
                            if(person){
                                var dayPerson = dayCom.people[person.ID];
                                if(!dayPerson){
                                    dayPerson = { ID: person.ID, ReviewedFlag: false, ReviewedDate: '' };
                                    dayCom.people[person.ID] = dayPerson;
                                }
                                dayPerson.ReviewedFlag = person.ReviewedFlag;
                                dayPerson.ReviewedDate = person.ReviewedDate;
                            }else{
                                var ids = Object.keys(com.people);
                                for(var i=0; i<ids.length; ++i){
                                    var person = dayCom.people[ids[i]];
                                    if(!person){
                                        person = { ID: ids[i], ReviewedFlag: false, ReviewedDate: '' };
                                        dayCom.people[ids[i]] = person;
                                    }
                                    person.ReviewedFlag = com.people[ids[i]].ReviewedFlag;
                                    person.ReviewedDate = com.people[ids[i]].ReviewedDate;
                                }
                            }
                        }
                    }
                    var show = false;
                    for(var i=0; i<comms.length; ++i){
                        var ids = Object.keys(comms[i].people);
                        for(var p=0; p<ids.length; ++p){
                            if(!comms[i].people[ids[p]].ReviewedFlag){
                                show = true;
                            }
                        }
                    }
                    workweek.WorkDay[d].showSafety = show;
                }
            }
        }
        var _unsetMemberSafety = function(workweek, personID){
            var day = _currentDay(workweek);
            var date = Utils.date(day.WeekdayDate, 'MM/dd/yyyy');
            for(var i=0; i<day.Comm.length; ++i){
                if(day.Comm[i].people[personID] && day.Comm[i].people[personID].ReviewedDate == date){
                    day.Comm[i].people[personID].ReviewedFlag = false
                    day.Comm[i].people[personID].ReviewedDate = ''
                    day.Comm[i].people[personID].changed = true;
                    _changedSafetyData(workweek, day.Comm[i], day.Comm[i].people[personID])
                }
            }
        }
        var _addPersonToComms = function(workweek, personID){
            var comms = _currentDay(workweek).Comm;
            if(comms && comms.length){
                for(var c=0; c<comms.length; ++c){
                    if(!comms[c].people[personID]){
                        comms[c].people[personID] = { ID: personID, ReviewedFlag: false, ReviewedDate: '' };
                    }
                }
            }
        }
        var _syncSafetyReviewed = function(comm, workweek){
            var dayCom;
            for(var d=0; d<workweek.WorkDay.length; ++d){
                if(d !== workweek.currentDay){
                    var comms = workweek.WorkDay[d].Comm;
                    if(comms && comms.length){
                        if((dayCom = comms.find(function(com){ return com.ID == comm.ID }))){
                           dayCom.reviewed = comm.reviewed;
                        }
                    }
                }
            }
        }

        var _findActiveMemberOnDayByPersonID = function(personId, day){
            for(var j=0; j<day.JobDayCrew.length; ++j){
                for(var m=0; m<day.JobDayCrew[j].JobDayCrewMember.length; ++m){
                    if(day.JobDayCrew[j].JobDayCrewMember[m].ActiveFlag && day.JobDayCrew[j].JobDayCrewMember[m].PersonID == personId){
                        return true;
                    }
                }
            }
            return false;
        }
        var _checkRequiredComms = function(workweek, dayIndex){
            var day = workweek.WorkDay[dayIndex];
            var comms = day.Comm;
            if(comms && comms.length){
                for(var c=0; c<comms.length; ++c){
                    if(comms[c].CompletionTypeCode=='REQ' && comms[c].Block){
                        var ids = Object.keys(comms[c].people);
                        for(var p=0; p<ids.length; ++p){
                            if(!comms[c].people[ids[p]].ReviewedFlag && _findActiveMemberOnDayByPersonID(ids[p], day)){
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        var _loadCrewReferenceData = function(workweek, scope, data){
            if(!data){
                scope.loadingDialog();
                _load(scope, 'fte/sp/crewReference', 'GET', {}, 'loadCrewReferenceResult');
                return;
            }
            if(data.JobDayCrewMemberStatus) workweek.sp.jobStatus = Utils.enforceArray(data.JobDayCrewMemberStatus);
            if(data.MemberInactiveReason) workweek.sp.memberInactiveReason = Utils.enforceArray(data.MemberInactiveReason);
            if(data.EquipmentInactiveReason) workweek.sp.equipmentInactiveReason = Utils.enforceArray(data.EquipmentInactiveReason);
            if(data.NoPerDiemReason) workweek.sp.noPerDiemReason = Utils.enforceArray(data.NoPerDiemReason);
        }
        var _calcHasPermanentMemberChanges = function(member, workweek, crew){
            var changes = false, crew = crew||_currentDay(workweek).JobDayCrew[0];
            member.permanentMessage = '';
            if(member.PersonID != workweek.PersonID && !member.duplicate){
                if((!member.JobCrewMemberID || Utils.isTempId(member.JobCrewMemberID))){
                    member.permanentMessage = 'Permanently Add To Crew';
                    changes = true;
                }else{
                    if(member.ActiveFlag != member.JobCrewMemberActiveFlag || (!member.ActiveFlag && member.InactiveReasonCode != member.JobCrewMemberInactiveReasonCode)|| member.CraftID != member.JobCrewMemberCraftID || member.ClassID != member.JobCrewMemberClassID){
                        member.permanentMessage = 'Make Changes Permanent';
                        changes = true;
                    }
                }
            }
            crew.hasChanges = changes||(crew.JobDayCrewMember.findIndex(function(jdcm){ return jdcm.permanentMessage })>-1);
            return changes;
        }
        var _checkMemberPermanent = function(member, workweek){
            var crew = _currentDay(workweek).JobDayCrew[0];
            if(!member.JobCrewMemberID || !Utils.isTempId(member.JobCrewMemberID)){
                if(member.hasPermanentChanges){
                    for(var m=0; m<crew.JobDayCrewMember.length; ++m){
                        var mem = crew.JobDayCrewMember[m];
                        if(mem.ID == member.ID){
                            mem.duplicate = false;
                        }else if(mem.PersonID == member.PersonID){
                            mem.duplicate = true;
                        }
                    }
                }else{
                    for(var m=0; m<crew.JobDayCrewMember.length; ++m){
                        var mem = crew.JobDayCrewMember[m];
                        if(mem.PersonID == member.PersonID){
                            delete mem.duplicate;
                        }
                    }
                }
            }
        }
        var _memberPermanent = function(member, jobs, workweek){
            if(!member.JobCrewMemberID){
                member.JobCrewMemberID = _getTempID(workweek);
            }
            member.JobCrewMemberActiveFlag = member.ActiveFlag;
            member.JobCrewMemberInactiveReasonCode = member.InactiveReasonCode;
            member.JobCrewMemberCraftID = member.CraftID;
            member.JobCrewMemberClassID = member.ClassID;
            member.permanentMessage = '';
            _syncRosterItem(member, 'JobDayCrewMember', workweek, true, jobs);
        }
        var _calcHasPermanentEquipChanges = function(equip, workweek, crew){
            var changes = false, crew = crew||_currentDay(workweek).JobDayCrew[0];
            equip.permanentMessage = '';
            if((!equip.JobCrewEquipmentID || Utils.isTempId(equip.JobCrewEquipmentID)) && !equip.hasPermanentChanges){
                equip.permanentMessage = 'Permanently Add To Crew';
                changes = true;
            }else{
                if(equip.ActiveFlag != equip.JobCrewEquipmentActiveFlag || (!equip.ActiveFlag && equip.InactiveReasonCode != equip.JobCrewEquipmentInactiveReasonCode)){
                    equip.permanentMessage = 'Make Changes Permanent';
                    changes = true;
                }
            }
            crew['hasChanges'+equip.EquipmentTypeCode] = changes||(crew.JobDayCrewEquipment.findIndex(function(jdcm){ return jdcm.EquipmentTypeCode==equip.EquipmentTypeCode&&jdcm.permanentMessage })>-1);
            return changes;
        }
        var _checkEquipPermanent = function(equip, workweek){
            //do nothing
        }
        var _equipPermanent = function(equip, jobs, workweek){
            if(!equip.JobCrewEquipmentID){
                equip.JobCrewEquipmentID = _getTempID(workweek);
            }
            equip.JobCrewEquipmentActiveFlag = equip.ActiveFlag;
            equip.JobCrewEquipmentInactiveReasonCode = equip.InactiveReasonCode;
            equip.hasPermanentChanges = true;
            equip.permanentMessage = '';
            _syncRosterItem(equip, 'JobDayCrewEquipment', workweek, true, jobs);
        }
        var _shouldHideMember = function(member, workweek, ignoreMessage){
            return !member.ActiveFlag && !member.JobCrewMemberActiveFlag && (ignoreMessage||!member.permanentMessage) && member.PersonID != workweek.PersonID;
        }
        var _changeCurrentDayMember = function(day, workweek){
            day = day||_currentDay(workweek);
            for(var m=0; m<day.members.length; ++m){
                if(!_shouldHideMember(day.members[m].Jobs[0].Member, workweek)){
                    day.currentMember = m;
                    break;
                }
            }
        }
        var _shouldHideEquip = function(equip, ignoreMessage){
            return !equip.ActiveFlag && !equip.JobCrewEquipmentActiveFlag && (ignoreMessage||!equip.permanentMessage);
        }
        var _changeCurrentDayEquipment = function(day){
            for(var e=0; e<day.equipment.length; ++e){
                if(!_shouldHideEquip(day.equipment[e].Jobs[0].Equipment)){
                    day.currentEquip = e;
                    break;
                }
            }
        }
        var _updateJobHasInactive = function(job, which, workweek){
            if(which == 'member'){
                var hasInactiveMember = false;
                for(var m=0; m<job.JobDayCrewMember.length; ++m){
                    if(!job.JobDayCrewMember[m].ActiveFlag && !_shouldHideMember(job.JobDayCrewMember[m], workweek)){
                        hasInactiveMember = true;
                        break;
                    }
                }
                job.hasInactiveMember = hasInactiveMember;
            }else{
                var hasInactiveEquip = false, hasInactiveRental = false;
                for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                    if(!job.JobDayCrewEquipment[e].ActiveFlag && !_shouldHideEquip(job.JobDayCrewEquipment[e])){
                        if(job.JobDayCrewEquipment[e].EquipmentTypeCode == 'SM'){
                            hasInactiveEquip = true;
                        }else{
                            hasInactiveRental = true;
                        }
                    }
                    if(hasInactiveEquip&&hasInactiveRental) break;
                }
                job.hasInactiveEquip = hasInactiveEquip;
                job.hasInactiveRental = hasInactiveRental;
            }
        }
        var _processPermanentChanges = function(workweek){
            //Process the permanent changes
            var changedDays = [];
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d], updatedInactiveReasons = {members:false,equips:false};
                var jobs = _activeJobs(workweek, day.WeekdayDate);
                if(jobs.length){
                    var changes = {members:false, SM:false, SR:false}, changedDay = {index:d,members:[],equip:[]}, members = [], equips = [];
                    if(jobs[0].JobDayCrewMember){
                        for(var m=0; m<jobs[0].JobDayCrewMember.length; ++m){
                            var member = jobs[0].JobDayCrewMember[m];
                            if(member.hasPermanentChanges){
                                _memberPermanent(member, jobs, workweek);
                                members.push(member);
                                if(member.JobCrewMemberID&&!Utils.isTempId(member.JobCrewMemberID)){
                                    changedDay.members.push(member);
                                }
                            }
                            changes.members = changes.members||member.permanentMessage;
                        }
                    }
                    for(var m=0; m<members.length; ++m){
                        if(_shouldHideMember(members[m], workweek, true)){
                            if(day.DayProcessCompletion[day.currentStep].ProcessStepCode == 'MT' && members[m] === day.members[day.currentMember].Jobs[0].Member){
                                _changeCurrentDayMember(day, workweek);
                            }
                            if(!updatedInactiveReasons.members){
                                _updateJobHasInactive(jobs[0], 'member', workweek);
                               updatedInactiveReasons.members = true; 
                            }
                        }
                    }
                    jobs[0].hasChanges = changes.members?true:false;
                    if(jobs[0].JobDayCrewEquipment){
                        for(var e=0; e<jobs[0].JobDayCrewEquipment.length; ++e){
                            var equip = jobs[0].JobDayCrewEquipment[e];
                            if(equip.hasPermanentChanges){
                                _equipPermanent(equip, jobs, workweek);
                                equips.push(equip);
                                if(equip.JobCrewEquipmentID&&!Utils.isTempId(equip.JobCrewEquipmentID)){
                                    changedDay.equip.push(equip);
                                }
                            }
                            changes[equip.EquipmentTypeCode] = changes[equip.EquipmentTypeCode]||equip.permanentMessage;
                        }
                    }
                    for(var e=0; e<equips.length; ++e){
                        if(_shouldHideEquip(equips[e], true)){
                            if(day.DayProcessCompletion[day.currentStep].ProcessStepCode == 'ET' && equips[e] === day.equipment[day.currentEquip].Jobs[0].Equipment){
                                _changeCurrentDayEquipment(day);
                            }
                            if(!updatedInactiveReasons.equips){
                                _updateJobHasInactive(jobs[0], 'equip');
                               updatedInactiveReasons.equips = true; 
                            }
                        }
                    }
                    jobs[0].hasChangesSM = changes.SM?true:false;
                    jobs[0].hasChangesSR = changes.SR?true:false;
                    if(changedDay.members.length||changedDay.equip.length){
                        changedDays.push(changedDay);
                    }
                }
            }
            //Now update others on the week that are no longer reflecting the job crew
            for(var cd=0; cd<changedDays.length; ++cd){
                var changedDay = changedDays[cd];
                for(var d=0; d<workweek.WorkDay.length; ++d){
                    if(d==changedDay.index) continue;
                    var day = workweek.WorkDay[d];
                    var jobs = _activeJobs(workweek, day.WeekdayDate);
                    var job = jobs[0];
                    if(job){
                        for(var m=0; m<changedDay.members.length; ++m){
                            var member = changedDay.members[m];
                            for(var om=0; om<job.JobDayCrewMember.length; ++om){
                                var otherMember = job.JobDayCrewMember[om];
                                if(member.JobCrewMemberID == otherMember.JobCrewMemberID){
                                    otherMember.JobCrewMemberActiveFlag = member.JobCrewMemberActiveFlag
                                    otherMember.JobCrewMemberInactiveReasonCode = member.JobCrewMemberInactiveReasonCode
                                    otherMember.JobCrewMemberCraftID = member.JobCrewMemberCraftID
                                    otherMember.JobCrewMemberClassID = member.JobCrewMemberClassID
                                    _calcHasPermanentMemberChanges(otherMember, workweek, job);
                                    _syncRosterItem(otherMember, 'JobDayCrewMember', workweek, true, jobs);
                                    break;
                                }
                            }
                        }
                        for(var e=0; e<changedDay.equip.length; ++e){
                            var equip = changedDay.equip[e];
                            for(var oe=0; oe<job.JobDayCrewEquipment.length; ++oe){
                                var otherEquip = job.JobDayCrewEquipment[oe];
                                if(equip.JobCrewEquipmentID == otherEquip.JobCrewEquipmentID){
                                    otherEquip.JobCrewEquipmentActiveFlag = equip.JobCrewEquipmentActiveFlag
                                    otherEquip.JobCrewEquipmentInactiveReasonCode = equip.JobCrewEquipmentInactiveReasonCode
                                    _calcHasPermanentEquipChanges(otherEquip, workweek, job);
                                    _syncRosterItem(otherEquip, 'JobDayCrewEquipment', workweek, true, jobs);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        var _getLocationsToInclude = function(dataObj, workweek){
            var jobIds = [];
            for(var j=0; j<workweek.Job.length; ++j){
                if(dataObj.jobs.includes(workweek.Job[j].ID) && _isLocationJob(workweek.Job[j])){
                    jobIds.push(workweek.Job[j].ID);
                }
            }
            if(jobIds.length){
                var jobs = _activeJobs(workweek), locations = [];
                for(var j=0; j<jobs.length; ++j){
                    if(jobIds.includes(jobs[j].JobCrewJobID) && jobs[j].cwLocations && jobs[j].cwLocations.length){
                        var args = jobs[j].cwLocations.slice(0);
                        if(jobs[j].Task && jobs[j].Task.length){
                            for(var t=0; t<jobs[j].Task.length; ++t){
                                if(jobs[j].Task[t].active && jobs[j].Task[t].LocationID && args.indexOf(jobs[j].Task[t].LocationID) == -1){
                                    args.push(jobs[j].Task[t].LocationID);
                                }
                            }
                        }
                        args.unshift(0);
                        args.unshift(locations.length);
                        Array.prototype.splice.apply(locations, args);
                    }
                }
                if(locations.length){
                    dataObj.locations = locations.join(',');
                }
            }
        }
        var _cleanJobLocation = function(location, workweek){
            if(location.LocationPhoto){
                workweek.Photos.Location[location.ID] = Utils.enforceArray(location.LocationPhoto);
                delete location.LocationPhoto;
            }
        }
        var addContractTimeValueToJobs = function(itemName, contract, jobs){
            contract[itemName] = Utils.enforceArray(contract[itemName]);
            for(var wt=0; wt<contract[itemName].length; ++wt){
                var workingTime = contract[itemName][wt];
                var wtJobs = workingTime.JobID.toString().split(',').map(function(id){ return parseInt(id) });
                for(var j=0; j<wtJobs.length; ++j){
                    var job = jobs.find(function(jb){ return wtJobs[j] == jb.JobCrewJobID; });
                    if(job){
                        job[itemName] = job[itemName]||[];
                        job[itemName].push(workingTime);
                    }
                }
            }
        }
        var _prepareContractWorkingTimeData = function(data, jobs, key){
            key = key || 'Contract';
            data[key] = Utils.enforceArray(data[key]);
            for(var c=0; c<data[key].length; ++c){
                if(data[key][c].ContractWorkingTime || data[key][c].ContractTimeOverrideDay){
                    addContractTimeValueToJobs('ContractWorkingTime', data[key][c], jobs);
                    addContractTimeValueToJobs('ContractTimeOverrideDay', data[key][c]), jobs;
                }
                if(data[key][c].CWWorkingTime || data[key][c].CWTimeOverrideDay){
                    addContractTimeValueToJobs('CWWorkingTime', data[key][c], jobs);
                    addContractTimeValueToJobs('CWTimeOverrideDay', data[key][c], jobs);
                }
            }
        }
        var _loadJobTasksInfo = function(scope, workweek, jobIdsOrDataObj, data, callback){
            callback = callback||'loadJobTasksResult';
            if(Array.isArray(jobIdsOrDataObj) || typeof jobIdsOrDataObj=='string') jobIdsOrDataObj = {jobs: jobIdsOrDataObj};
            if(!Array.isArray(jobIdsOrDataObj.jobs)) jobIdsOrDataObj.jobs = Utils.enforceArray((jobIdsOrDataObj.jobs||'').split(','));
            var addContractTimeValueToJobs = function(itemName, contract){
                contract[itemName] = Utils.enforceArray(contract[itemName]);
                for(var wt=0; wt<contract[itemName].length; ++wt){
                    var workingTime = contract[itemName][wt];
                    var wtJobs = workingTime.JobID.toString().split(',').map(function(id){ return parseInt(id) });
                    for(var j=0; j<wtJobs.length; ++j){
                        var job = jobs.find(function(jb){ return wtJobs[j] == jb.JobCrewJobID; });
                        if(job){
                            job[itemName] = job[itemName]||[];
                            job[itemName].push(workingTime);
                        }
                    }
                }
            }
            if(!data){
                scope.loadingDialog();
                jobIdsOrDataObj.org = workweek.PersonOrganizationID;
                _getLocationsToInclude(jobIdsOrDataObj, workweek);
                if(Array.isArray(jobIdsOrDataObj.jobs)) jobIdsOrDataObj.jobs = jobIdsOrDataObj.jobs.join(',');
                _load(scope, 'fte/jobtasks', 'GET', jobIdsOrDataObj, callback);
                return;
            }
            if(!data || !data.ContractPhaseCode || data.error){
                _showMessage(data.error||'Failed to load Job Tasks.', 'Error');
                return;
            }
            var jobs = _activeJobs(workweek);

            // Job Tasks
            data.JobTask = Utils.enforceArray(data.JobTask);
            for(var j=0; j<jobs.length; j++){
                var job = jobs[j];
                job.contractPhaseCodes = [];
                if(!job.JobTask){
                    job.JobTask = [];
                }
                for(var jt=0; jt<data.JobTask.length; ++jt){
                    var task = data.JobTask[jt];
                    if(task.JobID == job.JobCrewJobID && !job.JobTask.find(function(jbtsk){ return jbtsk.ID == task.ID})){
                        job.JobTask.push(task);
                    }
                }
            }

            // Locations
            if(data.JobDesign){
                data.JobDesign = Utils.enforceArray(data.JobDesign);
                workweek.jobLocations = workweek.jobLocations||{};
                for(var j=0; j<data.JobDesign.length; ++j){
                    workweek.jobLocations[data.JobDesign[j].JobID] = workweek.jobLocations[data.JobDesign[j].JobID]||[];
                    data.JobDesign[j].Location = Utils.enforceArray(data.JobDesign[j].Location);
                    for(var l=0; l<data.JobDesign[j].Location.length; ++l){
                        _cleanJobLocation(data.JobDesign[j].Location[l], workweek);
                        var existing = workweek.jobLocations[data.JobDesign[j].JobID].find(function(loc){ return loc.ID == data.JobDesign[j].Location[l].ID });
                        if(!existing){
                            workweek.jobLocations[data.JobDesign[j].JobID].push(data.JobDesign[j].Location[l]);
                        }else{
                            Object.assign(existing, data.JobDesign[j].Location[l])
                        }
                    }
                }
                for(var j=0; j<jobs.length; ++j){
                    for(var t=0; t<jobs[j].Task.length; ++t){
                        if(jobs[j].Task[t].LocationID){_updateTaskPhoto(jobs[j].Task[t], workweek)};
                    }
                }
            }

            // Contract Phase Codes
            if(data.ContractPhaseCode){
                data.ContractPhaseCode = Utils.enforceArray(data.ContractPhaseCode);
                for(var pc=0; pc<data.ContractPhaseCode.length; ++pc){
                    var item = data.ContractPhaseCode[pc];
                    var pcJobs = item.JobID.toString().split(',').map(function(id){ return parseInt(id) });
                    for(var j=0; j<pcJobs.length; ++j){
                        var job = jobs.find(function(jb){ return pcJobs[j] == jb.JobCrewJobID; });
                        if(job){
                            job.contractPhaseCodes.push(item);
                        }
                    }
                }
            }
            // Workgroup Phase Codes
            workweek.wgPhaseCodes = [];
            if(data.PhaseCode){
                data.PhaseCode = Utils.enforceArray(data.PhaseCode);
                for(var pc=0; pc<data.PhaseCode.length; ++pc){
                    var item = data.PhaseCode[pc];
                    workweek.wgPhaseCodes.push(item);
                }
            }

            // Contract Working Times
            _prepareContractWorkingTimeData(data, jobs);

            var step = _currentStep(workweek);
            if(callback=='loadJobTasksResult' && (step.subSteps && step.subSteps.current == 1)){
                _initCrewTime(workweek, scope);
            }else{
                _showJobTaskLevelsDay(workweek);
            }
        }
        var _taskScreenInfo = function(workweek, scope, callback){
            var jobs = _activeJobs(workweek);
            if(scope.editable){
                _clearMemberAndEquipTime(jobs);
            }
            if(scope.editable && !workweek.FromCompleteWork && (jobs.find(function(jb){ return !jb.JobTask || (!_isOverheadJob(jb,workweek) && (!jb.ContractWorkingTime && !jb.CWWorkingTime)) || (workweek.launchLocationPercent&&(!workweek.completeJobIds||!workweek.completeJobIds.length||!!workweek.completeJobIds.find(function(cjb){return cjb.JobID==jb.JobCrewJobID}))&&!workweek.jobLocations[jb.JobCrewJobID]) }))){
                _loadJobTasksInfo(scope, workweek, jobs.reduce(function(arr, jb){ arr.push(jb.JobCrewJobID); return arr; }, []), null, callback);
            }else if(!callback){
                _calcMeals(workweek);
                _initCrewTime(workweek, scope);
            }
            _markJobsWorkComplete(_currentDay(scope.workweek))
        }
        var _moveJobTask = function(scope, index){
            var ww = scope.workweek, fromLabel = 'Complete', toLabel = 'Active', taskTypes = scope.dialogs.pickTask._scope.data.taskTypes
            if(taskTypes[addTaskTabbar.getActiveTabIndex()].label == 'Active'){
                 fromLabel = 'Active';
                 toLabel = 'Complete';
            }
            var toIndex = taskTypes.findIndex(function(list){ return list.label == toLabel});
            var fromList = taskTypes.find(function(list){ return list.label == fromLabel}),
                toList = taskTypes[toIndex];
            var task = fromList.tasks.splice(index, 1)[0];
            toList.tasks.push(task);
            fromList.canComplete = (fromList.tasks.findIndex(function(t){return t.canComplete}) > -1);
            toList.canComplete = (toList.tasks.findIndex(function(t){return t.canComplete}) > -1);
        }
        var _phaseCodeToJobTaskPhaseCode = function(pc, workweek){
            var isContractPC = (pc.PhaseCodeID > 0);
            return {
                ID: _getTempID(workweek),
                ActiveFlag: true,
                PhaseCodeID: isContractPC ? pc.PhaseCodeID : pc.ID,
                PhaseCodeCodeNumber: isContractPC ? pc.PhaseCodeCodeNumber : pc.CodeNumber,
                PhaseCodeDisplayNumber: isContractPC ? pc.PhaseCodeDisplayNumber : pc.DisplayNumber,
                PhaseCodeDescription: isContractPC ? pc.PhaseCodeDescription : pc.Description,
                PhaseCodeOverrideNumber: isContractPC ? pc.PhaseCodeOverrideNumber : pc.OverrideNumber,
                PhaseCodeOverrideTypeCode: isContractPC ? pc.PhaseCodeOverrideTypeCode : pc.OverrideTypeCode,
                OverrideTypeMeaning2: isContractPC ? pc.OverrideTypeMeaning : pc.OverrideTypeMeaning,
                PhaseCodePhaseCodeSectionID: isContractPC ? pc.PhaseCodePhaseCodeSectionID : pc.SectionID,
                PhaseCodeSectionSectionNumber2: isContractPC ? pc.PhaseCodeSectionSectionNumber : pc.PhaseCodeSectionSectionNumber,
                PhaseCodeSortOrder: isContractPC ? pc.PhaseCodeSortOrder : pc.SortOrder,
                PhaseCodeIncludeInOTCalcFlag: isContractPC ? pc.PhaseCodeIncludeInOTCalcFlag : pc.IncludeInOTCalcFlag
            };
        }
        var _newJobTaskPhaseCodeReturn = function(scope, jobTask, dialog){
            var isNewTask = !jobTask || Utils.isTempId(jobTask.ID);
            jobTask = jobTask || scope.dialogs.pickTask._scope.data.newTask;
            var phaseCodes = dialog._scope.data.phaseCodes;
            for(var pc=0; pc<phaseCodes.length; ++pc){
                var phasecode = phaseCodes[pc], jtPhasecode;
                if(phasecode.selected){
                    if(!(jtPhasecode = jobTask.JobTaskPhaseCode.find(function(jtpc){ return jtpc.PhaseCodeID == (phasecode.PhaseCodeID||phasecode.ID)}))){
                        jobTask.JobTaskPhaseCode.push(_phaseCodeToJobTaskPhaseCode(phasecode, scope.workweek));
                    }else{
                        jtPhasecode.ActiveFlag = true;
                    }
                }else{
                    var index = jobTask.JobTaskPhaseCode.findIndex(function(jtpc){ return jtpc.PhaseCodeID == (phasecode.PhaseCodeID||phasecode.ID)});
                    if(index > -1){
                        jobTask.JobTaskPhaseCode[index].ActiveFlag = false;
                        if(isNewTask){
                            jobTask.JobTaskPhaseCode.splice(index, 1);
                        }
                    }
                }
            }
            _sortPhases(jobTask, 'JobTaskPhaseCode');
        }
        var _taskPhaseCodeReturn = function(scope, dialog){
            var changed = false, data = dialog._scope.data;
            if(data.taskMode == 'CrewMember'){
                var job = data.job.Job, task = data.task.Task;
                var memberTask = data.task;
                var jobTask = job.JobTask.find(function(jt){ return jt.PhaseCodeSectionID == task.PhaseCodeSectionID });
                _newJobTaskPhaseCodeReturn(scope, jobTask, dialog);
                for(var pc=0; pc<jobTask.JobTaskPhaseCode.length; ++pc){
                    var jtPhase = jobTask.JobTaskPhaseCode[pc];
                    var taskPC = task.MemberTaskPhaseCode.find(function(tpc){ return tpc.PhaseCodeID == jtPhase.PhaseCodeID });
                    if(!taskPC || !taskPC.active){
                        if(!taskPC){
                            taskPC = _getMemberPhaseCodeFromJTPC(jtPhase, scope.workweek);
                            task.MemberTaskPhaseCode.push(taskPC);
                        }
                        taskPC.active = true;
                        var memberPhaseCode = {
                            PhaseCode: taskPC,
                            STHours: 0,
                            OTHours: 0,
                            DTHours: 0,
                            TotalHours: 0
                        }
                        memberTask.PhaseCodes.push(memberPhaseCode);
                        changed = true;
                    }
                }
                for(var pc=task.MemberTaskPhaseCode.length-1; pc>=0; --pc){
                    var taskPC = task.MemberTaskPhaseCode[pc];
                    var jtPhase = jobTask.JobTaskPhaseCode.find(function(jtpc){ return jtpc.PhaseCodeID == taskPC.PhaseCodeID });
                    if(!jtPhase || !jtPhase.ActiveFlag){
                        taskPC.active = false;
                        taskPC.STHours = taskPC.OTHours = taskPC.DTHours = 0;
                        _updateMemberPhaseCodeTimeEntry(data.job.Member, {PhaseCode:taskPC}, task, ['ST','OT','DT'], scope.workweek, true);
                        var memberTaskIndex = memberTask.PhaseCodes.findIndex(function(mtp){ return mtp.PhaseCode.PhaseCodeID == taskPC.PhaseCodeID });
                        if(memberTaskIndex > -1){
                            memberTask.PhaseCodes.splice(memberTaskIndex, 1);
                            changed = true;
                        }
                    }
                }
                _sortPhases(task, 'MemberTaskPhaseCode');
                _totalMemberTime(scope.workweek, 'current');
            }else{
                var job = data.job, task = data.task;
                var jobTask = job.JobTask.find(function(jt){ return jt.ID == task.JobTaskID });
                _newJobTaskPhaseCodeReturn(scope, jobTask, dialog);
                for(var pc=0; pc<jobTask.JobTaskPhaseCode.length; ++pc){
                    var jtPhase = jobTask.JobTaskPhaseCode[pc];
                    var taskPC = task.PhaseCode.find(function(tpc){ return tpc.ID == jtPhase.PhaseCodeID });
                    if(!taskPC){
                        task.PhaseCode.push(_getPhaseCodeFromJTPC(jtPhase, scope.workweek));
                        changed = true;
                    }
                }
                var removedHours = [0,0,0];
                for(var pc=task.PhaseCode.length-1; pc>=0; --pc){
                    var taskPC = task.PhaseCode[pc];
                    var jtPhase = jobTask.JobTaskPhaseCode.find(function(jtpc){ return jtpc.PhaseCodeID == taskPC.ID });
                    if(!jtPhase || !jtPhase.ActiveFlag){
                        for(var i=0; i<_laborTypes.length; ++i){
                            if(task.PhaseCode[pc][_laborTypes[i]+"Hours"]){
                                var previous = task.PhaseCode[pc][_laborTypes[i]+"Hours"];
                                task.PhaseCode[pc][_laborTypes[i]+"Hours"] = 0;
                                _changePhaseHours(task.PhaseCode[pc], task, _laborTypes[i], previous);
                            }
                        }
                        task.PhaseCode.splice(pc, 1);
                        changed = true;
                    }else{
                        changed = changed||(!taskPC.active)
                        taskPC.active = true;
                    }
                }
                _sortPhases(task, 'PhaseCode');
            }
            return changed;
        }
        var _defaultNewTask = function(job, workweek){
            var phaseCodes = job.contractPhaseCodes.reduce(function(arr, pc){
                if(pc.DefaultFlag){
                    arr.push(_phaseCodeToJobTaskPhaseCode(pc, workweek));
                }
                return arr;
            }, []);
            var returnValue = {
                ActiveFlag: true,
                JobTaskPhaseCode: phaseCodes
            }
            _sortPhases(returnValue, 'JobTaskPhaseCode');
            return returnValue;
        }
        var _getTaskGroups = function(workweek, job, mode){
            var types = {
                "Active Tasks": {tasks:[], sort: 1, limitTo: 10, label: "Active", icon:'ion-arrow-graph-up-right'},
                "Workgroup Tasks": {tasks:[], sort: 2, limitTo: 10, label: "Workgroup", icon:'ion-settings'},
                "Complete Tasks": {tasks:[], sort: 4, limitTo: 10, label: "Complete", icon: 'ion-flag'}

            }, jobTasks = job.Job?job.Job.JobTask:job.JobTask;
            for(var t=0; t<jobTasks.length; ++t){
                jobTasks[t].JobTaskPhaseCode = Utils.enforceArray(jobTasks[t].JobTaskPhaseCode);
                jobTasks[t].showPhaseCodes = false;
                jobTasks[t].selected = jobTasks[t].selected2 = false;
                jobTasks[t].canComplete = (!jobTasks[t].UncompletableFlag)&&jobTasks[t].AllowFieldCompletionFlag;
                if(mode != 'CrewMember' && !jobTasks[t].PhaseCodeSectionID){
                    jobTasks[t].showPhaseCodes = (job.contractPhaseCodes && job.contractPhaseCodes.length > 1 && !jobTasks[t].LockPhases);
                    if(jobTasks[t].CompleteFlag){
                        types["Complete Tasks"].tasks.push(jobTasks[t]);
                        types["Complete Tasks"].showPhaseCodes = types["Complete Tasks"].showPhaseCodes||jobTasks[t].showPhaseCodes;
                        types["Complete Tasks"].canComplete = types["Complete Tasks"].canComplete||jobTasks[t].canComplete;
                    }else{
                        if(jobTasks[t].SystemCode != 'UN' || job.hasEarnedHours){
                            if(['UN','TEC','TEO'].includes(jobTasks[t].SystemCode)) jobTasks[t].showPhaseCodes = false;
                            types["Active Tasks"].tasks.push(jobTasks[t]);
                            types["Active Tasks"].showPhaseCodes = types["Active Tasks"].showPhaseCodes||jobTasks[t].showPhaseCodes;
                            types["Active Tasks"].canComplete = types["Active Tasks"].canComplete||jobTasks[t].canComplete;
                        }
                    }
                }else{
                    if(jobTasks[t].TypeCode != 'OT' && (mode != 'CrewMember' || jobTasks[t].TypeCode == 'NP')){
                        jobTasks[t].showPhaseCodes = (workweek.wgPhaseCodes && workweek.wgPhaseCodes.length > 1);
                        types["Workgroup Tasks"].tasks.push(jobTasks[t]);
                        types["Workgroup Tasks"].showPhaseCodes = types["Workgroup Tasks"].showPhaseCodes||jobTasks[t].showPhaseCodes;
                        types["Workgroup Tasks"].canComplete = types["Workgroup Tasks"].canComplete||jobTasks[t].canComplete;
                    }
                }
            }
            if(mode != 'CrewMember' && !_isOverheadJob(job,workweek)){
                //types["New Job Task"] = {sort: 3, label: "New Job Task"};
            }
            return Object.values(types).sort(function(a,b){ return a.sort - b.sort });
        }
        var _resetPhaseCode = function(pc, initial){
            if(initial === true){
                pc.EarnedHours = 0;
            }
            pc.STHours = 0;
            pc.OTHours = 0;
            pc.DTHours = 0;
            pc.TotalHours = 0;
        }
        var _getPhaseCodeFromJTPC = function(jtPhase, workweek){
            var override = (jtPhase.OverridePhaseCodeID > 0);
            var phase = {
                active: true,
                JobDayCrewTaskPhaseCodeID: _getTempID(workweek),
                CodeNumber: override ? jtPhase.OverridePhaseCodeCodeNumber : jtPhase.PhaseCodeCodeNumber,
                Description: override ? jtPhase.OverridePhaseCodeDescription : jtPhase.PhaseCodeDescription,
                DisplayNumber: override ? jtPhase.OverridePhaseCodeDisplayNumber : jtPhase.PhaseCodeDisplayNumber,
                ID: override ? jtPhase.OverridePhaseCodeID : jtPhase.PhaseCodeID,
                IncludeInOTCalcFlag: override ? jtPhase.OverridePhaseCoIncludeInOTCalcFlag : jtPhase.PhaseCodeIncludeInOTCalcFlag,
                PerDiemEligibleFlag: override ? jtPhase.OverridePhaseCoPerDiemEligibleFlag : jtPhase.PhaseCodePerDiemEligibleFlag,
                OverrideNumber: override ? jtPhase.OverridePhaseCodeOverrideNumber : jtPhase.PhaseCodeOverrideNumber,
                OverrideTypeCode: override ? jtPhase.OverridePhaseCodeOverrideTypeCode : jtPhase.PhaseCodeOverrideTypeCode,
                PhaseCodeSectionID: override ? jtPhase.OverridePhaseCodPhaseCodeSectionID : jtPhase.PhaseCodePhaseCodeSectionID,
                SectionNumber: override ? jtPhase.PhaseCodeSectionSectionNumber : jtPhase.PhaseCodeSectionSectionNumber2,
                SortOrder: override ? jtPhase.OverridePhaseCodeSortOrder : jtPhase.PhaseCodeSortOrder
            }
            _resetPhaseCode(phase, true);
            if(typeof jtPhase.earned != 'undefined'){
                phase.EarnedHours = jtPhase.earned;
            }
            return phase;
        }
        var _getMemberPhaseCodeFromJTPC = function(jtPhase, workweek){
            var override = (jtPhase.OverridePhaseCodeID > 0);
            var phase = {
                active: true,
                ID: _getTempID(workweek),
                PhaseCodeID: override ? jtPhase.OverridePhaseCodeID : jtPhase.PhaseCodeID,
                PhaseCodeCodeNumber: override ? jtPhase.OverridePhaseCodeCodeNumber : jtPhase.PhaseCodeCodeNumber,
                PhaseCodeDescription: override ? jtPhase.OverridePhaseCodeDescription : jtPhase.PhaseCodeDescription,
                PhaseCodeDisplayNumber: override ? jtPhase.OverridePhaseCodeDisplayNumber : jtPhase.PhaseCodeDisplayNumber,
                PhaseCodeIncludeInOTCalcFlag: override ? jtPhase.OverridePhaseCoIncludeInOTCalcFlag : jtPhase.PhaseCodeIncludeInOTCalcFlag,
                PhaseCodePerDiemEligibleFlag: override ? jtPhase.OverridePhaseCoPerDiemEligibleFlag : jtPhase.PhaseCodePerDiemEligibleFlag,
                PhaseCodeOverrideNumber: override ? jtPhase.OverridePhaseCodeOverrideNumber : jtPhase.PhaseCodeOverrideNumber,
                PhaseCodeOverrideTypeCode: override ? jtPhase.OverridePhaseCodeOverrideTypeCode : jtPhase.PhaseCodeOverrideTypeCode,
                PhaseCodePhaseCodeSectionID: override ? jtPhase.OverridePhaseCodPhaseCodeSectionID : jtPhase.PhaseCodePhaseCodeSectionID,
                PhaseCodeSectionSectionNumber: override ? jtPhase.PhaseCodeSectionSectionNumber : jtPhase.PhaseCodeSectionSectionNumber2,
                PhaseCodeSortOrder: override ? jtPhase.OverridePhaseCodeSortOrder : jtPhase.PhaseCodeSortOrder
            }
            _resetPhaseCode(phase);
            return phase;
        }
        var _checkCompletedForPhaseCode = function(job, phaseId, locationId){
            if(job.cwPhaseCodes && job.cwPhaseCodes[phaseId]){
                if(!locationId || typeof job.cwPhaseCodes[phaseId].locations[locationId] != 'undefined'){
                    return true;
                }
            }
            return false;
        }
        var _getPhaseCodesFromJobTask = function(jobTask, job, workweek, fn, locationId){
            var phases = [];
            jobTask.JobTaskPhaseCode = Utils.enforceArray(jobTask.JobTaskPhaseCode);
            if(jobTask.SystemCode == 'UN'){ // Do pre-check for units task
                var defaultPC = jobTask.JobTaskPhaseCode.find(function(pc){ return pc.DefaultForTimeEntry })
                for(var i=0; i<jobTask.JobTaskPhaseCode.length; ++i){
                    var jtPhase = jobTask.JobTaskPhaseCode[i];
                    //if(!jtPhase.ActiveFlag) continue;
                    if(defaultPC && !job.hasEarnedHours){
                        if(!jtPhase.DefaultForTimeEntry) continue;
                    }else{
                        if(!_checkCompletedForPhaseCode(job, (jtPhase.OverridePhaseCodeID||jtPhase.PhaseCodeID), locationId)) continue;
                    }
                    phases.push(fn(jtPhase, workweek));
                }
            }
            if(!phases.length){
                for(var i=0; i<jobTask.JobTaskPhaseCode.length; ++i){
                    var jtPhase = jobTask.JobTaskPhaseCode[i];
                    if(!jtPhase.ActiveFlag || jtPhase.Disabled) continue;
                    phases.push(fn(jtPhase, workweek));
                } 
            }
            return phases;
        }
        var _newTask = function(workweek, jobTask, job, locationId){
            var task = {
                ID: _getTempID(workweek),
                active: true,
                JobTaskID: jobTask.ID,
                TaskNumber: jobTask.TaskNumber,
                JobTaskDescription: jobTask.Description,
                JobTaskPhaseCodeSectionID: jobTask.PhaseCodeSectionID,
                JobTaskSystemCode: jobTask.SystemCode,
                JobTaskTypeCode: jobTask.TypeCode,
                JobTaskNonCompensatedFlag: jobTask.NonCompensatedFlag,
                PhaseCode: _getPhaseCodesFromJobTask(jobTask, job, workweek, _getPhaseCodeFromJTPC, locationId)
            }
            _setTaskPerDiemEligible(task, jobTask);
            if(locationId && workweek.jobLocations && workweek.jobLocations[job.JobCrewJobID]){
                var location = workweek.jobLocations[job.JobCrewJobID].find(function(loc){ return loc.ID == locationId });
                if(location){
                    task.LocationID = location.ID;
                    task.LocationNumber = location.NumberChar;
                }
            }else if(_isLocationTask(job, task)){
                task.canEditLocation = true;
            }
            return task;
        }
        var _resetTask = function(task, job){
            var tasks = _activeTasks(job);
            task.Duration = 0;
            task.STHours = 0;
            task.OTHours = 0;
            task.DTHours = 0;
            task.StartTime = new Date(tasks.length?tasks[tasks.length-1].EndTime.getTime() : job.StartTime.getTime());
            task.EndTime = new Date(job.EndTime.getTime());
            task.PhaseCode.forEach(_resetPhaseCode);
            task.TaskComment = '';
        }
        
        
        var _addTaskToJob = function(job, jobTask, isNewTask, workweek, taskMode, skipTotal, locationId, parseMode){
            if(taskMode == 'CrewMember'){
                return _addMemberTaskToCurrentMember(job, jobTask, workweek);
            }
            var tasks = _activeTasks(job);
            if(isNewTask){
                if(jobTask.TaskNumber.trim().toLowerCase() == 'units'){
                    _showHTMLMessage("<b>Units</b> is a reserved task name. Please choose another.", "Error");
                    return false;
                }
                jobTask.TypeCode = 'PR';
                jobTask.AllowFieldCompletionFlag = true;
                jobTask.ID = _getTempID(workweek);
                job.JobTask.push(jobTask);
            }
            job.Task = Utils.enforceArray(job.Task);
            var task = _newTask(workweek, jobTask, job, locationId);
            _resetTask(task, job);
            _updateTaskPhoto(task, workweek);
            task.OrderNumber = job.Task.length + 1;
            job.Task.push(task);
            if((!tasks.length&&!parseMode) || parseMode=='last'){
                _parseOutTaskTimes(workweek);
            }
            if(!skipTotal){
                _totalDay(workweek);
            }
            return task;
        }

        var _newMemberTask = function(workweek, jobTask, job){
            var memberTask = {
                ID: _getTempID(workweek),
                active: true,
                STHours: 0,
                OTHours: 0,
                DTHours: 0,
                Duration: 0
            }
            if(jobTask.JobTaskID){
                //jobTask is actually a task, we need to create a member task to track time info
                memberTask.StartTime = new Date(jobTask.StartTime.getTime());
                memberTask.EndTime = new Date(jobTask.EndTime.getTime());
                memberTask.TaskID = jobTask.ID;
            }else{
                // jobTask is a job task, manually adding a member task
                memberTask.PhaseCodeSectionID = jobTask.PhaseCodeSectionID;
                memberTask.PhaseCodeSectionDescription = jobTask.PhaseCodeSectionDescription;
                memberTask.PhaseCodeSectionOrgDepartmentID = jobTask.PhaseCodeSectionOrgDepartmentID;
                memberTask.PhaseCodeSectionOrganizationID = jobTask.PhaseCodeSectionOrganizationID;
                memberTask.PhaseCodeSectionSectionNumber = jobTask.PhaseCodeSectionSectionNumber;
                memberTask.MemberTaskPhaseCode = _getPhaseCodesFromJobTask(jobTask, job, workweek, _getMemberPhaseCodeFromJTPC);
                _setTaskPerDiemEligible(memberTask, null, true);
            }
            return memberTask;
        }

        var _addMemberTaskToCurrentMember = function(job, jobTask, workweek){
            var day = workweek.WorkDay[workweek.currentDay];
            var member = day.members[day.currentMember], jdcm = job.Member;

            jdcm.MemberTask = Utils.enforceArray(jdcm.MemberTask);
            var memberTask = _newMemberTask(workweek, jobTask, job.Job);
            jdcm.MemberTask.push(memberTask);
            var task = {
                STHours: 0,
                OTHours: 0,
                DTHours: 0,
                TotalHours: 0,
                Task: memberTask,
                JobTask: jobTask,
                isMember: true,
                PhaseCodes: []
            }
            for(var p=0; p<memberTask.MemberTaskPhaseCode.length; ++p){
                var pc = memberTask.MemberTaskPhaseCode[p];
                if(pc.active){
                    var memberPhaseCode = {
                        PhaseCode: pc,
                        STHours: pc.STHours,
                        OTHours: pc.OTHours,
                        DTHours: pc.DTHours,
                        TotalHours: (pc.STHours+pc.OTHours+pc.DTHours)
                    }
                    task.PhaseCodes.push(memberPhaseCode);
                }
            }
            job.Tasks.push(task);
            _calcShowTaskRows(workweek);
            _totalMemberTime(workweek, member);
            return task;
        }

        var _recalculateTaskPercentages = function(locationId, data){
            var total = 0, keys = Object.keys(data);
            for(var i=0; i<keys.length; ++i){
                var id = keys[i];
                if(id == locationId){
                    data[id] = 0;
                }else{
                    total += data[id];
                }
            }
            for(var i=0; i<keys.length; ++i){
                var id = keys[i];
                if(id != locationId){
                    if(total){
                        data[id] /= total;
                    }else{
                        data[id] = 1/keys.length;
                    }
                }
            }
        }
        var _allocateLocationTasks = function(job, workweek){
            //make the task hours allocate among the earned hours distribution
            var tasks = _activeTasks(job), locations = {}, total = 0;
            var locationEarned = Object.keys(job.cwPhaseCodes).reduce(function(locations, phase){
                var locationIDs = Object.keys(job.cwPhaseCodes[phase].locations);
                for(var l=0; l<locationIDs.length; ++l){
                    if(!locations[locationIDs[l]]) locations[locationIDs[l]] = 0;
                    locations[locationIDs[l]] += job.cwPhaseCodes[phase].locations[locationIDs[l]];
                    total += job.cwPhaseCodes[phase].locations[locationIDs[l]];
                }
                return locations;
            },{});
            for(var locationID in locationEarned){
                locationEarned[locationID] /= total;
            }
            var locationEarnedAmounts = Utils.sortObj(locationEarned, 'a');
            var addAmount = 0;
            for(var i=0; i<locationEarnedAmounts.length; ++i){
                var hours = (job.GrandTotalHours*locationEarnedAmounts[i][1])
                var value = hours + addAmount;
                var rounded = Math.max(0,Math.round(value*4)/4);
                var diff = hours-rounded;
                if(i < locationEarnedAmounts.length-1){
                    addAmount += diff/(locationEarnedAmounts.length-i-1)
                }
                locationEarned[locationEarnedAmounts[i][0]] = rounded;
            }
            var jobs = _activeJobs(workweek);
            for(var t=0; t<tasks.length; ++t){
                if(tasks[t].LocationID && locationEarned[tasks[t].LocationID]){
                    var taskHours = tasks[t].Duration, jobIndex = jobs.findIndex(function(jb){ return jb.JobCrewJobID == job.JobCrewJobID });
                    tasks[t].Duration = locationEarned[tasks[t].LocationID];
                    _updateTaskHoursEntry(workweek, job, t, jobIndex, taskHours);
                    delete locationEarned[tasks[t].LocationID];
                }
            }
        }

        var _jobEarnedHours = function(job){
            if(!job.cwPhaseCodes) return false;
            var ids = Object.keys(job.cwPhaseCodes);
            var total = 0;
            for(var i=0; i<ids.length; ++i){
                total += job.cwPhaseCodes[ids[i]].earned;
            }
            return total;
        }
        var _calcMeals = function(workweek){
            if(!_currentDay(workweek).hasMeals) return;
            var jobs = _activeJobs(workweek);
            for(var j=0; j<jobs.length; ++j){
                if(jobs[j].CanMeals){
                    jobs[j].calcMeals = Math.floor(jobs[j].TotalHours/(jobs[j].HrsMeal||5)); 
                }
            }
        }
        var _showJobTaskLevelsDay = function(workweek){
            var jobs = _activeJobs(workweek);
            for(var j=0; j<jobs.length; ++j){
                _showJobTaskLevels(jobs[j], workweek);
            }
            _calcShowTaskRows(workweek);
        }
        var _showJobTaskLevels = function(job, workweek){
            var dayJob = _currentDay(workweek).Job.find(function(j){ return j.ID == job.JobCrewJobID })
            if(dayJob){
                if(['TASK','PHASE'].includes(dayJob.DefaultTimeEntryDisplayCode)){
                    job.showTasks = true;
                    workweek.WorkDay[workweek.currentDay].isShowingTasks = true;
                    if(dayJob.DefaultTimeEntryDisplayCode == 'PHASE'){
                        workweek.WorkDay[workweek.currentDay].isShowingPhases = true;
                        for(var t=0; t<job.Task.length; ++t){
                            job.Task[t].showPhases = true;
                        }
                    }
                }
            }
        }
         var _hasDefaultPhase = function(jobTask){
            jobTask.JobTaskPhaseCode = Utils.enforceArray(jobTask.JobTaskPhaseCode);
            return !!jobTask.JobTaskPhaseCode.find(function(pc){ return pc.DefaultForTimeEntry })
        }
        var _addDefaultTask = function(job, workweek){
            var task,
                defaultTask = job.JobTask.find(function(jt){ return jt.DefaultForEndDay; }),
                unitsTask = job.UseDesignFlag?job.JobTask.find(function(jt){ return jt.SystemCode == 'UN'; }):null;
            if(unitsTask||defaultTask){
                task = defaultTask;
                if(unitsTask && (!job.BypassCompleteWorkFlag||_hasDefaultPhase(unitsTask)) && (_isLocationJob(job) || _jobEarnedHours(job))){
                    task = unitsTask;
                }
                if(!task) return;
                if(_isLocationJob(job)){
                    _removeTask(workweek, job, job.Task)
                }
                if(_doLocationTasks(job)){
                    var locationList = job.cwLocations.filter(function(lid){ return !job.Task.find(function(t){ return t.active&&t.LocationID==lid }) });
                    for(var l=0; l<locationList.length; ++l){
                        var newTask = _addTaskToJob(job, task, false, workweek, '', true, job.cwLocations[l], l==(locationList.length-1)?'last':'location');
                    }
                    _allocateLocationTasks(job, workweek);
                }else{
                    _addTaskToJob(job, task, false, workweek, '', true);
                }
                _showJobTaskLevels(job, workweek);
            }
        }

        var _deleteTask = function(workweek, job, task){
            task.active = false;
            var nextActive = null, foundMe = false, after = false;
            for(var i=0; i<job.Task.length; ++i){
                if(job.Task[i].active){
                    nextActive = job.Task[i];
                    if(foundMe){
                        after = true;
                        break;
                    }
                }
                if(job.Task[i].ID == task.ID){
                    foundMe = true;
                }
            }
            if(nextActive){
                if(after){
                    nextActive.StartTime = new Date(task.StartTime.getTime());
                }else{
                    nextActive.EndTime = new Date(task.EndTime.getTime());
                }
                nextActive.Duration = (nextActive.EndTime.getTime()-nextActive.StartTime.getTime())/1000/60/60;
            }else if(job.RatioOverridenFlag){
                job.RatioOverridenFlag = false;
            }
        }
        var _removeTask = function(workweek, job, tasks){
            tasks = Utils.enforceArray(tasks)
            for(var t=0; t<tasks.length; ++t){
                if(tasks[t].active)_deleteTask(workweek, job, tasks[t])
            }
            _parseOutTaskTimes(workweek);
            _calcShowTaskRows(workweek);
            _totalDay(workweek);
        }
        var _removeMemberTask = function(workweek, job, task){
            var day = workweek.WorkDay[workweek.currentDay];
            var member = day.members[day.currentMember];

            task.Task.active = false;
            for(var t=0; t<job.Tasks.length; ++t){
                if(job.Tasks[t].isMember && job.Tasks[t].ID == task.ID){
                    job.Tasks.splice(t, 1);
                    break;
                }
            }
            _calcShowTaskRows(workweek);
            _totalMemberTime(workweek, member);
        }
        var _isLocationJob = function(job){
            return job.CaptureTimeAtCode=='TLO'||job.CaptureTimeAtCode=='LPC';
        }
        var _isLocationTask = function(job, task){
            return _isLocationJob(job) && !task.JobTaskPhaseCodeSectionID && !task.PhaseCodeSectionID && task.JobTaskSystemCode == 'UN';
        }
        var _doLocationTasks = function(job){
            return (_isLocationJob(job)&&job.cwLocations&&job.cwLocations.length);
        }
        var _clearMemberAndEquipTime = function(jobs){
            jobs = Utils.enforceArray(jobs);
            for(var j=0; j<jobs.length; ++j){
                var job = jobs[j];
                if(job.JobDayCrewMember){
                    for(var m=0; m<job.JobDayCrewMember.length; ++m){
                        job.JobDayCrewMember[m].STHours = 0;
                        job.JobDayCrewMember[m].OTHours = 0;
                        job.JobDayCrewMember[m].DTHours = 0;
                        job.JobDayCrewMember[m].TotalHours = 0;
                    }
                }
                if(job.JobDayCrewEquipment){
                    for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                        job.JobDayCrewEquipment[e].TotalHours = 0;
                    }
                }
            }
        }
        var _initCrewTime = function(workweek, scope){
            if(workweek.launchLocationPercent){
                _openLocationPercents(scope);
                return;
            }

            var day = workweek.WorkDay[workweek.currentDay];
            var jobs = _activeJobs(workweek);
            var problemJobs = [];
            for(var j=0; j<jobs.length; ++j){
                var job = jobs[j];
                if(!_isOverheadJob(job,workweek) && !job.ContractWorkingTime && !job.CWWorkingTime){
                    problemJobs.push(job);
                    continue;
                }
                job.Task = Utils.enforceArray(job.Task);
                if(scope.editable){
                    for(var i=0; i<job.Task.length; ++i){ job.Task[i].active = false; }
                }
                if(!job.Task.find(function(t){ return t.active }) || _doLocationTasks(job)){
                    job.showTasks = false;
                    if(scope.editable){
                        job.RatioOverridenFlag = false;
                        _addDefaultTask(job, workweek);
                    }
                }else{
                    _showJobTaskLevels(job, workweek);
                }
            }
            if(scope.editable)_parseOutTaskTimes(workweek);
            _calcShowTaskRows(workweek);
            _totalDay(workweek);

            if(scope.editable&&problemJobs.length){
                _showHTMLMessage("There are no working times defined for these jobs: <ul><li>"+problemJobs.map(function(jb){
                    return jb.WorkgroupJobNumber||jb.NumberChar;
                }).join("</li><li>")+"</li></ul> Time allocation may not work as expected.", "Error");
            }
        }

        var _dateFromHourAndMinute = function(date, hours, minutes){
            var d = new Date(typeof date =='string'?date:date.getTime());
            d.setHours(parseInt(hours));
            d.setMinutes(parseInt(minutes));
            return d;
        }
        var _hourDifference = function(dateStart, dateEnd){
            var diff = (dateEnd.getTime() - dateStart.getTime()) / 3600000;
            return Math.round(diff*100)/100;
        }

        var _recalculatePhasePercentages = function(phaseID, data){
            var total = 0, keys = Object.keys(data.phases);
            for(var i=0; i<keys.length; ++i){
                var id = keys[i];
                if(id == phaseID){
                    data.phases[id] = 0;
                }else{
                    total += data.phases[id];
                }
            }
            for(var i=0; i<keys.length; ++i){
                var id = keys[i];
                if(id != phaseID){
                    if(total){
                        data.phases[id] /= total;
                    }else{
                        data.phases[id] = 1/keys.length;
                    }
                }
            }
        }
        var _total = function(item, prefix){
            prefix = prefix||"";
            return parseFloat(item[prefix+"STHours"])+parseFloat(item[prefix+"OTHours"])+parseFloat(item[prefix+"DTHours"]);
        }
        var _totalWeek = function(workweek){
            var totals = [0,0,0,0,0];
            workweek.TotalHours = 0;
            _resetBuckets(workweek);
            for(var d=0; d<workweek.WorkDay.length; ++d){
                totals[0] += parseFloat(workweek.WorkDay[d].STHours)||0;
                totals[1] += parseFloat(workweek.WorkDay[d].OTHours)||0;
                totals[2] += parseFloat(workweek.WorkDay[d].DTHours)||0;
                totals[3] += parseFloat(workweek.WorkDay[d].PerDiemTotal)||0;
                totals[4] += parseInt(workweek.WorkDay[d].MealsTotal)||0;
                workweek.TotalHours += parseFloat(workweek.WorkDay[d].WeekdayHours)||0;
                _sumDayBuckets(workweek, d);
            }
            workweek.STHours = totals[0];
            workweek.OTHours = totals[1];
            workweek.DTHours = totals[2];
            workweek.PerDiemTotal = totals[3];
            workweek.MealsTotal = totals[4];
        }
        var _totalDay = function(workweek, dayIndex){
            if(typeof dayIndex == 'undefined') dayIndex = workweek.currentDay;
            var day = workweek.WorkDay[dayIndex];
            _resetBucketDay(workweek, dayIndex);
            var jobs = _activeJobs(workweek, day.WeekdayDate);
            var editable = _isDayEditable(day, workweek);
            var totals = [0,0,0,0,0,0], crewTotals = [0,0,0,0,0,0], useCrewTime = false, hasMemberHours = false, foremanInactive = false;
            if(editable && day.currentStep < 3){
                useCrewTime = true;
            }
            for(var j=0; j<jobs.length; ++j){
                var bucket = _getJobBucket(jobs[j], workweek);
                var foremanHours = 0;
                crewTotals[0] += parseFloat(jobs[j].STHours)||0;
                crewTotals[1] += parseFloat(jobs[j].OTHours)||0;
                crewTotals[2] += parseFloat(jobs[j].DTHours)||0;
                crewTotals[3] += parseFloat(jobs[j].TotalHours)||0;
                crewTotals[5] += parseInt(jobs[j].Meals)||0;
                if(!useCrewTime){
                    crewTotals[5] = 0;
                    for(var m=0; m<jobs[j].JobDayCrewMember.length; ++m){
                        var member = jobs[j].JobDayCrewMember[m];
                        if(member.PersonID == workweek.PersonID){
                            if(!member.ActiveFlag){
                                foremanInactive = true;
                                continue;
                            }
                            foremanHours = parseFloat(member.TotalHours)||0;
                            totals[0] += parseFloat(member.STHours)||0;
                            totals[1] += parseFloat(member.OTHours)||0;
                            totals[2] += parseFloat(member.DTHours)||0;
                            totals[3] += foremanHours;
                            totals[4] += parseFloat(member.PerDiemAmount)||0;
                            totals[5] += parseInt(member.Meals)||0;
                            
                            if(bucket){
                                for(var te=0; te<member.TimeEntry.length; ++te){
                                    if(member.TimeEntry[te].TimeInHours){
                                        _calcTimeBucket(workweek, jobs[j], member.TimeEntry[te].PhaseCodeID, dayIndex, parseFloat(member.TimeEntry[te].TimeInHours)||0);
                                    }
                                }
                            }
                        }else{
                            if(parseFloat(member.STHours)||parseFloat(member.OTHours)||parseFloat(member.DTHours)){
                                hasMemberHours = true;
                            }
                        }
                    }
                }
            }
            if(useCrewTime){// || !totals[3]){
                totals = crewTotals;
            }else if(foremanInactive){
                if(hasMemberHours){
                    totals = crewTotals;
                }else{
                    totals = [0,0,0,0,0,0];
                }
            }
            day.STHours = totals[0];
            day.OTHours = totals[1];
            day.DTHours = totals[2];
            day.WeekdayHours = totals[3];
            day.PerDiemTotal = totals[4];
            day.MealsTotal = totals[5];
            _totalWeek(workweek);
        }
        var _getEarnedHours = function(job, task, workweek){
            var data = {
                phases: {},
                hasEarned: false
            }, total = 0, phases = _activePhases(task);
            if(_isLocationJob(job) && task.LocationID){
                for(var i=0; i<phases.length; ++i){
                    if(job.cwPhaseCodes[phases[i].ID] && job.cwPhaseCodes[phases[i].ID].locations[task.LocationID]){
                        total += job.cwPhaseCodes[phases[i].ID].locations[task.LocationID];
                        data.phases[phases[i].ID] = job.cwPhaseCodes[phases[i].ID].locations[task.LocationID];
                    }else{
                        data.phases[phases[i].ID] = 0;
                    }
                }
            }else{
                for(var i=0; i<phases.length; ++i){
                    total += phases[i].EarnedHours;
                    data.phases[phases[i].ID] = phases[i].EarnedHours;
                }
            }
            if(total) data.hasEarned = true;
            for(var i=0; i<phases.length; ++i){
                if(data.hasEarned){
                    data.phases[phases[i].ID] /= total;

                }else{
                    data.phases[phases[i].ID] = 1/phases.length;
                }
            }
            return data;
        }
        var _parseTimeToPhaseCodes = function(job, task, workweek){
            var normal = true, phases = _activePhases(task);
            if(task.JobTaskSystemCode == 'UN'){
                normal = false;
                //Units earned hour allocation
                var earned = _getEarnedHours(job, task, workweek);
                var existing = [task.STHours, task.OTHours, task.DTHours];
                var remaining = existing;
                for(var i=0; i<phases.length; ++i){
                    var percentage = earned.phases[phases[i].ID];
                    var alloc = [
                        Math.round(existing[0]*percentage * 4) / 4,
                        Math.round(existing[1]*percentage * 4) / 4,
                        Math.round(existing[2]*percentage * 4) / 4
                    ];
                    phases[i].STHours = Math.min(alloc[0], remaining[0]);
                    phases[i].OTHours = Math.min(alloc[1], remaining[1]);
                    phases[i].DTHours = Math.min(alloc[2], remaining[2]);
                    phases[i].TotalHours = _total(phases[i]);
                    remaining[0] -= phases[i].STHours;
                    remaining[1] -= phases[i].OTHours;
                    remaining[2] -= phases[i].DTHours;
                    delete phases[i].EntryOrderST;
                    delete phases[i].EntryOrderOT;
                    delete phases[i].EntryOrderDT;
                    _recalculatePhasePercentages(phases[i].ID, earned);
                }
                return;
            }
           //Normal allocation, just drop on first phase code
           for(var i=0; i<phases.length; ++i){
                var amounts = [0,0,0];
                if(i == 0){
                    amounts = [task.STHours, task.OTHours, task.DTHours];
                }
                phases[i].STHours = amounts[0];
                phases[i].OTHours = amounts[1];
                phases[i].DTHours = amounts[2];
                phases[i].TotalHours = _total(phases[i]);
                delete phases[i].EntryOrderST;
                delete phases[i].EntryOrderOT;
                delete phases[i].EntryOrderDT;
            }
        }
        var _parseMemberTimeToPhaseCodes = function(job, task, workweek){
            var normal = true, phases = task.PhaseCodes
            if(task.Task.JobTaskSystemCode == 'UN'){
                normal = false;
                //Units earned hour allocation
                var earned = _getEarnedHours(job.Job, task.Task, workweek);
                var existing = [task.STHours, task.OTHours, task.DTHours];
                var remaining = existing;
                for(var i=0; i<phases.length; ++i){
                    var percentage = earned.phases[phases[i].PhaseCode.ID];
                    var alloc = [
                        Math.round(existing[0]*percentage * 4) / 4,
                        Math.round(existing[1]*percentage * 4) / 4,
                        Math.round(existing[2]*percentage * 4) / 4
                    ];
                    phases[i].STHours = Math.min(alloc[0], remaining[0]);
                    phases[i].OTHours = Math.min(alloc[1], remaining[1]);
                    phases[i].DTHours = Math.min(alloc[2], remaining[2]);
                    phases[i].TotalHours = _total(phases[i]);
                    _updateMemberPhaseCodeTimeEntry(job.Member, phases[i], task.Task, ['ST','OT','DT'], workweek, task.isMember);
                    remaining[0] -= phases[i].STHours;
                    remaining[1] -= phases[i].OTHours;
                    remaining[2] -= phases[i].DTHours;
                    delete phases[i].EntryOrderST;
                    delete phases[i].EntryOrderOT;
                    delete phases[i].EntryOrderDT;
                    _recalculatePhasePercentages(phases[i].ID, earned);
                }
                return;
            }
           //Normal allocation, just drop on first phase code
           for(var i=0; i<phases.length; ++i){
                var amounts = [0,0,0];
                if(i == 0){
                    amounts = [task.STHours, task.OTHours, task.DTHours];
                }
                phases[i].STHours = amounts[0];
                phases[i].OTHours = amounts[1];
                phases[i].DTHours = amounts[2];
                phases[i].TotalHours = _total(phases[i]);
                _updateMemberPhaseCodeTimeEntry(job.Member, phases[i], task.Task, ['ST','OT','DT'], workweek, task.isMember);
                delete phases[i].EntryOrderST;
                delete phases[i].EntryOrderOT;
                delete phases[i].EntryOrderDT;
            }
        }
        var _changePhaseHours = function(pc, task, which, previousValue){
            var originalPhases = _activePhases(task);
            var phases = originalPhases.map(function(item){ return item; }), top = 0;
            for(var i=0; i<originalPhases.length; ++i){ 
                var phase = originalPhases[i];
                phase.index = i;
                if(!phase['EntryOrder'+which]) phase['EntryOrder'+which] = 0;
                if(phase['EntryOrder'+which] > top) top = phase['EntryOrder'+which];
            }
            pc['EntryOrder'+which] = top + 1;
            phases.sort(function(a,b){  return (a['EntryOrder'+which]==b['EntryOrder'+which])?(a.index-b.index):(a['EntryOrder'+which] - b['EntryOrder'+which]); });
            var total = parseFloat(task[which+'Hours']);
            var myHours = Math.max(0,Math.min(total, pc[which+'Hours']));
            pc[which+'Hours'] = myHours;
            pc.TotalHours = _total(pc);
            var remaining = previousValue-myHours;
            for(var i=0; i<phases.length, remaining != 0; ++i){
                var hours = parseFloat(phases[i][which+'Hours']);
                var amount = Math.max(hours+remaining, 0);
                phases[i][which+'Hours'] = amount;
                phases[i].TotalHours = _total(phases[i]);
                remaining -= (amount-hours);
            }
        }
        var _getTimeWorkWeekRule = function(code){
            switch(code){
                case 'F10':
                    return {days: 4, hours: 10}
                case 'F8':
                    return {days: 5, hours: 8}
                case 'STORM':
                    return {days: 7, hours: 16}
            }
            return false;
        }
        var _parseOutTaskTime = function(job, task, hoursSoFar, workweek){
            var types = ['ST','OT','DT'], times = [0,0,0], views = ['CWWorkingTime','CWTimeOverrideDay']//job.TypeCode=="TE"?['ContractWorkingTime','ContractTimeOverrideDay']:['CWWorkingTime','CWTimeOverrideDay'];
            if(job.RatioOverridenFlag){
                //Allocation has been hard-defaulted manually by the user
                var hrs = 0, soFar = hoursSoFar.job;
                for(var i=0; i<types.length, hrs<task.Duration; i++){
                    var remaining = job[types[i]+"Hours"]-soFar;
                    times[i] = Math.min(task.Duration-hrs, Math.max(0, remaining));
                    soFar -= (job[types[i]+"Hours"]-times[i]);
                    hrs += times[i];
                }
            }else{
                //Do contract day/hour allocation rules
                var currentTime = new Date(task.StartTime.getTime());
                var i = 0;
                while(currentTime < task.EndTime && i < 100){
                    ++i;
                    var overrideCode = '', add = 0, overrideDay;
                    if(job[views[1]] && (overrideDay = job[views[1]].find(function(od){
                        return od.OverrideDate == Utils.date(currentTime, 'MM/dd/yyyy');
                    }))){
                        switch(overrideDay.OverrideTypeCode){
                            case 'ALLDT':
                                overrideCode = 'DT';
                            break;
                            case 'ALLOT':
                                overrideCode = 'OT';
                            break;
                            case 'SHIFT':
                                ++add;
                            break;
                        }
                    }
                    if(job[views[0]]){
                        for(var i=0; i<job[views[0]].length; ++i){
                            var workingTime = job[views[0]][i];
                            var start = _dateFromHourAndMinute(currentTime, workingTime.StartHourCode, workingTime.StartMinuteCode);
                            var end = _dateFromHourAndMinute(currentTime, workingTime.EndHourCode, workingTime.EndMinuteCode);
                            if(workingTime.EndHourCode == '0') end.setDate(end.getDate()+1);
                            if(currentTime <= end && task.EndTime >= start){
                                var code = workingTime.LaborTypeCode, addIndex = add;
                                if(overrideCode){
                                    code = overrideCode;
                                }
                                if((code == 'DT' || (code == 'OT' && add == 1)) && !job.ContractAllowDoubleTimeFlag){
                                    code = 'OT';
                                    addIndex = 0;
                                }
                                var index = Math.min(types.findIndex(function(t){ return t==code})+addIndex, types.length-1);
                                var newStart = new Date(Math.min(end.getTime(), task.EndTime.getTime()));
                                times[index] += _hourDifference(new Date(Math.max(start.getTime(), currentTime.getTime())), newStart);
                                currentTime = newStart;
                            }
                        } 
                    }else if(_isOverheadJob(job, workweek)){
                        //Overhead jobs
                        times[0] = _hourDifference(new Date(task.StartTime.getTime()), new Date(task.EndTime.getTime()));
                    }
                }
                //Crew working time rules
                if(workweek.Code){
                    var crewRules = _getTimeWorkWeekRule(workweek.Code);
                    //Don't need to check the day here...that gets picked up in the member STForOT rules
                    if(hoursSoFar.day.ST+times[0] > crewRules.hours){
                        var remaining = Math.max(0, crewRules.hours-hoursSoFar.day.ST);
                        times[1] += times[0]-remaining;
                        times[0] = remaining;
                    }
                }
            }
            task.STHours = times[0];
            task.OTHours = times[1];
            task.DTHours = times[2];
            task.Duration = _total(task);
            _parseTimeToPhaseCodes(job, task, workweek);
        }
        var _parseOutTaskTimes = function(workweek, startTask){
            var jobs = _activeJobs(workweek), totals = {day:{ST:0}}
            for(var j=0; j<jobs.length; ++j){
                totals.job = 0;
                totals.jobComp = 0;
                var job = jobs[j], times = [0,0,0], tasks = _activeTasks(jobs[j]);
                for(var t=0; t<tasks.length; ++t){
                    _parseOutTaskTime(job, tasks[t], totals, workweek);
                    var st = parseFloat(tasks[t].STHours), ot = parseFloat(tasks[t].OTHours), dt = parseFloat(tasks[t].DTHours);
                    times[0] += st;
                    times[1] += ot;
                    times[2] += dt;
                    delete tasks[t].EntryOrder;
                    totals.job += (st+ot+dt);
                    if(!tasks[t].JobTaskNonCompensatedFlag){
                        totals.jobComp += (st+ot+dt);
                    }
                    totals.day.ST += st;
                }
                job.STHours = times[0];
                job.OTHours = times[1];
                job.DTHours = times[2];
                job.TotalHours = _total(job);
                if(job.CanMeals){
                    job.Meals = job.calcMeals = Math.floor(totals.jobComp/(job.HrsMeal||5));
                }
            }
        }

        var _parseOutMemberTaskTime = function(job, task, hoursSoFar, workweek){
            var types = ['ST','OT','DT'], times = [0,0,0], views = /*job.TypeCode=="TE"?['ContractWorkingTime','ContractTimeOverrideDay']:*/['CWWorkingTime','CWTimeOverrideDay'];
            if(job.Member.RatioOverridenFlag){
                //Allocation has been hard-defaulted manually by the user
                var hrs = 0, soFar = hoursSoFar.job;
                for(var i=0; i<types.length, hrs<task.TotalHours; i++){
                    var remaining = job[types[i]+"Hours"]-soFar;
                    times[i] = Math.min(task.TotalHours-hrs, Math.max(0, remaining));
                    soFar -= (job[types[i]+"Hours"]-times[i]);
                    hrs += times[i];
                }
            }else{
                //Do contract day/hour allocation rules
                var currentTime = new Date(task.StartTime.getTime());
                var i = 0;
                while(currentTime < task.EndTime && i < 100){
                    ++i;
                    var overrideCode = '', add = 0, overrideDay;
                    if(job.Job[views[1]] && (overrideDay = job.Job[views[1]].find(function(od){
                        return od.OverrideDate == Utils.date(currentTime, 'MM/dd/yyyy');
                    }))){
                        switch(overrideDay.OverrideTypeCode){
                            case 'ALLDT':
                                overrideCode = 'DT';
                            break;
                            case 'ALLOT':
                                overrideCode = 'OT';
                            break;
                            case 'SHIFT':
                                ++add;
                            break;
                        }
                    }
                    console.log(job.Job[views[0]])
                    if(job.Job[views[0]]){
                        for(var i=0; i<job.Job[views[0]].length; ++i){
                            var workingTime = job.Job[views[0]][i];
                            var start = _dateFromHourAndMinute(currentTime, workingTime.StartHourCode, workingTime.StartMinuteCode);
                            var end = _dateFromHourAndMinute(currentTime, workingTime.EndHourCode, workingTime.EndMinuteCode);
                            if(workingTime.EndHourCode == '0') end.setDate(end.getDate()+1);
                            if(currentTime <= end && task.EndTime >= start){
                                var code = workingTime.LaborTypeCode, addIndex = add;
                                if(overrideCode){
                                    code = overrideCode;
                                }
                                if((code == 'DT' || (code == 'OT' && add == 1)) && !job.Job.ContractAllowDoubleTimeFlag){
                                    code = 'OT';
                                    addIndex = 0;
                                }
                                var index = Math.min(types.findIndex(function(t){ return t==code})+addIndex, types.length-1);
                                var newStart = new Date(Math.min(end.getTime(), task.EndTime.getTime()));
                                times[index] += _hourDifference(new Date(Math.max(start.getTime(), currentTime.getTime())), newStart);
                                currentTime = newStart;
                            }
                        } 
                    }else if(_isOverheadJob(job.Job, workweek)){
                        //Overhead jobs
                        times[0] = _hourDifference(new Date(task.StartTime.getTime()), new Date(task.EndTime.getTime()));
                    }
                }
                //Crew working time rules
                if(workweek.Code){
                    var crewRules = _getTimeWorkWeekRule(workweek.Code);
                    //Don't need to check the day here...that gets picked up in the member STForOT rules
                    if(hoursSoFar.day.ST+times[0] > crewRules.hours){
                        var remaining = Math.max(0, crewRules.hours-hoursSoFar.day.ST);
                        times[1] += times[0]-remaining;
                        times[0] = remaining;
                    }
                }
            }
            console.log(times)
            task.STHours = times[0];
            task.OTHours = times[1];
            task.DTHours = times[2];
            task.TotalHours = _total(task);
            _parseMemberTimeToPhaseCodes(job, task, workweek);
        }

        var _calcShowTaskRows = function(workweek){
            var day = workweek.WorkDay[workweek.currentDay], jobs = _activeJobs(workweek);
            var isShowingTasks = false, isShowingPhases = false;
            for(var j=0; j<jobs.length; ++j){
                var stats = {compensatedCount:0};
                var tasks = _activeTasks(jobs[j],stats);
                var job = jobs[j], jobRowspan = job.showTasks?tasks.length:1, memberJobRowspan = job.showTasks?tasks.length:1, equipJobRowspan = job.showTasks?stats.compensatedCount:1;
                isShowingTasks = isShowingTasks||job.showTasks;
                for(var t=0; t<tasks.length; ++t){
                    var task = tasks[t], taskRowspan = 1, phases = _activePhases(tasks[t]);
                    if(job.showTasks){
                        if(task.showPhases){
                            taskRowspan = phases.length;
                            jobRowspan += 1+taskRowspan;
                            memberJobRowspan += 1+taskRowspan;
                            if(!task.JobTaskNonCompensatedFlag){
                                equipJobRowspan += 1+taskRowspan;
                            }
                        }
                    }
                    isShowingPhases = isShowingPhases||(job.showTasks&&task.showPhases);
                    task.rowspan = taskRowspan;

                }
                if(day.members && day.members.length){
                    var memberJob = day.members[day.currentMember].Jobs.find(function(jb){ return jb.ID == job.JobCrewJobID});
                    if(memberJob && memberJob.Member.MemberTask){
                       memberJob.Member.MemberTask = Utils.enforceArray(memberJob.Member.MemberTask); 
                        for(var t=0; t<memberJob.Member.MemberTask.length; ++t){
                            var task = memberJob.Member.MemberTask[t];
                            if(task.active && !task.TaskID){
                                var taskRowspan = 1; var phases = _activePhases(task, 'MemberTaskPhaseCode');
                                if(job.showTasks){
                                    ++memberJobRowspan;
                                    if(task.showPhases){
                                        taskRowspan = phases.length;
                                        memberJobRowspan += 1+taskRowspan;
                                    }
                                }
                                isShowingPhases = job.showTasks&&(isShowingPhases||task.showPhases);
                                task.rowspan = taskRowspan;
                            }
                        }
                    }
                }
                job.rowspan = jobRowspan;
                job.memberRowspan = memberJobRowspan;
                job.equipRowspan = equipJobRowspan;
                job.hasMemberTask = job.JobTask&&(job.JobTask.find(function(jt){ return jt.PhaseCodeSectionID&&jt.TypeCode == 'NP' }) != null);
            }
            day.isShowingTasks = isShowingTasks;
            day.isShowingPhases = isShowingTasks&&isShowingPhases;
        }

        var _defaultJobDuration = function(workweek, scope){
            if(scope.editable && _isDayEditable(_currentDay(workweek), workweek)){
                var rule = _getTimeWorkWeekRule(workweek.Code);
                var jobs = _activeJobs(workweek);
                for(var j=0; j<jobs.length; ++j){
                    if(j==0){
                        jobs[j].GrandTotalHours = rule.hours;
                    }else{
                        jobs[j].GrandTotalHours = 0;
                    }
                    _updateJobHours(workweek, j);
                }
            }
        }

        var _calcShowUpSites = function(workweek){
            var day = _currentDay(workweek);
            delete day.showUpSites;
            var jobs = _activeJobs(workweek);
            var tempJob;
            var sites = jobs.reduce(function(sites, job){ 
                if((tempJob=workweek.Job.find(function(jb){
                    return jb.ID==job.JobCrewJobID
                })) && tempJob.USUS){
                    sites.push(tempJob);
                } 
                return sites; 
            }, []);
            if(sites.length){
               day.showUpSites = sites; 
            }
        }

        var _isCompleteWorkJob = function(job){
            return job.js == 'PR' && job.UseDesignFlag && (job.DesignLocationPercentCompleteFlag||job.LocationPercentCompleteFlag||!job.BypassCompleteWorkFlag);
        }
        var _calcIfNextIsCompleteWork = function(workweek){
            var day = _currentDay(workweek);
            day.nextIsCompleteWork = false;
            day.nextCanCompleteWork = false;
            var jobs = _activeJobs(workweek);
            var cwJobs = jobs.reduce(function(jobs, job){ if(_isCompleteWorkJob(job)){jobs.push(job.JobCrewJobID);} return jobs; }, []);
            if(cwJobs.length){
                day.nextCanCompleteWork = true;
            }
            if(jobs.find(function(j){ return !j.hasCompletedWork})){
                day.nextIsCompleteWork = day.nextCanCompleteWork;
            }
        }
        var _completeWork = function(scope, jobIds, forceCW, afterLoad){
            delete scope.workweek.forceCompleteWork;
            var oneJob = !!jobIds
            var locationPercentCompleteJobs = []
            jobIds = Utils.enforceArray(jobIds);
            var day = _currentDay(scope.workweek), jobs = _activeJobs(scope.workweek);
            var translateJob = function(job){
                return {
                    JobID: job.JobCrewJobID,
                    JobNumber: job.NumberChar,
                    WorkgroupJobNumber: job.WorkgroupJobNumber,
                    Description: job.Description
                }
            }
            if(!jobIds.length){
                if(day.nextIsCompleteWork || (forceCW&&day.nextCanCompleteWork)){
                    jobIds = jobs.reduce(function(jobs, job){ if(job.UseDesignFlag && ((job.LocationPercentCompleteFlag && job.CaptureTimeAtCode == 'LPC')||!job.BypassCompleteWorkFlag)){jobs.push(translateJob(job));} return jobs; }, []);
                }
            }else{
                jobIds = jobs.reduce(function(jobs, job){ if(jobIds.includes(job.JobCrewJobID)){jobs.push(translateJob(job));} return jobs; }, []);
            }
            for(var j=jobIds.length-1; j>=0; --j){
                var job = jobs.find(function(jb){ return jb.JobCrewJobID == jobIds[j].JobID && jb.LocationPercentCompleteFlag && jb.CaptureTimeAtCode == 'LPC'})
                if(job){
                    locationPercentCompleteJobs.push(jobIds.splice(j, 1)[0])
                }
            }
            if(locationPercentCompleteJobs.length && (oneJob || day.nextIsCompleteWork || (forceCW&&day.nextCanCompleteWork))){
                if(afterLoad || day.cwJobScreen){
                    scope.workweek.launchLocationPercent = true;
                }else{
                    _openLocationPercents(scope);
                }
                delete scope.workweek.completeJobIds
                if(jobIds.length){
                    scope.workweek.completeJobIds = jobIds
                }
                return false;
            }
            if(!jobIds.length){
                return false;
            }
            _forceSaveCurrentStep(scope);
            _changes(scope.workweek, true);
            _save(scope, 'fte/saveAndCompleteWork', {jobs:jobIds, date: Utils.date(scope.workweek.WorkDay[scope.workweek.currentDay].WeekdayDate,'MM/dd/yyyy')}, 'completeWorkSaveDone', 'Loading Complete Work...');
            return true;
        }
        var _showUpSites = function(scope){
            _changes(scope.workweek, true);
            _save(scope, 'fte/save', {}, 'showUpSitesSaveDone', 'Loading Show Up Sites...');
            return true;
        }
        var _openLocationPercents = function(scope, jobs, jobScreen){
            jobs = Utils.enforceArray(jobs);
            if(!jobs.length) jobs = _activeJobs(scope.workweek).filter(function(jb){ return jb.LocationPercentCompleteFlag });

            var data = [], cwJobIds = []
            for(var j=0; j<jobs.length; ++j){
                var job = Utils.pick(jobs[j], 'JobCrewJobID', 'WorkgroupJobNumber', 'NumberChar')
                if(!scope.workweek.jobLocations[job.JobCrewJobID]){
                    cwJobIds.push(job.JobCrewJobID)
                    continue
                }
                job.Location = Utils.copy(scope.workweek.jobLocations[job.JobCrewJobID]).reduce(function(locations, loc){
                    if(loc.Converted){
                        loc.PercentComplete = Math.round(loc.PercentComplete*10000)/100
                        loc.OriginalPercentComplete = loc.PercentComplete;
                        locations.push(loc);
                    }
                    return locations
                }, []);
                job.Location.sort(function(a,b){
                    if(a.Complete != b.Complete){
                        return a.Complete?1:-1;
                    }
                    return a.SortOrder - b.SortOrder 
                })
                data.push(job)
            }
            if(cwJobIds.length){
                scope.workweek.completeJobIds = (scope.workweek.completeJobIds||[]).concat(cwJobIds)
                if(!data.length){
                    var jobIds = scope.workweek.completeJobIds
                    delete scope.workweek.completeJobIds
                    _save(scope, 'fte/saveAndCompleteWork', {jobs:jobIds.join(","), date: Utils.date(scope.workweek.WorkDay[scope.workweek.currentDay].WeekdayDate,'MM/dd/yyyy')}, 'completeWorkSaveDone', 'Loading Complete Work...');
                    return
                }
            }
            scope.dialogs.locationPercent.open(data, jobScreen)
        }
        var _finishLocationPercents = function(scope, jobs){
            var afterLoad = scope.workweek.launchLocationPercent;
            scope.workweek.launchLocationPercent = false;
            var noCompletions = []
            for(var j=jobs.length-1; j>=0; --j){
                jobs[j].Location = jobs[j].Location.filter(function(loc){ return loc.PercentComplete != loc.OriginalPercentComplete });
                if(!jobs[j].Location.length){
                    noCompletions.push(jobs.splice(j, 1)[0])
                }
            }
            if(noCompletions.length){
                var activeJobs = _activeJobs(scope.workweek);
                for(var j=0; j<noCompletions.length; ++j){
                    var job = activeJobs.find(function(jb){ return jb.JobCrewJobID == noCompletions[j].JobCrewJobID})
                    var tasks = _activeTasks(job);
                    if(job && (!tasks.length || (tasks.length == 1 && tasks[0].JobTaskSystemCode == 'UN' && !tasks[0].LocationID))){
                        var defaultTask = job.JobTask.find(function(jt){ return jt.DefaultForEndDay; });
                        if(defaultTask){
                            _removeTask(scope.workweek, job, tasks[0]);
                            _addTaskToJob(job, defaultTask, false, scope.workweek, '', true);
                        }
                    }
                }
            }
            var obj = {}
            if(jobs.length){
                _changes(scope.workweek, true);
                jobs = jobs.map(function(job){
                    job.Location = job.Location.map(function(loc){
                        return {
                            PercentComplete: Math.round(loc.PercentComplete*100)/10000,
                            ID: loc.ID
                        }
                    });
                    return {ID: job.JobCrewJobID, Location: job.Location}
                });
                obj.CompleteLocationJob = jobs
            }
            obj.date = Utils.date(scope.workweek.WorkDay[scope.workweek.currentDay].WeekdayDate,'MM/dd/yyyy')
            if(scope.workweek.completeJobIds && scope.workweek.completeJobIds.length){
                var jobIds = scope.workweek.completeJobIds
                delete scope.workweek.completeJobIds
                obj.jobs = jobIds
                _save(scope, 'fte/saveAndCompleteWork', obj, 'completeWorkSaveDone', 'Loading Complete Work...');
            }else if(obj.CompleteLocationJob){
                obj.workweek = scope.workweek.ID;
                obj.completedWork = true;
                obj.reload = true;
                if(scope.dialogs.locationPercent.jobScreen){
                    scope.workweek.jobScreen = true;
                }
                _save(scope, 'fte/save', obj, 'locationPercentDone', 'Completing locations...', true);
            }else{
                _markJobsWorkComplete(_currentDay(scope.workweek))
                if(afterLoad)_initCrewTime(scope.workweek, scope)
            }
        }
        var _markJobsWorkComplete = function(day){
            for(var j=0; j<day.JobDayCrew.length; ++j){
                if(day.JobDayCrew[j].active){
                    day.JobDayCrew[j].hasCompletedWork = true;
                }
            }
        }
        var _removeJobs = function(workweek, jobs){
            var selectJobs = workweek.WorkDay[workweek.currentDay].Job;
            for(var j=0; j<jobs.length; ++j){
                var job = selectJobs.find(function(jb){ return jb.ID == jobs[j].JobCrewJobID });
                if(job){
                    _selectStartJob(workweek, job);
                }
            }
            _addRemoveJobs(workweek, workweek.currentDay);
            _updateJobHours(workweek, 0);
            _parseOutTaskTimes(workweek);
            _totalDay(workweek);
        }
        var _newTimeEntry = function(task, pc, laborType, workweek, isMember){
            return {
                ID: _getTempID(workweek),
                PhaseCodeID: pc.ID,
                JobTaskID: isMember ? '' : task.JobTaskID,
                JobDayCrewTaskID: isMember ? '': task.ID,
                JobDayCrewMemberTaskID: isMember ? task.ID : '',
                TimeDate: workweek.WorkDay[workweek.currentDay].WeekdayDate,
                Amount: 0,
                BillableAmount: 0,
                LaborTypeCode: laborType,
                BillingLaborTypeCode: laborType,
                BillableFlag: true,
                StatusCode: 'PE'
            }
        }
        var _laborTypes = ['ST','OT','DT'];
        var _findTERecord = function(pc, task, type, isMember, ignoreIDs, highUtil){ 
            return function(te){ 
                return te.PhaseCodeID == pc.ID 
                && (!type||te.LaborTypeCode == type) 
                && te.JobTaskID == task.JobTaskID 
                && ((isMember && te.JobDayCrewMemberTaskID == task.ID) || (!isMember && te.JobDayCrewTaskID == task.ID))
                && (!ignoreIDs||ignoreIDs.indexOf(te.ID)==-1)
                && (typeof highUtil=="undefined" || highUtil==te.HighUtilizationFlag)
            } 
        };
        var _parseTaskToMember = function(task, member, workweek, isMember){
            //Create MemberTask records for tracking times?
            if(workweek.WorkDay[workweek.currentDay].showJobTimesOnCrew){
                var memberTask = member.MemberTask.find(function(mt){ return mt.TaskID == task.ID })
                if(!memberTask){
                    memberTask = _newMemberTask(workweek, task);
                    member.MemberTask.push(memberTask);
                }
            }
            for(var p=0; p<task.PhaseCode.length; ++p){
                var pc = task.PhaseCode[p];
                for(var l=0; l<_laborTypes.length; ++l){
                    var type = _laborTypes[l], te;
                    var teIndex = member.TimeEntry.findIndex(_findTERecord(pc, task, type, isMember));
                    if(teIndex == -1 && pc.active && pc[type+'Hours'] != 0){
                        //Add new time entry
                        te = _newTimeEntry(task, pc, type, workweek, isMember);
                        member.TimeEntry.push(te);
                    }else{
                        te = member.TimeEntry[teIndex];
                    }
                    if(te && pc.active && pc[type+'Hours'] != 0){
                        te.updated = true;
                        te.TimeInHours = pc[type+'Hours'];
                        te.BillableHours = pc[type+'Hours'];
                    }
                }
            }
        }
        var _dayMemberSortComparator = function(a,b){
            if(a.cssClass != b.cssClass) return a.cssClass.localeCompare(b.cssClass);
            if(a.CraftSort != b.CraftSort) return a.CraftSort - b.CraftSort;
            if(a.Craft != b.Craft) return a.Craft.localeCompare(b.Craft);
            if(a.ClassSort != b.ClassSort) return a.ClassSort - b.ClassSort;
            if(a.Class != b.Class) return a.Class.localeCompare(b.Class);
            return a.Name.localeCompare(b.Name);
        }
        var _sortDayMembers = function(workweek, skipIndex){
            var day = _currentDay(workweek);
            var current = day.members[day.currentMember];
            day.members.sort(_dayMemberSortComparator);
            for(var m=0; m<day.members.length; ++m){
                day.members[m].index = m;
                if(!skipIndex && day.members[m] === current){
                    day.currentMember = m;
                }
            }
        }
        var _changedMemberStatus = function(memberJob, dayMember, workweek){
            if(!memberJob) return;            
            dayMember.STForOT = dayMember.STForOTBaseline;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var memberJob = dayMember.Jobs[j];
                _parseTimeToOneMemberJob(memberJob, workweek);
                _updateMemberJob(memberJob, dayMember, workweek);
            }
            _totalMemberTime(workweek, dayMember);
            _sortDayMembers(workweek);
        }
        var _checkPerDiem = function(dayMember, workweek, autoTriggered){
            var newVal = dayMember.HasPerDiem;
            var hasPerDiem = dayMember.HasPerDiem;
            dayMember.HasPerDiem = false;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var memberJob = dayMember.Jobs[j];
                memberJob.Member.PerDiemFlag = !hasPerDiem;
                if(!newVal){
                    if(!autoTriggered) memberJob.Member.NoPerDiemReasonCode = 'MR';
                }else{
                    if(dayMember.HasPerDiem){
                        newVal = undefined;
                    }
                }
                _calcMemberJobPerDiem(memberJob, dayMember, workweek, newVal);
            }
            _cleanUpDayMemberPerDiem(dayMember, workweek);
            _updatePersonPerDiemTracking(_currentDay(workweek), dayMember);
            _updateDayMemberNoPerDiemReason(dayMember);
            _totalMemberTime(workweek, dayMember);
        }
        var _updatePerDiemAmount = function(dayMember, workweek){
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var memberJob = dayMember.Jobs[j];
                if(memberJob.Member.PerDiemFlag){
                    memberJob.Member.PerDiemAmount = dayMember.PerDiem;
                    var laborUnit = _getPerDiemUnit(memberJob, workweek);
                    _calcPerDiemPaid(memberJob, laborUnit, workweek)
                    _syncMemberExpense(memberJob, laborUnit, workweek, memberJob.Member.PerDiemAmount);
                }
            }
            _dividePerDiemAmounts(workweek, dayMember);
            _totalMemberTime(workweek, dayMember);
        }
        var _updateMemberJobPerDiemAmount = function(memberJob, dayMember, workweek, previousValue){
            if(dayMember.Jobs.filter(function(jb){ return jb.Member.PerDiemFlag }).length == 1){
                dayMember.PerDiem = memberJob.Member.PerDiemAmount;
                _updatePerDiemAmount(dayMember, workweek);
                return;
            }
            var member = memberJob.Member;
            var newPD = Math.max(0,Math.min(member.PerDiemAmount, dayMember.PerDiem));
            var index = dayMember.Jobs.findIndex(function(mj){ return mj.Member.ID == member.ID });
            var diff = previousValue - newPD;
            while(diff != 0){
                ++index;
                if(index > dayMember.Jobs.length-1) index = 0;
                if(!dayMember.Jobs[index].Member.PerDiemFlag || !dayMember.Jobs[index].Member.TotalHours) continue;
                var previous = dayMember.Jobs[index].Member.PerDiemAmount;
                dayMember.Jobs[index].Member.PerDiemAmount = Math.max(dayMember.Jobs[index].Member.PerDiemAmount+diff, 0);
                diff -= (dayMember.Jobs[index].Member.PerDiemAmount-previous);
                if(dayMember.Jobs[index].Member.ID != member.ID){
                    _changeExpenseEntryAmount(workweek, dayMember.Jobs[index].Job, dayMember.Jobs[index].Member, dayMember.Jobs[index].Member.PerDiemAmount);
                }
            }
            member.PerDiemAmount = newPD;
            _changeExpenseEntryAmount(workweek, memberJob.Job, member, member.PerDiemAmount);
        }
        var _parseTimeToMember = function(member, job, workweek){
            //Initialize tracking flag
            member.TimeEntry = Utils.enforceArray(member.TimeEntry);
            for(var te=member.TimeEntry.length-1; te>=0; --te){
                member.TimeEntry[te].updated = false;
            }
            //Do task-based records
            member.MemberTask = Utils.enforceArray(member.MemberTask);
            if(member.MemberStatusDraftCrewTimeFlag){
                for(var t=0; t<job.Task.length; ++t){
                    var task = job.Task[t];
                    if(task.active /*&& !task.JobTaskNonCompensatedFlag*/){
                        _parseTaskToMember(task, member, workweek);
                    }else if(task.JobTaskNonCompensatedFlag){
                        task.showPhases = false;
                    }
                }
            }
            //Meals
            member.Meals = job.Meals;
            member.calcMeals = job.calcMeals;
            _changeExpenseEntryQty(member, member.Meals, workweek);
            
            //Clean up
            for(var te=member.TimeEntry.length-1; te>=0; --te){
                if(!member.TimeEntry[te].updated){
                    member.TimeEntry[te].TimeInHours = member.TimeEntry[te].BillableHours = 0;
                }
            }
        }
        var _parseTimeToOneMemberJob = function(job, workweek){
            if(!job.Member.MemberStatusDraftCrewTimeFlag) return;
            _parseTimeToMember(job.Member, job.Job, workweek);
            job.StartTime = new Date(job.Job.StartTime.getTime())
            for(var t=0; t<job.Tasks.length; ++t){
                if(job.Tasks[t].isMember){
                    for(var p=0; p<job.Tasks[t].PhaseCodes.length; ++p){
                        var pc = job.Tasks[t].PhaseCodes[p];
                        pc.STHours = pc.OTHours = pc.DTHours = pc.TotalHours = 0;
                    }
                }else{
                    for(var p=0; p<job.Tasks[t].PhaseCodes.length; ++p){
                        var pc = job.Tasks[t].PhaseCodes[p];
                        var memberTimes = _getMemberPhaseCodeTimes(job.Tasks[t].Task, pc.PhaseCode, job.Member);
                        pc.STHours = memberTimes.ST;
                        pc.OTHours = memberTimes.OT;
                        pc.DTHours = memberTimes.DT;
                        pc.TotalHours = (memberTimes.ST+memberTimes.OT+memberTimes.DT);
                    } 
                }
            }
        }
        var _parseTimeToOneMember = function(dayMember, workweek){
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var job = dayMember.Jobs[j];
                job.Member.NoPerDiemReasonCode = '';
                job.Member.PerDiemFlag = false;
                _parseTimeToOneMemberJob(job, workweek);
            }
            _totalMemberTime(workweek, dayMember);
        }
        var _clearMemberTime = function(dayMember, workweek){
            if(dayMember.HasPerDiem)_checkPerDiem(dayMember, workweek);
            dayMember.Meals = dayMember.calcMeals = 0;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var job = dayMember.Jobs[j];
                var member = job.Member;
                member.Meals = member.calcMeals = 0;
                for(var te=member.TimeEntry.length-1; te>=0; --te){
                    member.TimeEntry[te].TimeInHours = 0;
                    member.TimeEntry[te].BillableHours = 0;
                }
                for(var t=0; t<job.Tasks.length; ++t){
                    if(job.Tasks[t].isMember){
                        for(var p=0; p<job.Tasks[t].PhaseCodes.length; ++p){
                            var pc = job.Tasks[t].PhaseCodes[p];
                            pc.STHours = pc.OTHours = pc.DTHours = pc.TotalHours = 0;
                        }
                    }else{
                        for(var p=0; p<job.Tasks[t].PhaseCodes.length; ++p){
                            var pc = job.Tasks[t].PhaseCodes[p];
                            pc.STHours = pc.OTHours = pc.DTHours = pc.TotalHours = 0;
                        }
                    }
                }
            }
            _totalMemberTime(workweek, dayMember);
        }
        var _parseTimeToMembers = function(workweek){
            // Make member time match crew time
            var day = workweek.WorkDay[workweek.currentDay], jobs = _activeJobs(workweek);
            day.equipParsed = false;
            for(var j=0; j<jobs.length; ++j){
                var job = jobs[j];
                for(var m=0; m<job.JobDayCrewMember.length; ++m){
                    var member = job.JobDayCrewMember[m];
                    _parseTimeToMember(member, job, workweek);
                }
            }
            _calcShowTaskRows(workweek);
        }
        var _getMemberPhaseCodeTimes = function(task, pc, member){
            var result = {};
            for(var l=0; l<_laborTypes.length; ++l){
                var type = _laborTypes[l];
                result[type] = 0;
                var te = member.TimeEntry.filter(_findTERecord(pc, task, type, false));
                for(var i=0; i<te.length; ++i){
                    result[type] += te[i].TimeInHours;
                }
            }
            return result;
        }
        var _getMemberWeekTotals = function(member, workweek, dayIndex){
            if(typeof dayIndex == "undefined"){
                dayIndex = workweek.currentDay
            }
            var totals = {WeekST:0,WeekOT:0,WeekDT:0,WeekPD:0,WeekME:0,ClassST:0,ClassOT:0,ClassDT:0,ClassPD:0,ClassME:0,HasClassPD:false}, personData = workweek.weekTotals[member.PersonID];
            //Get data from sources other than this timesheet
            if(personData){
                Object.assign(totals, Utils.pick(personData, 'STForOT','WeekPD','WeekST','WeekOT','WeekDT','WeekME'))
                if(personData.classes[member.ClassID]){
                    Object.assign(totals, Utils.pick(personData.classes[member.ClassID], 'ClassST', 'ClassOT', 'ClassDT','ClassPD','ClassME'))
                    totals.HasClassPD = totals.HasClassPD||totals.ClassPD > 0;
                }
                totals.CanPerDiem = personData.CanPerDiem[dayIndex];
            }
            //Get data from this timesheet
            for(var d=0; d<7; ++d){
                if(!_shouldCountWeekTime(workweek.WorkDay[d])) continue;
                for(var j=0; j<workweek.WorkDay[d].JobDayCrew.length; ++j){
                    var jdc = workweek.WorkDay[d].JobDayCrew[j];
                    if(!jdc.active) continue;
                    for(var m=0; m<jdc.JobDayCrewMember.length; ++m){
                        var jdcm = jdc.JobDayCrewMember[m];
                        if(jdcm.PersonID != member.PersonID || (jdcm.PersonID == member.PersonID && jdcm.ClassID == member.ClassID && d == dayIndex)) continue;
                        totals.WeekST += jdcm.STHours;
                        totals.WeekOT += jdcm.OTHours;
                        totals.WeekDT += jdcm.DTHours;
                        totals.WeekPD += jdcm.PerDiemFlag?jdcm.PerDiemAmount:0;
                        totals.WeekME += jdcm.Meals;
                        if(jdcm.ClassID != member.ClassID || d == dayIndex) continue;
                        totals.ClassST += jdcm.STHours;
                        totals.ClassOT += jdcm.OTHours;
                        totals.ClassDT += jdcm.DTHours;
                        totals.ClassPD += jdcm.PerDiemFlag?jdcm.PerDiemAmount:0;
                        totals.HasClassPD = totals.HasClassPD||jdcm.PerDiemFlag;
                        totals.ClassME += jdcm.Meals;
                    }
                }
            }
            return totals;
        }
        var _changeExpenseEntryAmount = function(workweek, jdc, member, amount){
            var entry = member.ExpenseEntry.find(function(ee){ return ee.TypeCode == 'PD'});
            if(entry){
                entry.BillingAmount = amount?Utils.round(entry.BillingAmount*amount/member.PerDiemAmount,2):0;
                entry.ContractAmount = amount?Utils.round(entry.ContractAmount*amount/member.PerDiemAmount,2):0;
                entry.Amount = amount;

                var contract = _jobContractData(jdc, workweek);
                if(contract && contract.PDSync){
                    entry.BillingAmount = entry.Amount;
                }
            }
        }
        var _changeExpenseEntryQty = function(member, qty, workweek){
            var entry = member.ExpenseEntry.find(function(ee){ return ee.TypeCode == 'ME'});
            if(!entry){
                entry = _newExpenseEntry('ME', member, workweek);
            }
            entry.Qty = qty||0;
            entry.BillingQty = qty||0;
        }
        var _dividePerDiemAmounts = function(workweek, dayMember, pdAmount){
            var hasPdAmount = (typeof pdAmount!='undefined'), totalPDHours = 0;
            var targetJob = null, highestHours = 0;
            pdAmount = pdAmount || 0;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var member = dayMember.Jobs[j].Member;
                if(member.PerDiemFlag){
                    if(!hasPdAmount && member.PerDiemAmount > pdAmount){
                        pdAmount = member.PerDiemAmount;
                    }
                    if((dayMember.PDSpread == 'FRST' && !targetJob) || (dayMember.PDSpread == 'HIGH' && member.TotalHours > highestHours)){
                        targetJob = member;
                        highestHours = member.TotalHours;
                    }
                    totalPDHours += member.TotalHours;
                }
            }
            dayMember.PerDiem = pdAmount;
            var pdPerHour = totalPDHours?(pdAmount/totalPDHours):0;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var member = dayMember.Jobs[j].Member;
                if(targetJob){
                    member.PerDiemFlag = targetJob===member;
                };
                if(member.PerDiemFlag){
                    var newPD = targetJob?(targetJob===member?dayMember.PerDiem:0):Math.min(pdAmount,Math.max(Utils.round(pdPerHour*member.TotalHours,0),0));
                    member.PerDiemAmount = newPD;
                    pdAmount -= newPD;
                }
            }
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var member = dayMember.Jobs[j].Member;
                if(member.PerDiemFlag){
                    var newPD = member.PerDiemAmount;
                    if(pdAmount){
                        newPD += pdAmount;
                        member.PerDiemAmount = newPD;
                        pdAmount = 0;
                    }
                    _changeExpenseEntryAmount(workweek, dayMember.Jobs[j].Job, member, newPD);
                }
            }
            
        }
        var _divideMeals = function(dayMember, workweek){
            var totalMealHours = 0, hrs = {}
            dayMember.Meals = dayMember.Meals||0;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var tasks = dayMember.Jobs[j].Tasks;
                if(dayMember.Jobs[j].Job.CanMeals){
                    var compHours = tasks.reduce(function(total, t){ if(!t.Task.JobTaskNonCompensatedFlag)total+=t.TotalHours; return total;}, 0);
                    totalMealHours += compHours;
                    hrs[dayMember.Jobs[j].ID] = compHours;
                }
            }
            var mealsPerHour = totalMealHours?(dayMember.Meals/totalMealHours):0,
                remaining = dayMember.Meals;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var member = dayMember.Jobs[j].Member;
                if(typeof hrs[dayMember.Jobs[j].ID] != 'undefined'){
                    var newMeals = Math.min(dayMember.Meals,Math.max(Utils.round(mealsPerHour*hrs[dayMember.Jobs[j].ID],0),0));
                    member.Meals = newMeals;
                    remaining -= newMeals;
                    _changeExpenseEntryQty(member, newMeals, workweek);
                }
            }            
        }
        var _otherHasPerDiem = function(day, dayMember){
            if(day.peoplePerDiem[dayMember.PersonID] && dayMember.Jobs.length && day.peoplePerDiem[dayMember.PersonID] != dayMember.Jobs[0].Member.ID){
                return true;
            }
            return false;
        }
        var _updatePersonPerDiemTracking = function(day, dayMember){
            if(dayMember.HasPerDiem){
                if(day.peoplePerDiem[dayMember.PersonID] != dayMember.Jobs[0].Member.ID){
                    day.peoplePerDiem[dayMember.PersonID] = dayMember.Jobs[0].Member.ID;
                    for(var m=0; m<day.members.length; ++m){
                        if(day.members[m].PersonID == dayMember.PersonID && day.members[m].ClassID != dayMember.ClassID){
                            var reasons = [];
                            if(!day.members[m].OtherHasPerDiem){
                                for(var j=0; j<day.members[m].Jobs.length; ++j){
                                    if(['MR',''].includes(day.members[m].Jobs[j].Member.NoPerDiemReasonCode)){
                                        day.members[m].Jobs[j].Member.NoPerDiemReasonCode = 'AM'
                                    }
                                    reasons.push(day.members[m].Jobs[j].Member.NoPerDiemReasonCode);
                                }
                            }
                            _updateDayMemberNoPerDiemReason(day.members[m], reasons);
                            day.members[m].OtherHasPerDiem = true;
                        }
                    }
                }
            }else{
                if(day.peoplePerDiem[dayMember.PersonID] == dayMember.Jobs[0].Member.ID){
                    delete day.peoplePerDiem[dayMember.PersonID];
                    for(var m=0; m<day.members.length; ++m){
                        if(day.members[m].PersonID == dayMember.PersonID && day.members[m].ClassID != dayMember.ClassID){
                            var reasons = [];
                            if(day.members[m].OtherHasPerDiem){
                                for(var j=0; j<day.members[m].Jobs.length; ++j){
                                    if(day.members[m].Jobs[j].Member.NoPerDiemReasonCode == 'AM'){
                                        day.members[m].Jobs[j].Member.NoPerDiemReasonCode = 'MR'
                                    }
                                    reasons.push(day.members[m].Jobs[j].Member.NoPerDiemReasonCode);
                                }
                            }
                            _updateDayMemberNoPerDiemReason(day.members[m], reasons);
                            day.members[m].OtherHasPerDiem = false;
                        }
                    }
                }
            }
        }
        var _totalMemberTime = function(workweek, member, skipPD, day, skipTotals){
            day = day||workweek.WorkDay[workweek.currentDay];
            day.peoplePerDiem = day.peoplePerDiem||{};
            var step = _currentStep(workweek);
            if(!member){
                day.peoplePerDiem = {};
                for(var m=0; m<day.members.length; ++m){
                    _totalMemberTime(workweek, day.members[m], skipPD, day, true);
                }
                _totalDay(workweek);
                return;
            }
            if(member == 'current'){
                member = day.members[day.currentMember];
            }
            var pdChanged = false;
            member.cssClass = '';
            member.STHours = member.OTHours = member.DTHours = member.WeekSTHours = member.WeekOTHours = member.WeekDTHours = member.ClassSTHours = member.ClassOTHours = member.ClassDTHours = 0;
            member.WeekPerDiem = member.ClassPerDiem = member.PerDiemTotal = member.PerDiem = 0;
            member.WeekMeals = member.ClassMeals = member.Meals = member.calcMeals = 0;
            member.perDiemCalc = 0;
            //Phase code and task add ups
            var hasActive = false, hasPerDiem = false, doTimes = true, currentTime;
            for(var j=0; j<member.Jobs.length; ++j){
                var job = member.Jobs[j];
                if(doTimes){
                    if(j == 0){
                        currentTime = job.StartTime;
                    }else{
                        job.StartTime = new Date(currentTime.getTime())
                    }
                }
                job.Member.STHours = job.Member.OTHours = job.Member.DTHours = job.PerDiemTotal = 0;
                for(var t=0; t<job.Tasks.length; ++t){
                    var task = job.Tasks[t];
                    task.STHours = task.OTHours = task.DTHours = 0;
                    for(var p=0; p<task.PhaseCodes.length; ++p){
                        var pc = task.PhaseCodes[p];
                        task.STHours += pc.STHours;
                        task.OTHours += pc.OTHours;
                        task.DTHours += pc.DTHours;
                        if(task.isMember){ pc.PhaseCode.STHours = pc.STHours; pc.PhaseCode.OTHours = pc.OTHours; pc.PhaseCode.DTHours = pc.DTHours; }
                        pc.TotalHours = _total(pc);
                    }
                    if(task.isMember){ task.Task.STHours = task.STHours; task.Task.OTHours = task.OTHours; task.Task.DTHours = task.DTHours; }
                    task.TotalHours = _total(task);
                    if(task.Task.perDiemEligible){
                        job.PerDiemTotal += task.TotalHours;
                        member.PerDiemTotal += task.TotalHours;
                    }
                    job.Member.STHours += task.STHours;
                    job.Member.OTHours += task.OTHours;
                    job.Member.DTHours += task.DTHours;
                    if(doTimes){
                        task.StartTime = new Date(currentTime.getTime())
                        task.EndTime = new Date(currentTime.getTime())
                        task.EndTime.addHours(task.TotalHours)
                        currentTime = task.EndTime;
                        var memTask = task.Task;
                        if(!task.isMember){
                            memTask = task.MemberTask;
                        }
                        if(memTask){ memTask.StartTime = task.StartTime; memTask.EndTime = task.EndTime; memTask.STHours = task.STHours; memTask.OTHours = task.OTHours; memTask.DTHours = task.DTHours; }
                    }
                }
                if(doTimes){
                    job.EndTime = new Date(currentTime.getTime())
                }
                job.Member.TotalHours = _total(job.Member);
                member.STHours += job.Member.STHours;
                member.OTHours += job.Member.OTHours;
                member.DTHours += job.Member.DTHours;
                member.PerDiem += job.Member.PerDiemFlag?job.Member.PerDiemAmount:0;
                member.perDiemCalc += job.Member.perDiemCalc;
                member.Meals += job.Member.Meals||0;
                member.calcMeals += job.Member.calcMeals||0;
                if(job.Member.ActiveFlag){
                    hasActive = true;
                }
            }
            //Set per diem
            member.OtherHasPerDiem = _otherHasPerDiem(day, member);
            var reasons = [];
            for(var j=0; j<member.Jobs.length; ++j){
                var job = member.Jobs[j];
                if(job.Member.NoPerDiemReasonCode != 'MR' && !skipPD){
                    _calcMemberJobPerDiem(job, member, workweek, undefined, (step.ProcessStepCode=='RS'||['SB','RV','AP'].includes(day.StatusCode)));
                }
                reasons.push(job.Member.NoPerDiemReasonCode);
                hasPerDiem = hasPerDiem||job.Member.PerDiemFlag;
            }
            _cleanUpDayMemberPerDiem(member, workweek);
            _updatePersonPerDiemTracking(day, member);
            _updateDayMemberNoPerDiemReason(member, reasons);
            //Totals for member
            if(!hasActive) member.cssClass = 'inactive';
            member.TotalHours = _total(member);
            var week = _getMemberWeekTotals(member, workweek, day.index);
            member.WeekSTHours = week.WeekST + member.STHours;
            member.WeekOTHours = week.WeekOT + member.OTHours;
            member.WeekDTHours = week.WeekDT + member.DTHours;
            member.ClassSTHours = week.ClassST + member.STHours;
            member.ClassOTHours = week.ClassOT + member.OTHours;
            member.ClassDTHours = week.ClassDT + member.DTHours;
            member.ClassTotalHours = _total(member, 'Class');
            member.WeekTotalHours = _total(member, 'Week');
            member.WeekPerDiem = week.WeekPD + member.PerDiem;
            member.WeekMeals = week.WeekME + member.Meals;
            member.ClassPerDiem = week.ClassPD + member.PerDiem;
            member.ClassMeals = week.ClassME + member.Meals;
            member.HasClassPerDiem = member.HasPerDiem||week.HasClassPD;
            if(!skipTotals){
                _totalDay(workweek);
            }
        }
        var _loadMemberWeekData = function(scope, member, fromInit){
            var jobs = {}, workDayIds = [];
            var Member = scope.workweek.WorkDay.reduce(function(ms, wd){
                if(!Utils.isTempId(wd.ID)) workDayIds.push(wd.ID);
                wd.JobDayCrew.reduce(function(ms, jb){ 
                    if(jb.active){
                        if(!jobs[jb.JobCrewJobID]) jobs[jb.JobCrewJobID] = true;
                        jb.JobDayCrewMember.reduce(function(ms, m){
                            if(!member || (Array.isArray(member)?member.includes(m.PersonID):(m.PersonID == member.PersonID && m.ClassID == member.ClassID))){
                                ms.push({ID: m.ID, PersonID: m.PersonID, ClassID: m.ClassID, JobCrewID: jb.JobCrewID });
                            }
                            return ms;
                        }, ms);
                    }
                    return ms;
                }, ms);
                return ms;
            }, []);
            var loadJobs = Object.keys(jobs).map(function(jbId){ return {ID: jbId}}).filter(function(job){
                if(!scope.workweek.contractData || !scope.workweek.contractData.jobs || !scope.workweek.contractData.jobs[job.ID] || !scope.workweek.contractData.jobs[job.ID].mbLoaded){
                    return true;
                }
                return false;
            });

            scope.loadingDialog();
            var data = {WorkDay:{IDs: workDayIds.join(','),Member: Member, Job:loadJobs}, sp:(typeof scope.workweek.sp.memberStatuses=='undefined'), org:scope.workweek.PersonOrganizationID, date: Utils.date(_currentDay(scope.workweek).WeekdayDate,'MM/dd/yyyy'), fromInit: fromInit?true:false};
            if(fromInit!==true && (!member||Array.isArray(member))){
                _save(scope, 'fte/initMemberTime/save', data, 'initMemberTimeResult');
            }else{
                _load(scope, 'fte/initMemberTime', 'POST', data, 'initMemberTimeResult');
            }
        }
        var _processMemberWeekData = function(workweek, data){
            data.Member = Utils.enforceArray(data.Member);
            workweek.weekTotals = workweek.weekTotals||{};
            var donePeople = {};
            var trackJCData = {}
            for(var m=0; m<data.Member.length; ++m){
                var memberData = data.Member[m];
                if(memberData.CanPerDiem){
                    if(!workweek.weekTotals[memberData.PersonID]) workweek.weekTotals[memberData.PersonID] = {classes: {},jobCrews:{}};
                    if(!donePeople[memberData.PersonID]){
                        Object.assign(workweek.weekTotals[memberData.PersonID], Utils.pick(memberData, 'STForOT','WeekPD','WeekST','WeekOT','WeekDT','WeekME'));
                        workweek.weekTotals[memberData.PersonID].CanPerDiem = memberData.CanPerDiem.split(',').map(function(pd){ return pd=='Y' });
                        donePeople[memberData.PersonID] = {};
                    }
                    if(!workweek.weekTotals[memberData.PersonID].classes[memberData.ClassID]) workweek.weekTotals[memberData.PersonID].classes[memberData.ClassID] = {};
                    if(!donePeople[memberData.PersonID][memberData.ClassID]){
                        Object.assign(workweek.weekTotals[memberData.PersonID].classes[memberData.ClassID], Utils.pick(memberData, 'ClassST','ClassOT','ClassDT','ClassPD','ClassME'));
                        donePeople[memberData.PersonID][memberData.ClassID] = true;
                    }
                }
                //Job crew related data
                for(var d=0; d<7; ++d){
                    var day = workweek.WorkDay[d];
                    for(var j=0; j<day.JobDayCrew.length; ++j){
                        var job = day.JobDayCrew[j];
                        if(!job.active) continue;
                        if(!trackJCData[job.JobCrewID]) trackJCData[job.JobCrewID] = {};
                        for(var jm=0; jm<job.JobDayCrewMember.length; ++jm){
                            var member = job.JobDayCrewMember[jm];
                            if(member.ID == memberData.ID){
                                if(!workweek.weekTotals[memberData.PersonID].jobCrews[job.JobCrewID]) workweek.weekTotals[memberData.PersonID].jobCrews[job.JobCrewID] = Utils.pick(memberData, 'PDEligible','PDOverride','PDType');
                                Object.assign(member, workweek.weekTotals[memberData.PersonID].jobCrews[job.JobCrewID]);
                                if(memberData.JobCrewMemberID){
                                    if(!trackJCData[job.JobCrewID][member.PersonID]) trackJCData[job.JobCrewID][member.PersonID] = {};
                                    if(memberData.JobCrewMemberID != member.JobCrewMemberID){
                                        trackJCData[job.JobCrewID][member.PersonID].assignMember = member;
                                        trackJCData[job.JobCrewID][member.PersonID].assignID = memberData.JobCrewMemberID;
                                    }else{
                                        trackJCData[job.JobCrewID][member.PersonID].alreadyAssigned = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Update job crew member IDs, if needed
            var jobCrewIDs = Object.keys(trackJCData);
            for(var i=0; i<jobCrewIDs.length; ++i){
                var personIDs = Object.keys(trackJCData[jobCrewIDs[i]]);
                for(var j=0; j<personIDs.length; ++j){
                    var jobData = trackJCData[jobCrewIDs[i]][personIDs[j]];
                    if(!jobData.alreadyAssigned && jobData.assignMember){
                        jobData.assignMember.JobCrewMemberID = jobData.assignID;
                    }
                }
            }
            //Person per diem
            /*if(data.Person){
                data.Person = Utils.enforceArray(data.Person);
                workweek.personPD = workweek.personPD||{}
                for(var p=0; p<data.Person.length; ++p){
                    workweek.personPD[data.Person[p].ID] = {Amount:data.Person[p].PDAmount,Type:data.Person[p].PDType}
                }
            }*/
        }
        var _loadMemberWeekDataDone = function(workweek, data){
            _processMemberWeekData(workweek, data);
            //Do contract data
            _contractMemberEquipDone(data, workweek, 'mbLoaded');
             if(data.WT){
                _prepareContractWorkingTimeData(data, _activeJobs(workweek), 'WT');
            }
            if(data.Comm){
                _initSafety(workweek, data);
            }
        }
        var _jobContractData = function(jdc, workweek){
            var data = { MinimumForPerDiem: 0, EquipmentTimeCap: '', PDSync: false }
            if(workweek.contractData && workweek.contractData.jobs && workweek.contractData.jobs[jdc.JobCrewJobID]){
                data = workweek.contractData.jobs[jdc.JobCrewJobID];
                var contract = workweek.contractData.contracts.find(function(c){ return c.jobs.indexOf(jdc.JobCrewJobID) > -1});
                if(contract){
                    data.PDSync = contract.PDSync;
                }
            }
            return data;
        }
        var _getPerDiemUnit = function(memberJob, workweek){
            if(workweek.contractData && workweek.contractData.contracts){
                var day = _currentDay(workweek);
                var contract = workweek.contractData.contracts.find(function(c){
                    return (c.Start <= day.WeekdayDate && c.End >= day.WeekdayDate)&&(c.jobs.indexOf(memberJob.Job.JobCrewJobID)>-1);
                });
                if(contract){
                    var unit = contract.LaborUnit.find(function(lu){ return lu.CraftID==memberJob.Member.CraftID && lu.ClassID==memberJob.Member.ClassID });
                    if(unit){
                        return unit;
                    }
                }
            }
            return _isOverheadJob(memberJob.Job, workweek);
        }
        var _getTopJobTask = function(memberJob){
            var topTask = null, topHours = 0;
            var topPC = null,   topPCHours = 0;
            for(var i=0; i<memberJob.Job.Task.length; ++i){
                if(memberJob.Job.Task[i].active){
                    if(!topTask || memberJob.Job.Task[i].Duration > topHours){
                        topTask = memberJob.Job.Task[i];
                        topHours = memberJob.Job.Task[i].Duration;
                    }
                    for(var j=0; j<memberJob.Job.Task[i].PhaseCode.length; ++j){
                        if(!topPC || memberJob.Job.Task[i].PhaseCode[j].TotalHours > topPCHours){
                            topPC = memberJob.Job.Task[i].PhaseCode[j];
                            topPCHours = memberJob.Job.Task[i].PhaseCode[j].TotalHours;
                        }
                        
                    }
                }
            }
            return {JobTaskID:topTask?topTask.JobTaskID:'', PhaseCodeID:topPC?topPC.ID:''};
        }
        var _isOverheadJob = function(job, workweek){
            return job.OwnerID==workweek.PayrollPeriodOrganizationID;
        }
        var _calcPerDiemPaid = function(memberJob, laborUnit, workweek){
            var member = memberJob.Member, isOverhead = _isOverheadJob(memberJob.Job, workweek);
            //Calc the amount we should apply
                if(isOverhead){
                    laborUnit = {
                        Amount: 0,
                        Price: 0,
                    }
                }
                var paidAmount = laborUnit.Amount;
                if(!paidAmount && paidAmount !== 0) paidAmount = laborUnit.Price;
                member.contractPDAmount = paidAmount;
                
                if(member.PDType == 'NO'){
                    paidAmount = 0;
                }else if (member.PDType == 'GR'){
                    paidAmount = Math.max(member.contractPDAmount, member.PDOverride);
                }else if(member.PDType == 'ST' && member.permanentMessage=='Permanently Add To Crew'){
                    paidAmount = member.contractPDAmount;
                }else{
                    paidAmount = member.PDOverride;
                }
                
                //store the "should" amount on member for later use
                member.perDiemCalc = paidAmount;
        }
        var _newExpenseEntry = function(type, member, workweek){
            var entry = {
                ID:_getTempID(workweek),
                TypeCode: type,
                Amount: 0,
                BillingAmount: 0,
                active: true
            }
            member.ExpenseEntry.push(entry);
            return entry;
        }
        var _syncMemberExpense = function(memberJob, laborUnit, workweek, amount){
            var member = memberJob.Member, isOverhead = _isOverheadJob(memberJob.Job, workweek);
            var entry = member.ExpenseEntry.find(function(ee){ return ee.TypeCode == 'PD'})
            if(!member.PerDiemFlag){
                if(entry) entry.active = false;
            }else if(laborUnit || isOverhead){
                if(isOverhead){
                    laborUnit = {
                        Amount: 0,
                        Price: 0,
                        ContractLUID: '',
                        ID: ''
                    }
                }
                var paidAmount = member.perDiemCalc,
                    contractAmount = member.contractPDAmount;
                //Update the expense if per diem is applied
                if(!entry){
                    entry = _newExpenseEntry('PD', member, workweek);
                }
                if(typeof amount != 'undefined'){
                    paidAmount = amount;
                }
                entry.active = true
                var topTask = _getTopJobTask(memberJob);
                entry.Amount = paidAmount;
                entry.BillingAmount = laborUnit.Price;
                entry.ContractAmount = contractAmount;
                entry.ContractLaborUnitID = laborUnit.ContractLUID;
                entry.JobTaskID = topTask.JobTaskID;
                entry.PhaseCodeID = topTask.PhaseCodeID;
                entry.LaborUnitID = laborUnit.ID;

                var contract = _jobContractData(memberJob.Job, workweek);
                if(contract && contract.PDSync){
                    entry.BillingAmount = entry.Amount;
                }
            }
            entry = member.ExpenseEntry.find(function(ee){ return ee.TypeCode == 'ME'})
            if(!member.Meals){
                if(entry)entry.active = false;
            }else{
                if(!entry){
                    entry = _newExpenseEntry('ME', member, workweek);
                }
                entry.active = true
                entry.Qty = entry.BillingQty = member.Meals;
            }
            _cleanExpenses(member);
        }
        var _clearPerDiemReason = function(memberJob){
            memberJob.Member.NoPerDiemReasonCode = '';
            delete memberJob.Member.noPerDiem;
        }
        var _updateDayMemberNoPerDiemReason = function(dayMember, reasons){
            reasons = Utils.enforceArray(reasons);
            if(!reasons.length){
                for(var j=0; j<dayMember.Jobs.length; ++j){
                    reasons.push(dayMember.Jobs[j].Member.NoPerDiemReasonCode);
                }
            }
            reasons = reasons.map(function(reason){ return _noPerDiemReasonOrder[reason]||10; });
            reasons.sort(function(a,b){ return a-b; });
            if(reasons.length && reasons[0] < 10){
                dayMember.NoPerDiemReasonCode = _noPerDiemReasonCodes[reasons[0]];
            }else{
                dayMember.NoPerDiemReasonCode = '';
            }
        }
        var _noPerDiemReasonOrder = {
            MR: 1, AM: 2, MH: 3, AJ: 4, NA: 5, LU: 6
        }
        var _noPerDiemReasonCodes = ['','MR','AM','MH','AJ','NA','LU'];
        var _updatePerDiemReason = function(memberJob, dayMember, weekTotals, workweek, noLaborUnit){
            var code = memberJob.Member.NoPerDiemReasonCode //If it was manually removed, this should already be MR
            if(code != 'MR'){
                if(noLaborUnit){
                    code = 'LU' //Labor unit not found on Contract
                }else if(!memberJob.Member.PDEligible){
                    code = 'NA' //Not allowed on Job/Contract
                }else if(!weekTotals.CanPerDiem){
                    code = 'AJ' //Per Diem applied on another Job
                }else if(dayMember.OtherHasPerDiem){
                    code = 'AM' //Per Diem already applied for Crew Member on Day
                }else if((dayMember.PerDiemTotal!=null&&dayMember.PerDiemTotal!=='') && dayMember.PerDiemTotal < _jobContractData(memberJob.Job, workweek).MinimumForPerDiem){
                    code = 'MH' //Minimum hours not met for this Crew Member on Day
                }else if(!memberJob.Member.TotalHours){
                    code = 'ZH' //Job has zero hours recorded
                }
            }
            if(!memberJob.Member.noPerDiem || memberJob.Member.noPerDiem.Code != code){
                memberJob.Member.NoPerDiemReasonCode = code;
                _mapSmartpick(workweek, memberJob.Member, 'noPerDiem', _spMappings.noPerDiemReason, 'noPerDiemReason');
            }
        }
        //This is the first pass per diem calc
        var _calcMemberJobPerDiem = function(memberJob, dayMember, workweek, tryValue, calcOnly){
            if(calcOnly) return;
            var manual = typeof tryValue =="undefined"?false:true, oldPerDiem = memberJob.Member.PerDiemFlag;
            var weekTotals = workweek.weekTotals[memberJob.Member.PersonID], applyPerDiem = false, laborUnit;
            if(manual || memberJob.Member.MemberStatusDraftCrewTimeFlag){
                var contractData = _jobContractData(memberJob.Job, workweek);
                if(memberJob.Member.PDEligible && dayMember.PerDiemTotal >= contractData.MinimumForPerDiem && weekTotals.CanPerDiem[workweek.currentDay] && !dayMember.OtherHasPerDiem && memberJob.Member.TotalHours){
                    applyPerDiem = manual?tryValue:true;
                }
            }
            laborUnit = _getPerDiemUnit(memberJob, workweek);
            if(applyPerDiem && !laborUnit){
                applyPerDiem = false;
            }
            if(applyPerDiem){
                if(!oldPerDiem){
                    memberJob.Member.PerDiemFlag = true;
                    _clearPerDiemReason(memberJob);
                }
            }else{
                if(oldPerDiem){
                    memberJob.Member.PerDiemFlag = false;
                    if(!manual) memberJob.Member.NoPerDiemReasonCode = '';
                    _updatePerDiemReason(memberJob, dayMember, weekTotals, workweek, laborUnit==null);
                }
            }
            memberJob.pdLaborUnit = laborUnit;
            memberJob.pdChanged = oldPerDiem != memberJob.Member.PerDiemFlag;
        }
        var _getPDSpreadStrategy = function(dayMember, workweek){
            var stategy = 'ALL';
            for(var j=0; j<dayMember.Jobs.length; ++j){
                if(dayMember.Jobs[j].Job.PDSpread == 'FRST'){
                    return 'FRST'
                }else if(dayMember.Jobs[j].Job.PDSpread == 'HIGH'){
                    stategy = 'HIGH'
                }
            }
            return stategy;
        }
        //Second pass needs to clean up all the info and sync expenses
        var _cleanUpDayMemberPerDiem = function(dayMember, workweek){
            var changed = !!dayMember.Jobs.find(function(mj){ return mj.pdChanged }), hasPerDiem = false, perDiemCalc = 0;
            for(var j=0; j<dayMember.Jobs.length; ++j){
                var memberJob = dayMember.Jobs[j];
                _calcPerDiemPaid(memberJob, memberJob.pdLaborUnit||_getPerDiemUnit(memberJob, workweek), workweek)
                if(changed){
                    _syncMemberExpense(memberJob, memberJob.pdLaborUnit, workweek);
                }
                hasPerDiem = hasPerDiem || memberJob.Member.PerDiemFlag;
                if(memberJob.Member.perDiemCalc > perDiemCalc){
                    perDiemCalc = memberJob.Member.perDiemCalc;
                }
                delete memberJob.pdLaborUnit;
                delete memberJob.pdChanged;
            }
            if(changed||dayMember.PDSpread != 'ALL'){
                _dividePerDiemAmounts(workweek, dayMember);
            }
            dayMember.HasPerDiem = hasPerDiem;
            dayMember.perDiemCalc = perDiemCalc;
        }
        var _getMemberSTForOTBaseline = function(dayMember, workweek){
            //Amounts not on this timesheet
            if(workweek.weekTotals[dayMember.PersonID]){
                dayMember.STForOTBaseline = dayMember.STForOT = workweek.weekTotals[dayMember.PersonID].STForOT;
            }else{
                dayMember.STForOTBaseline = dayMember.STForOT = 0;
            }
            //Amounts on this timesheet
            for(var d=0; d<7; ++d){
                if(d == workweek.currentDay || !_shouldCountWeekTime(workweek.WorkDay[d])) continue;
                for(var j=0; j<workweek.WorkDay[d].JobDayCrew.length; ++j){
                    if(!workweek.WorkDay[d].JobDayCrew[j].active) continue;
                    for(var m=0; m<workweek.WorkDay[d].JobDayCrew[j].JobDayCrewMember.length; ++m){
                        if(workweek.WorkDay[d].JobDayCrew[j].JobDayCrewMember[m].PersonID != dayMember.PersonID) continue;
                        _addMemberSTForOTFromJDCMember(dayMember, workweek.WorkDay[d].JobDayCrew[j], workweek.WorkDay[d].JobDayCrew[j].JobDayCrewMember[m], true);
                    }
                }
            }
        }

        var _getCachedPCInfo = function(phaseID, jobDayCrew, jobTaskID){
            if(!_pcInfoCache[phaseID]){
                _pcInfoCache[phaseID] = {
                    STCountsForOT: true
                }
                var jobTask = jobDayCrew.JobTask.find(function(jt){ return jt.ID == jobTaskID });
                if(jobTask){
                    var jtpc = jobTask.JobTaskPhaseCode.find(function(pc){ return pc.PhaseCodeID == phaseID });
                    if(jtpc){
                        var override = jtpc.OverridePhaseCodeID > 0;
                        _pcInfoCache[phaseID].STCountsForOT = override ? (!jobTask.PhaseCodeSectionID || jtpc.OverridePhaseCoIncludeInOTCalcFlag) : (!jobTask.PhaseCodeSectionID || jtpc.PhaseCodeIncludeInOTCalcFlag);
                    }
                }
            }
            return _pcInfoCache[phaseID];
        }
        var _addMemberSTForOTFromJDCMember = function(dayMember, jdc, jdcMember, doBaseline){
            jdcMember.TimeEntry = Utils.enforceArray(jdcMember.TimeEntry);
            for(var te=0; te<jdcMember.TimeEntry.length; te++){
                var entry = jdcMember.TimeEntry[te];
                if(entry.TimeInHours > 0 && entry.LaborTypeCode == 'ST'){
                    var pcInfo = _getCachedPCInfo(entry.PhaseCodeID, jdc, entry.JobTaskID);
                    if(pcInfo.STCountsForOT){
                        dayMember.STForOT += entry.TimeInHours;
                        if(doBaseline){
                            dayMember.STForOTBaseline += entry.TimeInHours;
                        }
                    }
                }
            }
        }
        var _processMemberTimeRules = function(dayMember, member, task, pc, memberPhaseCode, crewRules, workweek){
            if(!task.JobTaskPhaseCodeSectionID || pc.IncludeInOTCalcFlag){
                if(dayMember.STForOT+memberPhaseCode.STHours > 40){
                    var oldST = memberPhaseCode.STHours;
                    memberPhaseCode.STHours = Math.max(40-dayMember.STForOT, 0);
                    memberPhaseCode.OTHours += oldST-memberPhaseCode.STHours;
                    _updateMemberPhaseCodeTimeEntry(member, memberPhaseCode, task, ['ST','OT'], workweek, false);
                }
                dayMember.STForOT += memberPhaseCode.STHours;
            }
        }
        var _updateMemberJob = function(memberJob, dayMember, workweek, calcOnly){
            var member = memberJob.Member, job = memberJob.Job;
            var crewRules = _getTimeWorkWeekRule(workweek.Code);
            var processedTaskTimes = false
            memberJob.PerDiemTotal = 0;
            memberJob.StartTime = new Date(job.StartTime.getTime());
            memberJob.EndTime = new Date(job.EndTime.getTime());
            member.MemberTask = Utils.enforceArray(member.MemberTask);
            if(job.CanMeals && typeof member.calcMeals == 'undefined'){
                member.calcMeals = Math.floor(member.TotalHours/(job.HrsMeal||5));
            }
            if(member.MemberStatusDraftCrewTimeFlag){
                for(var t=0; t<job.Task.length; ++t){
                    var task = job.Task[t];
                    if(task.active/* && !task.JobTaskNonCompensatedFlag*/){
                        var memberJobTask = {
                            STHours: 0,
                            OTHours: 0,
                            DTHours: 0,
                            TotalHours: 0,
                            Task: task,
                            PhaseCodes: []
                        }
                        var memberTask = member.MemberTask.find(function(mt){ return mt.TaskID == task.ID })
                        if(memberTask){
                            if(!processedTaskTimes && calcOnly){
                                memberJob.StartTime = new Date(memberTask.StartTime.getTime());
                                processedTaskTimes = true;
                            }
                            memberJobTask.StartTime = memberTask.StartTime;
                            memberJobTask.EndTime = memberTask.EndTime;
                            memberJobTask.MemberTask = memberTask;
                        }
                        for(var p=0; p<task.PhaseCode.length; ++p){
                            var pc = task.PhaseCode[p];
                            if(pc.active){
                                var memberTimes = _getMemberPhaseCodeTimes(task, pc, member);
                                var memberPhaseCode = {
                                    PhaseCode: pc,
                                    STHours: memberTimes.ST,
                                    OTHours: memberTimes.OT,
                                    DTHours: memberTimes.DT,
                                    TotalHours: (memberTimes.ST+memberTimes.OT+memberTimes.DT)
                                }
                                if(!calcOnly){
                                    _processMemberTimeRules(dayMember, member, task, pc, memberPhaseCode, crewRules, workweek);
                                }
                                memberJobTask.PhaseCodes.push(memberPhaseCode);
                                if(task.perDiemEligible){
                                    memberJob.PerDiemTotal += memberPhaseCode.TotalHours;
                                }
                            }
                        }
                        memberJob.Tasks.push(memberJobTask);
                    }
                }
                for(var t=0; t<member.MemberTask.length; ++t){
                    var task = member.MemberTask[t];
                    if(task.active && !task.TaskID){
                        var jobTask = job.JobTask.find(function(jt){ return jt.PhaseCodeSectionID == task.PhaseCodeSectionID });
                        var memberJobTask = {
                            STHours: 0,
                            OTHours: 0,
                            DTHours: 0,
                            TotalHours: 0,
                            Task: task,
                            JobTask: jobTask,
                            isMember: true,
                            PhaseCodes: []
                        }
                        for(var p=0; p<task.MemberTaskPhaseCode.length; ++p){
                            var pc = task.MemberTaskPhaseCode[p];
                            if(pc.active){
                                var memberPhaseCode = {
                                    PhaseCode: pc,
                                    STHours: pc.STHours,
                                    OTHours: pc.OTHours,
                                    DTHours: pc.DTHours,
                                    TotalHours: (pc.STHours+pc.OTHours+pc.DTHours)
                                }
                                memberJobTask.PhaseCodes.push(memberPhaseCode);
                                if(pc.IncludeInOTCalcFlag){
                                    dayMember.STForOT += pc.STHours;
                                }
                                if(task.perDiemEligible){
                                    memberJob.PerDiemTotal += memberPhaseCode.TotalHours;
                                }
                            }
                        }
                        memberJob.Tasks.push(memberJobTask);
                    }
                }
                dayMember.Meals += member.Meals||0;
                dayMember.calcMeals += member.calcMeals||0;
                _changeExpenseEntryQty(member, member.Meals||0, workweek);
            }else{
                for(var te=0; te<member.TimeEntry.length; ++te){
                    member.TimeEntry[te].TimeInHours = 0;
                    member.TimeEntry[te].BillableHours = 0;
                }
                for(var mt=0; mt<member.MemberTask.length; ++mt){
                    member.MemberTask[mt].active = false;
                }
                memberJob.Tasks = []
                member.Meals = 0;
                _changeExpenseEntryQty(member, 0, workweek);
            }
            _calcMemberJobPerDiem(memberJob, dayMember, workweek, undefined, calcOnly);
        }
        var _getMembersForWeekTotals = function(workweek, jobs){
            var people = [], peopleMap = {};
            for(var j=0; j<jobs.length; ++j){
                for(var m=0; m<jobs[j].JobDayCrewMember.length; ++m){
                    var member = jobs[j].JobDayCrewMember[m];
                    if(!workweek.weekTotals[member.PersonID] || !workweek.weekTotals[member.PersonID].jobCrews || !workweek.weekTotals[member.PersonID].jobCrews[jobs[j].JobCrewID]){
                        if(!peopleMap[member.PersonID]){
                            peopleMap[member.PersonID] = true;
                            people.push(member.PersonID);
                        }
                    }else{
                        if(typeof member.PDEligible == 'undefined'){
                            Object.assign(member, workweek.weekTotals[member.PersonID].jobCrews[jobs[j].JobCrewID]);
                        }
                    }
                }
            }
            return people;
        }
        var _initMemberTime = function(workweek, scope, data, fromInit, day){
            workweek.weekTotals = workweek.weekTotals||{};
            day = day||workweek.WorkDay[workweek.currentDay];
            var jobs = _activeJobs(workweek, day.WeekdayDate), dayMembers = [], loading = false, step = _currentStep(workweek, day), loadJobContractData = false;
            var membersNeedWeekTotals = workweek.weekTotals?_getMembersForWeekTotals(workweek, jobs):null;
            if(scope.editable && day.showJobTimesOnCrew && (jobs.find(function(jb){ return !_isOverheadJob(jb,workweek) && (!jb.ContractWorkingTime && !jb.CWWorkingTime) }))){
                loadJobContractData = true;
            }
            if(!data && ((step.ProcessStepCode != 'RS'&&(!workweek.sp.jobStatus||loadJobContractData||!workweek.contractData||!workweek.contractData.jobs||jobs.reduce(function(load,jb){ return load||!workweek.contractData.jobs[jb.JobCrewJobID]||!workweek.contractData.jobs[jb.JobCrewJobID].mbLoaded},false)))||!workweek.weekTotals||(membersNeedWeekTotals&&membersNeedWeekTotals.length))){
                //If we need to load some data...do it
                $timeout(function(){
                    _loadMemberWeekData(scope, membersNeedWeekTotals, fromInit);
                });
                loading = true;
            }
            if(data){
                if(data.JobDayCrewMemberStatus){
                    _loadCrewReferenceData(workweek, scope, data);
                }
                if(data.Member){
                    _loadMemberWeekDataDone(workweek, data);
                }
                if(data.fromInit) fromInit = true;
            }
            if(!loading)_checkPricingPeriodErrors(workweek);
            var currentMemberIndex = typeof day.currentMember=="undefined"?-1:day.currentMember;
            for(var m=0; m<jobs[0].JobDayCrewMember.length; ++m){
                var dayMember = {
                    Name: jobs[0].JobDayCrewMember[m].PersonFullName,
                    Class: jobs[0].JobDayCrewMember[m].ClassClass,
                    ClassShort: jobs[0].JobDayCrewMember[m].ClassShortDescription,
                    ClassSort: jobs[0].JobDayCrewMember[m].ClassSortOrder,
                    Craft: jobs[0].JobDayCrewMember[m].CraftCraft,
                    CraftSort: jobs[0].JobDayCrewMember[m].CraftSortOrder,
                    PersonID: jobs[0].JobDayCrewMember[m].PersonID,
                    ClassID: jobs[0].JobDayCrewMember[m].ClassID,
                    Meals: 0, calcMeals: 0,
                    PerDiem: 0,
                    STHours: 0,
                    OTHours: 0,
                    DTHours: 0,
                    TotalHours: 0,
                    WeekSTHours: 0,
                    WeekOTHours: 0,
                    WeekDTHours: 0,
                    WeekPerDiem: 0,
                    WeekMeals: 0,
                    WeekTotalHours: 0,
                    HasPerDiem: false,
                    Jobs: [],
                    index: dayMembers.length
                }
                _getMemberSTForOTBaseline(dayMember, workweek);
                if(currentMemberIndex == -1 && !_shouldHideMember(jobs[0].JobDayCrewMember[m], workweek)){
                    currentMemberIndex = dayMembers.length;
                }
                dayMembers.push(dayMember);
            }
            day.currentMember = Math.max(0, currentMemberIndex);
            for(var j=0; j<jobs.length; ++j){
                var job = jobs[j];
                for(var m=0; m<job.JobDayCrewMember.length; ++m){
                    var member = job.JobDayCrewMember[m];
                    var dayMember = dayMembers.find(function(dm){ return dm.PersonID == member.PersonID && dm.ClassID == member.ClassID });
                    if(dayMember){
                        var memberJob = {
                            ID: job.JobCrewJobID,
                            Member: member,
                            Job: job,
                            Tasks: []
                        }
                        _updateMemberJob(memberJob, dayMember, workweek, fromInit||['SB','RV','AP'].includes(day.StatusCode));
                        dayMember.Jobs.push(memberJob);
                    }
                }
            }
            for(var m=0; m<dayMembers.length; ++m){
                dayMembers[m].PDSpread = _getPDSpreadStrategy(dayMembers[m], workweek);
            }
            day.members = dayMembers;
            _totalMemberTime(workweek, null, fromInit, day);
            _sortDayMembers(workweek, true);
            _calcShowTaskRows(workweek);
        }
        var _updateMemberPhaseCodeTimeEntry = function(member, pc, task, laborTypes, workweek, isMember){
            laborTypes = Utils.enforceArray(laborTypes);
            for(var i=0; i<laborTypes.length; ++i){
                var laborType = laborTypes[i];
                var teIndex = member.TimeEntry.findIndex(_findTERecord(pc.PhaseCode, task, laborType, isMember));
                if(pc[laborType+"Hours"] != 0){
                    if(teIndex < 0){
                        teIndex = member.TimeEntry.length;
                        member.TimeEntry.push(_newTimeEntry(task, pc.PhaseCode, laborType, workweek, isMember));
                    }
                    member.TimeEntry[teIndex].TimeInHours = pc[laborType+"Hours"];
                    member.TimeEntry[teIndex].BillableHours = pc[laborType+"Hours"];
                }else if(teIndex > -1){
                    member.TimeEntry[teIndex].TimeInHours = pc[laborType+"Hours"];
                    member.TimeEntry[teIndex].BillableHours = pc[laborType+"Hours"];
                }
            }
        }
        var _updatePerDiemDistribution = function(workweek){
            var dayMember = _currentMember(workweek);
            if(dayMember.HasPerDiem){
                _dividePerDiemAmounts(workweek, dayMember, dayMember.PerDiem);
            }
        }
        var _changeMemberJobHours = function(job, which, workweek){
            var data = {phases:{}}, remaining = job.Member[which+"Hours"];
            for(var t=0; t<job.Tasks.length; ++t){
                var task = job.Tasks[t];
                data.phases[task.Task.ID] = task.Task[which+"Hours"];
            }
            _recalculatePhasePercentages(-1, data);
            var total = 0;
            for(var t=0; t<job.Tasks.length; ++t){
                var task = job.Tasks[t];
                var amount = Math.round((remaining*data.phases[task.Task.ID])*4)/4;
                amount = Math.min(amount, remaining);
                remaining -= amount;
                task[which+"Hours"] = amount;
                _changeMemberTaskHours(job, task, which, workweek, true);
                _recalculatePhasePercentages(task.Task.ID, data);
                if(!task.Task.JobTaskNonCompensatedFlag){
                    total += _total(task);
                }
            }
            if(job.Job.CanMeals){
                job.Member.Meals = job.Member.calcMeals = Math.floor(total/(job.Job.HrsMeal||5));
                _changeExpenseEntryQty(job.Member, job.Member.Meals, workweek);
            }
            _changeExpenseEntryQty(job.Member, job.Member.Meals, workweek);
            _totalMemberTime(workweek, 'current');
            _updatePerDiemDistribution(workweek);
        }
        var _recalcMemberMeals = function(member, workweek){
            if(!member.Job.CanMeals) return;
            var total = 0;
            for(var t=0; t<member.Tasks.length; ++t){
                if(member.Tasks[t].Task.JobTaskNonCompensatedFlag) continue;
                for(var p=0; p<member.Tasks[t].PhaseCodes.length; ++p){
                    var pc = member.Tasks[t].PhaseCodes[p];
                    total += (pc.STHours||0)+(pc.OTHours||0)+(pc.DTHours||0);
                }
            }
            member.Member.calcMeals = member.Member.Meals = Math.floor(total / (member.Job.HrsMeal||5));
            _changeExpenseEntryQty(member.Member, member.Member.Meals, workweek);
        }
        var _changeMemberTaskHours = function(member, task, which, workweek, noTotal){
            var data = {phases:{}}, remaining = task[which+"Hours"];
            for(var p=0; p<task.PhaseCodes.length; ++p){
                var pc = task.PhaseCodes[p];
                data.phases[pc.PhaseCode.ID] = pc.PhaseCode[which+"Hours"];
            }
            _recalculatePhasePercentages(-1, data);
            for(var p=0; p<task.PhaseCodes.length; ++p){
                var pc = task.PhaseCodes[p];
                var amount = Math.round((remaining*data.phases[pc.PhaseCode.ID])*4)/4;
                amount = Math.min(amount, remaining);
                remaining -= amount;
                pc[which+"Hours"] = amount;
                _updateMemberPhaseCodeTimeEntry(member.Member, pc, task.Task, which, workweek, task.isMember);
                _recalculatePhasePercentages(pc.PhaseCode.ID, data);
            }
            if(!noTotal){
                _recalcMemberMeals(member, workweek);
                _totalMemberTime(workweek, 'current');
                _updatePerDiemDistribution(workweek);
            }
        }
        var _changeMemberPhaseHours = function(member, pc, task, which, workweek){
            pc[which+"Hours"] = pc[which+"Hours"]||0;
            _updateMemberPhaseCodeTimeEntry(member.Member, pc, task.Task, which, workweek, task.isMember);
            _recalcMemberMeals(member, workweek);
            _totalMemberTime(workweek, 'current');
            _updatePerDiemDistribution(workweek);
        }
        var _sortDayEquips = function(workweek, skipIndex){
            var day = _currentDay(workweek);
            var current = day.equipment[day.currentEquip];
            day.equipment.sort(function(a,b){
                if(a.cssClass != b.cssClass) return a.cssClass.localeCompare(b.cssClass);
                if(a.CategorySort != b.CategorySort) return a.CategorySort - b.CategorySort;
                if(a.Category != b.Category) return a.Category.localeCompare(b.Category);
                return a.Number.localeCompare(b.Number);
            });
            
            for(var e=0; e<day.equipment.length; ++e){
                day.equipment[e].index = e;
                if(!skipIndex && day.equipment[e] === current)
                    day.currentEquip = e;
            }
        }
        var _changedEquipmentStatus = function(equipJob, dayEquip, workweek){
            if(!equipJob) return;
            _setEquipWeekSoFar(dayEquip, workweek);
            for(var j=0; j<dayEquip.Jobs.length; ++j){
                var equipJob = dayEquip.Jobs[j];
                _parseTimeToOneEquipmentJob(equipJob, workweek);
                _updateEquipmentJob(equipJob, dayEquip, workweek);
            }
            _totalEquipmentTime(workweek, dayEquip);
            _sortDayEquips(workweek);
        }

        var _parseTaskToEquipment = function(task, equip, workweek){
            for(var p=0; p<task.PhaseCode.length; ++p){
                var pc = task.PhaseCode[p], te;
                var teIndex = equip.EquipmentTimeEntry.findIndex(_findTERecord(pc, task, null));
                if(teIndex == -1 && pc.active && pc.TotalHours != 0){
                    //Add new time entry
                    te = _newTimeEntry(task, pc, '', workweek);
                    equip.EquipmentTimeEntry.push(te);
                }else{
                    te = equip.EquipmentTimeEntry[teIndex];
                }
                if(te && pc.active && pc.TotalHours != 0){
                    te.updated = true;
                    te.TimeInHours = pc.TotalHours;
                    te.BillableHours = pc.TotalHours;
                }
            }
        }

        var _parseTimeToEquipment = function(equip, job, workweek){
            //Initialize tracking flag
            equip.EquipmentTimeEntry = Utils.enforceArray(equip.EquipmentTimeEntry);
            for(var te=equip.EquipmentTimeEntry.length-1; te>=0; --te){
                equip.EquipmentTimeEntry[te].updated = false;
            }
            //Parse task times
            if(equip.ActiveFlag){
                for(var t=0; t<job.Task.length; ++t){
                    var task = job.Task[t];
                    if(task.active && !task.JobTaskNonCompensatedFlag){
                        _parseTaskToEquipment(task, equip, workweek);
                    }else if(task.JobTaskNonCompensatedFlag){
                        task.showPhases = false;
                    }
                }
            }
            //Clean up
            for(var te=equip.EquipmentTimeEntry.length-1; te>=0; --te){
                if(!equip.EquipmentTimeEntry[te].updated){
                    equip.EquipmentTimeEntry[te].TimeInHours = equip.EquipmentTimeEntry[te].BillableHours = 0;
                }
            }
        }
        var _parseTimeToOneEquipmentJob = function(job, workweek){
            if(!job.Equipment.ActiveFlag) return;
            _parseTimeToEquipment(job.Equipment, job.Job, workweek);
            for(var t=0; t<job.Tasks.length; ++t){
                for(var p=0; p<job.Tasks[t].PhaseCodes.length; ++p){
                    var pc = job.Tasks[t].PhaseCodes[p];
                    var equipTimes = _getEquipmentPhaseCodeTimes(job.Tasks[t].Task, pc.PhaseCode, job.Equipment);
                    pc.TotalHours = equipTimes.TotalHours;
                }
            }
        }
        var _parseTimeToOneEquipment = function(dayEquip, workweek){
            for(var j=0; j<dayEquip.Jobs.length; ++j){
                var job = dayEquip.Jobs[j];
                _parseTimeToOneEquipmentJob(job, workweek);
            }
            _totalEquipmentTime(workweek, dayEquip);
        }
        var _clearEquipmentTime = function(dayEquip, workweek){
            for(var j=0; j<dayEquip.Jobs.length; ++j){
                var job = dayEquip.Jobs[j];
                var equip = job.Equipment;
                for(var te=equip.EquipmentTimeEntry.length-1; te>=0; --te){
                    equip.EquipmentTimeEntry[te].TimeInHours = 0;
                    equip.EquipmentTimeEntry[te].BillableHours = 0;
                }
                for(var t=0; t<job.Tasks.length; ++t){
                    for(var p=0; p<job.Tasks[t].PhaseCodes.length; ++p){
                        var pc = job.Tasks[t].PhaseCodes[p];
                        pc.TotalHours = 0;
                    }
                }
            }
            _totalEquipmentTime(workweek, dayEquip);
        }
        var _parseTimeToEquipments = function(workweek){
            // Make equip time match crew time
            var day = workweek.WorkDay[workweek.currentDay], jobs = _activeJobs(workweek);
            if(day.equipParsed) return;
            for(var j=0; j<jobs.length; ++j){
                var job = jobs[j];
                for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                    var equip = job.JobDayCrewEquipment[e];
                    _parseTimeToEquipment(equip, job, workweek);
                }
            }
            _calcShowTaskRows(workweek);
            day.equipParsed = true;
        }

        var _getEquipmentPhaseCodeTimes = function(task, pc, equip){
            var result = {TotalHours: 0};
            var te, processed = [];
            while(te = equip.EquipmentTimeEntry.find(_findTERecord(pc, task, null, false, processed))){
                result.TotalHours += te.TimeInHours;
                processed.push(te.ID);
            }
            return result;
        }

        var _mergeEquipTimeEntries = function(workweek, day){
            var jobs = _activeJobs(workweek, day?day.WeekdayDate:null);
            for(var j=0; j<jobs.length; ++j){
                for(var e=0; e<jobs[j].JobDayCrewEquipment.length; ++e){
                    var equip = jobs[j].JobDayCrewEquipment[e];

                    //First merge duplicate time
                    var timeEntries = equip.EquipmentTimeEntry.slice();
                    while(timeEntries.length){
                        var te = timeEntries.shift();
                        if(te.active){
                            var index;
                            var others = timeEntries.filter(_findTERecord({ID:te.PhaseCodeID}, {ID:te.JobDayCrewTaskID,JobTaskID:te.JobTaskID}, null, te.HighUtilizationFlag));
                            for(var i=0; i<others.length; ++i){
                                if(others[i].active){
                                    te.TimeInHours += others[i].TimeInHours;
                                    te.BillableHours += others[i].BillableHours;
                                }
                                if((index = timeEntries.findIndex(function(te2){ return te2.ID == others[i].ID})) > -1) timeEntries.splice(index, 1);
                                if((index = equip.EquipmentTimeEntry.findIndex(function(te2){ return te2.ID == others[i].ID})) > -1) equip.EquipmentTimeEntry.splice(index, 1);
                            }
                        }
                    }
                    //Now merge high utilization
                    for(var t=equip.EquipmentTimeEntry.length-1; t>=0; --t){
                        var te = equip.EquipmentTimeEntry[t];
                        if(te.active && te.HighUtilizationFlag){
                            var other = equip.EquipmentTimeEntry.find(_findTERecord({ID:te.PhaseCodeID}, {ID:te.JobDayCrewTaskID,JobTaskID:te.JobTaskID}, null, false, null, false));
                            if(other && other.active){
                                other.TimeInHours += te.TimeInHours;
                                other.BillableHours += te.BillableHours;
                            }
                            equip.EquipmentTimeEntry.splice(t, 1);
                        }
                    }
                }
            }
        }

        var _mergeMemberTimeEntries = function(workweek, day){
            var jobs = _activeJobs(workweek, day?day.WeekdayDate:null);
            for(var j=0; j<jobs.length; ++j){
                for(var m=0; m<jobs[j].JobDayCrewMember.length; ++m){
                    var member = jobs[j].JobDayCrewMember[m];
                    var timeEntries = member.TimeEntry.slice();
                    while(timeEntries.length){
                        var te = timeEntries.shift();
                        if(te.active){
                            var index;
                            var others = timeEntries.filter(_findTERecord({ID:te.PhaseCodeID}, {ID:te.JobDayCrewTaskID||te.JobDayCrewMemberTaskID,JobTaskID:te.JobTaskID}, te.LaborTypeCode, !!te.JobDayCrewMemberTaskID));
                            for(var i=0; i<others.length; ++i){
                                if(others[i].active){
                                    console.log('merging a into b', others[i], te)
                                    te.TimeInHours += others[i].TimeInHours;
                                    te.BillableHours += others[i].BillableHours;
                                }
                                if((index = timeEntries.findIndex(function(te2){ return te2.ID == others[i].ID})) > -1) timeEntries.splice(index, 1);
                                if((index = member.TimeEntry.findIndex(function(te2){ return te2.ID == others[i].ID})) > -1) member.TimeEntry.splice(index, 1);
                            }
                        }
                    }
                }
            }
        }

        var _getEquipmentWeekTotals = function(equip, workweek){
            workweek.weekEquipTotals = workweek.weekEquipTotals||{};
            var totals = {WeekTotal:0}, equipData = workweek.weekEquipTotals[equip.EquipmentID];
            //Get data from sources other than this timesheet
            if(equipData){
                Object.assign(totals, Utils.pick(equipData, 'WeekTotal'));
            }
            //Get data from this timesheet
            for(var d=0; d<7; ++d){
                if(!_shouldCountWeekTime(workweek.WorkDay[d])) continue;
                if(d == workweek.currentDay) continue;
                for(var j=0; j<workweek.WorkDay[d].JobDayCrew.length; ++j){
                    var jdc = workweek.WorkDay[d].JobDayCrew[j];
                    if(!jdc.active) continue;
                    for(var e=0; e<jdc.JobDayCrewEquipment.length; ++e){
                        var jdce = jdc.JobDayCrewEquipment[e];
                        if(jdce.EquipmentID != equip.EquipmentID) continue;
                        totals.WeekTotal += jdce.TotalHours;
                    }
                }
            }
            return totals;
        }

        var _totalEquipmentTime = function(workweek, equip, day){
            day = day||workweek.WorkDay[workweek.currentDay];
            if(!equip){
                for(var e=0; e<day.equipment.length; ++e){
                    _totalEquipmentTime(workweek, day.equipment[e], day);
                }
                return;
            }
            if(equip == 'current'){
                equip = day.equipment[day.currentEquip];
            }
            equip.cssClass = '';
            var hasActive = false;
            equip.TotalHours = equip.WeekTotalHours = 0;
            for(var j=0; j<equip.Jobs.length; ++j){
                var job = equip.Jobs[j];
                job.Equipment.TotalHours = 0;
                for(var t=0; t<job.Tasks.length; ++t){
                    var task = job.Tasks[t];
                    task.TotalHours = 0;
                    for(var p=0; p<task.PhaseCodes.length; ++p){
                        var pc = task.PhaseCodes[p];
                        task.TotalHours += pc.TotalHours;
                    }
                    job.Equipment.TotalHours += task.TotalHours;
                }
                equip.TotalHours += job.Equipment.TotalHours;
                if(job.Equipment.ActiveFlag) hasActive = true;
            }
            if(!hasActive) equip.cssClass = 'inactive';
            var week = _getEquipmentWeekTotals(equip, workweek);
            equip.WeekTotalHours = week.WeekTotal + equip.TotalHours;
        }

        var _updateEquipmentJob = function(equipJob, dayEquip, workweek){
            var equip = equipJob.Equipment, job = equipJob.Job;
            var contract = _jobContractData(job, workweek);
            if(equip.ActiveFlag){
                for(var t=0; t<job.Task.length; ++t){
                    var task = job.Task[t];
                    if(task.active && !task.JobTaskNonCompensatedFlag){
                        var equipJobTask = {
                            TotalHours: 0,
                            Task: task,
                            PhaseCodes: []
                        }
                        for(var p=0; p<task.PhaseCode.length; ++p){
                            var pc = task.PhaseCode[p];
                            if(pc.active){
                                var equipTimes = _getEquipmentPhaseCodeTimes(task, pc, equip);
                                if((contract.EquipmentTimeCap || contract.EquipmentTimeCap==0) && (dayEquip.WeekSoFar+equipTimes.TotalHours) > contract.EquipmentTimeCap){
                                    equipTimes.TotalHours = Math.max(contract.EquipmentTimeCap-dayEquip.WeekSoFar, 0);
                                }
                                var equipPhaseCode = {
                                    PhaseCode: pc,
                                    TotalHours: equipTimes.TotalHours
                                }
                                equipJobTask.PhaseCodes.push(equipPhaseCode);
                                dayEquip.WeekSoFar += equipPhaseCode.TotalHours;
                            }
                        }
                        equipJob.Tasks.push(equipJobTask);
                    }
                }
            }else{
                for(var te=0; te<equipJob.Equipment.EquipmentTimeEntry.length; ++te){
                    equipJob.Equipment.EquipmentTimeEntry[te].TimeInHours = 0;
                    equipJob.Equipment.EquipmentTimeEntry[te].BillableHours = 0;
                }
                equipJob.Tasks = []
            }
        }

        var _loadEquipWeekData = function(scope, equip){
            if(equip && equip.EquipmentID && Utils.isTempId(equip.EquipmentID)){
                return;
            }
            var jobs = {}, workDayIds = [];
            var Equipment = scope.workweek.WorkDay.reduce(function(eqs, wd){
                if(!Utils.isTempId(wd.ID)) workDayIds.push(wd.ID);
                wd.JobDayCrew.reduce(function(eqs, jb){ 
                    if(jb.active){
                        if(!jobs[jb.JobCrewJobID]) jobs[jb.JobCrewJobID] = true;
                        jb.JobDayCrewEquipment.reduce(function(eqs, e){
                            if(!equip || (Array.isArray(equip)?equip.includes(e.EquipmentID):(e.EquipmentID == equip.EquipmentID))){
                                eqs.push({ID: e.ID, EquipmentID: e.EquipmentID, JobCrewID: jb.JobCrewID });
                            }
                            return eqs;
                        }, eqs);
                    }
                    return eqs;
                }, eqs);
                return eqs;
            }, []);
            var loadJobs = Object.keys(jobs).map(function(jbId){ return {ID: jbId}}).filter(function(job){
                if(!scope.workweek.contractData || !scope.workweek.contractData.jobs || !scope.workweek.contractData.jobs[job.ID] || !scope.workweek.contractData.jobs[job.ID].eqLoaded){
                    return true;
                }
                return false;
            });
            scope.loadingDialog();
            _load(scope, 'fte/initEquipmentTime', 'POST', {WorkDay:{IDs:workDayIds.join(','),Equipment: Equipment, Job:loadJobs}, sp:(typeof scope.workweek.sp.equipmentInactiveReason=='undefined'), org:scope.workweek.PersonOrganizationID, date: Utils.date(_currentDay(scope.workweek).WeekdayDate,'MM/dd/yyyy')}, 'initEquipmentTimeResult');
        }
        var _showContractError = function(day, workweek){
            var html = 'One or more contracts for the current job(s) are missing a valid pricing period for this date.<ul>'+day.contractErrorJobs.map(function(jb){return '<li>'+jb.Number+'<span class="timesheet-ccc-badge">Contract #: '+jb.ContractNumber+'</span></li>';})+'</ul><p>Some features may not work correctly.</p>'
            _showHTMLMessage(html, "Warning");
            workweek.shownErrorContracts = true;
        }
        var _checkContractErrors = function(workweek){
            var day = _currentDay(workweek);
            if(!day.contractErrorJobs) return;
            var jobs = _activeJobs(workweek);
            for(var j=day.contractErrorJobs.length-1; j>=0; --j){
                day.contractErrorJobs[j].visible = false;
                if(jobs.find(function(jb){ return jb.JobCrewJobID == day.contractErrorJobs[j].ID})){
                    day.contractErrorJobs[j].visible = true;
                }
            }
        }
        var _checkPricingPeriodErrors = function(workweek){
            if(!workweek.contractData || !workweek.contractData.contracts) return;
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                var jobs = _activeJobs(workweek, day.WeekdayDate), jobMap = {};
                var oldJobs = Utils.copy(day.contractErrorJobs||[]).map(function(jb){ return jb.ID }), isNewJob = false;
                day.contractErrorJobs = [];
                for(var c=0; c<workweek.contractData.contracts.length; ++c){
                    var contract = workweek.contractData.contracts[c];
                    if(day.WeekdayDate >= contract.Start && day.WeekdayDate <= contract.End){
                        if(!contract.ID){
                            for(var j=0; j<contract.jobs.length; ++j){
                                var activeJob = jobs.find(function(jb){ return jb.JobCrewJobID==contract.jobs[j] && !_isOverheadJob(jb,workweek) });
                                if(activeJob && !jobMap[contract.jobs[j]]){
                                    jobMap[contract.jobs[j]] = true;
                                    day.contractErrorJobs.push({
                                        ID: contract.jobs[j],
                                        ContractNumber: workweek.contractData.jobs[contract.jobs[j]].ContractNumber,
                                        Number: activeJob.WorkgroupJobNumber,
                                        visible: true
                                    });
                                    if(!oldJobs.includes(contract.jobs[j])){
                                        isNewJob = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if(day.contractErrorJobs.length && isNewJob && d==workweek.currentDay){
                    _showContractError(day, workweek);
                }
            }
        }
        var _contractMemberEquipDone = function(data, workweek, fields){
            if(typeof fields == 'string') fields = fields.split(',')
            data.Contract = Utils.enforceArray(data.Contract);
            workweek.contractData = workweek.contractData||{};
            workweek.contractData.contracts = workweek.contractData.contracts||[];
            for(var c=0; c<data.Contract.length; ++c){
                var contract = workweek.contractData.contracts.find(function(cnt){ return (cnt.ID == data.Contract[c].ID && cnt.End.getTime()==Utils.date(data.Contract[c].End).getTime() && cnt.Start.getTime()==Utils.date(data.Contract[c].Start).getTime()) });
                data.Contract[c].Job = Utils.enforceArray(data.Contract[c].Job);
                data.Contract[c].LaborUnit = Utils.enforceArray(data.Contract[c].LaborUnit);
                if(!contract){
                    contract = {
                        ID: data.Contract[c].ID,
                        PDSync: data.Contract[c].PDSync||false,
                        jobs: []
                    }
                    workweek.contractData.contracts.push(contract);
                }
                contract.jobs = contract.jobs.concat(data.Contract[c].Job.filter(function(jb){ return !contract.jobs.find(function(cjb){return jb.ID==cjb.ID}); }).map(function(jb){ return jb.ID }));
                contract.Start = Utils.date(data.Contract[c].Start);
                contract.End = Utils.date(data.Contract[c].End);
                contract.LaborUnit = Utils.enforceArray(contract.LaborUnit);
                contract.LaborUnit = contract.LaborUnit.concat(data.Contract[c].LaborUnit.filter(function(lu){ return !contract.LaborUnit.find(function(clu){ return clu.ID==lu.ID }) }));
                workweek.contractData.jobs = workweek.contractData.jobs||{};
                for(var j=0; j<data.Contract[c].Job.length; ++j){
                    var job = workweek.contractData.jobs[data.Contract[c].Job[j].ID];
                    if(!job){
                        job = workweek.contractData.jobs[data.Contract[c].Job[j].ID] = data.Contract[c].Job[j];
                    }
                    if(data.Contract[c].Job[j].ContractNumber && !job.ContractNumber) job.ContractNumber = data.Contract[c].Job[j].ContractNumber;
                    for(var f=0; f<fields.length; ++f){
                        job[fields[f]] = true;
                    }
                }
            }
        }
        var _processEquipWeekData = function(workweek, data){
            data.Equipment = Utils.enforceArray(data.Equipment);
            workweek.weekEquipTotals = workweek.weekEquipTotals||{};
            var trackJCData = {}
            for(var m=0; m<data.Equipment.length; ++m){
                var equipData = data.Equipment[m];
                if(!workweek.weekEquipTotals[equipData.EquipmentID]) workweek.weekEquipTotals[equipData.EquipmentID] = {};
                Object.assign(workweek.weekEquipTotals[equipData.EquipmentID], Utils.pick(equipData, 'WeekTotal'));
                //Job crew related data
                for(var d=0; d<7; ++d){
                    var day = workweek.WorkDay[d];
                    for(var j=0; j<day.JobDayCrew.length; ++j){
                        var job = day.JobDayCrew[j];
                        if(!job.active) continue;
                        if(!trackJCData[job.JobCrewID]) trackJCData[job.JobCrewID] = {};
                        for(var je=0; je<job.JobDayCrewEquipment.length; ++je){
                            var equip = job.JobDayCrewEquipment[je];
                            if(equip.EquipmentID == equipData.EquipmentID){
                                if(equipData.JobCrewEquipmentID){
                                    if(!trackJCData[job.JobCrewID][equip.EquipmentID]) trackJCData[job.JobCrewID][equip.EquipmentID] = {};
                                    if(equipData.JobCrewEquipmentID != equip.JobCrewEquipmentID){
                                        trackJCData[job.JobCrewID][equip.EquipmentID].assignEquip = equip;
                                        trackJCData[job.JobCrewID][equip.EquipmentID].assignID = equipData.JobCrewEquipmentID;
                                    }else{
                                        trackJCData[job.JobCrewID][equip.EquipmentID].alreadyAssigned = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Update job crew member IDs, if needed
            var jobCrewIDs = Object.keys(trackJCData);
            for(var i=0; i<jobCrewIDs.length; ++i){
                var equipIDs = Object.keys(trackJCData[jobCrewIDs[i]]);
                for(var j=0; j<equipIDs.length; ++j){
                    var jobData = trackJCData[jobCrewIDs[i]][equipIDs[j]];
                    if(!jobData.alreadyAssigned && jobData.assignEquip){
                        jobData.assignEquip.JobCrewMemberID = jobData.assignID;
                    }
                }
            }
        }
        var _loadEquipWeekDataDone = function(workweek, data){
            _processEquipWeekData(workweek, data);
            //Do contract data
            _contractMemberEquipDone(data, workweek, 'eqLoaded');
        }

        var _setEquipWeekSoFar = function(dayEquip, workweek, equipJob){
            var totals = _getEquipmentWeekTotals(dayEquip, workweek);
            dayEquip.WeekSoFar = totals.TotalHours;

            if(equipJob){
                for(var j=0; j<dayEquip.Jobs.length; ++j){
                    if(dayEquip.Jobs[j].ID != equipJob.Job.JobCrewJobID){
                        dayEquip.WeekSoFar += dayEquip.Jobs[j].TotalHours;
                    }
                }
            }
        }

        var _getEquipsForWeekTotals = function(workweek, jobs){
            var equips = [];
            for(var e=0; e<jobs[0].JobDayCrewEquipment.length; ++e){
                var equip = jobs[0].JobDayCrewEquipment[e];
                if(!workweek.weekEquipTotals[equip.EquipmentID]){
                    equips.push(equip.EquipmentID);
                }
            }
            return equips;
        }

        var _initEquipmentTime = function(workweek, scope, data, day){
            workweek.weekTotals = workweek.weekTotals||{};
            day = day||workweek.WorkDay[workweek.currentDay];
            var jobs = _activeJobs(workweek, day.WeekdayDate), dayEquipment = [], loading = false, step = _currentStep(workweek, day);
            var equipsNeedWeekTotals = workweek.weekEquipTotals?_getEquipsForWeekTotals(workweek, jobs):null;
            if(!data && ((step.ProcessStepCode != 'RS' &&(!workweek.sp.equipmentInactiveReason||!workweek.contractData||!workweek.contractData.jobs||jobs.reduce(function(load,jb){ return load||!workweek.contractData.jobs[jb.JobCrewJobID]||!workweek.contractData.jobs[jb.JobCrewJobID].eqLoaded},false)))||!workweek.weekEquipTotals||(equipsNeedWeekTotals&&equipsNeedWeekTotals.length))){
                //If we need to load some data...do it
                $timeout(function(){
                    _loadEquipWeekData(scope, equipsNeedWeekTotals);
                });
                loading = true;
            }
            if(data){
                if(data.EquipmentInactiveReason){
                    _loadCrewReferenceData(workweek, scope, data);
                }
                if(data.Equipment){
                    _loadEquipWeekDataDone(workweek, data);
                }
            }
            if(!loading)_checkPricingPeriodErrors(workweek);
            var currentEquipIndex = typeof day.currentEquip=="undefined"?-1:day.currentEquip;
            for(var e=0; e<jobs[0].JobDayCrewEquipment.length; ++e){
                var dayEquip = {
                    Number: jobs[0].JobDayCrewEquipment[e].EquipmentEquipmentNumber,
                    Description: jobs[0].JobDayCrewEquipment[e].EquipmentDescription,
                    Category: jobs[0].JobDayCrewEquipment[e].CategoryCategory,
                    CategorySort: jobs[0].JobDayCrewEquipment[e].CategorySortOrder,
                    TypeCode: jobs[0].JobDayCrewEquipment[e].EquipmentTypeCode,
                    EquipmentID: jobs[0].JobDayCrewEquipment[e].EquipmentID,
                    TotalHours: 0,
                    WeekTotalHours: 0,
                    Jobs: [],
                    index: dayEquipment.length
                }
                _setEquipWeekSoFar(dayEquip, workweek);
                if(currentEquipIndex == -1 && !_shouldHideEquip(jobs[0].JobDayCrewEquipment[e])){
                    currentEquipIndex = dayEquipment.length;
                }
                dayEquipment.push(dayEquip);
            }
            day.currentEquip = Math.max(0, currentEquipIndex);
            for(var j=0; j<jobs.length; ++j){
                var job = jobs[j];
                for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                    var equip = job.JobDayCrewEquipment[e];
                    var dayEquip = dayEquipment.find(function(de){ return de.EquipmentID == equip.EquipmentID });
                    if(dayEquip){
                        var equipJob = {
                            ID: job.JobCrewJobID,
                            Equipment: equip,
                            Job: job,
                            Tasks: []
                        }
                        _updateEquipmentJob(equipJob, dayEquip, workweek);
                        dayEquip.Jobs.push(equipJob);
                    }
                }
            }
            day.equipment = dayEquipment;
            _totalEquipmentTime(workweek, null, day);
            _sortDayEquips(workweek, true);
            _calcShowTaskRows(workweek);
        }

        var _updateEquipmentPhaseCodeTimeEntry = function(equip, pc, task, workweek){
            var teIndex = equip.EquipmentTimeEntry.findIndex(_findTERecord(pc.PhaseCode, task, null));
            if(pc.TotalHours != 0){
                if(teIndex < 0){
                    teIndex = equip.EquipmentTimeEntry.length;
                    equip.EquipmentTimeEntry.push(_newTimeEntry(task, pc.PhaseCode, '', workweek));
                }
                equip.EquipmentTimeEntry[teIndex].TimeInHours = pc.TotalHours;
                equip.EquipmentTimeEntry[teIndex].BillableHours = pc.TotalHours;
            }else if(teIndex > -1){
                equip.EquipmentTimeEntry[teIndex].TimeInHours = pc.TotalHours;
                equip.EquipmentTimeEntry[teIndex].BillableHours = pc.TotalHours;
            }
        }

        var _changeEquipJobHours = function(job, workweek){
            var data = {phases:{}}, remaining = job.Equipment.TotalHours;
            for(var t=0; t<job.Tasks.length; ++t){
                var task = job.Tasks[t];
                data.phases[task.Task.ID] = task.Task.TotalHours;
            }
            _recalculatePhasePercentages(-1, data);
            for(var t=0; t<job.Tasks.length; ++t){
                var task = job.Tasks[t];
                var amount = Math.round((remaining*data.phases[task.Task.ID])*4)/4;
                amount = Math.min(amount, remaining);
                remaining -= amount;
                task.TotalHours = amount;
                _changeEquipTaskHours(job.Equipment, task, workweek);
                _recalculatePhasePercentages(task.Task.ID, data);
            }
            _totalEquipmentTime(workweek, 'current');
        }
        var _changeEquipTaskHours = function(equip, task, workweek){
            var data = {phases:{}}, remaining = task.TotalHours;
            for(var p=0; p<task.PhaseCodes.length; ++p){
                var pc = task.PhaseCodes[p];
                data.phases[pc.PhaseCode.ID] = pc.PhaseCode.TotalHours;
            }
            _recalculatePhasePercentages(-1, data);
            for(var p=0; p<task.PhaseCodes.length; ++p){
                var pc = task.PhaseCodes[p];
                var amount = Math.round((remaining*data.phases[pc.PhaseCode.ID])*4)/4;
                amount = Math.min(amount, remaining);
                remaining -= amount;
                pc.TotalHours = amount;
                _updateEquipmentPhaseCodeTimeEntry(equip, pc, task.Task, workweek);
                _recalculatePhasePercentages(pc.PhaseCode.ID, data);
            }
            _totalEquipmentTime(workweek, 'current');
        }
        var _changeEquipPhaseHours = function(equip, pc, task, workweek){
            pc.TotalHours = pc.TotalHours||0;
            _updateEquipmentPhaseCodeTimeEntry(equip, pc, task.Task, workweek);
            _totalEquipmentTime(workweek, 'current');
        }
        var _loadWeekSummaryData = function(scope, members, equips){
            var jobs = {};
            var Member = scope.workweek.WorkDay.reduce(function(ms, wd){
                wd.JobDayCrew.reduce(function(ms, jb){ 
                    if(jb.active){
                        if(!jobs[jb.JobCrewJobID]) jobs[jb.JobCrewJobID] = true;
                        jb.JobDayCrewMember.reduce(function(ms, m){
                            if(!members || members.includes(m.PersonID)){
                                ms.push({ID: m.ID, PersonID: m.PersonID, ClassID: m.ClassID, JobCrewID: jb.JobCrewID });
                            }
                            return ms;
                        }, ms);
                    }
                    return ms;
                }, ms);
                return ms;
            }, []);
            var Equipment = scope.workweek.WorkDay.reduce(function(eqs, wd){
                wd.JobDayCrew.reduce(function(eqs, jb){ 
                    if(jb.active){
                        if(!jobs[jb.JobCrewJobID]) jobs[jb.JobCrewJobID] = true;
                        jb.JobDayCrewEquipment.reduce(function(eqs, e){
                            if(!equips || equips.includes(e.EquipmentID)){
                                eqs.push({ID: e.ID, EquipmentID: e.EquipmentID, JobCrewID: jb.JobCrewID });
                            }
                            return eqs;
                        }, eqs);
                    }
                    return eqs;
                }, eqs);
                return eqs;
            }, []);
            var loadJobs = Object.keys(jobs).map(function(jbId){ return {ID: jbId}}).filter(function(job){
                if(!scope.workweek.contractData || !scope.workweek.contractData.jobs || !scope.workweek.contractData.jobs[job.ID] || !scope.workweek.contractData.jobs[job.ID].mbLoaded){
                    return true;
                }
                return false;
            });
            var sp = !scope.workweek.sp.equipmentInactiveReason||!scope.workweek.sp.memberInactiveReason;
            scope.loadingDialog();
            _load(scope, 'fte/initSummary', 'POST', {WorkDay:{Member: Member, Equipment: Equipment, Job:loadJobs}, org:scope.workweek.PersonOrganizationID, date: Utils.date(_currentDay(scope.workweek).WeekdayDate,'MM/dd/yyyy'), sp:sp}, 'initSummaryResult');
        }
        var _loadWeekSummaryDataDone = function(workweek, data){
            _processMemberWeekData(workweek, data);
            _processEquipWeekData(workweek, data);
            _contractMemberEquipDone(data, workweek, 'mbLoaded,eqLoaded');
        }
        var _calcMemberEquipJobWeekTotals = function(workweek){
            workweek.memberJobWeekTotals = {};
            workweek.equipJobWeekTotals = {};
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                for(var j=0; j<day.JobDayCrew.length; ++j){
                    if(!day.JobDayCrew[j].active) continue;
                    var job = day.JobDayCrew[j];
                    for(var m=0; m<job.JobDayCrewMember.length; ++m){
                        var member = job.JobDayCrewMember[m];
                        if(!workweek.memberJobWeekTotals[member.PersonID]) workweek.memberJobWeekTotals[member.PersonID] = {};
                        if(!workweek.memberJobWeekTotals[member.PersonID][member.ClassID]) workweek.memberJobWeekTotals[member.PersonID][member.ClassID] = {};
                        if(!workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID]) workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID] = {WeekSTHours:0,WeekOTHours:0,WeekDTHours:0,WeekTotalHours:0,WeekPerDiem:0,WeekMeals:0,active:false}
                        workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].WeekSTHours += member.STHours;
                        workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].WeekOTHours += member.OTHours;
                        workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].WeekDTHours += member.DTHours;
                        workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].WeekTotalHours += member.TotalHours;
                        workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].WeekPerDiem += member.PerDiemAmount;
                        workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].WeekMeals += member.Meals;
                        workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].active = workweek.memberJobWeekTotals[member.PersonID][member.ClassID][job.JobCrewJobID].active || member.ActiveFlag;
                    }
                    for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                        var equip = job.JobDayCrewEquipment[e];
                        if(!workweek.equipJobWeekTotals[equip.EquipmentID]) workweek.equipJobWeekTotals[equip.EquipmentID] = {};
                        if(!workweek.equipJobWeekTotals[equip.EquipmentID][job.JobCrewJobID]) workweek.equipJobWeekTotals[equip.EquipmentID][job.JobCrewJobID] = {WeekTotalHours:0,active:false};
                        workweek.equipJobWeekTotals[equip.EquipmentID][job.JobCrewJobID].WeekTotalHours += equip.TotalHours;
                        workweek.equipJobWeekTotals[equip.EquipmentID][job.JobCrewJobID].active = workweek.equipJobWeekTotals[equip.EquipmentID][job.JobCrewJobID].active || equip.ActiveFlag;
                    }
                }

            }
        }
        var _calcShowSummaryRows = function(workweek, which){
            var day = workweek.WorkDay[workweek.currentDay];
            if(!which || which.indexOf('Person') > -1){
                var isShowingSummaryJobsPerson = false, isShowingSummaryTasksPerson = false, isShowingSummaryPhasesPerson = false;
                for(var p=0; p<day.memberSummary.length; ++p){
                    var person = day.memberSummary[p], isShowingPersonJobs = false, isShowingPersonTasks = false, isShowingPersonPhases = false;
                    for(var c=0; c<person.Classes.length; ++c){
                        var classRows = 0;
                        if(p == day.memberSummary.length-1 && c == person.Classes.length-1){
                            person.Classes[c].isLast = true;
                        }else{
                            person.Classes[c].isLast = false;
                        }
                        if(person.Classes[c].showJobs){
                            isShowingPersonJobs = true;
                            for(var j=0; j<person.Classes[c].Member.Jobs.length; ++j){
                                var jb=person.Classes[c].Member.Jobs[j], isShowingJobTasks = false, isShowingJobPhases = false, jobRows = 0;
                                if(!jb.Member.ActiveFlag && !workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID].active) continue;
                                if(j > 0){
                                    person.Classes[c].Member.Jobs[j-1].isLast = false
                                }
                                jb.isLast = true;
                                classRows++;
                                if(workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID].showTasks){
                                    isShowingJobTasks = true;
                                    for(var t = 0; t<jb.Tasks.length; ++t){
                                        var tsk = jb.Tasks[t].Task, isShowingTaskPhases = false;
                                        if(!tsk.active||jb.Tasks[t].TotalHours == 0) continue;
                                        if(t > 0){
                                            jb.Tasks[t-1].isLast = false
                                        }
                                        jb.Tasks[t].isLast = true;
                                        jobRows++;
                                        classRows++;
                                        if(workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID].Tasks&&workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID].Tasks[tsk.ID]){
                                            isShowingTaskPhases = true;
                                            for(var pc=0; pc<jb.Tasks[t].PhaseCodes.length; ++pc){
                                                if(jb.Tasks[t].PhaseCodes[pc].PhaseCode.active && jb.Tasks[t].PhaseCodes[pc].TotalHours){
                                                    jobRows++;
                                                    classRows++;
                                                }
                                            }
                                        }
                                        isShowingJobPhases = isShowingJobPhases||isShowingTaskPhases;
                                        workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID].showPhases = isShowingTaskPhases;
                                    }
                                    workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID].rows = jobRows;
                                }
                                person.Classes[c].showTasks = isShowingJobTasks;
                                person.Classes[c].showPhases = isShowingJobPhases;
                                isShowingPersonTasks = isShowingPersonTasks||isShowingJobTasks;
                                isShowingPersonPhases = isShowingPersonPhases||isShowingJobPhases;
                            }
                        }
                        person.Classes[c].rows = classRows;
                    }
                    person.showJobs = isShowingPersonJobs;
                    person.showTasks = isShowingPersonTasks;
                    person.showPhases = isShowingPersonPhases;
                    isShowingSummaryJobsPerson = isShowingSummaryJobsPerson||isShowingPersonJobs;
                    isShowingSummaryTasksPerson = isShowingSummaryTasksPerson||isShowingPersonTasks;
                    isShowingSummaryPhasesPerson = isShowingSummaryPhasesPerson||isShowingPersonPhases;
                }
                day.isShowingSummaryJobsPerson = isShowingSummaryJobsPerson;
                day.isShowingSummaryTasksPerson = isShowingSummaryTasksPerson;
                day.isShowingSummaryPhasesPerson = isShowingSummaryPhasesPerson;
            }
            if(!which || which.indexOf('SM') > -1 || which.indexOf('SR') > -1){
                var isShowingSummaryJobsEquip = {SM:false,SR:false}, isShowingSummaryTasksEquip = {SM:false,SR:false}, isShowingSummaryPhasesEquip = {SM:false,SR:false}, lastIndex = {SM:-1,SR:-1}
                for(var e=0; e<day.equipSummary.length; ++e){
                    var equip = day.equipSummary[e], equipRows = 0, isShowingEquipJobs = false, isShowingEquipTasks = false, isShowingEquipPhases = false;
                    equip.isLast = false;
                    lastIndex[equip.Equip.TypeCode] = e;
                    if((!which || which.includes(equip.Equip.TypeCode)) && equip.showJobs){
                        isShowingEquipJobs = true;
                        
                        for(var j=0; j<equip.Equip.Jobs.length; ++j){
                            var jb=equip.Equip.Jobs[j], isShowingJobTasks = false, isShowingJobPhases = false, jobRows = 0;
                            if(j > 0){
                                equip.Equip.Jobs[j-1].isLast = false;
                            }
                            if(!jb.Equipment.ActiveFlag && !workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID].active) continue;
                            jb.isLast = true;
                            equipRows++;
                            if(workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID].showTasks){
                                isShowingJobTasks = true;
                                for(var t = 0; t<jb.Tasks.length; ++t){
                                    var tsk = jb.Tasks[t].Task, isShowingTaskPhases = false;
                                    if(t > 0){
                                        jb.Tasks[t-1].isLast = false;
                                    }
                                    if(!tsk.active||tsk.Duration == 0) continue;
                                    jb.Tasks[t].isLast = true;
                                    jobRows++;
                                    equipRows++;
                                    if(workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID].Tasks&&workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID].Tasks[tsk.ID]){
                                        isShowingTaskPhases = true;
                                        for(var pc=0; pc<jb.Tasks[t].PhaseCodes.length; ++pc){
                                            if(jb.Tasks[t].PhaseCodes[pc].PhaseCode.active && jb.Tasks[t].PhaseCodes[pc].TotalHours){
                                                jobRows++;
                                                equipRows++;
                                            }
                                        }
                                    }
                                    isShowingJobPhases = isShowingJobPhases||isShowingTaskPhases;
                                    workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID].showPhases = isShowingTaskPhases;
                                }
                                workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID].rows = jobRows;
                            }
                            equip.showTasks = isShowingJobTasks;
                            equip.showPhases = isShowingJobPhases;
                            isShowingEquipTasks = isShowingEquipTasks||isShowingJobTasks;
                            isShowingEquipPhases = isShowingEquipPhases||isShowingJobPhases;
                        }
                        equip.rows = equipRows;
                    }
                    isShowingSummaryJobsEquip[equip.Equip.TypeCode] = isShowingSummaryJobsEquip[equip.Equip.TypeCode]||isShowingEquipJobs;
                    isShowingSummaryTasksEquip[equip.Equip.TypeCode] = isShowingSummaryTasksEquip[equip.Equip.TypeCode]||isShowingEquipTasks;
                    isShowingSummaryPhasesEquip[equip.Equip.TypeCode] = isShowingSummaryPhasesEquip[equip.Equip.TypeCode]||isShowingEquipPhases;
                }

                if(!which || which.indexOf('SM') > -1){
                    day.isShowingSummaryJobsEquipSM = isShowingSummaryJobsEquip.SM;
                    day.isShowingSummaryTasksEquipSM = isShowingSummaryTasksEquip.SM;
                    day.isShowingSummaryPhasesEquipSM = isShowingSummaryPhasesEquip.SM;
                }
                if(!which || which.indexOf('SR') > -1){
                    day.isShowingSummaryJobsEquipSR = isShowingSummaryJobsEquip.SR;
                    day.isShowingSummaryTasksEquipSR = isShowingSummaryTasksEquip.SR;
                    day.isShowingSummaryPhasesEquipSR = isShowingSummaryPhasesEquip.SR;
                }
                if(lastIndex.SM > -1)day.equipSummary[lastIndex.SM].isLast = true;
                if(lastIndex.SR > -1)day.equipSummary[lastIndex.SR].isLast = true;
            }
            if(!which || which.indexOf('Crew') > -1){
                var jobs = _activeJobs(workweek, 'all');
                var isShowingSummaryJobTasks = false, isShowingSummaryPhases = false;
                for(var j=0; j<jobs.length; ++j){
                    if(!jobs[j].active) continue;
                    var jobRows = 0;
                    if(jobs[j].showSummaryTasks){
                        for(var t=0; t<jobs[j].Task.length; ++t){
                            if(!jobs[j].Task[t].active) continue;
                            var taskRows = 0;
                            jobRows++;    
                            isShowingSummaryPhases = isShowingSummaryPhases ||jobs[j].Task[t].showSummaryPhases;
                            if(jobs[j].Task[t].showSummaryPhases){
                                for(var p=0; p<jobs[j].Task[t].PhaseCode.length; ++p){
                                    if(jobs[j].Task[t].PhaseCode[p].active){
                                        jobRows++;
                                        taskRows++;
                                    }
                                }
                                jobs[j].Task[t].rowspan = taskRows;
                                if(taskRows) ++jobRows;
                            }
                        }
                    }
                    isShowingSummaryJobTasks = isShowingSummaryJobTasks||jobs[j].showSummaryTasks;
                    jobs[j].rowspan = jobRows;
                }
                day.isShowingSummaryJobTasks = isShowingSummaryJobTasks;
                day.isShowingSummaryPhases = isShowingSummaryPhases;
            }
        }
        var _dayPersonPropCompare = ['STHours','OTHours','DTHours','TotalHours','PerDiem','WeekSTHours','WeekOTHours','WeekDTHours','WeekTotalHours','WeekPerDiem'];
        var _shouldCountWeekTime = function(day){
            return day.currentStep >= 3 || ['SU','RV','AP'].includes(day.StatusCode);
        }
        var _initSummary = function(workweek, scope, data){
            var jobs = _activeJobs(workweek, 'all');
            if(!data){
                var membersNeedWeekTotals = workweek.weekTotals?_getMembersForWeekTotals(workweek, jobs):null;
                var equipsNeedWeekTotals = workweek.weekEquipTotals?_getEquipsForWeekTotals(workweek, jobs):null;
                if(!workweek.weekTotals || !workweek.weekEquipTotals || (membersNeedWeekTotals && membersNeedWeekTotals.length) || (equipsNeedWeekTotals && equipsNeedWeekTotals.length)){
                    _loadWeekSummaryData(scope, membersNeedWeekTotals, equipsNeedWeekTotals);
                    return;
                }
            }
            if(data){
                _loadWeekSummaryDataDone(workweek, data);
            }
            var dayTasks = [], day = _currentDay(workweek);
            // Total the members and equipment
            _initMemberTime(workweek, scope, data, true);
            _initEquipmentTime(workweek, scope, data);

            //Set up the person structure
            var dayPeople = {}, dayEquipment = {}, even = true, days = [0,1,2,3,4,5,6]
            days.unshift(days.splice(workweek.currentDay, 1)[0]);
            day.memberSummary = [], day.equipSummary = [];
            day.hasMemberValue = {day:{st:0,ot:0,dt:0,pd:0,me:0},week:{st:0,ot:0,dt:0,pd:0,me:0}}
            for(var dayIndex=0; dayIndex<days.length; ++dayIndex){
                var d = days[dayIndex];
                var weekDay = workweek.WorkDay[d];
                if(_shouldCountWeekTime(weekDay)){
                    //Do we need to generate the member list?
                    if(!weekDay.members){
                        _initMemberTime(workweek, scope, null, true, weekDay);
                    }
                    //Do we need to generate the equip list?
                    if(!weekDay.equipment){
                        _initEquipmentTime(workweek, scope, null, weekDay);
                    }
                }else{ //Not far enough along to bother with members/equip
                    continue;
                }
                //Look at each member
                for(var m=0; m<weekDay.members.length; ++m){
                    var dayMember = weekDay.members[m];
                    if(!dayMember.Jobs.length) continue;
                    if(!_shouldHideMember(dayMember.Jobs[0].Member, workweek)){
                        if(!dayPeople[dayMember.PersonID]){
                            dayPeople[dayMember.PersonID] = { 
                                PersonID: dayMember.PersonID,
                                Name: dayMember.Name,
                                STHours: 0,
                                OTHours: 0,
                                DTHours: 0,
                                TotalHours: 0,
                                PerDiem: 0,
                                Meals: '',
                                HasPerDiem: false,
                                WeekSTHours: 0,
                                WeekOTHours: 0,
                                WeekDTHours: 0,
                                WeekTotalHours: 0,
                                WeekPerDiem: 0,
                                WeekMeals: '',
                                HasWeekPerDiem: false,
                                cssClass: 'inactive',
                                Classes: {},
                                ClassSort: 1000000000,
                                showJobs: false,
                                isToday: false
                            }
                            day.memberSummary.push(dayPeople[dayMember.PersonID]);
                        }
                        var dayPerson = dayPeople[dayMember.PersonID];
                        var isNewClass = !dayPerson.Classes[dayMember.ClassID];
                        if(isNewClass){
                            dayPerson.Classes[dayMember.ClassID] = {
                                Member:dayMember,
                                showJobs:false,
                                isToday:false
                            }
                        }
                        var dayPersonClass = dayPerson.Classes[dayMember.ClassID];
                        if(dayMember.ClassSort < dayPerson.ClassSort) dayPerson.ClassSort = dayMember.ClassSort;
                        if(d==workweek.currentDay){
                            dayPerson.STHours += dayMember.STHours;
                            dayPerson.OTHours += dayMember.OTHours;
                            dayPerson.DTHours += dayMember.DTHours;
                            dayPerson.TotalHours += dayMember.TotalHours;
                            dayPerson.PerDiem += dayMember.PerDiem;
                            dayPerson.Meals = dayMember.Meals?((dayPerson.Meals||0)+dayMember.Meals):dayPerson.Meals;
                            dayPerson.HasPerDiem = dayPerson.HasPerDiem||dayMember.HasPerDiem;
                            if(dayMember.cssClass != 'inactive') dayPerson.cssClass = '';
                            dayPerson.isToday = true;
                            dayPersonClass.isToday = true;
                            day.hasMemberValue.day.st += dayMember.STHours;
                            day.hasMemberValue.day.ot += dayMember.OTHours;
                            day.hasMemberValue.day.dt += dayMember.DTHours;
                            day.hasMemberValue.day.pd += dayMember.PerDiem;
                            day.hasMemberValue.day.me += dayMember.Meals;
                        }
                        if(isNewClass){
                            dayPerson.WeekSTHours += dayMember.ClassSTHours;
                            dayPerson.WeekOTHours += dayMember.ClassOTHours;
                            dayPerson.WeekDTHours += dayMember.ClassDTHours;
                            dayPerson.WeekTotalHours += dayMember.ClassTotalHours;
                            dayPerson.WeekPerDiem = dayMember.WeekPerDiem;
                            dayPerson.WeekMeals = dayMember.WeekMeals;
                            dayPerson.HasWeekPerDiem = dayPerson.HasWeekPerDiem||dayMember.HasPerDiem;
                            day.hasMemberValue.week.st += dayMember.ClassSTHours;
                            day.hasMemberValue.week.ot += dayMember.ClassOTHours;
                            day.hasMemberValue.week.dt += dayMember.ClassDTHours;
                            day.hasMemberValue.week.pd += dayMember.WeekPerDiem;
                            day.hasMemberValue.week.me += dayMember.WeekMeals;
                        }
                        
                        if(dayMember.cssClass != 'inactive' && !dayPerson.isToday) dayPerson.cssClass = '';
                    }
                }
                
                //Look at each equip
                for(var e=0; e<weekDay.equipment.length; ++e){
                    var dayEquip = weekDay.equipment[e];
                    if(!dayEquip.Jobs.length) continue;
                    if(!_shouldHideEquip(dayEquip.Jobs[0].Equipment)){
                        if(!dayEquipment[dayEquip.EquipmentID]){
                            dayEquipment[dayEquip.EquipmentID] = { 
                                Equip: dayEquip,
                                isToday: false
                            }
                            day.equipSummary.push(dayEquipment[dayEquip.EquipmentID]);
                        }
                        if(d==workweek.currentDay){
                            dayEquipment[dayEquip.EquipmentID].Equip = dayEquip;
                            dayEquipment[dayEquip.EquipmentID].isToday = true;
                        }
                    }
                }
            }
            day.hasMemberValue.weekColCount = 1
            if(day.hasMemberValue.week.pd&&!workweek.HidePerDiem){day.hasMemberValue.weekColCount++;}
            if(day.hasMemberValue.week.st){day.hasMemberValue.weekColCount++;}
            if(day.hasMemberValue.week.ot){day.hasMemberValue.weekColCount++;}
            if(day.hasMemberValue.week.dt){day.hasMemberValue.weekColCount++;}
            if(day.hasMemberValue.week.me){day.hasMemberValue.weekColCount++;}
            day.hasMemberValue.colCount = 1
            if(day.hasMemberValue.day.pd&&!workweek.HidePerDiem){day.hasMemberValue.colCount++;}
            if(day.hasMemberValue.day.st){day.hasMemberValue.colCount++;}
            if(day.hasMemberValue.day.ot){day.hasMemberValue.colCount++;}
            if(day.hasMemberValue.day.dt){day.hasMemberValue.colCount++;}
            if(day.hasMemberValue.day.me){day.hasMemberValue.colCount++;}

            day.memberSummary.sort(_dayMemberSortComparator);
            var keys = Object.keys(dayPeople);
            for(var p=0; p<day.memberSummary.length; ++p){
                var dayPerson = day.memberSummary[p];
                dayPerson.Classes = Object.keys(dayPerson.Classes).map(function(clssId){
                    return dayPerson.Classes[clssId];
                }).sort(function(a,b){
                    return _dayMemberSortComparator(a.Member, b.Member);
                });
                even = !even;
                dayPerson.inactive = dayPerson.cssClass=='inactive';
                if(even) dayPerson.cssClass += ' alt';
            }
            // Make sure task grid is initialized
            day.isShowingSummaryJobTasks = false;
            for(var j=0; j<jobs.length; ++j){
                jobs[j].showSummaryTasks = false;
            }
            _calcMemberEquipJobWeekTotals(workweek);
            _calcShowSummaryRows(workweek)
        }

        var _viewMemberWeekCheckCurrentTimesheet = function(members, jdcmId, workweek, data, classMap, jobMap, crewMap, workDay, currentMember){
            if(!members) return;
            var member = members.find(function(mbr){ return mbr.ID == jdcmId });
            if(member || currentMember){
                if(currentMember){
                    if(member || !currentMember.ActiveFlag) return;
                    member = currentMember;
                }
                
                if(!classMap[member.ClassID]){
                    classMap[member.ClassID] = true;
                    data.Class.push({ID: member.ClassID, Class:member.ClassClass });
                }
                var jobData = workDay.JobDayCrew[member.jobIndex];
                if(!jobMap[jobData.JobCrewJobID]){
                    jobMap[jobData.JobCrewJobID] = true;
                    workweek.weekData.Job.push({ID: jobData.JobCrewJobID, Number: jobData.WorkgroupJobNumber });
                }
                if(!crewMap[workweek.CrewID]){
                    crewMap[workweek.CrewID] = true;
                    workweek.weekData.Crew.push({ID: workweek.CrewID, Name: workweek.CrewName });
                }
                var PD = 0, ME = 0;
                for(var i=0; i<member.ExpenseEntry.length; ++i){
                    if(member.ExpenseEntry[i].TypeCode == 'PD'){
                        PD += member.ExpenseEntry[i].Amount;
                    }else if(member.ExpenseEntry[i].TypeCode == 'ME'){
                        ME += member.ExpenseEntry[i].Qty;
                    }
                }
                return {
                    JobID: jobData.JobCrewJobID,
                    ClassID: member.ClassID,
                    CrewID: workweek.CrewID,
                    PerDiem: PD,
                    Meals: ME,
                    STHours: member.STHours,
                    OTHours: member.OTHours,
                    DTHours: member.DTHours
                }
            }
        }

        var _viewMemberWeekResult = function(workweek, allClasses, classId){
            workweek.weekData.Job = Utils.enforceArray(workweek.weekData.Job);
            workweek.weekData.D = Utils.enforceArray(workweek.weekData.D);
            workweek.weekData.Crew = Utils.enforceArray(workweek.weekData.Crew);
            var data = {Class: Utils.enforceArray(workweek.weekData.Class),Days:[], Total:0, Meals: false};
            var classMap = data.Class.reduce(function(map, clss){  map[clss.ID] = true; return map; }, {}), existingJobMap = workweek.weekData.Job.reduce(function(map, jb){ map[jb.ID] = true; return map; }, {}), existingCrewMap = workweek.weekData.Crew.reduce(function(map, crew){  map[crew.ID] = true; return map; }, {});
            for(var d=0; d<workweek.weekData.D.length; ++d){ 
                var day = {Date: Utils.date(workweek.weekData.D[d].Date),ME:0,PD:0,ST:0,OT:0,DT:0,Total:0,Expanded:false,Jobs:[]};
                var jobMap = {}, crewMap = {}, workDay = workweek.WorkDay.find(function(wd){ return wd.WeekdayDate.getTime() == day.Date.getTime()}), checkJDCMs;
                 workweek.weekData.D[d].JDCM = Utils.enforceArray(workweek.weekData.D[d].JDCM);
                if(workDay){
                    checkJDCMs = _activeJobs(workweek, workDay.WeekdayDate).reduce(function(arry, jb){ for(var m=0; m<jb.JobDayCrewMember.length; ++m){ if(jb.JobDayCrewMember[m].PersonID==workweek.weekData.ID){jb.JobDayCrewMember[m].jobIndex=jb.index;arry.push(jb.JobDayCrewMember[m])}}; return arry;}, []);
                    for(var m=0; m<checkJDCMs.length; ++m){
                        var memberData = _viewMemberWeekCheckCurrentTimesheet(workweek.weekData.D[d].JDCM, checkJDCMs[m].ID, workweek, data, classMap, existingJobMap, existingCrewMap, workDay, checkJDCMs[m])
                        if(!memberData) continue;
                        if(allClasses || memberData.ClassID == classId){
                            var job = jobMap[memberData.JobID];
                            if(!job){
                                var jobData = workweek.weekData.Job.find(function(jb){ return jb.ID == memberData.JobID});
                                job = {Number: jobData.Number,ME:0,PD:0,ST:0,OT:0,DT:0,Total:0,Crews:[]};
                                workweek.weekData.D[d].Override = Utils.enforceArray(workweek.weekData.D[d].Override);
                                if(workweek.weekData.D[d].Override.length){
                                   job.Number += '('+workweek.weekData.D[d].Override.filter(function(ovr){ return ovr.JobID == memberData.JobID}).join(', ')+')';
                                }
                                jobMap[memberData.JobID] = job;
                                day.Jobs.push(job);
                            }
                            job.Style = {'font-weight':'bold'};
                            var crewKey = memberData.JobID+'_'+memberData.CrewID
                            var crew = crewMap[crewKey];
                            if(!crew){
                                var crewData = workweek.weekData.Crew.find(function(c){ return c.ID == memberData.CrewID});
                                crew = {ID:crewData.ID,Name:crewData.Name,ME:0,PD:0,ST:0,OT:0,DT:0,Total:0,Style:crewData.ID==workweek.CrewID?{'font-weight':'bold'}:{}};
                                crewMap[crewKey] = crew;
                                job.Crews.push(crew);
                            }
                            crew.Style = {'font-weight':'bold'}
                            crew.PD += memberData.PerDiem;
                            crew.ST += memberData.STHours;
                            crew.OT += memberData.OTHours;
                            crew.DT += memberData.DTHours;
                            crew.ME += memberData.Meals;
                            crew.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                            job.PD += memberData.PerDiem;
                            job.ST += memberData.STHours;
                            job.OT += memberData.OTHours;
                            job.DT += memberData.DTHours;
                            job.ME += memberData.Meals;
                            job.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                            day.PD += memberData.PerDiem;
                            day.ST += memberData.STHours;
                            day.OT += memberData.OTHours;
                            day.DT += memberData.DTHours;
                            day.ME += memberData.Meals;
                            day.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                            data.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                            data.Meals = data.Meals||day.ME>0;
                        }
                    }
                }
                for(var m=0; m<workweek.weekData.D[d].JDCM.length; ++m){
                    var memberData = _viewMemberWeekCheckCurrentTimesheet(checkJDCMs, workweek.weekData.D[d].JDCM[m].ID, workweek, data, classMap, existingJobMap, existingCrewMap, workDay)
                                  || workweek.weekData.D[d].JDCM[m];
                    if(allClasses || memberData.ClassID == classId){
                        var job = jobMap[memberData.JobID];
                        if(!job){
                            var jobData = workweek.weekData.Job.find(function(jb){ return jb.ID == memberData.JobID});
                            job = {Number: jobData.Number,ME:0,PD:0,ST:0,OT:0,DT:0,Total:0,Crews:[]};
                            workweek.weekData.D[d].Override = Utils.enforceArray(workweek.weekData.D[d].Override);
                            if(workweek.weekData.D[d].Override.length){
                               job.Number += '('+workweek.weekData.D[d].Override.filter(function(ovr){ return ovr.JobID == memberData.JobID}).join(', ')+')';
                            }
                            jobMap[memberData.JobID] = job;
                            day.Jobs.push(job);
                        }
                        var crewKey = memberData.JobID+'_'+memberData.CrewID
                        var crew = crewMap[crewKey];
                        if(!crew){
                            var crewData = workweek.weekData.Crew.find(function(c){ return c.ID == memberData.CrewID});
                            crew = {ID:crewData.ID,Name:crewData.Name,ME:0,PD:0,ST:0,OT:0,DT:0,Total:0};
                            crewMap[crewKey] = crew;
                            job.Crews.push(crew);
                        }
                        crew.PD += memberData.PerDiem;
                        crew.ST += memberData.STHours;
                        crew.OT += memberData.OTHours;
                        crew.DT += memberData.DTHours;
                        crew.ME += memberData.Meals;
                        crew.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                        job.PD += memberData.PerDiem;
                        job.ST += memberData.STHours;
                        job.OT += memberData.OTHours;
                        job.DT += memberData.DTHours;
                        job.ME += memberData.Meals;
                        job.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                        day.PD += memberData.PerDiem;
                        day.ST += memberData.STHours;
                        day.OT += memberData.OTHours;
                        day.DT += memberData.DTHours;
                        day.ME += memberData.Meals;
                        day.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                        data.Total += (memberData.STHours+memberData.OTHours+memberData.DTHours);
                        data.Meals = data.Meals||day.ME>0;
                    }
                }
                day.crewCount = day.Jobs.reduce(function(crews, job){ job.Crews.forEach(function(c){ if(crews.indexOf(c.ID) == -1){ crews.push(c.ID)} }); return crews; },[]).length;
                day.Style = (day.Date.getTime()==_currentDay(workweek).WeekdayDate.getTime())?{color:'#00F'}:(day.Jobs.length?{}:{color:'#999'});
                data.Days.push(day);
            }
            return data;
        }
        var _viewMemberWeek = function(dayMember, scope, date){
            var workDayIds = scope.workweek.WorkDay.map(function(wd){ return wd.ID });
            var day = _currentDay(scope.workweek);
            date = date||day.WeekdayDate;
            scope.dialogs.memberWeek._scope.data.member = dayMember;
            scope.loadingDialog();
            _load(scope, 'fte/loadMemberWeek', 'GET', {person: dayMember.PersonID, org:scope.workweek.PersonOrganizationID, date: Utils.date(date,'MM/dd/yyyy'), workdayIds:workDayIds.join(',')}, 'viewMemberWeekResult');
        }

        var _viewEquipWeekCheckCurrentTimesheet = function(equips, jdceId, workweek, data, jobMap, workDay, currentEquip){
            if(!equips) return;
            var equip = equips.find(function(eq){ return eq.ID == jdceId });
            if(equip || currentEquip){
                if(currentEquip){
                    if(equip || !currentEquip.ActiveFlag) return;
                    equip = currentEquip;
                }
                var jobData = workDay.JobDayCrew[equip.jobIndex];
                if(!jobMap[jobData.JobCrewJobID]){
                    jobMap[jobData.JobCrewJobID] = true;
                    workweek.weekData.Job.push({ID: jobData.JobCrewJobID, Number: jobData.WorkgroupJobNumber });
                }
                return {
                    JobID: jobData.JobCrewJobID,
                    Hours: equip.TotalHours
                }
            }
        }
        var _viewEquipWeekCheckCurrentTimesheet = function(equips, jdceId, workweek, data, jobMap, crewMap, workDay, currentEquip){
            if(!equips) return;
            var equip = equips.find(function(eq){ return eq.ID == jdceId });
            if(equip || currentEquip){
                if(currentEquip){
                    if(equip || !currentEquip.ActiveFlag) return;
                    equip = currentEquip;
                }
                var jobData = workDay.JobDayCrew[equip.jobIndex];
                if(!jobMap[jobData.JobCrewJobID]){
                    jobMap[jobData.JobCrewJobID] = true;
                    workweek.weekData.Job.push({ID: jobData.JobCrewJobID, Number: jobData.WorkgroupJobNumber });
                }
                if(!crewMap[workweek.CrewID]){
                    crewMap[workweek.CrewID] = true;
                    workweek.weekData.Crew.push({ID: workweek.CrewID, Name: workweek.CrewName });
                }
                return {
                    JobID: jobData.JobCrewJobID,
                    CrewID: workweek.CrewID,
                    Hours: equip.TotalHours
                }
            }
        }
        
        var _viewEquipWeekResult = function(workweek){
            workweek.weekData.Job = Utils.enforceArray(workweek.weekData.Job);
            workweek.weekData.D = Utils.enforceArray(workweek.weekData.D);
            workweek.weekData.Crew = Utils.enforceArray(workweek.weekData.Crew);
            var data = {Days:[], Total:0};
            var existingJobMap = workweek.weekData.Job.reduce(function(map, jb){ map[jb.ID] = true; return map; }, {}), existingCrewMap = workweek.weekData.Crew.reduce(function(map, crew){  map[crew.ID] = true; return map; }, {});
            for(var d=0; d<workweek.weekData.D.length; ++d){
                var day = {Date: Utils.date(workweek.weekData.D[d].Date),Total:0,Expanded:false,Jobs:[]};
                var jobMap = {}, crewMap = {}, workDay = workweek.WorkDay.find(function(wd){ return wd.WeekdayDate.getTime() == day.Date.getTime()}), checkJDCEs;
                workweek.weekData.D[d].JDCE = Utils.enforceArray(workweek.weekData.D[d].JDCE);
                if(workDay){
                    checkJDCEs = _activeJobs(workweek, workDay.WeekdayDate).reduce(function(arry, jb){ for(var e=0; e<jb.JobDayCrewEquipment.length; ++e){ if(jb.JobDayCrewEquipment[e].EquipmentID==workweek.weekData.ID){jb.JobDayCrewEquipment[e].jobIndex=jb.index;arry.push(jb.JobDayCrewEquipment[e])}}; return arry;}, []);
                    for(var e=0; e<checkJDCEs.length; ++e){
                        var equipData = _viewEquipWeekCheckCurrentTimesheet(workweek.weekData.D[d].JDCE, checkJDCEs[e].ID, workweek, data, existingJobMap, existingCrewMap, workDay, checkJDCEs[e]);
                        if(!equipData) continue;
                        var job = jobMap[equipData.JobID];
                        if(!job){
                            var jobData = workweek.weekData.Job.find(function(jb){ return jb.ID == equipData.JobID});
                            job = {Number: jobData.Number,Total:0,Crews:[]};
                            workweek.weekData.D[d].Override = Utils.enforceArray(workweek.weekData.D[d].Override);
                            if(workweek.weekData.D[d].Override.length){
                               job.Number += '('+workweek.weekData.D[d].Override.filter(function(ovr){ return ovr.JobID == equipData.JobID}).join(', ')+')';
                            }
                            jobMap[equipData.JobID] = job;
                            day.Jobs.push(job);
                        }
                        job.Style = {'font-weight':'bold'};
                        var crewKey = equipData.JobID+'_'+equipData.CrewID
                        var crew = crewMap[crewKey];
                        if(!crew){
                            var crewData = workweek.weekData.Crew.find(function(c){ return c.ID == equipData.CrewID});
                            crew = {ID:crewData.ID,Name:crewData.Name,Total:0,Style:crewData.ID==workweek.CrewID?{'font-weight':'bold'}:{}};
                            crewMap[crewKey] = crew;
                            job.Crews.push(crew);
                        }
                        crew.Style = {'font-weight':'bold'}
                        crew.Total += equipData.Hours;
                        job.Total += equipData.Hours;
                        day.Total += equipData.Hours;
                        data.Total += equipData.Hours;
                    }
                }
                for(var e=0; e<workweek.weekData.D[d].JDCE.length; ++e){
                    var equipData = _viewEquipWeekCheckCurrentTimesheet(checkJDCEs, workweek.weekData.D[d].JDCE[e].ID, workweek, data, existingJobMap, existingCrewMap, workDay)
                                 || workweek.weekData.D[d].JDCE[e];
                    var job = jobMap[equipData.JobID];
                    if(!job){
                        var jobData = workweek.weekData.Job.find(function(jb){ return jb.ID == equipData.JobID});
                        job = {Number: jobData.Number,Total:0,Crews:[]};
                        workweek.weekData.D[d].Override = Utils.enforceArray(workweek.weekData.D[d].Override);
                        if(workweek.weekData.D[d].Override.length){
                           job.Number += '('+workweek.weekData.D[d].Override.filter(function(ovr){ return ovr.JobID == equipData.JobID}).join(', ')+')';
                        }
                        jobMap[equipData.JobID] = job;
                        day.Jobs.push(job);
                    }
                    var crewKey = equipData.JobID+'_'+equipData.CrewID
                    var crew = crewMap[crewKey];
                    if(!crew){
                        var crewData = workweek.weekData.Crew.find(function(c){ return c.ID == equipData.CrewID});
                        crew = {ID:crewData.ID,Name:crewData.Name,Total:0,Style:crewData.ID==workweek.CrewID?{'font-weight':'bold'}:{}};
                        crewMap[crewKey] = crew;
                        job.Crews.push(crew);
                    }
                    crew.Total += equipData.Hours;
                    job.Total += equipData.Hours;
                    day.Total += equipData.Hours;
                    data.Total += equipData.Hours;
                }
                day.crewCount = day.Jobs.reduce(function(crews, job){ job.Crews.forEach(function(c){ if(crews.indexOf(c.ID) == -1){ crews.push(c.ID)} }); return crews; },[]).length;
                day.Style = (day.Date.getTime()==_currentDay(workweek).WeekdayDate.getTime())?{color:'#00F'}:(day.Jobs.length?{}:{color:'#999'});
                data.Days.push(day);
            }
            return data;
        }
        var _viewEquipWeek = function(dayEquip, scope, date){
            var workDayIds = scope.workweek.WorkDay.map(function(wd){ return wd.ID });
            var day = _currentDay(scope.workweek);
            date = date||day.WeekdayDate;
            scope.dialogs.equipWeek._scope.data.equip = dayEquip;
            scope.loadingDialog();
            _load(scope, 'fte/loadEquipWeek', 'GET', {equip: dayEquip.EquipmentID, org:scope.workweek.PersonOrganizationID, date: Utils.date(date,'MM/dd/yyyy'), workdayIds:workDayIds.join(',')}, 'viewEquipWeekResult');
        }

        var _setWorkWeekStatus = function(workweek){
            var code = 'IP' //Default to In Process
            var rules = {all:['PE'],atLeast:['SB','RV','AP','ZH'],one:['RJ']}
            var ruleVals = {all:{},atLeast:{lowest:rules.atLeast.length,count:0},one:{}}
            //Tally rule matches
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                if(rules.one.includes(day.StatusCode)){
                    ruleVals.one[day.StatusCode] = true;
                }else if(rules.all.includes(day.StatusCode)){
                    ruleVals.all[day.StatusCode] = (ruleVals.all[day.StatusCode]||0) + 1;
                }else{
                    var indexVal = rules.atLeast.indexOf(day.StatusCode);
                    if(indexVal > -1){
                        ruleVals.atLeast.lowest = Math.min(indexVal, ruleVals.atLeast.lowest);
                        ruleVals.atLeast.count++;
                    }
                }
            }
            //Check if we can assign a new code
            if(Object.keys(ruleVals.one).length){
                for(var o=0; o<rules.one.length; ++o){
                    if(ruleVals.one[rules.one[o]]){
                        code = rules.one[o];
                        break;
                    }
                }
            }else{
                var allKeys = Object.keys(ruleVals.all);
                if(allKeys.length == 1 && ruleVals.all[allKeys[0]] == 7){
                    code = allKeys[0];
                }else{
                    if(ruleVals.atLeast.count == 7){
                        code = rules.atLeast[ruleVals.atLeast.lowest];
                    }
                }
            }
            if(code != workweek.StatusCode){
                workweek.StatusCode = code;
                workweek.StatusMeaning = _jdcStatusMeanings[code];
            }
        }
        var _review = function(scope, code, force, dayIndex, comment){
            var workweek = scope.workweek
            //if we need validation, return an object like this on failure
            //return {title:'Hours Not Equal',message:msg}
            workweek.changes = true;
            var changedActive = false
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                if(d == dayIndex || (dayIndex == null && ((day.StatusCode == 'SB' && ['AP','RV'].includes(code))||(day.StatusCode == 'RV'&&code=='AP')||(day.StatusCode=='PE'&&code=='ZH')))){
                    day.priorStatus = day.StatusCode;
                    day.StatusCode = code;
                    day.StatusMeaning = _jdcStatusMeanings[code];
                    day.reviewComment = comment;
                    var jdcCode = ['PE','IP'].includes(code)?'EPE':code=='SB'?'SU':code;
                    var crewCode = jdcCode=='EPE'?'PE':jdcCode;
                    for(var j=0; j<day.JobDayCrew.length; ++j){
                        var job = day.JobDayCrew[j];
                        if(job.active){
                            job.priorStatus = job.StatusCode
                            job.StatusCode = jdcCode
                            for(var m=0; m<job.JobDayCrewMember.length; ++m){
                                job.JobDayCrewMember[m].StatusCode = crewCode
                            }
                            for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                                job.JobDayCrewEquipment[e].StatusCode = crewCode
                            }
                        }
                    }
                    //On sumbit, inactivate non-permanent members with no hours
                    if(code == 'SB'){
                        for(var m=0; m<day.members.length; ++m){
                            var dayMember = day.members[m];
                            if(dayMember.Jobs[0].Member.ActiveFlag && dayMember.Jobs[0].Member.permanentMessage=='Permanently Add To Crew' && !dayMember.TotalHours && !dayMember.PerDiem){
                                scope.changeCrewActive('Member', false, dayMember.Jobs[0].Member, dayMember.Jobs[0], dayMember);
                                if(d==workweek.currentDay)changedActive = true
                            }
                        }
                    }
                }
            }
            _setWorkWeekStatus(workweek);
            if(changedActive){
                 _initSummary(workweek, scope);
            }
            return {};
        }
        var _cleanPriorStatuses = function(workweek){
            for(var d=0; d<workweek.WorkDay.length; d++){
                var day = workweek.WorkDay[d];
                if(day.priorStatus){
                    delete day.priorStatus;
                    for(var j=0; j<day.JobDayCrew.length; ++j){
                        var job = day.JobDayCrew[j];
                        if(job.priorStatus){
                            delete job.priorStatus;
                            for(var m=0; m<job.JobDayCrewMember.length; ++m){
                                delete job.JobDayCrewMember[m].priorStatus;
                            }
                            for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                                delete job.JobDayCrewEquipment[e].priorStatus;
                            }
                        }
                    }
                }
            }
        }
        var _revertReview = function(failedDays, workweek){
            for(var d=0; d<workweek.WorkDay.length; d++){
                var day = workweek.WorkDay[d];
                if(day.priorStatus && failedDays.find(function(fd){ return fd.ID==day.ID})){
                    day.StatusCode = day.priorStatus;
                    day.StatusMeaning = _jdcStatusMeanings[day.priorStatus];
                    delete day.priorStatus;
                    for(var j=0; j<day.JobDayCrew.length; ++j){
                        var job = day.JobDayCrew[j];
                        if(job.active){
                            job.StatusCode = job.priorStatus;
                            delete job.priorStatus;
                            for(var m=0; m<job.JobDayCrewMember.length; ++m){
                                job.JobDayCrewMember[m].StatusCode = job.JobDayCrewMember[m].priorStatus;
                                delete job.JobDayCrewMember[m].priorStatus;
                            }
                            for(var e=0; e<job.JobDayCrewEquipment.length; ++e){
                                job.JobDayCrewEquipment[e].StatusCode = job.JobDayCrewEquipment[e].priorStatus;
                                delete job.JobDayCrewEquipment[e].priorStatus;
                            }
                        }
                    }
                }
            }
        }

        var _reviewDone = function(succeededDays, failedDays, scope){
            var workweek = scope.workweek;
            //Handle merging equip time entries back together if re-opened
            for(var d=0; d<workweek.WorkDay.length; d++){
                var day = workweek.WorkDay[d];
                if(succeededDays.includes(day.ID)){
                    if(day.StatusCode == 'IP' && day.priorStatus == 'AP'){
                        _mergeEquipTimeEntries(workweek, day);
                        _mergeMemberTimeEntries(workweek, day);
                    }
                }
            }
            var currentDay = workweek.currentDay;
            //If only one day was handled, then potentially move to another day for review
            if(succeededDays.length==1 && !failedDays.length){
                if(workweek.currentDay < 6){
                    //Just submitted, move to next
                    if(day.StatusCode == 'SB'){
                        ++workweek.currentDay;
                    }
                    //Just reviewed / approved, moved to next if one needs review
                    if(['AP','RV'].includes(day.StatusCode)){
                        if(workweek.AllowApproval){
                            var nextIndex = workweek.WorkDay.findIndex(function(wd, i){ return i>workweek.currentDay&&['SB','RV'].includes(wd.StatusCode) });
                            if(nextIndex > -1){
                                workweek.currentDay = nextIndex;
                            }
                        }else if(workweek.UseReviewer && workweek.AllowReview){
                            var nextIndex = workweek.WorkDay.findIndex(function(wd, i){ return i>workweek.currentDay&&wd.StatusCode=='SB' });
                            if(nextIndex > -1){
                                workweek.currentDay = nextIndex;
                            }
                        }
                    }
                }
            }
            //If day(s) failed, revert them
            if(failedDays.length){
                _revertReview(failedDays, workweek);
            }
            //Clean up
            _cleanPriorStatuses(workweek);
            _setWorkWeekStatus(workweek);
            if(currentDay != workweek.currentDay){
                _initDay(scope);
            }
            _checkApproveAll(workweek);
        }

        var _getReportLevelData = function(workweek, isWeek){
            var ww = Utils.pick(workweek, 'CrewID','DepartmentCode','OrgWorkWeekID','PayrollPeriodID','PersonID','PersonOrganizationID','ID','End','StatusCode');
            ww.WorkDay = workweek.WorkDay.map(function(day, index){
                var d = {
                    WeekdayDate: day.WeekdayDate,
                    WorkWeekID: ww.ID,
                    StatusCode: day.StatusCode,
                    ID: day.ID,
                    JobDayCrew: day.JobDayCrew.reduce(function(jobs, jb){
                        if(jb.active){
                            var job = Utils.pick(jb, 'ID','JobCrewCrewID','JobCrewJobID','JobCrewID','ForemanID','OwnerID','WorkgroupJobNumber','NumberChar','StatusCode')
                            job.WorkDayID = day.ID
                            jobs.push(job);
                        }
                        return jobs;
                    }, [])
                }
                if(!isWeek && index == workweek.currentDay){
                    d.expanded = true;
                }
                return d;
            });
            ww.expanded = true;
            return {WorkWeek:ww};
        }
        var _cleanReportSelectionData = function(data, level, dayIndex, jobIndex){
            var ww = data.WorkWeek;
            ww.End = Utils.date(ww.End, 'MM/dd/yyyy');
            delete ww.expanded;
            if(level == 'WorkWeek'){
                ww.Report = 'Y';
                delete ww.WorkDay;
            }else{
                for(var d=ww.WorkDay.length-1; d>=0; --d){
                    if(d != dayIndex){
                        ww.WorkDay.splice(d, 1);
                    }else{
                        ww.WorkDay[d].WeekdayDate = Utils.date(ww.WorkDay[d].WeekdayDate, 'MM/dd/yyyy');
                        delete ww.WorkDay[d].expanded;
                        if(level == 'WorkDay'){
                            ww.WorkDay[d].Report = 'Y'
                            delete ww.WorkDay[d].JobDayCrew;
                        }else{
                            for(var j=ww.WorkDay[d].JobDayCrew.length-1; j>=0; --j){
                                if(j != jobIndex){
                                    ww.WorkDay[d].JobDayCrew.splice(j, 1);
                                }else{
                                    delete ww.WorkDay[d].JobDayCrew[j].WorkgroupJobNumber;
                                    delete ww.WorkDay[d].JobDayCrew[j].NumberChar;
                                    delete ww.WorkDay[d].JobDayCrew[j].expanded;
                                    ww.WorkDay[d].JobDayCrew[j].Report = 'Y';
                                }
                            }
                        }
                    }
                }
            }
            return data;
        }

        var _addJobsDefaultData = function(data, workweek){
            workweek.sp.jobType = Utils.enforceArray(data.JobType);
            workweek.sp.region = Utils.enforceArray(data.Region);
            workweek.sp.division = Utils.enforceArray(data.Division);
            workweek.sp.country = Utils.enforceArray(data.Country);
            workweek.sp.state = Utils.enforceArray(data.State);
            workweek.sp.project = Utils.enforceArray(data.Project);

            workweek.dialog1.job = data.Job;
            var job = workweek.dialog1.job;
            job.TypeCode = 'TE';
            job.UseDesignFlag = false;
            if(job.TypeCode){
                job.jobType = workweek.sp.jobType.find(function(jt){return jt.SmartCode==job.TypeCode});
            }
            if(job.DivisionRegionID){
                job.region = workweek.sp.region.find(function(r){return r.RegionID==job.DivisionRegionID});
            }
            if(job.DivisionID){
                job.division = workweek.sp.division.find(function(d){return d.DivisionID==job.DivisionID});
            }
            if(job.StateCountryID){
                job.country = workweek.sp.country.find(function(c){return c.ID==job.StateCountryID});
            }
            if(job.MasterProjectID){
                job.project = workweek.sp.project.find(function(cp){return cp.ID==job.MasterProjectID});
            }
            if(job.StateID){
                job.state = workweek.sp.state.find(function(s){return s.ID==job.StateID});
            }
        }
        var _trackNewJob = function(job, workweek){
            _cleanJobs(workweek, [job]);
            var day = _currentDay(workweek);
            day.currentJobType = job.Type;
            _selectStartJob(workweek, day.Job.find(function(jb){ return jb.ID == job.ID }));
        }


        var _jdcStatusMeanings = {
            PE: 'Pending',
            BIP: 'Begin Day In Process',
            EPE: 'Pending End of Day',
            SU: 'Submitted',
            RV: 'Reviewed',
            AP: 'Approved',
            RJ: 'Rejected',
            SB: 'Submitted',
            IP: 'In Process',
            ZH: 'Zero Hours'
        }
        var _reviewStatusMeanings = {
            RJ: 'Reject',
            SB: 'Resubmit'
        }
        var _setJDCStatus = function(workweek, statusCode){
            var jobs = _activeJobs(workweek), statusMeaning = _jdcStatusMeanings[statusCode];
            for(var j=0; j<jobs.length; ++j){
                jobs[j].StatusCode = statusCode;
                jobs[j].StatusMeaning = statusMeaning;
            }
        }
        var _setDayStatus = function(day, workweek, statusCode){
            day.StatusCode = statusCode;
            day.StatusMeaning = _jdcStatusMeanings[day.StatusCode];
            _setWorkWeekStatus(workweek);
        }
        var _steps = {
            CJ: { //Choose Jobs
                init: function(workweek, scope){
                    _resetJobTabLimits(scope)
                    var jobs = _activeJobs(workweek), day = _currentDay(workweek);
                    if(workweek.filter)workweek.filter.jobNumber = '';
                    if(day.StatusCode != 'ZH' && scope.editable){
                        _setDayStatus(day, workweek, 'PE');
                        _checkZeroAll(workweek);
                        if(jobs.length && jobs[0].StatusCode != 'PE'){
                            _setJDCStatus(workweek, 'PE');
                        }
                    }
                },
                validate: function(workweek, scope, isMovingForward){
                    if(!isMovingForward) return true;
                    var day = _currentDay(workweek);
                    if(!day.Job.find(function(jb){ return jb.selected })){
                        _showMessage('You must select at least one job before proceeding.','Error')
                        return false;
                    }
                    return true;
                },
                complete: function(workweek, scope){
                    var jobs;
                    if(scope.editable){
                        _addRemoveJobs(workweek, workweek.currentDay);
                        _updateJobHours(workweek, 0);
                        var day = _currentDay(workweek);
                        jobs = _activeJobs(workweek);
                        _setDayStatus(day, workweek, 'IP');
                        _checkZeroAll(workweek);
                        if(jobs[0].StatusCode == 'PE'){
                            _setJDCStatus(workweek, 'BIP');
                        }
                        if(jobs[0].init){
                            _draftCrews(scope, workweek);
                        }
                    }
                    jobs = _activeJobs(workweek);
                    for(var j=0; j<jobs.length; ++j){
                        delete jobs[j].init;
                    }
                }
            },
            DR: { //Define Roster
                init: function(workweek, scope){
                    var jobs = _activeJobs(workweek);
                    if(jobs.length){
                        _sortMembersOnJob(jobs[0], scope);
                        _sortEquipOnJob(jobs[0]);
                    }
                    if(!workweek.sp.jobStatus || !workweek.sp.memberInactiveReason || !workweek.sp.equipmentInactiveReason){
                        _loadCrewReferenceData(workweek, scope);
                    }
                },
                validate: function(workweek, scope){
                    var jobs = _activeJobs(workweek);
                    var incompleteMember = jobs[0].JobDayCrewMember.find(function(m){ return !m.class });
                    if(incompleteMember){
                        var msg = 'Please select a ';
                        if(!incompleteMember.craft) msg += 'craft/';
                        msg += 'class for '+incompleteMember.PersonFullName+'.';
                        _showMessage(msg,'Error');
                        return false;
                    }
                    return true;
                },
                complete: function(workweek, scope){
                    var jobs = _activeJobs(workweek);
                    if(jobs[0].StatusCode == 'BIP') _setJDCStatus(workweek, 'EPE');
                    _calcIfNextIsCompleteWork(workweek);
                    _defaultJobDuration(workweek, scope);
                    _resetJobTabLimits(scope)
                }
            },
            CT: { //Crew Time
                0: { //Arrange Jobs & Hours
                    init: function(workweek, scope){
                        _resetJobTabLimits(scope)
                        if(workweek.FromCompleteWork && !workweek.jobScreen){
                            _navStep(scope, workweek, 1);
                        }else{
                            if(workweek.jobScreen){
                                _markJobsWorkComplete(_currentDay(workweek))
                            }
                            _calcIfNextIsCompleteWork(workweek);
                            _calcShowUpSites(workweek);
                        }
                    },
                    validate: function(workweek, scope, isMovingForward){
                        if(!isMovingForward) return true;
                        var jobs = _activeJobs(workweek);
                        if(!jobs.reduce(function(total, jb){ return total+jb.GrandTotalHours }, 0)){
                            _showMessage('At least one job should have more than zero hours assigned.','Error')
                            return false;
                        }
                        return true;
                    },
                    complete: function(workweek, scope){
                        if(!workweek.FromCompleteWork && _completeWork(scope, null, workweek.forceCompleteWork, true)){
                            return;
                        }
                        _taskScreenInfo(workweek, scope);
                    }
                },
                1: { // Task & phase times
                    init: function(workweek, scope){
                        _taskScreenInfo(workweek, scope, 'loadJobTasksResultNoInit');
                    },
                    validate: function(workweek, scope){
                        var activeJobs = _activeJobs(workweek);
                        var jobs = activeJobs.filter(function(jb){ return !jb.Task || !_activeTasks(jb).length });
                        if(jobs.length){
                            if(activeJobs.length == jobs.length){
                                _showMessage("There are no tasks assigned to the job(s). Please add at least one task.");
                            }else{
                                var msg = jobs.reduce(function(txt, jb){ txt+='<li>'+jb.WorkgroupJobNumber+'</li>'; return txt; }, '');
                                var jobPlural = jobs.length==1?'job does':'jobs do'; var jobWords = jobs.length==1?'this job':'these jobs'; var buttonLabel = jobs.length==1?'Remove Job':'Remove Jobs';
                                _showHTMLMessage('The following '+jobPlural+' not have any tasks assigned: <ul>'+msg+'</ul>Would you like to remove '+jobWords+'?','Error', [{
                                    label: 'Close'
                                },{
                                    label: buttonLabel,
                                    callback: function(){
                                        _removeJobs(workweek, jobs);
                                    }
                                }])
                            }
                            return false;
                        }else{
                            var problemJobs = {}, problemCount = 0, unassignedLocationTasks = [];
                            var zeroHoursJobs = {}, zeroHoursCount = 0, totalHours = 0
                            for(var j=0; j<activeJobs.length; ++j){
                                var tasks = _activeTasks(activeJobs[j]);
                                for(var t=0; t<tasks.length; ++t){
                                    totalHours += tasks[t].Duration;
                                    var phases = _activePhases(tasks[t]);
                                    if(!phases.length){
                                        if(!problemJobs[activeJobs[j].JobCrewJobID]) problemJobs[activeJobs[j].JobCrewJobID] = {job:activeJobs[j],tasks:[]}
                                        problemJobs[activeJobs[j].JobCrewJobID].tasks.push(tasks[t]);
                                        ++problemCount;
                                    }else if(!tasks[t].Duration && !tasks[t].askedAboutHours){
                                        if(!zeroHoursJobs[activeJobs[j].JobCrewJobID]) zeroHoursJobs[activeJobs[j].JobCrewJobID] = {job:activeJobs[j],tasks:[]}
                                        zeroHoursJobs[activeJobs[j].JobCrewJobID].tasks.push(tasks[t]);
                                        ++zeroHoursCount;
                                    }
                                    if(_isLocationTask(activeJobs[j], tasks[t]) && !tasks[t].LocationID && unassignedLocationTasks.indexOf(activeJobs[j].JobCrewJobID) == -1){
                                        unassignedLocationTasks.push(activeJobs[j].JobCrewJobID);
                                    }
                                }
                            }
                            var problemJobIDs = Object.keys(problemJobs);
                            if(problemCount){
                                var taskPlural = problemCount==1?'task does':'tasks do',
                                    taskWords = problemCount==1?'this task':'these tasks',
                                    buttonLabel = problemCount==1?'Remove Task':'Remove Tasks',
                                    msg = problemJobIDs.reduce(function(txt, jobId){ txt+='<li>'+problemJobs[jobId].job.WorkgroupJobNumber+'<ul>'+(problemJobs[jobId].tasks.reduce(function(txt2, task){ txt2+='<li>'+task.TaskNumber+'</li>'; return txt2; }, ''))+'</ul></li>'; return txt; }, '');
                                _showHTMLMessage('The following '+taskPlural+' not have any phase codes assigned: <ul>'+msg+'</ul>Would you like to remove '+taskWords+'?','Error', [{
                                    label: 'Close'
                                },{
                                    label: buttonLabel,
                                    callback: function(){
                                        problemJobIDs.forEach(function(jobId){
                                            problemJobs[jobId].tasks.forEach(function(task){
                                                _removeTask(workweek, problemJobs[jobId].job, task);
                                            })
                                            
                                        });                                        
                                    }
                                }])
                                return false;
                            }else if(zeroHoursCount && !totalHours){
                                var problemJobIDs = Object.keys(zeroHoursJobs);
                                var taskPlural = zeroHoursCount==1?'task does':'tasks do',
                                    taskWords = zeroHoursCount==1?'this task':'these tasks',
                                    buttonLabel = zeroHoursCount==1?'Remove Task':'Remove Tasks',
                                    msg = problemJobIDs.reduce(function(txt, jobId){ txt+='<li>'+zeroHoursJobs[jobId].job.WorkgroupJobNumber+'<ul>'+(zeroHoursJobs[jobId].tasks.reduce(function(txt2, task){ task.askedAboutHours = true; txt2+='<li>'+task.TaskNumber+'</li>'; return txt2; }, ''))+'</ul></li>'; return txt; }, '');
                                _showHTMLMessage('The following '+taskPlural+' not have any hours: <ul>'+msg+'</ul>Would you like to remove '+taskWords+'?','Error', [{
                                    label: 'Close'
                                },{
                                    label: buttonLabel,
                                    callback: function(){
                                        problemJobIDs.forEach(function(jobId){
                                            zeroHoursJobs[jobId].tasks.forEach(function(task){
                                                _removeTask(workweek, zeroHoursJobs[jobId].job, task);
                                            })
                                            
                                        });                                        
                                    }
                                }])
                                return false;
                            }else if(unassignedLocationTasks.length){
                                for(var j=0; j<activeJobs.length; ++j){
                                    activeJobs[j].showTasks = unassignedLocationTasks.indexOf(activeJobs[j].JobCrewJobID) > -1;
                                }
                                _calcShowTaskRows(workweek);
                                _showMessage('One or more tasks does not have a location assigned. Please correct it before proceeding.','Error')
                                return false;
                            }
                        }
                        return true;
                    },
                    complete: function(workweek, scope){
                        _changes(workweek, true);
                        if(scope.editable)_parseTimeToMembers(workweek);
                        delete workweek.currentMember;
                        _initMemberTime(workweek, scope);
                    }
                }
            },
            MT: { // Member time
                init: function(workweek, scope){
                    _initMemberTime(workweek, scope, null, true);
                },
                validate: function(workweek, scope, isMovingForward){
                    if(!isMovingForward) return true;
                    return _validateTimeBuckets(workweek, scope);
                },
                complete: function(workweek, scope){
                    _changes(workweek, true);
                    if(scope.editable)_parseTimeToEquipments(workweek);
                    delete workweek.currentEquip;
                    _initEquipmentTime(workweek, scope);
                }
            },
            ET: { // Equipment time
                init: function(workweek, scope){
                    _initEquipmentTime(workweek, scope);
                },
                complete: function(workweek, scope){
                    _changes(workweek, true);
                    _currentDay(workweek).showSafety = false;
                    _initSummary(workweek, scope);
                }
            },
            RS: { // Review summary
                init: function(workweek, scope){
                    _initSummary(workweek, scope);
                }
            },
            init: function(step, workweek, scope){
                if(_steps[step.ProcessStepCode]) {
                    if(step.subSteps){
                        if(_steps[step.ProcessStepCode][step.subSteps.current].init){
                            return _steps[step.ProcessStepCode][step.subSteps.current].init(workweek, scope);
                        }
                    }else if(_steps[step.ProcessStepCode].init){
                        return _steps[step.ProcessStepCode].init(workweek, scope);
                    }
                }
                return true;
            },
            validate: function(step, workweek, scope, isMovingForward){
                if(_steps[step.ProcessStepCode]) {
                    if(step.subSteps){
                        if(_steps[step.ProcessStepCode][step.subSteps.current].validate){
                            return _steps[step.ProcessStepCode][step.subSteps.current].validate(workweek, scope, isMovingForward);
                        }
                    }else if(_steps[step.ProcessStepCode].validate){
                        return _steps[step.ProcessStepCode].validate(workweek, scope, isMovingForward);
                    }
                }
                return true;
            },
            complete: function(step, lastSubStep, dir, workweek, scope){
                if(dir < 0) return;
                if(_steps[step.ProcessStepCode]) {
                    if(step.subSteps){
                        if(_steps[step.ProcessStepCode][lastSubStep].complete){
                            return _steps[step.ProcessStepCode][lastSubStep].complete(workweek, scope);
                        }
                    }else if(_steps[step.ProcessStepCode].complete){
                        return _steps[step.ProcessStepCode].complete(workweek, scope);
                    }
                }
            }
        }
        var _saveCurrentStep = function(day){
            for(var i=0; i<day.DayProcessCompletion.length; i++){
                var step = day.DayProcessCompletion[i];
                if(i < day.currentStep){
                    step.CompleteFlag = true;
                }else{
                    step.CompleteFlag = false;
                }
            }
        }
        var _forceSaveCurrentStep = function(scope){
            if(!scope.original) return;
            var day = _currentDay(scope.workweek);
            var originalDay = _dayByDate(scope.original, day.WeekdayDate)
            if(originalDay){
                _saveCurrentStep(day);
                originalDay.currentStep = day.currentStep - 1;
                _saveCurrentStep(originalDay);
            }
        }
        var _navStep = function(scope, workweek, dir){
            var day =  _currentDay(workweek);
            var currentStep = _currentStep(workweek);
            if(dir > 0){
                if(!_steps.validate(currentStep, workweek, scope, true))
                    return false;
            }
            var lastSubStep = currentStep.subSteps?currentStep.subSteps.current:0;
            if(currentStep.subSteps && ((currentStep.subSteps.current > 0 && dir < 0) || (currentStep.subSteps.current < currentStep.subSteps.last && dir > 0)) ){
                currentStep.subSteps.current += dir;
            }else{
                day.currentStep += dir;
            }
            scope.disableRight = day.currentStep < day.stepLimit ? false : true;
            _saveCurrentStep(day)
            if(dir > 0){
                _steps.complete(currentStep, lastSubStep, dir, workweek, scope);
                _changes(workweek, true);
            }else{
                _steps.init(day.DayProcessCompletion[day.currentStep], workweek, scope);
            }
            return true;
        }

        var _save = function(scope, endpoint, extraData, callbackName, loadingText, noValidate){
            if(endpoint!='fte/saveAndCompleteWork' && !noValidate && !_steps.validate(_currentStep(scope.workweek), scope.workweek, scope, false)){
                return false;
            }
            endpoint = endpoint||'fte/save';
            scope.loading = true;
            scope.loadingDialog(loadingText);

            var done = function(resp){
                scope.loading = false;
                if(resp){
                    if(resp.replacements && resp.replacements.length){
                        Utils.replace(scope.workweek, resp.replacements);
                    }
                    if(!resp.status || resp.status == 200){
                        scope.original = Utils.copy(scope.workweek)
                        if(scope.$parent && scope.$parent.current && scope.$parent.current.original){
                            scope.$parent.current.original = scope.original;
                            if(scope.share) scope.share.updateOriginal(scope.original);
                        }
                    }
                }
                if(scope.callbacks[callbackName]){
                    if(typeof scope.callbacks[callbackName] == "function"){
                        scope.callbacks[callbackName].apply(null, [resp, extraData]);
                    }else if(typeof scope.callbacks[callbackName].fn == "function"){
                        scope.callbacks[callbackName].fn.apply(null, [resp, extraData]);
                    }
                }
            }

            if(endpoint.match(/\/save/i) && !endpoint.match('initMemberTime')){
                _changes(scope.workweek, false);
            }
            _processPermanentChanges(scope.workweek);
            var data = SaveService.getPostData(scope.workweek, scope.original);
            if(extraData){
                data += '[!-- @@@@@ --]'+(typeof extraData=='string'?extraData:JSON.stringify(extraData));
            }
            var route = {route:endpoint.split('fte/').pop()};
            $timeout(function(){
                ApiService.post('fte', data, null, null, null, route).then(done,done);
            });
            return true;
        }

        var _load = function(scope, endpoint, method, extraData, callbackName){
            var done = function(){
                scope.loading = false;
                if(scope.callbacks[callbackName]){
                    scope.callbacks[callbackName].apply(null, arguments);
                }
            }
            endpoint = endpoint||'fte/load';
            extraData.route = endpoint.split('fte/').pop();
            scope.loading = true;
            $timeout(function(){
                if(method == 'GET'){
                    ApiService.get('fte', extraData).then(done,done);
                }else{
                    ApiService.post('fte', extraData).then(done,done);
                }
            });
        }

        var _parseTime = function(value, modelValue){
            var str = value.toString(), rounding = 0.066666667;
            if(str.length <= 10){
                str = str.replace('.',':').replace(/[^\d:]/ig, '');
                if(str.length > 2 && str.indexOf(':') == -1){
                    str = str.substr(0,str.length-2)+':'+str.substr(str.length-2,2);
                }
                str = str.split(':');
                var date = new Date(modelValue&&modelValue.getTime?modelValue.getTime():undefined);
                Utils.clearTimeFromDate(date);
                if(str.length > 0){
                    var hours = parseInt(str[0]);
                    if(hours <= 23 && hours >= 0)
                        date.setHours(hours);
                    
                }
                if(str.length > 1){
                    if(str[1].length == 1)
                        str[1] += '0';
                    var minutes = Math.round(Math.round(parseInt(str[1])*rounding)/rounding);
                    if(minutes <= 59 && minutes >= 0)
                        date.setMinutes(minutes);
                }
                return date;
            }
            return new Date(Date.parse(value));
        }
        var _formatTime = function(value){
            if(typeof value == 'object'){
                return Utils.date(value, 'HH:mm');
            }else if(typeof value == 'string'){
                return Utils.date(new Date(Date.parse(value)), 'HH:mm');
            }
            return '';
        }

        var _getTempID = function(workweek) {
            if(!workweek.lastTemp) workweek.lastTemp = 0;
            ++workweek.lastTemp;
            return '__temp'+workweek.lastTemp.toString(16)+'__';
        };
        var _cleanID = function(id){
            if(typeof id == 'string'){
                if(id.length && id.substr(0,6) != '__temp')
                    return parseInt(id);
            }
            return id;
        }

        var _spMappings = {
            jobStatus: {
                'ID':  'MemberStatusID',
                'Code': {lookup: true, name:'MemberStatusCode'},
                'Description': 'MemberStatusDescription',
                'DraftCrewTimeFlag': 'MemberStatusDraftCrewTimeFlag',
                'CrewMemberActiveFlag': 'MemberStatusCrewMemberActiveFlag'
            },
            craft: {
                'ID': {lookup: true, name: 'CraftID'},
                'Craft': 'CraftCraft',
                'OrganizationID': 'CraftOrganizationID',
                'Sort': 'CraftSortOrder'
            },
            class: {
                'ID': {lookup: true, name: 'ClassID'},
                'Class': 'ClassClass',
                'CraftID': 'ClassCraftID',
                'Short': 'ClassShortDescription',
                'OrganizationID': 'ClassOrganizationID',
                'Sort': 'ClassSortOrder'
            },
            inactiveReason: {
                'SmartCode': {lookup: true, name: 'InactiveReasonCode'},
                'Meaning': 'InactiveReasonMeaning'
            },
            noPerDiemReason: {
                'SmartCode': {lookup: true, name: 'NoPerDiemReasonCode'},
                'Meaning': 'NoPerDiemReasonMeaning'
            },
            timeWorkWeek: {
                'SmartCode': {lookup: true, name: 'Code'}
            }
        }

        if(!Object.entries)Object.entries = function(obj) { var arr=Object.keys(obj); for(var i=0; i<arr.length; ++i){arr[i]=[arr[i],obj[arr[i]]]} return arr; }
        function findKey(obj, fn) {
          var found = Object.entries(obj).find(fn);
          if(found) return found[0];
          return null;
        }

        var _mapSmartpick = function(workweek, obj, spName, fieldMap, workweekSpName){
            workweekSpName = workweekSpName||spName;
            fieldMap = (typeof fieldMap == 'string' ? _spMappings[fieldMap] : fieldMap);
            var primaryKey = findKey(fieldMap, function(entry){ return typeof entry[1] == 'object' && entry[1].lookup });
            if(!obj[spName]){
                obj[spName] = {};
            }else if(!obj[fieldMap[primaryKey].name]){
                delete obj[spName];
            }
            if(obj[spName] && obj[fieldMap[primaryKey].name]){
                var keys = Object.keys(fieldMap);
                if(workweek.sp[workweekSpName]){
                    obj[spName] = workweek.sp[workweekSpName].find(function(sp){ return sp[primaryKey] == obj[fieldMap[primaryKey].name] });
                }else{
                    for(var i=0; i<keys.length; ++i){
                        var fieldKey = (typeof fieldMap[keys[i]] == 'object')? fieldMap[keys[i]].name : fieldMap[keys[i]];
                        obj[spName][keys[i]] = obj[fieldKey];
                    }
                }
            }else{
                delete obj[spName];
            }
        }

        var _mapSmartpickBack = function(workweek, obj, spName, map){
            var keys;
            var spObj = obj[spName];
            var fieldMap = (typeof map == 'string' ? _spMappings[map] : map);
            if(!spObj || !(keys=Object.keys(spObj))){
                delete obj[spName];
                keys = Object.keys(fieldMap);
                for(var i=0; i<keys.length; ++i){
                    var fieldKey = (typeof fieldMap[keys[i]] == 'object')? fieldMap[keys[i]].name : fieldMap[keys[i]];
                    obj[fieldKey] = '';
                }
            }else{
                for(var i=0; i<keys.length; ++i){
                    var fieldKey = (typeof fieldMap[keys[i]] == 'object')? fieldMap[keys[i]].name : fieldMap[keys[i]];
                    obj[fieldKey] = spObj[keys[i]];
                }
            }
        }

        return {
          setDialog: _setDialog,
            cleanWorkweek: _cleanWorkweek,
            inactivateCrew: _inactivateCrew,
            selectStartJob: _selectStartJob,
            initDay: _initDay,
            isDayEditable: _isDayEditable,
            addRemoveJobs: _addRemoveJobs,
            activeJobs: _activeJobs,
            currentDay: _currentDay,
            currentMember: _currentMember,
            currentStep: _currentStep,
            navStep: _navStep,
            updateJobTime: _updateJobTime,
            updateMemberJobTime: _updateMemberJobTime,
            updateJobHours: _updateJobHours,
            updateMemberJobHours: _updateMemberJobHours,
            changeJobDescription: _changeJobDescription,
            moveJob: _moveJob,
            draftCrews: _draftCrews,
            cleanID: _cleanID,
            addCrewMember: _addCrewMember,
            addCrewEquipment: _addCrewEquipment,
            addCrewRental: _addCrewRental,
            syncRosterItem: _syncRosterItem,
            mapSmartpick: _mapSmartpick,
            mapSmartpickBack: _mapSmartpickBack,
            moveJobTask: _moveJobTask,
            loadJobTasksInfo: _loadJobTasksInfo,
            loadCrewReferenceData: _loadCrewReferenceData,
            shouldHideMember: _shouldHideMember,
            shouldHideEquip: _shouldHideEquip,
            changeCurrentDayMember:_changeCurrentDayMember,
            changeCurrentDayEquipment: _changeCurrentDayEquipment,
            calcHasPermanentMemberChanges: _calcHasPermanentMemberChanges,
            memberPermanent: _memberPermanent,
            checkMemberPermanent: _checkMemberPermanent,
            calcHasPermanentEquipChanges: _calcHasPermanentEquipChanges,
            equipPermanent: _equipPermanent,
            checkEquipPermanent: _checkEquipPermanent,
            getTaskGroups: _getTaskGroups,
            addTaskToJob: _addTaskToJob,
            removeTask: _removeTask,
            defaultNewTask: _defaultNewTask,
            newJobTaskPhaseCodeReturn: _newJobTaskPhaseCodeReturn,
            taskPhaseCodeReturn: _taskPhaseCodeReturn,
            calcShowTaskRows: _calcShowTaskRows,
            changePhaseHours: _changePhaseHours,
            updateTaskHoursEntry: _updateTaskHoursEntry,
            updateTaskTime: _updateTaskTime,
            updateMemberTaskTime: _updateMemberTaskTime,
            updateMemberTaskHoursEntry : _updateMemberTaskHoursEntry,
            changeJobHourRatio: _changeJobHourRatio,
            changeJobMeals: _changeJobMeals,
            changeMemberJobMeals: _changeMemberJobMeals,
            changeMemberMeals: _changeMemberMeals,
            completeWork: _completeWork,
            showUpSites: _showUpSites,
            openLocationPercents: _openLocationPercents,
            initMemberTime: _initMemberTime,
            totalMemberTime: _totalMemberTime,
            changeMemberJobHours: _changeMemberJobHours,
            changeMemberTaskHours: _changeMemberTaskHours,
            changeMemberPhaseHours: _changeMemberPhaseHours,
            parseTimeToOneMember: _parseTimeToOneMember,
            clearMemberTime: _clearMemberTime,
            removeMemberTask: _removeMemberTask,
            changedMemberStatus: _changedMemberStatus,
            checkPerDiem: _checkPerDiem,
            updatePerDiemAmount: _updatePerDiemAmount,
            updateMemberJobPerDiemAmount:  _updateMemberJobPerDiemAmount,
            initEquipmentTime: _initEquipmentTime,
            parseTimeToOneEquipment: _parseTimeToOneEquipment,
            clearEquipmentTime: _clearEquipmentTime,
            changedEquipmentStatus: _changedEquipmentStatus,
            changeEquipJobHours: _changeEquipJobHours,
            changeEquipTaskHours: _changeEquipTaskHours,
            changeEquipPhaseHours: _changeEquipPhaseHours,
            calcShowSummaryRows: _calcShowSummaryRows,
            initSummary: _initSummary,
            viewMemberWeek: _viewMemberWeek,
            viewEquipWeek: _viewEquipWeek,
            viewMemberWeekResult: _viewMemberWeekResult,
            viewEquipWeekResult: _viewEquipWeekResult,
            review: _review,
            revertReview: _revertReview,
            reviewDone: _reviewDone,
            mergeEquipTimeEntries: _mergeEquipTimeEntries,
            addJobsDefaultData: _addJobsDefaultData,
            trackNewJob: _trackNewJob,
            isLocationTask: _isLocationTask,
            isLocationJob: _isLocationJob,
            getReportLevelData: _getReportLevelData,
            cleanReportSelectionData: _cleanReportSelectionData,
            finishLocationPercents: _finishLocationPercents,
            isCompleteWorkJob: _isCompleteWorkJob,
            isOverheadJob: _isOverheadJob,
            updateTaskPhotos: _updateTaskPhotos,
            updateJobPhotos: _updateJobPhotos,
            syncSafetyReviewed: _syncSafetyReviewed,
            changedSafetyData: _changedSafetyData,
            checkRequiredComms: _checkRequiredComms,
            unsetMemberSafety: _unsetMemberSafety,

            changes: _changes,
            save: _save,
            load: _load,

            jdcStatusMeanings: _jdcStatusMeanings,
            statusMeanings: _reviewStatusMeanings,

            parseTime: _parseTime,
            formatTime: _formatTime
        };

    }]);


})();

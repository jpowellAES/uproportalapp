(function(){
    'use strict';

    angular.module('app').controller('LoginController', ['$scope','AuthService', 'StorageService', '$window', '$rootScope', 'environment', 'packageName', function($scope, AuthService, StorageService, $window, $rootScope, environment, packageName) {
        $scope.username = '';
        $scope.password = '';
        $scope.savePassword = false;
        $scope.init = true;
        $scope.working = false;
        $scope.button = 'Log In'
        $scope.savePassword = false;
        $scope.env = environment;

        var doLogin = function(){
            AuthService.login($scope.username, $scope.password, $scope.savePassword).then(function(resp){
                loginResult(resp && resp.success, resp);
            }, function(resp){
                loginResult(false, resp);
            });
        };

        var compareVersions = function(appVersion, serverVersion){
            var parseVersion = function(str){
                var parts = str.split('.');
                return (parts[0]*100000)+(parts[1]*1000)+(parts[2]*10);
            };
            return parseVersion(appVersion) >= parseVersion(serverVersion);
        }

        var compareStoreVersion = function(serverVersion, done){
            if(typeof AppUpdate == 'undefined' || !AppUpdate){
                if(serverVersion){
                    done(serverVersion);
                }else{
                    done(false);
                }
                return;
            }
            AppUpdate.checkAppUpdate(
              function(latestVersion){
                done(latestVersion);
              },
              function(fail){
                done(false)
              }, packageName, {}
            );
        }
        var loginResult = function(success, data){
            if(success){
                var serverVersion = data && data.data ? data.data.version : false;
                compareStoreVersion(serverVersion, function(version){
                    if(serverVersion){
                        if(!compareVersions(config.version, serverVersion) && !compareVersions(config.version, version)){
                            var forceUpdate = data && data.data && data.data.forceUpdate == 'Y';
                            var msg = "Your app version <b>"+config.version+"</b> is not the latest version (<b>"+version+"</b>). Would you like to update?";
                            var title = 'Update Available';
                            if(forceUpdate){
                                msg = "Your app version <b>"+config.version+"</b> is not the latest version (<b>"+version+"</b>). Would you like to update now?";
                                title = 'Update Required';
                            }
                            ons.notification.confirm(msg, {
                                title: title,
                                buttonLabels: ["Not Now","Update"],
                                callback: function(buttonIndex){
                                    if(buttonIndex == 1){
                                        var storeUrl = 'market://details?id='+packageName;
                                        if(cordova.platformId == 'ios'){
                                            storeUrl = 'itms-apps://itunes.apple.com/app/'+config.appleId;
                                        }
                                        cordova.InAppBrowser.open(storeUrl, '_system');

                                        AuthService.logout();
                                    }else{
                                        if(!forceUpdate){
                                            StorageService.set('MyPersonID',data.data.person)
                                            goToApp();
                                        }else{
                                            AuthService.logout();
                                        }
                                    }
                                }
                            });
                            $scope.working = false;
                            $scope.button = 'Log In'
                            return;  
                        }
                    }
                    StorageService.set('MyPersonID',data.data.person)
                    goToApp();
                });
            }else{
                $scope.working = false;
                $scope.button = 'Log In'
                if(data && data.message){
                    ons.notification.alert(data.message, {title: "Login Failure"});
                }
                
            }
        };


        var goToApp = function(){
            mainNav.resetToPage('app/app.html');
            AuthService.setStatusBarStyle(true);
            $rootScope.$emit('AuthService', {type: 'login'});
        };

        $scope.forgotPassword = function(){
            mainNav.pushPage('app/login/forgot-password.html', { animation : 'fade' });
        }
        $scope.forgotPassword = function(){
            mainNav.pushPage('app/login/forgot-password.html', { animation : 'fade' });
        }
        $scope.signUp = function(){
            mainNav.pushPage('app/login/sign-up.html', { animation : 'fade' });
        }

        $scope.login = function(){
            if($scope.username == '' || $scope.password == ''){
                ons.notification.alert("Please enter a username and password", {title: "Login Failure"});
                return;
            }
            $scope.working = true;
            $scope.button = 'Logging In'
            doLogin();
        };

        
        var checkCurrentLogin = function(){
            AuthService.restore();
            var lastLogin = AuthService.getLastLogin();
            $scope.username = lastLogin.username;
            if(lastLogin.password){
                $scope.password = lastLogin.password;
                $scope.savePassword = true;
            }

            if(AuthService.isAuthenticated()){
                mainNav.once('show',goToApp);
            }else{
                $scope.init = false;
            }            
        };

        if($window.plugins&&$window.plugins.webintent){
            $window.plugins.webintent.onNewIntent(function(url) {
                var ref;
                if(url && (ref =url.match(/https?:\/\/portal.unitspro.com\/reset-password\?t=(.+)/i))){
                    mainNav.resetToPage('app/login/reset-password.html', { animation : 'fade', token: ref[1] });
                }
            });
        }

        $scope.show = function(){
            if(!AuthService.isAuthenticated()){
                AuthService.setStatusBarStyle(false);
            }
        }
        checkCurrentLogin();
    }]);

    // Service
    angular.module('app').service('AuthService', ['ApiService', 'StorageService', 'ModuleService', '$rootScope', '$window', 'environment', function(ApiService, StorageService, ModuleService, $rootScope, $window, environment){
        
        var _session = null;
        var _login = function(username, password, savePassword, version){
            StorageService.clear(['last_login']);
            var data = {username: username, password:password, data: {platform:cordova.platformId, version: config.version}};
            if(environment)
                data.environment = environment;
            return ApiService.post('login', data, true, true).then(function(resp){
                if(resp && resp.success){
                    _session = resp;
                    _save();
                    _setLastLogin(username, password, savePassword);
                }
                return resp;
            },function(){});
        };
        var _resetPassword = function(username, token, password){
            var data = {username: username||'', token: token, password:password, data: {platform:cordova.platformId, version: config.version}};
            if(environment)
                data.environment = environment;
            return ApiService.post('resetPassword', data, true, true);
        };
        var _signUp = function(data){
            if(environment)
                data.environment = environment;
            return ApiService.post('signUp', data, true, true);
        };
        var _logout = function() {
            return ApiService.post('logout').then(_clearSession,_clearSession);
        };
        var _clearSession = function(){
            _session = null;
            StorageService.clear(['last_login']);
        };
        var _isAuthenticated = function(skipTimeout){
            if(_session == null) return false;
            if(!skipTimeout) return _checkSessionTimeout();
            return true;
        };
        var _restore = function(){
            _session =  StorageService.get('session') || null;
            if(_session){
                _checkSessionTimeout();
            }
        };
        var _setLastLogin = function(username, password, savePassword){
            var lastLogin = {username: username};
            if(savePassword)
                lastLogin.password = password;
            StorageService.set('last_login', lastLogin);
        };
        var _getLastLogin = function(){
            var lastLogin = StorageService.get('last_login');
            if(lastLogin && lastLogin.username){
                return lastLogin;
            }
            return {username: ''};
        };
        var _save = function(){
            if(_session && !_session['expires']){
                _session['expires'] = new Date().getTime() + (_session['lengthMinutes']||120)*60*1000;
            }
            StorageService.set('session', _session);
            if(_session && _session.data && _session.data.permissions){
                ModuleService.initModules(_session.data.permissions);
            }
        }
        var _renew = function(){
            if(_session){
                _session['expires'] = new Date().getTime() + (_session['lengthMinutes']||120)*60*1000;
                StorageService.set('session', _session);
            }
        }

        var _checkSessionTimeout = function(){
            if(!_session['expires'] || _session['expires'] <= new Date().getTime()){
                _session = null;
                _save();
                return false;
            }
            return true;
        };

        var _getSessionVar = function(key){
            if(_session && _session.data && _session.data[key]){
                return _session.data[key];
            }
        };

        var _subscribe = function(scope, callback) {
            var handler = $rootScope.$on('AuthService', callback);
            scope.$on('$destroy', handler);
        };

        var _notify = function() {
            $rootScope.$emit('AuthServiceTimeout');
        };

        var _statusBarInit = 0;
        var _statusBarTimeout = null;
        var _setStatusBarStyleExecute = function(loggedIn){
            if(typeof loggedIn == 'undefined') loggedIn = _isAuthenticated();
            if(loggedIn){
                document.body.style.backgroundColor = '#333';
                StatusBar.styleLightContent();
                //if (cordova.platformId == 'android') {
                    StatusBar.backgroundColorByHexString("#333");
                //}
            }else{
                document.body.style.backgroundColor = '#efeff4';
                StatusBar.styleDefault();
                //if (cordova.platformId == 'android') {
                    StatusBar.backgroundColorByHexString("#efeff4");
                //}
            }
        }
        var _statusBarInitRepeat = function(loggedIn){
            if(_statusBarTimeout){
                _setStatusBarStyleExecute(loggedIn);
                _statusBarInit++;
                if(_statusBarInit < 30){
                    _statusBarTimeout = setTimeout(_statusBarInitRepeat.bind(null, loggedIn), 100)
                }else{
                    _statusBarTimeout = null;
                }
            }
        }
        var _setStatusBarStyle = function(loggedIn){
            if(_statusBarTimeout){
                clearTimeout(_statusBarTimeout);
                _statusBarTimeout = null;
            }
            if(_statusBarInit === 0){
                _statusBarInit = 1;
                _statusBarTimeout = setTimeout(_statusBarInitRepeat.bind(null, loggedIn), 100)
            }else{
                _setStatusBarStyleExecute(loggedIn);
            }
        }

        var inBackground = false, heartbeat = null, lastHeartbeat = 0;
        var _heartbeat = function(){
            if(_isAuthenticated()){
                ApiService.get('heartbeat', {}, true, true)
            }
            heartbeat = setTimeout(_heartbeat, 60000)
        }
        var _startHeartbeat = function(){
            document.addEventListener("deviceready", function(){
                document.addEventListener("resume", function(){
                    inBackground = false;
                    if(!heartbeat){
                        heartbeat = setTimeout(_heartbeat, Math.max(0, Math.min(60000, (60000-(Date.now()-lastHeartbeat)))))
                    }
                    _setStatusBarStyle()
                }, false);
                document.addEventListener("pause", function(){
                    inBackground = true;
                    if(heartbeat){
                        lastHeartbeat = Date.now()
                        clearTimeout(heartbeat)
                    }
                }, false);
                if(!heartbeat){
                    heartbeat = setTimeout(_heartbeat, 60000)
                }
            }, false);
        }
        _startHeartbeat()

        return {
            login: _login,
            logout: _logout, 
            resetPassword: _resetPassword,
            renew: _renew,
            clearSession: _clearSession,
            isAuthenticated: _isAuthenticated,
            restore: _restore,
            subscribe: _subscribe,
            notify: _notify,
            getSessionVar: _getSessionVar,
            signUp: _signUp,
            getLastLogin: _getLastLogin,
            setStatusBarStyle: _setStatusBarStyle
        };
    }]);

    //Forgot password controller
    angular.module('app').controller('ForgotPasswordController', ['$scope','AuthService', function($scope, AuthService) {
        $scope.username = '';
        $scope.working = false;
        $scope.button = 'Submit'

        var doForgotPassword = function(){
            AuthService.resetPassword($scope.username).then(function(resp){
                result(resp && resp.success, resp);
            }, function(resp){
                result(false, resp);
            });
        };

        var result = function(success, data){
            if(success){
                if(data.message){
                    ons.notification.alert(data.message, {title: "Success"});
                }
                mainNav.popPage();
            }else{
                if(data.message){
                    ons.notification.alert(data.message, {title: "Error"});
                }
                $scope.working = false;
                $scope.button = 'Submit'
            }
        };

        $scope.submit = function(){
            if($scope.username == ''){
                ons.notification.alert("Please enter a username", {title: "Error"});
                return;
            }
            $scope.working = true;
            $scope.button = 'Submitting'
            doForgotPassword();
        };

        $scope.cancel = function(){
            mainNav.popPage();
        }

    }]);

    //Reset password controller
    angular.module('app').controller('ResetPasswordController', ['$scope','AuthService', function($scope, AuthService) {
        $scope.password = '';
        $scope.verifyPassword = '';
        $scope.working = false;
        $scope.button = 'Reset'
        $scope.token = mainNav.topPage.data.token;

        var doResetPassword = function(){
            AuthService.resetPassword('', $scope.token, $scope.password).then(function(resp){
                result(resp && resp.success, resp);
            }, function(resp){
                result(false, resp);
            });
        };

        var result = function(success, data){
            if(success){
                if(data.message){
                    ons.notification.alert(data.message, {title: "Success"});
                }
                mainNav.resetToPage('app/login/login.html');
            }else{
                if(data.message){
                    ons.notification.alert(data.message, {title: "Error"});
                }
                $scope.working = false;
                $scope.button = 'Reset'
            }
        };

        $scope.reset = function(){
            if($scope.password == '' && $scope.verifyPassword == ''){
                ons.notification.alert("Please enter & verify new password", {title: "Error"});
                return;
            }
            if($scope.verifyPassword == ''){
                ons.notification.alert("Please verify new password", {title: "Error"});
                return;
            }
            if($scope.password != $scope.verifyPassword){
                ons.notification.alert("Passwords do not match", {title: "Error"});
                return;
            }
            $scope.working = true;
            $scope.button = 'Resetting'
            doResetPassword();
        };

        $scope.cancel = function(){
           mainNav.resetToPage('app/login/login.html');
        };

        $scope.show = function(){
            document.body.style.backgroundColor = '#efeff4';
            StatusBar.styleDefault();
            if (cordova.platformId == 'android') {
                StatusBar.backgroundColorByHexString("#efeff4");
            }
        }
        
    }]);

    //Demo sign up controller
    angular.module('app').controller('SignUpController', ['$scope','AuthService', function($scope, AuthService) {
        $scope.data = {
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            company: ''
        };
        $scope.working = false;
        $scope.button = 'Submit'

        var doSignUp = function(){
            AuthService.signUp($scope.data).then(function(resp){
                result(resp && resp.success, resp);
            }, function(resp){
                result(false, resp);
            });
        };

        var result = function(success, data){
            if(success){
                if(data.message){
                    ons.notification.alert(data.message, {title: "Success"});
                }
                mainNav.popPage();
            }else{
                if(data.message){
                    ons.notification.alert(data.message, {title: "Error"});
                }
                $scope.working = false;
                $scope.button = 'Submit'
            }
        };

        $scope.submit = function(){
            if($scope.data.firstName == ''){
                ons.notification.alert("Please enter a first name", {title: "Error"});
                return;
            }
            if($scope.data.lastName == ''){
                ons.notification.alert("Please enter a last name", {title: "Error"});
                return;
            }
            if($scope.data.email == ''){
                ons.notification.alert("Please enter an email address", {title: "Error"});
                return;
            }
            $scope.working = true;
            $scope.button = 'Submitting'
            doSignUp();
        };

        $scope.cancel = function(){
            mainNav.popPage();
        }
    }]);

})();
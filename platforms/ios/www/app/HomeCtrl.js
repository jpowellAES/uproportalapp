(function(){
    'use strict';

    angular.module('app').controller('HomeController', ['$scope','StorageService', 'ModuleService', function($scope, StorageService, ModuleService) {
        $scope.working = false;

        $scope.goToModule = function(module){
            $scope.$parent.current.module = module;
            $scope.splitter.content.load(module.page);
        }

        var saveStateFields = ['modules','working'];
        $scope = StorageService.state.load('HomeController', $scope, saveStateFields);
        $scope.$on('$destroy', function() {
            StorageService.state.save('HomeController', $scope, saveStateFields, 10);
        });
    }]);

})();
(function(){
    'use strict';
    angular.module('app').controller('AppController', ['$scope', 'AuthService', 'ModuleService', '$rootScope', '$timeout', 'environment', 'ImgOrPdf', function($scope, AuthService, ModuleService, $rootScope, $timeout, environment, ImgOrPdf) {

        $scope.current = {};
        $scope.version = config.version;


        $scope.env = environment;
        $scope.goToInfo = function(){
            mainNav.pushPage('app/sales-info.html');
        }
        var _this = this;

        this.menuNav = function(page) {
          $scope.splitter.content.load(page);
          $scope.splitter.left.close();
        };

        this.changeModule = function(module){
            $scope.current.module = module;
            $scope.splitter.content.load(module.page);
            $scope.splitter.left.close();
        }
 
        this.toggle = function() {
          $scope.splitter.left.toggle();
        };

        var closeHandler = function(){
            if($scope.splitter.content.page != 'home.html'){
                var dialogs = []
                document.querySelectorAll('ons-dialog,ons-alert-dialog').forEach(function(d){ if(d.style.display != 'none'){ dialogs.push(d)}})
                if(dialogs.length){
                    dialogs = dialogs.sort(function(a,b){ return (b.style.zIndex||0) - (a.style.zIndex||0)})
                    if(dialogs[0].cancelable){
                        dialogs[0].hide();
                    }
                    return;
                }
                _this.menuNav('home.html');
            }
        }
        var init = false;
       var postpop = function(){
            if(mainNav.pages.length == 1){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', closeHandler, false);
            }
        }
        var prepush = function(){
            if(mainNav.pages.length == 1){
                if(init){
                    document.removeEventListener('backbutton', closeHandler);
                    ons.enableDeviceBackButtonHandler();
                }else{
                    postpop()
                    init = true;
                }
            }
        }
        ons.ready(function(){
            mainNav.on('prepush', prepush);
            mainNav.on('postpop', postpop);
        })

        $rootScope.$on('AuthService', function(evt, args) {
            switch(args.type){
                case 'expired':
                    if(AuthService.isAuthenticated(true)){
                        ons.notification.alert("You have been logged out due to inactivity");
                        mainNav.resetToPage('app/login/login.html');
                        AuthService.clearSession();
                    }
                break;
                case 'renew':
                    AuthService.renew();
                break;
                case 'login':
                    $scope.modules = ModuleService.getModules();
                    $scope.OrgType = AuthService.getSessionVar('OrgType');
                break;
            }
        });

        var goToLogin = function(){
            $scope.splitter.left.once('postclose', function(e){
                mainNav.resetToPage('app/login/login.html');
            });
            $scope.splitter.left.close();
        };

        this.logout = function(){
            AuthService.logout().then(goToLogin, goToLogin);      
        };
    }]);

    angular.module('app').controller('SalesInfoController', ['$scope', '$window', function($scope, $window) {
        $scope.openSite = function(){
            cordova.InAppBrowser.open('http://www.unitspro.com', '_system');
        }
    }]);

})();
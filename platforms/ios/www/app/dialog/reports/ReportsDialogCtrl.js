(function(){
    'use strict';

    angular.module('app').controller('ReportsDialogController', ['$scope', 'ImgOrPdf', 'urls', function($scope, ImgOrPdf, urls) {
       
        $scope.openingReport = false;
        $scope.reports = [];
        
        $scope.openReport = function(report){
            if($scope.openingReport)
                return;
            var path = 'report/'+report.Token;
            $scope.openingReport = true;
            var downloadUrl =  urls.media+'/'+path;
            ImgOrPdf.load(path, downloadUrl, report.Name).finally(function(){
                $scope.openingReport = false;
            });
        }
    }]);

})();
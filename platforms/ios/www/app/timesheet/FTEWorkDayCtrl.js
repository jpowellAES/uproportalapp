(function(){
    'use strict';

    angular.module('app').controller('FTEWorkDayController', ['$scope', 'ApiService', 'Utils', '$rootScope', '$timeout', '$anchorScroll', 'TimeEntryService', 'Rentals', 'updateTableHighlight', '$sce', 'TECompletionService', 'UploadDialog', 'ImgOrPdf', function($scope, ApiService, Utils, $rootScope, $timeout, $anchorScroll, TimeEntryService, Rentals, updateTableHighlight, $sce, TECompletionService, UploadDialog, ImgOrPdf) {
        
        $scope.current = $scope.$parent.current;
        $scope.workweek = $scope.$parent.current.workweek;
        $scope.original = $scope.$parent.current.original;
        $scope.dialogs = {}
        var share = $scope.$parent.current.share;
        $scope.share = share;
        $scope.getStatusClass = $scope.$parent.current.share.getStatusClass;
        $scope.fromGeneralTime = $scope.$parent.current.fromGeneralTime;
        $scope.fromDayCompleteWork = $scope.$parent.current.fromDayCompleteWork;
        delete $scope.$parent.current.fromDayCompleteWork;

        if($scope.current.completionDone||$scope.current.showupSitesDone){
            var day = TimeEntryService.currentDay($scope.workweek)
            var completion = $scope.current.completionDone;
            var jobScreen = completion?day.cwJobScreen:null;
            $scope.loading = true;
            share.reload(function(ww){
                $scope.workweek = ww;
                $scope.loading = false;
            }, {currentDay:$scope.workweek.currentDay, FromCompleteWork:completion, jobScreen:jobScreen}, $scope, {completedWork:completion,date:Utils.date(TimeEntryService.currentDay($scope.workweek).WeekdayDate,'M/d/yyyy')});
        }

        var _initProgress = false;
        $scope.initMembersCarousel = function(){
            var day = TimeEntryService.currentDay($scope.workweek);
            if(membersCarousel.getActiveIndex() != day.currentMember){
                membersCarousel.setActiveIndex(day.currentMember);
            }
        }

        $scope.callbacks = Utils.copy($scope.$parent.current.callbacks);
        $scope.checkCallback = function(when){
            when = when||'beforeInit';
            var handled = false, init = $scope.workweek.Init;
            $scope.workweek.Init = false;
            if($scope.workweek.callback && $scope.callbacks[$scope.workweek.callback]){
                var callback = $scope.callbacks[$scope.workweek.callback];
                if(typeof callback == 'function') callback = {when:'beforeInit', fn: callback}
                if(when == callback.when){
                    callback.fn($scope.workweek.callbackData);
                    if(!init) handled = $scope.workweek.callback;
                    delete $scope.workweek.callback;
                    delete $scope.workweek.callbackData;
                }
            }
            return handled;
        }

        $scope.toastJobDescription = function(job, screen){
            screen = screen || 'ct';
            Utils.notification.hideToast();
            Utils.notification.toast(screen=='ct'?job.Description:job.Job.Description, {timeout:2000});
            Utils.flash('ft-'+screen+'-job-'+(screen=='ct'?job.JobCrewJobID:job.Job.ID));
        }
        $scope.toastTaskDescription = function(task, screen){
            screen = screen || 'ct';
            Utils.notification.hideToast();
            Utils.notification.toast(screen=='ct'?task.JobTaskDescription:task.Task.JobTaskDescription, {timeout:2000});
            Utils.flash('ft-'+screen+'-task-'+(screen=='ct'?task.ID:task.Task.ID));
        }
        $scope.toastPhaseDescription = function(pc, screen){
            screen = screen || 'ct';
            Utils.notification.hideToast();
            Utils.notification.toast(screen=='ct'?pc.Description:pc.PhaseCode.Description, {timeout:2000});
            Utils.flash('ft-'+screen+'-pc-'+(screen=='ct'?pc.ID:pc.PhaseCode.ID));
        }

        $scope.loadingDialog = function(text){
            if(text){
                Utils.notification.toast(text);
            }
        }
        $scope.changes = function(changes){
            TimeEntryService.changes($scope.workweek,changes);
        }

        $scope.nextButtonVisible = ['Approve','Reviewed','Submit','Resubmit','Reopen'];
        $scope.getNextButtonLabel = function(processStepCode, currentSubstep, nextIsCompleteWork, dayStatusCode, hideReject, dayHasBilledItems){
            if(processStepCode == 'CT'){
                if(currentSubstep == 0 && nextIsCompleteWork){
                    return 'Complete Work'
                }
            }else if(processStepCode == 'RS'){
                if($scope.workweek.AllowApproval && ['SB','RV'].includes(dayStatusCode)){
                    return 'Approve'
                }
                if($scope.workweek.UseReviewer && $scope.workweek.AllowReview && dayStatusCode == 'SB'){
                    return 'Reviewed'
                }
                if(($scope.workweek.AllowReview  || $scope.workweek.AllowForeman || $scope.workweek.AllowApproval) && dayStatusCode == 'RJ'){
                    return 'Resubmit'
                }
                if(['PE','IP'].includes(dayStatusCode)){
                    return 'Submit'
                }
                if(($scope.workweek.AllowReview  || $scope.workweek.AllowForeman || $scope.workweek.AllowApproval) && ['SB','RV'].includes(dayStatusCode) && hideReject){
                    return 'Reopen'
                }
                if($scope.workweek.AllowApproval && dayStatusCode == 'AP' && !dayHasBilledItems){
                    return 'Reopen'
                }
                return TimeEntryService.jdcStatusMeanings[dayStatusCode];
            }
            return 'Next'
        }
        var navStep = function(dir){
            if(dir > 0 && $scope.nextButtonVisible.includes($scope.nextLabel)){
                return doReview($scope.nextLabel);
            }
            if(dir < 0 && $scope.editable && !$scope.forceNav){
                if(TimeEntryService.currentStep($scope.workweek).ProcessStepCode == 'MT'){
                    Utils.notification.confirm({
                          title: 'Back to Crew Time?',
                          message: 'Changes to member or equipment time will be lost.',
                          callback: function(ok){
                            if(ok == 0){
                              $scope.forceNav = true;
                              progress.prev();
                            }
                          },
                          buttonLabels: ["Continue","Cancel"]
                    });
                    return 0;
                }
            }
            delete $scope.forceNav;
            return TimeEntryService.navStep($scope, $scope.workweek, dir);
        }
        $scope.setCurrentStep = function(index, lastIndex){
            if(_initProgress){
                _initProgress = false;
                return;
            }
            var ok;
            if(index > lastIndex){
                ok = navStep(1);
            }else if(index < lastIndex){
                ok = navStep(-1);
            }
            if(!ok){
                _initProgress = true;
                progress.setActiveIndex(lastIndex);
            }
        }
        $scope.progressStep = function(dir){
            if(navStep(dir)){
                _initProgress = true;
                if(dir > 0){
                    progress.next();
                }else{
                    progress.prev();
                }
            }
        }
        $scope.navDay = function(dir){
            $scope.workweek.currentDay += dir;
            TimeEntryService.initDay($scope);
            _initProgress = true;
            var day = TimeEntryService.currentDay($scope.workweek);
            progress.setActiveIndex(day.currentStep>2?(day.currentStep+1):(day.currentStep==2?(day.currentStep+day.DayProcessCompletion[day.currentStep].subSteps.current):day.currentStep));
        }

        $scope.increaseJobLimit = function(type){
            var current = addJobTypesMap[type]
            if(current.limitTo){
                current.limitTo += 10;
            }
        }

        var reviewCodes = {
            'Approve': 'AP',
            'Reviewed': 'RV',
            'Resubmit': 'SB',
            'Submit': 'SB',
            'Reopen': 'IP',
            'Reject': 'RJ'
        }
        var doReview = function(status){
            var code = reviewCodes[status];
            if(code){
                $scope.review(code, false, $scope.workweek.currentDay);
            }
        }

        $scope.openCommunication = function(com, language){
            language = language||'';
            com.reviewed = true;
            TimeEntryService.syncSafetyReviewed(com, $scope.workweek);
            if(com.TypeCode == 'DOC'){
                $scope.loading = true;
                ImgOrPdf.load('communication/'+com['FileString'+language]).finally(function(){ $scope.loading = false; });
            }else{
                cordova.InAppBrowser.open(com['URL'+language], '_system');
            }
        }
        $scope.markCommuncationDone = function(com){
            var ww = $scope.workweek;
            var day = ww.WorkDay[ww.currentDay];
            var members = day.JobDayCrew[0].JobDayCrewMember;
            for(var i=0; i<members.length; ++i){
                if(!TimeEntryService.shouldHideMember(members[i], ww) && members[i].ActiveFlag && com.people[members[i].PersonID]){
                    com.people[members[i].PersonID].changed = true
                    com.people[members[i].PersonID].ReviewedFlag = true
                    com.people[members[i].PersonID].ReviewedDate = Utils.date(day.WeekdayDate,'MM/dd/yyyy');
                }
            }
            $scope.changedCommunicationReview(com)
        }
        $scope.changedCommunicationReview = function(com, person){
            $scope.changes(true);
            if(person){
                if(person.ReviewedFlag){
                    person.ReviewedDate = Utils.date($scope.workweek.WorkDay[$scope.workweek.currentDay].WeekdayDate,'MM/dd/yyyy');
                }else{
                    person.ReviewedDate = '';
                }
                person.changed = true
            }
            TimeEntryService.changedSafetyData($scope.workweek, com, person)
        }

        $scope.getEditableClass = function(day){
            if(TimeEntryService.isDayEditable(day, $scope.workweek)){
                return 'editable';
            }
            return ''
        }

        $scope.addJobTypes = [{
            Code: 'AJ', Name: 'Assigned', filter: {Type:'AJ'}, limitTo: 10
        },{
            Code: 'OT', Name: 'Other', filter: {Type:'OT'}, limitTo: 10
        },{
            Code: 'OJ', Name: 'Overhead', filter: {Type:'OJ'}, limitTo: 10
        },{
            Code: 'WGC', Name: 'Contracts', filter: {Type:''}
        }];
        var addJobTypesMap = {'AJ':$scope.addJobTypes[0],'OT':$scope.addJobTypes[1],'OJ':$scope.addJobTypes[2],'WGC':$scope.addJobTypes[3]}
        $scope.filterAddJobs = function(code, statusFilter, numberFilter){
            if(code == 'WGC') return;
            var filter = ($scope.addJobTypes.find(function(jt){return jt.Code == code})||{filter:{Type:''}}).filter, regex = null;
            if(statusFilter && statusFilter.Code != 'ALL'){
                filter = Object.assign({StatusCode: statusFilter.Code}, filter);
            }
            var keys = Object.keys(filter);
            if(numberFilter && numberFilter.length){
                var regex = new RegExp(Utils.sanitizeTermForRegex(numberFilter), 'i');
                return function(item){
                    if(item.selected && item.Type==filter.Type) return true;
                    var match = item.NumberChar.match(regex)||item.WorkgroupJobNumber.match(regex)||item.Description.match(regex);
                    if(match){
                        for(var i=0; i<keys.length; i++){
                            if(filter[keys[i]] != item[keys[i]])
                                return false;
                        }
                        return true;
                    }
                    return false;
                }
            }else{
                return function( item ) {
                    if(item.selected && item.Type==filter.Type) return true;
                    for(var i=0; i<keys.length; i++){
                        if(filter[keys[i]] != item[keys[i]])
                            return false;
                    }
                    return true;
                }
            }
            
        }
        $scope.showJobBadge = function(jobs, type){
            var num = jobs.filter(function(job){return job.Type==type&&(job.selected||job.crewEquipSelected)}).length;
            if(num){
                return num;
            }
            return '';
        }

        $scope.inactivateCrew = function(job, code){
            if(code != 'AJ') return;
            navigator.vibrate(10);
            Utils.notification.toast('Job Crew Inactivated', {timeout: 2000 });
            TimeEntryService.inactivateCrew($scope.workweek, job);
        }

        $scope.selectStartJob = function(job, $event){
            $event.stopPropagation();
            job.selected = !!job.selected2;
            if($scope.beginDayDone||!$scope.editable) return false;
            TimeEntryService.selectStartJob($scope.workweek, job);
            job.selected2 = job.selected;
            return false;
        }

        var defaultStatus = function(){
            if($scope.workweek.filters.jobStatus){
                return;
            }
            var pr = $scope.workweek.jobStatuses.find(function(st){return st.Code == 'PR'});
            if(pr){
                $scope.workweek.filters.jobStatus = pr;
            }else if($scope.workweek.jobStatuses.length){
                $scope.workweek.filters.jobStatus = $scope.workweek.jobStatuses[0];
            }
        }
        $timeout(defaultStatus);
        $scope.callbacks.addJobsContractCallback = function(data){
            if(data){
                $scope.workweek.WGManagedContract = Utils.enforceArray(data.WGManagedContract);
            }
        }
        $scope.changeJobTab = function(event){
            //Load contracts
            if(event.index == 3){
                if(typeof $scope.workweek.WGManagedContract == "undefined"){
                    TimeEntryService.load($scope, 'fte/addJob/contracts', 'GET', {
                        org:$scope.workweek.PersonOrganizationID,
                        person:$scope.workweek.MyPersonID
                    }, 'addJobsContractCallback')
                }
            }
        }
        $scope.callbacks.addJobsSaveCallback = function(data){
            if(data && data.Job){
                TimeEntryService.trackNewJob(data.Job, $scope.workweek);
                jobsTabbar.setActiveTab($scope.addJobTypes.findIndex(function(jt){ return jt.Code==data.Job.Type }));
            }
        }
        $scope.quickAddJob = function(contract){
            mainNav.pushPage('app/addjob/add-job.html', {contract: contract, process: {name:'fte'}});
        }

        $scope.callbacks.loadCrewResult = function(data){
            TimeEntryService.draftCrews($scope, $scope.workweek, data);
        }

        $scope.callbacks.loadCraftClass = function(data){
            if(data && data.Smartpicks){
                $scope.workweek.sp.craft = Utils.enforceArray(data.Smartpicks.Craft);
                $scope.workweek.sp.class = Utils.enforceArray(data.Smartpicks.Class);
                if(data.sp && data.member){
                    var id = TimeEntryService.cleanID(data.member);
                    openSp(data.where, data.sp, id);
                }
            }
        }
        $scope.callbacks.loadCrewReferenceResult = function(data){
            TimeEntryService.loadCrewReferenceData($scope.workweek, $scope, data);
        }
        $scope.loadCraftClass = function(member, sp, where){
            TimeEntryService.load($scope, 'fte/sp/craftClass', 'GET', {org: $scope.workweek.PersonOrganizationID, sp: sp, where: (where||'roster'), member: member.ID }, 'loadCraftClass');
        }
        $scope.filterMembers = function(memberTime){
            if(memberTime){
                return function(item){
                    if(TimeEntryService.shouldHideMember(item.Jobs[0].Member, $scope.workweek)){
                        return false;
                    }
                    return true;
                }
            }
            return function(item){
                if(TimeEntryService.shouldHideMember(item, $scope.workweek)){
                    return false;
                }
                return true;
            }
        }
        $scope.selectCraft = function(member, dayMember){
            delete member.class;
            $scope.mapBack(member, 'craft');
            if(dayMember){
                dayMember.Craft = member.CraftCraft;
                dayMember.CraftSort = member.CraftSortOrder;
            }
            TimeEntryService.syncRosterItem(member, 'JobDayCrewMember', $scope.workweek);
            TimeEntryService.calcHasPermanentMemberChanges(member, $scope.workweek);
            $scope.changes(true);
        }
        $scope.selectClass = function(member){
            $scope.mapBack(member,'class');
            TimeEntryService.syncRosterItem(member, 'JobDayCrewMember', $scope.workweek);
            TimeEntryService.calcHasPermanentMemberChanges(member, $scope.workweek);
            $scope.changes(true);
        }
        $scope.changeMemberInactiveReason = function(member){
            $scope.mapBack(member,'inactiveReason');
            TimeEntryService.calcHasPermanentMemberChanges(member, $scope.workweek);
            TimeEntryService.syncRosterItem(member, 'JobDayCrewMember', $scope.workweek);
            $scope.changes(true);
        }

        $scope.addCrewMember = function(person, perm){
            if(person){
                var member = TimeEntryService.addCrewMember(person, $scope.workweek, $scope);
                if(perm){
                    member.hasPermanentChanges = true;
                }
                return;
            }
            $scope.dialogs.addMemberDialog.open();
        }

        $scope.changeCrewActive = function(which, active, item, itemParent, parentParent){
            var has = 'hasInactive'+which, view = 'JobDayCrewEquipment', ww = $scope.workweek, roster=itemParent==null;
            if(which == 'Member'){
                view = 'JobDayCrewMember';
                item.ActiveFlag = active;
                if(active){
                    delete item.inactiveReason;
                    item.jobStatus = $scope.workweek.sp.jobStatus.find(function(status){ return status.Code == 'AC' })
                }else{
                    item.inactiveReason = ww.sp.memberInactiveReason.find(function(r){ return r.SmartCode == 'OJ' });
                    item.jobStatus = $scope.workweek.sp.jobStatus.find(function(status){ return status.Code == 'IN' })
                    if(roster && ww.WorkDay[ww.currentDay].hasSafety){
                        TimeEntryService.unsetMemberSafety(ww, item.PersonID);
                    }
                }
                $scope.mapBack(item, 'jobStatus');
                $scope.mapBack(item, 'inactiveReason');
                TimeEntryService.syncRosterItem(item, view, ww, true);
                TimeEntryService.changedMemberStatus(itemParent, parentParent, ww);
                TimeEntryService.calcHasPermanentMemberChanges(item, $scope.workweek);
            }else{
                view = 'JobDayCrewEquipment';
                if(active){
                    delete item.inactiveReason;
                }else{
                    item.inactiveReason = ww.sp.equipmentInactiveReason.find(function(r){ return r.SmartCode == 'ID' });
                }
                $scope.mapBack(item,'inactiveReason');
                TimeEntryService.syncRosterItem(item, view, ww, true);
                TimeEntryService.changedEquipmentStatus(itemParent, parentParent, ww);
                TimeEntryService.calcHasPermanentEquipChanges(item, $scope.workweek);
            }
            var job = ww.WorkDay[ww.currentDay].JobDayCrew[0], hasInactive = false;
            for(var i=0; i<job[view].length; ++i){
                if(!job[view][i].ActiveFlag && (which != 'Equip'||job[view][i].EquipmentTypeCode=='SM') && (which != 'Rental'||job[view][i].EquipmentTypeCode=='SR')){
                    hasInactive = true;
                    break;
                }
            }
            job[has] = hasInactive;
            $scope.changes(true);
        }


        $scope.filterEquip = function(typeCode, equipTime){
            if(equipTime){
                return function(item){
                    var equip = item.Equip||item;
                    if((typeCode&&equip.TypeCode != typeCode) || TimeEntryService.shouldHideEquip(equip.Jobs[0].Equipment)){
                        return false;
                    }
                    return true;
                }
            }
            return function(item){
                var equip = item.Equip||item;
                if((typeCode&&equip.EquipmentTypeCode != typeCode) || TimeEntryService.shouldHideEquip(equip)){
                    return false;
                }
                return true;
            }
        }

        $scope.changeEquipInactiveReason = function(equip){
            $scope.mapBack(equip,'inactiveReason');
            TimeEntryService.calcHasPermanentEquipChanges(equip, $scope.workweek);
            TimeEntryService.syncRosterItem(equip, 'JobDayCrewEquipment', $scope.workweek);
            $scope.changes(true);
        }

        $scope.addCrewEquipment = function(equip, perm){
            if(equip){
                var equipment = TimeEntryService.addCrewEquipment(equip, $scope.workweek, $scope);
                if(perm){
                    equipment.hasPermanentChanges = true;
                }
                return;
            }
            $scope.dialogs.addEquipmentDialog.open();
        }

        $scope.filterAddRentals = function(){
            var ww=$scope.workweek;
            return function(item){
                return (!ww.WorkDay[ww.currentDay].JobDayCrew[0].JobDayCrewEquipment.find(function(eq){return eq.EquipmentID==item.ID&&(eq.ActiveFlag||eq.JobCrewEquipmentActiveFlag)}))
            }
        }
        $scope.addCrewRental = function(equip){
            if(equip){
                TimeEntryService.addCrewRental(equip, $scope.workweek, $scope);
                return;
            }
            Rentals.show({
              id: TimeEntryService.currentDay($scope.workweek).ID,
              which: 'WorkDay',
              orgId: $scope.workweek.PersonOrganizationID,
              noSave: true,
              filter: $scope.filterAddRentals
            }, function(equip){
              if(equip){
                $scope.addCrewRental({
                    category: equip.category,
                    company: equip.company,
                    CategoryCategory: equip.Category,
                    CategoryID: equip.CategoryID,
                    Description: equip.Description,
                    ID: equip.ID,
                    Number: equip.EquipmentNumber,
                    RentalCompanyName: equip.RentalCompany,
                    RentalCompanyID: equip.RentalCompanyID,
                    isNewCompany: equip.isNewCompany,
                    TypeCode: equip.TypeCode
                });
              }
            });
        }

        $scope.setCarouselNav = function(){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            $scope.disableLeft = false;
            $scope.disableRight = day.currentStep < day.stepLimit ? false : true;
        }

        $scope.changeJobs = function(){
            var ww = $scope.workweek;
            ww.filters.jobNumber = '';
            var open = ww.WorkDay[ww.currentDay].DayProcessCompletion[ww.WorkDay[ww.currentDay].currentStep].addingJob;
            $scope.disableNav = !open;
            if(open){
                $scope.setCarouselNav();
                var jobs = $scope.workweek.WorkDay[$scope.workweek.currentDay].Job;
                if(!jobs.find(function(jb){ return jb.selected })){
                    share.dialogs.alert.open({
                        title: 'Error',
                        content: 'You must select at least one job before proceeding.',
                        buttons: [{
                            label: 'Ok'
                        }],
                        cancelable: true
                    });
                }
                TimeEntryService.addRemoveJobs($scope.workweek, $scope.workweek.currentDay);
                TimeEntryService.updateJobHours($scope.workweek, 0);
                $scope.changes(true);
            }else{
                $scope.disableLeft = true;
                $scope.disableRight = true;
                ww.WorkDay[ww.currentDay].weekdayHoursBeforeJobChange = ww.WorkDay[ww.currentDay].WeekdayHours;
            }
            ww.WorkDay[ww.currentDay].DayProcessCompletion[ww.WorkDay[ww.currentDay].currentStep].addingJob = !open;
        }

        $scope.editJob = function(job){
            $scope.dialogs.editJob.open(function(ok, comment){
                if(ok){
                    $scope.jobEditDone(job, comment);
                }
                return true;
            }, {job: job, comment: (job.JobComment||'').toString()});
        }
        $scope.jobEditDone = function(job, comment){
            job.JobComment = (comment||'').trim();
        }
        $scope.editJobDescription = function(job){
            if(job.Type == 'OJ'){
                $scope.openComments(job);
                return;
            }
            $scope.dialogs.editJobDescription.open(function(ok, desc, comment){
                if(ok){
                    if(!desc){
                        $scope.dialogs.editJobDescription._scope.error = 'Please enter a description'
                        return false;
                    }
                    TimeEntryService.changeJobDescription($scope.workweek, job, desc);
                    $scope.jobEditDone(job, comment);
                }
                return true;
            }, {job: job, description: (job.Description||'').toString(), comment: (job.JobComment||'').toString()});
        }

        $scope.moveJob = function(index, dir){
            TimeEntryService.moveJob($scope.workweek, index, dir);
            $scope.changes(true);
        }

        $scope.editJobHours = function(index, job){
            if($scope.loading || !$scope.editable){
                return;
            }
            $scope.dialogs.hours.open(job.GrandTotalHours, function(hours){
                job.GrandTotalHours = hours;
                $scope.updateJobHours(index);
            }, {title:'Hours - '+job.WorkgroupJobNumber});
        }
        $scope.updateJobHours = function(index){
            TimeEntryService.updateJobHours($scope.workweek, index);
            $scope.changes(true);
        }
        $scope.updateJobTime = function(index, which){
            TimeEntryService.updateJobTime($scope.workweek, index, which);
            $scope.changes(true);
        }
        $scope.editJobAllocation = function(job, which, tasks, $event){
            if(!$scope.editable||$scope.loading||!job.GrandTotalHours||!tasks.length||(which=='DT'&&!job.ContractAllowDoubleTimeFlag)){
                return;
            }
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(job[which+'Hours'], function(hours){
                job[which+'Hours'] = hours;
                $scope.changeJobHours(job, which);
            }, {title:which+' Hours - '+job.WorkgroupJobNumber});
            $event.stopPropagation();
        }
        $scope.changeJobHours = function(job, which){
            TimeEntryService.changeJobHourRatio(job, which, $scope.workweek);
            $scope.changes(true);
        }
        $scope.editJobTime = function(job, index, which, noEdit){
            if(noEdit){
                return;
            }
            $scope.dialogs.timePicker.openDialog(function(ok,time,timeDialog){
              if(!ok) return true;
              job[which] = new Date(time.getTime());
              $scope.updateJobTime(index, which);
              return true;
            }, which=='EndTime'?'End':'Start', job[which]);            
        }
        $scope.editJobMeals = function(job, $event){
            if(!$scope.editable||$scope.loading||!job.TotalHours){
                return;
            }
            $scope.dialogs.meals.open(job.Meals, function(meals){
                job.Meals = meals=='reset'?'':meals?meals:0;
                $scope.changeJobMeals(job);
            }, {title:'Meals - '+job.WorkgroupJobNumber});
            $event.stopPropagation();
        }
        $scope.changeJobMeals = function(job){
            TimeEntryService.changeJobMeals(job, $scope.workweek);//TODO
            $scope.changes(true);
        }


        $scope.updateTaskHours = function(job, index, jobIndex, previousValue){
            TimeEntryService.updateTaskHoursEntry($scope.workweek, job, index, jobIndex, previousValue);
            $scope.changes(true);
        }
        $scope.updateTaskTime = function(job, index, which, jobIndex){
            TimeEntryService.updateTaskTime($scope.workweek, job, index, which, jobIndex);
            $scope.changes(true);
        }
        $scope.editTaskTime = function(job, task, index, jobIndex, noEdit){
            if(noEdit) return;
            $scope.dialogs.timePicker.openDialog(function(ok,time,timeDialog){
              if(!ok) return true;
              task.EndTime = new Date(time.getTime());
              $scope.updateTaskTime(job, index, 'EndTime', jobIndex);
              return true;
            }, 'End', task.EndTime);
        }
        $scope.editTaskHours = function(job, task, index, jobIndex, noEdit, $event){
            if(noEdit) return;
            var previous = task.Duration,
                element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(task.Duration, function(hours){
                task.Duration = hours;
                $scope.updateTaskHours(job, index, jobIndex, previous);
            }, {title:'Hours - '+task.TaskNumber});
            $event.stopPropagation();
        }
        $scope.changePhaseHours = function(pc, task, which, $previous){
            TimeEntryService.changePhaseHours(pc, task, which, $previous);
            $scope.changes(true);
        }
        $scope.editPhaseAllocation = function(pc, task, which, noEdit, $event){
            if(noEdit) return;
            var previous = pc[which+'Hours'],
                element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(pc[which+'Hours'], function(hours){
                pc[which+'Hours'] = hours;
                $scope.changePhaseHours(pc, task, which, previous);
            }, {title:which+' Hours - '+pc.DisplayNumber});
            $event.stopPropagation();
        }
        $scope.editMemberJobTotalHours = function(job, index, noEdit){
            if(noEdit) return;
            $scope.dialogs.hours.open(job.Member.TotalHours, function(hours){
                job.Member.TotalHours = hours;
                $scope.updateMemberJobHours(index);
            }, {title:'Hours - '+job.Job.WorkgroupJobNumber});
        }
        $scope.updateMemberJobHours = function(index){
            TimeEntryService.updateMemberJobHours($scope.workweek, index);
            $scope.changes(true);
        }
        $scope.editMemberTaskTotalHours = function(job, index, jobIndex, noEdit, $event){
            if(noEdit) return;
            var task = job.Tasks[index];
            var previous = job.Tasks[index].TotalHours;
            var taskNum = task.isMember ? task.JobTask.TaskNumber : task.Task.TaskNumber;
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(task.TotalHours, function(hours){
                task.TotalHours = hours;
                $scope.updateMemberTaskHours(job, index, jobIndex, previous);
            }, {title:'Hours - '+taskNum});
        }
        $scope.updateMemberTaskHours = function(job, index, jobIndex, previousValue){
            TimeEntryService.updateMemberTaskHoursEntry($scope.workweek, job, index, jobIndex, previousValue);
            $scope.changes(true);
        }
        $scope.editMemberJobTime = function(job, index, which, noEdit){
            if(noEdit){
                return;
            }
            $scope.dialogs.timePicker.openDialog(function(ok,time,timeDialog){
              if(!ok) return true;
              job[which] = new Date(time.getTime());
              $scope.updateMemberJobTime(index, which);
              return true;
            }, which=='EndTime'?'End':'Start', job[which]);            
        }
        $scope.updateMemberJobTime = function(index, which){
            TimeEntryService.updateMemberJobTime($scope.workweek, index, which);
            $scope.changes(true);
        }
        $scope.editMemberTaskTime = function(job, index, which, jobIndex, noEdit){
            if(noEdit){
                return;
            }
            $scope.dialogs.timePicker.openDialog(function(ok,time,timeDialog){
              if(!ok) return true;
              job.Tasks[index][which] = new Date(time.getTime());
              $scope.updateMemberTaskTime(job, index, which, jobIndex);
              return true;
            }, which=='EndTime'?'End':'Start', job.Tasks[index][which]);            
        }
        $scope.updateMemberTaskTime = function(job, index, which, jobIndex){
            TimeEntryService.updateMemberTaskTime($scope.workweek, job, index, which, jobIndex);
            $scope.changes(true);
        }

        var uploadResult = function(resp, title, obj){
            $scope.loading = false;
            $scope.dialogs.uploads.setOpening(false)
            if(resp && resp.Key){
                var which = obj.location?'Location':'Job', id = obj.location?obj.location:obj.job;
                if(!$scope.workweek.Photos[which][id]){
                    $scope.workweek.Photos[which][id] = []
                }
                $scope.workweek.Photos[which][id].push({String:resp.Key,Description:resp.Description,TypeCode:'PH'});
                var uploads = $scope.workweek.Photos[which][id].map(function(photo){
                    return {Key: photo.String, Description: photo.Description, TypeCode: photo.TypeCode}
                });
                if($scope.dialogs.uploads.visible){
                    $scope.dialogs.uploads.setUploads(uploads);
                }else{
                    $scope.dialogs.uploads.open(title, obj, uploads);
                }
                if(which == 'Location'){
                    TimeEntryService.updateTaskPhotos(id, $scope.workweek)
                }else{
                    TimeEntryService.updateJobPhotos(id, $scope.workweek)
                }
            }
        }
        var showPhotoUpload = function(callback, title, obj){
            obj.route = 'photo'
            UploadDialog.show({save: function(data){
                if(data.description) obj.description = data.description;
                ApiService.getFormData(obj, {photo:data.photoUrl}).then(function(formdata){
                    $scope.loading = true;
                    if($scope.dialogs.uploads.visible){
                        $scope.dialogs.uploads.setOpening(true)
                    }
                    var done = function(resp){callback(resp,title,obj)};
                    ApiService.post('fte', obj, '', '', formdata)
                    .then(done, done);
                });
                UploadDialog.hide();
            }});
        }
        $scope.jobPhotos = function(job){
            var title = 'Job Photos - '+job.WorkgroupJobNumber
            var obj = {workweek: $scope.workweek.ID, job:job.JobCrewJobID}
            if(!$scope.workweek.Photos.Job[job.JobCrewJobID]){
                showPhotoUpload(uploadResult, title, obj);
            }else{
                $scope.dialogs.uploads.open(title, obj, $scope.workweek.Photos.Job[job.JobCrewJobID].map(function(photo){
                    return {Key: photo.String, Description: photo.Description}
                }));
            }
        }
        $scope.locationPhotos = function(job, task){
            var title = 'Location Photos - '+task.LocationNumber
            var obj = {workweek: $scope.workweek.ID, job: job.JobCrewJobID, location: task.LocationID}
            if(!$scope.workweek.Photos.Location[task.LocationID]){
                showPhotoUpload(uploadResult, title, obj);
            }else{
                $scope.dialogs.uploads.open(title, obj, $scope.workweek.Photos.Location[task.LocationID].map(function(photo){
                    return {Key: photo.String, Description: photo.Description}
                }));
            }
        }

        $scope.callbacks.loadJobTasksResult = function(data){
            TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, '', data);
        }
        $scope.callbacks.loadJobTasksResultNoInit = function(data){
            TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, '', data, 'loadJobTasksResultNoInit');
            if(data.currentJob){
                var job = TimeEntryService.activeJobs($scope.workweek).find(function(jb){ return jb.JobCrewJobID == data.currentJob });
                if(job){
                    if(data.currentTask){
                        var task = job.Task.find(function(t){ return t.ID == data.currentTask; });
                        if(task){
                            $scope.editTask(job, task);
                        }
                    }else{
                        $scope.addTask(job);
                    }
                }
            }
        }
        $scope.expandJob = function(job){
            job.showTasks = !job.showTasks;
            TimeEntryService.calcShowTaskRows($scope.workweek);
        }
        $scope.expandTask = function(task){
            task.showPhases = !task.showPhases;
            TimeEntryService.calcShowTaskRows($scope.workweek);
        }
        $scope.expandAllJobs = function(){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            for(var j=0; j<day.JobDayCrew.length; ++j){
                var job = day.JobDayCrew[j];
                if(job.active){
                    job.showTasks = !day.isShowingTasks;
                }
            }
            TimeEntryService.calcShowTaskRows($scope.workweek);
        }
        $scope.expandAllTasks = function(){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            for(var j=0; j<day.JobDayCrew.length; ++j){
                var job = day.JobDayCrew[j];
                if(job.active){
                    for(var t=0; t<job.Task.length; ++t){
                        var task = job.Task[t];
                        if(task.active){
                            task.showPhases = !day.isShowingPhases;
                        }
                    }
                    if(day.members && day.members.length){
                        var member = day.members[day.currentMember];
                        var memberJob = member.Jobs.find(function(jb){return jb.ID == job.JobCrewJobID });
                        if(memberJob && memberJob.Member.MemberTask){
                            memberJob.Member.MemberTask = Utils.enforceArray(memberJob.Member.MemberTask);
                            for(var t=0; t<memberJob.Member.MemberTask.length; ++t){
                                var task = memberJob.Member.MemberTask[t];
                                if(task.active){
                                    task.showPhases = !day.isShowingPhases;
                                }
                            }
                        }
                    }
                }
            }
            TimeEntryService.calcShowTaskRows($scope.workweek);
        }

        $scope.expandSummaryPerson = function(longpress, people){
            var expand = false, day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            if(!Array.isArray(people)){
                if(people){
                    expand = !people.showJobs;
                    people = [people];
                }else{
                    expand = !day.isShowingSummaryJobsPerson;
                    people = day.memberSummary;
                }
            }
            for(var p=0; p<people.length; ++p){
                var person = people[p];
                for(var c=0; c<person.Classes.length; ++c){
                    person.Classes[c].showJobs = expand;
                    if(longpress){
                        var clss = person.Classes[c];
                        for(var j=0; j<clss.Member.Jobs.length; ++j){
                            if((clss.Member.Jobs[j].Member.ActiveFlag || ($scope.workweek.memberJobWeekTotals[members[m].PersonID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].active)) && clss.isToday){
                                $scope.workweek.memberJobWeekTotals[person.PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].showTasks = expand;
                                expandSummaryMemberJobTasks(clss.Member.Jobs[j]);
                            }
                        }
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Person']);
        }
        $scope.expandSummaryClass = function(clss, longpress){
            clss.showJobs = !clss.showJobs;
            if(longpress){
                for(var j=0; j<clss.Member.Jobs.length; ++j){
                    if((clss.Member.Jobs[j].Member.ActiveFlag || $scope.workweek.memberJobWeekTotals[person.PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].active) && clss.isToday){
                        $scope.workweek.memberJobWeekTotals[clss.Member.PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].showTasks = clss.showJobs;
                        expandSummaryMemberJobTasks(clss.Member.Jobs[j]);
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Person']);
        }
        $scope.expandSummaryMemberJobsAll = function(longpress){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            var members = day.memberSummary;
            var expand = !day.isShowingSummaryTasksPerson;
            for(var m=0; m<members.length; ++m){
                for(var c=0; c<members[m].Classes.length; ++c){
                    var clss=members[m].Classes[c];
                    for(var j=0; j<clss.Member.Jobs.length; ++j){
                        if((clss.Member.Jobs[j].Member.ActiveFlag || ($scope.workweek.memberJobWeekTotals[members[m].PersonID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].active)) && clss.isToday){
                            $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].showTasks = expand;
                            if(longpress){
                                expandSummaryMemberJobTasks(clss.Member.Jobs[j]);
                            }
                        }
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Person']);
        }
        $scope.expandSummaryMemberTasksAll = function(){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            var members = day.memberSummary;
            var expand = !day.isShowingSummaryPhasesPerson;
            for(var m=0; m<members.length; ++m){
                for(var c=0; c<members[m].Classes.length; ++c){
                    var clss=members[m].Classes[c];
                    for(var j=0; j<clss.Member.Jobs.length; ++j){
                        if((clss.Member.Jobs[j].Member.ActiveFlag || ($scope.workweek.memberJobWeekTotals[members[m].PersonID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID] && $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].active)) && clss.isToday){
                            $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].Tasks = $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].Tasks||{}
                            for(var t=0; t<clss.Member.Jobs[j].Tasks.length; ++t){
                                var tsk = clss.Member.Jobs[j].Tasks[t];
                                if(tsk.Task.active && tsk.TotalHours > 0){
                                    $scope.workweek.memberJobWeekTotals[members[m].PersonID][clss.Member.ClassID][clss.Member.Jobs[j].Job.JobCrewJobID].Tasks[tsk.Task.ID] = expand;
                                }
                            }
                        }
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Person']);
        }
        var expandSummaryMemberJobTasks = function(jb){
            var job = $scope.workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID];
            job.Tasks = job.Tasks||{}
            for(var t=0; t<jb.Tasks.length; ++t){
                var tsk = jb.Tasks[t];
                if(tsk.Task.active && tsk.TotalHours > 0){
                   job.Tasks[tsk.Task.ID] = job.showTasks;
                }
            }
        }
        $scope.expandSummaryMemberJob = function(jb, longpress){
            var job = $scope.workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID];
            job.showTasks = !job.showTasks;
            if(longpress){
                expandSummaryMemberJobTasks(jb);
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Person']);
        }
        $scope.expandSummaryMemberTask = function(tsk, jb){
            var job = $scope.workweek.memberJobWeekTotals[jb.Member.PersonID][jb.Member.ClassID][jb.Job.JobCrewJobID];
            job.Tasks = job.Tasks||{};
            job.Tasks[tsk.Task.ID] = !job.Tasks[tsk.Task.ID];
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Person']);
        }


        $scope.expandSummaryJobTasks = function(job, longpress){
            var jobs, expand;
            if(job){
                jobs = [job];
                expand = !job.showSummaryTasks;
            }else{
                jobs = TimeEntryService.activeJobs($scope.workweek);
                expand = !$scope.workweek.WorkDay[$scope.workweek.currentDay].isShowingSummaryJobTasks;
            }
            for(var j=0; j<jobs.length; ++j){
                jobs[j].showSummaryTasks = expand;
                if(longpress){
                    for(var t=0; t<jobs[j].Task.length; ++t){
                        if(jobs[j].Task[t].active){
                            jobs[j].Task[t].showSummaryPhases = expand;
                        }
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Crew']);
        }
        $scope.expandSummaryTaskPhases = function(task){
            if(task){
                task.showSummaryPhases = !task.showSummaryPhases;
            }else{
                var jobs = TimeEntryService.activeJobs($scope.workweek);
                var expand = !$scope.workweek.WorkDay[$scope.workweek.currentDay].isShowingSummaryPhases;
                for(var j=0; j<jobs.length; ++j){
                    for(var t=0; t<jobs[j].Task.length; ++t){
                        if(jobs[j].Task[t].active){
                            jobs[j].Task[t].showSummaryPhases = expand;
                        }
                    }
                }
            }
            
            TimeEntryService.calcShowSummaryRows($scope.workweek, ['Crew']);
        }

        $scope.filterSummaryPersonJobs = function(){
            return function(item){
                return item.Member.ActiveFlag || $scope.workweek.memberJobWeekTotals[item.Member.PersonID][item.Member.ClassID][item.Job.JobCrewJobID].active;
            }
        }
        $scope.filterSummaryPersonTasks = function(){
            return function(item){
                return item.Task.active&&item.TotalHours>0;
            }
        }
        $scope.filterSummaryPersonPhases = function(){
            return function(item){
                return item.PhaseCode.active&&item.TotalHours>0;
            }
        }

        $scope.expandSummaryEquip = function(type, longpress, equipment){
            var expand = false, day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            if(!Array.isArray(equipment)){
                if(equipment){
                    expand = !equipment.showJobs;
                    equipment = [equipment];
                }else{
                    expand = !day["isShowingSummaryJobsEquip"+type];
                    equipment = day.equipSummary;
                }
            }
            for(var e=0; e<equipment.length; ++e){
                var equip = equipment[e];
                if(equip.Equip.TypeCode == type){
                    equip.showJobs = expand;
                    if(longpress){
                        for(var j=0; j<equip.Equip.Jobs.length; ++j){
                            if((equip.Equip.Jobs[j].Equipment.ActiveFlag || ($scope.workweek.equipJobWeekTotals[equip.EquipmentID] && $scope.workweek.equipJobWeekTotals[equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID] && $scope.workweek.equipJobWeekTotals[equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].active)) && equip.isToday){
                                $scope.workweek.equipJobWeekTotals[equip.Equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].showTasks = expand;
                                expandSummaryEquipJobTasks(equip.Equip.Jobs[j]);
                            }
                        }
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, [type]);
        }

        $scope.expandSummaryEquipJob = function(jb, type, longpress){
            var job = $scope.workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID]
            job.showTasks = !job.showTasks;
            if(longpress){
                expandSummaryEquipJobTasks(jb);
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, [type]);
        }

        $scope.expandSummaryEquipJobsAll = function(type, longpress){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            var equipment = day.equipSummary;
            var expand = !day['isShowingSummaryTasksEquip'+type];
            for(var e=0; e<equipment.length; ++e){
                var equip = equipment[e];
                if(equip.Equip.TypeCode == type){
                    for(var j=0; j<equip.Equip.Jobs.length; ++j){

                        if((equip.Equip.Jobs[j].Equipment.ActiveFlag || ($scope.workweek.equipJobWeekTotals[equip.EquipmentID] && $scope.workweek.equipJobWeekTotals[equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID] && $scope.workweek.equipJobWeekTotals[equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].active)) && equip.isToday){
                            $scope.workweek.equipJobWeekTotals[equip.Equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].showTasks = expand;
                            if(longpress){
                                expandSummaryEquipJobTasks(equip.Equip.Jobs[j]);
                            }
                        }
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, [type]);
        }

        $scope.expandSummaryEquipTask = function(tsk, jb, type){
            var job = $scope.workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID];
            job.Tasks = job.Tasks||{};
            job.Tasks[tsk.Task.ID] = !job.Tasks[tsk.Task.ID];
            TimeEntryService.calcShowSummaryRows($scope.workweek, [type]);
        }

        $scope.expandSummaryEquipTasksAll = function(type){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            var equipment = day.equipSummary;
            var expand = !day['isShowingSummaryPhasesEquip'+type];
            for(var e=0; e<equipment.length; ++e){
                var equip = equipment[e];
                if(equip.Equip.TypeCode == type){
                    for(var j=0; j<equip.Equip.Jobs.length; ++j){
                        if((equip.Equip.Jobs[j].Equipment.ActiveFlag || ($scope.workweek.equipJobWeekTotals[equip.EquipmentID] && $scope.workweek.equipJobWeekTotals[equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID] && $scope.workweek.equipJobWeekTotals[equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].active)) && equip.isToday){
                            $scope.workweek.equipJobWeekTotals[equip.Equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].Tasks = $scope.workweek.equipJobWeekTotals[equip.Equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].Tasks||{};
                            for(var t=0; t<equip.Equip.Jobs[j].Tasks.length; ++t){
                                var tsk = equip.Equip.Jobs[j].Tasks[t];
                                if(tsk.Task.active && tsk.TotalHours > 0){
                                    $scope.workweek.equipJobWeekTotals[equip.Equip.EquipmentID][equip.Equip.Jobs[j].Job.JobCrewJobID].Tasks[tsk.Task.ID] = expand;
                                }
                            }
                        }
                    }
                }
            }
            TimeEntryService.calcShowSummaryRows($scope.workweek, [type]);
        }

        var expandSummaryEquipJobTasks = function(jb){
            var job = $scope.workweek.equipJobWeekTotals[jb.Equipment.EquipmentID][jb.Job.JobCrewJobID]
            job.Tasks = job.Tasks||{}
            for(var t=0; t<jb.Tasks.length; ++t){
                var tsk = jb.Tasks[t];
                if(tsk.Task.active && tsk.TotalHours > 0){
                   job.Tasks[tsk.Task.ID] = job.showTasks;
                }
            }
        }

        $scope.filterSummaryEquipJobs = function(){
            return function(item){
                return item.Equipment.ActiveFlag || $scope.workweek.equipJobWeekTotals[item.Equipment.EquipmentID][item.Job.JobCrewJobID].active;
            }
        }

        
        $scope.callbacks.completeWorkSaveDone = function(resp, data){
            var jobs = TimeEntryService.activeJobs($scope.workweek);
            for(var j=0; j<data.jobs.length; ++j){
                var job = jobs.find(function(jb){ return jb.JobCrewJobID == data.jobs[j].JobID });
                if(job){
                    data.jobs[j].JobDayCrewID = job.ID
                    data.jobs[j].OwnerID = job.OwnerID
                    var jobRef = TimeEntryService.currentDay($scope.workweek).Job.find(function(jb){ return jb.ID == data.jobs[j].JobID })
                    if(jobRef && jobRef.DispatchJobID){
                        data.jobs[j].DispatchJobID = jobRef.DispatchJobID
                        data.jobs[j].Map = jobRef.Map
                    }
                }
            }
            $scope.loading = true;
            $scope.working = true;
            var timesheet = {title:$scope.workweek.CrewName,StartDate:share.timesheet.StartDate,EndDate:share.timesheet.EndDate,WorkWeekID:$scope.workweek.ID}
            TECompletionService.loadJob({jobs:data.jobs, date:data.date, timesheet:timesheet, fromGeneralTime:$scope.fromGeneralTime}, $scope, function(){
              $scope.loading = false;
              $scope.working = false;
            });
        }
        
        $scope.callbacks.completeWorkJobScreenLoaded = function(data){
            TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, '', data, 'completeWorkJobScreenLoaded');
            if($scope.workweek.launchLocationPercent){
                TimeEntryService.openLocationPercents($scope, null, true);
            }
        }
        $scope.completeWorkJobScreen = function(job){
            TimeEntryService.currentDay($scope.workweek).cwJobScreen = true;
            if(!$scope.completeWork(job, true) && $scope.workweek.launchLocationPercent){
                var jobs = TimeEntryService.activeJobs($scope.workweek);
                if($scope.editable){
                    if(jobs.find(function(jb){ return !jb.JobTask || (!TimeEntryService.isOverheadJob(jb.OwnerID,$scope.workweek) && (!jb.ContractWorkingTime && !jb.CWWorkingTime)) || ($scope.workweek.launchLocationPercent&&(!$scope.workweek.completeJobIds||!$scope.workweek.completeJobIds.length||!!$scope.workweek.completeJobIds.find(function(cjb){return cjb.JobID==jb.JobCrewJobID}))&&!$scope.workweek.jobLocations[jb.JobCrewJobID]) })){
                    TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, jobs.reduce(function(arr, jb){ arr.push(jb.JobCrewJobID); return arr; }, []), null, 'completeWorkJobScreenLoaded');
                    }else{
                        TimeEntryService.openLocationPercents($scope, null, true);
                    }
                }
            }
        }
        $scope.completeWork = function(job, forceCW){
            var ids = job?[job.JobCrewJobID]:null;
            return TimeEntryService.completeWork($scope, ids, forceCW);
        }

        $scope.callbacks.showUpSitesSaveDone = function(resp, data){
            var day = TimeEntryService.currentDay($scope.workweek);
            var jobIds = day.showUpSites.reduce(function(jobs,site){ jobs.push(site.ID); return jobs; }, [])
            var timesheet = {title:$scope.workweek.CrewName,StartDate:share.timesheet.StartDate,EndDate:share.timesheet.EndDate,WorkWeekID:$scope.workweek.ID}
            TECompletionService.loadShowUpSites({jobs:jobIds, timesheet:timesheet, date:Utils.date(day.WeekdayDate,'MM/dd/yyyy'), fromGeneralTime:$scope.fromGeneralTime}, $scope);
        }
        $scope.openShowupSites = function(){
            TimeEntryService.showUpSites($scope);
        }

        $scope.callbacks.locationPercentDone = function(resp, data){
            Utils.notification.hideToast();
            var opened = TimeEntryService.activeJobs($scope.workweek).reduce(function(jbs, jb){
                jbs[jb.JobCrewJobID] = jb.showTasks
                return jbs;
            }, {})
            share.workWeekResult(resp, {completeJobIds: $scope.workweek.completeJobIds, jobScreen:$scope.workweek.jobScreen}, $scope)
            var jobs = TimeEntryService.activeJobs($scope.workweek)
            jobs.forEach(function(jb){
                if(opened[jb.JobCrewJobID]){
                    jb.showTasks = true;
                }
            });
            if(resp.errors){
                jobs = jobs.filter(function(jb){ return jb.LocationPercentCompleteFlag });
                Utils.notification.alert(resp.errors.map(function(er){
                    return '<div>'+er.message+'<ul>'+er.locations.map(function(loc){
                        var location;
                        for(var j=0; j<jobs.length; ++j){
                            if($scope.workweek.jobLocations[jobs[j].JobCrewJobID] && (location = $scope.workweek.jobLocations[jobs[j].JobCrewJobID].find(function(l){ return l.ID==loc.id }))){
                                break;
                            }
                        }
                        if(location){
                            return '<li>'+Utils.adjustLocationNumber(location.NumberChar)+' ('+loc.percent+'%)</li>';
                        }
                        return '';
                    }).join('')+'</ul></div>';
                }).join(''), {title:"Warning"})
            }
            TimeEntryService.calcShowTaskRows($scope.workweek);
        }

        $scope.addTask = function(job){
            var jobs = TimeEntryService.activeJobs($scope.workweek);
            if(jobs.find(function(jb){ return !jb.JobTask || (jb.OwnerID!=$scope.workweek.PayrollPeriodOrganizationID && (!jb.ContractWorkingTime && !jb.CWWorkingTime)) })){
                var jobIds = jobs.reduce(function(arr, jb){ arr.push(jb.JobCrewJobID); return arr; }, []);
                TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, {jobs:jobIds, currentJob: job.JobCrewJobID}, null, 'loadJobTasksResultNoInit');
                return;
            }
            $scope.dialogs.pickTask.open($scope.selectJobTask, {
                job: job,
                taskTypes: TimeEntryService.getTaskGroups($scope.workweek, job),
                taskMode: ''
            });
        }
        $scope.isLocationTask = function(job, task){
            return TimeEntryService.isLocationTask(job, task);
        }
        $scope.selectJobTask = function(jobTask, isNew){
            var newTask = TimeEntryService.addTaskToJob($scope.dialogs.pickTask._scope.job, jobTask, isNew, $scope.workweek, $scope.dialogs.pickTask._scope.taskMode);
            if(newTask){
                $scope.dialogs.pickTask._scope.job.showTasks = true;
                TimeEntryService.calcShowTaskRows($scope.workweek);
                $scope.changes(true);
                if($scope.isLocationTask($scope.dialogs.pickTask._scope.job, newTask)){
                    newTask.canEditLocation = true;
                    $scope.editTask($scope.dialogs.pickTask._scope.job, newTask, true);
                    $scope.dialogs.editTask._scope.data.error = 'Choose a location';
                }
                return true;
            }
        }
        $scope.removeTask = function(job, task, confirm){
            if(confirm){
                TimeEntryService.removeTask($scope.workweek, job, task);
                $scope.changes(true);
            }else{
                share.dialogs.alert.open({
                    title: "Remove Task",
                    content: "Are you sure you want to remove this task?",
                    buttons: [{
                        label: 'Remove Task',
                        callback: function(){ $scope.removeTask(job, task, true); }
                    },{
                        label: 'Cancel'
                    }],
                    cancelable: true
                });
            }
        }
        $scope.editTask = function(job, task, autoTriggered){
            if($scope.editable && !$scope.workweek.wgPhaseCodes){
                var jobIds = TimeEntryService.activeJobs($scope.workweek).reduce(function(arr, jb){ arr.push(jb.JobCrewJobID); return arr; }, []);
                TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, {jobs:jobIds, currentJob: job.JobCrewJobID, currentTask: task.ID}, null, 'loadJobTasksResultNoInit');
                return;
            }
            var data = {};
            data.autoTriggered = autoTriggered;
            data.task = task;
            data.job = job;
            data.location = $scope.isLocationTask(job, task)?$scope.workweek.jobLocations[job.JobCrewJobID].find(function(loc){return loc.ID==task.LocationID}):null;
            data.editingTask = true;
            data.comment = task.TaskComment||'';
            if($scope.editable){
                data.phaseCodes = (function(){
                    if(task.JobTaskSystemCode == 'UN' || job.OwnerID == $scope.workweek.PayrollPeriodOrganizationID){
                        return job.JobTask.find(function(jt){ return jt.ID == task.JobTaskID }).JobTaskPhaseCode.slice(0);
                    }else if(task.JobTaskPhaseCodeSectionID > 0){
                        return $scope.workweek.wgPhaseCodes;
                    }
                    return job.contractPhaseCodes;
                })();
                var jobTask = job.JobTask.find(function(jt){ return jt.ID == task.JobTaskID });
                data.task.LockPhases = jobTask.LockPhases;
                for(var pc=0; pc<data.phaseCodes.length; ++pc){
                    var phasecode = data.phaseCodes[pc];
                    phasecode.selected = false;
                    phasecode.hidden = false;
                    var taskPhase = task.PhaseCode.find(function(phase){ return phase.ID == (phasecode.PhaseCodeID||phasecode.ID) })
                    if(taskPhase){
                        phasecode.selected = true;
                    }else if(jobTask){
                        var jtphase = jobTask.JobTaskPhaseCode.find(function(jtp){ return jtp.PhaseCodeID == (phasecode.PhaseCodeID||phasecode.ID) });
                        if(jtphase && jtphase.Disabled){
                            phasecode.hidden = true;
                        }
                    }
                }
            }
            $scope.dialogs.editTask.open(function(ok){
                if(ok){
                    return $scope.taskEditDone();
                }
                return $scope.taskEditCancel();
            }, data);
        }
        $scope.taskEditDone = function(){
            var data = $scope.dialogs.editTask._scope.data;
            if($scope.isLocationTask(data.job,data.task)){
                if(!data.location){
                    data.error = "You must select a location"
                    return false;
                }
                data.task.LocationID = data.location.ID;
                data.task.LocationNumber = data.location.NumberChar;
                TimeEntryService.updateTaskPhotos(data.task, $scope.workweek);
            }
            data.task.TaskComment = (data.comment||'').trim();
            return $scope.newJobTaskPhaseCodeReturn($scope.dialogs.editTask);
        }
        $scope.taskEditCancel = function(){
            var data = $scope.dialogs.editTask._scope.data;
            if($scope.isLocationTask(data.job,data.task) && data.autoTriggered){
                if(!data.task.LocationID){
                    $scope.removeTask(data.job, data.task, true);
                }
            }
            return true;
        }
        $scope.newJobTaskPhaseCodeReturn = function(dialog){
            if(!dialog._scope.data.phaseCodes.find(function(pc){ return pc.selected })){
                dialog._scope.data.error = "Select a phase code";
                return false;
            }
            if(dialog._scope.data.editingTask){
                if(dialog._scope.data.jobTask){
                    dialog._scope.data.task = TimeEntryService.addTaskToJob(dialog._scope.data.job, dialog._scope.data.jobTask, false, $scope.workweek, dialog._scope.data.taskMode);
                    dialog._scope.data.job.showTasks = true;
                    dialog.hide();
                    delete dialog._scope.data.jobTask;
                }
                if(TimeEntryService.taskPhaseCodeReturn($scope, dialog)){
                    dialog._scope.data.task.showPhases = true;
                }
                if(dialog._scope.data.taskMode == 'CrewMember' && dialog._scope.data.task.Task){
                    dialog._scope.data.task = dialog._scope.data.task.Task;
                }
                $scope.changes(true);
            }else{
                TimeEntryService.newJobTaskPhaseCodeReturn($scope, null, dialog);
            }
            TimeEntryService.calcShowTaskRows($scope.workweek);
            return true;
        }
        $scope.openComments = function(job, task){
            if(task){
                $scope.editTask(job, task);
                Utils.focusInput('taskCommentArea');
            }else{
                $scope.editJob(job);
                Utils.focusInput('jobCommentArea');
            }
        }

        $scope.clearTableHighlight = function(){
            var cell = document.querySelector('td.timesheet-active-cell');
            if(cell){
                updateTableHighlight(angular.element(cell), "blur");
            }
        }

        $scope.callbacks.initMemberTimeResult = function(data){
            if((!data.status || data.status == 200)){
                TimeEntryService.initMemberTime($scope.workweek, $scope, data);
            }else{
                $scope.progressStep(-1);
                Utils.notification.alert('Failed to initialize crew member time. Please try again.', {title: 'Error'});
            }
        }

        $scope.navMember = function($index){
            $scope.workweek.WorkDay[$scope.workweek.currentDay].currentMember = $index;
            TimeEntryService.totalMemberTime($scope.workweek, 'current');
            TimeEntryService.calcShowTaskRows($scope.workweek);
        }
        $scope.navMemberCarousel = function(dir){
            if(dir > 0){
                membersCarousel.next();
            }else{
                membersCarousel.prev();
            }
        }
        $scope.openMemberSelect = function(){
            $scope.dialogs.radioDialog.open(function(ok, index){
                if(ok){
                    membersCarousel.setActiveIndex(index);
                }
                return true;
            }, {
                title: 'Crew Member',
                currentIndex: $scope.workweek.WorkDay[$scope.workweek.currentDay].currentMember,
                items: $scope.workweek.WorkDay[$scope.workweek.currentDay].members.map(function(mem){ 
                    var item = {
                        Name: mem.Name, 
                        ClassShort: mem.ClassShort,
                        ClassID: mem.Jobs[0].Member.ClassID,
                        PersonClassID: mem.Jobs[0].Member.PersonClassID,
                        cssClass:mem.cssClass,
                        addToCrew: mem.Jobs[0].Member.permanentMessage=='Permanently Add To Crew'
                    }; 
                    if($scope.workweek.PersonID==mem.PersonID){item.isMe=true}; 
                    return item;
                }),
                template: 'app/timesheet/member-select-row.html',
                autoClose: true
            });
        }
        $scope.editMember = function(member, jobIndex){
            $scope.dialogs.memberInfo.open({
                member: member,
                memberJob: member.Jobs[jobIndex],
                jobIndex: jobIndex
            });
        }
        var fixMemberCarousel = function(){
            var day = TimeEntryService.currentDay($scope.workweek);
            var member = day.members[day.currentMember];
            var index = day.members.findIndex(function(mem){ return mem === member});
            if(index != membersCarousel.getActiveIndex()){
                membersCarousel.setActiveIndex(index);
            }
        }
        $scope.closeMemberInfo = function(){
            if(TimeEntryService.shouldHideMember($scope.dialogs.memberInfo._scope.data.member.Jobs[0].Member, $scope.workweek)){
                TimeEntryService.changeCurrentDayMember(null, $scope.workweek);
            }
            fixMemberCarousel();
            return true;
        }
        $scope.navCrewMemberInfoJob = function(dir){
            $scope.workweek.dialog1.jobIndex += dir;
        }
        $scope.editMemberJobHours = function(job, which, noEdit, $event){
            if(noEdit) return;
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(job.Member[which+'Hours'], function(hours){
                job.Member[which+'Hours'] = hours;
                $scope.changeMemberJobHours(job, which);
            }, {title:which+' Hours - '+job.Job.WorkgroupJobNumber});
        }
        $scope.changeMemberJobHours = function(job, which){
            TimeEntryService.changeMemberJobHours(job, which, $scope.workweek);
            $scope.changes(true);
        }
        $scope.editMemberTaskHours = function(member, task, which, noEdit){
            if(noEdit) return;
            $scope.dialogs.hours.open(task[which+'Hours'], function(hours){
                task[which+'Hours'] = hours;
                $scope.changeMemberTaskHours(member, task, which);
            }, {title:which+' Hours - '+(task.isMember ? task.JobTask.TaskNumber : task.Task.TaskNumber)});
        }
        $scope.changeMemberTaskHours = function(member, task, which){
            TimeEntryService.changeMemberTaskHours(member, task, which, $scope.workweek);
            $scope.changes(true);
        }
        $scope.editMemberPhaseHours = function(member, pc, task, which, noEdit, $event){
            if(noEdit) return;
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(pc[which+'Hours'], function(hours){
                pc[which+'Hours'] = hours;
                $scope.changeMemberPhaseHours(member, pc, task, which);
            }, {title:which+' Hours - '+(task.isMember ? pc.PhaseCode.PhaseCodeDisplayNumber : pc.PhaseCode.DisplayNumber)});
        }
        $scope.changeMemberPhaseHours = function(member, pc, task, which){
            TimeEntryService.changeMemberPhaseHours(member, pc, task, which, $scope.workweek);
            $scope.changes(true);
        }
        $scope.changeMemberClass = function(memberJob, dayMember){
            var day = TimeEntryService.currentDay($scope.workweek);
            if(day.members){
                var existing = day.members.find(function(dm){ return dm.Jobs[0].Member.ID != memberJob.Member.ID && dm.PersonID==dayMember.PersonID && (memberJob.Member.class&&dm.ClassID == memberJob.Member.class.ID) });
                if(existing){
                    $scope.dialogs.memberInfo._scope.data.error = 'This person already exists on the crew with class '+memberJob.Member.class.Class;
                    $scope.mapSP(memberJob.Member, 'class');
                    return;
                }
            }
            memberJob.Member.oldClassID = memberJob.Member.ClassID;
            $scope.dialogs.memberInfo._scope.data.error = '';
            $scope.mapBack(memberJob.Member,'class');
            dayMember.ClassID = memberJob.Member.ClassID;
            dayMember.Class = memberJob.Member.ClassClass;
            dayMember.ClassShort = memberJob.Member.ClassShortDescription;
            dayMember.ClassSort = memberJob.Member.ClassSortOrder;
            TimeEntryService.syncRosterItem(memberJob.Member, 'JobDayCrewMember', $scope.workweek);
            TimeEntryService.calcHasPermanentMemberChanges(memberJob.Member, $scope.workweek);
            $scope.changeMemberPerDiem(dayMember, true);
            $scope.changes(true);
        }
        $scope.resetMemberTime = function(dayMember){
            if(dayMember){
                if(dayMember.Jobs.length && !dayMember.Jobs[0].Member.ActiveFlag){
                    dayMember.Jobs[0].Member.jobStatus = $scope.workweek.sp.jobStatus.find(function(js){ return js.Code == 'AC' });
                    $scope.changeCrewActive('Member', true, dayMember.Jobs[0].Member, dayMember.Jobs[0], dayMember)
                }else{
                    TimeEntryService.parseTimeToOneMember(dayMember, $scope.workweek);
                    $scope.changes(true);
                }
            }else{
                var members = TimeEntryService.currentDay($scope.workweek).members;
                for(var m=0; m<members.length; ++m){
                    TimeEntryService.parseTimeToOneMember(members[m], $scope.workweek);
                }
                $scope.changes(true);
            }
            fixMemberCarousel()
        }
        $scope.resetMemberMenu = function(dayMember){
            $scope.dialogs.radioDialog.open(function(ok, index){
                if(ok){
                    if(index){
                        $scope.resetMemberTime();
                    }else{
                        $scope.resetMemberTime(dayMember);
                    }
                }
                return true;
            }, {
                title: 'Reset to Crew Time',
                noRadio: true,
                items: [{label:'Reset Current Member'}, {label:'Reset All Members'}],
                autoClose: true
            });
        }
        $scope.clearMemberTime = function(member){
            if(member){
                TimeEntryService.clearMemberTime(member, $scope.workweek);
                $scope.changes(true);
            }else{
                var members = TimeEntryService.currentDay($scope.workweek).members;
                for(var m=0; m<members.length; ++m){
                    TimeEntryService.clearMemberTime(members[m], $scope.workweek);
                }
                $scope.changes(true);
            }
        }
        $scope.zeroMemberMenu = function(dayMember){
            $scope.dialogs.radioDialog.open(function(ok, index){
                if(ok){
                    if(index){
                        $scope.clearMemberTime();
                    }else{
                        $scope.clearMemberTime(dayMember);
                    }
                }
                return true;
            }, {
                title: 'Zero Out Time',
                noRadio: true,
                items: [{label:'Zero Current Member'}, {label:'Zero All Members'}],
                autoClose: true
            });
        }
        $scope.addMemberTask = function(job){
            $scope.dialogs.pickTask.open($scope.selectJobTask, {
                job: job,
                taskTypes: TimeEntryService.getTaskGroups($scope.workweek, job, 'CrewMember'),
                taskMode: 'CrewMember'
            });
        }
        $scope.callbacks.memberTaskLoadJobInfo = {when:'afterInit', fn:function(data){
            TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, '', data, 'memberTaskLoadJobInfo');
            if(data.currentJob){
                var job = TimeEntryService.currentMember($scope.workweek).Jobs.find(function(jb){ return jb.ID == data.currentJob; });
                if(job && data.currentTask){
                    var task = job.Tasks.find(function(t){ return t.Task.ID == data.currentTask; })
                    if(task){
                        $scope.editMemberTask(job, task);
                    }
                }
            }
        }}
        $scope.editMemberTask = function(job, task){
            if(!$scope.workweek.wgPhaseCodes){
                var jobIds = TimeEntryService.activeJobs($scope.workweek).reduce(function(arr, jb){ arr.push(jb.JobCrewJobID); return arr; }, []);
                TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, {jobs:jobIds, currentJob:job.ID, currentTask: task.Task.ID}, null, 'memberTaskLoadJobInfo');
                return;
            }
            for(var pc=0; pc<$scope.workweek.wgPhaseCodes.length; ++pc){
                var phasecode = $scope.workweek.wgPhaseCodes[pc];
                if(task.Task.MemberTaskPhaseCode.find(function(phase){ return phase.active && phase.PhaseCodeID == phasecode.ID; })){
                    phasecode.selected = true;
                }else{
                    phasecode.selected = false;
                }
            }
            $scope.dialogs.pickPhaseCode.open({
                job: job,
                task: task,
                phaseCodes: $scope.workweek.wgPhaseCodes,
                taskMode: 'CrewMember',
                editingTask: true
            });
        }
        $scope.removeMemberTask = function(job, task, confirm){
            if(confirm){
                TimeEntryService.removeMemberTask($scope.workweek, job, task);
                $scope.changes(true);
            }else{
                share.dialogs.alert.open({
                    title: "Remove Task",
                    content: "Are you sure you want to remove this task?",
                    buttons: [{
                        label: 'Cancel'
                    },{
                        label: 'Remove Task',
                        callback: function(){ $scope.removeMemberTask(job, task, true); }
                    }],
                    cancelable: true
                });
            }
        }
        $scope.editMemberPerDiem = function(member, noEdit){
            if(noEdit) return;
            $scope.dialogs.dollars.open(member.PerDiem, function(dollars){
                member.PerDiem = dollars;
                $scope.changeMemberPerDiemAmount(member);
            }, {title:'Per Diem'});
        }
        $scope.changeMemberPerDiem = function(dayMember, autoTriggered){
            TimeEntryService.checkPerDiem(dayMember, $scope.workweek, autoTriggered);
            $scope.changes(true);
        }
        $scope.changeMemberPerDiemAmount = function(dayMember){
            TimeEntryService.updatePerDiemAmount(dayMember, $scope.workweek);
            $scope.changes(true);
        }
        $scope.editMemberMeals = function(member, noEdit){
            if(noEdit) return;
            $scope.dialogs.meals.open(member.Meals, function(meals){
                member.Meals = meals=='reset'?'':meals?meals:0;
                $scope.changeMemberMeals(member);
            }, {title:'Meals'});
        }
        $scope.changeMemberMeals = function(member){
            TimeEntryService.changeMemberMeals(member, $scope.workweek);
            $scope.changes(true);
        }

        $scope.editMemberJobPerDiem = function(memberJob, dayMember, noEdit){
            if(noEdit) return;
            var previous = memberJob.Member.PerDiemAmount;
            $scope.dialogs.dollars.open(memberJob.Member.PerDiemAmount, function(dollars){
                memberJob.Member.PerDiemAmount = dollars;
                $scope.changeMemberJobPerDiemAmount(memberJob, dayMember, previous);
            }, {title:'Per Diem - '+memberJob.Job.WorkgroupJobNumber});
        }
        $scope.changeMemberJobPerDiemAmount = function(memberJob, dayMember, $previous){
            TimeEntryService.updateMemberJobPerDiemAmount(memberJob, dayMember, $scope.workweek, $previous);
            $scope.changes(true);
        }

        $scope.editMemberJobMeals = function(memberJob, dayMember, noEdit){
            if(noEdit) return;
            $scope.dialogs.meals.open(memberJob.Member.Meals, function(meals){
                memberJob.Member.Meals = meals=='reset'?'':meals?meals:0;
                $scope.changeMemberJobMeals(memberJob, dayMember);
            }, {title:'Meals - '+memberJob.Job.WorkgroupJobNumber});
        }
        $scope.changeMemberJobMeals = function(job, member){
            TimeEntryService.changeMemberJobMeals(job, member, $scope.workweek);
            $scope.changes(true);
        }


        $scope.navEquip = function(dayEquip){
            $scope.workweek.WorkDay[$scope.workweek.currentDay].currentEquip = dayEquip.index;
            TimeEntryService.calcShowTaskRows($scope.workweek);
        }
        $scope.navEquipCarousel = function(dir){
            if(dir > 0){
                equipCarousel.next();
            }else{
                equipCarousel.prev();
            }
        }
        $scope.openEquipSelect = function(){
            $scope.dialogs.radioDialog.open(function(ok, index){
                if(ok){
                    equipCarousel.setActiveIndex(index);
                }
                return true;
            }, {
                title: 'Equipment',
                currentIndex: $scope.workweek.WorkDay[$scope.workweek.currentDay].currentEquip,
                items: $scope.workweek.WorkDay[$scope.workweek.currentDay].equipment.map(function(eq){ 
                    var item = {
                        Number: eq.Number, 
                        Description: eq.Description,
                        Category: eq.Category,
                        rental: eq.TypeCode=='SR',
                        cssClass: eq.cssClass,
                        addToCrew: eq.Jobs[0].Equipment.permanentMessage=='Permanently Add To Crew'
                    };
                    return item;
                }),
                template: 'app/timesheet/equip-select-row.html',
                autoClose: true
            });
        }
        var fixEquipCarousel = function(){
            var day = TimeEntryService.currentDay($scope.workweek);
            var equip = day.equipment[day.currentEquip];
            var index = day.equipment.findIndex(function(eq){ return eq === equip});
            if(index != equipCarousel.getActiveIndex()){
                equipCarousel.setActiveIndex(index);
            }
        }

        $scope.callbacks.initEquipmentTimeResult = function(data){
            TimeEntryService.initEquipmentTime($scope.workweek, $scope, data);
        }

        $scope.resetEquipmentTime = function(dayEquip){
            if(dayEquip){
                if(dayEquip.Jobs.length && !dayEquip.Jobs[0].Equipment.ActiveFlag){
                    dayEquip.Jobs[0].Equipment.ActiveFlag = true;
                    $scope.changeCrewActive('Equipment', true, dayEquip.Jobs[0].Equipment, dayEquip.Jobs[0], dayEquip);
                }else{
                    TimeEntryService.parseTimeToOneEquipment(dayEquip, $scope.workweek);
                    $scope.changes(true);
                }
            }else{
                var equipment = TimeEntryService.currentDay($scope.workweek).equipment;
                for(var e=0; e<equipment.length; ++e){
                    TimeEntryService.parseTimeToOneEquipment(equipment[e], $scope.workweek);
                    $scope.changes(true);
                }
            }
            fixEquipCarousel()
        }

        $scope.resetEquipmentMenu = function(dayEquip){
            $scope.dialogs.radioDialog.open(function(ok, index){
                if(ok){
                    if(index){
                        $scope.resetEquipmentTime();
                    }else{
                        $scope.resetEquipmentTime(dayEquip);
                    }
                }
                return true;
            }, {
                title: 'Reset to Crew Time',
                noRadio: true,
                items: [{label:'Reset Current Equipment'}, {label:'Reset All Equipment'}],
                autoClose: true
            });
        }

        $scope.clearEquipmentTime = function(dayEquip){
            if(dayEquip){
                TimeEntryService.clearEquipmentTime(dayEquip, $scope.workweek);
                $scope.changes(true);
            }else{
                var equipment = TimeEntryService.currentDay($scope.workweek).equipment;
                for(var e=0; e<equipment.length; ++e){
                    TimeEntryService.clearEquipmentTime(equipment[e], $scope.workweek);
                    $scope.changes(true);
                }
            }
        }

        $scope.zeroEquipmentMenu = function(dayEquip){
            $scope.dialogs.radioDialog.open(function(ok, index){
                if(ok){
                    if(index){
                        $scope.clearEquipmentTime();
                    }else{
                        $scope.clearEquipmentTime(dayEquip);
                    }
                }
                return true;
            }, {
                title: 'Zero Out Time',
                noRadio: true,
                items: [{label:'Zero Current Equipment'}, {label:'Zero All Equipment'}],
                autoClose: true
            });
        }

        $scope.editEquipment = function(dayEquip, jobIndex){
            $scope.dialogs.equipInfo.open({
                equip: dayEquip,
                equipJob: dayEquip.Jobs[jobIndex],
                jobIndex: jobIndex
            });
        }

        $scope.closeEquipInfo = function(){
            if(TimeEntryService.shouldHideEquip($scope.dialogs.equipInfo._scope.data.equipJob.Equipment, $scope.workweek)){
                TimeEntryService.changeCurrentDayEquipment(TimeEntryService.currentDay($scope.workweek), $scope.workweek);
            }
            fixEquipCarousel();
            return true;
        }

        $scope.navEquipInfoJob = function(dir){
            $scope.workweek.dialog1.jobIndex += dir;
        }

        $scope.editEquipJobHours = function(equipJob, noEdit){
            if(noEdit) return;
            $scope.dialogs.hours.open(equipJob.Equipment.TotalHours, function(hours){
                equipJob.Equipment.TotalHours = hours;
                $scope.changeEquipJobHours(equipJob);
            }, {title:'Hours - '+equipJob.Job.WorkgroupJobNumber});
        }
        $scope.changeEquipJobHours = function(equipJob){
            TimeEntryService.changeEquipJobHours(equipJob, $scope.workweek);
            $scope.changes(true);
        }
        $scope.editEquipTaskHours = function(equip, task, noEdit){
            if(noEdit) return;
            $scope.dialogs.hours.open(task.TotalHours, function(hours){
                task.TotalHours = hours;
                $scope.changeEquipTaskHours(equip, task);
            }, {title:'Hours - '+task.Task.TaskNumber});
        }
        $scope.changeEquipTaskHours = function(equip, task){
            TimeEntryService.changeEquipTaskHours(equip, task, $scope.workweek);
            $scope.changes(true);
        }
        $scope.editEquipPhaseHours = function(equip, pc, task, noEdit){
            if(noEdit) return;
            $scope.dialogs.hours.open(pc.TotalHours, function(hours){
                pc.TotalHours = hours;
                $scope.changeEquipPhaseHours(equip, pc, task);
            }, {title:'Hours - '+pc.PhaseCode.DisplayNumber});
        }
        $scope.changeEquipPhaseHours = function(equip, pc, task){
            TimeEntryService.changeEquipPhaseHours(equip, pc, task, $scope.workweek);
            $scope.changes(true);
        }

        $scope.callbacks.initSummaryResult = function(data){
            TimeEntryService.initSummary($scope.workweek, $scope, data);
        }
        $scope.review = function(code, force, dayIndex, comment){
            share.review($scope, code, force, dayIndex, comment);
        }
        $scope.zeroHours = function(){
            Utils.notification.toast('Day marked "Zero Hours"', {timeout: 2000 });
            share.unselectJobs($scope.workweek.currentDay)
            share.review($scope, 'ZH', false, $scope.workweek.currentDay);
        }

        $scope.refreshMemberWeekDialog = function(clss){
            $scope.dialogs.memberWeek._scope.data.weekData = TimeEntryService.viewMemberWeekResult($scope.workweek, $scope.dialogs.memberWeek._scope.data.allClasses, clss);
        }
        $scope.callbacks.viewMemberWeekResult = function(data){
            $scope.workweek.weekData = data.Person;
            $scope.refreshMemberWeekDialog($scope.dialogs.memberWeek._scope.data.member.ClassID);
            $scope.workweek.filters.filterClass = $scope.dialogs.memberWeek._scope.data.weekData.Class.find(function(clss){ return clss.ID == $scope.dialogs.memberWeek._scope.data.member.ClassID});
            if(!$scope.workweek.filters.filterClass && $scope.dialogs.memberWeek._scope.data.weekData.Class.length){
                $scope.workweek.filters.filterClass = $scope.dialogs.memberWeek._scope.data.weekData.Class[0];
            }
            $scope.dialogs.memberWeek.open();
        }
        $scope.viewMemberWeek = function(dayMember, dir){
            if($scope.loading) return;
            $scope.dialogs.memberWeek._scope.data.allClasses = dayMember.Class==null;
            if($scope.dialogs.memberWeek.visible){
                $scope.dialogs.memberWeek.hide();
            }
            var date;
            if(dir){
                date = new Date($scope.dialogs.memberWeek._scope.data.weekData.Days[0].Date);
                date.setDate(date.getDate() + 7*dir);
            }
            TimeEntryService.viewMemberWeek($scope.dialogs.memberWeek._scope.data.allClasses?dayMember.Classes[0].Member:dayMember, $scope, date);
        }

        $scope.expandCrews = function(which, day, job){
            if(which == 'day'){
                day.CrewExpanded = !day.CrewExpanded;
                if(day.CrewExpanded && !day.Expanded){
                    day.Expanded = true;
                }
                for(var i=0; i<day.Jobs.length; ++i){
                    if(day.Jobs[i].Crews.length > 1)day.Jobs[i].CrewExpanded = day.CrewExpanded;
                }
            }
            if(which == 'job'){
                job.CrewExpanded = !job.CrewExpanded;
                var expanded = false;
                for(var i=0; i<day.Jobs.length; ++i){
                    if(day.Jobs[i].CrewExpanded){
                        expanded = true;
                        break;
                    }
                }
                day.CrewExpanded = expanded;
            }
        }

        $scope.callbacks.viewEquipWeekResult = function(data){
            $scope.workweek.weekData = data.Equipment;
            $scope.dialogs.equipWeek._scope.data.weekData = TimeEntryService.viewEquipWeekResult($scope.workweek);
            $scope.dialogs.equipWeek.open();
        }
        $scope.viewEquipWeek = function(dayEquip, dir){
            if($scope.loading) return;
            if($scope.dialogs.equipWeek.visible){
                $scope.dialogs.equipWeek.hide();
            }
            var date;
            if(dir){
                date = new Date($scope.dialogs.equipWeek._scope.data.weekData.Days[0].Date);
                date.setDate(date.getDate() + 7*dir);
            }
            TimeEntryService.viewEquipWeek(dayEquip, $scope, date);
        }
        
        $scope.callbacks.historyResult = function(data){
            data = data||{};
            TimeEntryService.currentDay($scope.workweek).statusHistory = Utils.enforceArray(data.StatusHistory).map(function(status){
                status.CreationDateTime = Utils.date(status.CreationDateTime);
                return status;
            });
            $scope.dialogs.statusHistory.open();
        }
        $scope.viewHistory = function(){
            if($scope.loading) return;
            $scope.loadingDialog();
            TimeEntryService.load($scope, 'fte/reviewHistory', 'GET', {workday:TimeEntryService.currentDay($scope.workweek).ID}, 'historyResult');
        }
        $scope.callbacks.reportsDone = function(resp){
            share.reportsDone(resp, $scope);
        }
        $scope.reports = function(){
            share.dialogs.reportLevel.open($scope, TimeEntryService.getReportLevelData($scope.workweek))
        }
        $scope.goToGT = function(){
            $scope.current.nextAction = 'closeTimesheet';
            mainNav.popPage();
        }


        $scope.mapBack = function(obj, name, spMapName){
            spMapName = spMapName||name;
            TimeEntryService.mapSmartpickBack($scope.workweek, obj, name, spMapName);
        }
        $scope.mapSP = function(obj, name, spMapName){
            spMapName = spMapName||name;
            TimeEntryService.mapSmartpick($scope.workweek, obj, name, spMapName);
        }
        var openSp = function(where, sp, id){
            $timeout(function(){
                var location = 'sp_'+where+'_'+sp;
                if(id) location += '_'+id;
                var elem = document.getElementById(location);
                if(elem){
                    var scope = angular.element(elem).scope();
                    if(scope)
                        scope.$select.open = true;
                }
            });
        }
        var dependOn = function(name, loadName, args){
            var obj = $scope.workweek.sp[name];
            if(!obj){
                loadName = loadName||name;
                args = Object.assign({org: $scope.workweek.PersonOrganizationID }, args||{});
                $timeout(function(){
                    TimeEntryService.load($scope, 'fte/sp/'+loadName, 'GET', args, 'dependOn_'+name);
                });
                return false;
            }
            return true;
        }
        $scope.parseTime = TimeEntryService.parseTime;
        $scope.formatTime = TimeEntryService.formatTime;

        $scope.isCompleteWork = TimeEntryService.isCompleteWorkJob


        //Uploads dialog
        ons.createDialog('app/dialog/uploads/uploads-dialog.html').then(function(dialog){
            var dataObj;
            dialog._scope.canEdit = true;
            dialog.open = function(title, obj, uploads){
                dataObj = obj;
                dialog._scope.title = title;
                dialog._scope.openingUpload = false;
                dialog.setUploads(uploads);
                Utils.openDialog(dialog);
            };
            dialog.setUploads = function(uploads){
              dialog._scope.uploads = uploads;
            }
            dialog.setOpening = function(opening){
                dialog._scope.openingUpload = opening;
            }
            dialog._scope.addUpload = function(){
                showPhotoUpload(uploadResult, dialog._scope.title, dataObj);
            }
            dialog._scope.captionEdited = function(upload){
                var photo;
                var photos = dataObj.location?$scope.workweek.Photos.Location[dataObj.location]:$scope.workweek.Photos.Job[dataObj.job]
                if(photo = photos.find(function(dp){ return dp.String == upload.Key})){
                    photo.Description = upload.Description;
                }
            }
            $scope.dialogs.uploads = dialog;
        });

        //Add Member dialog
        $scope.callbacks.dependOn_person = function(data){
            if(data && data.Person){
                $scope.workweek.sp.person = Utils.enforceArray(data.Person);
                $scope.workweek.filters.class = Utils.translateArray($scope.workweek.sp.person, {ClassID: {to:'ID',unique:true}, ClassClass: 'Class', CraftID:'CraftID'}, {noNulls: true, nullItem:{'ID':'','Class':'-- Any --'}}); 
                $scope.workweek.filters.filterClass = $scope.workweek.filters.class[0];
                $scope.workweek.filters.craft = Utils.translateArray($scope.workweek.sp.person, {CraftID: {to:'ID',unique:true}, CraftCraft: 'Craft'}, {noNulls: true, nullItem:{'ID':'','Craft':'-- Any --'}});
                $scope.workweek.filters.filterCraft = $scope.workweek.filters.craft[0];
                $scope.dialogs.addMemberDialog.init();
            }
        }
        ons.createAlertDialog('app/timesheet/add-member-dialog.html').then(function(dialog){
            dialog._scope.selects = {craft:{},class:{},person:{}};
            dialog.init = function(){
                dialog._scope.data = {
                    Person: $scope.workweek.sp.person,
                    Craft: $scope.workweek.filters.craft,
                    Class: $scope.workweek.filters.class
                }
                dialog._scope.selects.craft = $scope.workweek.filters.filterCraft;
                dialog._scope.selects.class = $scope.workweek.filters.filterClass;
                var foreman = TimeEntryService.activeJobs($scope.workweek)[0].JobDayCrewMember.find(function(member){
                    return member.PersonID == $scope.workweek.PersonID;
                });
                dialog._scope.selects.craft = foreman?$scope.workweek.filters.craft.find(function(crft){
                    return crft.ID == foreman.CraftID;
                }):$scope.workweek.filters.craft[0];
                dialog._scope.selects.class = $scope.workweek.filters.class.find(function(clss){
                    return !clss.ID;
                });
                dialog._scope.filterPersons = (TimeEntryService.currentStep($scope.workweek).ProcessStepCode == 'DR');
            }
            dialog.open = function(){
              dialog._scope.selects = {};
              dialog._scope.permanent = false;
              dialog._scope.hidePermanent = false;
              if(dependOn('person')){
                dialog.init();
              }
              Utils.openDialog(dialog);
            }
            dialog._scope.filterAddMember = function(text, crft, clss){
                var regex = new RegExp(Utils.sanitizeTermForRegex(text||''), 'i');
                return function(item){
                    return (!crft||!crft.ID||crft.ID==item.CraftID) && (!clss||!clss.ID||clss.ID==item.ClassID) && (item.FullName.match(regex)||item.CraftCraft.match(regex)||item.ClassClass.match(regex))&&(!dialog._scope.filterPersons||!$scope.workweek.WorkDay[$scope.workweek.currentDay].JobDayCrew[0].JobDayCrewMember.find(function(me){return me.PersonID==item.ID&&(me.ActiveFlag||me.JobCrewMemberActiveFlag)}))
                }
            };
            dialog._scope.clearPerson = function(){
                if(dialog._scope.selects.person){
                    if((dialog._scope.selects.craft && dialog._scope.selects.craft.ID && dialog._scope.selects.craft.ID != dialog._scope.selects.person.CraftID)
                    || (dialog._scope.selects.class && dialog._scope.selects.class.ID && dialog._scope.selects.class.ID != dialog._scope.selects.person.ClassID)){
                        delete dialog._scope.selects.person;
                    }
                }
            }
            dialog._scope.selectPerson = function($item, $model){
                var jdc = $scope.workweek.WorkDay[$scope.workweek.currentDay].JobDayCrew[0], existingClasses = [];
                if(jdc.JobDayCrewMember.findIndex(function(m){return m.PersonID==$item.ID}) > -1){
                    existingClasses = jdc.JobDayCrewMember.reduce(function(classes, member){
                      if(member.PersonID == $item.ID){
                        classes.push(member.ClassID);
                      }
                      return classes;
                    }, []);
                }
                if(existingClasses.length){
                    dialog._scope.hidePermanent = true;
                    dialog._scope.permanent = false;
                }else{
                    dialog._scope.hidePermanent = false;
                }
            };
            dialog._scope.done = function(ok, person, permanent){
                if(ok){
                    $scope.addCrewMember(person, permanent);
                }else{
                    dialog.hide();
                }
            };
            $scope.dialogs.addMemberDialog = dialog;
        });

        //Add equipment dialog
        $scope.callbacks.dependOn_equipment = function(data){
            if(data && data.Equipment){
                $scope.workweek.sp.equipment = Utils.enforceArray(data.Equipment);
                $scope.workweek.filters.category = Utils.translateArray($scope.workweek.sp.equipment, {CategoryID: {to:'ID',unique:true}, CategoryCategory: 'Category'}, {noNulls: true, nullItem:{'ID':'',Category:'-- Any --'}});
                $scope.workweek.filters.filterCategory = $scope.workweek.filters.category[0];
                $scope.dialogs.addEquipmentDialog.init();
            }
        }
        ons.createAlertDialog('app/timesheet/add-equip-dialog.html').then(function(dialog){
          dialog._scope.selects = {equipment:{}};
          dialog._scope.canPermanent = true;
          dialog.init = function(){
            dialog._scope.data = {
                Equipment: $scope.workweek.sp.equipment,
                Category: $scope.workweek.filters.category
            }
            $scope.workweek.filters.filterCategory = $scope.workweek.filters.category.find(function(category){
                return !category.ID;
            });
            dialog._scope.selects.category = $scope.workweek.filters.filterCategory;
          }
          dialog.open = function(){
            dialog._scope.selects = {};
            dialog._scope.permanent = false;
            if(dependOn('equipment')){
                dialog.init();
            }
            Utils.openDialog(dialog);
          }
          dialog._scope.filterAddEquip = function(text, category){
            var regex = new RegExp(Utils.sanitizeTermForRegex(text||''), 'i'), ww=$scope.workweek;
            return function(item){
                return (!category||!category.ID||category.ID==item.CategoryID)&&(item.Number.match(regex)||item.Description.match(regex))&&(!ww.WorkDay[ww.currentDay].JobDayCrew[0].JobDayCrewEquipment.find(function(eq){return eq.EquipmentID==item.ID&&(eq.ActiveFlag||eq.JobCrewEquipmentActiveFlag)}))
            }
          }
          dialog._scope.selectCategory = function(){
            if(dialog._scope.selects.equipment){
                if(dialog._scope.selects.category && dialog._scope.selects.equipment.CategoryID != dialog._scope.selects.category.ID){
                    delete dialog._scope.selects.equipment;
                }
            }
          }
          dialog._scope.openRentals = function(){
            dialog.hide();
            $scope.addCrewRental();
          }
          dialog._scope.done = function(ok, equipment, permanent){
              if(ok){
                  $scope.addCrewEquipment(equipment, permanent);
              }else{
                dialog.hide();
            }
          };
          $scope.dialogs.addEquipmentDialog = dialog;
        });

        //Location percent complete dialog
        ons.createDialog('app/completework/location-percent-complete-dialog.html').then(function(dialog){
            $scope.dialogs.locationPercent = dialog;
            dialog._scope.data = {}
            dialog.originalJobs = {}
            var closing;
            dialog.open = function(jobs, jobScreen){
                closing = false;
                dialog.jobScreen = jobScreen
                dialog.locations = jobs.reduce(function(locs, job){ return locs.concat(job.Location) }, [])
                dialog.originalJobs = jobs
                dialog._scope.data = {
                    showJobs: true,
                    jobs: Utils.copy(jobs)
                }
                Utils.openDialog(dialog)
            }
            dialog._scope.adjustLocationNumber = Utils.adjustLocationNumber
            dialog._scope.editPercent = function(location){
                $scope.dialogs.percent.open(location.PercentComplete, function(value){
                    location.PercentComplete = value
                }, {
                    title: location.NumberChar
                })
            }
            dialog._scope.setLocationActive = function(location){
                if(location.isActive) return;
                for(var j=0; j<dialog._scope.data.jobs.length; ++j){
                    for(var l=0; l<dialog._scope.data.jobs[j].Location.length; ++l){
                        dialog._scope.data.jobs[j].Location[l].isActive = false;
                    }
                }
                location.isActive = true;
            }
            dialog._scope.findLocation = function(){
                $scope.dialogs.findLocation.open(dialog.locations)
            }
            dialog.navigateToNewItem = function(location){
                var id = 'location-percent-complete-'+location.ID
                $timeout(function() {
                    $anchorScroll(id);
                    Utils.flash(id)
                });
            }
            dialog._scope.scrollLocation = function(dir){
                var scroller = dialog._element[0].getElementsByClassName('location-percent-complete-scroll')[0];
                scroller.scrollTop += dir*scroller.clientHeight;
            }
            dialog._scope.done = function(save){
                var jobs = save?dialog._scope.data.jobs:dialog.originalJobs;
                if(!closing && jobs.find(function(jb){ return jb.Location.find(function(loc){ return loc.PercentComplete != loc.OriginalPercentComplete; }); })){
                    closing = true;
                    TimeEntryService.finishLocationPercents($scope, Utils.copy(jobs))
                }
                dialog.hide();
            }
        });

        //Find location
        ons.createAlertDialog('app/completework/find-location-dialog.html').then(function(dialog){
            $scope.dialogs.findLocation = dialog;
            dialog._scope.data = {}
            dialog.open = function(){
                dialog._scope.data.term = ''
                Utils.openDialog(dialog)
            }
            dialog._scope.search = function(){
                if(!dialog._scope.data.term.trim()) return;
                var index = Utils.findArrayBestMatch(dialog._scope.data.term, $scope.dialogs.locationPercent.locations, ['NumberChar'])
                if(index > -1){
                    $scope.dialogs.locationPercent.navigateToNewItem($scope.dialogs.locationPercent.locations[index])
                    dialog.hide()
                }else{
                    Utils.notification.toast('Could not find location', {timeout:2000})
                }
            }
            dialog._scope.close = function(){
                dialog.hide()
            }
        });

        //Time edit dialog
        ons.createAlertDialog('app/timesheet/time-picker-dialog.html').then(function(dialog){
            dialog._scope.time = new Date('00:00');
            dialog.openDialog = function(done, title, time){
              dialog._scope.title = title||'Pick Time';
              dialog._scope.loading = false;
              dialog._scope.hours = true;
              if(time) dialog._scope.time = new Date(time.getTime());
              dialog._scope.done = function(ok, time){
                if(done(ok, time, dialog))
                  dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.timePicker = dialog;
        });

        //Hour edit dialog
        ons.createAlertDialog('app/timesheet/timesheet-hour-dialog.html').then(function(dialog){
            $scope.dialogs.hours = dialog;
            dialog._scope.model = {}
            dialog.open = function(value, callback, options){
                dialog._scope.model.value = value;
                dialog._scope.options = options||{};
                dialog._scope.done = function(ok){
                    if(ok){
                        var doneInner = function(){
                            if(dialog._scope.options.validate&&!dialog._scope.options.validate(dialog._scope.model.value)){
                                return;
                            }
                            callback(dialog._scope.model.value);
                        }
                       var input = angular.element(dialog._element[0]).find('input')[0];
                       if(input==document.activeElement){
                           input.addEventListener('blur', function(){
                                $timeout(doneInner, 10);
                           })
                       }else{
                          $timeout(doneInner, 10);
                       }

                    }
                    dialog.hide();
                }
                Utils.openDialog(dialog);
            }
        });

        //Meal edit dialog
        ons.createAlertDialog('app/timesheet/timesheet-meal-dialog.html').then(function(dialog){
            $scope.dialogs.meals = dialog;
            dialog._scope.model = {}
            dialog.open = function(value, callback, options){
                dialog._scope.model.value = value;
                dialog._scope.options = options||{};
                dialog._scope.done = function(ok){
                    if(ok){
                        var doneInner = function(){
                            if(dialog._scope.options.validate&&!dialog._scope.options.validate(dialog._scope.model.value)){
                                return;
                            }
                            callback(dialog._scope.model.value);
                        }
                       var input = angular.element(dialog._element[0]).find('input')[0];
                       if(input==document.activeElement){
                           input.addEventListener('blur', function(){
                                $timeout(doneInner, 10);
                           })
                       }else{
                          $timeout(doneInner, 10);
                       }

                    }
                    dialog.hide();
                }
                dialog._scope.reset = function(){
                    callback('reset');
                    dialog.hide();
                }
                Utils.openDialog(dialog);
            }
        });

        //Dollar edit dialog
        ons.createAlertDialog('app/timesheet/timesheet-dollar-dialog.html').then(function(dialog){
            $scope.dialogs.dollars = dialog;
            dialog._scope.model = {}
            dialog.open = function(value, callback, options){
                dialog._scope.model.value = value;
                dialog._scope.options = options||{};
                dialog._scope.done = function(ok){
                    if(ok){
                        var doneInner = function(){
                            if(dialog._scope.options.validate&&!dialog._scope.options.validate(dialog._scope.model.value)){
                                return;
                            }
                            callback(dialog._scope.model.value);
                        }
                       var input = angular.element(dialog._element[0]).find('input')[0];
                       if(input==document.activeElement){
                           input.addEventListener('blur', function(){
                                $timeout(doneInner, 10);
                           })
                       }else{
                          $timeout(doneInner, 10);
                       }
                    }
                    dialog.hide();
                }
                Utils.openDialog(dialog);
            }
        });

        //Percent entry dialog
        ons.createAlertDialog('app/timesheet/timesheet-percent-dialog.html').then(function(dialog){
            $scope.dialogs.percent = dialog;
            dialog._scope.model = {}
            dialog.open = function(value, callback, options){
                dialog._scope.model.value = value;
                dialog._scope.options = options||{};
                dialog._scope.done = function(ok){
                    if(ok){
                        var doneInner = function(){
                            if(dialog._scope.options.validate&&!dialog._scope.options.validate(dialog._scope.model.value)){
                                return;
                            }
                            callback(dialog._scope.model.value);
                        }
                       var input = angular.element(dialog._element[0]).find('input')[0];
                       if(input==document.activeElement){
                           input.addEventListener('blur', function(){
                                $timeout(doneInner, 10);
                           })
                       }else{
                          $timeout(doneInner, 10);
                       }
                    }
                    dialog.hide();
                }
                Utils.openDialog(dialog);
            }
        });

        // Add task dialog
        ons.createDialog('app/timesheet/te-pick-task-dialog.html').then(function(dialog){
            dialog.open = function(done, data){
              data.multiSelect = false;
              data.canMultiSelect = false;
              data.filterTasks = ''
              if(!TimeEntryService.isLocationJob(data.job)){
                  data.canMultiSelect = true
              }
              dialog._scope.data = data;
              dialog._scope.job = data.job;
              dialog._scope.phaseCodes = data.phaseCodes;
              dialog._scope.mode = 'Choose Task';
              dialog._scope.taskMode = data.taskMode;
              dialog._scope.done = function(task, isNew){
                  task.CompleteFlag = task.CompleteFlag||addTaskTabbar.getActiveTabIndex()==2;
                  if(done(task, isNew)){
                    dialog.hide();
                    return true;
                  }                  
              }
              dialog._scope.increaseLimit = function(tab){
                  tab.limitTo += 10;
              }
              dialog._scope.multiDone = function(){
                var tasks = []
                for(var i=0; i<dialog._scope.data.taskTypes.length; ++i){
                    var tt = dialog._scope.data.taskTypes[i];
                    if(!tt.tasks) continue;
                    for(var t=0; t<tt.tasks.length; ++t){
                        if(tt.tasks[t].selected){
                            tasks.push(tt.tasks[t]);
                        }
                    }
                }
                tasks.sort(function(a,b){ return a.multiSelectSort-b.multiSelectSort });
                for(var t=0; t<tasks.length; ++t){
                    done(tasks[t]);
                }
                dialog.hide();
                return true;
            }
              Utils.openDialog(dialog, {noFocus:true});
            };
            dialog._scope.close = function(){
                addTaskTabbar.setActiveTab(0);
            }
            var toggleTaskSelected = function(task){
                var prior = !!task.selected2, order = task.multiSelectSort||0;
                task.selected = !prior;
                if(prior){
                    delete task.multiSelectSort;
                }
                for(var i=0; i<dialog._scope.data.taskTypes.length; ++i){
                    var tt = dialog._scope.data.taskTypes[i];
                    if(!tt.tasks) continue;
                    for(var t=0; t<tt.tasks.length; ++t){
                        if(tt.tasks[t].selected){
                            if(prior){
                                if(tt.tasks[t].multiSelectSort > order){
                                    --tt.tasks[t].multiSelectSort;
                                }
                            }else{
                                order = Math.max(order, tt.tasks[t].multiSelectSort||0);
                            }
                        }
                    }
                }
                if(!prior){
                    task.multiSelectSort = order+1;
                }
                task.selected2 = task.selected;
            }
            dialog._scope.selectTask = function(task, multiSelect, $event){
                $event.stopPropagation();
                if(multiSelect){
                    toggleTaskSelected(task);
                    return;
                }
              dialog._scope.done(task);
            };
            dialog._scope.removeNewTask = function(){
                dialog._scope.mode = 'Choose Task';
                delete dialog._scope.data.newTask;
            }
            dialog._scope.createTask = function(){
              Utils.focusInput('newTaskTaskNumber');
              dialog._scope.data.newTask = TimeEntryService.defaultNewTask(dialog._scope.job, $scope.workweek);
              dialog._scope.data.newTask.TaskNumber = ''
              dialog._scope.data.newTask.Description = ''
              dialog._scope.data.newTask.CompleteFlag = false
              dialog._scope.mode = 'Create Task';
            };
            var validateNewTaskNumber = function(num){
              var regex = new RegExp('^'+Utils.sanitizeTermForRegex(num)+'$', 'i');
              var exists = dialog._scope.data.taskTypes.find(function(tt){
                return tt.tasks.find(function(t){
                    return t.TaskNumber.match(regex);
                  });
              });
              return !exists;
            };
            dialog._scope.saveNewTask = function(){
              if(!validateNewTaskNumber(dialog._scope.data.newTask.TaskNumber)){
                Utils.notification.alert('A task named <b>'+dialog._scope.data.newTask.TaskNumber+'</b> already exists.', {title: 'Error'});
                return;
              }
              if(dialog._scope.data.multiSelect){
                dialog._scope.multiDone();
              }
              if(dialog._scope.done(dialog._scope.data.newTask, true)){
                delete dialog._scope.data.newTask;
              }
            };
            dialog._scope.addPhaseCode = function(){
                var phaseCodes = dialog._scope.job.contractPhaseCodes;
                for(var pc=0; pc<phaseCodes.length; ++pc){
                    var phasecode = phaseCodes[pc];
                    phasecode.hidden = false;
                    phasecode.selected = false;
                    var taskPhase = dialog._scope.data.newTask.JobTaskPhaseCode.find(function(phase){ return phase.PhaseCodeID == phasecode.PhaseCodeID })
                    if(taskPhase){
                        if(taskPhase.Disabled){
                            phasecode.hidden = true;
                        }else if(taskPhase.ActiveFlag){
                            phasecode.selected = true;
                        }
                    }
                }
                $scope.dialogs.pickPhaseCode.open({
                    phaseCodes: phaseCodes,
                    taskMode: '',
                    editingTask: false
                });
            };
            dialog._scope.transferTask = function($index){
              var tab = dialog._scope.data.taskTypes[addTaskTabbar.getActiveTabIndex()]
              if(tab.label == 'Workgroup' || !tab.tasks[$index].canComplete)
                return;
              navigator.vibrate(10);
              var newTabLabel = tab.label=='Active'?'Complete':'Active';
              var newTabIndex = dialog._scope.data.taskTypes.findIndex(function(t){return t.label == newTabLabel});
              var task = tab.tasks.splice($index, 1)[0];
              dialog._scope.data.taskTypes[newTabIndex].tasks.push(task);
              task.CompleteFlag = !task.CompleteFlag;
              $timeout(function(){addTaskTabbar.setActiveTab(newTabIndex)});
            }
            $scope.dialogs.pickTask = dialog;
          });

        // Pick phase code dialog
          ons.createDialog('app/timesheet/te-pick-phasecode-dialog.html').then(function(dialog){
            dialog.open = function(data){
              dialog._scope.data = data;
              dialog._scope.data.hasSelection = !!dialog._scope.data.phaseCodes.find(function(pc){return pc.selected});
              dialog._scope.done = function(){
                  $scope.newJobTaskPhaseCodeReturn(dialog);
                  dialog.hide();
              }
              dialog._scope.selectPhaseCode = function(pc){
                if(!pc.selected){
                    pc.selected = true;
                    dialog._scope.data.hasSelection = true;
                }else{
                    pc.selected = false;
                    dialog._scope.data.hasSelection = !!dialog._scope.data.phaseCodes.find(function(pc){return pc.selected});
                }
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.pickPhaseCode = dialog;
        });

        // Task edit dialog
        ons.createAlertDialog('app/timesheet/te-edit-task-dialog.html').then(function(dialog){
            dialog.open = function(done, data){
              dialog._scope.data = data;
              dialog._scope.parent = $scope;
              dialog._scope.showPhaseCodes = true;
              dialog._scope.isLocationTask = $scope.isLocationTask
              dialog._scope.workweek = $scope.workweek
              dialog._scope.done = function(ok){
                if(done(ok))
                  dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.editTask = dialog;
        });

        // Job comment dialog
        ons.createAlertDialog('app/timesheet/job-comment-dialog.html').then(function(dialog){
            dialog.open = function(done, data){
              dialog._scope.data = data;
              dialog._scope.parent = $scope;
              dialog._scope.done = function(ok, comment){
                if(done(ok, comment))
                  dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.editJob = dialog;
        });

        // Job edit dialog
        ons.createAlertDialog('app/timesheet/job-edit-dialog.html').then(function(dialog){
            dialog.open = function(done, data){
              dialog._scope.data = data;
              dialog._scope.parent = $scope;
              dialog._scope.done = function(ok){
                if(done(ok, dialog._scope.data.description.trim(), dialog._scope.data.comment.trim()))
                  dialog.hide();
              }
              Utils.openDialog(dialog, {selector:'#jobCommentArea2'});
            };
            $scope.dialogs.editJobDescription = dialog;
        });

        //Radio Dialog
        ons.createAlertDialog('app/dialog/radio-button-dialog.html').then(function(dialog){
            dialog._scope.selectRow = function(index){
                dialog._scope.data.currentIndex = index;
                if(dialog._scope.data.autoClose){
                    dialog._scope.done(true);
                }
            }
            dialog.open = function(done, options){
                dialog._scope.data = options;
                dialog._scope.data.title = options.title||'Select';
                dialog._scope.data.currentIndex = options.currentIndex||dialog._scope.data.currentIndex;
                dialog._scope.data.items = options.items||[];
                dialog._scope.data.template = options.template||'';
                dialog._scope.data.autoClose = options.autoClose||false;
                dialog._scope.done = function(ok){
                    if(done(ok, dialog._scope.data.currentIndex, dialog._scope.data.items)){
                        if(dialog.visible)dialog.hide();
                    }
                }
                Utils.openDialog(dialog);
            }
            $scope.dialogs.radioDialog = dialog;
        });

        // Member info dialog
        ons.createDialog('app/timesheet/timesheet-edit-member-dialog.html').then(function(dialog){
            dialog.open = function(data){
              dialog._scope.data = data;
              dialog._scope.parent = $scope;
              dialog._scope.done = function(){
                if($scope.closeMemberInfo()){
                    dialog.hide();
                }
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.memberInfo = dialog;
        });

        // Equip info dialog
        ons.createDialog('app/timesheet/timesheet-edit-equip-dialog.html').then(function(dialog){
            dialog.open = function(data){
              dialog._scope.data = data;
              dialog._scope.parent = $scope;
              dialog._scope.done = function(){
                if($scope.closeEquipInfo()){
                    dialog.hide();
                }
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.equipInfo = dialog;
        });

        // Member week time dialog
        ons.createDialog('app/timesheet/member-week-summary-dialog.html').then(function(dialog){
            dialog._scope.data = {};
            dialog._scope.step = function(dir){
                $scope.viewMemberWeek(dialog._scope.data.member, dir);
            }
            dialog._scope.refresh = $scope.refreshMemberWeekDialog
            dialog.open = function(){
              dialog._scope.parent = $scope;
              dialog._scope.done = function(){
                dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.memberWeek = dialog;
        });

        // Equip week time dialog
        ons.createDialog('app/timesheet/equip-week-summary-dialog.html').then(function(dialog){
            dialog._scope.data = {};
            dialog._scope.step = function(dir){
                $scope.viewEquipWeek(dialog._scope.data.equip, dir);
            }
            dialog.open = function(){
              dialog._scope.parent = $scope;
              dialog._scope.done = function(){
                dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.equipWeek = dialog;
        });

        // Status history dialog
        ons.createDialog('app/timesheet/status-history-dialog.html').then(function(dialog){
            dialog._scope.data = {};
            dialog.open = function(){
              dialog._scope.parent = $scope;
              dialog._scope.done = function(){
                dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.statusHistory = dialog;
        });


        /* init / destroy */
        var orientationChange = function(){
            var stepCode = TimeEntryService.currentStep($scope.workweek).ProcessStepCode;
            if(stepCode == 'MT'){
                var index = membersCarousel.getActiveIndex();
                membersCarousel.refresh()
                membersCarousel.setActiveIndex(index);
            }
            if(stepCode == 'ET'){
                var index = equipCarousel.getActiveIndex();
                equipCarousel.refresh()
                equipCarousel.setActiveIndex(index);
            }
        }
        $scope.$on('$destroy', function(){
            var dialogs = Object.keys($scope.dialogs);
            for(var k=0; k<dialogs.length; ++k){
                var dialog = $scope.dialogs[dialogs[k]];
                if(dialog.visible){
                    dialog.hide();
                }
            }
            Rentals.hide();
            UploadDialog.hide();
            ons.orientation.off('change', orientationChange);
        });
        $scope.showPage = function(){
            share.dayOpened();
            if($scope.current.addJobResponse){
                $scope.callbacks.addJobsSaveCallback($scope.current.addJobResponse);
                delete $scope.current.addJobResponse;
            }
            if(($scope.fromGeneralTime&&!$scope.current.showupSitesDone) || $scope.fromDayCompleteWork){
                Utils.notification.hideModal();
            }
            delete $scope.current.completionDone;
            delete $scope.current.showupSitesDone;
        }
        $scope.hidePage = function(){
            Rentals.reset();
            Utils.scrollTo('timesheetCurrentDayCard');
        }
        $scope.callbacks.saveDayCallback = function(){
            share.updateTimesheetStatus();
        }
        $scope.save = function(){
            share.save($scope, 'fte/save', null, 'saveDayCallback');
        }
        ons.orientation.on('change', orientationChange);

        TimeEntryService.initDay($scope);

    }]);

})();
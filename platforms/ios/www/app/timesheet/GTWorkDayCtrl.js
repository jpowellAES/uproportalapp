(function(){
    'use strict';

    angular.module('app').controller('GTWorkDayController', ['$scope', 'ApiService', 'Utils', '$rootScope', '$timeout', '$anchorScroll', 'TimesheetService', 'updateTableHighlight', 'UploadDialog', 'ImgOrPdf', function($scope, ApiService, Utils, $rootScope, $timeout, $anchorScroll, TimesheetService, updateTableHighlight, UploadDialog, ImgOrPdf) {
        
        $scope.current = $scope.$parent.current;
        $scope.workweek = $scope.$parent.current.gtWorkweek;
        $scope.dialogs = {}
        var share = $scope.$parent.current.gtShare;
        $scope.original = share.getOriginal();

        if($scope.current.ftDone){
            delete $scope.current.ftDone;
            $scope.loading = true;
            share.reload(function(ww){
                $scope.workweek = ww;
                $timeout(defaultStatus);
                $scope.loading = false;
                setCallbacks();
                share.setCallbacks($scope);
                TimesheetService.updateDayComms(ww);
            }, {currentDay:$scope.workweek.currentDay});
        }

        $scope.callbacks = Utils.copy($scope.$parent.current.callbacks);
        $scope.checkCallback = function(when){
            when = when||'beforeInit';
            var handled = false, init = $scope.workweek.Init;
            $scope.workweek.Init = false;
            if($scope.workweek.callback && $scope.callbacks[$scope.workweek.callback]){
                var callback = $scope.callbacks[$scope.workweek.callback];
                if(typeof callback == 'function') callback = {when:'beforeInit', fn: callback}
                if(when == callback.when){
                    callback.fn($scope.workweek.callbackData);
                    if(!init) handled = $scope.workweek.callback;
                    delete $scope.workweek.callback;
                    delete $scope.workweek.callbackData;
                }
            }
            return handled;
        }

        $scope.getEditableClass = function(day){
            if(!['PE','IP','RJ','ZH'].includes(day.StatusCode)||!share.getEditable()){
                return '';
            }
            return 'editable'
        }
        $scope.getStatusClass = share.getStatusClass

        var jobTypes = [{
            Name: 'Assigned', filter: {TypeCode:'AJ',AssignedFlag: true}, HasJobs: $scope.workweek.HasAssignedJobs
        },{
            Name: 'Other', filter: {TypeCode:['AJ','DJ'],AssignedFlag: false}, HasJobs: $scope.workweek.HasUnassignedJobs
        },{
            Name: 'Overhead', filter: {TypeCode:'OJ'}, HasJobs: $scope.workweek.HasOverheadJobs
        }];
        $scope.filterJobs = function(jobType){
            var filter = Object.assign(jobType.filter, {active: true});
            var keys = Object.keys(filter);
            return function(item){
                for(var i=0; i<keys.length; i++){
                    var isArray = Array.isArray(filter[keys[i]]);
                    if((isArray&&!filter[keys[i]].includes(item[keys[i]])) || (!isArray && filter[keys[i]] != item[keys[i]]))
                        return false;
                }
                return true;
            }
        }
        $scope.getHasJobs = function(){
            var ww=$scope.workweek;
            for(var i=0; i<ww.WorkDay[ww.currentDay].WorkDayJob.length; ++i){
                if(ww.WorkDay[ww.currentDay].WorkDayJob[i].active){
                    return true;
                }
            }
            return false;
        }

        var addJobTypes = [{
            Code: 'AJ', Name: 'Assigned', filter: {Type:'AJ'}
        },{
            Code: 'OT', Name: 'Other', filter: {Type:['OT','DJ']}
        },{
            Code: 'OJ', Name: 'Overhead', filter: {Type:'OJ'}
        },{
            Code:'WGC', Name: 'Contracts', filter: { Type: ''}
        }];
        var loadJobTypes = function(workweek){
            $scope.addJobTypes = [];
            $scope.jobTypes = [];
            var sequences = {'AJ':[0,1],'OJ':[2,1],'BO':[0,1,2],'OP':[2,0,1]};
            var type = workweek.PersonTimesheetJobTypeCode||'AJ';
            if(type=='AJ'&&$scope.workweek.HasOverheadJobs){
                type = 'BO';
            }else if(type=='OJ'&&$scope.workweek.HasAssignedJobs){
                type = 'OP';
            }
            for(var i=0; i<sequences[type].length; ++i){
                var index = sequences[type][i];
                $scope.addJobTypes.push(addJobTypes[index]);
                $scope.jobTypes.push(jobTypes[index]);
            }
            if(workweek.CanQuickAddJobs){
                $scope.addJobTypes.push(addJobTypes[3]);
            }
        }
        loadJobTypes($scope.workweek);

        $scope.filterAddJobs = function(code, statusFilter, numberFilter){
            var filter = $scope.addJobTypes.find(function(jt){return jt.Code == code}).filter;
            var keys = Object.keys(filter);
            var regex = (numberFilter&&numberFilter.trim().length)?(new RegExp(Utils.sanitizeTermForRegex(numberFilter), 'i')):null;
            return function(item){
                if(code == 'OT'){
                    if(['AJ','OJ'].includes($scope.workweek.PersonTimesheetJobTypeCode||'AJ') && item.Type!=='DJ'){
                        return false;
                    }
                }
                for(var i=0; i<keys.length; i++){
                    var isArray = Array.isArray(filter[keys[i]]);
                    if((isArray&&!filter[keys[i]].includes(item[keys[i]])) || (!isArray && filter[keys[i]] != item[keys[i]]))
                        return false;
                }
                if(!item.selected&&!item.crewEquipSelected){
                    if(statusFilter.Code != 'ALL' && statusFilter.Code != item.StatusCode) return false;
                    var match = !regex||item.NumberChar.match(regex)||item.WorkgroupJobNumber.match(regex)||item.Description.match(regex);
                    if(!match) return false;
                }
                return true;
            }
        }
        
        $scope.showJobBadge = function(jobs, type){
            var num = jobs.filter(function(job){
                var count = job.Type==type||(type=='OT'&&job.Type == 'DJ')
                return count&&(job.selected||job.crewEquipSelected);
            }).length;
            if(num){
                return num;
            }
            return '';
        }

        var defaultStatus = function(){
            if($scope.workweek.filters.jobStatus){
                return;
            }
            var pr = $scope.workweek.jobStatuses.find(function(st){return st.Code == 'PR'});
            if(pr){
                $scope.workweek.filters.jobStatus = pr;
            }else if($scope.workweek.jobStatuses.length){
                $scope.workweek.filters.jobStatus = $scope.workweek.jobStatuses[0];
            }
        }
        $timeout(defaultStatus);
        var gtOtherJobsCallback = function(data){
            var jobs = data.Job;
            if(jobs){
                $scope.workweek.Job = Utils.enforceArray($scope.workweek.Job);
                jobs = Utils.enforceArray(jobs);
                for(var i=0; i<jobs.length; ++i){
                    if(!$scope.workweek.Job.find(function(j){ return j.ID == jobs[i].ID })){
                        $scope.workweek.Job.push(jobs[i])
                        for(var d=0; d<$scope.workweek.WorkDay.length; d++){
                            if(!$scope.workweek.WorkDay[d].Job.find(function(j){ return j.ID == jobs[i].ID })){
                                $scope.workweek.WorkDay[d].Job.push(Utils.copy(jobs[i]));
                            }
                        }
                    }
                }
            }
            $scope.workweek.LoadedOtherJobs = true;
        }
        var gtAddJobsContractCallback = function(data){
            if(data){
                $scope.workweek.WGManagedContract = Utils.enforceArray(data.WGManagedContract);
            }
        }
        $scope.changeJobTab = function(event){
            if($scope.addJobTypes[event.index].Code == 'OT' && !$scope.workweek.LoadedOtherJobs){
                TimesheetService.load($scope, 'timesheet/otherJobs', 'GET', {
                    workweek:$scope.workweek.ID
                }, 'otherJobsCallback');
                return;
            }
            //Load contracts
            if(event.index == 3){
                if(typeof $scope.workweek.WGManagedContract == "undefined"){
                    TimesheetService.load($scope, 'timesheet/addJob/contracts', 'GET', {
                        org:$scope.workweek.PersonOrganizationID,
                        person:$scope.workweek.MyPersonID
                    }, 'addJobsContractCallback')
                }
            }
        }
        var gtAddJobsSaveCallback = function(data){
            if(data && data.Job){
                TimesheetService.trackNewJob(data.Job, $scope);
                jobsTabbarGT.setActiveTab($scope.addJobTypes.findIndex(function(jt){ return jt.Code==data.Job.Type }));
            }
        }
        $scope.quickAddJob = function(contract){
            mainNav.pushPage('app/addjob/add-job.html', {contract: contract, process: {name:'gt'}});
        }
        $scope.selectStartJob = function(job, myTime, crewEquip, $event){
            $event.stopPropagation();
            job.checkbox = !!job.checkbox2;
            if(job.hasDirect){
                Utils.notification.toast('Job has hours on a Foreman timesheet', {timeout: 3000, replace: true})
            }else if(crewEquip&&!$scope.workweek.hasCrewEquip){
                Utils.notification.toast('No crew equipment assigned', {timeout: 3000, replace: true})
            }
            if(!$scope.editable || job.hasDirect || (crewEquip&&!$scope.workweek.hasCrewEquip)){
                $event.preventDefault();
                return false;
            }
            TimesheetService.selectStartJob(job, myTime, crewEquip, $scope.workweek);
            job.checkbox2 = job.checkbox;
            return false;
        }


        $scope.loadingDialog = function(text){
            if(text){
                Utils.notification.toast(text);
            }
        }
        $scope.changes = function(changes){
            TimesheetService.changes($scope.workweek,changes);
        }

        var initDay = function(){
            TimesheetService.autoAddJobs($scope.workweek, $scope.workweek.currentDay);
        }
        $scope.navDay = function(dir){
            $scope.workweek.currentDay += dir;
            initDay();
        }
        $scope.changeJobs = function(){
            if(!$scope.workweek.WorkDay[$scope.workweek.currentDay].addingJob){
                $scope.workweek.WorkDay[$scope.workweek.currentDay].addingJob = true;
                $scope.workweek.WorkDay[$scope.workweek.currentDay].currentJobType = $scope.addJobTypes[0].Code 
            }else{
                $scope.workweek.filters.jobNumber = '';
                TimesheetService.addRemoveJobs($scope.workweek, $scope.workweek.currentDay);
                TimesheetService.updateDayComms($scope.workweek);
                $scope.workweek.WorkDay[$scope.workweek.currentDay].addingJob = false;
            }
        }

        $scope.toastJobDescription = function(job){
            Utils.notification.hideToast();
            Utils.notification.toast(job.JobDescription, {timeout:2000});
            Utils.flash('gt-job-number-'+job.JobID)
        }
        $scope.toastEquipDescription = function(equip, which){
            Utils.notification.hideToast();
            Utils.notification.toast(equip.EquipmentDescription, {timeout:2000});
            Utils.flash('gt-equip-'+which+'-'+equip.EquipmentID)
        }

        $scope.updateGrid = function(value, type, dayIndex, jobID, equipID){
            $scope.changes(true);
            TimesheetService.enterValue(value, type, dayIndex, $scope.workweek, jobID, equipID);
        }

        $scope.clearTableHighlight = function(){
            var cell = document.querySelector('td.timesheet-active-cell');
            if(cell){
                updateTableHighlight(angular.element(cell), "blur");
            }
        }

        $scope.openCommunication = function(com, language){
            language = language||'';
            com.reviewed = true;
            if(com.TypeCode == 'DOC'){
                $scope.loading = true;
                ImgOrPdf.load('communication/'+com['FileString'+language]).finally(function(){ $scope.loading = false; });
            }else{
                cordova.InAppBrowser.open(com['URL'+language], '_system');
            }
        }
        $scope.markCommunicationDone = function(com){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            $scope.changes(true);
            if(com.Person.ReviewedFlag){
                com.Person.ReviewedDate = Utils.date(day.WeekdayDate,'MM/dd/yyyy');
                if(!day.Comm.find(function(c){ return !c.Person.ReviewedFlag })){
                    day.showSafety = false;
                }
            }else{
                com.Person.ReviewedDate = '';
            }
            com.Person.changed = true
        }

        var showUpSitesSaveDone = function(resp, data){
            Utils.notification.hideToast();
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay];
            var jobIds = day.showUpSites.reduce(function(jobs,site){ jobs.push(site.ID); return jobs; }, [])
            $scope.current.job = {ID:jobIds.join(',')};
            $scope.current.job.process = {name:'GT'};
            mainNav.replacePage('app/showup/job-sites.html');
        }
        $scope.openShowupSites = function(){
            TimesheetService.showUpSites($scope);
        }

        $scope.editMyHours = function(which){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay]
            if(!$scope.editable||!$scope.hasJobs||$scope.loading||(which=='DT'&&!day.hasDoubleTime)){
                return;
            }
            $scope.dialogs.hours.open(day[which+'Hours'], function(hours){
                day[which+'Hours'] = hours;
                $scope.updateGrid(hours, 'MyHours,'+which, $scope.workweek.currentDay)
            }, {title: 'My '+which+' Hours'});
        }
        $scope.editMyPD = function(){
            var day = $scope.workweek.WorkDay[$scope.workweek.currentDay]
            if(!$scope.editable||!$scope.hasJobs||$scope.loading||!day.WeekdayHours){
                return;
            }
            $scope.dialogs.dollars.open(day.TotalPerDiem, function(amount){
                day.TotalPerDiem = amount;
                $scope.updateGrid(amount, 'MyPerDiem', $scope.workweek.currentDay)
            }, {title:'My Per Diem'});
        }
        $scope.editMyEquipHours = function(equip, $event){
            if(!$scope.editable||!$scope.hasJobs||$scope.loading){
                return;
            }
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(equip.TotalHours, function(hours){
                equip.TotalHours = hours;
                $scope.updateGrid(equip.TotalHours, 'MyEquipHours', $scope.workweek.currentDay, null, equip.EquipmentID)
            }, {title:equip.EquipmentEquipmentNumber+' Hours'});
            $event.stopPropagation();
        }
        $scope.editJobHours = function(job, which, jobEditable, $event){
            if(!$scope.editable||!jobEditable||$scope.loading||(which=='DT'&&!job.ContractAllowDoubleTimeFlag&&!job.AllowOHDoubleTimeFlag)){
                return;
            }
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(job[which+'Hours'], function(hours){
                job[which+'Hours'] = hours;
                $scope.updateGrid(hours, 'JobHours,'+which, $scope.workweek.currentDay, job.JobID)
            }, {title:which+' Hours - '+job.JobWorkgroupJobNumber});
            $event.stopPropagation();
        }
        $scope.editJobPD = function(job, jobEditable, $event){
            if(!$scope.editable||(!jobEditable&&!job.CanChangeDirect)||$scope.loading||!job.TotalHours){
                return;
            }
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.dollars.open(job.PerDiem, function(amount){
                job.PerDiem = amount;
                $scope.updateGrid(amount, 'JobPerDiem', $scope.workweek.currentDay, job.JobID)
            }, {title:' PD - '+job.JobWorkgroupJobNumber});
            $event.stopPropagation();
        }
        $scope.editEquipHours = function(job, equip, equipEditable, $event){
            if(!$scope.editable||!equipEditable||$scope.loading){
                return;
            }
            var element = angular.element($event.target);
            updateTableHighlight(element, "focus", element.attr('group-cells'));
            $scope.dialogs.hours.open(equip.TotalHours, function(hours){
                equip.TotalHours = hours;
                $scope.updateGrid(hours, 'EquipHours', $scope.workweek.currentDay, job.JobID, equip.EquipmentID)
            }, {title:equip.EquipmentEquipmentNumber+' Hours - '+job.JobWorkgroupJobNumber});
            $event.stopPropagation();
        }

        var uploadResult = function(resp, title, obj){
            $scope.loading = false;
            $scope.dialogs.uploads.setOpening(false)
            if(resp && resp.Key){
                //Leaving the location stuff in here (copied over from FT) ... shouldn't affect anything
                var which = obj.location?'Location':'Job', id = obj.location?obj.location:obj.job;
                if(!$scope.workweek.Photos[which][id]){
                    $scope.workweek.Photos[which][id] = []
                }
                $scope.workweek.Photos[which][id].push({String:resp.Key,Description:resp.Description});
                var uploads = $scope.workweek.Photos[which][id].map(function(photo){
                    return {Key: photo.String, Description: photo.Description}
                });
                if($scope.dialogs.uploads.visible){
                    $scope.dialogs.uploads.setUploads(uploads);
                }else{
                    $scope.dialogs.uploads.open(title, obj, uploads);
                }
                if(which == 'Location'){
                    //TimesheetService.updateTaskPhotos(id, $scope.workweek)
                }else{
                    TimesheetService.updateJobPhotos(id, $scope.workweek)
                }
            }
        }
        var showPhotoUpload = function(callback, title, obj){
            obj.route = 'photo'
            UploadDialog.show({save: function(data){
                if(data.description) obj.description = data.description;
                ApiService.getFormData(obj, {photo:data.photoUrl}).then(function(formdata){
                    $scope.loading = true;
                    if($scope.dialogs.uploads.visible){
                        $scope.dialogs.uploads.setOpening(true)
                    }
                    var done = function(resp){callback(resp,title,obj)};
                    ApiService.post('gt', obj, '', '', formdata)
                    .then(done, done);
                });
                UploadDialog.hide();
            }});
        }
        $scope.jobPhotos = function(job){
            var title = 'Job Photos - '+job.JobWorkgroupJobNumber
            var obj = {workweek: $scope.workweek.ID, job:job.JobID}
            if(!$scope.workweek.Photos.Job[job.JobID]){
                showPhotoUpload(uploadResult, title, obj);
            }else{
                $scope.dialogs.uploads.open(title, obj, $scope.workweek.Photos.Job[job.JobID].map(function(photo){
                    return {Key: photo.String, Description: photo.Description}
                }));
            }
        }

        var openCrewPopup = function(){
            $scope.workweek.crew.CrewEquipment.sort(function(a,b){
                if(a.MyEquipmentFlag != b.MyEquipmentFlag) return a.MyEquipmentFlag?-1:1;
                return a.EquipmentEquipmentNumber-b.EquipmentEquipmentNumber;
            });
            $scope.dialogs.crew.open($scope.workweek.crew.CrewEquipment)
        }
        var gtOpenCrewResult = function(resp){
            if(resp.Crew){
                resp.Crew.CrewEquipment = Utils.enforceArray(resp.Crew.CrewEquipment)
                $scope.workweek.crew = resp.Crew;
                openCrewPopup();
            }else{
                share.dialogs.alert.open({
                    title: 'No Crew',
                    content: 'Unable to find a crew for this person.',
                    buttons: [{
                        label: 'Ok'
                    }],
                    cancelable: true
                });
            }
        }
        $scope.openCrew = function(){
            if(!$scope.workweek.crew){
                TimesheetService.load($scope, 'timesheet/crew', 'GET', {crew: $scope.workweek.CrewID}, 'openCrewResult');
                return;
            }
            openCrewPopup();
        }

        $scope.openComments = function(job){
            $scope.dialogs.jobComment.open(function(ok, comment){
                if(ok){
                    job.JobComment = (comment||'').trim();
                    $scope.changes(true);
                }
                return true;
            }, {job: job, title: job.TypeCode=='OJ'?job.JobDescription:(job.JobWorkgroupJobNumber+' ('+job.JobJobNumberChar+')'),comment: (job.JobComment||'').toString()});
        }

        $scope.saveCrew = function(){
            //Add new
            var crewEquip = $scope.dialogs.crew._scope.model.CrewEquipment
            for(var i=0; i<crewEquip.length; i++){
                var equip = $scope.workweek.crew.CrewEquipment.find(function(eq){ return eq.EquipmentID == crewEquip[i].EquipmentID});
                if(!equip){
                    TimesheetService.addCrewEquipment($scope, crewEquip[i]);
                }else if(!crewEquip[i].MyEquipmentFlag != !equip.MyEquipmentFlag){
                    TimesheetService.modifyCrewEquipment($scope.workweek, crewEquip[i]);
                }
            }
            //Remove deleted
            for(var i=0; i<$scope.workweek.crew.CrewEquipment.length; i++){
                var equip = crewEquip.find(function(eq){ return eq.EquipmentID == $scope.workweek.crew.CrewEquipment[i].EquipmentID});
                if(!equip){
                    TimesheetService.removeCrewEquipment($scope.workweek, $scope.workweek.crew.CrewEquipment[i]);
                }
            }
            $scope.workweek.hasCrewEquip = false;
            for(var i=0; i<crewEquip.length; ++i){
                if(!crewEquip[i].MyEquipmentFlag){
                    $scope.workweek.hasCrewEquip = true;
                }
            }
            $scope.workweek.crew.CrewEquipment = crewEquip;
            var crew = Utils.copy($scope.workweek.crew)
            delete crew.equipment;
            delete crew.categories;
            delete crew.filterCategories;
            TimesheetService.save($scope, 'timesheet/saveCrew', {workweek: $scope.workweek.ID,Crew:crew});
        }

        $scope.review = function(code, force, dayIndex, comment){
            share.review($scope, code, force, dayIndex, comment)
        }

        var gtftOpenResult = function(data){
            Utils.notification.hideToast();
            $scope.current.preloadedWorkWeek = data
            $scope.current.fromWorkWeekID = $scope.workweek.ID
            $scope.current.timesheet = {title:data.WorkWeek.PersonFullName,StartDate:Utils.date(data.WorkWeek.Start),EndDate:Utils.date(data.WorkWeek.End),WorkWeekID:data.WorkWeek.ID}
            mainNav.replacePage('app/timesheet/fte-work-week.html');
        }
        $scope.goToForemanTime = function(job, force){
            var equip = job.WorkDayJobEquipment.find(function(e){return !!e.JobDayCrewEquipmentID});
            var data = job.JobDayCrewMemberID+'~'+Utils.date($scope.workweek.WorkDay[$scope.workweek.currentDay].WeekdayDate,'MM/dd/yyyy')+'~'+(equip?equip.JobDayCrewEquipmentID:'');
            var gpsDone = function(position){
                if(position){
                    data += '~'+position.coords.latitude+'~'+position.coords.longitude;
                }
                if($scope.workweek.changes && !force){
                    Utils.notification.confirm("Save changes?", {
                      callback: function(ok){
                        if(ok == 0){
                            TimesheetService.save($scope, 'timesheet/saveAndOpenFT', {data: data}, 'ftOpenResult', 'Opening Foreman Timesheet');
                        }else if(ok == 1){
                            $scope.goToForemanTime(job, true);
                        }
                      },
                      buttonLabels: ["Save","Discard","Cancel"]
                  });
                }else{
                    TimesheetService.load($scope, 'timesheet/openFT', 'GET', {data: data}, 'ftOpenResult', 'Opening Foreman Timesheet');
                }
            }
            Utils.getGpsPosition(function(position){
                gpsDone(position)
            }, function(){
                gpsDone()
            }) 
        }

        /* Dialogs */
        // Job comment dialog
        ons.createAlertDialog('app/timesheet/job-comment-dialog.html').then(function(dialog){
            dialog.open = function(done, data){
              dialog._scope.data = data;
              dialog._scope.parent = $scope;
              dialog._scope.done = function(ok, comment){
                if(done(ok, comment))
                  dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            $scope.dialogs.jobComment = dialog;
        });

        //Uploads dialog
        ons.createDialog('app/dialog/uploads/uploads-dialog.html').then(function(dialog){
            var dataObj;
            dialog._scope.canEdit = true;
            dialog.open = function(title, obj, uploads){
                dataObj = obj;
                dialog._scope.title = title;
                dialog._scope.openingUpload = false;
                dialog.setUploads(uploads);
                Utils.openDialog(dialog);
            };
            dialog.setUploads = function(uploads){
              dialog._scope.uploads = uploads;
            }
            dialog.setOpening = function(opening){
                dialog._scope.openingUpload = opening;
            }
            dialog._scope.addUpload = function(){
                showPhotoUpload(uploadResult, dialog._scope.title, dataObj);
            }
            dialog._scope.captionEdited = function(upload){
                var photo;
                var photos = dataObj.location?$scope.workweek.Photos.Location[dataObj.location]:$scope.workweek.Photos.Job[dataObj.job]
                if(photo = photos.find(function(dp){ return dp.String == upload.Key})){
                    photo.Description = upload.Description;
                }
            }
            $scope.dialogs.uploads = dialog;
        });
        
        //Hour edit dialog
        ons.createAlertDialog('app/timesheet/timesheet-hour-dialog.html').then(function(dialog){
            $scope.dialogs.hours = dialog;
            dialog._scope.model = {}
            dialog.open = function(value, callback, options){
                dialog._scope.model.value = value;
                dialog._scope.options = options||{};
                dialog._scope.done = function(ok){
                    if(ok){
                        var doneInner = function(){
                            if(dialog._scope.options.validate&&!dialog._scope.options.validate(dialog._scope.model.value)){
                                return;
                            }
                            callback(dialog._scope.model.value);
                        }
                       var input = angular.element(dialog._element[0]).find('input')[0];
                       if(input==document.activeElement){
                           input.addEventListener('blur', function(){
                                $timeout(doneInner, 10);
                           })
                       }else{
                          $timeout(doneInner, 10);
                       }
                    }
                    dialog.hide();
                }
                Utils.openDialog(dialog);
            }
        });

        //Dollar edit dialog
        ons.createAlertDialog('app/timesheet/timesheet-dollar-dialog.html').then(function(dialog){
            $scope.dialogs.dollars = dialog;
            dialog._scope.model = {}
            dialog.open = function(value, callback, options){
                dialog._scope.model.value = value;
                dialog._scope.options = options||{};
                dialog._scope.done = function(ok){
                    if(ok){
                        var doneInner = function(){
                            if(dialog._scope.options.validate&&!dialog._scope.options.validate(dialog._scope.model.value)){
                                return;
                            }
                            callback(dialog._scope.model.value);
                        }
                       var input = angular.element(dialog._element[0]).find('input')[0];
                       if(input==document.activeElement){
                           input.addEventListener('blur', function(){
                                $timeout(doneInner, 10);
                           })
                       }else{
                          $timeout(doneInner, 10);
                       }
                    }
                    dialog.hide();
                }
                Utils.openDialog(dialog);
            }
        });

        //Crew dialog
        ons.createAlertDialog('app/timesheet/timesheet-crew-dialog.html').then(function(dialog){
            $scope.dialogs.crew = dialog;
            dialog._scope.model = {}
            dialog.open = function(crewEquip){
                dialog._scope.editable = $scope.editable
                dialog._scope.model.changes = false;
                dialog._scope.model.loading = false;
                dialog._scope.model.CrewEquipment = Utils.copy(crewEquip)
                Utils.openDialog(dialog);
            }
            dialog._scope.done = function(ok){
                if(ok){
                    $scope.saveCrew()
                }
               dialog.hide();
            }
            dialog._scope.addCrewEquipment = function(){
                $scope.dialogs.equipment.open()
            }
            dialog._scope.mine = function(equip, flip, $event){
                if(!dialog._scope.editable) return;
                if(flip){
                    equip.MyEquipmentFlag = !equip.MyEquipmentFlag;
                }else{
                    $event.stopPropagation()
                }
                dialog._scope.model.changes = true;
            }
            dialog.equipSelected = function(equip){
                dialog._scope.model.changes = true;
                dialog._scope.model.CrewEquipment.push({
                    EquipmentID: equip.ID,
                    EquipmentCategoryID: equip.CategoryID,
                    CategoryCategory: equip.CategoryCategory,
                    EquipmentDescription: equip.Description,
                    EquipmentEquipmentNumber: equip.Number,
                    CategorySortOrder: equip.CategorySortOrder,
                    MyEquipmentFlag: false
                });
            }
        });

        //Equip select
        var gtLoadCrewEquipResult = function(resp){
            $scope.dialogs.crew._scope.model.loading = false;
            $scope.workweek.crew.equipment = Utils.enforceArray(resp.Equipment);
            $scope.workweek.crew.categories = [{ID:'',Category:'All'}];
            $scope.workweek.crew.filterCategory = $scope.workweek.crew.categories[0];
            var ids = [];
            for(var i=0; i<$scope.workweek.crew.equipment.length; i++){
                if(ids.indexOf($scope.workweek.crew.equipment[i].CategoryID) == -1){
                    $scope.workweek.crew.categories.push({
                        ID: $scope.workweek.crew.equipment[i].CategoryID,
                        Category: $scope.workweek.crew.equipment[i].CategoryCategory
                    });
                    ids.push($scope.workweek.crew.equipment[i].CategoryID);
                }
            }
            $scope.dialogs.equipment.init()
        }
        ons.createAlertDialog('app/timesheet/add-equip-dialog.html').then(function(dialog){
          dialog._scope.selects = {equipment:{}};
          dialog.init = function(){
            if(!$scope.workweek.crew.equipment){
                $scope.dialogs.crew._scope.model.loading = true;
                TimesheetService.load($scope, 'timesheet/equipment', 'GET', {organization: $scope.workweek.PayrollPeriodOrganizationID}, 'loadCrewEquipResult');
                return;
            }
            dialog._scope.data = {
                Equipment: $scope.workweek.crew.equipment,
                Category: $scope.workweek.crew.categories
            }
            $scope.workweek.crew.filterCategory = $scope.workweek.crew.categories.find(function(category){
                return !category.ID;
            });
            dialog._scope.selects.category = $scope.workweek.crew.filterCategory;
          }
          dialog.open = function(){
            dialog._scope.selects = {};
            dialog.init();
            Utils.openDialog(dialog);
          }
          dialog._scope.filterAddEquip = function(text, category){
            var regex = new RegExp(Utils.sanitizeTermForRegex(text||''), 'i');
            return function(item){
                return (!category||!category.ID||category.ID==item.CategoryID)&&(item.Number.match(regex)||item.Description.match(regex))&&(!$scope.dialogs.crew._scope.model.CrewEquipment.find(function(eq){return eq.EquipmentID==item.ID}))
            }
          }
          dialog._scope.selectCategory = function(){
            if(dialog._scope.selects.equipment){
                if(dialog._scope.selects.category && dialog._scope.selects.equipment.CategoryID != dialog._scope.selects.category.ID){
                    delete dialog._scope.selects.equipment;
                }
            }
          }
          dialog._scope.done = function(ok, equipment, permanent){
              if(ok){
                 $scope.dialogs.crew.equipSelected(equipment)
              }
              dialog.hide();
          };
          $scope.dialogs.equipment = dialog;
        });

        var setCallbacks = function(){
            $scope.callbacks.saveDayCallback = gtSaveDayCallback;
            $scope.callbacks.otherJobsCallback = gtOtherJobsCallback;
            $scope.callbacks.addJobsContractCallback = gtAddJobsContractCallback;
            $scope.callbacks.addJobsSaveCallback = gtAddJobsSaveCallback;
            $scope.callbacks.openCrewResult = gtOpenCrewResult;
            $scope.callbacks.ftOpenResult = gtftOpenResult;
            $scope.callbacks.loadCrewEquipResult = gtLoadCrewEquipResult;
            $scope.callbacks.showUpSitesSaveDone = showUpSitesSaveDone;
        }

        /* init / destroy */
        $scope.$on('$destroy', function(){
            var dialogs = Object.keys($scope.dialogs);
            for(var k=0; k<dialogs.length; ++k){
                var dialog = $scope.dialogs[dialogs[k]];
                if(dialog.visible){
                    dialog.hide();
                }
            }
        });
        $scope.showPage = function(){
            share.dayOpened();
            if($scope.current.addJobResponse){
                $scope.callbacks.addJobsSaveCallback($scope.current.addJobResponse);
                delete $scope.current.addJobResponse;
            }
        }
        $scope.hidePage = function(){
            Utils.scrollTo('timesheetCurrentDayCard');
        }
        var gtSaveDayCallback = function(){
            share.updateTimesheetStatus();
        }
        $scope.save = function(){
            share.save($scope, '', null, 'saveDayCallback');
        }
        setCallbacks();
        initDay()

    }]);

})();
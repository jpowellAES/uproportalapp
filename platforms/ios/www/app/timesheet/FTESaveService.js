(function(){
    'use strict';

    angular.module('app').service('FTESaveService', ['Utils', function(Utils){
        
        var _csvQuote = function(string){
            if(!string) return '';
            var quoted = string;
            if(quoted.match(/[,"\n]/i)){
                quoted = quoted.replace(/"/ig,'""');
                return '"'+quoted+'"';
            }
            return quoted;
        }
        var _isTemp = function(id){
            return Utils.isTempId(id);
        }

        var _compareExpenseEntry = function(current, original){
            var changed = false;
            if(!original) changed = true;
            if(current.TypeCode == 'PD'){
                changed = changed || current.Amount !== original.Amount
                    || current.BillingAmount !== original.BillingAmount
                    || current.LaborUnitID !== original.LaborUnitID
                    || current.ContractLaborUnitID !== original.ContractLaborUnitID
                    || current.AcceptedDateTime !== original.AcceptedDateTime
                    || current.AcceptedByUserID !== original.AcceptedByUserID
                    || current.PhaseCodeID !== original.PhaseCodeID;
            }else if(current.TypeCode == 'ME'){
                changed = changed || current.Qty !== original.Qty
                     || current.BillingQty !== original.BillingQty;
            }
            if(changed){
                current.AcceptedDateTime = Utils.null(current.AcceptedDateTime);
                if(current.AcceptedDateTime)
                    current.AcceptedDateTime = Utils.date(current.AcceptedDateTime, 'MM/dd/yyyy HH:mm:ss');
            }
            return changed;
        }
        var _getExpenseEntry = function(member, originalMember, memberReturn, ee){
            var me = member.ExpenseEntry[ee];
            var expenseEntry = {
                data: ["ExpenseEntry,"+me.ID],
                changed: false
            }
            if((me.TypeCode == 'PD' && !me.Amount) || (me.TypeCode == 'ME' && !me.Qty)) me.active = false; //Don't save zero per diems or meals
            if(me.active && ((me.TypeCode == 'PD' && member.PerDiemFlag) || me.TypeCode == 'ME')){
                var originalEE = (originalMember&&originalMember.ExpenseEntry&&originalMember.ExpenseEntry.length)?originalMember.ExpenseEntry.find(function(eem){return eem.ID==me.ID}):null;
                expenseEntry.changed = _compareExpenseEntry(me,originalEE)
                if(expenseEntry.changed){
                    expenseEntry.data[0] += ","+Utils.null(me.Amount)+","+Utils.null(me.BillingAmount)+","+Utils.null(me.LaborUnitID)+","+Utils.null(me.ContractLaborUnitID)+","+me.AcceptedDateTime+","+Utils.null(me.AcceptedByUserID)+","+Utils.null(me.PhaseCodeID)+","+me.TypeCode+","+Utils.null(me.Qty);
                }
            }else{
                if(!_isTemp(me.ID)){
                    expenseEntry.changed = true;
                    expenseEntry.data[0] += ",DELETE";
                }
                member.ExpenseEntry.splice(ee, 1);
            }
            if(expenseEntry.changed){
                memberReturn.data.push(expenseEntry.data.join('\n'));
                memberReturn.changed = true;
            }
            return expenseEntry;
        }

        var _compareTimeEntry = function(current, original){
            var changed = false;
            if(!original) changed = true;
            changed = changed || current.Amount !== original.Amount
                || current.BillableAmount !== original.BillableAmount
                || current.AcceptedDateTime !== original.AcceptedDateTime
                || current.AcceptedByUserID !== original.AcceptedByUserID
                || current.BillableHours !== original.BillableHours
                || current.TimeInHours !== original.TimeInHours
                || current.JobDayCrewMemberTaskID !== original.JobDayCrewMemberTaskID
                || current.JobDayCrewTaskID !== original.JobDayCrewTaskID
                || current.JobTaskID !== original.JobTaskID
                || current.LaborTypeCode !== original.LaborTypeCode
                || current.PhaseCodeID !== original.PhaseCodeID
                || current.StatusCode !== original.StatusCode;
            if(changed){
                current.TimeDate = Utils.date(current.TimeDate, 'MM/dd/yyyy');
                current.AcceptedDateTime = Utils.null(current.AcceptedDateTime);
                if(current.AcceptedDateTime)
                    current.AcceptedDateTime = Utils.date(current.AcceptedDateTime, 'MM/dd/yyyy HH:mm:ss');
            }
            return changed;
        }
        var _getTimeEntry = function(member, originalMember, memberReturn, te){
            var me = member.TimeEntry[te];
            var timeEntry = {
                data: ["TimeEntry,"+me.ID],
                changed: false
            }            
            if(me.TimeInHours > 0 || me.BillableHours > 0){
                var originalTE = (originalMember&&originalMember.TimeEntry&&originalMember.TimeEntry.length)?originalMember.TimeEntry.find(function(tm){return tm.ID==me.ID}):null;
                timeEntry.changed = _compareTimeEntry(me,originalTE)
                if(timeEntry.changed){
                    timeEntry.data[0] += ","+me.Amount+","+me.BillableAmount+","+me.TimeInHours+","+me.BillableHours+","+me.AcceptedDateTime+","+Utils.null(me.AcceptedByUserID)+","+me.TimeDate+","+me.StatusCode;
                    if(!originalTE || _isTemp(me.ID)){
                        timeEntry.data[0] += ","+me.PhaseCodeID+","+me.LaborTypeCode+","+me.JobTaskID+","+me.JobDayCrewTaskID+","+me.JobDayCrewMemberTaskID;
                    }
                }
            }else{
                if(!_isTemp(me.ID)){
                    timeEntry.changed = true;
                    timeEntry.data[0] += ",DELETE";
                }
                member.TimeEntry.splice(te, 1);
            }
            if(timeEntry.changed){
                memberReturn.data.push(timeEntry.data.join('\n'));
                memberReturn.changed = true;
            }
            return timeEntry;
        }

        var _compareMemberTaskPhaseCode = function(current, original){
            if(!original) return true;
            return current.STHours !== original.STHours
                || current.OTHours !== original.OTHours
                || current.DTHours !== original.DTHours;
        }
        var _getMemberTaskPhaseCode = function(task, originalTask, taskReturn, p){
            var pc = {
                data: ["MemberTaskPhaseCode,"+task.MemberTaskPhaseCode[p].ID],
                changed: false
            }
            if(task.MemberTaskPhaseCode[p].active){
                var originalPC = (originalTask&&originalTask.MemberTaskPhaseCode&&originalTask.MemberTaskPhaseCode.length)?originalTask.MemberTaskPhaseCode.find(function(phase){return phase.ID==task.MemberTaskPhaseCode[p].ID}):null;
                pc.changed = _compareMemberTaskPhaseCode(task.MemberTaskPhaseCode[p],originalPC)
                if(pc.changed){
                    pc.data[0] += ","+task.MemberTaskPhaseCode[p].STHours+","+task.MemberTaskPhaseCode[p].OTHours+","+task.MemberTaskPhaseCode[p].DTHours;
                    if(!originalPC || _isTemp(task.MemberTaskPhaseCode[p].ID)){
                        pc.data[0] += ","+task.MemberTaskPhaseCode[p].PhaseCodeID;
                    }
                }
            }else{
                if(!_isTemp(task.MemberTaskPhaseCode[p].ID)){
                    pc.changed = true;
                    pc.data[0] += ",DELETE";
                }
                task.MemberTaskPhaseCode.splice(p, 1);
            }
            if(pc.changed){
                taskReturn.data.push(pc.data.join('\n'));
                taskReturn.changed = true;
            }
            return pc;
        }
        var _compareMemberTask = function(current, original){
            var changed = false;
            if(original){
                changed =  current.STHours !== original.STHours
                        || current.OTHours !== original.OTHours
                        || current.DTHours !== original.DTHours;
                if(!changed && current.StartTime){
                    changed = !Utils.compareDate(current.StartTime,original.StartTime)
                           || !Utils.compareDate(current.EndTime,original.EndTime);
                }
            }else{
                changed = true;
            }
            if(changed && current.StartTime){
                _cleanTaskTime(current)
            }
            return changed;
        }
        var _getMemberTask = function(member, originalMember, memberReturn, t){
            var me = member.MemberTask[t];
            var task = {
                data: ["MemberTask,"+me.ID],
                changed: false,
                includeJobTask: false
            }
            if(me.active){
                var originalTask = (originalMember&&originalMember.MemberTask&&originalMember.MemberTask.length)?originalMember.MemberTask.find(function(tsk){return tsk.ID==me.ID}):null;
                task.changed = _compareMemberTask(me,originalTask)
                task.includeJobTask = true
                if(task.changed){
                    task.data[0] += ","+me.STHours+","+me.OTHours+","+me.DTHours+","+Utils.null(me.PhaseCodeSectionID)+","+Utils.null(me.TaskID);
                    if(me.StartDate){
                        task.data[0] += ","+me.StartHour+","+me.StartMinute+","+me.StartDate+","+me.EndHour+","+me.EndMinute+","+me.EndDate;
                    }
                }
                if(me.MemberTaskPhaseCode){
                    for(var p=me.MemberTaskPhaseCode.length-1; p>=0; p--){
                        _getMemberTaskPhaseCode(me, originalTask, task, p);
                    }
                }
            }else{
                if(!_isTemp(me.ID)){
                    task.changed = true;
                    task.data[0] += ",DELETE";
                }
                member.MemberTask.splice(t, 1);
            }
            if(task.changed){
                memberReturn.data.push(task.data.join('\n'));
                memberReturn.changed = true;
            }
            return task;
        }

        var _compareJobTaskPhaseCode = function(current, original){
            if(!original) return true;
            return current.ActiveFlag !== original.ActiveFlag;
        }
        var _getJobTaskPhaseCode = function(task, originalTask, taskReturn, p){
            var pc = {
                data: ["JobTaskPhaseCode,"+task.JobTaskPhaseCode[p].ID],
                changed: false
            }
            var originalPC = originalTask?originalTask.JobTaskPhaseCode.find(function(phase){return phase.ID==task.JobTaskPhaseCode[p].ID}):null;
            pc.changed = _compareJobTaskPhaseCode(task.JobTaskPhaseCode[p],originalPC)
            if(pc.changed){
                pc.data[0] += ","+Utils.bool(task.JobTaskPhaseCode[p].ActiveFlag);
                if(!originalPC || _isTemp(task.JobTaskPhaseCode[p].ID)){
                    pc.data[0] += ","+task.JobTaskPhaseCode[p].PhaseCodeID+","+Utils.null(task.JobTaskPhaseCode[p].OverridePhaseCodeID);
                }
            }
            if(pc.changed){
                taskReturn.data.push(pc.data.join('\n'));
                taskReturn.changed = true;
            }
            return pc;
        }
        var _compareJobTask = function(current, original, usedTasks){
            console.log(current, original, usedTasks)
            if(_isTemp(current.ID)) return true;
            if(!original){
                if(usedTasks.indexOf(current.ID) > -1){
                    return 'reference'
                }
                return false;
            }
            return current.ActiveFlag !== original.ActiveFlag
                || current.CompleteFlag !== original.CompleteFlag;
        }
        var _getJobTask = function(job, originalJob, jobReturn, t, usedTasks){
            var task = {
                data: ["JobTask,"+job.JobTask[t].ID],
                changed: false
            }
            var originalTask = originalJob?originalJob.JobTask.find(function(tsk){return tsk.ID==job.JobTask[t].ID}):null;
            task.changed = _compareJobTask(job.JobTask[t], originalTask, usedTasks)
            if(task.changed){
                task.data[0] += ","+Utils.bool(job.JobTask[t].ActiveFlag, false)+","+Utils.bool(job.JobTask[t].CompleteFlag,false);
                if(!originalTask || _isTemp(job.JobTask[t].ID)){
                    task.data[0] += ","+_csvQuote(job.JobTask[t].TaskNumber)+","+_csvQuote(job.JobTask[t].Description)+","+Utils.null(job.JobTask[t].PhaseCodeSectionID);
                }
            }
            if((task.changed || originalTask) && job.JobTask[t].JobTaskPhaseCode){
                for(var p=job.JobTask[t].JobTaskPhaseCode.length-1; p>=0; p--){
                    _getJobTaskPhaseCode(job.JobTask[t], originalTask, task, p);
                }
            }
            if(task.changed){
                jobReturn.data.push(task.data.join('\n'));
                jobReturn.changed = true;
            }
            return task;
        }

        var _comparePhaseCode = function(current, original){
            if(!original) return true;
            return current.EarnedHours !== original.EarnedHours
                || current.STHours !== original.STHours
                || current.OTHours !== original.OTHours
                || current.DTHours !== original.DTHours;
        }
        var _getPhaseCode = function(task, originalTask, taskReturn, p){
            var pc = {
                data: ["PhaseCode,"+task.PhaseCode[p].JobDayCrewTaskPhaseCodeID+","+task.PhaseCode[p].ID],
                changed: false
            }
            if(task.PhaseCode[p].active){
                var originalPC = originalTask?originalTask.PhaseCode.find(function(phase){return phase.ID==task.PhaseCode[p].ID}):null;
                pc.changed = _comparePhaseCode(task.PhaseCode[p],originalPC)
                if(pc.changed){
                    pc.data[0] += ","+task.PhaseCode[p].EarnedHours+","+task.PhaseCode[p].STHours+","+task.PhaseCode[p].OTHours+","+task.PhaseCode[p].DTHours;
                }
            }else{
                if(!_isTemp(task.PhaseCode[p].JobDayCrewTaskPhaseCodeID)){
                    pc.changed = true;
                    pc.data[0] += ",DELETE";
                }
                task.PhaseCode.splice(p, 1);
            }
            if(pc.changed){
                taskReturn.data.push(pc.data.join('\n'));
                taskReturn.changed = true;
            }
            return pc;
        }
        var _getDaysBetweenDates = function(d0, d1) {
          var msPerDay = 8.64e7;
          // Copy dates so don't mess them up
          var x0 = new Date(d0);
          var x1 = new Date(d1);
          // Set to noon - avoid DST errors
          x0.setHours(12,0,0);
          x1.setHours(12,0,0);
          // Round to remove daylight saving errors
          return Math.round( Math.abs(x1 - x0) / msPerDay );
        }
        var _cleanTaskTime = function(task){
            task.AdditionalDays = _getDaysBetweenDates(task.StartTime, task.EndTime);
            task.StartHour = task.StartTime.getHours();
            task.StartMinute = task.StartTime.getMinutes();
            task.StartDate = Utils.date(task.StartTime, 'MM/dd/yyyy');
            task.EndHour = task.EndTime.getHours();
            task.EndMinute = task.EndTime.getMinutes();
            task.EndDate = Utils.date(task.EndTime, 'MM/dd/yyyy');
        }
        var _compareTask = function(current, original){
            var changed = false;
            if(original){
                changed =  current.Duration !== original.Duration
                        || current.OrderNumber !== original.OrderNumber
                        || current.TaskComment !== original.TaskComment
                        || current.STHours !== original.STHours
                        || current.OTHours !== original.OTHours
                        || current.DTHours !== original.DTHours
                        || current.AdditionalDays !== original.AdditionalDays
                        || current.LocationID !== original.LocationID
                        || !Utils.compareDate(current.StartTime,original.StartTime)
                        || !Utils.compareDate(current.EndTime,original.EndTime);
            }else{
                changed = true;
            }
            if(changed){
                _cleanTaskTime(current);
            }
            return changed;
        }
        var _getTask = function(job, originalJob, jobReturn, t){
            var task = {
                data: ["Task,"+job.Task[t].ID+","+job.Task[t].JobTaskID],
                changed: false,
                includeJobTask: false
            }
            if(job.Task[t].active){
                var originalTask = originalJob?originalJob.Task.find(function(tsk){return tsk.ID==job.Task[t].ID}):null;
                task.changed = _compareTask(job.Task[t],originalTask)
                task.includeJobTask = true;
                if(task.changed){
                    task.data[0] += ","+job.Task[t].Duration+","+job.Task[t].OrderNumber+","+_csvQuote(job.Task[t].TaskComment)+","+job.Task[t].STHours+","+job.Task[t].OTHours+","+job.Task[t].DTHours+","+job.Task[t].AdditionalDays+","+job.Task[t].StartDate+","+job.Task[t].EndDate+","+job.Task[t].EndHour+","+job.Task[t].EndMinute+","+job.Task[t].StartHour+","+job.Task[t].StartMinute+","+Utils.null(job.Task[t].LocationID);
                }
                if(job.Task[t].PhaseCode){
                    for(var p=job.Task[t].PhaseCode.length-1; p>=0; p--){
                        _getPhaseCode(job.Task[t], originalTask, task, p);
                    }
                }
            }else{
                if(!_isTemp(job.Task[t].ID)){
                    task.changed = true;
                    task.data[0] += ",DELETE";
                }
                job.Task.splice(t, 1);
            }
            if(task.changed){
                jobReturn.data.push(task.data.join('\n'));
                jobReturn.changed = true;
            }
            return task;
        }

        var _compareEquipTimeEntry = function(current, original){
            var changed = false;
            if(!original) changed = true;
            changed = changed || current.AcceptedDateTime !== original.AcceptedDateTime
                || current.AcceptedByUserID !== original.AcceptedByUserID
                || current.BillableHours !== original.BillableHours
                || current.TimeInHours !== original.TimeInHours
                || current.JobDayCrewTaskID !== original.JobDayCrewTaskID
                || current.JobTaskID !== original.JobTaskID
                || current.PhaseCodeID !== original.PhaseCodeID
                || current.StatusCode !== original.StatusCode;
            if(changed){
                current.TimeDate = Utils.date(current.TimeDate, 'MM/dd/yyyy');
                current.AcceptedDateTime = Utils.null(current.AcceptedDateTime);
                if(current.AcceptedDateTime)
                    current.AcceptedDateTime = Utils.date(current.AcceptedDateTime, 'MM/dd/yyyy HH:mm:ss');
            }
            return changed;
        }
        var _getEquipTimeEntry = function(equip, originalEquip, equipReturn, te){
            var me = equip.EquipmentTimeEntry[te];
            var timeEntry = {
                data: ["EquipmentTimeEntry,"+me.ID],
                changed: false
            }            
            if(me.TimeInHours > 0 || me.BillableHours > 0){
                var originalTE = (originalEquip&&originalEquip.EquipmentTimeEntry&&originalEquip.EquipmentTimeEntry.length)?originalEquip.EquipmentTimeEntry.find(function(tm){return tm.ID==me.ID}):null;
                timeEntry.changed = _compareEquipTimeEntry(me,originalTE)
                if(timeEntry.changed){
                    timeEntry.data[0] += ","+me.TimeInHours+","+me.BillableHours+","+me.AcceptedDateTime+","+Utils.null(me.AcceptedByUserID)+","+me.StatusCode;
                    if(!originalTE || _isTemp(me.ID)){
                        timeEntry.data[0] += ","+me.PhaseCodeID+","+me.JobTaskID+","+me.JobDayCrewTaskID;
                    }
                }
            }else{
                if(!_isTemp(me.ID)){
                    timeEntry.changed = true;
                    timeEntry.data[0] += ",DELETE";
                }
                equip.EquipmentTimeEntry.splice(te, 1);
            }
            if(timeEntry.changed){
                equipReturn.data.push(timeEntry.data.join('\n'));
                equipReturn.changed = true;
            }
            return timeEntry;
        }

        var _compareEquipment = function(current, original){
            if(!original) return true;
            return current.ActiveFlag !== original.ActiveFlag
                || current.InactiveReasonCode !== original.InactiveReasonCode
                || current.StatusCode !== original.StatusCode
                || current.TotalHours !== original.TotalHours
                || current.JobCrewEquipmentID !== original.JobCrewEquipmentID
                || current.hasPermanentChanges;
        }
        var _getEquipment = function(job, originalJob, jobReturn, e){
            var me = job.JobDayCrewEquipment[e];
            var equip = {
                data: ["JobDayCrewEquipment,"+me.ID+","+me.EquipmentID],
                changed: false
            }
            var originalEquipment = originalJob?originalJob.JobDayCrewEquipment.find(function(eq){return eq.ID==me.ID}):null;
            equip.changed = _compareEquipment(me,originalEquipment)
            if(equip.changed){
                equip.data[0] += ","+Utils.bool(me.ActiveFlag)+","+me.TotalHours+","+Utils.null(me.InactiveReasonCode)+","+me.StatusCode+","+Utils.null(me.JobCrewEquipmentID);
                var newRental = _isTemp(me.ID) && me.EquipmentTypeCode == 'SR';
                if(me.hasPermanentChanges || newRental){
                    equip.data[0] += ","+Utils.bool(me.hasPermanentChanges);
                    delete me.hasPermanentChanges;
                }
                if(newRental){
                    equip.data[0] += ","+_csvQuote(me.EquipmentEquipmentNumber)+","+_csvQuote(me.EquipmentDescription)+","+me.CategoryID+","+Utils.null(me.RentalCompanyID)+","+_csvQuote(Utils.null(me.RentalCompanyName));
                }
            }
            if(me.EquipmentTimeEntry && me.EquipmentTimeEntry.length){
                for(var te=me.EquipmentTimeEntry.length-1; te>=0; te--){
                    _getEquipTimeEntry(me, originalEquipment, equip, te);
                }
            }
            if(equip.changed){
                jobReturn.data.push(equip.data.join('\n'));
                jobReturn.changed = true;
            }
            return equip;
        }
        var _compareMember = function(current, original){
            if(!original) return true;
            return current.ActiveFlag !== original.ActiveFlag
                || current.CraftID !== original.CraftID
                || current.ClassID !== original.ClassID
                || current.TotalHours !== original.TotalHours
                || current.STHours !== original.STHours
                || current.OTHours !== original.OTHours
                || current.DTHours !== original.DTHours
                || current.PerDiemFlag !== original.PerDiemFlag
                || current.MemberStatusCode !== original.MemberStatusCode
                || current.InactiveReasonCode !== original.InactiveReasonCode
                || current.NoPerDiemReasonCode !== original.NoPerDiemReasonCode
                || current.StatusCode !== original.StatusCode
                || current.JobCrewMemberID !== original.JobCrewMemberID
                || current.hasPermanentChanges;
        }
        var _getMember = function(job, originalJob, jobReturn, m){
            var member = {
                data: ["JobDayCrewMember,"+job.JobDayCrewMember[m].ID+","+job.JobDayCrewMember[m].PersonID],
                changed: false,
                usedJobTasks: []
            }
            var originalMember = originalJob?originalJob.JobDayCrewMember.find(function(mbr){return mbr.ID==job.JobDayCrewMember[m].ID}):null;
            member.changed = _compareMember(job.JobDayCrewMember[m],originalMember)
            var me = job.JobDayCrewMember[m];
            if(member.changed){
                member.data[0] += ","+Utils.bool(me.ActiveFlag)+","+me.CraftID+","+me.ClassID+","+me.TotalHours+","+me.STHours+","+me.OTHours+","+me.DTHours+","+Utils.bool(me.PerDiemFlag)+","+me.MemberStatusCode+","+Utils.null(me.InactiveReasonCode)+","+Utils.null(me.NoPerDiemReasonCode)+","+me.StatusCode+","+Utils.null(me.JobCrewMemberID);
                if(me.hasPermanentChanges){
                    member.data[0] += ","+Utils.bool(true);
                }
                delete me.hasPermanentChanges;
            }
            if(job.JobDayCrewMember[m].MemberTask && job.JobDayCrewMember[m].MemberTask.length){
                for(var mt=job.JobDayCrewMember[m].MemberTask.length-1; mt>=0; mt--){
                    var memTask = _getMemberTask(job.JobDayCrewMember[m], originalMember, member, mt);
                }
            }
            if(job.JobDayCrewMember[m].TimeEntry && job.JobDayCrewMember[m].TimeEntry.length){
                for(var te=job.JobDayCrewMember[m].TimeEntry.length-1; te>=0; te--){
                    _getTimeEntry(job.JobDayCrewMember[m], originalMember, member, te);
                }
            }
            if(job.JobDayCrewMember[m].ExpenseEntry && job.JobDayCrewMember[m].ExpenseEntry.length){
                for(var ee=job.JobDayCrewMember[m].ExpenseEntry.length-1; ee>=0; ee--){
                    _getExpenseEntry(job.JobDayCrewMember[m], originalMember, member, ee);
                }
            }
            if(member.changed){
                jobReturn.data.push(member.data.join('\n'));
                jobReturn.changed = true;
            }
            return member;
        }

        var _compareJob = function(current, original){
            if(!original) return true;
            return current.TotalHours !== original.TotalHours 
                || current.GrandTotalHours !== original.GrandTotalHours
                || current.NonCompHours !== original.NonCompHours
                || current.STHours !== original.STHours
                || current.OTHours !== original.OTHours
                || current.DTHours !== original.DTHours
                || current.OrderNumber !== original.OrderNumber
                || current.RatioOverridenFlag !== original.RatioOverridenFlag
                || current.StatusCode !== original.StatusCode 
                || current.JobComment !== original.JobComment
                || !Utils.compareDate(current.StartTime,original.StartTime) 
                || !Utils.compareDate(current.EndTime,original.EndTime);
        }

        var _getJob = function(day, originalDay, dayReturn, j){
            var me = day.JobDayCrew[j];
            var job = {
                data: ["JobDayCrew,"+me.ID+","+me.JobCrewJobID],
                changed: false
            }
            if(me.active){
                var originalJob = originalDay.JobDayCrew.find(function(jb){return jb.JobCrewJobID==me.JobCrewJobID});
                job.changed = _compareJob(me,originalJob)
                if(job.changed){
                    job.data[0] += ","+me.TotalHours+","+me.GrandTotalHours+","+me.NonCompHours+","+me.STHours+","+me.OTHours+","+me.DTHours+","+me.OrderNumber+","+Utils.bool(me.RatioOverridenFlag)+","+me.StatusCode+","+Utils.date(me.StartTime, 'MM/dd/yyyy HH:mm:ss')+","+Utils.date(me.EndTime, 'MM/dd/yyyy HH:mm:ss')+","+me.JobCrewID+","+_csvQuote(me.JobComment);
                    if(me.QuickAddJob){
                        job.data[0] += ","+me.QuickAddJob
                    }
                }
                var usedTasks = [];
                if(me.JobDayCrewMember){
                    for(var m=me.JobDayCrewMember.length-1; m>=0; m--){
                        var mem = _getMember(me, originalJob, job, m);
                        for(var i=0; i<mem.usedJobTasks.length; ++i){
                            usedTasks.push(mem.usedJobTasks[i])
                        }
                    }
                }
                if(me.JobDayCrewEquipment){
                    for(var e=me.JobDayCrewEquipment.length-1; e>=0; e--){
                        _getEquipment(me, originalJob, job, e);
                    }
                }
                var taskJob = {data:[],changed:false}
                if(me.Task){
                    for(var t=me.Task.length-1; t>=0; t--){
                        var tsk = _getTask(me, originalJob, taskJob, t);
                        if(tsk.includeJobTask){
                            usedTasks.push(me.Task[t].JobTaskID);
                        }
                    }
                }
                if(me.JobTask){
                    for(var jt=me.JobTask.length-1; jt>=0; jt--){
                        _getJobTask(me, originalJob, job, jt, usedTasks);
                    }
                }
                if(taskJob.changed){
                    job.data.push(taskJob.data.join('\n'));
                    job.changed = true;
                }
            }else{
                if(!_isTemp(me.ID)){
                    job.changed = true;
                    job.data[0] += ",DELETE";
                }
                day.JobDayCrew.splice(j, 1);
            }
            if(job.changed){
                dayReturn.data.push(job.data.join('\n'));
                dayReturn.changed = true;
            }
        }
        var _compareDay = function(current, original){
            var changed = current.WeekdayHours !== original.WeekdayHours
                || current.STHours !== original.STHours
                || current.OTHours !== original.OTHours
                || current.DTHours !== original.DTHours
                || current.PerDiemTotal !== original.PerDiemTotal
                || current.StatusCode != original.StatusCode;
            if(!changed){
                var currentStep = current.DayProcessCompletion.findIndex(function(step){ return !step.CompleteFlag; }), originalStep = original.DayProcessCompletion.findIndex(function(step){ return !step.CompleteFlag; });
                if(currentStep != originalStep) changed = true;
            }
            return changed;
        }
        var _getDay = function(workweek, original, d){
            var me = workweek.WorkDay[d];
            var day = {
                data: ["WorkDay,"+me.ID],
                changed: _compareDay(me,original.WorkDay[d])
            }
            if(day.changed){
                day.data[0] += ","+me.WeekdayHours+","+me.STHours+","+me.OTHours+","+me.DTHours+","+me.PerDiemTotal+","+me.StatusCode+","+me.DayProcessCompletion[me.currentStep].ProcessStepCode;
                if(me.priorStatus != me.StatusCode && typeof me.reviewComment != 'undefined'){
                    day.data[0] += ","+_csvQuote(me.reviewComment);
                }
            }
            if(d == 0){
                for(var j=me.Job.length-1; j>=0; j--){
                    _getReferenceJob(me, original.WorkDay[d], day, j);
                }
            }
            for(var j=me.JobDayCrew.length-1; j>=0; j--){
                _getJob(me, original.WorkDay[d], day, j);
            }
            for(var j=0; j<me.JobDayCrew.length; ++j){
                me.JobDayCrew[j].index = j;
            }
            return day;
        }
        var _compareReferenceJob = function(current, original){
             if(!original) return true;
            return current.Type !== original.Type
                || current.Description !== original.Description
                || current.StatusCode !== original.StatusCode;
        }
        var _getReferenceJob = function(day, originalDay, dayReturn, j){
            var me = day.Job[j];
            var job = {
                data: ["Job,"+me.ID],
                changed: false
            }
            var originalJob = originalDay.Job.find(function(jb){return jb.ID==me.ID});
            job.changed = _compareReferenceJob(me,originalJob)
            if(job.changed){
                job.data[0] += ","+me.Type+","+_csvQuote(me.Description)+","+me.StatusCode;
            }
            if(job.changed){
                dayReturn.data.push(job.data.join('\n'));
                dayReturn.changed = true;
            }
        }
        var _getSafety = function(workweek){
            var comms = {}
            for(var d=0; d<workweek.WorkDay.length; d++){
                var dayComms = workweek.WorkDay[d].Comm;
                if(dayComms && dayComms.length){
                    for(var c=0; c<dayComms.length; ++c){
                        var comm = comms[dayComms[c].ID];
                        if(!comm){
                            comm = {ID:dayComms[c].ID,people:{}}
                            comms[dayComms[c].ID] = comm;
                        }
                        var ids = Object.keys(dayComms[c].people);
                        for(var i=0; i<ids.length; ++i){
                            var person = dayComms[c].people[ids[i]];
                            if(person.changed){
                                if(!comm.people[ids[i]]){
                                    comm.people[ids[i]] = 'Person,'+ids[i]+','+Utils.bool(person.ReviewedFlag)+','+person.ReviewedDate;
                                    comm.changed = true;
                                }
                                delete person.changed;
                            }
                        }
                    }
                }
            }
            var returnValue = []
            var keys = Object.keys(comms);
            for(var k=0; k<keys.length; ++k){
                if(comms[keys[k]].changed){
                    returnValue.push('Communication,'+keys[k])
                    var people = Object.keys(comms[keys[k]].people)
                    for(var i=0; i<people.length; ++i){
                      people[i] = comms[keys[k]].people[people[i]]
                    }
                    returnValue.push(people.join('\n'))
                }
            }
            return returnValue.length?returnValue.join('\n'):false;
        }
        var _getPostData = function(workweek, original){
            var result = ["WorkWeek,"+workweek.ID+","+workweek.TotalHours+","+workweek.EquipmentHoursTotal+","+workweek.PerDiemTotal+","+workweek.STHours+","+workweek.OTHours+","+workweek.DTHours+","+workweek.Code+","+workweek.StatusCode];
            if(original && typeof workweek.reviewComment != 'undefined'){
                result[0] += ","+_csvQuote(workweek.reviewComment);
            }
            for(var d=0; d<workweek.WorkDay.length; d++){
                var day = _getDay(workweek, original, d);
                if(day.changed){
                    result.push(day.data.join('\n'));
                }
            }
            var safety = _getSafety(workweek);
            if(safety) result.push(safety);
            console.log(result.join('\n'))
            return result.join('\n');
        }

        return {
          getPostData: _getPostData
        };

    }]);


})();

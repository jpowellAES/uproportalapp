(function(){
    'use strict';

    angular.module('app').controller('DesignLocationController', ['$scope', 'CommentDialog', 'ApiService', 'Utils', '$rootScope', 'Smartpick', '$anchorScroll', '$timeout', '$filter', function($scope, CommentDialog, ApiService, Utils, $rootScope, Smartpick, $anchorScroll, $timeout, $filter) {
        $scope.job = $scope.$parent.current.job;
        $scope.design = $scope.$parent.current.design;
        $scope.module = $scope.$parent.current.module;
        $scope.completing = false;
        $scope.editingDetailIndex = -1;
        Smartpick.reset();
        var data = false, dialogs = {};

        $scope.showEst = $scope.design.DesignTypeCode!='OD' || ($scope.design.WorkgroupID != -1 && ($scope.design.WorkgroupID==0 || $scope.design.ImportedFlag));
        $scope.editDes = $scope.design.StatusCode=='IP' && $scope.design.DesignTypeCode=='OD' && (!$scope.design.ImportedFlag || $scope.design.WorkgroupID <= 0);
        
        var sortDetails = function(){
            return;
            $scope.working = true;
            if($scope.location && $scope.location.LocationDetail){
                $scope.location.LocationDetail.sort(function(a,b){                    
                    if(a.ActionWorkAction < b.ActionWorkAction) return -1;
                    if(a.ActionWorkAction > b.ActionWorkAction) return 1;
                    
                    if(a.SpecificationSpecificationNumber < b.SpecificationSpecificationNumber) return -1;
                    if(a.SpecificationSpecificationNumber > b.SpecificationSpecificationNumber) return 1;

                    return 0;
                });
            }
            $scope.working = false;
        };

        var cleanDetail = function(ld, init){
            return ld;
        };
        var cleanDetails = function(details, init){
            details = Utils.enforceArray(details).map(function(detail){return cleanDetail(detail, init)});
            $scope.location.LocationDetail = Utils.enforceArray($scope.location.LocationDetail);
            for(var i=0; i<details.length; i++){
                for(var j=0; j<$scope.location.LocationDetail.length; j++){
                    if($scope.location.LocationDetail[j].JobLocationDetailID == details[i].JobLocationDetailID){
                        details[i] = Object.assign($scope.location.LocationDetail[j], details[i]);
                        break;
                    }
                }
                if(init && details[i].ParentAssemblyID){
                    details[i].visible = false;
                }
                details[i].editing = false;
            }
            $scope.location.LocationDetail = details;
        };

         var cleanFactor = function(factor){
            factor.PercentDisplay = Utils.fixPercent(factor.PercentDisplay);
            return factor;
        };

        var calcTotalFactor = function(location){
            if(location.LFactor.length){
                if(location.UnfactoredHours == 0)
                    return 100;
                return (location.FactoredHours/location.UnfactoredHours)*100;
            }
            return -1;
        }

        var fetchDetails = function(){
            $scope.working = true;
            ApiService.get('designLocation', {location: $scope.location.JobLocationID}).then(fetchResult, fetchResult)
        };

        var fetchResult = function(resp, skipDetails){
            if(resp && resp.JobDesign){
                Object.assign($scope.design, Utils.pick(resp.JobDesign, 'FactoredHours','UnfactoredHours'));
                $scope.$parent.current.calcTotalFactor();
                resp = resp.JobDesign;
            }
            if(resp && resp.Location){
                Object.assign($scope.location, Utils.pick(resp.Location, 'UseAssembliesFlag','FactoredHours','UnfactoredHours'));
                if(!skipDetails)cleanDetails(resp.Location.LocationDetail, true);
                $scope.location.LFactor = Utils.enforceArray(resp.Location.LFactor).map(cleanFactor);
                $scope.location.totalFactor = calcTotalFactor($scope.location);
                $scope.location.lastDetailFetch = new Date().getTime();
            }
            $scope.working = false;
            $scope.loading = false;
        };

        $scope.updateEstimate = function(){
            $scope.loading = true;
            var location = $scope.location;
            $scope.$parent.current.updateEstimate(function(design){
                $scope.design = design;
                $scope.location = design.Location.find(function(l){return l.JobLocationID==location.JobLocationID});
                $scope.location.LocationDetail = location.LocationDetail;
                location = null;
                $scope.loading = false;
            });
        }

        $scope.detailOnLongPress = function(detail) {
            if(!$scope.completing){
                navigator.vibrate(10);
                detail.JobBillingUponCode = $scope.design.JobBillingUponCode;
                $rootScope.$emit('LocationDetailDialog', {
                    type:'init', 
                    detail: detail, 
                    endpoint: 'locationDetailCompletion',
                    location: $scope.location,
                    design: $scope.design
                });
                Utils.openDialog(dialogs.info);
            }
        };

        var addSpecAction = function(specAction, qty, complete, comments, assemblyNum, assemblyDesc, wireSpan, action){
            $scope.job.changed = true;
            $scope.adding = true;
            var obj = {location:$scope.location.JobLocationID,specAction:specAction.ID,qty:qty, comments:comments}
            if(assemblyNum){
                obj.assemblyNum = assemblyNum;
                obj.assemblyDesc = assemblyDesc;
                obj.wireSpan = wireSpan;
                obj.actionID = action.ActionID;
            }
            ApiService.post('designLocationDetail', obj)
            .then(addSpecActionResult,addSpecActionResult);
        };

        var addSpecActionResult = function(resp){
            if(resp && resp.JobDesign){
                resp.JobDesign.Location.LocationDetail = Utils.enforceArray(resp.JobDesign.Location.LocationDetail);
                $scope.location.LocationDetail = $scope.location.LocationDetail;
                resp.JobDesign.Location.LocationDetail.forEach(function(ld){
                    cleanDetail(ld);
                    $scope.location.LocationDetail.push(ld);
                });
                navigateToNewItem(resp.JobDesign.Location.LocationDetail[0]);
            }
            $scope.adding = false;
        };

        var navigateToNewItem = function(detail){
            if($scope.location.UseAssembliesFlag){
                $scope.goToAssembly(detail);
            }else{
                $scope.goToDetail(detail);
            }
        }

        $scope.addDetail = function(){
            if($scope.location.UseAssembliesFlag){
                addAssembly();
            }else{
                addNormalDetail();
            }
            
        };

        var getSpecActionData = function(){
            if(!data){
                var obj = {job:$scope.job.JobID,owner:$scope.job.OwnerID,assemblies:$scope.location.UseAssembliesFlag};
                ApiService.get('designSpecActions', obj)
                .then(function(resp){
                    if(resp && resp.ReferenceAction){
                        data = resp;
                        dialogs.addDetail.setData(resp);
                        dialogs.addAssembly.setData(resp);
                    }
                });
            }
        };

        var addNormalDetail = function(){
            getSpecActionData();
            dialogs.addDetail.openDialog();
        };

        var addAssembly = function(){
            getSpecActionData();
            dialogs.addAssembly.openDialog();
        };

        $scope.useAssemblies = function(){
            $scope.loading = true;
            ApiService.post('designLocationAction', {
                action: 'edit',
                design: $scope.job.JobDesignID, 
                location: $scope.location.JobLocationID,
                useAssemblies: $scope.location.UseAssembliesFlag
            }).then(function(){
                Smartpick.reset();
            }).finally(function(){ $scope.loading = false; });
            
        }

        $scope.getDetailClass = function(detail, byDetail){
            var cls = '';
            if(detail.FailedInspectionFlag){
                cls += ' red';
            }
            if(detail.Complete && detail.CompleteToEstimateButton != 'Uncomplete'){
                cls += ' faded';
            }
            if(!byDetail && $scope.location.UseAssembliesFlag && !detail.AssemblyID){
                cls += ' assembly-item';
            }
            if(detail.editing){
                cls += ' selected';
            }
            return cls;
        }

        $scope.goToAssembly = function(detail){
            var assemblyID = detail.AssemblyID?detail.JobLocationDetailID:detail.ParentAssemblyID;
            $scope.location.LocationDetail.forEach(function(ld){
                if(!ld.AssemblyID){
                    ld.visible = ld.ParentAssemblyID==assemblyID;
                }
            });
            $timeout(function() {
                $anchorScroll('anchor'+assemblyID);
            });
        };

        $scope.goToDetail = function(detail){
            var detailID = detail.JobLocationDetailID;
            $timeout(function() {
                $anchorScroll('anchor'+detailID);
            });
        };

        $scope.canMove = {
            up: false,
            down: false,
            originalIndex: -1,
            originalEditingIndex: -1,
            editingDetail: null,
            getRangeAndIndex: function(detail){
                var range = [], index = 0;
                for(var i=0; i<$scope.location.LocationDetail.length; i++){
                    if((detail.AssemblyID && $scope.location.LocationDetail[i].AssemblyID)||(!detail.AssemblyID && $scope.location.LocationDetail[i].ParentAssemblyID == detail.ParentAssemblyID)){
                        if($scope.location.LocationDetail[i].JobLocationDetailID == detail.JobLocationDetailID){
                            index = range.length;
                        }
                        range.push($scope.location.LocationDetail[i]);
                    }
                }
                return {range: range, index: index};
            },
            range: [],
            calc: function(detail){
                var results = $scope.canMove.getRangeAndIndex(detail);
                $scope.canMove.range = results.range;
                $scope.canMove.editingDetail = detail;
                $scope.canMove.up = results.index > 0;
                $scope.canMove.down = results.index<(results.range.length-1);
            }
        }
        var originalDetail;
        $scope.editDetail = function(detail, $index, $event){
            originalDetail = Object.assign({}, detail);
            var id = originalDetail.JobLocationDetailID;
            $scope.location.LocationDetail.forEach(function(ld){
                ld.editing = false;
                if(originalDetail.AssemblyID){
                    if(!ld.AssemblyID)
                        ld.visible = false;
                }
            });
            detail.editing = true;
            $scope.editingDetailIndex = $index;
            $scope.canMove.calc(detail);
            $scope.canMove.originalEditingIndex = $index;
            $scope.canMove.originalIndex = $scope.location.LocationDetail.findIndex(function(ld){return ld.JobLocationDetailID==detail.JobLocationDetailID});
            $event.stopPropagation();
            return false;
        }
        $scope.cancelEdit = function(detail, $event){
            detail.editing = false;
            Object.assign(detail, originalDetail);
            if($scope.editingDetailIndex != $scope.canMove.originalEditingIndex){
                var detail = ($scope.location.UseAssembliesFlag?$scope.location.LocationDetail.filter(function(ld){return ld.AssemblyID||ld.visible}):$scope.location.LocationDetail)[$scope.editingDetailIndex];
                var detailIndex = $scope.location.LocationDetail.findIndex(function(ld){return ld.JobLocationDetailID==detail.JobLocationDetailID});
                var details;
                if(detail.AssemblyID){
                    var count = 1;
                    for(var i=detailIndex+1; i<$scope.location.LocationDetail.length; i++){
                        if($scope.location.LocationDetail[i].AssemblyID){
                            break;
                        }else if($scope.location.LocationDetail[i].ParentAssemblyID == detail.JobLocationDetailID){
                            count++;
                        }
                    }
                    details = $scope.location.LocationDetail.splice(detailIndex, count);
                }else{
                    details = $scope.location.LocationDetail.splice(detailIndex, 1);
                }
                var args = [$scope.canMove.originalIndex, 0];
                for(var d=0; d<details.length; ++d){
                    args.push(details[d]);
                }
                Array.prototype.splice.call($scope.location.LocationDetail,  args);
            }
            $scope.editingDetailIndex = -1;
            if($event){
                $event.stopPropagation();
                return false;
            }
        }
        $scope.canDeleteDetail = function(editingIndex){
            var detail = ($scope.location&&$scope.location.LocationDetail&&(($scope.location.UseAssembliesFlag?$scope.location.LocationDetail.filter(function(ld){return ld.AssemblyID||ld.visible}):$scope.location.LocationDetail)[$scope.editingDetailIndex]));
            if(detail&&detail.ParentLocationDetailID > 0){
                return false;
            }
            return true;
        }
        $scope.deleteDetail = function(){
            var detail = ($scope.location.UseAssembliesFlag?$scope.location.LocationDetail.filter(function(ld){return ld.AssemblyID||ld.visible}):$scope.location.LocationDetail)[$scope.editingDetailIndex];
            $scope.loading = true;
            var obj = {
                action: 'delete',
                detail: detail.JobLocationDetailID
            };
            ApiService.post('designLocationDetailAction', obj).then(deleteDetailResult,deleteDetailResult);
        }
        var deleteDetailResult = function(resp){
            $scope.loading = false;
            if(resp && resp.JobDesign){;
                cleanDetails(resp.JobDesign.Location.LocationDetail);
                $scope.location.lastDetailFetch = new Date().getTime();
                $scope.editingDetailIndex = -1;
            }
        }
        var detailHasChanges = function(detail){
            if($scope.canMove.originalEditingIndex != $scope.editingDetailIndex)
                return true;
            if(originalDetail.Comments != detail.Comments)
                return true;
            return false;
        }
        var getDetailIndex = function(detail){
            var id = detail.JobLocationDetailID;
            return $scope.canMove.range.findIndex(function(ld){return ld.JobLocationDetailID == id});
        }
        $scope.saveDetail = function(){
            var detail = $scope.canMove.editingDetail;
            if(!detailHasChanges(detail)){
                $scope.cancelEdit(detail);
                return;
            }
            $scope.loading = true;
            var obj = {
                action: 'edit',
                detail: detail.JobLocationDetailID,
                index: getDetailIndex(detail),
                comment: detail.Comments
            };
            ApiService.post('designLocationDetailAction', obj).then(saveDetailResult,saveDetailResult);
        }
        var saveDetailResult = function(resp){
            $scope.loading = false;
            console.log(resp)
            if(resp && resp.JobDesign){;
                cleanDetails(resp.JobDesign.Location.LocationDetail);
                $scope.location.lastDetailFetch = new Date().getTime();
                $scope.editingDetailIndex = -1;
            }
        }
        $scope.moveDetail = function(dir){
            var details;
            var detail = ($scope.location.UseAssembliesFlag?$scope.location.LocationDetail.filter(function(ld){return ld.AssemblyID||ld.visible}):$scope.location.LocationDetail)[$scope.editingDetailIndex];
            var detailIndex = $scope.location.LocationDetail.findIndex(function(ld){return ld.JobLocationDetailID==detail.JobLocationDetailID});
            if(detail.AssemblyID){
                for(var i=detailIndex+dir; i>=0&&i<$scope.location.LocationDetail.length; i+=dir){
                    if($scope.location.LocationDetail[i].AssemblyID){
                        var index = Math.min(detailIndex, i), count = Math.abs(detailIndex - i);
                        details = $scope.location.LocationDetail.splice(index, count);
                        $scope.editingDetailIndex += dir;
                        detailIndex += count;
                        break;
                    }
                }
            }else{
                details = $scope.location.LocationDetail.splice(detailIndex, 1);
                $scope.editingDetailIndex += dir;
                detailIndex += dir;
            }
            var args = [detailIndex, 0];
            for(var i=0; i<details.length; ++i){
                args.push(details[i]);
            }
            Array.prototype.splice.call($scope.location.LocationDetail, args);
            $scope.canMove.calc(detail);
        }
        $scope.detailComment = function(){
            var detail = $scope.location.LocationDetail[$scope.editingDetailIndex];
            CommentDialog.show({
                title: "Comments",
                comment: detail.Comments,
                requireComment: false,
                button: ['accept','OK'],
                accept: function(comment){
                    CommentDialog.hide();
                    detail.Comments = comment;
                }
            });
        }

        var setNewQty = function(which, detail, qty){
            $scope.loading = true;
            var obj = {
                action: 'edit',
                detail: detail.JobLocationDetailID
            }
            obj[which] = qty;
            ApiService.post('designLocationDetailAction', obj).then(saveDetailResult,saveDetailResult);
        }
        $scope.editQuantity = function(which, detail, $event){
            if($scope.loading||$scope.adding||$scope.design.StatusCode != 'IP' || (which == 'DesignQuantity'&&($scope.design.DesignTypeCode!='OD'||($scope.design.ImportedFlag&&$scope.design.WorkgroupID > 0)))){
                return;
            }
            var title = which=='DesignQuantity'?'Design Qty':'Estimated Qty';
            dialogs.duration.openDialog(function(ok, newQty){
                if(ok){
                    setNewQty(which, detail, newQty);
                }
                return true;
            }, detail[which], {title: title, placeholder: detail.UnitOfMeasureAbbreviation||'Quantity'});
            $event.stopPropagation();
            $event.preventDefault();
            return false;
        }


        var saveLocationFactors = function(factors){
            var obj = {
                action: 'edit',
                design: $scope.job.JobDesignID,
                location: $scope.location.JobLocationID,
                LFactor: factors,
                updateEstimate: true
            };
            $scope.loading = true;
            ApiService.post('designLocationAction', obj).then(function(resp){fetchResult(resp, true)}, function(resp){fetchResult(resp, true)});
        }

        var checkLocationFactors = function(originals,changed){
            var changedFactors = [];
            changed.forEach(function(lf){
                var original = originals.find(function(olf){
                    return lf.ContractFactorTypeFactorTypeID == olf.ContractFactorTypeFactorTypeID;
                });
                if(!original || ((lf.FactorUseCode == 'EN' && lf.PercentDisplay != original.PercentDisplay)||(lf.FactorUseCode == 'SE' && lf.ContractFactorID != original.ContractFactorID))){
                    changedFactors.push({
                        FactorTypeID: lf.ContractFactorTypeFactorTypeID,
                        Percent: lf.PercentDisplay/100,
                        ContractFactorID: lf.ContractFactorID
                    });
                }
            });
            return changedFactors;
        }

        $scope.locationFactors = function(){
            var changed = Utils.copy($scope.location.LFactor);
            $scope.$parent.current.locationFactors(changed, function(ok){
                var changedFactors = checkLocationFactors($scope.location.LFactor, changed);
                if(ok && changedFactors.length){
                    saveLocationFactors(changedFactors);
                }
            });
        }
        
        $scope.openLocationDetail = function(detail){
            if($scope.editingDetailIndex >= 0)
                return;
            if(detail.AssemblyID){
                var id = detail.JobLocationDetailID;
                for(var i=0; i<$scope.location.LocationDetail.length; i++){
                    if($scope.location.LocationDetail[i].ParentAssemblyID == id){
                        $scope.location.LocationDetail[i].visible = !$scope.location.LocationDetail[i].visible;
                    }
                }
            }
        }

        $scope.showDetailRow = function(detail){
            if($scope.location.UseAssembliesFlag && !detail.AssemblyID){
                return detail.visible;
            }
            return true;
        }

        $scope.filterByDetail = function(detail){
            return !detail.AssemblyID;
        }

        $scope.canNav = {
            left: true,
            right: true
        }
        var calcCanNav = function(){
            $scope.canNav.left = mainNav.topPage.pushedOptions.locationIndex > 0;
            $scope.canNav.right = mainNav.topPage.pushedOptions.locationIndex < $scope.design.Location.length-1;
        }
        $scope.navigateLocation = function(step){
            mainNav.topPage.pushedOptions.locationIndex += step;
            $scope.location = $scope.design.Location[mainNav.topPage.pushedOptions.locationIndex];
            init();
        }

        var init = function(){
            $scope.location = $scope.design.Location[mainNav.topPage.pushedOptions.locationIndex];
            calcCanNav();
            var now = new Date().getTime();
            //if(!$scope.location.LocationDetail || ($scope.location.lastDetailFetch-now > 1000*60*10)){
                fetchDetails();
            //}else if($scope.location.needsSort){
            //    sortDetails();
            //}
        };

        $timeout(init);

        $scope.$on('$destroy', function(){
            if(dialogs.info.visible){
                dialogs.info.hide();
            }
            if(dialogs.addDetail.visible){
                dialogs.addDetail.hide();
            }
            if(dialogs.addAssembly.visible){
                dialogs.addAssembly.hide();
            }
            CommentDialog.hide();
            Smartpick.hide();
            $rootScope.$emit('LocationDetailDialog', {type:'destroy'});
        });

        ons.createAlertDialog('app/te/te-duration-dialog.html').then(function(dialog){
            dialog.openDialog = function(done, duration, options){
              if(duration){
                dialog._scope.duration = new Number(duration);
              }else{
                dialog._scope.duration = '';
              }
              dialog._scope.options = options||{};
              dialog._scope.loading = false;
              dialog._scope.done = function(ok, newDuration){
                if(done(ok, newDuration, dialog))
                  dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            dialog._scope.roundHours = function(){
            }
            dialogs.duration = dialog;
        });

        ons.createDialog('app/dialog/location-detail-info-dialog.html').then(function(dialog){
            dialogs.info = dialog;
        });
        ons.createAlertDialog('app/completework/create-location-detail-dialog.html').then(function(dialog){
            dialogs.addDetail = dialog;
            dialog._scope.form = {};
            dialog._scope.hideComplete = true;
            dialog._scope.requireComment = false;
            dialog.openDialog = function(){
                dialog._scope.selects = {action:{}};
                dialog._scope.form = {};
                dialog.defaultSelection();
                Utils.openDialog(dialog);
            }
            dialog.setData = function(data){
                dialog._scope.data = data;
                dialog.defaultSelection();
            }
            dialog.defaultSelection = function(){
                dialog._scope.form.qty = 1;
                dialog._scope.form.comments = '';
                if(dialog._scope.data){
                    var action = dialog._scope.data.ReferenceAction.find(function(a){
                        return a.WorkAction == 'Install';
                    });
                    if(action){
                        dialog._scope.selects.action = action;
                    }
                }
            }
            dialog._scope.tagHandler = function(tag){
                return null;
            };
            dialog._scope.selectAction = function($item, $model){
                delete dialog._scope.selects.spec;
            };
            dialog._scope.searchSpec = function(){
                if(dialog._scope.selects.spec && Smartpick.ok()){
                    Smartpick.show();
                }else{
                    Smartpick.show({
                        endpoint: 'designSpecActions',
                        endpointData: {
                            actionID: dialog._scope.selects.action.ActionID,
                            job: $scope.job.JobID,
                            owner:$scope.job.OwnerID,
                            assemblies:$scope.location.UseAssembliesFlag
                        },
                        itemView: 'SpecificationAction',
                        itemTemplate: 'completion-pick-spec.html',
                        title: 'Search Specs',
                        extend: {
                            openDocument: function(injector){
                                return function(item, $event){
                                    $event.stopPropagation();
                                    if(item.opening)return; 
                                    item.opening = true;
                                    injector.get('ImgOrPdf').load('spec/'+item.SpecificationSpecDocumentID).finally(function(){item.opening = false;});
                                }
                            },
                            filterLimitThreshold: function(){return 75;}
                        },
                        filters: [
                            {key: 'SpecBook', label:'Spec Book', from: 'ID', to:'SpecificationBookID', display:'Name'},
                            {key: 'Section', from: 'ID', to:'SpecificationSectionID', display:'Name', dependencies:{'SpecBook':{from:'SpecBookID',to:'ID'}}},
                            {key: 'SubSection', label:'Sub Section', from: 'ID', to:'SpecificationSubSectionID', display:'Name', dependencies:{'Section':{from:'SectionID',to:'ID'}}}
                        ],
                        minSearchLength: $scope.design.OwnerMinimumSpecSearchLength||3
                    }, function(item){
                        dialog._scope.selects.spec = item;
                    });
                }
            };
            
            dialog._scope.selects = {action:{}};
            dialog._scope.done = function(add, specAction, qty, complete, comments){
                if(add && specAction){
                    addSpecAction(specAction, qty, complete, comments);
                }
                dialog.hide();
            };
        });
        ons.createAlertDialog('app/completework/create-ld-assembly-dialog.html').then(function(dialog){
            dialogs.addAssembly = dialog;
            dialog._scope.form = {};
            dialog._scope.hideComplete = true;
            dialog.openDialog = function(){
                dialog._scope.selects = {action:{}};
                dialog._scope.form = {};
                dialog._scope.step = 1;
                dialog.defaultSelection();
                Utils.openDialog(dialog);
            }
            dialog.setData = function(data){
                dialog._scope.data = data;
                dialog.defaultSelection();
            }
            dialog.defaultSelection = function(){
                dialog._scope.form.qty = 1;
                if(dialog._scope.data){
                    var action = dialog._scope.data.ReferenceAction.find(function(a){
                        return a.WorkAction == 'Install';
                    });
                    if(action){
                        dialog._scope.selects.action = action;
                    }
                }
            }
            dialog._scope.tagHandler = function(tag){
                return null;
            };
            dialog._scope.selectAction = function($item, $model){
                //delete dialog._scope.selects.spec;
            };
            dialog._scope.searchSpec = function(){
                if(dialog._scope.selects.spec && Smartpick.ok()){
                    Smartpick.show();
                }else{
                    Smartpick.show({
                        endpoint: 'designSpecActions',
                        endpointData: {
                            actionID: dialog._scope.selects.action.ActionID,
                            job: $scope.job.JobID,
                            owner:$scope.job.OwnerID,
                            assemblies:$scope.location.UseAssembliesFlag
                        },
                        itemView: 'SpecificationAction',
                        itemTemplate: 'completion-pick-spec.html',
                        title: 'Search Assemblies',
                        extend: {
                            openDocument: function(injector){
                                return function(item, $event){
                                    $event.stopPropagation();
                                    if(item.opening)return; 
                                    item.opening = true;
                                    injector.get('ImgOrPdf').load('spec/'+item.SpecificationSpecDocumentID).finally(function(){item.opening = false;});
                                }
                            },
                            filterLimitThreshold: function(){return 75;}
                        },
                        filters: [
                            {key: 'SpecBook', label:'Spec Book', from: 'ID', to:'SpecificationBookID', display:'Name'},
                            {key: 'Section', from: 'ID', to:'SpecificationSectionID', display:'Name', dependencies:{'SpecBook':{from:'SpecBookID',to:'ID'}}},
                            {key: 'SubSection', label:'Sub Section', from: 'ID', to:'SpecificationSubSectionID', display:'Name', dependencies:{'Section':{from:'SectionID',to:'ID'}}}
                        ]
                    }, function(item){
                        dialog._scope.selects.spec = item;
                        dialog._scope.form.assemblyNumber = item.SpecificationSpecificationNumber;
                        dialog._scope.form.assemblyDesc = item.SpecificationDescription;
                    });
                }
            };
            dialog._scope.selects = {action:{}};
            dialog._scope.form = {};
            dialog._scope.goStep = function(stp){
                dialog._scope.step = stp;
            };
            dialog._scope.done = function(add, specAction, form, action, complete){
                if(add){
                    addSpecAction(specAction, form.qty, complete, form.comments, form.assemblyNumber, form.assemblyDesc, form.wireSpan, action);
                }
                dialog.hide();
            };
        });
        
    }]);

})();
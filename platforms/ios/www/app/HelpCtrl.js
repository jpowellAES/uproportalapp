(function(){
    'use strict';

    angular.module('app').controller('HelpController', ['$scope', function($scope) {

        var backButton = function(){
            var frame = document.getElementById('helpFrame');
            if(frame.contentWindow.history.length > 1){
                frame.contentWindow.history.back();
            }
        }

        $scope.hidePage = function(){
            ons.enableDeviceBackButtonHandler();
            document.removeEventListener('backbutton', backButton);
        }
        $scope.showPage = function(){
            ons.disableDeviceBackButtonHandler();
            document.addEventListener('backbutton', backButton, false);
        }

    }]);

})();
(function(){
    'use strict';

    angular.module('app').controller('DailyReportJobController', ['$scope', '$timeout', 'ApiService', 'Utils', 'UploadDialog', 'ImgOrPdf', function($scope, $timeout, ApiService, Utils, UploadDialog, ImgOrPdf) {
        $scope.working = true;
        $scope.job = $scope.$parent.current.job;
        $scope.loading = false;
        $scope.data = {
            expanded: {
                photos: true
            },
            changed: false
        }
        
        var loadJob = function(date, save){
            $scope.working = true;
            var obj = {
                route: 'job',
                id: $scope.job.ID
            };
            if(date){
                obj.date = Utils.date(date,'MM/dd/yyyy');
            }
            if(save){
                _getPostData(obj, function(obj, formdata){
                    ApiService.post('daily', obj, '', '', formdata).then(_saveResult,_saveResult);
                });
            }else{
                ApiService.get('daily', obj).then(result, result);
            }
        }
        var init = function(){
            loadJob();
        }

        var result = function(resp){
            if(resp && resp.OrgRelationship){
                $scope.data.form = cleanJob(resp.OrgRelationship.ATCReportDay.ATCJobDay);
                $scope.data.date = Utils.date(resp.OrgRelationship.Date);
                $scope.data.backupDate = Utils.date(resp.OrgRelationship.Date);
            }
            $scope.working = false;
            $scope.loading = false;
        }
        var cleanJob = function(job){
            job.ATCPhoto = Utils.enforceArray(job.ATCPhoto);
            return job;
        }

        $scope.toggle = function(which){
            $scope.data.expanded[which] = !$scope.data.expanded[which];
        }

        $scope.addPhoto = function(existingIndex){
            $scope.data.expanded.photos = true;
            var existing = typeof existingIndex != 'undefined';
            var options = {save: function(data){
                    var newObj = {
                        thumb: data.photoUrl,
                        Description: data.description
                    }
                    if(existing){
                        $scope.data.form.ATCPhoto[existingIndex] = newObj
                    }else{
                        $scope.data.form.ATCPhoto.push(newObj)
                    }
                    $scope.data.changed = true;
                    UploadDialog.hide();
                    $scope.save();
                },
                title: 'Add Photo',
                saveLabel: 'Ok'
            }
            if(existing){
                options.photoUrl = $scope.data.form.ATCPhoto[existingIndex].thumb
                options.description = $scope.data.form.ATCPhoto[existingIndex].Description
            }
            UploadDialog.show(options);
        }

        $scope.removePhoto = function(index){
            $scope.data.form.ATCPhoto.splice(index, 1);
            $scope.data.changed = true;
        }
        $scope.openPhoto = function(index){
            if($scope.data.form.ATCPhoto[index].thumb){
                $scope.addPhoto(index)
                return;
            }
            var path =  '/upload/' + $scope.data.form.ATCPhoto[index].String;
            $scope.loading = true;
            ImgOrPdf.loadWithCaption(path, {Key:$scope.data.form.ATCPhoto[index].String,Description:$scope.data.form.ATCPhoto[index].Description} , {editable: true, callback: function(upl){
                $scope.data.form.ATCPhoto[index].Description = upl.Description;
            }}).finally(function(){
                $scope.loading = false;
            });
        }

        var newDate = function(date, save){
            $scope.data.date = date;
            loadJob($scope.data.date, save);
        }
        $scope.navDate = function(dir){
            if(!$scope.data.date){
                $scope.data.date = new Date($scope.data.backupDate.getTime());
            }
            if($scope.data.changed){
                Utils.notification.confirm("Save changes?", {
                    callback: function(ok){
                        if(ok == 2){
                            return;
                        }
                        newDate($scope.data.date.addDays(dir),ok==0);
                    },
                    buttonLabels: ["Save","Discard","Cancel"]
                });
                return;
            }
            newDate($scope.data.date.addDays(dir));
        }
        $scope.changeDate = function(){
            if(!$scope.data.date){
                $scope.data.date = new Date($scope.data.backupDate.getTime());
                return;
            }
            if($scope.data.changed){
                Utils.notification.confirm("Save changes?", {
                    callback: function(ok){
                        if(ok == 2){
                            return;
                        }
                        newDate(Utils.date($scope.data.date),ok==0);
                    },
                    buttonLabels: ["Save","Discard","Cancel"]
                });
                return;
            }
            newDate(Utils.date($scope.data.date));
        }

        $scope.save = function(){
            $scope.loading = true;
            _getPostData({}, function(obj, formdata){
                ApiService.post('daily', obj, '', '', formdata).then(_saveResult,_saveResult);
            });
        }

        var _saveResult = function(resp){
            $scope.data.changed = false;
            result(resp);
            if(closing){
                close();
            }
        }

        var _getPostData = function(obj, done){
            obj = obj||{};
            obj.saveDate = Utils.date($scope.data.backupDate, 'MM/dd/yyyy');
            if(!obj.date) obj.date = obj.saveDate;
            if(!obj.id) obj.id = $scope.job.ID;
            obj.route = 'save';
            obj.ATCJobDay = Utils.pick($scope.data.form,'JobID','ExplanationOTDT','HoursOTDT','PersistOTDT','ApproverOTDT','WorkInProgress','PersistWorkInProgress','LookAhead',
            'PersistLookAhead','CompletedWorkPreOutage','CompletedWorkOutage','PersistCompletedWork','Problems','PersistProblems','Solutions','PersistSolutions','MaterialsReceived');
            obj.ATCJobDay.ATCScope = [];
            obj.ATCJobDay.ATCMemo = [];
            obj.ATCJobDay.ATCOutage = [];
            obj.ATCJobDay.ATCSubcontractor = [];
            obj.ATCJobDay.ATCEquip = [];
            obj.ATCJobDay.ATCUnionFTE = [];
            obj.ATCJobDay.ATCPhoto = [];
            
            
            var files = {}, count = 0
            for(var i=0; i<$scope.data.form.ATCPhoto.length; ++i){
                if(!$scope.data.form.ATCPhoto[i].UploadID){
                    files['photo'+count] = $scope.data.form.ATCPhoto[i].thumb;
                    obj.ATCJobDay.ATCPhoto[i] = {Description: $scope.data.form.ATCPhoto[i].Description, Name: $scope.data.form.ATCPhoto[i].thumb.split('/').pop().split('?').shift()};
                    ++count;
                }else{
                    obj.ATCJobDay.ATCPhoto[i] = {UploadID:$scope.data.form.ATCPhoto[i].UploadID}
                }
            }
            if(count){
                ApiService.getFormData(obj, files, {base64:true}).then(function(formdata){
                    done(obj, formdata);
                });
            }else{
                done(obj);
            }
        }

        var closing = false;
        var close = function(save){
            if(save){
                $scope.save();
                return;
            }
            mainNav.popPage();
        }
        $scope.close = function(){
            if(closing || (mainNav.pages[mainNav.pages.length-1].pushedOptions.page != 'app/dailyreport/job.html')) return;
            if(!$scope.data.changed){
                close();
                return;
            }
            closing = true;
            Utils.notification.confirm("Save changes?", {
              callback: function(ok){
                if(ok == 2){
                  closing = false;
                  return;
                }
                close(ok==0);
              },
              buttonLabels: ["Save","Discard","Cancel"]
          });
        }

        var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', $scope.close, false);
            }else{
                document.removeEventListener('backbutton', $scope.close);
                ons.enableDeviceBackButtonHandler();
            }
          }

          $scope.hidePage = function(){
            toggleBackButton(false)
            Utils.clearBackButtonHandlers()
          }
          $scope.showPage = function(){
            toggleBackButton(true)
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })

            ons.findComponent('#dailyreport-back-button')._element[0].onClick = $scope.close;
          }

        $scope.$on('$destroy', function(){
            UploadDialog.hide();
        });

        $timeout(init);
    }]);

    angular.module('app').controller('DailyReportController', ['$scope', '$timeout', 'ApiService', 'StorageService', 'Utils', '$anchorScroll', function($scope, $timeout, ApiService, StorageService, Utils, $anchorScroll) {
        var staticData = {
            'PE': {title:'Pre-Active', icon:'ion-hammer', sort:2},
            'PS': {title:'Pending Start', icon:'ion-hammer', sort:2},
            'PR': {title:'Active', icon:'ion-hammer', sort:1},
            'FC': {title:'Field Complete', icon:'ion-checkmark', sort:4},
            'OnHold': {title: 'On Hold', icon: '', sort:5}
        }
        var dialogs = {};
        staticData.DE = staticData.PE;
        $scope.loaded = false;
        $scope.openingJob = false;
        $scope.working = true;
        $scope.data = {
            tabs: []
        };
        $scope.styles = {
            tabbar: 'display: block; overflow-x: auto !important; overflow-y: hidden !important;',
            tab: {'display': 'inline-block','box-sizing': 'border-box'},
            tabInner: 'padding: 0px 10px;'
        };
        $scope.tabIndex = 0;
        $scope.hasJobs = false;

        $scope.loadJobs = function($done){
            var obj = {
                route: 'jobs'
            };
            ApiService.get('daily', obj).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var pickOutOnHoldJobs = function(jobs){
            var tab = $scope.data.tabs.find(function(t){return t.key == 'OnHold'});
            for(var i=jobs.length-1; i>=0; i--){
                if(jobs[i].OnHoldFlag){
                    if(!tab){
                        tab = addTab('OnHold', staticData['OnHold'].title, staticData['OnHold'].icon, false);
                    }
                    tab.jobs.push(jobs.splice(i,1)[0]);
                }
            }
        }

        var cleanJobs = function(jobs){
            if(!Array.isArray(jobs))
                jobs = jobs?[jobs]:[];
            $scope.hasJobs = $scope.hasJobs||jobs.length > 0;
            pickOutOnHoldJobs(jobs);
            return jobs;
        }

        var result = function(resp){
            if(resp && resp.Job){
                $scope.data.date = Utils.date(resp.date);
                var jobs = cleanJobs(resp.Job);
                $scope.data.jobs = jobs;
                var tabsDone = {};
                for(var j=0; j<jobs.length; ++j){
                    var tab = tabsDone[jobs[j].StatusCode];
                    if(!tab){
                        tab = addTab(jobs[j].StatusCode, staticData[jobs[j].StatusCode].title, staticData[jobs[j].StatusCode].icon, false);
                        tabsDone[jobs[j].StatusCode] = tab;
                    }
                    if(!tab.jobs.find(function(jb){ return jb.JobID == jobs[j].ID})){
                        tab.jobs.push(jobs[j]);
                    }
                }
            }else{
                $scope.hasJobs = false;
            }
            $scope.working = false;
        }

        var checkOverflowX = function(el){
           var curOverflow = el.style.overflowX;
           if ( !curOverflow || curOverflow === "visible" )
              el.style.overflowX = "hidden";
           var isOverflowing = el.clientWidth < el.scrollWidth;
           el.style.overflowX = curOverflow;
           return isOverflowing;
        };

        var checkTabbarWidth = function(){
            var tb = dailyReportTabbar._element[0];
            var inner = tb.getElementsByClassName('tab-bar');
            if(inner.length){
                var tabsInner = inner[0].getElementsByClassName('tab-bar__label');
                for(var i=0; i<tabsInner.length; i++){
                    tabsInner[i].setAttribute('style', $scope.styles.tabInner);
                }
                inner[0].setAttribute('style', $scope.styles.tabbar);
                if(!checkOverflowX(inner[0])){
                    inner[0].setAttribute('style', '');
                    for(var i=0; i<tabsInner.length; i++){
                        tabsInner[i].setAttribute('style', '');
                    }
                    $scope.styles.tab = {};
                }
            }
        };

        var addTab = function(key, title, icon, active){
            var tab;
            if((tab=$scope.data.tabs.find(function(t){ return t.sort==staticData[key].sort}))){
                return tab;
            }
            tab = {key: key, title: title, icon: icon, active: active, jobs:[], sort: staticData[key].sort};
            $scope.data.tabs.push(tab);
            $scope.data.tabs.sort(function(a,b){ return a.sort-b.sort })
            return tab;
        }

        var getPrompt = function(){
            if($scope.OrgType == 'OW')
                return 'Job #';
            return 'WG Job #';
        }
        $scope.getOrg = function(job){
            if($scope.OrgType == 'OW')
                return job.Workgroup;
            return job.Owner;
        };
        var getJobNumber = function(number){
            return number.replace(/ - Owner Job#: .+$/i, '');
        };
        $scope.getJobNumber = function(job){
            if($scope.OrgType == 'OW')
                return job.JobNumber;
            return getJobNumber(job.WorkgroupJobNumber);
        }
        $scope.getOtherJobNumber = function(job){
            var wg = getJobNumber(job.WorkgroupJobNumber);
            if(wg == job.JobNumber)
                return '';
            if($scope.OrgType == 'OW')
                return wg;
            return job.NumberChar;
            
        };

        $scope.openJob = function(job, tab){
            if($scope.openingJob)
                return;
            $scope.current.job = job;
            $scope.current.tab = $scope.data.tabs[dailyReportTabbar.getActiveTabIndex()];
            mainNav.pushPage('app/dailyreport/job.html');
        }

        $scope.jobOnLongPress = function(job, tab){
            if($scope.openingJob)
                return;
            navigator.vibrate(10);
            //
        };

        var findJobTab = function(jobID){
            for(var i=0; i<$scope.data.tabs.length; ++i){
                for(var j=0; j<$scope.data.tabs[i].jobs.length; ++j){
                    if($scope.data.tabs[i].jobs[j].ID == jobID){
                        return i;
                    }
                }
            }
            return -1;
        }
        var navigateToJob = function(job){
            var jobID = 'anchor'+job.ID;
            var tabIndex = findJobTab(job.ID);
            if(tabIndex == -1) return;
            if(dailyReportTabbar.getActiveTabIndex() != tabIndex){
                dailyReportTabbar.on('postchange', function(e){                            
                    $timeout(function() {
                        $anchorScroll(jobID);
                        Utils.flash(jobID)
                    });
                    dailyReportTabbar.off('postchange');
                })
                dailyReportTabbar.setActiveTab(tabIndex);
                return;
            }
            $timeout(function() {
                $anchorScroll(jobID);
                Utils.flash(jobID)
            });
        }
        $scope.findJob = function(){
            dialogs.findJob.open();
        }

        var init = function(){
            $scope.loaded = true;
        };

        var saveStateFields = ['data','working', 'tabIndex', 'hasJobs'];
        $scope = StorageService.state.load('DailyReportController', $scope, saveStateFields);
        if(!$scope.hasJobs)
            $scope.loadJobs();
        $timeout(init);
        
        ons.orientation.on('change', function(){
            $timeout(checkTabbarWidth,0);
        });

        $scope.$on('$destroy', function(){
            StorageService.state.clear('DailyReportController');
            ons.orientation.off('change');
            dialogs.findJob.hide();
        });

        //Find location
        ons.createAlertDialog('app/dailyreport/find-job-dialog.html').then(function(dialog){
            dialogs.findJob = dialog;
            dialog._scope.data = {}
            dialog.open = function(){
                dialog._scope.data.term = ''
                Utils.openDialog(dialog)
            }
            dialog._scope.search = function(){
                if(!dialog._scope.data.term.trim()) return;
                var index = Utils.findArrayBestMatch(dialog._scope.data.term.trim(), $scope.data.jobs, ['NumberChar','WorkgroupJobNumber','Description'])
                if(index > -1){
                    navigateToJob($scope.data.jobs[index])
                    dialog.hide()
                }else{
                    Utils.notification.toast('Could not find job', {timeout:2000})
                }
            }
            dialog._scope.close = function(){
                dialog.hide()
            }
        });
    }]);

})();
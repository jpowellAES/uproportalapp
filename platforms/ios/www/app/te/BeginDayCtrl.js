(function(){
    'use strict';

    angular.module('app').controller('BeginDayController', ['$scope', 'ApiService', 'Utils', '$rootScope', '$timeout', '$filter', 'Rentals', function($scope, ApiService, Utils, $rootScope, $timeout, $filter, Rentals) {
        $scope.job = $scope.$parent.current.job;
        $scope.jobday = $scope.$parent.current.jobday;
        $scope.jobdaycrew = {};
        $scope.module = $scope.$parent.current.module;
        $scope.working = false;
        $scope.loading = false;
        var data = {};
       

      var getDayCrewObj = function(){
        return {job: $scope.job.JobID, crew: $scope.jobday.CrewID, date: $filter('date')($scope.jobday.Date, 'M/d/yyyy')};
      }
      var openBeginDay = function(){
        $scope.working = true;
        var obj = getDayCrewObj();
        ApiService.post('teBeginDay', obj).then(openBeginDayResult,openBeginDayResult);
      };

      var openBeginDayResult = function(resp, skipCrew){
        $scope.working = false;
        if(resp && resp.Job && resp.Job.JobDay){
          $scope.job.ContractDepartmentCode = resp.Job.ContractDepartmentCode;
          $scope.job.BypassCompleteWorkFlag = resp.Job.BypassCompleteWorkFlag;
          $scope.jobday.Status = resp.Job.JobDay.JobDayCrew.StatusMeaning;
          $scope.jobday.StatusCode = resp.Job.JobDay.JobDayCrew.StatusCode;
          if(!skipCrew)
            $scope.jobdaycrew = cleanJobDayCrew(resp.Job.JobDay.JobDayCrew);
        }
      };

      var cleanJobDayCrew = function(jdc){
        if(!Array.isArray(jdc.JobDayCrewMember))
          jdc.JobDayCrewMember = jdc.JobDayCrewMember?[jdc.JobDayCrewMember]:[];
        if(!Array.isArray(jdc.JobDayCrewEquipment))
          jdc.JobDayCrewEquipment = jdc.JobDayCrewEquipment?[jdc.JobDayCrewEquipment]:[];

        return jdc;
      };

      $scope.addItem = function(which, $event){
        switch(which){
          case 'member':
            addMember();
          break;
          case 'equipment':
            addEquipment();
          break;
          case 'rental':
            Rentals.show({id: $scope.jobday.CrewID, which: 'JobDayCrew', orgId: $scope.jobdaycrew.ForemanOrganizationID}, function(equip){
              if(equip && equip.ID){
                addEquipmentExecute(equip);
              }
            });
          break;
        }
        ons.findParentComponentUntil('ons-speed-dial',$event.target).toggleItems();
      };

      var save = function(callback){
        $scope.loading = true;
        ApiService.post('teBeginDayFinish', getDayCrewObj()).then(function(resp){
          openBeginDayResult(resp, true);
          callback();
        });
      }

      var loadCompletionJob = function(){
          $scope.loading = true;
          var obj = {design: $scope.current.job.ActiveJobDesignID};
          ApiService.get('jobCompletion', obj).then(completionJobResult, completionJobResult)
      }
      var completionJobResult = function(resp){
          $scope.loading = false;
          if(resp && resp.JobDesign){
              $scope.current.job.design = cleanCompletionResponse(resp.JobDesign);
              $scope.current.job.design.lastLocationFetch = new Date().getTime();
              $scope.current.job.design.process = {name:'EndDay', date:$scope.current.jobday.Date, crew: $scope.jobdaycrew.ID};
              mainNav.replacePage('complete-work-job.html');
          }else{
              if(resp && resp.locked && resp.message){
                  ons.notification.confirm(resp.message+" Go directly to End of Day?", {
                      title: "Job Locked",
                      buttonLabels: ["No","Yes"],
                      callback: function(buttonIndex){
                          if(buttonIndex == 1){
                              mainNav.replacePage('te-end-day.html');
                          }
                      }
                  });
              }else{
                  ons.notification.alert("Failed to load job data", {title: "Error"});
              }
          }
      };
      var cleanCompletionResponse = function(design){
          design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
          if(!design.Location)
              design.Location = [];
          if(!Array.isArray(design.Location))
              design.Location = [design.Location];
          design.Location.map(cleanCompletionLocation);
          design.PercentComplete = Utils.fixPercent(design.PercentComplete);
          return design;
      };
      var cleanCompletionLocation = function(location){
          location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
          location.PercentComplete = Utils.fixPercent(location.PercentComplete);
          return location;
      };
      $scope.completeWork = function(){
        save(function(){
          if($scope.jobday.UseDesignFlag && $scope.jobday.StatusCode == 'EPE' && !$scope.job.BypassCompleteWorkFlag){
              loadCompletionJob();
          }else if(['PE', 'BIP', 'RV', 'AP'].indexOf($scope.jobday.StatusCode) == -1){
              $scope.loading = false;
              mainNav.replacePage('te-end-day.html');
          }
        });
      }

      $scope.finish = function(){
        save(function(){
          $scope.loading = false;
          mainNav.popPage();
        });
      }

      var activateEquipmentExecute = function(equipment, active, reason, permanent){
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = 'activate';
        obj.active = active;
        obj.permanent = permanent;
        obj.equipment = equipment.EquipmentID;
        if(reason)
          obj.reason = reason.SmartCode;
        ApiService.post('teBeginEquip', obj)
        .then(activateEquipmentResult,activateEquipmentResult)
      };

      var activateEquipmentResult = function(resp){
        if(resp && resp.JobDayCrewEquipment){
          var equipment = $scope.jobdaycrew.JobDayCrewEquipment.find(function(e){
            return e.EquipmentID == resp.JobDayCrewEquipment.EquipmentID;
          });
          Object.assign(equipment,resp.JobDayCrewEquipment);
        }
        $scope.loading = false;
      };

      var cleanEquipmentSmartpicks = function(sp){
        if(!Array.isArray(sp.SmartCodes))
          sp.SmartCodes = [sp.SmartCodes];
        if(!Array.isArray(sp.Equipment))
          sp.Equipment = [sp.Equipment];
        return sp;
      }

      var getEquipmentData = function(){
        if(!data.equipment){
          ApiService.get('teBeginEquip', {
            action:'smartpicks', 
            organization: $scope.jobdaycrew.ForemanOrganizationID
          })
          .then(function(resp){
              if(resp && resp.SmartCodes){
                data.equipment = cleanEquipmentSmartpicks(resp);
                $scope.activateEquipmentDialog._scope.data = data.equipment;
                $scope.addEquipmentDialog._scope.data = data.equipment;
              }
          });
        }
      }

      $scope.activateEquipment = function(equipment, $event){
        $event.preventDefault();
        $event.stopPropagation();
        $scope.activateEquipmentDialog._scope.equipActive = equipment.ActiveFlag;
        $scope.activateEquipmentDialog._scope.originalActive = equipment.ActiveFlag;
        $scope.activateEquipmentDialog._scope.equipPermanent = {value: false};
        $scope.activateEquipmentDialog._scope.equipment = equipment;
        $scope.activateEquipmentDialog._scope.selects = {};
        if(equipment.InactiveReasonCode)
          $scope.activateEquipmentDialog._scope.selects.inactiveReason = {SmartCode: equipment.InactiveReasonCode, Meaning: equipment.InactiveReasonMeaning};

        getEquipmentData();
        $scope.activateEquipmentDialog.show();
        return false;
      }

      var addEquipment = function(){
        $scope.addEquipmentDialog._scope.permanent = false;
        $scope.addEquipmentDialog._scope.selects = {};
        getEquipmentData();
        $scope.addEquipmentDialog.show();
      };

      var addEquipmentExecute = function(equipment, permanent){
        if($scope.jobdaycrew.JobDayCrewEquipment.find(function(e){
          return e.EquipmentID == equipment.ID;
        })){
          return;
        }
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = 'add';
        obj.equipment = equipment.ID;
        if(permanent)
          obj.permanent = true;
        ApiService.post('teBeginEquip', obj)
        .then(addEquipmentResult,addEquipmentResult)
      };

      var addEquipmentResult = function(resp){
        if(resp && resp.JobDayCrewEquipment){
          $scope.jobdaycrew.JobDayCrewEquipment.push(resp.JobDayCrewEquipment);
          beginDayTabs.setActiveTab(1);
        }
        $scope.loading = false;
      };

      var sortMembers = function(){
        $scope.jobdaycrew.JobDayCrewMember.sort(Utils.multiSort('CraftSortOrder','ClassSortOrder','PersonFullName'));
        for(var i=0; i<$scope.jobdaycrew.JobDayCrewMember.length; i++){
          $scope.jobdaycrew.JobDayCrewMember[i].Sort = null;
        }
        var sort = 0, active = true;
        for(var i=0; i<$scope.jobdaycrew.JobDayCrewMember.length; i++){
          var member = $scope.jobdaycrew.JobDayCrewMember[i], next;
          if(member.ActiveFlag==active && !member.IsForeman && member.Sort == null){
            member.Sort = ++sort;
            while((next = $scope.jobdaycrew.JobDayCrewMember.find(function(m){
              return (m.ActiveFlag==active && !m.IsForeman && m.Sort == null && m.PersonID==member.PersonID)
            }))){
              next.Sort = ++sort;
            }
          }
          if(i == $scope.jobdaycrew.JobDayCrewMember.length-1 && active){
            active = false;
            i = -1;
          }
        }
        $scope.jobdaycrew.JobDayCrewMember.sort(Utils.multiSort({name:'ActiveFlag',reverse:true},{name:'IsForeman',reverse:true},'Sort'));
      }

      var activateMemberExecute = function(member, craft, clss, jobStatus, reason, permanent){
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = 'activate';
        obj.jobstatus = jobStatus.ID;
        obj.craft = craft.ID;
        obj.member = member.ID;
        obj.class = clss.ID;
        if(reason)
          obj.reason = reason.Code;
        if(permanent)
          obj.permanent = true;
        ApiService.post('teBeginMember', obj)
        .then(activateMemberResult,activateMemberResult)
      };

      var activateMemberResult = function(resp){
        if(resp && resp.JobDayCrewMember){
          var member = $scope.jobdaycrew.JobDayCrewMember.find(function(m){
            return m.ID == resp.JobDayCrewMember.ID;
          });
          if(member){
            Object.assign(member,resp.JobDayCrewMember);
            sortMembers();
          }
        }
        $scope.loading = false;
      };

      var getMemberData = function(){
        if(!data.member){
          ApiService.get('teBeginMember', {
            action:'smartpicks', 
            organization: $scope.jobdaycrew.ForemanOrganizationID,
            department: $scope.job.ContractDepartmentCode
          })
          .then(function(resp){
              if(resp && resp.Smartpicks){
                data.member = cleanMemberSmartpicks(resp.Smartpicks);
                $scope.activateMemberDialog._scope.data = data.member;
                $scope.addMemberDialog._scope.data = data.member;
              }
          });
        }
      }

      $scope.openMember = function(member, $event){
        $event.stopPropagation();
        $scope.activateMemberDialog.openDialog(member);
        return false;
      }

      var cleanMemberSmartpicks = function(sp){
        if(!Array.isArray(sp.Craft))
          sp.Craft = [sp.Craft];
        if(!Array.isArray(sp.Class))
          sp.Class = [sp.Class];
        if(!Array.isArray(sp.JobStatus))
          sp.JobStatus = [sp.JobStatus];
        if(!Array.isArray(sp.InactiveReason))
          sp.InactiveReason = [sp.InactiveReason];
        if(!Array.isArray(sp.Person))
          sp.Person = [sp.Person];
        return sp;
      }

      var addMember = function(){
        $scope.addMemberDialog._scope.permanent = false;
        $scope.addMemberDialog._scope.selects = {};
        getMemberData();
        $scope.addMemberDialog.show();
      };

      var addMemberExecute = function(person, craft, clss, permanent){
        $scope.loading = true;
        var obj = getDayCrewObj();
        obj.action = 'add';
        obj.person = person.ID;
        obj.craft = craft.ID;
        obj.class = clss.ID;
        if(permanent)
          obj.permanent = true;
        ApiService.post('teBeginMember', obj)
        .then(addMemberResult,addMemberResult)
      };

      var addMemberResult = function(resp){
        if(resp && resp.JobDayCrewMember){
          $scope.jobdaycrew.JobDayCrewMember.push(resp.JobDayCrewMember);
          sortMembers();
          beginDayTabs.setActiveTab(0);
        }
        $scope.loading = false;
      };

      $scope.onHide = function(){
        Rentals.reset();
      }

      $scope.$on('$destroy', function(){
        if($scope.activateEquipmentDialog.visible){
            $scope.activateEquipmentDialog.hide();
        }
        if($scope.addEquipmentDialog.visible){
            $scope.addEquipmentDialog.hide();
        }
        if($scope.activateMemberDialog.visible){
            $scope.activateMemberDialog.hide();
        }
        if($scope.addMemberDialog.visible){
            $scope.addMemberDialog.hide();
        }
        Rentals.hide();
      });

      ons.createAlertDialog('activate-equipment-dialog.html').then(function(dialog){
          $scope.activateEquipmentDialog = dialog;
          $scope.activateEquipmentDialog._scope.equipPermanent = {value: false};
          $scope.activateEquipmentDialog._scope.select = {inactiveReason:{}};
          $scope.activateEquipmentDialog._scope.tagHandler = function(tag){
              return null;
          };
          $scope.activateEquipmentDialog._scope.filterReason = function(prop, search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  return item[prop].match(regex);
              };
          };
          $scope.activateEquipmentDialog._scope.activate = function(){
            if($scope.activateEquipmentDialog._scope.equipActive){
              $scope.activateEquipmentDialog._scope.selects = {};
            }else{
              $scope.activateEquipmentDialog._scope.selects.inactiveReason = $scope.activateEquipmentDialog._scope.data.SmartCodes.find(function(sc){return sc.SmartCode == 'ID';});
            }
          }
          $scope.activateEquipmentDialog._scope.done = function(ok, active, reason, permanent){
              if(ok){
                  activateEquipmentExecute($scope.activateEquipmentDialog._scope.equipment, active, reason, permanent);
              }
              $scope.activateEquipmentDialog.hide();
          };
      });

      ons.createAlertDialog('te-add-equipment-dialog.html').then(function(dialog){
          $scope.addEquipmentDialog = dialog;
          $scope.addEquipmentDialog._scope.selects = {equipment:{}};
          $scope.addEquipmentDialog._scope.tagHandler = function(tag){
              return null;
          };
          $scope.addEquipmentDialog._scope.filterSearch = function(search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  return (!dialog._scope.selects.category||item.CategoryID==dialog._scope.selects.category.ID) && $scope.jobdaycrew.JobDayCrewEquipment.findIndex(function(e){return e.EquipmentID==item.ID})<0 && (item.Number.match(regex)||item.Description.match(regex));
              };
          };
          $scope.addEquipmentDialog._scope.selectCategory = function(){
            delete dialog._scope.selects.equipment;
          }
          $scope.addEquipmentDialog._scope.selectEquip = function(equip){
            var category = dialog._scope.data.Category.find(function(category){return category.ID == equip.CategoryID});
            if(category){
              dialog._scope.selects.category = category;
            }
          }
          $scope.addEquipmentDialog._scope.done = function(ok, equipment, permanent){
              if(ok){
                  addEquipmentExecute(equipment, permanent);
              }
              $scope.addEquipmentDialog.hide();
          };
      });

      ons.createAlertDialog('activate-member-dialog.html').then(function(dialog){
          $scope.activateMemberDialog = dialog;
          $scope.activateMemberDialog._scope.permanentLabel = 'Permanent';
          $scope.activateMemberDialog._scope.selects = {craft:{},class:{},jobStatus:{},inactiveReason:{}};
          dialog.openDialog = function(member){
            dialog._scope.member = member;
            dialog._scope.hasChanges = false;
            dialog._scope.permanent = {value: false};
            dialog._scope.hidePermanent = false;
            dialog.existingClasses = $scope.jobdaycrew.JobDayCrewMember.reduce(function(classes, m){
              if(m.PersonID == member.PersonID && m.ID != member.ID){
                classes.push(m.ClassID);
              }
              return classes;
            }, []);
            if(dialog.existingClasses.length){
              dialog._scope.hidePermanent = true;
            }
            dialog._scope.selects = {
              craft: {ID: member.CraftID,Craft:member.CraftCraft},
              class: {ID: member.ClassID,Class:member.ClassClass, CraftID: member.CraftID},
              jobStatus: {Code:member.MemberStatusCode,Description:member.MemberStatusDescription,Active:member.MemberStatusCrewMemberActiveFlag,ID:member.MemberStatusID},
              inactiveReason: member.InactiveReasonCode?{Code:member.InactiveReasonCode,Meaning:member.InactiveReasonMeaning}:undefined
            };
            $scope.activateMemberDialog._scope.calcHasChanges();
            getMemberData();
            dialog.show();
          }
          $scope.activateMemberDialog._scope.tagHandler = function(tag){
              return null;
          };
          $scope.activateMemberDialog._scope.filterSearch = function(prop, search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  return item[prop].match(regex);
              };
          };
          $scope.activateMemberDialog._scope.selectCraft = function($item, $model){
              delete $scope.activateMemberDialog._scope.selects.class;
          };
          $scope.activateMemberDialog._scope.calcHasChanges = function(){
            var member = $scope.activateMemberDialog._scope.member, selects = $scope.activateMemberDialog._scope.selects;
            if(selects.jobStatus.Active != member.OriginalActive){
              $scope.activateMemberDialog._scope.hasChanges = true;
              return;
            }
            if(selects.class && selects.class.ID != member.OriginalClassID){
              $scope.activateMemberDialog._scope.hasChanges = true;
              return;
            }
            $scope.activateMemberDialog._scope.hasChanges = false;
          }
          $scope.activateMemberDialog._scope.filterClass = function(prop, search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  if($scope.activateMemberDialog._scope.selects.craft.ID == item.CraftID && !dialog.existingClasses.includes(item.ID)){
                    return item[prop].match(regex);
                  }
                  return false;
              };
          };
          $scope.activateMemberDialog._scope.done = function(ok, craft, clss, jobStatus, reason){
              if(ok){
                  activateMemberExecute($scope.activateMemberDialog._scope.member, craft, clss, jobStatus, reason, $scope.activateMemberDialog._scope.hasChanges&&$scope.activateMemberDialog._scope.permanent.value);
              }
              $scope.activateMemberDialog.hide();
          };
      });

      ons.createAlertDialog('te-add-member-dialog.html').then(function(dialog){
          $scope.addMemberDialog = dialog;
          dialog.existingClasses = [];
          $scope.addMemberDialog._scope.permanent = false;
          $scope.addMemberDialog._scope.hidePermanent = false;
          $scope.addMemberDialog._scope.selects = {craft:{},class:{},person:{}};
          $scope.addMemberDialog._scope.tagHandler = function(tag){
              return null;
          };
          $scope.addMemberDialog._scope.filterSearch = function(prop, search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  return item[prop].match(regex);
              };
          };
          $scope.addMemberDialog._scope.filterName = function(search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  return item.FullName.match(regex);
              };
          };
          $scope.addMemberDialog._scope.selectCraft = function($item, $model){
              delete $scope.addMemberDialog._scope.selects.class;
          };
          $scope.addMemberDialog._scope.selectPerson = function($item, $model){

            $scope.addMemberDialog._scope.selects.craft = data.member.Craft.find(function(crft){
              return crft.ID == $item.CraftID;
            });
            if($scope.jobdaycrew.JobDayCrewMember.findIndex(function(m){return m.PersonID==$item.ID}) < 0){
              dialog.existingClasses = [];
              $scope.addMemberDialog._scope.selects.class = data.member.Class.find(function(clss){
                return clss.ID == $item.ClassID;
              });
            }else{
              dialog.existingClasses = $scope.jobdaycrew.JobDayCrewMember.reduce(function(classes, member){
                if(member.PersonID == $item.ID){
                  classes.push(member.ClassID);
                }
                return classes;
              }, []);
            }
            if(dialog.existingClasses.length){
              dialog._scope.hidePermanent = true;
              dialog._scope.permanent = false;
            }else{
              dialog._scope.hidePermanent = false;
            }
          };
          $scope.addMemberDialog._scope.filterClass = function(prop, search){
              var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
              return function( item ) {
                  if($scope.addMemberDialog._scope.selects.craft && $scope.addMemberDialog._scope.selects.craft.ID == item.CraftID && !dialog.existingClasses.includes(item.ID)){
                    return item[prop].match(regex);
                  }
                  return false;
              };
          };
          $scope.addMemberDialog._scope.done = function(ok, person, craft, clss, permanent){
              if(ok){
                  addMemberExecute(person, craft, clss, permanent);
              }
              $scope.addMemberDialog.hide();
          };
        });

        openBeginDay();
    }]);

})();
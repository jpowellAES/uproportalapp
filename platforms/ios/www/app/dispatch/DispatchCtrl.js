(function(){
    'use strict';
    angular.module('app').controller('DispatchController', ['$scope', 'DispatchService', '$timeout', '$anchorScroll', 'Utils', '$sce', '$filter', 'CommentDialog', 'UploadDialog', 'ApiService', 'TECompletionService', function($scope, DispatchService, $timeout, $anchorScroll, Utils, $sce, $filter, CommentDialog, UploadDialog, ApiService, TECompletionService) {

        $scope.loading = false;
        $scope.parentJob = $scope.$parent.current.job;
        $scope.job = $scope.parentJob.dispatch;

        $scope.callbacks = {}
        var dialogs = {}
        var locked = false;
        $scope.checkCallback = function(when){
            when = when||'beforeInit';
            var handled = false, init = $scope.job.Init;
            $scope.job.Init = false;
            if($scope.job.callback && $scope.callbacks[$scope.job.callback]){
                var callback = $scope.callbacks[$scope.job.callback];
                if(typeof callback == 'function') callback = {when:'beforeInit', fn: callback}
                if(when == callback.when){
                    callback.fn($scope.job.callbackData);
                    if(!init) handled = $scope.job.callback;
                    delete $scope.job.callback;
                    delete $scope.job.callbackData;
                }
            }
            return handled;
        }

        var init = function(){
            if(!$scope.job.IsReadOnly){locked = true}
            if($scope.job.DispatchWizardShowFiltersFlag){
                $scope.searchVisible = true;
            }
            DispatchService.cleanJob($scope.job, $scope);
            initFilters();
        }

        var flash = function(id){
            var flashing = document.querySelectorAll('.flash')
            for(var e=0; e<flashing.length; ++e){
                flashing[e].classList.remove('flash');
            }
            $timeout(function(){
                var elem = document.getElementById(id);
                if(elem){
                    $timeout(function(){
                        elem.classList.add('flash');
                    });
                }
            });
        }
        $scope.flash = flash;

        $scope.changes = function(changes){
            DispatchService.changes($scope.job,changes);
        }

        $scope.count = function(variable){
            $scope[variable] = $scope[variable]||0
            return $scope[variable]++;
        }
        $scope.reset = function(variable, num){
            return $scope[variable] = num||0;
        }

        $scope.buildWildcardRegex = function(term){
            var str = term.replace(/^[\*\?\s]*/g, '').replace(/[\*\?\s]*$/g, '');
            str = Utils.sanitizeTermForRegex(str);
            str = str.replace(/\\\*/g, '.*');
            str = str.replace(/\\\?/g, '.');
            return new RegExp(str, 'ig');
        };

        $scope.filters = {}
        $scope.filterLocations = function(){
            $scope.filteredCounts = [0,0]
            var keys = Object.keys($scope.filters)
            var regex = []
            for(var k=0; k<keys.length; ++k){
                regex[k] = $scope.buildWildcardRegex($scope.filters[keys[k]])
            }
            var completeDate = $scope.job.CompleteDate?$scope.job.CompleteDate.getTime():null,
                dateFilter = $scope.job.DateFilter?$scope.job.DateFilter.getTime():null
            return function(item){
                if(item.deleted) return false;
                $scope.filteredCounts[1]++;
                if(Utils.isTempId(item.ID) || item.forceVisible){
                    $scope.filteredCounts[0]++;
                    return true;
                }
                var time = item.WorkDate.getTime();
                if(dateFilter && time != dateFilter) return false;
                if(time!=completeDate && (($scope.job.doneFilter.Display == 'Not Done' && item.LocationComplete) || ($scope.job.doneFilter.Display == 'Done' && !item.LocationComplete))) return false;
                if($scope.job.hasForemenAssigned && $scope.job.foremanFilter.ID!='ALL' && $scope.job.foremanFilter.ID!=item.LocationAssignedForemanID) return false;
                if(regex.length){
                    for(var k=0; k<keys.length; ++k){
                        if(item[keys[k]].match(regex[k])){
                            $scope.filteredCounts[0]++;
                            return true;
                        }
                    }
                    return false;
                }
                $scope.filteredCounts[0]++;
                return true;
            }
        }
        $scope.executefilters = function(){
            var filter = $scope.filterLocations();
            $scope.filteredLocations = $scope.job.DispatchLocation.filter(filter);
        }
        

        $scope.doneFilterOptions = [
            { Display: 'Not Done' },
            { Display: 'Done' },
            { Display: 'All' }
        ]
        $scope.foremanFilterOptions = [
            { ID: 'ALL', FullName: 'All Foremen' },
            { ID: '', FullName: 'Unassigned' }
        ]
        $scope.mappedFilterOptions = [
            { Display: 'All Statuses' },
            { Display: 'Mapped' },
            { Display: 'Unmapped' }
        ]

        $scope.changeFilter = function(field){
            for(var l=0; l<$scope.job.DispatchLocation.length; ++l){
                delete $scope.job.DispatchLocation[l].forceVisible;
            }
            if(field === 'date'){
                DispatchService.changeDatefilter($scope);
                return;
            }
            var val = (field.filterValue||'').trim()
            if(val){
                $scope.filters[field.key] = val
            }else{
                delete $scope.filters[field.key]
            }
        }
        var initFilters = function(){
            for(var f=0; f<$scope.job.DispatchField.length; ++f){
                if($scope.job.DispatchField[f].FilterFlag && $scope.job.DispatchField[f].filterValue){
                    $scope.changeFilter($scope.job.DispatchField[f])
                }
            }
            $scope.job.doneFilter = $scope.job.doneFilter||$scope.doneFilterOptions[0];
            $scope.job.mappedFilter = $scope.job.mappedFilter||$scope.mappedFilterOptions[0];
        }

        $scope.filterLocationsFields = function(){
            return function(item){
                return $scope.job.fields[item.DispatchJobFieldID]&&$scope.job.fields[item.DispatchJobFieldID].VisibleFlag;
            }
        }

        $scope.clearSearch = function(){
            for(var i=0; i<$scope.job.DispatchField.length; ++i){
                var field = $scope.job.DispatchField[i];
                var focused = false;
                if(field.FilterFlag){
                    field.filterValue = '';
                    delete $scope.filters[field.key];
                    if(!focused){
                        document.querySelector('#dispatch-filter-'+field.DispatchWizardFieldID).focus();
                        focused = true;
                    }
                }
            }
        }

        var searchCallback = null;
        $scope.callbacks.searchCallback = function(resp){
            $scope.working = false;
            if(resp){
                if(resp.DispatchLocation){
                    var locations = DispatchService.includeLocations($scope, resp);
                    if(typeof searchCallback == 'function') searchCallback(locations);
                }else{
                    if(!$scope.job.uniqueLocationID){
                        if(typeof resp == 'string'){
                            dialogs.alert.open({title: 'Error', content: resp, buttons:[{label:'Ok'}]})
                        }else if(resp.locked){
                            ons.notification.alert(resp.message, {title: "Error"});
                        }
                    }
                }
            }
            if(typeof searchCallback == 'function') searchCallback();
            if($scope.job.uniqueLocationID){
                DispatchService.checkUniqueErrorPostSearch($scope)
            }
        }
        $scope.search = function(options){
            options = options || {};
            var searchTerms = $scope.job.DispatchField.filter(function(f){ return f.FilterFlag && f.filterValue && f.filterValue.trim()})
            if(searchTerms.length||(options.lon&&options.lat)){
                var obj = getPostObj('search')
                obj.timeDate = obj.date;
                delete obj.date;
                if(options.lon&&options.lat){
                    obj.lon = options.lon;
                    obj.lat = options.lat;
                }else{
                    for(var f=0; f<searchTerms.length; ++f){
                        obj[searchTerms[f].ColumnName] = searchTerms[f].filterValue.trim()
                    }
                }
                searchCallback = options.callback;
                $scope.load('dispatch/search', 'GET', obj, 'searchCallback');
                $scope.loading = true;
            }
        }

        $scope.load = function(endpoint, method, extraData, callbackName){
            DispatchService.load($scope, endpoint, method, extraData, callbackName);
        }

        $scope.isTempId = Utils.isTempId

        $scope.showUntrackedNotice = function(){
            dialogs.alert.open({title: $scope.job.UntrackedLocations+' Untracked Locations', content: 'Some locations pre-existing on the job could not be tracked via '+$scope.job.DispatchWizardName+' (e.g., locations using Assemblies). These locations can be managed in Complete Work.', buttons:[{label:'Ok'}]})
        }


        $scope.markAllComplete = function(forceDates){
            var dateMismatch = false;
            var error = false
            var locationsToComplete = $scope.filteredLocations.filter(function(location){
                if(!forceDates && $scope.job.CompleteDate && !location.deleted && location.WorkDate.getTime() != $scope.job.CompleteDate.getTime()){
                    dateMismatch = true
                }
                return !location.deleted && !location.LocationComplete;
            })
            if(dateMismatch){
                dialogs.alert.open({title: 'Warning', content: 'You cannot complete items unless their dates match your time entry date ('+Utils.date($scope.job.CompleteDate, 'MM/dd/yyyy')+'). Would you like to change the dates and complete?', buttons:[{label:'Change Date & Complete',callback:function(){
                    $scope.markAllComplete(true)
                }},{label:'Cancel'}]})
                return;
            }
            for(var l=0; l<locationsToComplete.length; ++l){
                var location = locationsToComplete[l]
                if(forceDates){
                    location.WorkDate = new Date($scope.job.CompleteDate.getTime())
                }
                location.LocationComplete = true
                error = error||$scope.checkComplete($scope.job.DispatchLocation[l], true);
                if(error){
                    break;
                }
            }
            if(error){
                dialogs.alert.open({title: 'Some Locations Not Completed', content: error, buttons:[{label:'Ok'}]})
            }
        }
        $scope.addLocation = function(){
            var location = DispatchService.addLocation($scope)
            location.touched = true;
            $timeout(function(){
                $anchorScroll('location-'+location.ID)
                document.getElementById('location-'+location.ID).querySelector('.dispatch-field contenteditable').focus()
            })
            $scope.changes(true);
        }
        $scope.selectOption = function(field, location, $event){
            if($scope.job.fields[field.DispatchJobFieldID].TypeCode == 'DROP'){
                var elem = angular.element($event.target), temp = angular.element(document.querySelector("#width_tmp_option"))
                temp[0].parentNode.style.display = 'inline-block';
                temp.html(angular.element(elem[0].querySelector('option:checked')).text());
                elem[0].style.minWidth = temp[0].parentNode.offsetWidth + 'px';
                temp[0].parentNode.style.display = 'none';
            }
            if(field.option){
                field.FieldValue = field.option.FieldValue;
                $scope.changes(true);
            }
            location.touched = true;
            DispatchService.updateDependencyValues($scope.job, field, location)
        }
        $scope.changeField = function(field, location){
            if(!DispatchService.uniqueField($scope, field, location)){
                $scope.changes(true);
                DispatchService.updateDependencyValues($scope.job, field, location)
                location.touched = true;
            }
        }
        $scope.openCharacter = function(field, location){
            if($scope.loading) return;
            var disabled = location.LocationComplete||$scope.job.IsReadOnly
            CommentDialog.show({
                title: $scope.job.fields[field.DispatchJobFieldID].Name,
                accept: function(comment){
                    field.FieldValue = comment;
                    $scope.changeField(field, location)
                    CommentDialog.hide();
                },
                comment: field.FieldValue,
                highlight: !disabled,
                disabled: disabled,
                requireComment: !!$scope.getRequiredClass(field.FieldValue, field),
                placeholder: $scope.job.fields[field.DispatchJobFieldID].Name,
                button: ['accept','Ok']
            });
        }

        var secondaryCallback;
        var performSecondaryCallback = function(){
            if(typeof secondaryCallback === 'function'){
                secondaryCallback(true)
            }
        }
        $scope.callbacks.locationDeleteCallback = function(resp, callback){
            if(resp.data && resp.data.success === false &&  resp.data.message){
                dialogs.alert.open({title: 'Cannot Delete', content: resp.data.message, buttons:[{label:'Ok'}]})
            }else if(resp){
                if(resp.locked){
                    ons.notification.alert(resp.message, {title: "Error"});
                }else if(resp.dispatchLocation){
                    DispatchService.deleteLocation($scope, resp.dispatchLocation, true);
                    performSecondaryCallback();
                }
            }
        }
        var deleteLocation = function(location){
            location.touched = true;
            return DispatchService.deleteLocation($scope, location);
        }
        $scope.deleteLocation = function(location, callback){
            secondaryCallback = callback;
            if(location.LocationComplete){
                dialogs.alert.open({title: 'Delete Location', content: 'This location is marked complete. Are you sure you want to delete it?', buttons:[{label:'Cancel'},{label:'Delete', callback: function(){
                    if(deleteLocation(location)>-1) performSecondaryCallback();
                }}]});
            }else{
                if(deleteLocation(location)>-1) performSecondaryCallback();
            }
        }

        $scope.copyLocation = function(location){
            $scope.changes(true);
            $scope.copying = false;
            var newLocation = DispatchService.copyLocation($scope, location);
            newLocation.touched = true;
            $timeout(function(){
                $anchorScroll('location-'+newLocation.ID)
                var id = null
                for(var i=0; i<$scope.job.DispatchField.length; ++i){
                    if($scope.job.DispatchField[i].VisibleFlag && $scope.job.DispatchField[i].EditableFlag){
                        id = $scope.job.DispatchField[i].DispatchWizardFieldID
                        if(!$scope.job.DispatchField[i].ClonesFlag){
                            break;
                        }
                    }
                }
                if(id){
                    document.getElementById('location-'+newLocation.ID).querySelector('.dispatch-field-'+id+' contenteditable').focus()
                    document.getElementById('location-'+newLocation.ID).querySelector('.dispatch-field-'+id+' contenteditable').click()
                }
            })
        }

        $scope.checkComplete = function(location, noMessage){
            if(location.LocationComplete){
                if(!$scope.job.canComplete){
                    location.LocationComplete = false;
                    if(!noMessage)dialogs.alert.open({title: 'Cannot Complete', content: 'Locations cannot be completed until this job has an active design.', buttons:[{label:'Ok'}]})
                    return;
                }
                var result = DispatchService.validateLocation($scope.job, location)
                if(!result.ok){
                    highlightLocationErrors(result);
                    location.LocationComplete = false;
                    if(!noMessage){
                        dialogs.alert.open({title: 'Cannot Complete', content: 'This location is missing required information.', buttons:[{label:'Ok', callback: function(){
                            $timeout(function(){
                                $anchorScroll('location-'+location.ID)
                                var field = document.getElementById('location-'+location.ID).querySelector('td.'+result.fields[0]+' input');
                                if(field){
                                    field.focus();
                                }
                            })
                        }}]})
                    }
                    return 'Some location(s) are missing required information.';
                }
                if($scope.job.CompleteDate && location.WorkDate.getTime() != $scope.job.CompleteDate.getTime()){
                    location.LocationComplete = false;
                    if(!noMessage){
                        dialogs.alert.open({title: 'Warning', content: 'You cannot complete items unless their dates match your time entry date ('+Utils.date($scope.job.CompleteDate, 'MM/dd/yyyy')+'). Would you like to change the date and complete?', buttons:[{label:'Change Date & Complete',callback:function(){
                            location.WorkDate = new Date($scope.job.CompleteDate.getTime())
                            location.LocationComplete = true
                            $scope.checkComplete(location)
                        }},{label:'Cancel'}]})
                    }
                    return;
                }
            }
            if(!noMessage){
                var indexChanged;
                if(indexChanged = DispatchService.sortLocations($scope.job, location)){
                    flash('location-'+location.ID)
                }
            }
            location.touched = true;
            $scope.changes(true)
        }

        $scope.getRequiredClass = function(value, field){
            if(!field || (field.DispatchJobFieldID && $scope.job.fields[field.DispatchJobFieldID] && $scope.job.fields[field.DispatchJobFieldID].RequiredFlag) || (field.Required)){
                if((!value || !value.toString().trim()) && value !== 0){
                    return 'required'
                }
            }
            return ''
        }
        $scope.getTakenClass = function(field){
            if(field && field.Taken){
                return 'taken'
            }
            return ''
        }
        var removeErrorListener = function(e){
            e.target.classList.remove('error');
            e.target.removeEventListener('paste', removeErrorListener);
            e.target.removeEventListener('keydown', removeErrorListener);
        }
        var highlightLocationErrors = function(data){
            var row = document.getElementById('location-'+data.location.ID);
            for(var f=0; f<data.fields.length; ++f){
                var field = row.querySelector('td.'+data.fields[f]+' contenteditable');
                if(!field) field = row.querySelector('td.'+data.fields[f]+' input');
                if(field){
                    field.addEventListener('paste', removeErrorListener);
                    field.addEventListener('keydown', removeErrorListener);
                    field.classList.add('error');
                }
            }
        }
        $scope.save = function(endpoint, extraData, callbackName, noValidate){
            var result = DispatchService.validate($scope.job)
            if(noValidate||result.ok){
                DispatchService.save($scope, endpoint, extraData, callbackName, '', noValidate);
            }else{
                $scope.working = false;
                for(var l=0; l<result.locations.length; ++l){
                    highlightLocationErrors(result.locations[l]);
                }
                $timeout(function(){
                    dialogs.alert.open({title: 'Error', content: 'One or more locations are missing required information.', buttons:[{label:'Ok', callback: function(){
                        $timeout(function(){
                            var first = result.locations[0];
                            $anchorScroll('location-'+first.location.ID)
                            var field = document.getElementById('location-'+first.location.ID).querySelector('td.'+first.fields[0]+' contenteditable');
                            if(!field)field = document.getElementById('location-'+first.location.ID).querySelector('td.'+first.fields[0]+' input');
                            if(field)field.focus();
                        })
                    }}]})
                })
            }
        }

        $scope.reports = function(){
            //$scope.loadingDialog(null, 'Loading Reports...');
            //$scope.save('dispatch/reports');
        }

        var uploadResult = function(resp, title, obj){
            $scope.loading = false;
            dialogs.uploads.setOpening(false)
            if(resp && resp.Key){
                var location = $scope.job.DispatchLocation.find(function(l){ return l.ID == obj.location})
                if(location){
                    location.LPhoto.push({String:resp.Key,Description:resp.Description,TypeCode:resp.TypeCode});
                    location.hasPhoto = location.hasPhoto||resp.TypeCode=='PH';
                    var uploads = location.LPhoto.map(function(photo){
                        return {Key: photo.String, Description: photo.Description, TypeCode: photo.TypeCode}
                    });
                    if(dialogs.uploads.visible){
                        dialogs.uploads.setUploads(uploads);
                        dialogs.uploads.setTags($scope.job.Tag);
                        dialogs.uploads.setOptions({requireDescription:$scope.job.DispatchWizardRequirePhotoDescription,tagOnly:$scope.job.DispatchWizardOnlyAllowTagDescriptions});
                    }else{
                        dialogs.uploads.open(title, obj, uploads, $scope.job.Tag, {requireDescription:$scope.job.DispatchWizardRequirePhotoDescription,tagOnly:$scope.job.DispatchWizardOnlyAllowTagDescriptions}, location.hasDocument?'PH':'');
                    }
                }
            }
        }
        var showPhotoUpload = function(callback, title, obj){
            UploadDialog.show({save: function(data){
                if(data.description) obj.description = data.description;
                obj.session = $scope.job.Session;
                ApiService.getFormData(obj, {photo:data.photoUrl}).then(function(formdata){
                    $scope.loading = true;
                    if(dialogs.uploads.visible){
                        dialogs.uploads.setOpening(true)
                    }
                    var objCopy = Utils.copy(obj);
                    var done = function(resp){callback(resp,title,objCopy)};
                    ApiService.post('dispatch', obj, '', '', formdata)
                    .then(done, done);
                });
                UploadDialog.hide();
            },
            tags: $scope.job.Tag,
            requireDescription:$scope.job.DispatchWizardRequirePhotoDescription,
            tagOnly:$scope.job.DispatchWizardOnlyAllowTagDescriptions
            });
        }

        var deleteUpload = function(upload, dialogData, callback){
            var obj = getPostObj('photo', {key: upload.Key,delete:true})
            var done = function(upl){
                var location = $scope.job.DispatchLocation.find(function(l){ return l.ID == dialogData.location })
                var photos = location.LPhoto;
                var uploadIndex = photos.findIndex(function(p){  return p.String == upl.Key });
                if(uploadIndex > -1){
                    photos.splice(uploadIndex, 1);
                    location.hasPhoto = !!photos.find(function(ph){ return ph.TypeCode == 'PH'})
                }
                callback(upl);
            }
            ApiService.post('dispatch', obj)
                    .then(done, done);
        }

        $scope.locationPhotos = function(location, type){
            var visibleField = $scope.job.DispatchField.find(function(f){ return f.VisibleFlag })
            var title;
            if(visibleField){
                title = location[visibleField.key]
            }else{
                title = location.Address
            }
            var obj = getPostObj('photo', {location: location.ID})
            if(!location.LPhoto.length){
                showPhotoUpload(uploadResult, title, obj);
            }else{
                dialogs.uploads.open(title, obj, location.LPhoto.map(function(photo){
                    return {Key: photo.String, Description: photo.Description, TypeCode: photo.TypeCode}
                }), $scope.job.Tag, {requireDescription:$scope.job.DispatchWizardRequirePhotoDescription,tagOnly:$scope.job.DispatchWizardOnlyAllowTagDescriptions}, location.hasDocument?type:'');
            }
        }

        $scope.getLocationAddressObj = DispatchService.getLocationAddressObj;

        $scope.load = function(endpoint, method, extraData, callbackName){
            DispatchService.load($scope, endpoint, method, extraData, callbackName);
        }
        $scope.loadingDialog = function(text){
            if(text){
                Utils.notification.toast(text);
            }
        }

        $scope.endDay = function(){
            $scope.closing = true;
            closeJob();
        }

        $scope.openRouting = function(){
            mainNav.pushPage('app/mapping/route.html', {
                parent: $scope
            });
        }

        $scope.callbacks.closeJobResult = function(resp){
            if(resp){
               if(resp.locked){
                    ons.notification.alert(resp.message, {title: "Error"});
                }else{
                    locked = false;
                    if($scope.job.process && $scope.job.process.name == 'EndDay'){
                        TECompletionService.checkNextJob($scope, $scope.job.process, function(){
                            $scope.loading = false;
                            $scope.working = false;
                        });
                    }else{
                        mainNav.popPage();
                    }
                }
            }
        };
        var getPostObj = function(action, data){
            data = data||{}
            data.action = action
            data.dispatch = $scope.job.ID
            data.session = $scope.job.Session;
            data.job = $scope.job.JobID
            if($scope.job.process && $scope.job.process.name == 'EndDay'){
               data.jdc = $scope.parentJob.JobDayCrewID;
               data.date = $scope.job.process.date;
            }
            return data;
        }
        $scope.getPostObj = getPostObj;
        var closeJob = function(skipSave){
           $scope.working = true;
           if(skipSave){
                $scope.save('completionClose', {job:$scope.job.JobID}, 'closeJobResult', true);
           }else{
               $scope.save('dispatch', getPostObj('save', {close:true}), 'closeJobResult');
           }
           $scope.closing = false;
        };
        $scope.closing = false;

        $scope.close = function(force){
            if(!$scope.job.changes || $scope.job.IsReadOnly){
                closeJob(true);
                return;
            }
            if($scope.closing || (mainNav.pages[mainNav.pages.length-1].pushedOptions.page != 'app/dispatch/dispatch-job.html')) return;
            $scope.closing = true;
            if(force === true){
                closeJob();
                return;
            }
            ons.notification.confirm("Would you like to save your changes?", {
                callback: function(ok){
                    if(ok == 2){
                        closeJob();
                    }else if(ok == 1){
                        closeJob(true);
                    }else{
                        $scope.closing = false;                        
                    }
                },
                buttonLabels: ['Cancel', 'Discard', 'Save']
            });
        }
        
        // Alert dialog
        ons.createAlertDialog('alert-dialog.html').then(function(dialog){
            dialogs.alert = dialog;
            dialog._scope.clickButton = function(button){
                if(button.callback){
                    if(button.callback()!==true){
                        dialog.hide();
                    }
                }else{
                    dialog.hide();
                }
            }
            dialog._scope.model = {}
            dialog.open = function(options){
                dialog.cancelable = options.cancelable||false; 

                dialog._scope.model.title = options.title||'';
                dialog._scope.model.html = options.html?$sce.trustAsHtml(options.html):'';
                dialog._scope.model.content = options.content||'';
                dialog._scope.model.buttons = options.buttons||[];
                dialog._scope.model.visible = true;
                dialog._scope.model.changes = false;
                dialog._scope.model.error = "";
                Utils.openDialog(dialog);
            }
            DispatchService.setDialog(dialog);
        });

        //Uploads dialog
        ons.createDialog('app/dialog/uploads/uploads-dialog.html').then(function(dialog){
            var dataObj;
            dialog._scope.types = [{code:'PH',label:'Photos'},{code:'DO',label:'Documents'}]
            dialog._scope.canEdit = true;
            dialog._scope.settings = {}
            dialog.open = function(title, obj, uploads, tags, options, type){
                dataObj = Utils.copy(obj);
                dialog._scope.title = title;
                dialog._scope.openingUpload = false;
                dialog.setType(type);
                dialog.setTags(tags);
                dialog.setOptions(options);
                dialog.setUploads(uploads);
                Utils.openDialog(dialog);
            };
            dialog.setUploads = function(uploads){
              dialog._scope.uploads = uploads;
            }
            dialog.setTags = function(tags){
                dialog._scope.setTags(tags);
            }
            dialog.setOptions = function(options){
                dialog._scope.setOptions(options);
            }
            dialog.setOpening = function(opening){
                dialog._scope.openingUpload = opening;
            }
            dialog.setType = function(type){
                dialog._scope.settings.initType = type||'';
                dialog._scope.settings.type = type||'';
                if(type){
                    $timeout(function(){
                        uploadsTypeTabBar.on('prechange', function(e){                            
                            dialog._scope.$apply(function(){dialog._scope.settings.type = e.index?'DO':'PH';})
                        })
                    })
                }
            }
            dialog._scope.addUpload = function(){
                showPhotoUpload(uploadResult, dialog._scope.title, dataObj);
            }
            dialog._scope.deleteUpload = function(upload){
                dialog._scope.openingUpload = true;
                deleteUpload(upload, dataObj, function(upl){
                    dialog._scope.openingUpload = false;
                    var uploadIndex = dialog._scope.uploads.findIndex(function(u){ return u.Key == upl.Key});
                    if(uploadIndex > -1){
                        dialog._scope.uploads.splice(uploadIndex, 1);
                    }
                });
            }
            dialog._scope.captionEdited = function(upload){
                var photo;
                var photos = $scope.job.DispatchLocation.find(function(l){ return l.ID == dataObj.location }).LPhoto;
                if(photo = photos.find(function(dp){ return dp.String == upload.Key})){
                    photo.Description = upload.Description;
                }
            }
            dialog.on('posthide', function(){
                dialog._scope.$apply(function(){
                    dialog._scope.settings.type = '';
                    dialog._scope.settings.initType = '';
                });
            })
            dialogs.uploads = dialog;
        });


    $scope.$on('$destroy', function(){
        if(locked){
            ApiService.post('completionClose',  {job:$scope.job.JobID});
        }
        var dialogKeys = Object.keys(dialogs);
        for(var k=0; k<dialogKeys.length; ++k){
            var dialog = dialogs[dialogKeys[k]];
            if(dialog.visible){
                dialog.hide();
            }
        }
        CommentDialog.hide()
        UploadDialog.hide()
    });

    var toggleBackButton = function(on){
        if(on){
            ons.disableDeviceBackButtonHandler();
            document.addEventListener('backbutton', $scope.close, false);
        }else{
            document.removeEventListener('backbutton', $scope.close);
            ons.enableDeviceBackButtonHandler();
        }
      }

      $scope.hidePage = function(){
        toggleBackButton(false)
        Utils.clearBackButtonHandlers()
      }
      $scope.showPage = function(){
        toggleBackButton(true)
        Utils.setBackButtonHandlers(function(){
            toggleBackButton(false);
        }, function(){
            toggleBackButton(true);
        })

        ons.findComponent('#dispatch-back-button')._element[0].onClick = $scope.close;
      }
        

      init();
    }]);

})();
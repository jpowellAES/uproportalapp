(function(){
    'use strict';

    angular.module('app').controller('SpecDocsController', ['$scope', '$timeout', 'ApiService', 'StorageService', function($scope, $timeout, ApiService, StorageService) {
        $scope.loaded = false;
        $scope.working = false;
        $scope.tabs = [];
        $scope.tabIndex = 0;
        $scope.hasSpecs = false;

        var loadSpecBooks = function($done){
            $scope.working = true;
            ApiService.get('specdocs',{action: 'specbooks'}).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var initTabs = function(sb){
            if(!Array.isArray(sb.SpecBook)){
                sb.SpecBook = [sb.SpecBook];
            }
            sb.SpecBook.forEach(function(specbook){
                var tab = $scope.tabs.find(function(tab){
                    return tab.title == specbook.OwnerName;
                });
                if(!tab){
                    tab = {title: specbook.OwnerName, specbooks:[]};
                    $scope.tabs.push(tab);
                }
                tab.specbooks.push(specbook);
            });
        }

        var result = function(resp){
            if(resp){
                $scope.hasSpecs = true;
                initTabs(resp);
                $timeout(function(){
                    $scope.specDocTabbar.refresh()
                    $scope.specDocTabbar.element.setActiveTab(0);
                },0);
            }else{
                $scope.hasSpecs = false;
            }
            $scope.working = false;
        }

        $scope.openSpecBook = function(sb){
            $scope.current.specbook = sb;
            mainNav.pushPage('app/docs/spec-docs-book.html');
        }

        var init = function(){
            $scope.loaded = true;
        };

        var saveStateFields = ['tabs','working', 'tabIndex', 'hasSpecs'];
        $scope = StorageService.state.load('SpecDocsController', $scope, saveStateFields);
        $scope.$on('$destroy', function() {
            StorageService.state.clear('SpecDocsController');
        });
        if(!$scope.hasSpecs)
            loadSpecBooks();
        $timeout(init);

    }]);

    angular.module('app').controller('SpecDocsBookController', ['$scope', 'ApiService', function($scope, ApiService) {
        $scope.working = false;
        $scope.specbook = $scope.$parent.current.specbook;


        var loadSections = function($done){
            $scope.working = true;
            ApiService.get('specdocs',{action: 'specsections', specbook:$scope.specbook.ID}).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var cleanSections = function(sections){
            if(!Array.isArray(sections))
                sections = [sections];
            return sections;
        };

        var result = function(resp){
            if(resp && resp.SpecSection){
                $scope.specbook.sections = cleanSections(resp.SpecSection);
            }
            $scope.working = false;
        }

        $scope.openSection = function(section){
            $scope.current.section = section;
            mainNav.pushPage('app/docs/spec-docs-section.html');
        }

        if(!$scope.specbook.sections || !$scope.specbook.sections.length)
            loadSections();
        
    }]);

    angular.module('app').controller('SpecDocsSectionController', ['$scope', 'ApiService', 'ImgOrPdf', function($scope, ApiService, ImgOrPdf) {
        $scope.working = false;
        $scope.opening = false;
        $scope.section = $scope.$parent.current.section;


        var loadSpecs = function($done){
            $scope.working = true;
            ApiService.get('specdocs',{action: 'specs', specsection:$scope.section.SpecificationSectionID}).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var cleanSpecs = function(docs){
            if(!Array.isArray(docs))
                docs = [docs];
            return docs;
        };

        var result = function(resp){
            if(resp && resp.SpecDocument){
                $scope.section.docs = cleanSpecs(resp.SpecDocument);
            }
            $scope.working = false;
        }

        $scope.openDocument = function(doc){
            if($scope.opening)
                return;
            var path = 'spec/'+doc.ID;
            $scope.opening = true;
            ImgOrPdf.load(path).finally(function(){
                $scope.opening = false;
            });
        }

        if(!$scope.section.docs || !$scope.section.docs.length)
            loadSpecs();
        
    }]);
})();
(function(){
    'use strict';

    angular.module('app').controller('OrgDocsController', ['$scope', '$timeout', 'ApiService', 'StorageService', 'ImgOrPdf', 'Download', 'urls', function($scope, $timeout, ApiService, StorageService, ImgOrPdf, Download, urls) {
        $scope.loaded = false;
        $scope.working = false;
        $scope.opening = false;
        $scope.tabs = [];
        $scope.tabIndex = 0;
        $scope.hasDocs = false;

        var loadDocs = function($done){
            $scope.working = true;
            ApiService.get('orgdocs',{type: '*'}).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var initTabs = function(resp){
            if(!Array.isArray(resp.OrgDocument)){
                resp.OrgDocument = resp.OrgDocument?[resp.OrgDocument]:[];
            }
            resp.OrgDocument.forEach(function(doc){
                var tab = $scope.tabs.find(function(tab){
                    return tab.code == doc.TypeCode;
                });
                if(!tab){
                    tab = {title: doc.TypeMeaning, code: doc.TypeCode, docs:[]};
                    $scope.tabs.push(tab);
                }
                tab.docs.push(doc);
            });
        }

        var result = function(resp){
            if(resp){
                $scope.hasDocs = true;
                initTabs(resp);
                $timeout(function(){
                    $scope.orgDocTabbar.refresh()
                    $scope.orgDocTabbar.element.setActiveTab(0);
                },0);
            }else{
                $scope.hasDocs = false;
            }
            $scope.working = false;
        }

        $scope.openDocument = function(doc){
            if($scope.opening)
                return;
            var path = 'org/'+doc.ID, promise = null, done = function(){$scope.opening = false;};
            var downloadUrl =  urls.media+'/'+path;
            if(doc.Mime == 'application/pdf' || doc.Mime.match('image/')){
                $scope.opening = true;
                if(doc.Mime == 'application/pdf'){
                    ImgOrPdf.load(path, downloadUrl, doc.Name).finally(done);
                }else{
                    ImgOrPdf.load(path).finally(done);
                }
            }else{
                Download.go(downloadUrl, doc.Name+doc.Extension, 'ACTION_VIEW', doc.Mime);
            }
        }

        var init = function(){
            $scope.loaded = true;
        };

        var saveStateFields = ['tabs','working', 'tabIndex', 'hasDocs'];
        $scope = StorageService.state.load('OrgDocsController', $scope, saveStateFields);
        $scope.$on('$destroy', function() {
            StorageService.state.clear('OrgDocsController');
        });
        if(!$scope.hasDocs)
            loadDocs();
        $timeout(init);

    }]);

})();
(function(){
    'use strict';

    angular.module('app').service('ApiService', ['urls', '$http', '$rootScope', '$window', '$q', function(urls, $http, $rootScope, $window, $q){
        
        var _checkResponseError = function(error){
            console.log(error);
            if(error && error.data && error.data.reason && error.data.reason == "expiredSession"){
                $rootScope.$emit('AuthService', {type:'expired'});
            }
            return error;
        };

        var _sanitizeRegex = [new RegExp('\\uFFFD','g'),new RegExp('[^\\P{C}\\n]+','gu')];
        var _sanitizeString = function(str){
            _sanitizeRegex.forEach(function(re){
                str = str.replace(re, '');
            });
            return str
        };

        var _sanitizeProps = function(obj){
            if(!obj){
                return obj;
            }
            var props = Object.keys(obj);
            for (var p=0; p<props.length; ++p) {
                if (obj.hasOwnProperty(props[p])) {
                  if (typeof obj[props[p]] == "object") {
                    obj[props[p]] = _sanitizeProps(obj[props[p]]);
                  } else {
                    if(typeof obj[props[p]] == "string"){
                        obj[props[p]] = _sanitizeString(obj[props[p]]);
                    }
                  }
                }
            }
            return obj;
        }

        var _fillFormat = function(url, data, route){
            if(!url.match(/\{/))
                return url;
            var result = url;
            var isFormData = data.toString().indexOf('object FormData') > -1,
                obj = typeof route=='object'?route:data;
            if(isFormData){
                obj.forEach(function(key, value){
                    var regex = new RegExp('\\{'+key+'\\}', 'g');
                    if(result.match(regex)){
                        result = result.replace(regex, value);
                        delete obj[key];
                    }
                });
            }else{
                var keys = Object.keys(obj);
                keys.forEach(function(key){
                    var regex = new RegExp('\\{'+key+'\\}', 'g');
                    if(result.match(regex)){
                        result = result.replace(regex, obj[key]);
                        delete obj[key];
                    }
                });
            }
            return result;
        };

        var _getFormData = function(plain, files, options){
            return $q(function(resolve,reject){
                var fd = new FormData(), plainKeys = Object.keys(plain), fileKeys = Object.keys(files);
                for(var i=0; i<plainKeys.length; i++){
                    var value = plain[plainKeys[i]];
                    if(value && typeof value == "object"){
                        value = JSON.stringify(plain[plainKeys[i]]);
                        if(options && options.base64){
                            value = window.btoa(value);
                            fd.append(plainKeys[i]+"_isBase64", true);
                        }
                    }
                    fd.append(plainKeys[i], value);
                }
                var remaining = fileKeys.length;
                for(var i=0; i<fileKeys.length; i++){
                    (function(myName){
                        console.log(files[myName])
                        var filename = files[myName].split('/').pop().split('?').shift();
                        $window.resolveLocalFileSystemURL(files[myName], function(fileEntry) {
                             console.log(fileEntry)
                             fileEntry.file(function(file) {
                                 var reader = new FileReader();
                                 reader.onloadend = function(e) {
                                    console.log(e)
                                      var fileBlob = new Blob([ this.result ], { type: "image/jpeg" } );
                                      fd.append(myName, fileBlob, filename);
                                      --remaining;
                                      if(!remaining)
                                            resolve(fd);
                                 };
                                 console.log(file)
                                 reader.readAsArrayBuffer(file);
                             }, function(e){
                                console.log(e)
                                reject()
                             });
                        }, reject);
                    })(fileKeys[i]);
                }
                if(!remaining)
                    resolve(fd);
            });
        };

        var _getMedia = function(path, data){
            return $http.get(urls['media']+'/'+path, {params: data, responseType: "arraybuffer"}).then(
                function(resp){
                    return resp.data;
                },
                _checkResponseError
            );
        };

        var _getMediaWithMime = function(path, data){
            return $http.get(urls['media']+'/'+path, {params: data, responseType: "arraybuffer"}).then(
                function(resp){
                    var headers = resp.headers();
                    return {data: resp.data, mime: headers['content-type']};
                },
                _checkResponseError
            );
        };

        var count = 0;
        return {
            get: function(action, data, route){
                var url = _fillFormat(urls[action], data, route);
                return $http.get(url, {params: data}).then(
                    function(resp){
                        $rootScope.$emit('AuthService', {type:'renew'});
                        return _sanitizeProps(resp.data);
                    },
                    _checkResponseError
                );
            },
            post: function(action, data, suppressRenewal, isLogin, altPayload, route){
                var payload = altPayload||data;
                var isFormData = payload&&payload.toString().indexOf('object FormData') > -1;
                var url = _fillFormat(urls[action], data, route);
                var obj = isLogin||isFormData ? payload : {data:payload};
                if((!isLogin&&!isFormData) && typeof obj.data != 'object'){
                    obj.raw = true;
                }
                var config = isFormData ? 
                    {
                        transformRequest: angular.identity,
                        headers: {'Content-Type':undefined}
                    } : 
                    {};
                return $http.post(url, obj, config).then(
                    function(resp){
                        console.log(resp)
                        if(!suppressRenewal)
                            $rootScope.$emit('AuthService', {type:'renew'});
                        return _sanitizeProps(resp.data);
                    },
                    _checkResponseError
                );
            },
            getFormData: _getFormData,
            getMedia: _getMedia,
            getMediaWithMime: _getMediaWithMime
        };
    }]);

    angular.module('app').service('Utils', ['$sce', '$timeout', '$filter', '$location','$anchorScroll', '$rootScope', function($sce, $timeout, $filter, $location, $anchorScroll, $rootScope){
        var _regex = [
            [new RegExp('&','g'), '&amp'],
            [new RegExp('&','g'), '&lt'],
            [new RegExp('&','g'), '&gt'],
            [new RegExp('&','g'), '&apos'],
            [new RegExp('\\n','g'), '<br>']
        ];

        var _updateZPositions = function(){
            var dialogs = Array.prototype.concat.call(Array.prototype.slice.call(document.getElementsByTagName('ons-dialog'),0), Array.prototype.slice.call(document.getElementsByTagName('ons-alert-dialog'),0));
            var maxZ = 1000;
            dialogs.forEach(function(d){
                if(d._visible){
                    if(d._flagged){
                        maxZ = Math.max(maxZ, parseInt(d.style.zIndex||0));
                    }
                    d._flagged = true;
                }else{
                    d._flagged = false;
                }
            });
            dialogs.forEach(function(d){
                if(!d.style.zIndex){
                    maxZ += 1;
                    d.style.zIndex = maxZ;
                }
            });
        };

        Date.prototype._copy = function(){
            return new Date(this.getTime());
        }
        var _copy = function(o, exclude) {
          exclude = exclude||[];
          var _out, v, _key, typeOf, _keys = Object.keys(o);
          _out = Array.isArray(o) ? [] : {};
          for (var i=0; i<_keys.length; i++){
            _key = _keys[i];
            if(exclude.indexOf(_key) == -1){
                v = o[_key];
                typeOf = typeof v;
                if(typeOf == 'object' && v != null && typeof Object.getPrototypeOf(v)._copy != 'undefined'){
                    _out[_key] = v._copy();
                }else{
                    _out[_key] = (typeOf == 'object' && v !== null) ? _copy(v, exclude) : v;
                }
            }
          }
          return _out;
        };

        var _replace = function(o, r) {
          var find = [], replace = [];
          for(var i=0; i<r.length; ++i){
            find[i] = r[i][0]
            replace[i] = r[i][1]
          }
          var _replaceInner = function(o){
            var v, _key, _keys = Object.keys(o), index;
            for (var i=0; i<_keys.length; i++){
                _key = _keys[i];
                v = o[_key];
                if(typeof v === "object" && v !== null){
                    _replaceInner(v);
                }else if(typeof v === "string" && (index=find.indexOf(v))>-1){
                    o[_key] = replace[index];
                }
              }
          }
          _replaceInner(o);
        };

        var _date = function(input, mask){
            var date = input;
            if(typeof input == 'string'){
                var match;
                if(match = input.match(/(\d{4}\/\d{1,2}\/\d{1,2}) (\d{2}) (\d{2}) \d{2}/i)){
                    date = new Date(match[1]);
                    date.setHours(parseInt(match[2]));
                    date.setMinutes(match[3]);
                }else{
                    date = new Date(input.indexOf('T')>-1?input:input.replace('-','/'));
                }
            }else if(typeof input == 'undefined'){
                return '';
            }
            if(mask){
                return $filter('date')(date, mask);
            }
            return date;
        }

        var _treatAsUTC = function(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        var _daysBetween = function(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (_treatAsUTC(endDate) - _treatAsUTC(startDate)) / millisecondsPerDay;
        }

        var _translateArray = function(fromArr, props, options){
            options = options||{};
            var toArr = [], keys = Object.keys(props), uniqueKeys = {};
            for(var k=0; k<keys.length; ++k){
                if(typeof props[keys[k]] == 'object' && props[keys[k]].unique){
                    uniqueKeys[keys[k]] = {};
                }
            }
            if(options.nullItem){
                toArr.push(options.nullItem);
            }
            for(var i=0; i<fromArr.length; ++i){
                var fromObj = fromArr[i], toObj = {}, ok = true;
                for(var k=0; k<keys.length; ++k){
                    if(options.noNulls && (fromObj[keys[k]] == null || fromObj[keys[k]] == '')){
                        k = keys.length;
                        ok = false;
                    }else{
                        switch(typeof props[keys[k]]){
                            case 'object':
                                toObj[props[keys[k]].to] = fromObj[keys[k]];
                                if(props[keys[k]].unique){
                                    if(!uniqueKeys[keys[k]][fromObj[keys[k]]]){
                                        uniqueKeys[keys[k]][fromObj[keys[k]]] = true;
                                    }else{
                                        k = keys.length;
                                        ok = false;
                                    }
                                }
                            break;
                            case 'string':
                                toObj[props[keys[k]]] = fromObj[keys[k]];
                            break;
                        }
                    }
                }
                if(ok){
                    toArr.push(toObj);
                }
            }

            return toArr;
        }

        var _sanitizeTermForRegex = function(term){
            return term.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        }
        var _findArrayBestMatch = function(term, arry, props){
            var re = new RegExp(_sanitizeTermForRegex(term), 'i')
            var iterateProps = function(term, type){
                for(var p=0; p<props.length; ++p){
                    for(var i=0; i<arry.length; ++i){
                        if((type == 'exact' && arry[i][props[p]] == term) || (type == 'match' && arry[i][props[p]].match(re))){
                            return i;
                        }
                    }
                }
                return -1
            }
            term = (term||'').toString().trim()
            var index = iterateProps(term, 'exact')
            if(index > -1) return index;
            return iterateProps(term, 'match')
        }

        var _focusInput = function(item, select, selector){
            if(typeof item == 'string'){
                var input = document.getElementById(item);
                if(input){
                    $timeout(function(){
                        input.focus();
                        if(select){
                            input.setSelectionRange(0, input.value.length)
                        }
                    }, 100);
                }
                return;
            }
            var inputs = selector?item.querySelector(selector):item.getElementsByTagName('input');
            if(!inputs.length){
                inputs = item.getElementsByTagName('textarea');
            }
            if(inputs.length && inputs[0]){
                $timeout(function(){
                    inputs[0].focus();
                    if(select){
                        inputs[0].setSelectionRange(0, inputs[0].value.length)
                    }
                }, 100);
            }
        };

        var _default_cmp = function(a, b) {
            if (a == b) return 0;
            return a < b ? -1 : 1;
        },
        _getCmpFunc = function(primer, reverse) {
            var dfc = _default_cmp, // closer in scope
                cmp = _default_cmp;
            if (primer) {
                cmp = function(a, b) {
                    return dfc(primer(a), primer(b));
                };
            }
            if (reverse) {
                return function(a, b) {
                    return -1 * cmp(a, b);
                };
            }
            return cmp;
        };

        var _ensureTabbarIndex = function(tabbar, index, count){
            count = count||0;
            if(tabbar.getActiveTabIndex() != index && count < 20) {
                tabbar.setActiveTab(index);
            }else{
                return;
            }
            $timeout(function(){
                _ensureTabbarIndex(tabbar, index, count + 1);
            },20)
        }

        var _toast, _modalHide;
        var _hideToast = function(){
            if(_toast){
                _toast.hide();
                var oldToast = _toast;
                oldToast.className += ' removing';
                _toast = null;
                $timeout(function(){
                    if(oldToast&&oldToast.parentNode)oldToast.parentNode.removeChild(oldToast);
                }, 1000);
            }
        }
        var _dialogHandlers = {}

        var _pickProps = function(o, props) {
            return Object.assign({}, props.reduce(function(obj, prop){
                obj[prop] = o[prop];
                return obj;
            }, {}));
        }

        var _this = {
            fixUnwantedBool: function(val){
                if(val === true)
                    return 'Y';
                if(val === false)
                    return 'N'
                return val;
            },
            bool: function(val, allowNull){
                if(val === true || val == 'Y')
                    return 'Y';
                if(val === false || val == 'N')
                    return 'N'
                return allowNull?'':'N';
            },
            null: function(val){
                if(typeof val == 'undefined')
                    return '';
                return val;
            },
            number: function(val){
                if(typeof val == "number")
                    return val;
                return parseInt(val);
            },
            empty: function(val){
                if(typeof val == 'undefined' || !val || !val.toString().trim())
                    return true;
                return false;
            },
            prepareForHTML: function(text){
                _regex.forEach(function(re){
                    text = text.replace(re[0],re[1]);
                });
                return $sce.trustAsHtml(text);
            },
            round: function(value, exp){
                value += 0.00000000000000000000001;
              var mult = Math.pow(10, exp);
              return Math.round(value*mult)/mult;
            },
            date: _date,
            daysBetween: _daysBetween,
            compareDate: function(a,b){
                var _a = _date(a),
                    _b = _date(b);
                if(typeof _a == 'object' && typeof _b == 'object'){
                    return _a.getTime()==_b.getTime();
                }
                return a==b;
            },
            clearTimeFromDate: function(date){
                date.setSeconds(0);
                date.setMilliseconds(0);
                date.setMinutes(0);
                date.setHours(0);
            },
            fixPercent: function(num){
                return num?Math.round(num*10000)/100:0;
            },
            arrayBufferToBase64: function( buffer ) {
                var binary = '';
                var bytes = new Uint8Array( buffer );
                var len = bytes.byteLength;
                for (var i = 0; i < len; i++) {
                    binary += String.fromCharCode( bytes[ i ] );
                }
                return window.btoa( binary );
            },
            base64ToArrayBuffer: function(base64) {
                var binary_string =  window.atob(base64);
                var len = binary_string.length;
                var bytes = new Uint8Array( len );
                for (var i = 0; i < len; i++)        {
                    bytes[i] = binary_string.charCodeAt(i);
                }
                return bytes.buffer;
            },
            sortOnProperty: function(array, property, descending){
                if(descending){
                    array.sort(function(a,b) {return (a[property] < b[property]) ? 1 : ((b[property] < a[property]) ? -1 : 0)});
                    return;
                }
                array.sort(function(a,b) {return (a[property] > b[property]) ? 1 : ((b[property] > a[property]) ? -1 : 0)});
            },
            multiSort: function() {
                var fields = [],
                    n_fields = arguments.length,
                    field, name, reverse, cmp;
                // preprocess sorting options
                for (var i = 0; i < n_fields; i++) {
                    field = arguments[i];
                    if (typeof field === 'string') {
                        name = field;
                        cmp = _default_cmp;
                    }
                    else {
                        name = field.name;
                        cmp = _getCmpFunc(field.primer, field.reverse);
                    }
                    fields.push({
                        name: name,
                        cmp: cmp
                    });
                }
                // final comparison function
                return function(A, B) {
                    var a, b, name, result;
                    for (var i = 0; i < n_fields; i++) {
                        result = 0;
                        field = fields[i];
                        name = field.name;

                        result = field.cmp(A[name], B[name]);
                        if (result !== 0) break;
                    }
                    return result;
                }
            },
            sortObj: function(obj, dir){
                dir = dir||'a';
                var sortable = [];
                for(var key in obj) {
                    sortable.push([key, obj[key]]);
                }
                if(dir == 'a'){
                    sortable.sort(function(a, b) {
                        return a[1] - b[1];
                    });
                }else{
                    sortable.sort(function(a, b) {
                        return b[1] - a[1];
                    });
                }
                return sortable;
            },
            pick: function(o) {
                var props = [];
                for(var i=1; i<arguments.length; i++){
                    props.push(arguments[i]);
                }
                return _pickProps(o, props);
            },
            pickProps: _pickProps,
            propHash: function(obj, props){
                props = props||Object.keys(obj);
                if(typeof props == 'string') props = [props];
                var str = '';
                for(var i=0; i<props.length; ++i){
                    var val = (obj[props[i]]).toString();
                    str += val;
                }
                return str.hashCode();
            },
            scrollTo: function(id, time){
                $timeout(function() {
                    $location.hash(id);
                    $anchorScroll(id);
                }, time);
            },
            flash: function(item){
                if(typeof item == 'string'){
                    item = document.getElementById(item)
                }
                if(item){
                    item.className += ' flash';
                    setTimeout(function(){
                        if(item){
                            item.className = item.className.replace(' flash', '')
                        }
                    }, 3000)
                }
            },
            hideKeyboard: function(element) {
                if(element.tagName == 'TEXTAREA'){
                    element.setAttribute('readonly', 'readonly'); // Force keyboard to hide on input field.
                }else{
                    element.setAttribute('disabled', 'true'); // Force keyboard to hide on textarea field.
                }
                if (window.getSelection) {
                    window.getSelection().removeAllRanges();
                } else if (document.selection) {
                    document.selection.empty();
                }
                setTimeout(function() {
                    element.blur();  //actually close the keyboard
                   if(element.tagName == 'TEXTAREA'){
                        element.removeAttribute('readonly');
                    }else{
                        element.removeAttribute('disabled'); 
                    }
                }, 100);
            },
            copy: _copy,
            replace: _replace,
            notification: {
                alert: function(){
                    var promise = ons.notification.alert.apply(this, arguments);
                    _updateZPositions();
                    return promise;
                },
                confirm: function(){
                    var promise = ons.notification.confirm.apply(this, arguments);
                    _updateZPositions();
                    return promise;
                },
                prompt: function(){
                    var promise = ons.notification.prompt.apply(this, arguments);
                    _updateZPositions();
                    return promise;
                },
                toast: function(message, options){
                    if(typeof message == 'object'){
                        options = message
                    }else{
                        options = options||{};
                        options.message = message;
                    }
                    options.replace = typeof options.replace == 'undefined' ? true : options.replace;
                    if(options.replace){
                        _hideToast()
                    }
                    options.force = true;
                    ons.notification.toast(options);
                    $timeout(function(){
                        _toast = document.querySelector('ons-toast:not(.removing)');
                    });
                },
                hideToast: _hideToast,
                modal: function(text, duration){
                    $rootScope.modalText = text||'';
                    $timeout(function(){
                        _this.openDialog(mainModal)
                    })
                    if(duration){
                        if(_modalHide){
                            $timeout.cancel(_modalHide)
                        }
                        _modalHide = $timeout(function() {
                        mainModal.hide();
                      }, duration);
                    }
                },
                hideModal: function(){
                    if(_modalHide){
                        $timeout.cancel(_modalHide)
                        _modalHide = null
                    }
                    mainModal.hide()
                }
            },
            clearBackButtonHandlers: function(){
                delete _dialogHandlers.onShow;
                delete _dialogHandlers.onHide;
            },
            setBackButtonHandlers: function(onShow, onHide){
                _dialogHandlers.onShow = onShow;
                _dialogHandlers.onHide = onHide;
            },
            openDialog: function(dialog, options){
                if(!dialog || !dialog.show)
                    return;
                options = options||{}
                if(_dialogHandlers.onShow && !_dialogHandlers.open){
                    _dialogHandlers.open = dialog;
                    _dialogHandlers.onShow();
                }
                if(!dialog.cancelable){
                    dialog.onDeviceBackButton = function(){ return true }
                }
                dialog.once('preshow', function(ev){
                    var dialogs = Array.prototype.concat.call(Array.prototype.slice.call(document.getElementsByTagName('ons-dialog'),0), Array.prototype.slice.call(document.getElementsByTagName('ons-alert-dialog'),0));
                    var me = ev.dialog?ev.dialog:ev.alertDialog, maxZ = 1000;
                    var meElement = me;
                    dialogs.forEach(function(d){
                        if(d._visible){
                            if(d._flagged){
                                maxZ = Math.max(maxZ, parseInt(d.style.zIndex||0));
                            }
                            d._flagged = true;
                        }else{
                            d._flagged = false;
                        }
                    });
                    if(meElement){
                        meElement.style.zIndex = maxZ + 1;
                        meElement.style.position = 'absolute';
                        meElement._flagged = true;
                        if(!options.noFocus)_focusInput(meElement, options.select, options.selector);
                    } 
                });
                dialog.once('prehide', function(ev){
                    if(_dialogHandlers.open === dialog){
                        if(_dialogHandlers.onHide){
                            _dialogHandlers.onHide();
                        }
                        delete _dialogHandlers.open;
                    }
                })
                dialog.show();
            },
            isTempId: function(id){
                return id.toString().substr(0,6)=='__temp';
            },
            focusInput: _focusInput,
            sanitizeTermForRegex: _sanitizeTermForRegex,
            enforceArray: function(item){
                return item?Array.isArray(item)?item:[item]:[];
            },
            findArrayBestMatch: _findArrayBestMatch,
            translateArray: _translateArray,
            adjustLocationNumber: function(num){
                if(num.match(/^\d+$/)){
                    return 'Location '+num;
                }
                return num;
            },
            ensureTabbarIndex: _ensureTabbarIndex,

            getGpsPosition: function(success, failure){
                var timer = setTimeout(failure, 10000);
                navigator.geolocation.getCurrentPosition(function(position){
                    clearTimeout(timer);
                    success(position);
                }, function(){
                    clearTimeout(timer);
                    failure();
                })
            }
        };

        return _this;
    }]);


    angular.module('app').service('StorageService', ['$window', function($window){
        
        // Saving states between controllers
        var _states = {};
        var NEVER = -1;

        var _saveState = function(name, scope, fields, expiresMinutes){
            var expires = NEVER;
            if(expiresMinutes > 0){
                expires = new Date().getTime()+expiresMinutes*1000*60;
            }
            if(!_states[name])
                _states[name] = {data:{}};
            _states[name]['expires'] = expires;
            for(var i=0; i<fields.length; i++){
                _states[name]['data'][fields[i]] = scope[fields[i]];
            }
            _set('state_'+name, _states[name]);
        }
        var _loadState = function(name, scope, fields){
            if(typeof _states[name] == 'undefined')
                _states[name] = _get('state_'+name);
            var now = new Date().getTime();
            if(!_states[name] || (_states[name]['expires'] > 0 && _states[name]['expires'] <= now))
                return scope;
            for(var i=0; i<fields.length; i++){
                if(typeof _states[name]['data'][fields[i]] !== 'undefined')
                    scope[fields[i]] = _states[name]['data'][fields[i]];
            }
            return scope;
        }
        var _clearState = function(name){
            if(_states[name]){
                delete _states[name];
                _set('state_'+name, null);
            }
        }

        // Local storage
        var _variables = {};
        var _get = function(varname) {
            if(typeof _variables[varname] == 'undefined')
                _variables[varname] = $window.localStorage.getObject(varname);
            return (typeof _variables[varname] !== 'undefined') ? _variables[varname] : false;
         };
        var _set = function(varname, value) {
            if(_variables[varname] == undefined || _variables[varname] !== value){
                if(value != undefined){
                    _variables[varname] = value;
                }else{
                    delete _variables[varname];
                }
                $window.localStorage.setObject(varname, value);
            }     
        };
        var _clear = function(saveArray){
            _variables = {};
            _states = {};
            var save = [];
            for(var i=0; i<saveArray.length; i++){
                save[i] = $window.localStorage.getObject(saveArray[i]);
            }
            $window.localStorage.clear();
            for(var i=0; i<saveArray.length; i++){
                $window.localStorage.setObject(saveArray[i], save[i]);
            }
        };

        // ===== Return exposed objects ===== //
        return({
            state: {
                save:  _saveState,
                load: _loadState,
                clear: _clearState
            },
            get: _get,
            set: _set,
            clear: _clear
        });
    }]);

    angular.module('app').service('ModuleService', ['StorageService', function(StorageService){

        var _modules = [];
        var _getModules = function(){
            if(!_modules.length)
                _modules = StorageService.get('modules')||[];
            return _modules;
        }

        var _addModule = function(key, title, page, options){
            if(_modules.find(function(module){
                return module.key == key;
            })) return;
            _modules.push({key: key, title:title, page:page, options:options});
        }

        var _getTimesheetDetailPage = function(tab){
            console.log(tab);
        }
        var _initModules = function(permissions){
            permissions = permissions.filter(function(item, pos) {
                return permissions.indexOf(item) == pos;
            });
            _modules = [];
            for(var i=0; i<permissions.length; i++){
                switch(permissions[i]){
                    case 'Inspection':
                        _addModule(permissions[i], 'Inspect Jobs', 'job-list-host.html', {lists:['ActiveJob','FieldCompleteJob'],detailPage:'app/inspect/inspect-job.html'});
                    break;
                    case 'Completion':
                        _addModule(permissions[i], 'Complete Work', 'job-list-host.html', {lists:['ActiveJob','MyActiveJob','PendingStartJob','MyPendingStartJob'],detailPage:'app/completework/complete-work-job.html'});
                    break;
                    case 'MyTimesheets':
                        _addModule('MyTimesheets', 'My Timesheets', 'app/timesheet/timesheet-list-host.html', {lists:['MyTimesheet'], detailPage: {ForemanTimesheet:'app/timesheet/fte-work-week.html',TimeSheet:'app/timesheet/gt-work-week.html'}});
                    break;
                    case 'TimeApproval':
                    case 'TimeReview':
                        _addModule('TimeReview', 'Timesheet Review', 'app/timesheet/timesheet-list-host.html', {lists:['ForemanTimesheet','TimeSheet'], detailPage: {ForemanTimesheet:'app/timesheet/fte-work-week.html',TimeSheet:'app/timesheet/gt-work-week.html'}});
                    break;
                    case 'WorkgroupToAddJob':
                        _addModule('ManagedContracts', 'Add Jobs', 'app/addjob/managed-contracts-list.html', {});
                    break;
                    case 'Design':
                    case 'DesignApproval':
                        _addModule('DesignJobs', 'Design Jobs', 'job-list-host.html', {lists:['PreActiveJob'], detailPage:'app/designjob/design-job.html', hideTab: true});
                    break;
                    case 'Receipts':
                        _addModule('Receipts', 'Track Receipts', 'app/receipts/receipts.html', {});
                    break;
                    case 'Hotels':
                        _addModule('Hotels', 'Hotels', 'job-list-host.html', {lists:['ActiveJob','MyActiveJob','FieldCompleteJob'], detailPage:'app/hotels/hotels.html', hideTab: true});
                    break;
                    case 'ShowupSites':
                        _addModule('ShowupSites', 'Show Up Sites', 'app/showup/showup-sites.html', {});
                    break;
                    case 'DailyReport':
                        _addModule('DailyReport', 'ATC Daily Report', 'app/dailyreport/dailyreport.html', {});
                    break;
                }
            }
            StorageService.set('modules', _modules);
        }

        return {
            getModules: _getModules,
            initModules: _initModules
        };
    }]);

    angular.module('app').service('CommentDialog', ['$sce', '$timeout', 'Utils', function($sce, $timeout, Utils){

        var dialog = null;

        var _cameraOptions = {
            quality: 80,
            targetWidth: 1000,
            targetHeight: 1000,
            correctOrientation: true
        }
        var _cleanUpCamera = function(){
            if(dialog){
                dialog._scope.model.photoUrl = null;
            }
            navigator.camera.cleanup();
        };
        var _setUpCamera = function(use){
            dialog._scope.model.useCamera = use;
            dialog._scope.model.photoUrl = null;
            dialog._scope.openCamera = function(type){
                var options = Utils.copy(_cameraOptions);
                if(typeof type != 'undefined'){
                    options.sourceType = type;
                }
                navigator.camera.getPicture(function(photo){
                    dialog._scope.$apply(function(){
                        dialog._scope.model.photoUrl = photo;
                    });
                }, function(error){
                    console.log(error)
                }, options);
            };
            dialog._scope.removePhoto = function(){
                _cleanUpCamera();
            };
        };

        var _selectPreCheck = function(callback){
            if(!callback)
                return;
            return function(comment, photoUrl){
                if(dialog._scope.model.selectItems && (!dialog._scope.model.selectItems.items || !dialog._scope.model.selectItems.items.length)){
                    callback(comment, photoUrl);
                }
                callback(comment, photoUrl, dialog._scope.model.selectItems.items);
            }
        }
        var _setUpSelect = function(options){
            dialog._scope.model.selectItems = options;
            if(!options)
                return;
            if(options.callback){
                dialog._scope.accept = _selectPreCheck(dialog._scope.accept);
                dialog._scope.reject = _selectPreCheck(dialog._scope.reject);
                dialog._scope.cancel = _selectPreCheck(dialog._scope.cancel);
            }
        };

        var _applyOptions = function(options){
            //return;
            dialog._scope.model.title = options.title||'';
            dialog._scope.model.placeholder = options.placeholder||'Comment';
            dialog._scope.model.requireComment = typeof options.requireComment == 'undefined'?true:options.requireComment;
            dialog._scope.accept = options.accept||(function(){dialog.hide();});
            dialog._scope.reject = options.reject||(function(){dialog.hide();});
            dialog._scope.cancel = options.cancel||(function(){dialog.hide();});
            dialog._scope.model.comment = options.comment||'';
            dialog._scope.model.disabled = options.disabled;
            dialog._scope.model.otherHTML = options.otherHTML?$sce.trustAsHtml(options.otherHTML):'';

            dialog._scope.model.buttons = {accept: 'Accept', reject: 'Reject'};
            _setUpCamera(options.camera);
            _setUpSelect(options.select);
            if(options.button){
                var keys = Object.keys(dialog._scope.model.buttons);
                for(var i=0; i<keys.length; i++){
                    if(typeof options.button == 'string'){
                        if(keys[i] != options.button){
                            dialog._scope.model.buttons[keys[i]] = false;
                        }
                    }else{
                        if(keys[i] != options.button[0]){
                            dialog._scope.model.buttons[keys[i]] = false;
                        }else{
                            dialog._scope.model.buttons[keys[i]] = options.button[1];
                        }
                    }
                }
            }
            Utils.openDialog(dialog, {select:options.highlight});
        };

        var _show = function(options){
            _cleanUpCamera();
            if(!dialog){
                ons.createAlertDialog('comment-dialog.html').then(function(d){
                    dialog = d;
                    dialog._scope.model = {};
                    $timeout(function(){
                        _applyOptions(options);
                    });
                })
            }else{
                _applyOptions(options);
            }            
        };

        var _hide = function(options){
            if(dialog)
                dialog.hide();
        };

        return {
            show: _show,
            hide: _hide,
            cleanUpCamera: _cleanUpCamera
        };
    }]);

    angular.module('app').service('UploadDialog', ['$sce', '$timeout', 'Utils', function($sce, $timeout, Utils){

        var dialog = null;

        var _cameraOptions = {
            quality: 80,
            targetWidth: 1000,
            targetHeight: 1000,
            correctOrientation: true
        }
        var _cleanUpCamera = function(){
            if(dialog){
                dialog._scope.model.photoUrl = null;
            }
            navigator.camera.cleanup();
        };
        var _setUpCamera = function(){
            dialog._scope.openCamera = function(type){
                var options = Utils.copy(_cameraOptions);
                if(typeof type != 'undefined'){
                    options.sourceType = type;
                }
                navigator.camera.getPicture(function(photo){
                    dialog._scope.$apply(function(){
                        dialog._scope.model.photoUrl = photo;
                    });
                }, function(error){
                    console.log(error)
                }, options);
            };
            dialog._scope.removePhoto = function(){
                _cleanUpCamera();
            };
        };

        var _applyOptions = function(options){
            dialog._scope.model.title = options.title||'Upload Photo';
            dialog._scope.model.saveLabel = options.saveLabel||'Save'
            dialog._scope.model.placeholder = options.placeholder||'Description';
            dialog._scope.model.requireDescription = typeof options.requireDescription == 'undefined'?false:options.requireDescription;
            dialog._scope.model.tagged = false;
            dialog._scope.model.tagOnly = typeof options.tagOnly == 'undefined'?false:options.tagOnly;
            dialog._scope.model.noCaption = options.noCaption||false;
            dialog._scope.save = function(model){
                if(dialog._scope.model.requireDescription||dialog._scope.model.tagOnly){
                    dialog._scope.model.description = dialog._scope.model.description.trim();
                    if(!dialog._scope.model.description && dialog._scope.model.requireDescription){
                        Utils.notification.alert('Descriptions are required for each photo');
                        return (function(){});
                    }
                    if(dialog._scope.model.tagOnly && dialog._scope.model.description && !dialog._scope.model.tagged){
                        Utils.notification.alert('Photo descriptions can only be a tag value');
                        return (function(){});
                    }
                }
                if(options.save) return options.save(Utils.copy(dialog._scope.model));
                dialog.hide();
                return (function(){});
            }
            dialog._scope.cancel = options.cancel||(function(){dialog.hide();});
            dialog._scope.model.description = options.description||'';
            dialog._scope.model.otherHTML = options.otherHTML?$sce.trustAsHtml(options.otherHTML):'';
            dialog._scope.model.photoUrl = options.photoUrl||null;
            dialog._scope.model.tags = options.tags;

            if(dialog._scope.model.tags){
                var defaultTag = dialog._scope.model.tags.find(function(t){ return t.DefaultFlag });
                if(defaultTag){
                    dialog._scope.model.description = defaultTag.Text;
                }
            }
            _setUpCamera();
            Utils.openDialog(dialog);
        };

        var _show = function(options){
            _cleanUpCamera();
            if(!dialog){
                ons.createAlertDialog('upload-dialog.html').then(function(d){
                    dialog = d;
                    dialog._scope.model = {};
                    dialog._scope.chooseTag = function(tag){
                        if(dialog._scope.model.description==tag.Text){
                            if(!dialog._scope.model.requireDescription){
                                dialog._scope.model.description = '';
                            }
                        }else{
                            dialog._scope.model.description = tag.Text;
                        }
                    }
                    dialog._scope.isPhotoTagged = function(desc){
                        return dialog._scope.model.tags?dialog._scope.model.tags.find(function(t){ return desc==t.Text}):false;
                    }
                    $timeout(function(){
                        _applyOptions(options);
                    });
                })
            }else{
                _applyOptions(options);
            }

        };

        var _hide = function(options){
            if(dialog)
                dialog.hide();
        };

        return {
            show: _show,
            hide: _hide,
            cleanUpCamera: _cleanUpCamera
        };
    }]);


    angular.module('app').service('ImageModal', ['Utils', '$timeout', 'ApiService', function(Utils, $timeout, ApiService){

        var dialog = null;
        var _applyOptions = function(data, options){
            options = options || {}
            dialog._scope.model = {
                init: false,
                loading: (data.data==null || data.mime==null),
                src: '',
                caption: options.caption||'',
                editable: options.editable||false,
                callback: options.callback||null,
                loaded: options.loaded||null,
                key: options.key||null,
                showControls: true,
                editing: false,
                saving: false,
                tagged: false,
                requireDescription: options.requireDescription||false,
                tagOnly: options.tagOnly||false,
                tags: options.tags||null
            };
            dialog._scope.delete = options.delete?(function(){
                options.delete();
                _hide();
            }):false;
            $timeout(function(){
                dialog._scope.model.init = true;
                if(data.data && data.mime){
                    dialog._scope.model.src = 'data:'+data.mime+';base64,'+Utils.arrayBufferToBase64(data.data);
                }else{
                    dialog._scope.model.src = data.src||'';
                }
                dialog._scope.close = _hide;
                Utils.openDialog(dialog);
            });
        };

        var _saveResult = function(resp){
            dialog._scope.model.saving = false;
            if(resp && resp.Upload){
                dialog._scope.model.editing = false;
                dialog._scope.model.caption = resp.Upload.Description;
                if(dialog._scope.model.callback){
                    dialog._scope.model.callback(resp.Upload);
                }
            }
        }
        var _saveCaption = function(){
            var obj = { route: 'edit', key: dialog._scope.model.key, description: dialog._scope.model.caption }
            ApiService.post('upload', obj).then(_saveResult,_saveResult);
        }
        var _show = function(data, options){
            if(!dialog){
                ons.createDialog('image-viewer-modal.html').then(function(d){
                    dialog = d;
                    dialog._scope.toggleControls = function(){
                        dialog._scope.model.showControls = !dialog._scope.model.showControls;
                    }
                    dialog._scope.editCaption = function(){
                        Utils.focusInput('img-modal-edit', true);
                        dialog._scope.model.editing = true;
                    }
                    dialog._scope.saveCaption = function(){
                        if(dialog._scope.model.requireDescription||dialog._scope.model.tagOnly){
                            dialog._scope.model.caption = dialog._scope.model.caption.trim();
                            if(!dialog._scope.model.caption && dialog._scope.model.requireDescription){
                                Utils.notification.alert('Descriptions are required for each photo');
                                return;
                            }
                            if(dialog._scope.model.tagOnly && dialog._scope.model.caption && !dialog._scope.model.tagged){
                                Utils.notification.alert('Photo descriptions can only be a tag value');
                                return;
                            }
                        }
                        dialog._scope.model.saving = true;
                        _saveCaption()
                    }
                    dialog._scope.loadDone = function(){
                        dialog._scope.model.loading = false;
                        if(dialog._scope.model.loaded){
                            dialog._scope.model.loaded();
                        }
                    }
                    dialog._scope.isPhotoTagged = function(desc){
                        return dialog._scope.model.tags?dialog._scope.model.tags.find(function(t){ return desc==t.Text}):false;
                    }
                    dialog._scope.chooseTag = function(tag){
                        if(dialog._scope.model.caption==tag.Text){
                            if(!dialog._scope.model.requireDescription){
                                dialog._scope.model.caption = '';
                            }
                        }else{
                            dialog._scope.model.caption = tag.Text;
                        }
                    }
                    _applyOptions(data, options);
                })
            }else{
                _applyOptions(data, options);
            }            
        };

        var _hide = function(options){
            if(dialog){
                dialog._scope.model.src = null;
                dialog.hide();
            }
        };

        return {
            show: _show,
            hide: _hide
        };
    }]);

    angular.module('app').service('PdfModal', ['ApiService', 'Utils', 'Download', 'AuthService', function(ApiService, Utils, Download, AuthService){

        var win = null;
        var _load = function(mediaUrl, downloadUrl, filename){
            ApiService.getMedia(mediaUrl).then(function(data){
                _open(data, downloadUrl, filename);
            });
        };

        var _open = function(pdf, url, filename){
            win = cordova.InAppBrowser.open('pdfviewer/viewer.html', '_blank', 'location=no,clearCache=no,clearsessioncache=no,hidenavigationbuttons=yes,closebuttoncolor=#FFFFFF,toolbarcolor=#333333');
            function listener() {
                win.executeScript({ code: "go('"+Utils.arrayBufferToBase64(pdf)+"','"+(url?btoa(url):"")+"','"+(filename?btoa(filename):"")+"')" });
                win.removeEventListener("loadstop", listener);
            };
            win.addEventListener( "loadstop", listener);
            win.addEventListener('loadstart', function(e) {
                if(e.url.indexOf('download://') > -1) {
                    win.close();
                    win = null;
                    var mode = e.url.split('download://')[1];
                    Download.go(url, filename+'.pdf', mode, 'application/pdf').then(function(path){
                        if(mode=='ACTION_SEND'){
                            if(ons.platform.isIOS()){
                                window.plugins.socialsharing.share(null, '[UnitsPro] '+filename+'.pdf', path, null)
                            }else{
                                Download.open(path);
                            }
                        }else{
                            Download.open(path);
                        }
                    });
                }
            });
            win.addEventListener( "exit", function(){
                var count = 0;
                var setStatusBar = function(){
                    if(AuthService.isAuthenticated()){
                        document.body.style.backgroundColor = '#333';
                        StatusBar.styleLightContent();
                        if (cordova.platformId == 'android') {
                            StatusBar.backgroundColorByHexString("#333");
                        }
                    }else{
                        document.body.style.backgroundColor = '#efeff4';
                        StatusBar.styleDefault();
                        if (cordova.platformId == 'android') {
                            StatusBar.backgroundColorByHexString("#efeff4");
                        }
                    }
                    if(count < 20){
                        ++count;
                        setTimeout(setStatusBar, 50)
                    }
                }
                setTimeout(setStatusBar, 50);
            });
        };

        return {
            load: _load,
            open: _open,
            hide: function(){
                if(win){
                    win.close();
                }
            }
        };
    }]);

    angular.module('app').service('ImgOrPdf', ['ApiService', 'ImageModal', 'PdfModal', function(ApiService, ImageModal, PdfModal){

        var _load = function(mediaUrl, downloadUrl, filename){
            return ApiService.getMediaWithMime(mediaUrl).then(function(data){
                _open(data, downloadUrl, filename);
            });
        };

        var _loadWithCaption = function(mediaUrl, upload, options){
            options = options||{}
            options.caption = upload.Description;
            options.key = upload.Key;
            return ApiService.getMediaWithMime(mediaUrl).then(function(data){
                _open(data, null, null, options);
            });
        };

        var _loadOptions = function(mediaUrl, options){
            return ApiService.getMediaWithMime(mediaUrl).then(function(data){
                _open(data, null, null, options);
            });
        };

        var _open = function(data, url, filename, options){
            console.log(data)
            if(!data || !data.mime)
                return;
            if(data.mime.match('image')){
                ImageModal.show(data, options);
            }else{
                if(data.mime.match('pdf')){
                    PdfModal.open(data.data, url, filename);
                }
            }
        };

        var _hide = function(){
            ImageModal.hide();
            PdfModal.hide();
        }

        return {
            load: _load,
            loadWithCaption: _loadWithCaption,
            loadOptions: _loadOptions,
            hide: _hide
        };
    }]);

    angular.module('app').service('Download', ['$window', function($window){

        var _openMode = '';
        var _mime = '';
        var _open = function(uri){
            if(ons.platform.isIOS()){
                cordova.plugins.fileOpener2.open(uri,'application/pdf');
            }else{
                var obj = {action: $window.plugins.webintent[_openMode], url: uri, mediaStore: true};
                switch(_openMode){
                    case 'ACTION_SEND':
                        obj.type = _mime;
                        obj.chooser = true;
                        obj.extras = {}
                        delete obj.url;
                        obj.extras[$window.plugins.webintent.EXTRA_STREAM] = uri;
                    break;
                }
                $window.plugins.webintent.startActivity(
                    obj,
                    function() {},
                    function() {}
                );
            }
        };
        var _downloadManager = function(fileEntry, fileName, uri) {
            return new Promise(function(resolve, reject){
                var fileTransfer = new FileTransfer();
                var filePath = fileEntry.toURL() + fileName;
                fileTransfer.dm(
                    encodeURI(uri),
                    filePath,
                    function (entry) {
                        console.log("Successful download...");
                        resolve();
                    },
                    function (error) {
                        console.log("Error downloading...");
                        reject();
                    },
                    {filename: fileName}
                );
            });
        };
        var _download = function(fileEntry, fileName, uri) {
            return new Promise(function(resolve, reject){
                var fileTransfer = new FileTransfer();
                var filePath = fileEntry.toURL() + fileName;
                fileTransfer.download(
                    encodeURI(uri),
                    filePath,
                    function (entry) {
                        console.log("Successful download...");
                        console.log("download complete: " + entry.toURL());
                        resolve(decodeURI(entry.toURL()));
                    },
                    function (error) {
                        console.log("download error source " + error.source);
                        console.log("download error target " + error.target);
                        console.log("upload error code" + error.code);
                        reject();
                    },
                    null, // or, pass false
                    {}
                );
            })
        };
        var _getDir = function(){
            if($window.cordova.platformId=='android')
                return $window.cordova.file.externalDataDirectory;
            return $window.cordova.file.documentsDirectory;
        }
        var _go = function(url, fileName, mode, mime){
            return new Promise(function(resolve, reject){
                _openMode = mode||'';
                _mime = mime;
                $window.resolveLocalFileSystemURL(_getDir(), function (fileEntry) {
                    if($window.cordova.platformId=='android' && !_openMode){
                        _downloadManager(fileEntry, fileName, url).then(resolve,reject);
                        return;
                    }
                   _download(fileEntry, fileName, url).then(resolve,reject);
                }, function(fsError){
                    reject();
                });
            });
        }

        return {
            go: _go,
            open: _open
        };
    }]);

    angular.module('app').service('Smartpick', ['$timeout', 'Utils', function($timeout, Utils){

        var dialog = null;
        var _ok = false;
        var _options = {
            title: 'Select',
            itemTemplate: null,
            itemView: null,
            endpoint: null,
            endpointData: {},
            selectItem: null,
            minSearchLength: 3,
            filterProperties: [],
            filtersSelected: {},
            filterLimitCount: 1000,
            hasFilterSelected: false,
            extend: null,
            filters: null
        };
        var itemSelected;
        var _extend = function(options, scope, injector){
            if(options.extend){
                var keys = Object.keys(options.extend);
                for(var k=0; k<keys.length; ++k){
                    scope[keys[k]] = options.extend[keys[k]](injector, scope);
                }
                delete options.extend;
            }
            scope.options = options;
        };
        var _applyOptions = function(options, selectItem){
            var elem = angular.element(dialog._element);
            var injector = elem.injector(), scope = elem.scope();
            if(selectItem && itemSelected !== selectItem){
                itemSelected = selectItem;
            }
            if(options){
                options = Object.assign(_options, options);
                if(!options.endpoint || !options.itemTemplate || !selectItem || typeof selectItem != 'function')
                    return;
                options.selectItem = function(item){
                    itemSelected(item);
                    _hide();
                }
                _extend(options, scope, injector);
                dialog._scope.searchTerm = '';
                dialog._scope.loading = false;
                dialog._scope.items = [];
                if(dialog._scope.options.filtersData && dialog._scope.options.defaultFilters){
                    scope.defaultFilters();
                }
            }else{
                if(!_options.endpoint)
                    return;
            }
            Utils.openDialog(dialog);
            $timeout(function(){
                document.getElementById('smartpick-search-term').getElementsByTagName('input')[0].focus();
            }, 100);
        };

        var _show = function(options, selectItem){
            if(!dialog){
                ons.createDialog('app/dialog/smartpick/smartpick-search-dialog.html').then(function(d){
                    dialog = d;
                    _applyOptions(options, selectItem);
                })
            }else{
                _applyOptions(options, selectItem);
            }
            _ok = true;           
        };

        var _hide = function(options){
            if(dialog){
                dialog.hide();
            }
        };

        var _reset = function(){
            _ok = false;
            if(!dialog)
                return;
            var elem = angular.element(dialog._element);
            var scope = elem.scope();
            if(scope && scope.options){
                if(scope.options.filtersData)
                    delete scope.options.filtersData;
                if(scope.options.filtersSelected)
                    scope.options.filtersSelected = {};
                if(scope.options.filterProperties)
                    scope.options.filterProperties = [];
                scope.options.hasFilterSelected = false;
                scope.options.filtersVisible = false;
                scope.options.filterLimitCount = 1000;
            }

        }

        var _getFiltersData = function(key){
            if(!dialog)
                return;
            var elem = angular.element(dialog._element);
            var scope = elem.scope();
            if(scope.options.filtersData && scope.options.filtersData[key]){
                return scope.options.filtersData[key]
            }
        }

        var _clearFilters = function(){
            if(!dialog)
                return;
            var elem = angular.element(dialog._element);
            var scope = elem.scope();
            if(scope && scope.options){
                if(scope.options.filtersSelected)
                    scope.options.filtersSelected = {};
                scope.options.hasFilterSelected = false;
                scope.options.filtersVisible = false;
                scope.options.filterLimitCount = 1000;
            }

        }

        return {
            show: _show,
            hide: _hide,
            reset: _reset,
            getFiltersData: _getFiltersData,
            clearFilters: _clearFilters,
            ok: function(){ return _ok }
        };
    }]);

    angular.module('app').service('Rentals', ['$timeout', 'Utils', function($timeout, Utils){

        var dialog = null;
        var _ok = false;
        var _options = {
            title: 'Rental Equipment',
            id: null,
            orgId: null,
            which: null,
            noSave: false
        };
        var _extend = function(options, scope){
            var keys = Object.keys(options);
            for(var k=0; k<keys.length; ++k){
                scope[keys[k]] = options[keys[k]];
            }
        };
        var _applyOptions = function(options, selectItem){
            var elem = angular.element(dialog._element);
            var scope = elem.scope();
            if(typeof options != 'undefined'){
                options = Object.assign(_options, options);
                console.log(options)
                if(!options.id || !options.orgId || !options.which || !selectItem || typeof selectItem != 'function')
                    return;
                options.selectItem = function(item){
                    if(item.Message){
                        Utils.notification.alert(item.Message);
                        return;
                    }
                    selectItem(item);
                    _hide();
                }
                _extend(options, scope);
            }
            scope.init();
            Utils.openDialog(dialog);
            $timeout(function(){
                document.getElementById('rental-equipment-search').focus();
            }, 100);
        };

        var _reset = function(){
            if(!dialog)
                return;
            var elem = angular.element(dialog._element);
            if(elem){
                var scope = elem.scope();
                if(scope)
                    scope.reset();
            }
        }

        var _show = function(options, selectItem){
            console.log("show")
            if(!dialog){
                ons.createDialog('app/dialog/rental-equipment-dialog.html').then(function(d){
                    dialog = d;
                    _applyOptions(options, selectItem);
                })
            }else{
                _applyOptions(options, selectItem);
            } 
            _ok = true;           
        };

        var _hide = function(options){
            if(dialog && dialog.visible){
                dialog.hide();
            }
        };

        return {
            show: _show,
            hide: _hide,
            reset: _reset,
            ok: function(){ return _ok }
        };
    }]);

    angular.module('app').service('Vendors', ['$timeout', 'Utils', function($timeout, Utils){

        var dialog = null;
        var _ok = false;
        var _options = {
            title: 'Vendors'
        };
        var _extend = function(options, scope){
            var keys = Object.keys(options);
            for(var k=0; k<keys.length; ++k){
                scope[keys[k]] = options[keys[k]];
            }
        };
        var _applyOptions = function(options, selectItem){
            var elem = angular.element(dialog._element);
            var scope = elem.scope();
            if(typeof options != 'undefined'){
                options = Object.assign(_options, options);
                if(typeof selectItem != 'function')
                    return;
                options.selectItem = function(item){
                    if(item.Message){
                        Utils.notification.alert(item.Message);
                        return;
                    }
                    selectItem(item);
                    _hide();
                }
                _extend(options, scope);
            }
            scope.init();
            if(options.items){
                scope.setItems(options.items);
            }
            if(options.forceType){
                scope.set('forceType', options.forceType);
            }
            if(options.forceSubType){
                scope.set('forceSubType', options.forceSubType);
            }
            Utils.openDialog(dialog);
            $timeout(function(){
                document.getElementById('vendor-name-search').focus();
            }, 100);
        };

        var _reset = function(){
            if(!dialog)
                return;
            var elem = angular.element(dialog._element);
            if(elem){
                var scope = elem.scope();
                if(scope)
                    scope.reset();
            }
        }

        var _show = function(options, selectItem){
            if(!dialog){
                ons.createDialog('app/dialog/vendor-dialog.html').then(function(d){
                    dialog = d;
                    _applyOptions(options, selectItem);
                })
            }else{
                _applyOptions(options, selectItem);
            } 
            _ok = true;           
        };

        var _hide = function(options){
            if(dialog && dialog.visible){
                dialog.hide();
            }
        };

        return {
            show: _show,
            hide: _hide,
            reset: _reset,
            ok: function(){ return _ok }
        };
    }]);

})();

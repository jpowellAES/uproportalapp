(function(){
    'use strict';

    angular.module('app').controller('AddJobController', ['$scope', 'ApiService', 'Utils', 'Smartpick', '$timeout', function($scope, ApiService, Utils, Smartpick, $timeout) {

        $scope.loading = false;
        $scope.data = {};
        $scope.page = 0;
        var dialogs = {}
        var process = ''

        var loadNewJob = function(){
            $scope.contract = mainNav.topPage.pushedOptions.contract;
            $scope.isTimeEntry = mainNav.topPage.pushedOptions.process&&['fte','gt'].includes(mainNav.topPage.pushedOptions.process.name);
            process = $scope.isTimeEntry?mainNav.topPage.pushedOptions.process.name:''
            $scope.working = true;
            var obj = {
                action:'',
                contract: $scope.contract.ContractID
            }
            ApiService.get('addJob', obj).then(loadJobResult,loadJobResult)
        }
        var loadJobResult = function(resp){
            if(resp && resp.Job){
                cleanSmartpicks(resp);
                $scope.job = cleanJob(resp.Job);
            }else{
                Utils.notification.alert('Failed to create new job for this contract.', {title: 'Error'});
                mainNav.popPage();
            }
            $scope.working = false;
        }
        var cleanJob = function(job){
            if(job.TypeCode){
                job.JobType = $scope.data.JobType.find(function(jt){return jt.SmartCode==job.TypeCode});
            }
            if(job.DivisionRegionID){
                job.Region = $scope.data.Region.find(function(r){return r.RegionID==job.DivisionRegionID});
            }
            if(job.DivisionID){
                job.Division = $scope.data.Division.find(function(d){return d.DivisionID==job.DivisionID});
            }
            if(job.StateCountryID){
                job.Country = $scope.data.Country.find(function(c){return c.ID==job.StateCountryID});
            }
            if(job.MasterProjectID){
                job.Project = $scope.data.Project.find(function(cp){return cp.ID==job.MasterProjectID});
            }
            if(job.StateID){
                job.State = $scope.data.State.find(function(s){return s.ID==job.StateID});
            }
            return job;
        }
        var cleanSmartpicks = function(data){
            $scope.data.MyOrgID = data.MyOrgID;
            $scope.data.JobType = Utils.enforceArray(data.JobType);
            $scope.data.Region = Utils.enforceArray(data.Region);
            $scope.data.Division = Utils.enforceArray(data.Division);
            $scope.data.Country = Utils.enforceArray(data.Country);
            $scope.data.State = Utils.enforceArray(data.State);
            $scope.data.Project = Utils.enforceArray(data.Project);
        }

        $scope.navStep = function(dir){
            $scope.page += dir;
        }

        $scope.selectRegion = function($item, $model){
            delete $scope.job.Division;
            var divs = $scope.data.Division.filter(function(div){return $item.RegionID == div.DivisionRegionID});
            if(divs.length == 1){
                $scope.job.Division = divs[0];
            }
        };

        $scope.filterDivisions = function(search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                if($scope.job.Region && $scope.job.Region.RegionID == item.DivisionRegionID){
                  return item["DivisionName"].match(regex);
                }
                return false;
            };
        }

        $scope.clearPostal = function(){
            $scope.job.PostalCodePostalCode = '';
            $scope.job.PostalCodeID = '';
            $scope.job.City = '';
        }

        $scope.selectCountry = function($item, $model){
            delete $scope.job.State;
            $scope.clearPostal();
            var states = $scope.data.State.filter(function(s){return $item.ID == s.CountryID});
            if(states.length == 1){
                $scope.job.State = states[0];
            }
        };

        $scope.filterStates = function(search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                if($scope.job.Country && $scope.job.Country.ID == item.CountryID){
                  return item["Name"].match(regex);
                }
                return false;
            };
        }

        $scope.openZipSearch = function(){
            if($scope.job.PostalCodePostalCode && Smartpick.ok()){
                Smartpick.show();
            }else{
                Smartpick.show({
                    endpoint: 'postalSearch',
                    itemView: 'PostalCode',
                    itemTemplate: 'postal-code.html',
                    title: 'City / Zip',
                    filters: [
                        {key: 'Country', from: 'ID', to:'StateCountryID', display:'Name'},
                        {key: 'State', from: 'ID', to:'StateStateID', display:'Name', dependencies:{'Country':{from:'CountryID',to:'ID'}}}
                    ],
                    filtersData: {
                        Country: $scope.data.Country,
                        State: $scope.data.State
                    },
                    defaultFilters: {
                        Country: $scope.job.Country,
                        State: $scope.job.State
                    }
                }, function(item){
                    $scope.job.PostalCodePostalCode = item.PostalCode;
                    $scope.job.PostalCodeID = item.ID;
                    $scope.job.City = item.City;
                    $scope.job.Country = $scope.data.Country.find(function(c){return c.ID==item.StateCountryID});
                    $scope.job.State = $scope.data.State.find(function(s){return s.ID==item.StateStateID});
                });
            }
        }

        var errorField = function(item){
            var verb = item.select?'select':'enter', field = item.prompt?item.prompt:item.field;
            Utils.notification.alert('You must '+verb+' a value for <br><b>'+field+'</b>.', {title:'Error', callback:function(){
                if(item.id){
                    $timeout(function(){
                        document.getElementById(item.id).focus();
                    })
                }
            }});
            $scope.page = item.page;
            
        }
        var validateLocally = function(){
            var requireNotNull = [{
                field:'WorkgroupJobNumber',
                prompt:'WG Job #',
                page: 0,
                id: 'addJobWGJobNumber'
            },{
                field:'JobType',
                prompt:'Billing Type',
                select: true,
                page: 0
            },{
                field:'Region',
                select: true,
                page: 0
            },{
                field:'Division',
                select: true,
                page: 0
            }];
            for(var i=0; i<requireNotNull.length; i++){
                if(typeof $scope.job[requireNotNull[i].field] == 'undefined' || $scope.job[requireNotNull[i].field].toString().trim() == ''){
                    errorField(requireNotNull[i]);
                    return false;
                }
            }
            return true;
        }
        var getJobData = function(obj){
            obj.contract = $scope.contract.ContractID;
            obj.org = $scope.data.MyOrgID;
            obj.wgJobNum = $scope.job.WorkgroupJobNumber;
            obj.jobNum = $scope.job.NumberChar;
            obj.description = $scope.job.Description;
            obj.type = $scope.job.JobType.SmartCode;
            obj.division = $scope.job.Division.DivisionID;
            obj.useDesign = $scope.job.UseDesignFlag;
            obj.trackWeather = $scope.job.TrackWeatherFlag;
            obj.address = $scope.job.AddressLine1;
            obj.country = $scope.job.Country?$scope.job.Country.ID:undefined;
            obj.state = $scope.job.State?$scope.job.State.ID:undefined;
            obj.postal = $scope.job.PostalCodeID;
            if($scope.job.Project){
                if($scope.job.Project.ID){
                    obj.project = $scope.job.Project.ID
                }else{
                    obj.projNum = $scope.job.Project.ProjectNumber,
                    obj.projName = $scope.job.Project.ProjectName
                }
            }
            if($scope.isTimeEntry){
                obj.type = 'TE';
                obj.useDesign = false;
            }
        }
        $scope.filterMasterProjects = function(search){
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i');
            return function( item ) {
                if(item.ActiveFlag){
                  return !search||item["ProjectNumber"].match(regex) || item["ProjectName"].match(regex);
                }
                return false;
            };
        }
        $scope.addMasterProject = function(){
            dialogs.addMasterProject.open()
        }
        

        $scope.addJob = function(){
            if(!validateLocally())
                return;
            $scope.loading = true;
            var obj = $scope.isTimeEntry?{route:'addJob/save'}:{action: 'save'}
            getJobData(obj)
            ApiService.post($scope.isTimeEntry?process:'addJob', obj).then(addJobResult,addJobResult)
        }
        var addJobResult = function(resp){
            if(resp){
                var ok = false;
                if($scope.isTimeEntry){
                    if(resp.Job){
                        ok = true
                        //Let timesheet handle it
                        $scope.$parent.current.addJobResponse = resp;
                        mainNav.popPage();
                    }
                }else{
                    if(resp.job){
                        ok = true
                        //prompt to design if job has one
                        //for T&E, show it in the list on My Jobs
                        mainNav.popPage();
                    }
                }
                if(!ok && (resp.error||(resp.data&&resp.data.message))){
                    Utils.notification.alert(resp.error||resp.data.message, {title:'Error', callback:function(){
                        if(resp.id){
                            $timeout(function(){
                                document.getElementById(resp.id).focus();
                            })
                        }
                    }});
                    if(typeof resp.screen != 'undefined'){
                        $scope.page = resp.screen;
                    }
                }
            }
            $scope.loading = false;
        }

        $timeout(loadNewJob);

        ons.createAlertDialog('app/addjob/add-master-project-dialog.html').then(function(dialog){
            dialog.open = function(){
                dialog._scope.data = {
                    number: '',
                    name: ''
                }
                Utils.openDialog(dialog);
            };
            dialog._scope.resetMasterProject = function(){
                dialog._scope.data.error = '';
                delete dialog._scope.data.existing;
            }
            dialog._scope.done = function(save){
                if(save){
                    dialog._scope.data.number = dialog._scope.data.number.trim()
                    dialog._scope.data.name = dialog._scope.data.name.trim()
                    if(!dialog._scope.data.number||!dialog._scope.data.name){
                        dialog._scope.data.error = 'Enter a number and name.'
                        return;
                    }
                    var proj = dialog._scope.data;
                    var existing = $scope.data.Project.find(function(pr){ return pr.ProjectNumber.toLowerCase()==proj.number.toLowerCase()||pr.ProjectName.toLowerCase()==proj.name.toLowerCase()})
                    if(existing){
                        if(!proj.existing){
                            proj.existing = existing
                            dialog._scope.data.error = 'A project exists with this number or name. Press [Add] again to use that project.'
                            return;
                        }else{
                            proj.existing.ActiveFlag = true
                            $scope.job.Project = proj.existing
                        }
                    }else{
                        proj = {
                            ActiveFlag: true,
                            ProjectNumber: proj.number,
                            ProjectName: proj.name
                        }
                        $scope.data.Project.push(proj);
                        $scope.job.Project = proj;
                    }
                }
                dialog.hide();
            }
            dialogs.addMasterProject = dialog;
        });

        $scope.$on('$destroy', function(){
            var dialogKeys = Object.keys(dialogs);
            for(var k=0; k<dialogKeys.length; ++k){
                var dialog = dialogs[dialogKeys[k]];
                if(dialog.visible){
                    dialog.hide();
                }
            }
            Smartpick.reset();
            Smartpick.hide();
        });
    }]);

})();
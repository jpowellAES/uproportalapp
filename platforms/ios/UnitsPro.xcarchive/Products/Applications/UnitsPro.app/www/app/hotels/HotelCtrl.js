(function(){
    'use strict';

    angular.module('app').controller('HotelController', ['$scope', '$timeout', 'ApiService', 'Utils', 'Vendors', 'Smartpick', 'UploadDialog', 'ImgOrPdf', function($scope, $timeout, ApiService, Utils, Vendors, Smartpick, UploadDialog, ImgOrPdf) {
        $scope.working = true;
        $scope.job = $scope.$parent.current.job;
        $scope.day = $scope.$parent.current.day;
        $scope.hotel = $scope.$parent.current.hotel;
        $scope.data = $scope.$parent.current.data;
        $scope.parent = $scope.$parent.current.parent;
        $scope.loading = false
        var date = Utils.date($scope.day.JobDate,'MM/dd/yyyy')

        $scope.filterRooms = function(){
            return function(item){
                return item.active;
            }
        }

        $scope.findHotel = function(){
            Vendors.show({
                items:$scope.data.Vendor, 
                forceType:{Code:'VE',Meaning:'Vendor'}, 
                forceSubType:{Code:'HT',Meaning:'Hotel'}, 
                title:'Hotels'
            }, function(item){
                $scope.parent.changed($scope.day);
                $scope.hotel.VendorName = item.ReportName,
                $scope.hotel.VendorID = item.OrganizationID
            })
        }

        $scope.deleteHotel = function(){
            Utils.notification.confirm("Delete "+$scope.hotel.VendorName+" for "+Utils.date($scope.day.JobDate,'M/dd')+'?', {
              callback: function(ok){
                if(ok == 0){
                    $scope.parent.tempPhotosToDelete = []
                    $scope.parent.deleteHotel($scope.hotel);
                    $scope.parent.deleteTempPhotos();
                    mainNav.popPage();
                }
              },
              buttonLabels: ["Delete","Cancel"]
            });
        }

        var uploadResult = function(resp, title, obj){
            $scope.loading = false;
            if(resp && (resp.Key||resp.success)){
                var which = obj.room?'Room':'Hotel';
                if(which == 'Hotel'){
                    $scope.hotel.HasReceipt = true;
                    if($scope.parent.isTempID($scope.hotel.ID)){
                        $scope.hotel.tempPhotoKey = resp.Key;
                    }
                }else if(which == 'Room'){
                    var room = $scope.hotel.HotelRoom.find(function(r){ return (r.ID&&r.ID==obj.room)||(r.temp&&r.temp==obj.room)})
                    if(room){
                        room.HasReceipt = true;
                        if($scope.parent.isTempID(room.ID)){
                            room.tempPhotoKey = resp.Key;
                        }
                    }
                }
            }
        }
        var showPhotoUpload = function(callback, title, obj){
            obj.route = 'photo'
            UploadDialog.show({noCaption:true, save: function(data){
                ApiService.getFormData(obj, {photo:data.photoUrl}).then(function(formdata){
                    $scope.loading = true;
                    var done = function(resp){callback(resp,title,obj)};
                    ApiService.post('hotels', obj, '', '', formdata)
                    .then(done, done);
                });
                UploadDialog.hide();
            }});
        }
        $scope.hotelPhoto = function(){
            var obj = {job:$scope.job.ID, hotel:$scope.hotel.ID}
            if(!$scope.hotel.HasReceipt){
                showPhotoUpload(uploadResult, 'Upload Receipt', obj);
            }else{
                var key = $scope.job.ID+'|'+$scope.hotel.ID;
                var url = 'hotels/'+encodeURIComponent(key);
                if($scope.parent.isTempID($scope.hotel.ID)){
                    key = $scope.hotel.tempPhotoKey;
                    url = 'upload/'+key;
                }
                $scope.loading = true;
                ImgOrPdf.loadOptions(url, {delete:function(){
                    var done = function(resp){
                        $scope.loading = false;
                        if(resp && resp.success){
                            if($scope.hotel.tempPhotoKey){
                                delete $scope.hotel.tempPhotoKey;
                            }
                            $scope.hotel.HasReceipt = false;
                        }
                    }
                    $scope.loading = true;
                    if(!$scope.parent.isTempID($scope.hotel.ID)){
                        var obj = {route:'photo',delete:true,job:$scope.job.ID,hotel:$scope.hotel.ID}
                        ApiService.post('hotels', obj)
                        .then(done, done);
                    }else{
                        var obj = {route:'deleteTempPhotos',keys:$scope.hotel.tempPhotoKey}
                        ApiService.post('hotels', obj).then(done, done);
                    }
                }}).finally(function(){
                    $scope.loading = false;
                })
            }
        }

        $scope.viewCrew = function(){
            $scope.parent.viewCrew($scope.hotel, $scope);
        }

        var closeRooms = function(){
            for(var i=0; i<$scope.hotel.HotelRoom.length; ++i){
                $scope.hotel.HotelRoom[i].open = false;
                $scope.hotel.HotelRoom[i].addingOccupants = false;
            }
        }
        $scope.addRoom = function(noFocus){
            var currentRoom = $scope.hotel.HotelRoom.find(function(r){ return r.active&&r.open })
            if(currentRoom && !$scope.validateRoom(currentRoom)){
                return;
            }
            closeRooms();
            var room = {
                open: true,
                active: true,
                ID: $scope.parent.getTempID(),
                HasReceipt: false,
                OccupantCount: 0,
                HotelRoomOccupant: []
            }
            $scope.hotel.HotelRoom.push(room);
            $scope.toggleOccupantMode(room);
            if(!noFocus){
                $timeout(function(){
                    Utils.focusInput(document.querySelector('.hotel-room.open'));
                })
            }
        }

        $scope.expandRoom = function(room){
            var currentRoom = $scope.hotel.HotelRoom.find(function(r){ return r.active&&r.open })
            if(currentRoom && !$scope.validateRoom(currentRoom)){
                return;
            }
            var open = room.open;
            closeRooms();
            if(!open){
                room.open = true;
                if(!room.OccupantCount){
                    $scope.toggleOccupantMode(room);
                }
            }
        }
        $scope.roomPhoto = function(room){
            var obj = {job:$scope.job.ID, hotel:$scope.hotel.ID, room:room.ID}
            if(!room.HasReceipt){
                showPhotoUpload(uploadResult, 'Upload Receipt', obj);
            }else{
                var url = 'hotels/'+encodeURIComponent($scope.job.ID+'|'+$scope.hotel.ID+'|'+room.ID);
                if($scope.parent.isTempID(room.ID)){
                    url = 'upload/'+room.tempPhotoKey;
                }
                $scope.loading = true;
                ImgOrPdf.loadOptions(url, {delete:function(){
                    var done = function(resp){
                        $scope.loading = false;
                        if(resp && resp.success){
                            if(room.tempPhotoKey){
                                delete room.tempPhotoKey;
                            }
                            room.HasReceipt = false;
                        }
                    }
                    $scope.loading = true;
                    if(!$scope.parent.isTempID(room.ID)){
                        var obj = {route:'photo',delete:true,job:$scope.job.ID,hotel:$scope.hotel.ID,room:room.ID}
                        ApiService.post('hotels', obj)
                        .then(done, done);
                    }else{
                        var obj = {route:'deleteTempPhotos',keys:room.tempPhotoKey}
                        ApiService.post('hotels', obj)
                        .then(done, done);
                    }
                }}).finally(function(){
                    $scope.loading = false;
                });
            }
        }
        
        $scope.deleteRoom = function(room){
            Utils.notification.confirm("Delete "+(room.RoomNumber?('room '+room.RoomNumber):'this room')+'?', {
              callback: function(ok){
                if(ok == 0){
                    $scope.$apply(function(){
                        $scope.parent.tempPhotosToDelete = []
                        $scope.parent.deleteRoom(room, $scope.hotel, $scope);
                        $scope.parent.deleteTempPhotos()
                        totalHotel();
                    })
                }
              },
              buttonLabels: ["Delete","Cancel"]
            });
        }

        $scope.removeOccupant = function(room, occupant){
            $scope.parent.removeOccupant(room, occupant, $scope.hotel, $scope)
        }

        $scope.addOccupant = function(room, occupant){
            $scope.parent.addOccupant(room, occupant, $scope.hotel, $scope)
        }

        var sortOccupants = Utils.multiSort({name:'isMyCrew',reverse:true},'Name');
        $scope.toggleOccupantMode = function(room){
            room.addingOccupants = true;
            $scope.availableOccupants = []
            for(var i=0; i<$scope.parent.crew.length; ++i){
                if((!$scope.parent.crew[i].rooms || !$scope.parent.crew[i].rooms[date]) && !$scope.day.HotelExclusion.find(function(e){ return e.active && e.PersonID == $scope.parent.crew[i].ID})){
                    $scope.availableOccupants.push({
                        ID: $scope.parent.crew[i].ID,
                        Name: $scope.parent.crew[i].Name,
                        isMyCrew: $scope.parent.crew[i].isMyCrew
                    });
                }
            }
            $scope.availableOccupants.sort(sortOccupants);
        }
        $scope.blur = function(){
            Utils.hideKeyboard(document.activeElement);
        }
        $scope.changeHotel = function(){
            $scope.parent.changed($scope.day);
        }
        var totalHotel = function(){
            var total = 0
            for(var i=0; i<$scope.hotel.HotelRoom.length; ++i){
                if($scope.hotel.HotelRoom[i].active){
                    total += $scope.hotel.HotelRoom[i].Cost||0;
                }
            }
            $scope.hotel.Cost = total;
        }
        $scope.changeRoom = function(room){
            $scope.parent.changed($scope.day);
        }
        $scope.changeCost = function(room){
            totalHotel();
            $scope.changeRoom(room);
        }

        var errorField = function(item, within){
            within = within||document;
            var verb = item.select?'select':'enter', field = item.prompt?item.prompt:item.field;
            Utils.notification.alert('You must '+verb+' a value for <br><b>'+field+'</b>.', {title:'Error', callback:function(){
                if(item.query){
                    $timeout(function(){
                        within.querySelector(item.query).focus();
                    })
                }
            }});            
        }
        $scope.validateRoom = function(room){
            var requireNotNull = [
            {
                field:'RoomNumber',
                prompt:'Room #',
                query: '.hotel-room-number'
            }];
            for(var i=0; i<requireNotNull.length; i++){
                if(typeof room[requireNotNull[i].field] == 'undefined' || room[requireNotNull[i].field].toString().trim() == ''){
                    errorField(requireNotNull[i], document.getElementById('room_'+room.ID));
                    return false;
                }
            }
            return true;
        }
        $scope.validateHotel = function(){
            var requireNotNull = [
            {
                field:'City',
                prompt:'City',
                query: '#hotel-city'
            }];
            for(var i=0; i<requireNotNull.length; i++){
                if(typeof $scope.hotel[requireNotNull[i].field] == 'undefined' || $scope.hotel[requireNotNull[i].field].toString().trim() == ''){
                    errorField(requireNotNull[i]);
                    return false;
                }
            }
            for(var i=0; i<$scope.hotel.HotelRoom.length; ++i){
                if($scope.hotel.HotelRoom[i].active){
                    if(!$scope.validateRoom($scope.hotel.HotelRoom[i])){
                        return false;
                    }
                }
            }
            return true;
        }

        var init = function(){
            if($scope.parent.isTempID($scope.hotel.ID) && !$scope.hotel.HotelRoom.find(function(r){ return r.active })){
                $scope.addRoom(true);
            }
        }

        $scope.close = function(){
            if($scope.day.changed){
                if(!$scope.validateHotel()){
                    return;
                }
            }
            mainNav.popPage();
        }

        var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', $scope.close, false);
            }else{
                document.removeEventListener('backbutton', $scope.close);
                ons.enableDeviceBackButtonHandler();
            }
          }

          $scope.hidePage = function(){
            toggleBackButton(false)
            Utils.clearBackButtonHandlers()
          }
          $scope.showPage = function(){
            if(!$scope.hotel.City)Utils.focusInput('hotel-city');
            toggleBackButton(true)
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })

            ons.findComponent('#hotel-back-button')._element[0].onClick = $scope.close;
          }

        $scope.$on('$destroy', function(){
            Smartpick.hide();
            Vendors.hide();
            UploadDialog.hide();
        });

        $timeout(init);
    }]);

})();
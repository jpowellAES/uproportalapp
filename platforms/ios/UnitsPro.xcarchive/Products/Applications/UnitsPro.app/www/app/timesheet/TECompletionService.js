(function(){
    'use strict';

    angular.module('app').service('TECompletionService', ['ApiService', 'Utils', 'StorageService', function(ApiService, Utils, StorageService){
        

        var loadDispatchJob = function(data, done, force, position){
            if(!force && data.jobs[0].Map){
                Utils.getGpsPosition(function(position){
                    loadDispatchJob(data, done, true, position)
                }, function(){
                    loadDispatchJob(data, done, true)
                })
                return;
            }
            var obj = {job: data.jobs[0].JobID, date: data.date};
            if(position){
                obj.lat = position.coords.latitude
                obj.lon = position.coords.longitude
            }
            obj.dispatch = data.jobs[0].DispatchJobID;
            obj.action = 'open'
            obj.date = data.date
            obj.jdc = data.jobs[0].JobDayCrewID;
            ApiService.get('dispatch', obj).then(done, done)
        }

        var _loadJob = function(data, $scope, loadDone){
          var done = function(resp){
            if(typeof loadDone == 'function') loadDone();
            if(data.jobs[0].DispatchJobID){
              _dispatchJobResult(resp, data, $scope)
            }else{
              _completionJobResult(resp, data, $scope);
            }
            Utils.notification.hideToast();
          }
          if(data.jobs[0].DispatchJobID){
            loadDispatchJob(data, done);
          }else{
            var obj = {job: data.jobs[0].JobID, date: data.date};
            obj.getDesign = true;
            ApiService.get('jobCompletion', obj).then(done, done)
          }
        }
        var _saveLastProcess = function(data){
          var saveData = Utils.copy(data);
          var minutes = 60 * 8 //eight hours
          saveData.expires = new Date().getTime()+(minutes*1000*60);
          StorageService.set('lastEndDayCompletionProcess', saveData);
        }
        var _completionJobResult = function(resp, data, $scope){
              if(resp && resp.JobDesign){
                _saveLastProcess(data);
                $scope.current.job = data.jobs.shift();
                $scope.current.job.design = cleanCompletionResponse(resp.JobDesign);
                $scope.current.job.design.lastLocationFetch = new Date().getTime();
                $scope.current.job.design.process = {name:'EndDay', date:data.date, jobs: data.jobs, timesheet:data.timesheet, type:'completion', fromGeneralTime:data.fromGeneralTime};
                _openPage('app/completework/complete-work-job.html');
              }else{
                  if(resp && resp.locked && resp.message){
                      Utils.notification.alert(resp.message+" Cannot complete work at this time.", {
                          title: "Job Locked",
                      });
                  }else{
                      Utils.notification.alert("Failed to load job data", {title: "Error"});
                  }
              }
        };
        var cleanCompletionResponse = function(design){
          design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
          if(!design.Location)
              design.Location = [];
          if(!Array.isArray(design.Location))
              design.Location = [design.Location];
          design.Location.map(cleanCompletionLocation);
          design.PercentComplete = Utils.fixPercent(design.PercentComplete);
          return design;
        };
        var cleanCompletionLocation = function(location){
          location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
          location.PercentComplete = Utils.fixPercent(location.PercentComplete);
          return location;
        };

        var _openPage = function(page){
          if(mainNav.pages.length < 2){
            mainNav.pushPage(page);
          }else{
            mainNav.replacePage(page);
          }
        }

        var _loadShowUpSites = function(data, $scope){
          Utils.notification.hideToast();
          _saveLastProcess(data);
          $scope.current.job = {ID:data.jobs.join(',')};
          $scope.current.job.process = {name:'EndDay', timesheet:data.timesheet, jobs:[], date: data.date, type:'showUpSites', fromGeneralTime: data.fromGeneralTime};
          _openPage('app/showup/job-sites.html');
        }

        var _dispatchJobResult = function(resp, data, $scope){
              if(resp && resp.DispatchJob){
                _saveLastProcess(data);
                $scope.current.job = data.jobs.shift();
                $scope.current.job.dispatch = resp.DispatchJob;
                $scope.current.job.dispatch.process = {name:'EndDay', date:data.date, jobs: data.jobs, timesheet:data.timesheet, type:'dispatch', fromGeneralTime:data.fromGeneralTime};
                _openPage('app/dispatch/dispatch-job.html');
              }else{
                  if(resp && resp.locked && resp.message){
                      Utils.notification.alert(resp.message+" Cannot complete work at this time.", {
                          title: "Job Locked",
                      });
                  }else{
                      Utils.notification.alert("Failed to load job data", {title: "Error"});
                  }
              }
        };

        var _processComplete = function(){
          StorageService.set('lastEndDayCompletionProcess');
        }

        var _checkNextJob = function($scope, process, loadDone){
          if(process.jobs.length){
              Utils.notification.toast('Loading job ('+process.jobs[0].WorkgroupJobNumber+') ...');
              _loadJob({jobs:process.jobs, date:process.date, timesheet: process.timesheet}, $scope, loadDone);
          }else{
              _processComplete();
              if(process.type == 'showUpSites'){
                if(mainNav.pages.length < 2 || mainNav.pages[mainNav.pages.length-2].pushedOptions.page != 'app/timesheet/fte-work-week.html'){
                  $scope.current.timesheet = process.timesheet;
                  $scope.current.nextAction = 'fromDayShowUpSites'
                  $scope.current.date = process.date;
                  $scope.current.fromGeneralTime = process.fromGeneralTime;
                  mainNav.replacePage('app/timesheet/fte-work-week.html');
                }else{
                  $scope.$parent.current.showupSitesDone = true;
                  $scope.$parent.current.fromGeneralTime = process.fromGeneralTime;
                  mainNav.replacePage('app/timesheet/fte-work-day.html');
                }
              }else{
                if(mainNav.pages.length < 2 || mainNav.pages[mainNav.pages.length-2].pushedOptions.page != 'app/timesheet/fte-work-week.html'){
                  $scope.current.timesheet = process.timesheet;
                  $scope.current.nextAction = 'fromDayCompleteWork'
                  $scope.current.date = process.date;
                  $scope.current.fromGeneralTime = process.fromGeneralTime;
                  mainNav.replacePage('app/timesheet/fte-work-week.html');
                }else{
                  $scope.$parent.current.completionDone = true;
                  $scope.$parent.current.fromGeneralTime = process.fromGeneralTime;
                  mainNav.replacePage('app/timesheet/fte-work-day.html');
                }
              }
          }
        }

        var _resumeProcess = function(scope, job, fallback, loadStart, loadDone){
          var lastProcess = StorageService.get('lastEndDayCompletionProcess');
          var now = new Date().getTime();
          var index;
          if(lastProcess && (!lastProcess.expires || lastProcess.expires > now) && (index=lastProcess.jobs.findIndex(function(j){ return j.JobID == job.JobID})) > -1){
            Utils.notification.confirm('You were previously entering time on <b>'+lastProcess.date+'</b> for <b>'+lastProcess.timesheet.title+'</b>.',{
                title: 'Resume?',
                buttonLabels: ['No thanks', 'Resume'],
                cancelable: true,
                callback: function(button){
                    if(button == 1){
                        _loadJob({timesheet: lastProcess.timesheet, date: lastProcess.date, jobs: lastProcess.jobs.splice(index, 1)}, scope, loadDone);
                        if(typeof loadStart == 'function'){
                          loadStart();
                        }
                    }else if(button > -1){
                      _processComplete();
                      if(typeof fallback == 'function'){
                        fallback();
                      }
                    }
                }
            })
          }else{
            if(typeof fallback == 'function'){
              fallback();
            }
          }
        }

        return {
          loadJob: _loadJob,
          processComplete: _processComplete,
          checkNextJob: _checkNextJob,
          resumeProcess: _resumeProcess,
          loadShowUpSites: _loadShowUpSites
        };

    }]);


})();

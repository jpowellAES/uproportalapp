(function(){
    'use strict';

    angular.module('app').controller('ShowupSitesController', ['$scope', '$timeout', 'ApiService', 'StorageService', '$rootScope', 'Utils', 'Smartpick', function($scope, $timeout, ApiService, StorageService, $rootScope, Utils, Smartpick) {
        $scope.working = true;
        $scope.module = $scope.$parent.current.module;
        $scope.title = $scope.module.title;
        $scope.current = $scope.$parent.current;

        $scope.opening = false
        $scope.tabs = []
        var tabIndices = {}


        var jobSiteLookup = {}
        var compareProperties = ['JobShowUpSiteID','Name','LocationVerified','Longitude','Latitude','Address','City','StateCode','PostalCode','InexactAddressMatch'];

        $scope.openSite = function(site){
            var tempJob = Utils.copy(site.job);
            tempJob.Site = Utils.copy(jobSiteLookup[site.job.ID]);
            for(var i=0; i<tempJob.Site.length; ++i){
                if(tempJob.Site[i].JobShowUpSiteID == site.JobShowUpSiteID){
                    tempJob.Site[i].selected = true;
                    break;
                }
            }
            $scope.current.job = tempJob;
            $scope.current.updateSites = function(sites){
                for(var i=0; i<sites.length; ++i){
                    var existing = findSite(sites[i].JobShowUpSiteID);
                    if(existing){
                        if(existing.site.LocationVerified != sites[i].LocationVerified){
                            var oldTab = getTab(existing.site);
                            oldTab.sites.splice(existing.index, 1);
                            existing.site.LocationVerified = sites[i].LocationVerified;
                            var newTab = getTab(existing.site);
                            newTab.sites.push(existing.site);
                            newTab.sites.sort(function(a, b){
                                if (a.job.WorkgroupJobNumber < b.job.WorkgroupJobNumber) {
                                    return -1;
                                }
                                if (a.job.WorkgroupJobNumber > b.job.WorkgroupJobNumber) {
                                    return 1;
                                }
                                if (a.Name < b.Name) {
                                    return -1;
                                }
                                if (a.Name > b.Name) {
                                    return 1;
                                }
                                return 0;
                            });
                            sortTabs();
                        }
                        for(var j=0; j<compareProperties.length; ++j){
                            existing.site[compareProperties[j]] = sites[i][compareProperties[j]]
                        }
                    }
                }
            }
            mainNav.pushPage('app/showup/job-sites.html');
        }

        var findSite = function(id){
            for(var t=0; t<$scope.tabs.length; ++t){
                for(var s=0; s<$scope.tabs[t].sites.length; ++s){
                    if($scope.tabs[t].sites[s].JobShowUpSiteID == id){
                        return {site:$scope.tabs[t].sites[s], index: s};
                    }
                }
            }
        }
        var getTab = function(site){
            var title = site.LocationVerified?'Confirmed':'Unconfirmed';
            if(typeof tabIndices[title] == 'undefined'){
                tabIndices[title] = $scope.tabs.length;
                $scope.tabs.push({
                    title: title,
                    sites: []
                })
            }
            return $scope.tabs[tabIndices[title]];
        }
        var addSite = function(site, job){
            var tab = getTab(site);
            site.job = job;
            tab.sites.push(site);
        }
        var sortTabs = function(){
            $scope.tabs.sort(function(a, b){
                if (a.title < b.title) {
                    return 1;
                }
                if (a.title > b.title) {
                    return -1;
                }
                return 0;
            });
            for(var i=0; i<$scope.tabs.length; ++i){
                tabIndices[$scope.tabs[i].title] = i;
            }
        }
        var result = function(resp){
            if(resp){
                if(resp.Job){
                    resp.Job = Utils.enforceArray(resp.Job);
                    $scope.hasSites = resp.Job.length>0;
                    for(var i=0; i<resp.Job.length; ++i){
                        var job = Utils.pick(resp.Job[i],'ID','NumberChar','Description','WorkgroupJobNumber');
                        var sites = Utils.enforceArray(resp.Job[i].Site);
                        jobSiteLookup[job.ID] = sites;
                        for(var j=0; j<sites.length; ++j){
                            addSite(sites[j], job);
                        }
                    }
                }
            }
            sortTabs();
            $scope.working = false;
            $scope.opening = false;
        }
        var loadSites = function(){
            var obj = {
                route: 'sites'
            };
            ApiService.get('showup', obj).then(result, result);
        };

        var init = function(){
            $scope.loaded = true;
        };

        loadSites();
        $timeout(init);

    }]);

})();
(function(){
    'use strict';

    angular.module('app').controller('GTWorkWeekController', ['$scope', 'ApiService', 'Utils', '$rootScope', '$timeout', '$anchorScroll', 'TimesheetService', '$sce', 'CommentDialog', function($scope, ApiService, Utils, $rootScope, $timeout, $anchorScroll, TimesheetService, $sce, CommentDialog) {
        
        $scope.timesheet = $scope.$parent.current.timesheet;
        $scope.current = $scope.$parent.current;
        var share = {dialogs:{}}
        $scope.$parent.current.gtShare = share

        $scope.callbacks = {}

        var workWeekResult = function(resp, setData){
            if(resp && resp.WorkWeek){
                if(setData){
                    var keys = Object.keys(setData);
                    for(var i=0; i<keys.length; ++i){
                        resp.WorkWeek[keys[i]] = setData[keys[i]];
                    }
                }
                $scope.current.workweek = $scope.workweek = resp.WorkWeek;
                TimesheetService.cleanWorkweek($scope.workweek, $scope);
            }
            $scope.working = false;
        }
        var loadWorkWeek = function(callback, setData){
            var done = function(resp){
                Utils.notification.hideModal();
                workWeekResult(resp, setData);
                if(callback){
                    callback($scope.workweek);
                }
            }
            $scope.working = true;
            var obj = {};
            obj.workweek = $scope.timesheet.WorkWeekID;
            obj.route = 'open';
            ApiService.get('gt', obj).then(done, done);
        }

        share.reload = function(callback, setData){
            Utils.notification.modal('Reloading timesheet...');
            loadWorkWeek(callback, setData);
        }

        share.setCallbacks = function(scope){
            $scope.callbacks.gtReviewCallback = gtReviewCallback;
            $scope.callbacks.gtReportsDone = gtReportsDone;
            $scope.callbacks.gtHistoryResult = gtHistoryResult;
            $scope.callbacks.gtSaveCallback = gtSaveCallback;
            if(scope){
                scope.callbacks.gtReviewCallback = gtReviewCallback;
                scope.callbacks.gtReportsDone = gtReportsDone;
                scope.callbacks.gtHistoryResult = gtHistoryResult;
                scope.callbacks.gtSaveCallback = gtSaveCallback;
            }
        }

        $scope.changeTimeWorkWeek = function(){
            $scope.workweek.changes = true;
            $scope.workweek.Code = $scope.workweek.timeWorkWeek?$scope.workweek.timeWorkWeek.SmartCode:'';
            TimesheetService.setUpWorkWeekDays($scope.workweek);
        }

        $scope.mapBack = function(obj, name, spMapName){
            spMapName = spMapName||name;
            TimesheetService.mapSmartpickBack($scope.workweek, obj, name, spMapName);
        }

        $scope.loadingDialog = function(){
            //Nothing?
        }

        $scope.getBucketDate = function(bucket, which){
            if(bucket.IntervalCode == 'WE'){
                return 'This Week';
            }
            if(bucket.IntervalCode == 'MN'){
                return Utils.date(bucket[which+'End'], 'MMMM')
            }
            if(bucket.IntervalCode == 'AN'){
                return Utils.date(bucket[which+'End'], 'yyyy')
            }
        }

        share.getStatusClass = function(code){
            switch(code){
                case 'SB':
                case 'Submitted':
                    return 'timesheet-blue';
                break;
                case 'RJ':
                case 'Rejected':
                case 'IP':
                case 'In Process':
                    return 'timesheet-red';
                break;
                case 'ZH':
                case 'Zero Hours':
                case 'AP':
                case 'Approved':
                    return 'timesheet-black';
                break;
                case 'RV':
                case 'Reviewed':
                    return 'timesheet-green';
                break;
                case 'PE':
                case 'Pending':
                    return 'timesheet-gray';
            }
            return '';
        }
        $scope.getStatusClass = share.getStatusClass
        share.getEditable = function(){
            if(!$scope.workweek) return false;
            if(!['PE','IP','RJ','ZH'].includes($scope.workweek.StatusCode) || !$scope.workweek.AllowEdit){
                return false;
            }
            return true
        }
        $scope.getEditable = share.getEditable

        share.dayOpened = function(){
            $scope.loading = false;
        }
        share.getOriginal = function(){
            return $scope.original;
        }
        $scope.changeDay = function($index){
            if($scope.loading) return;
            $scope.loading = true;
            $scope.workweek.currentDay = $index;
            $scope.current.gtWorkweek = $scope.workweek;
            $scope.current.callbacks = $scope.callbacks;
            mainNav.pushPage('app/timesheet/gt-work-day.html');
        }


        var gtReviewCallback = function(data){
            if(data && data.data){
                if(!data.data.success){
                    data = data.data.message||'Failed to save timesheet';
                }
            }
            if(data && data != 1 && typeof data == 'string'){
                share.dialogs.alert.open({
                    title: 'Error',
                    html: data,
                    buttons: [{
                        label: 'Ok'
                    }],
                    cancelable: true
                });
                TimesheetService.revertReview($scope.workweek);
                $scope.original = Utils.copy($scope.workweek);
            }else{
                share.updateTimesheetStatus()
            }
        }
        var openReviewComment = function(scope, code, force, dayIndex){
            var dialogOptions = {
                title: 'Comment',
                accept: function(comment){
                    share.review(scope, code, force, dayIndex, comment);
                    CommentDialog.hide();
                },
                requireComment: code=='RJ',
                placeholder: 'Enter a comment',
                button: ['accept',TimesheetService.statusMeanings[code]]
            };
            CommentDialog.show(dialogOptions);
        }
        share.review = function(scope, code, force, dayIndex, comment){
            if(code == 'SB'){
                if(TimesheetService.checkRequiredComms(scope.workweek, dayIndex)){
                    share.dialogs.alert.open({
                        title: "Review Safety Communications",
                        content: "One or more safety communications are required. You must review them before submitting the timesheet.",
                        buttons: [{
                            label: 'Ok',
                            callback: function(){
                                $timeout(function(){
                                    if(typeof dayIndex != 'undefined') scope.workweek.WorkDay[dayIndex].showSafety = true;
                                });
                            }
                        }],
                        cancelable: true
                    });
                    return;
                }
            }
            if((code == 'SB' && scope.workweek.StatusCode == 'RJ') || code == 'RJ'){
                if(typeof comment == 'undefined' && typeof dayIndex == 'undefined'){
                    openReviewComment(scope, code, force, dayIndex);
                    return;
                }
            }
            var result = TimesheetService.review(scope.workweek, code, force, dayIndex, comment);
            if(result.message){
                share.dialogs.alert.open({
                    title: result.title,
                    content: result.message,
                    buttons: result.buttons||[{
                        label: 'Cancel'
                    },{
                        label: 'Submit Anyway',
                        callback: function(){ share.review(scope, code, true, dayIndex, comment); }
                    }],
                    cancelable: true
                });
            }else{
                share.save(scope, 'timesheet/saveAndReview', '', 'gtReviewCallback');
            }
        }

        $scope.reviewAll = function(code){
            share.review($scope, code);
        }

        var gtReportsDone = function(resp){
            Utils.notification.hideToast();
            if(!resp || !resp.Reports || !resp.Reports.length){
                Utils.notification.alert('No reports available for this item.');
            }else{
                share.dialogs.reports.open(resp.Reports);
            }
        }
        $scope.reports = function(){
            TimesheetService.load($scope, 'timesheet/reports', 'GET', {workweek: $scope.workweek.ID}, 'gtReportsDone');
            Utils.notification.toast('Loading reports...');
        }

        var gtHistoryResult = function(data){
            data = data||{};
            $scope.workweek.statusHistory = Utils.enforceArray(data.StatusHistory).map(function(status){
                status.CreationDateTime = Utils.date(status.CreationDateTime);
                return status;
            });
            share.dialogs.statusHistory.open();
        }
        $scope.viewHistory = function(){
            if($scope.loading) return;
            TimesheetService.load($scope, 'timesheet/reviewHistory', 'GET', {workweek:$scope.workweek.ID}, 'gtHistoryResult');
        }


        // Alert dialog
        ons.createAlertDialog('alert-dialog.html').then(function(dialog){
            share.dialogs.alert = dialog;
            dialog._scope.clickButton = function(button){
                if(button.callback){
                    if(button.callback()!==true){
                        dialog.hide();
                    }
                }else{
                    dialog.hide();
                }
            }
            dialog._scope.model = {}
            dialog.open = function(options){
                dialog.cancelable = options.cancelable||false; 

                dialog._scope.model.title = options.title||'';
                dialog._scope.model.html = options.html?$sce.trustAsHtml(options.html):'';
                dialog._scope.model.content = options.content||'';
                dialog._scope.model.buttons = options.buttons||[];
                dialog._scope.model.visible = true;
                dialog._scope.model.changes = false;
                dialog._scope.model.error = "";

                Utils.openDialog(dialog);
            }
            TimesheetService.setDialog(dialog);
        });

        //Reports dialog
        ons.createDialog('app/dialog/reports/reports-dialog.html').then(function(dialog){
            dialog.open = function(reports){
              dialog.setReports(reports);
              dialog._scope.openingDialog = false;
              Utils.openDialog(dialog);
            };
            dialog.setReports = function(reports){
              dialog._scope.reports = reports;
            }
            share.dialogs.reports = dialog;
        });

        // Status history dialog
        ons.createDialog('app/timesheet/status-history-week-dialog.html').then(function(dialog){
            dialog._scope.data = {};
            dialog.open = function(){
              dialog._scope.parent = $scope;
              dialog._scope.done = function(){
                dialog.hide();
              }
              Utils.openDialog(dialog);
            };
            share.dialogs.statusHistory = dialog;
        });

        $scope.$on('$destroy', function(){
            var dialogs = Object.keys(share.dialogs);
            for(var k=0; k<dialogs.length; ++k){
                var dialog = share.dialogs[dialogs[k]];
                if(dialog.visible){
                    dialog.hide();
                }
            }
        });

        share.updateTimesheetStatus = function(){
            $scope.timesheet.Status = $scope.workweek.StatusMeaning
            $scope.timesheet.StatusCode = $scope.workweek.StatusCode
        }
        
        var gtSaveCallback = function(){
            share.updateTimesheetStatus();
            if(closing) closeTimesheet(false);
        }
        share.save = function(scope, endpoint, extraData, callback){
            return TimesheetService.save(scope, endpoint, extraData, callback)
        }
        $scope.save = function(){
            share.save($scope, null, null, 'gtSaveCallback');
        }

        var closing = false;
        var closeTimesheet = function(save){
            if(save){
              $scope.save();
              return;
            }
            mainNav.popPage();
          };
          $scope.closeTimesheet = function(){
              if(!$scope.workweek.changes || !$scope.workweek.AllowEdit){
                  closeTimesheet();
                  return;
              }
              if(closing || (mainNav.pages[mainNav.pages.length-1].pushedOptions.page != 'app/timesheet/gt-work-week.html')) return;
              closing = true;
              Utils.notification.confirm("Save changes?", {
                  callback: function(ok){
                    if(ok == 2){
                      closing = false;
                      return;
                    }
                    closeTimesheet(ok==0);
                  },
                  buttonLabels: ["Save","Discard","Cancel"]
              });
          };

          var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', $scope.closeTimesheet, false);
            }else{
                document.removeEventListener('backbutton', $scope.closeTimesheet);
                ons.enableDeviceBackButtonHandler();
            }
          }

          share.setCallbacks();
          
          $scope.hidePage = function(){
            toggleBackButton(false)
            Utils.clearBackButtonHandlers()
          }
          $scope.showPage = function(){
            toggleBackButton(true)
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })
            ons.findComponent('#gt-back-button')._element[0].onClick = $scope.closeTimesheet;
          }

        loadWorkWeek();
    }]);

})();
(function(){
    'use strict';

    angular.module('app').controller('FTEWorkWeekController', ['$scope', 'ApiService', 'Utils', '$rootScope', '$timeout', '$anchorScroll', 'TimeEntryService', '$sce', 'CommentDialog', function($scope, ApiService, Utils, $rootScope, $timeout, $anchorScroll, TimeEntryService, $sce, CommentDialog) {
        
        $scope.timesheet = $scope.$parent.current.timesheet;
        $scope.current = $scope.$parent.current;
        var share = {dialogs:{},timesheet:$scope.timesheet}
        $scope.fromGeneralTime = false
        $scope.$parent.current.share = share

        share.workWeekResult = function(resp, setData, scope){
            if(resp && resp.WorkWeek){
                if(setData){
                    var keys = Object.keys(setData);
                    for(var i=0; i<keys.length; ++i){
                        resp.WorkWeek[keys[i]] = setData[keys[i]];
                    }
                }
                $scope.current.workweek = $scope.workweek = TimeEntryService.cleanWorkweek(resp.WorkWeek, scope||$scope);
            }
            $scope.working = false;
        }

        var loadWorkWeek = function(callback, setData, scope, postData){
            if($scope.current.preloadedWorkWeek){
                return;
            }
            $scope.working = true;
            var loadDone = function(resp){
                Utils.notification.hideModal();
                share.workWeekResult(resp, setData, scope);
                if(callback){
                    callback($scope.workweek);
                }
            }
            var gpsDone = function(position){
                var obj = postData||{};
                if(position){
                    obj.lat = position.coords.latitude
                    obj.lon = position.coords.longitude
                }
                if($scope.current.nextAction == 'fromDayCompleteWork' || $scope.current.nextAction == 'fromDayShowUpSites'){
                    var nextAction = $scope.current.nextAction;
                    delete $scope.current.nextAction;
                    obj.completedWork = true
                    obj.date = $scope.current.date
                    setData = {}
                    if(nextAction)setData.FromCompleteWork = true;
                    callback = function(workweek){
                        var date = Utils.date($scope.current.date)
                        var index = workweek.WorkDay.findIndex(function(d){ return d.WeekdayDate.getTime() == date.getTime() });
                        if(nextAction == 'fromDayCompleteWork')$scope.current.fromDayCompleteWork = true;
                        $scope.changeDay(index, true);
                    }
                }
                obj.workweek = $scope.timesheet.WorkWeekID;
                obj.route = 'open';
                ApiService.get('fte', obj).then(loadDone, loadDone);
            }
            Utils.getGpsPosition(function(position){
                gpsDone(position)
            }, function(){
                gpsDone()
            })            
        }
        share.reload = function(callback, setData, scope, postData, noToast){
            if(!noToast)Utils.notification.modal('Reloading timesheet...');
            loadWorkWeek(callback, setData, scope, postData);
        }

        $scope.changeTimeWorkWeek = function(){
            $scope.workweek.changes = true
            $scope.mapBack($scope.workweek,'timeWorkWeek');
        }

        $scope.mapBack = function(obj, name, spMapName){
            spMapName = spMapName||name;
            TimeEntryService.mapSmartpickBack($scope.workweek, obj, name, spMapName);
        }

        $scope.callbacks = {}
        $scope.checkCallback = function(when){
            when = when||'beforeInit';
            var handled = false, init = $scope.workweek.Init;
            $scope.workweek.Init = false;
            if($scope.workweek.callback && $scope.callbacks[$scope.workweek.callback]){
                var callback = $scope.callbacks[$scope.workweek.callback];
                if(typeof callback == 'function') callback = {when:'beforeInit', fn: callback}
                if(when == callback.when){
                    callback.fn($scope.workweek.callbackData);
                    if(!init) handled = $scope.workweek.callback;
                    delete $scope.workweek.callback;
                    delete $scope.workweek.callbackData;
                }
            }
            return handled;
        }
        $scope.callbacks.loadJobTasksResult = function(data){
            TimeEntryService.loadJobTasksInfo($scope, $scope.workweek, '', data);
        }
        $scope.callbacks.initMemberTimeResult = function(data){
            TimeEntryService.initMemberTime($scope.workweek, $scope, data);
        }
        $scope.callbacks.initEquipmentTimeResult = function(data){
            TimeEntryService.initEquipmentTime($scope.workweek, $scope, data);
        }
        $scope.callbacks.initSummaryResult = function(data){
            TimeEntryService.initSummary($scope.workweek, $scope, data);
        }
        $scope.callbacks.loadCrewResult = function(data){
            TimeEntryService.draftCrews($scope, $scope.workweek, data);
        }

        $scope.loadingDialog = function(){
            //Nothing?
        }

        share.getStatusClass = function(code){
            switch(code){
                case 'SB':
                case 'Submitted':
                    return 'timesheet-blue';
                break;
                case 'RJ':
                case 'Rejected':
                case 'IP':
                case 'In Process':
                    return 'timesheet-red';
                break;
                case 'ZH':
                case 'Zero Hours':
                case 'AP':
                case 'Approved':
                    return 'timesheet-black';
                break;
                case 'RV':
                case 'Reviewed':
                    return 'timesheet-green';
                break;
                case 'PE':
                case 'Pending':
                    return 'timesheet-gray';
            }
            return '';
        }
        $scope.getStatusClass = share.getStatusClass

        share.dayOpened = function(){
            $scope.loading = false;
        }
        $scope.changeDay = function($index, auto){
            if($scope.loading) return;
            if(auto){
                Utils.notification.modal('Opening '+Utils.date($scope.workweek.WorkDay[$index].WeekdayDate, 'MM/dd'))
            }
            $scope.loading = true;
            $scope.workweek.currentDay = $index;
            $scope.current.workweek = $scope.workweek;
            $scope.current.original = $scope.original;
            $scope.current.callbacks = $scope.callbacks;
            mainNav.pushPage('app/timesheet/fte-work-day.html');
        }

        $scope.getBucketDate = function(bucket, which){
            if(bucket.IntervalCode == 'WE'){
                return 'This Week';
            }
            if(bucket.IntervalCode == 'MN'){
                return Utils.date(bucket[which+'End'], 'MMMM')
            }
            if(bucket.IntervalCode == 'AN'){
                return Utils.date(bucket[which+'End'], 'yyyy')
            }
        }
        $scope.max = Math.max;

        share.unselectJobs = function(dayIndex){
            var removed = 0, day = $scope.workweek.WorkDay[dayIndex];
            for(var j=0; j<day.Job.length; ++j){
                if(day.Job[j].selected) ++removed;
                day.Job[j].selected = false;
                delete day.Job[j].order;
            }
            if(removed){
                TimeEntryService.addRemoveJobs($scope.workweek, dayIndex);
            }
        }

        share.updateOriginal = function(original){
            $scope.original = original;
        }

        $scope.callbacks.reviewCallback = function(data){
            if(data && data.success){
                data.succeeded = Utils.enforceArray(data.succeeded);
                data.failed = Utils.enforceArray(data.failed);
                if(data.failed.length){
                    var num = data.succeeded.length+data.failed.length;
                    var title;
                    var html = [];
                    for(var d=0; d<data.failed.length; ++d){
                        var day = $scope.workweek.WorkDay.find(function(wd){ return wd.ID==data.failed[d].ID });
                        if(!title){
                            title = 'Error - Cannot '+(day.StatusCode=='AP'?'Approve':((day.StatusCode == 'IP'&&day.priorStatus=='AP')?'Reopen':''))+(num>1?' Some Days':'');
                        }
                        data.failed[d].errors = Utils.enforceArray(data.failed[d].errors);
                        if(day){
                            html.push('<b>'+Utils.date(day.WeekdayDate,'M/dd/yyyy')+'</b><ul>'+data.failed[d].errors.map(function(err){ return '<li>'+err.Message+'</li>' })+'</ul>');
                        }
                    }
                    share.dialogs.alert.open({
                        title: title,
                        html: html.join('<br>'),
                        buttons: [{
                            label: 'Ok'
                        }],
                        cancelable: true
                    });
                }

                TimeEntryService.reviewDone(data.succeeded, data.failed, $scope);

                if(data.failed.length){
                    $scope.original = Utils.copy($scope.workweek);
                    $scope.current.original = $scope.original;
                }
                share.updateTimesheetStatus();
            }else{
                share.dialogs.alert.open({
                    title: 'Error',
                    content: 'Failed to save the timesheet',
                    buttons: [{
                        label: 'Ok'
                    }],
                    cancelable: true
                });
            }
        }

        var openReviewComment = function(scope, code, force, dayIndex){
            var dialogOptions = {
                title: 'Comment',
                accept: function(comment){
                    share.review(scope, code, force, dayIndex, comment);
                    CommentDialog.hide();
                },
                requireComment: code=='RJ',
                placeholder: 'Enter a comment',
                button: ['accept',TimeEntryService.statusMeanings[code]]
            };
            CommentDialog.show(dialogOptions);
        }
        share.review = function(scope, code, force, dayIndex, comment){
            if(code == 'SB' && TimeEntryService.checkRequiredComms(scope.workweek, dayIndex)){
                share.dialogs.alert.open({
                    title: 'Review Safety Communications',
                    content: 'One or more safety communications are required. You must review them before submitting the timesheet.',
                    buttons: [{
                        label: 'Ok',
                        callback: function(){
                            $timeout(function(){
                                scope.workweek.WorkDay[dayIndex].showSafety = true;
                            });
                        }
                    }],
                    cancelable: true
                });
                return;
            }
            if((code == 'SB' && $scope.workweek.WorkDay[dayIndex].StatusCode == 'RJ') || code == 'RJ'){
                if(typeof comment == 'undefined'){
                    openReviewComment(scope, code, force, dayIndex);
                    return;
                }
            }
            var result = TimeEntryService.review(scope, code, force, dayIndex, comment);
            if(result.message){
                share.dialogs.alert.open({
                    title: result.title,
                    content: result.message,
                    buttons: result.buttons||[{
                        label: 'Cancel'
                    },{
                        label: 'Submit Anyway',
                        callback: function(){ share.review(scope, code, true, dayIndex, comment); return true; }
                    }],
                    cancelable: true
                });
            }else{
                share.save(scope, 'fte/saveAndReview', '', 'reviewCallback', code=='ZH');
            }
        }

        $scope.reviewAll = function(code){
            share.review($scope, code);
        }

        $scope.zeroAllHours = function(){
            for(var d=0; d<$scope.workweek.WorkDay.length; ++d){
                if($scope.workweek.WorkDay[d].StatusCode == 'PE'){
                    share.unselectJobs(d);
                }
            }
            $scope.reviewAll('ZH');
        }

        share.save = function(scope, endpoint, extraData, callbackName, noValidate){
            for(var d=0; d<scope.workweek.WorkDay.length; ++d){
                if(TimeEntryService.currentStep(scope.workweek, scope.workweek.WorkDay[d]).ProcessStepCode == 'CJ'){
                    TimeEntryService.addRemoveJobs(scope.workweek, d);
                }
            }
            TimeEntryService.save(scope, endpoint, extraData, callbackName, '', noValidate);
        }

        // Alert dialog
        ons.createAlertDialog('alert-dialog.html').then(function(dialog){
            share.dialogs.alert = dialog;
            dialog._scope.clickButton = function(button){
                if(button.callback){
                    if(button.callback()!==true){
                        dialog.hide();
                    }
                }else{
                    dialog.hide();
                }
            }
            dialog._scope.model = {}
            dialog.open = function(options){
                dialog.cancelable = options.cancelable||false; 

                dialog._scope.model.title = options.title||'';
                dialog._scope.model.html = options.html?$sce.trustAsHtml(options.html):'';
                dialog._scope.model.content = options.content||'';
                dialog._scope.model.buttons = options.buttons||[];
                dialog._scope.model.visible = true;
                dialog._scope.model.changes = false;
                dialog._scope.model.error = "";

                Utils.openDialog(dialog);
            }
            TimeEntryService.setDialog(dialog);
        });

        //Reports dialog
        ons.createDialog('app/dialog/reports/reports-dialog.html').then(function(dialog){
            dialog.open = function(reports){
              dialog.setReports(reports);
              dialog._scope.openingDialog = false;
              Utils.openDialog(dialog);
            };
            dialog.setReports = function(reports){
              dialog._scope.reports = reports;
            }
            share.dialogs.reports = dialog;
        });

        //Report level dialog
        ons.createDialog('app/timesheet/report-level-dialog.html').then(function(dialog){
            dialog.open = function(scope, data){
              dialog._scope.data = data;
              dialog._scope.parent = scope;
              Utils.openDialog(dialog);
            };
            dialog._scope.selectReportLevel = function(level, dayIndex, jobIndex){
                var data = TimeEntryService.cleanReportSelectionData(dialog._scope.data, level, dayIndex, jobIndex);
                loadReports(data, dialog._scope.parent);
                dialog.hide();
            }
            dialog._scope.done = function(){
                dialog.hide();
            }
            share.dialogs.reportLevel = dialog;
        });

         $scope.$on('$destroy', function(){
            var dialogs = Object.keys(share.dialogs);
            for(var k=0; k<dialogs.length; ++k){
                var dialog = share.dialogs[dialogs[k]];
                if(dialog.visible){
                    dialog.hide();
                }
            }
        });

        share.updateTimesheetStatus = function(){
            $scope.timesheet.Status = $scope.workweek.StatusMeaning
            $scope.timesheet.StatusCode = $scope.workweek.StatusCode
        }
        $scope.callbacks.saveCallback = function(){
            share.updateTimesheetStatus();
            if(closing) closeTimesheet(false);
        }
        $scope.save = function(){
            share.save($scope, 'fte/save', '', 'saveCallback');
        }

        share.reportsDone = function(resp, scope){
            Utils.notification.hideToast();
            scope.loading = false;
            if(!resp || !resp.Reports || !resp.Reports.length){
                Utils.notification.alert('No reports available for this item.');
            }else{
                share.dialogs.reports.open(resp.Reports);
            }
        }
        var loadReports = function(reportSelection, scope){
            scope.loading = true;
            share.save(scope, 'fte/reports/save', reportSelection, 'reportsDone');
            Utils.notification.toast('Loading reports...');
        }
        $scope.callbacks.reportsDone = function(resp){
            share.reportsDone(resp, $scope);
        }
        $scope.reports = function(){
            share.dialogs.reportLevel.open($scope, TimeEntryService.getReportLevelData($scope.workweek, true))
        }

        var closing = false;
        var closeTimesheet = function(save){
            if(save){
              $scope.save();
              return;
            }

            if($scope.fromGeneralTime){
                delete $scope.current.fromGeneralTime;
                $scope.$parent.current.ftDone = true;
                mainNav.replacePage('app/timesheet/gt-work-day.html');
            }else{
                mainNav.popPage();
            }
          };
          $scope.closeTimesheet = function(){
              if(!$scope.workweek.changes||!$scope.workweek.AllowEdit){
                  closeTimesheet();
                  return;
              }
              if(closing || (mainNav.pages[mainNav.pages.length-1].pushedOptions.page != 'app/timesheet/fte-work-week.html')) return;
              closing = true;
              Utils.notification.confirm("Save changes?", {
                  callback: function(ok){
                    if(ok == 2){
                      closing = false;
                      return;
                    }
                    closeTimesheet(ok==0);
                  },
                  buttonLabels: ["Save","Discard","Cancel"]
              });
          };

          var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', $scope.closeTimesheet, false);
            }else{
                document.removeEventListener('backbutton', $scope.closeTimesheet);
                ons.enableDeviceBackButtonHandler();
            }
          }

          $scope.hidePage = function(){
            toggleBackButton(false)
            Utils.clearBackButtonHandlers()
          }
          $scope.showPage = function(){
            toggleBackButton(true)
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })

            ons.findComponent('#timesheet-back-button')._element[0].onClick = $scope.closeTimesheet;
            if($scope.current.preloadedWorkWeek){
                share.workWeekResult($scope.current.preloadedWorkWeek);
                delete $scope.current.preloadedWorkWeek
                $scope.current.fromGeneralTime = true;
                $scope.changeDay($scope.workweek.currentDay, true)
            }else if($scope.current.nextAction == 'closeTimesheet'){
                delete $scope.current.nextAction;
                $scope.closeTimesheet()
            }
            if($scope.current.fromGeneralTime){
                $scope.fromGeneralTime = $scope.current.fromGeneralTime;
            }
          }

        loadWorkWeek();
    }]);

})();
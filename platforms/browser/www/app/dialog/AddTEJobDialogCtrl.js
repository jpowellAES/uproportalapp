(function(){
    'use strict';

    angular.module('app').controller('AddTEJobDialogController', ['$scope', 'ApiService', '$timeout', '$rootScope', 'Utils', 'ImgOrPdf', 'urls', function($scope, ApiService, $timeout, $rootScope, Utils, ImgOrPdf, urls) {

        $scope.smartpicks = null;

        $scope.executeSearch = function(){
            var obj = {
                action: 'add',
            }
            if($scope.form.department && $scope.form.crew){
                obj.department = $scope.form.department.Name;
                obj.crew = $scope.form.crew.ID
            }
            ApiService.get('teJobAction', obj).then(searchResponse, searchResponse);
            $scope.loading = true;
        }
        var setDefaultSmartpicks = function(resp){
            if(resp.DefaultDepartment){
                var dept = $scope.smartpicks.Department.find(function(d){
                    return d.Name == resp.DefaultDepartment;
                });
                if(dept){
                    $scope.form.department = dept;
                }
            }
            if(resp.DefaultCrew){
                var crew = $scope.smartpicks.Crew.find(function(c){
                    return c.ID == resp.DefaultCrew;
                });
                if(crew){
                    $scope.form.crew = crew;
                }
            }
        }
        var searchResponse = function(resp){
            if(resp){
                if(!$scope.smartpicks){
                    $scope.smartpicks = {
                        Crew: resp.Crew?Array.isArray(resp.Crew)?resp.Crew:[resp.Crew]:[],
                        Department: resp.Department?Array.isArray(resp.Department)?resp.Department:[resp.Department]:[]
                    }
                    setDefaultSmartpicks(resp);
                }
                if(resp.ActiveJob || resp.PendingStartJob){
                    resp.ActiveJob = resp.ActiveJob?Array.isArray(resp.ActiveJob)?resp.ActiveJob:[resp.ActiveJob]:[];
                    resp.ActiveJob.forEach(function(job){job.status = 'Active'});
                    resp.PendingStartJob = resp.PendingStartJob?Array.isArray(resp.PendingStartJob)?resp.PendingStartJob:[resp.PendingStartJob]:[];
                    resp.PendingStartJob.forEach(function(job){job.status = 'Pending Start'});
                    $scope.items = resp.ActiveJob.concat(resp.PendingStartJob);
                }else{
                    $scope.items = [];
                }
            }
            $scope.form.lastSearch = {
                department: $scope.form.department.Code,
                crew: $scope.form.crew.ID
            }
            $scope.checkLastSearch($scope.form.lastSearch);
            $scope.loading = false;
        }

        $scope.filterSmartpick = function(props, term){
            var regex = new RegExp(Utils.sanitizeTermForRegex(term), 'i');
            return function(item){
                for(var i=0; i<props.length; i++){
                    if(item[props[i]].match(regex))
                        return true;
                }
                return false;
            }
        }

        $scope.filterCrew = function(){
            return function(item){
                return item.DepartmentCode == ''||($scope.form.department && $scope.form.department.Code == item.DepartmentCode);
            }
        }

        $scope.changeDepartment = function(){
            if($scope.form.department && $scope.form.crew && $scope.form.department.Code == $scope.form.crew.DepartmentCode){
                return;
            }
            $scope.form.crew = null;
            if($scope.form.department){
                $scope.smartpicks.Crew.forEach(function(crew){
                    if(crew.DepartmentCode == $scope.form.department.Code){
                        $scope.form.crew = crew;
                        return;
                    }
                });
            }
        }

        $scope.checkLastSearch = function(lastSearch){
            return $scope.showItems = $scope.form.department.Code == lastSearch.department
                && $scope.form.crew.ID == lastSearch.crew;
        }

        $scope.init = function(){
            $scope.loading = false;
            $scope.form = {
                department: null,
                crew: null
            };
            $scope.items = [];
            $scope.executeSearch();
        }
        $scope.reset = function(){
            $scope.smartpicks = null;
            $scope.init();
        }

    }]);

})();
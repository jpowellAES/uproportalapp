(function(){
    'use strict';

    angular.module('app').controller('UploadsDialogController', ['$scope', 'ImgOrPdf', 'urls', function($scope, ImgOrPdf, urls) {
       
        $scope.openingUpload = false;
        $scope.uploads = [];
        
        $scope.addUpload = function(){
            //override when including on page
        }

        //$scope.deleteUpload = function(){
            //override when including on page
        //}

        $scope.setTags = function(tags){
            $scope.tags = tags;
        }

        $scope.setOptions = function(options){
            $scope.options = options;
        }

        $scope.captionEdited = function(upload){
            //override when including on page
        }
        $scope.openUpload = function(upload){
            if($scope.openingUpload)
                return;

            var path =  '/upload/' + upload.Key;
            $scope.openingUpload = true;
            var options = $scope.options||{};
            options.editable = $scope.canEdit;
            options.callback = function(upl){
                var upload = $scope.uploads.find(function(u){ return u.Key == upl.Key});
                if(upload){
                    upload.Description = upl.Description;
                    $scope.captionEdited(upload)
                }
            }
            options.tags = $scope.tags;
            ImgOrPdf.loadWithCaption(path, upload, options).finally(function(){
                $scope.openingUpload = false;
            });
        }
    }]);

})();
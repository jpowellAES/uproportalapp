(function(){
    'use strict';

    angular.module('app').controller('JobInfoController', ['$scope', 'ApiService', '$timeout', '$rootScope', 'Utils', 'ImgOrPdf', 'urls', function($scope, ApiService, $timeout, $rootScope, Utils, ImgOrPdf, urls) {
        $scope.loading = true;
        $scope.openingReport = false;
        $scope.endpoint = ''
        var joblist = '';
        var tabbar = document.getElementById('j-dialog-tabbar');
        
        
        var getInfo = function(){
            var obj = {
                job: $scope.job.JobID,
                type: joblist
            };
            if($scope.job.JobDesignID){
                obj.design = $scope.job.JobDesignID;
            }
            if($scope.reportField){
                obj.reportField = $scope.reportField;
            }
            ApiService.get($scope.endpoint, obj).then(infoResult,infoResult);
        };

        var cleanResponse = function(job){
            job.ActualStartDate = job.ActualStartDate?new Date(job.ActualStartDate):'';
            job.TargetCompleteDate = job.TargetCompleteDate?new Date(job.TargetCompleteDate):'';
            job.PercentComplete = Utils.fixPercent(job.PercentComplete);
            if(job.Documents && !Array.isArray(job.Documents))
                job.Documents = [job.Documents];
            return job;
        }

        var infoResult = function(resp){
            if(resp && typeof resp.ActualStartDate != 'undefined'){
                angular.extend($scope.job, cleanResponse(resp));
            }
            $timeout(function(){window.dispatchEvent(new Event('resize'))},500);
            $scope.loading = false;
        };

        var init = function(args){
            $scope.loading = true;
            $scope.job = args.job;
            $scope.endpoint = args.endpoint;
            $scope.reportField = args.reportField;
            joblist = args.joblist;
            getInfo();
        };

        $scope.posthide = function(){
            tabbar.setActiveTab(0);
            console.log(tabbar.getActiveTabIndex())
        }
        var destroy = function(remove){
            $scope.posthide()
            remove();
        }

        $scope.openJobDoc = function(doc){
            if($scope.openingReport)
                return;
            var path = 'document/'+doc.ID;
            $scope.openingReport = true;
            ImgOrPdf.load(path).finally(function(){
                $scope.openingReport = false;
            });
        }

        $scope.openReport = function(report){
            if($scope.openingReport)
                return;
            var path = 'report/'+report.Token;
            $scope.openingReport = true;
            var downloadUrl =  urls.media+'/'+path;
            ImgOrPdf.load(path, downloadUrl, report.Name).finally(function(){
                $scope.openingReport = false;
            });
        }

        var remove = $rootScope.$on('JobInfoDialog', function(evt, args) {
            switch(args.type){
                case 'init':
                   init(args);
                break;
                case 'destroy':
                    destroy(remove);
                break;
            }
        });
        $scope.$on('$destroy', function(){destroy(remove)});
    }]);

    angular.module('app').controller('LocationInfoController', ['$scope', 'ApiService', '$timeout', '$rootScope', 'Utils', 'urls', 'ImageModal', function($scope, ApiService, $timeout, $rootScope, Utils, urls, ImageModal) {
        $scope.loading = true;
        var tabbar = document.getElementById('l-dialog-tabbar');
        $scope.endpoint = '';
        
        var getInfo = function(){
            ApiService.get($scope.endpoint, {location: $scope.location.JobLocationID, history: true}).then(infoResult,infoResult);
        };

        var cleanResponse = function(location){
            if(location.LInspection){
                if(!Array.isArray(location.LInspection))
                    location.LInspection = [location.LInspection];
                location.LInspection.forEach(function(inspection){
                    inspection.StatusDate = new Date(inspection.StatusDate);
                    inspection.Comments = Utils.prepareForHTML(inspection.Comments);
                });
            }
            return location.LInspection;
        }

        $scope.openPhoto = function(key){
            var src = urls.media + '/upload/' + key;
            ImageModal.show({
                src: src
            });
        };

        var infoResult = function(resp){
            if(resp && resp.Location){
                $scope.location.LInspection = cleanResponse(resp.Location);
            }
            $timeout(function(){window.dispatchEvent(new Event('resize'))},500);
            $scope.loading = false;
        };

        var init = function(args){
            $scope.loading = true;
            $scope.location = args.location;
            $scope.endpoint = args.endpoint;
            getInfo();
        };

        $scope.posthide = function(){
            tabbar.setActiveTab(0);
        }

        $scope.loadedInspections = function(item){
            return typeof item.StatusDate != 'undefined'
        };

        var remove = $rootScope.$on('LocationDialog', function(evt, args) {
            switch(args.type){
                case 'init':
                   init(args);
                break;
                case 'destroy':
                    remove();
                break;
            }
        });
    }]);

    angular.module('app').controller('LocationDetailInfoController', ['$scope', 'ApiService', '$timeout', '$rootScope', 'Utils', 'urls', 'ImageModal', 'CommentDialog', 'ImgOrPdf', function($scope, ApiService, $timeout, $rootScope, Utils, urls, ImageModal, CommentDialog, ImgOrPdf) {
        $scope.loading = true;
        $scope.resubmitting = false;
        $scope.openingDoc = false;
        var tabbar = document.getElementById('ld-dialog-tabbar');
        $scope.endpoint = '';
        
        var getInfo = function(){
        	ApiService.get($scope.endpoint, {detail: $scope.detail.JobLocationDetailID}).then(infoResult,infoResult);
        };

        var cleanResponse = function(detail){
            if(detail.LDChangeOrder && !Array.isArray(detail.LDChangeOrder))
                detail.LDChangeOrder = [detail.LDChangeOrder];
            if(detail.CompletionDetail){
                if(!Array.isArray(detail.CompletionDetail))
                    detail.CompletionDetail = [detail.CompletionDetail];
                detail.CompletionDetail.forEach(function(completion){
                    completion.CompleteDate = new Date(completion.CompleteDate)
                });
            }
            if(detail.LDInspection){
                if(!Array.isArray(detail.LDInspection))
                    detail.LDInspection = [detail.LDInspection];
                detail.LDInspection.forEach(function(inspection){
                    inspection.StatusDate = new Date(inspection.StatusDate);
                    inspection.Comments = Utils.prepareForHTML(inspection.Comments);
                });
            }
            detail.CompleteDate = detail.CompleteDate?new Date(detail.CompleteDate):'';
            return detail;
        }

        $scope.openPhoto = function(key){
            var src = urls.media + '/upload/' + key;
            ImageModal.show({
                src: src
            });
        };

        $scope.openSpecDiagram = function(documentID){
            if($scope.openingDoc || !documentID)
                return;
            console.log(documentID)
            var path = 'spec/'+documentID;
            $scope.openingDoc = true;
            ImgOrPdf.load(path).finally(function(){
                $scope.openingDoc = false;
            });
        };

        $scope.resubmitInspection = function(inspection){
            var title = 'Re-submit';
            CommentDialog.show({
                title: title,
                accept: function(comment){
                    if(comment)
                        resubmit(inspection, comment);
                    CommentDialog.hide();
                },
                button: ['accept','Re-submit'],
                camera: false
            });
            
        };

        var resubmit = function(inspection, comment){
            console.log(inspection)
            var obj = {detail:$scope.detail.JobLocationDetailID, action:'resubmit-inspection', inspection: inspection.JobInspectionID, comment: comment};
            $scope.resubmitting = true;
            ApiService.post('completeLocationDetail', obj)
            .then(function(resp){
                resubmitResult(inspection, resp);
            },function(resp){
                resubmitResult(inspection, resp);
            });
        };

        var resubmitResult = function(inspection, resp){
            if(resp && resp.Location){
                $scope.location.FailedInspectionFlag = resp.Location.FailedInspectionFlag;
                $scope.detail.FailedInspectionFlag = resp.Location.LocationDetail.FailedInspectionFlag;
                inspection.StatusMeaning = resp.Location.LocationDetail.LDInspection.StatusMeaning;
                inspection.StatusDate = new Date(resp.Location.LocationDetail.LDInspection.StatusDate);
                inspection.Comments = resp.Location.LocationDetail.LDInspection.Comments;
            }
            $scope.resubmitting = false;
        };

        var infoResult = function(resp){
        	if(resp && resp.LocationDetail){
        		angular.extend($scope.detail, cleanResponse(resp.LocationDetail));
        	}
            $timeout(function(){window.dispatchEvent(new Event('resize'))},500);
        	$scope.loading = false;
        };

        var init = function(args){
        	$scope.detail = args.detail;
            $scope.location = args.location;
            $scope.design - args.design;
            $scope.endpoint = args.endpoint;
            $scope.loading = true;
        	getInfo();
        };

        $scope.posthide = function(){
            tabbar.setActiveTab(0);
        }

        var remove = $rootScope.$on('LocationDetailDialog', function(evt, args) {
            switch(args.type){
                case 'init':
                   init(args);
                break;
                case 'destroy':
                    remove();
                break;
            }
        });
    }]);

})();
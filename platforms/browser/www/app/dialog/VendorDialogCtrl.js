(function(){
    'use strict';

    angular.module('app').controller('VendorDialogController', ['$scope', 'ApiService', '$timeout', '$rootScope', 'Utils', 'ImgOrPdf', 'urls', function($scope, ApiService, $timeout, $rootScope, Utils, ImgOrPdf, urls) {

        $scope.smartpicks = {};

        $scope.init = function(){
            $scope.filters = {
                show: false
            };
            $scope.loading = false;
            $scope.loadingSmartpicks = false;
            $scope.addingNew = false;
            $scope.searched = false;
            $scope.searches = {
                company: ''
            }
            $scope.form = {
                newCompany: '',
                companyType: 'VE',
                vendorType: ''
            };
            $scope.items = [];
            $scope.forceType = '';
            $scope.forceSubType = '';
        }
        $scope.reset = function(){
            $scope.smartpicks = {}
            $scope.init();
        }
        $scope.init();

        $scope.setItems = function(items){
            $scope.items = items;
        }
        $scope.set = function(prop, val){
            $scope[prop] = val;
            if(prop == 'forceType' || prop == 'forceSubType'){
                $scope.filters.type = Utils.copy(val);
                if(prop == 'forceSubType'){
                    $scope.filters.type.SubCode = $scope.filters.type.Code;
                    $scope.filters.type.Code = 'VE';
                }
            }
        }

        var loadSmartpicks = function(){
            return new Promise(function(resolve,reject){
                var obj = {
                    action: 'smartpicks'
                }
                ApiService.get('vendors', obj).then(smartpickResponse, smartpickResponse).then(resolve);
                $scope.loading = true;
                $scope.loadingSmartpicks = true;
            });
        }
        var smartpickResponse = function(resp){
            if(resp){
                if(resp.VendorType){
                    $scope.smartpicks.VendorType = Utils.enforceArray(resp.VendorType)
                    for(var i=0; i<$scope.smartpicks.VendorType.length; ++i){
                        $scope.smartpicks.VendorType[i].SubCode = false;
                    }
                }
                if(resp.VendorSubType){
                    $scope.smartpicks.VendorSubType = Utils.enforceArray(resp.VendorSubType)
                    for(var i=0; i<$scope.smartpicks.VendorSubType.length; ++i){
                        if($scope.smartpicks.VendorSubType[i].Code != 'UC'){
                            $scope.smartpicks.VendorType.push({
                                Code: 'VE',
                                SubCode: $scope.smartpicks.VendorSubType[i].Code,
                                Meaning: $scope.smartpicks.VendorSubType[i].Meaning
                            })
                        }
                    }
                    defaultVendorType();
                }
            }
            $scope.loading = false;
            $scope.loadingSmartpicks = false;
        }

        var defaultVendorType = function(){
            if($scope.forceType && $scope.form.companyType != $scope.forceType.Code){
                $scope.form.companyType = $scope.forceType.Code;
            }
            if( $scope.smartpicks.VendorSubType && !$scope.form.vendorType || $scope.forceSubType){
                var type = $scope.forceSubType.Code||'UC';
                $scope.form.vendorType = $scope.smartpicks.VendorSubType.find(function(st){ return st.Code == type });
            }
        }
        var returnNewVendor = function(){
            var item = {
                ReportName: $scope.form.newCompany,
                OrgTypeCode: $scope.form.companyType,
            }
            if($scope.form.companyType == 'VE' && $scope.form.vendorType){
                item.Type = $scope.form.vendorType.Code;
            }
            $scope.selectItem(item);
        }
        $scope.addNew = function(){
            if(!$scope.addingNew){
                $scope.filters.show = false;
                $scope.addingNew = true;
                if(!$scope.smartpicks.VendorType){
                    loadSmartpicks();
                }
                defaultVendorType();
                return;
            }
            returnNewVendor();
        }
        $scope.cancelNew = function(){
            $scope.addingNew = false;
        }

        $scope.filterSmartpick = function(props, term){
            var regex = new RegExp(Utils.sanitizeTermForRegex(term), 'i');
            return function(item){
                for(var i=0; i<props.length; i++){
                    if(item[props[i]].match(regex))
                        return true;
                }
                return false;
            }
        }

        $scope.filterItems = function(search){
            var filter = $scope.filter?$scope.filter():null;
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i')
            return function(item){
                return (!$scope.filters.type || ($scope.filters.type.Code==item.OrgTypeCode&&(!$scope.filters.type.SubCode||$scope.filters.type.SubCode==item.Type)))&&(!filter||filter(item))&&(!search||item.ReportName.match(regex))
            }
        }

        $scope.toggleFilter = function(){
            if(!$scope.smartpicks.VendorType){
                loadSmartpicks().then(function(){
                    $timeout(function(){
                        $scope.filters.show = true;
                    });
                });
            }else{
                $scope.filters.show = !$scope.filters.show;
            }
        }

    }]);

})();
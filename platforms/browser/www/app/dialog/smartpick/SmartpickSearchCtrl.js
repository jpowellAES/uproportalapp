(function(){
    'use strict';

    angular.module('app').controller('SmartpickSearchController', ['$scope', 'ApiService', 'urls', 'Utils', function($scope, ApiService, urls, Utils) {
        
        $scope.searchTerm = '';
        $scope.loading = false;
        $scope.items = [];
        $scope.filterLimitThreshold = 25;


        $scope.validateSearchLength = function(term){
            var noWildcards = term.replace(/[\*\?]/ig, '');
            if(noWildcards.length >= $scope.options.minSearchLength)
                return true;
            return false;
        };

        var buildFilterLimits = function(params){
            if(!$scope.options.filters) return;
            var froms = {}, keys = Object.keys($scope.options.filters);
            for(var i=0; i<keys.length; ++i){
                var filter = $scope.options.filters[keys[i]];
                froms[filter.key] = filter.from;
                delete params[filter.key];
            }
            if($scope.options.filtersSelected){
                keys = Object.keys($scope.options.filtersSelected);
                for(var i=0; i<keys.length; ++i){
                    params[keys[i]] = $scope.options.filtersSelected[keys[i]][froms[keys[i]]];
                }
            }
        };
        $scope.search = function(){
            if($scope.options.filterLimitCount == 0 || $scope.options.filterLimitCount > $scope.filterLimitThreshold){
                if(!$scope.validateSearchLength($scope.searchTerm))
                    return;
                $scope.options.endpointData.search = $scope.searchTerm;
            }else{
                $scope.options.endpointData.search = '*';
            }
            $scope.loading = true;
            delete $scope.options.endpointData.filters;
            buildFilterLimits($scope.options.endpointData);
            ApiService.get($scope.options.endpoint, $scope.options.endpointData).then(result,result);
        }

        var result = function(resp){
            $scope.loading = false;
            if(!resp || typeof resp.success != 'undefined' || !resp[$scope.options.itemView])
                return;
            if(!Array.isArray(resp[$scope.options.itemView]))
                resp[$scope.options.itemView] = [resp[$scope.options.itemView]];
            $scope.items = resp[$scope.options.itemView];
            if($scope.items.length)
                defaultFilterProperties($scope.items);
            if(resp.total && resp.total > $scope.items.length){
                Utils.notification.alert("Only "+$scope.items.length+" of "+resp.total+" matches shown. Consider adding more to your search query.", {title: "Large Result Set"});
            }
        }

        var defaultFilterProperties = function(items){
            var props = Object.keys(items[0]);
            for(var p=0; p<props.length; ++p){
                var ok = true;
                for(var i=0; i<items.length; ++i){
                    if(typeof items[i][props[p]] != 'string'){
                        ok = false;
                        break;
                    }
                }
                if(ok){
                    $scope.options.filterProperties.push(props[p]);
                }
            }
        };
        var buildWildcardRegex = function(term){
            var str = term.replace(/^[\*\?\s]*/g, '').replace(/[\*\?\s]*$/g, '');
            str = Utils.sanitizeTermForRegex(str);
            str = str.replace(/\\\*/g, '.*');
            str = str.replace(/\\\?/g, '.');
            return new RegExp(str, 'ig');
        };
        $scope.filterItems = function(term){
            var regex = buildWildcardRegex($scope.searchTerm), params = {}, keys;
            if($scope.options.filtersSelected && $scope.options.filters){
                keys = Object.keys($scope.options.filters);
                for(var k=0; k<keys.length; ++k){
                    var filter = $scope.options.filters[keys[k]];
                    if($scope.options.filtersSelected[filter.key]){
                        params[filter.to] = $scope.options.filtersSelected[filter.key][filter.from];
                    }
                }
            }else if(!$scope.options.filterProperties||!$scope.options.filterProperties.length){
                return true;
            }
            keys = Object.keys(params);
            var props = Object.keys($scope.options.filterProperties);
            return function(item){
                for(var k=0; k<keys.length; ++k){
                    if(item[keys[k]] != params[keys[k]])
                        return false;
                }
                for(var p=0; p<props.length; ++p){
                    if(item[$scope.options.filterProperties[props[p]]].match(regex))
                        return true;
                }
                return false;
            }
        }

        $scope.clearFilters = function(){
            if($scope.options.filtersSelected){
                var keys = Object.keys($scope.options.filters);
                for(var k=0; k<keys.length; ++k){
                    var filter = $scope.options.filters[keys[k]];
                    delete $scope.options.filtersSelected[filter.key];
                }
            }
            $scope.options.filterLimitCount = 1000;
            $scope.options.hasFilterSelected = false;
        };
        var loadFilters = function(){
            $scope.loading = true;
            $scope.options.endpointData.filters = true;
            delete $scope.options.endpointData.search;
            ApiService.get($scope.options.endpoint, $scope.options.endpointData).then(loadFiltersResult).finally(function(){$scope.loading = false;});
        };
        var loadFiltersResult = function(resp){
            var keys = Object.keys(resp)
            for(var k=0; k<keys.length; ++k){
                $scope.options.filtersData[keys[k]] = resp[keys[k]];
            }
            $scope.options.filtersVisible = true;
            calcFilters();
        };
        $scope.openFilters = function(){
            $scope.options.filtersData = $scope.options.filtersData||{}
            if($scope.options.filtersVisible){
                $scope.options.filtersVisible = false;
                return;
            }
            var keys = Object.keys($scope.options.filters);
            for(var k=0; k<keys.length; ++k){
                if(!$scope.options.filtersData[$scope.options.filters[keys[k]].key]){
                    loadFilters();
                    return;
                }
            }
            $scope.options.filtersVisible = true;
            calcFilters();
        }

        $scope.defaultFilters = function(){
            var keys = Object.keys($scope.options.defaultFilters), changed = false;
            for(var k=0; k<keys.length; ++k){
                if($scope.options.filtersData[keys[k]]){
                    $scope.options.filtersSelected[keys[k]] = $scope.options.defaultFilters[keys[k]];
                    changed = true;
                }
            }
            if(changed){
                updateFilterLimitCount();
            }
        }

        var calcFilters = function(){
            var calculated = [], filterKeys = Object.keys($scope.options.filters);
            while(calculated.length < $scope.options.filters.length){
                var currentLength = calculated.length;
                filter:
                for(var k=0; k<filterKeys.length; ++k){
                    var filter = $scope.options.filters[filterKeys[k]];
                    if(calculated.indexOf(filter.key) > -1)
                        continue;
                    var deps = Object.keys(filter.dependencies||{});
                    for(var d=0; d<deps.length; ++d){
                        if(calculated.indexOf(deps[d]) == -1)
                            continue filter;
                    }
                    var filteredData = $scope.options.filtersData[filter.key].filter($scope.filterFilter('',filter,$scope.options.filtersSelected));
                    if(filteredData.length == 1){
                       $scope.options.filtersSelected[filter.key] = filteredData[0];
                       updateFilterLimitCount();
                    }
                    calculated.push(filter.key);
                }
                if(currentLength == calculated.length)
                    break;
            }
        };
        var updateFilterLimitCount = function(){
            $scope.options.hasFilterSelected = true;
            $scope.options.filterLimitCount = 1000;
            if($scope.options.filtersSelected){
                var keys = Object.keys($scope.options.filtersSelected);
                for(var k=0; k<keys.length; ++k){
                    var key = keys[k];
                    if($scope.options.filtersSelected[key].limitCount && $scope.options.filtersSelected[key].limitCount < $scope.options.filterLimitCount){
                        $scope.options.filterLimitCount = $scope.options.filtersSelected[key].limitCount;
                    }
                }
            }
        };

        $scope.selectFilter = function($item, changedFilter){
            var keys = Object.keys($scope.options.filters);
            for(var k=0; k<keys.length; ++k){
                var filter = $scope.options.filters[keys[k]]
                if(filter.dependencies && filter.dependencies[changedFilter.key]){
                    delete $scope.options.filtersSelected[filter.key];
                }
            }
            updateFilterLimitCount();
            calcFilters();
        };
        $scope.filterFilter = function(search, filter, selected){
            var params = {}, deps = Object.keys(filter.dependencies||{});
            filter.disabled = false;
            if(selected){
                for(var d=0; d<deps.length; ++d){
                    if(selected[deps[d]]){
                        params[filter.dependencies[deps[d]].from] = selected[deps[d]][filter.dependencies[deps[d]].to];
                    }else{
                        filter.disabled = true;
                        return function(){return false};
                    }
                }
            }
            var regex = new RegExp(Utils.sanitizeTermForRegex(search), 'i'), keys = Object.keys(params);
            return function(item){
                for(var k=0; k<keys.length; ++k){
                    var param = keys[k];
                    if(item[param] != params[param])
                        return false;
                }
                return search == ''?true:item[filter.display].match(regex);
            };
        }

    }]);

})();
(function(){
    'use strict';

    angular.module('app').controller('ShowupLocationsController', ['$scope', '$timeout', 'ApiService', 'Utils', 'Smartpick', 'StorageService', 'States', 'pinVariants', 'TECompletionService', function($scope, $timeout, ApiService, Utils, Smartpick, StorageService, States, pinVariants, TECompletionService) {
        
        $scope.loading = false;
        $scope.working = true;
        $scope.current = $scope.$parent.current;

        $scope.job = $scope.current.job;
        
        var map, dialogs = {}, cachedSites;
        var selectedMarker;
        var markers = new ol.source.Vector({
          features: []
        })

        $scope.colors = {
            verified: '#0683FF',
            unverified: '#F97C00'
        }
        var overlay, vectorLayer, geolocation;
        var createOverlay = function(){
            var container = document.getElementById('showupPopup');
            overlay = new ol.Overlay({
              element: container,
              positioning: 'bottom-center',
              stopEvent: true,
              offset: [0, -50],
              autoPan: true,
              autoPanAnimation: {
                duration: 250
              }
            });
        }
        var waitForCurrentLocation = function(){
          var location = geolocation.getPosition();
          if(!location){
            return $timeout(waitForCurrentLocation, 5);
          }
          map.getView().setCenter(location);
          map.getView().setZoom(7);
        }
        var initMapPosition = function(){
          if(!map || !vectorLayer){
            return $timeout(initMapPosition, 50);
          }
          $timeout(function(){
            if(vectorLayer.getSource().getFeatures().length){
              var extent = vectorLayer.getSource().getExtent();
              map.getView().fit(extent, {size: map.getSize(), padding:[80,40,40,80], maxZoom:9});
            }else{
              $timeout(function(){
                waitForCurrentLocation();
                jobSitesTabbar.setActiveTab(1);
              }, 50);
            }
          });
        }
        var initMap = function(){
            if(!map){
                if(document.getElementById('showup-map')){
                    var source = new ol.source.OSM();
                    var view = new ol.View({
                      center: ol.proj.fromLonLat([-98.5795,39.8283]),
                      zoomFactor: 4,
                      zoom: 1.75
                    });
                    geolocation = new ol.Geolocation({
                      projection: view.getProjection()
                    });
                    geolocation.on('change:position', function() {
                      var coordinates = geolocation.getPosition();
                      positionFeature.setGeometry(coordinates ?
                        new ol.geom.Point(coordinates) : null);
                    });

                    var accuracyFeature = new ol.Feature();
                    geolocation.on('change:accuracyGeometry', function() {
                      accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
                    });

                    var positionFeature = new ol.Feature();
                    positionFeature.setStyle(new ol.style.Style({
                      image: new ol.style.Circle({
                        radius: 6,
                        fill: new ol.style.Fill({
                          color: '#3399CC'
                        }),
                        stroke: new ol.style.Stroke({
                          color: '#fff',
                          width: 2
                        })
                      })
                    }));
                    var positionLayer = new ol.layer.Vector({
                        source: new ol.source.Vector({
                          features: [accuracyFeature, positionFeature]
                        })
                    })
                    vectorLayer = new ol.layer.Vector({
                      source: markers,
                      renderOrder: function(a,b){
                          if(a === selectedMarker) return 1;
                          if(b === selectedMarker) return -1;
                          var coords = [a.getGeometry().getCoordinates(), b.getGeometry().getCoordinates()];
                          if(coords[0][1] == coords[1][1]) return coords[1][0]-coords[0][0];
                          return coords[1][1]-coords[0][1];
                      }
                    });
                    var centerMapControl = (function (Control) {
                      function CenterMapControl(opt_options) {
                        var options = opt_options || {};

                        var button = document.createElement('button');
                        button.innerHTML = '<i class="ion-android-locate"></i>';

                        var element = document.createElement('div');
                        element.className = 'ol-unselectable ol-control ol-center-control';
                        element.appendChild(button);

                        Control.call(this, {
                          element: element,
                          target: options.target
                        });

                        button.addEventListener('click', this.handleCenter.bind(this), false);
                      }

                      if ( Control ) CenterMapControl.__proto__ = Control;
                      CenterMapControl.prototype = Object.create( Control && Control.prototype );
                      CenterMapControl.prototype.constructor = CenterMapControl;

                      CenterMapControl.prototype.handleCenter = function handleCenter () {
                        this.getMap().getView().setCenter(geolocation.getPosition());
                      };

                      return CenterMapControl;
                    }(ol.control.Control));
                    createOverlay();

                    map = new ol.Map({
                        controls: ol.control.defaults().extend([
                            new centerMapControl()
                        ]),
                        overlays: [overlay],
                        target: 'showup-map',
                        pixelRatio: Math.max(2, ol.has.DEVICE_PIXEL_RATIO),
                        layers: [
                          new ol.layer.Tile({
                            source: source
                          }),
                          positionLayer,
                          vectorLayer
                        ],
                        view: view
                    });
                    $scope.working = false;
                    $timeout(fixCanvasSize);
                    geolocation.setTracking(true);
                    map.on("click", function(e) {
                        if($scope.locating){
                            return;
                        }
                        var feature = map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
                            if(feature.get('site')){
                                return feature;
                            }
                        });
                        if(feature){
                            showLocationInfo(feature.get('site'));                            
                        }else{
                            overlay.setPosition(undefined);
                            unselectMarker();
                        }
                    });
                    map.on('movestart', function(e){
                      if(!$scope.locating) return;
                      overlay.setPosition(undefined);
                    });
                    map.on('moveend', function(e){
                      if(!$scope.locating) return;
                      var coords = map.getView().getCenter();
                      var lonLat = ol.proj.toLonLat(coords);
                      showContextMenu({lon:lonLat[0],lat:lonLat[1],coordinates:coords});
                    });
                }else{
                    $timeout(initMap);
                }
            }
        }

        $scope.changeTab = function($event){
           
        }

        var stopLocating = function(){
          if(!$scope.locating) return;
          overlay.setPosition(undefined);
          $scope.locating.locating = false;
          setMarkerStyle($scope.locating.pin);
          $scope.locating = null;
        }
        var beginLocatingSite = function(site){
          site.loadingAddress = true;
          site.locating = true;
          site.selected = false;
          $scope.popupSite = null;
          if(!site.pin){
            createNewPin(site);
          }
          unselectMarker(site.pin);
          $scope.locating = site;
          setMarkerStyle(site.pin);
          showContextMenu({lon:site.Longitude,lat:site.Latitude,coordinates:ol.extent.getCenter(site.pin.getGeometry().getExtent())});
          $timeout(function(){
            positionMapOnSite(site);
          });
          var pins = document.querySelectorAll('.map-overlay-pointer')
          for(var i=0; i<pins.length; ++i){
            pins[i].style.backgroundImage = 'url('+pinVariants['pin_locate']+')';
          }
        }
        
        $scope.saveLocatingPosition = function(){
          var site = $scope.locating;
          stopLocating();
          site.Latitude = $scope.contextMenu.lat;
          site.Longitude = $scope.contextMenu.lon;
          site.LocationVerified = true;
          site.updatedLonLat = true;
          if(site.loadingAddress){
            geocodeAddress(site, function(){}, true);
          }
          addSiteToMap(site);
          showLocationInfo(site);
          
        }

        $scope.cancelLocating = function(){
          var site = $scope.locating;
          stopLocating();
          site.loadingAddress = false;
          positionMapOnSite(site);
          showLocationInfo(site);
        }

        function getSupportedTransform() {
            var prefixes = 'transform WebkitTransform MozTransform OTransform msTransform'.split(' ');
            for(var i = 0; i < prefixes.length; i++) {
                if(document.createElement('div').style[prefixes[i]] !== undefined) {
                    return prefixes[i];
                }
            }
            return false;
        }
        var canvasTries = 0;
        var fixCanvasSize = function(){ //this hack is needed to support old android versions
            if(getSupportedTransform() !== 'transform'){
                var canvases = document.querySelectorAll('.ol-layer canvas')
                if(!canvases.length){
                    if(canvasTries < 5){
                        ++canvasTries;
                        $timeout(fixCanvasSize)
                    }
                    return;
                }
            
                canvases.forEach(function(canvas){
                    canvas.style.width = '100%';
                    canvas.style.height = '100%';
                })
            }
        }

        var markerStyles = {}
        var getMarkerStyle = function(color, variant, opacity){
            variant = variant?('_'+variant):'';
            color = (color.indexOf('#')>-1?'#':'')+color.replace('#','').toUpperCase();
            opacity = opacity?opacity:1;
            if(!markerStyles[color+variant+opacity]){
                markerStyles[color+variant+opacity] = new ol.style.Style({
                  image: new ol.style.Icon({
                    anchor: [0.5, 1],
                    color: color,
                    scale: 0.1,
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    src: pinVariants['pin'+variant],
                    opacity: opacity
                  })
                });
            }
            return markerStyles[color+variant+opacity];
        }
        var setMarkerStyle = function(feature, color){
            var site = feature.get('site');
            feature.setStyle(getMarkerStyle(color?color:site.LocationVerified?$scope.colors.verified:$scope.colors.unverified, site.InexactAddressMatch?'notice':'', site.locating?0.5:1));
        }

        var createNewPin = function(site){
          var latLon = geolocation.getPosition();
          var marker = new ol.Feature({
            geometry: new ol.geom.Point(latLon),
            site: site
          });
          if(!site.Latitude||!site.Longitude){
            var ll = ol.proj.toLonLat(latLon);
            site.Latitude = ll[1];
            site.Longitude = ll[0];
          }
          site.pin = marker;
          setMarkerStyle(marker);
          markers.addFeature(marker);
        }
        var addSiteToMap = function(site){
            var lonLat = new ol.proj.fromLonLat([site.Longitude, site.Latitude]);
            if(site.pin){
                site.pin.getGeometry().setCoordinates(lonLat);
            }else{
                var marker = new ol.Feature({
                  geometry: new ol.geom.Point(lonLat),
                  site: site
                });
                site.pin = marker;
                setMarkerStyle(marker);
                markers.addFeature(marker);
            }
        }

        var positionMapOnSite = function(site){
          map.getView().setCenter(ol.extent.getCenter(site.pin.getGeometry().getExtent()));
          if(jobSitesTabbar.getActiveTabIndex() != 0){
              jobSitesTabbar.setActiveTab(0);
          }
        }
        $scope.locatePin = function(site){
          stopLocating();
          site.loadingAddress = false;
          if(site.status == 'map'){
              positionMapOnSite(site);
              showLocationInfo(site);
          }else if(site.status == 'error'){
              $scope.editSiteAddress(site);
          }
        }
        var unselectMarker = function(){
            if(selectedMarker){
                setMarkerStyle(selectedMarker);
                selectedMarker = null;
            }
        }
        var showLocationInfo = function(location){
          var selected = selectedMarker;
          if(selectedMarker === location.pin){
            map.getView().setCenter(location.pin.getGeometry().getCoordinates());
          }else{
            $scope.contextMenu = null;
            unselectMarker();
            selectedMarker = location.pin;
            setMarkerStyle(selectedMarker, '#FF0000')
            var coordinates = selectedMarker.getGeometry().getCoordinates();
            $timeout(function(){
                $scope.popupSite = location;
                overlay.setPosition(coordinates);
            });
          }
        }
        var showContextMenu = function(data){
            $timeout(function(){
                $scope.contextMenu = data;
                overlay.setPosition(data.coordinates);
            });
            
        }

        $scope.showJobDescription = function(job){
          Utils.notification.hideToast();
          Utils.notification.toast(job.Description, {timeout:2000});
        }

        $scope.editSite = function(site){
            dialogs.site.open(site);
        }
        $scope.editSiteAddress = function(site){
            dialogs.address.open(site);
        }

        $scope.nav = function(location){
            var url;
            var address = encodeURI(location.Latitude+','+location.Longitude);
            if(device.platform == 'Android'){
                if(location.Address && location.PostalCode){
                    address = encodeURI(location.Address+', '+location.PostalCode);
                }
                url = 'geo:0,0?q=' + address;
                window.open(url, '_system');
            }else{
                url = 'maps://?q='+address;
                window.open(url, '_system');
            }
        }

        var lonLatRegExp = [/^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/,/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/]
        var validateLonLat = function(site){
            if(   (site.Longitude && site.Longitude.toString().match(lonLatRegExp[0]))
               &&  (site.Latitude && site.Latitude.toString().match(lonLatRegExp[1]))){
                return true;
            }
            return false;
        }
        var geocodeORS = function(location, callback){
            var address = location.LocationPostalCode;
            if(location.LocationAddress){
                address = location.LocationAddress+', '+address;
            }
            ApiService.get('geocodeORS', {text: address}).then(function(resp){
                if(resp && resp.features){
                    location.status = 'map';
                    location.updatedLonLat = true;
                    location.Longitude = resp.features[0].geometry.coordinates[0];
                    location.Latitude = resp.features[0].geometry.coordinates[1];
                    addSiteToMap(location);
                }
                callback();
            });
        }

        var getAddressComponent = function(components, componentType){
            var component;
            if(component = components.find(function(c){ return c.types.indexOf(componentType) > -1 })){
                return component;
            }
            return {short_name:'',long_name:''}
        }
        var mapGMResults = function(item){
            var c = item.address_components;
            var address = getAddressComponent(c, 'street_number').long_name+' '+getAddressComponent(c, 'route').short_name,
                postal = getAddressComponent(c, 'postal_code').long_name,
                city = getAddressComponent(c, 'locality').short_name,
                state = getAddressComponent(c, 'administrative_area_level_1').short_name
            return {
                Lat:item.geometry.location.lat,
                Lon: item.geometry.location.lng,
                Address: address,
                PC: postal,
                City: city,
                State: state,
                Part: item.partial_match||(!address||!postal||!city||!state)
            } 
        }
        var geocodeGM = function(site, callback, latLng){
            var req = {}
            if(latLng){
              req.latlng = site.Latitude+','+site.Longitude;
            }else{
              req.address = (site.Address||'');
              if(site.City){
                  req.address += (site.Address?', ':'')+site.City;
              }
              if(site.PostalCode){
                  req.address += ', '+site.PostalCode;
              }
            }
            ApiService.get('geocodeGM', req).then(function(resp){
                if(resp){
                    if(resp.status == 'OK'){
                        if(resp.results && resp.results.length){
                            var results = resp.results.map(mapGMResults);
                            if(results.length > 0){//== 1){
                                site.status = 'map';
                                if(latLng){
                                  site.Address = results[0].Address;
                                }else{
                                  site.updatedLonLat = true;
                                  site.Longitude = results[0].Lon;
                                  site.Latitude = results[0].Lat;
                                }
                                site.PostalCode = results[0].PC;
                                site.City = results[0].City;
                                site.StateCode = results[0].State;
                                site.InexactAddressMatch = results[0].Part;
                                site.loadingAddress = false;
                                addSiteToMap(site);
                           //TODO (maybe): add to list, have user choose best match
                           // }else{
                           //     location.pendingAddresses = results;
                            }
                        }
                    }
                }
                callback();
            });
        }
        var geocodeAddress = function(location, callback, latLng){
            geocodeGM(location, callback, latLng);
        }
        var getSiteAddressObj = function(site){
            var obj = {}
            if(site.JobShowUpSiteID) obj.ID = site.JobShowUpSiteID;
            if(site.Longitude) obj.Lon = site.Longitude.toString();
            if(site.Latitude) obj.Lat = site.Latitude.toString();
            if(site.Address) obj.Add = site.Address;
            if(site.City) obj.City = site.City;
            if(site.StateCode) obj.State = site.StateCode;
            if(site.PostalCode) obj.PC = site.PostalCode;
            obj.LV = !!site.LocationVerified;
            obj.Name = site.Name;
            obj.JobID = site.job.ID;
            if(site.InexactAddressMatch){
              obj.Part = true;
            }
            return obj;
        }
        var saveLonLats = function(){
            var sites = $scope.job.Site.reduce(function(sites, loc){
                if(loc.updatedLonLat && loc.JobShowUpSiteID){
                    loc.updatedLonLat = false;
                    sites.push(getSiteAddressObj(loc));
                }
                return sites;
            }, [])
            if(sites.length){
                ApiService.post('showup', {route:'updateLonLat',Site:sites})
            }
        }
        var tryToSelect = function(site){
          if(map){
            $scope.locatePin(site);
          }else{
            setTimeout(function(){
              tryToSelect(site);
            }, 100);
          }
        }
        var initSites = function(){
            var sites = $scope.job.Site;
            var lookup = []
            var count = 0
            var selected = null;
            cachedSites = Utils.copy(sites);
            for(var l=0; l<sites.length; ++l){
                if(!sites[l].pin){
                    ++count;
                    if(validateLonLat(sites[l])){
                        sites[l].status = 'map';
                        addSiteToMap(sites[l]);
                        if(sites[l].selected){
                          selected = sites[l];
                          sites[l].selected = false;
                        }
                    }else{
                        if(sites[l].PostalCode||sites[l].City){
                            lookup.push(sites[l]);
                            sites[l].status = 'loading';
                        }else{
                            sites[l].status = 'error';
                        }
                    }
                }
            }
            if(lookup.length){
                var count = lookup.length;
                var done = function(){
                    --count;
                    if(!count){
                        saveLonLats();
                    }
                    initMapPosition();
                }
                for(var l=0; l<lookup.length; ++l){
                    geocodeAddress(lookup[l], done);
                }
            }else{
              initMapPosition();
            }
            if(selected||(sites.length==1 && sites[0].status == 'map')){
              tryToSelect(selected||sites[0]);
            }
            return count;
        }



        ons.createAlertDialog('app/showup/showup-site-dialog.html').then(function(dialog){
            dialogs.site = dialog;
            dialog.open = function(site){
                dialog._scope.parent = $scope.parent;
                dialog._scope.job = $scope.job;
                dialog._scope.site = site;
                Utils.openDialog(dialog);
            }
            dialog._scope.close = function(){
              dialog.hide();
            }
            dialog._scope.editLatLon = function(){
              beginLocatingSite(dialog._scope.site);
              dialog.hide();
            }
            dialog._scope.editAddress = function(){
              $scope.editSiteAddress(dialog._scope.site);
            }
        });

        $scope.openPostalSearch = function(callback){
           if(Smartpick.ok()){
                Smartpick.show();
            }else{
                Smartpick.show({
                    endpoint: 'postalSearch',
                    itemView: 'PostalCode',
                    itemTemplate: 'postal-code.html',
                    title: 'City / Zip',
                    filters: [
                        {key: 'Country', from: 'ID', to:'StateCountryID', display:'Name'},
                        {key: 'State', from: 'ID', to:'StateStateID', display:'Name', dependencies:{'Country':{from:'CountryID',to:'ID'}}}
                    ]
                }, callback);
            }
        }
        ons.createDialog('app/mapping/route-address-dialog.html').then(function(dialog){
            dialogs.address = dialog;
            dialog._scope.data = {
                State: States
            }
            dialog.open = function(location){
                dialog._scope.job = $scope.job;
                dialog._scope.form = {
                    address: location.Address||'',
                    city: location.City||'',
                    postal: location.PostalCode||'',
                    state: location.StateCode?dialog._scope.data.State.find(function(s){return s.Code==location.StateCode}):null
                }
                dialog._scope.loading = false;
                dialog._scope.items = []
                dialog._scope.showItems = false;

                dialog._scope.location = location;
                Utils.openDialog(dialog);
            }
            dialog._scope.openPostalSearch = function(){
               $scope.openPostalSearch(function(item){
                    dialog._scope.form.postal = item.PostalCode;
                });
            }
            dialog._scope.executeSearch = function(){
                var form = dialog._scope.form;
                if(!form.address.trim() || (form.postal.length<5&&!form.city.trim()&&!form.state)){
                    return;
                }
                dialog._scope.loading = true;
                var address = form.address;
                if(form.city) address += ', '+form.city;
                if(form.state) address += ', '+form.state.Code;
                var obj = {address: address}
                if(form.postal){
                    obj.components = 'postal_code:'+form.postal
                }
                ApiService.get('geocodeGM', obj).then(function(resp){
                    if(resp){
                        dialog._scope.showItems = true;
                        if(resp.status == 'OK'){
                            if(resp.results && resp.results.length){
                                dialog._scope.items = resp.results.map(mapGMResults);
                            }
                        }
                    }
                    dialog._scope.loading = false;
                });
            }
            dialog._scope.selectItem = function(item){
                var location = dialog._scope.location;
                var pin = location.pin;
                location.status = 'map';
                location.updatedLonLat = true;
                location.Longitude = item.Lon;
                location.Latitude = item.Lat;
                location.Address = item.Address;
                location.City = item.City;
                location.StateCode = item.State;
                location.PostalCode = item.PC;
                location.InexactAddressMatch = item.Part;
                addSiteToMap(location);
                saveLonLats();
                dialog.hide();

                if(pin && $scope.popupSite === location){
                    setMarkerStyle(pin, '#FF0000')
                }
                if(jobSitesTabbar.getActiveTabIndex() == 0){
                    $scope.locatePin(location);
                }
            }
        });


        var result = function(resp){
          if(resp){
              if(resp.Job){
                var p = $scope.job.process;
                var jobs =  Utils.enforceArray(resp.Job);
                $scope.job = Utils.pick(jobs[0],'ID','NumberChar','Description','WorkgroupJobNumber');
                $scope.job.process = p;
                $scope.job.multiple = jobs.length>1;
                $scope.job.Site = [];
                for(var j=0; j<jobs.length; ++j){
                  jobs[j].Site = Utils.enforceArray(jobs[j].Site);
                  var tempJob = Utils.pick(jobs[j],'ID','NumberChar','Description','WorkgroupJobNumber');
                  for(var s=0; s<jobs[j].Site.length; ++s){
                    jobs[j].Site[s].job = tempJob;
                    $scope.job.Site.push(jobs[j].Site[s]);
                  }
                }
              }
          }
          $scope.working = false;
          $scope.opening = false;
          initSites();
        }
        var load = function(){
          if(!$scope.job.Site){
            var obj = {
                route: 'sites',
                job: $scope.job.ID
            };
            ApiService.get('showup', obj).then(result, result);
            return;
          }
          $scope.working = false;
          $scope.opening = false;
          initSites();
        }

        var compareProperties = ['JobShowUpSiteID','Name','LocationVerified','Longitude','Latitude','Address','City','StateCode','PostalCode','InexactAddressMatch'];
        var getDiffs = function(){
          var diffs = [];
          for(var i=0; i<$scope.job.Site.length; ++i){
            for(var j=0; j<compareProperties.length; ++j){
              if($scope.job.Site[i][compareProperties[j]] != cachedSites[i][compareProperties[j]]){
                diffs.push(getSiteAddressObj($scope.job.Site[i]));
                break;
              }
            }
          }
          return diffs;
        }
        $scope.saveAndClose = function(){
          var result = function(){
            if($scope.current.updateSites)$scope.current.updateSites($scope.job.Site.map(function(site){ return Utils.pickProps(site, compareProperties) }));
            $scope.close(true);
          }
          var changes = getDiffs();
          if(changes.length){
            $scope.loading = true;
            $scope.working = true;
            var obj = {
                route: 'sites',
                Site: changes
            };
            ApiService.post('showup', obj).then(result, result);
          }else{
            $scope.close(true);
          }
        }
        $scope.close = function(force){
          if(!force){
            var changes = getDiffs();
            if(changes.length){
              Utils.notification.confirm("Save changes?", {
                  callback: function(ok){
                    if(ok == 0){
                      $scope.saveAndClose();
                    }else if(ok == 1){
                      $scope.close(true);
                    }
                  },
                  buttonLabels: ["Save","Discard","Cancel"]
              });
              return;
            }
          }
          if($scope.job.process && $scope.job.process.name == 'EndDay'){
            $scope.loading = true;
            TECompletionService.checkNextJob($scope, $scope.job.process, function(){
              $scope.loading = false;
            });
          }else if($scope.job.process && $scope.job.process.name == 'GT'){
            $scope.$parent.current.ftDone = true;
            mainNav.replacePage('app/timesheet/gt-work-day.html');
          }else{
            mainNav.popPage();
          }
        }
        $scope.endDay = function(){
          $scope.close();
        }

        var backButton = function(){
          $scope.close();
        }
        var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', backButton, false);
            }else{
                document.removeEventListener('backbutton', backButton);
                ons.enableDeviceBackButtonHandler();
            }
        }
        $scope.hidePage = function(event){
          if(event.target.id != 'jobSitesPage') return;
          toggleBackButton(false);
          Utils.clearBackButtonHandlers()
        }
        $scope.showPage = function(event){
          if(event.target.id != 'jobSitesPage') return;
            toggleBackButton(true);
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })
        }

        $scope.$on('$destroy', function(){
            var dialogKeys = Object.keys(dialogs);
            for(var k=0; k<dialogKeys.length; ++k){
                var dialog = dialogs[dialogKeys[k]];
                if(dialog.visible){
                    dialog.hide();
                }
                if(dialog.saveState){
                    StorageService.state.save(dialog.saveState.name, dialog._scope, dialog.saveState.fields)
                }
            }
            Smartpick.reset();
            Smartpick.hide();
        });

        var init = function(){
            initMap();
            load();
            $timeout(function(){
                ons.findComponent('#showup-back-button')._element[0].onClick = backButton;
            },1);
        }

        $timeout(init);



    }]);


    
})();
(function(){
    'use strict';

    angular.module('app').controller('InspectLocationController', ['$scope', 'CommentDialog', 'ApiService', 'Utils', '$rootScope', '$timeout', function($scope, CommentDialog, ApiService, Utils, $rootScope, $timeout) {
        $scope.job = $scope.$parent.current.job;
        $scope.design = $scope.$parent.current.design;
        $scope.tab = $scope.$parent.current.tab;
        $scope.reviewing = false;
        $scope.infoDialog = {};
        $scope.selectMode = false;
        ons.createDialog('app/dialog/location-detail-info-dialog.html').then(function(dialog){
            $scope.infoDialog.dialog = dialog;
        });
 
        var resetDialog = function(){
            CommentDialog.hide();
            $scope.selectMode = false;
        }
        var selectCallback = function(){
            $scope.selectMode = true;
            $scope.location.LocationDetail.forEach(function(ld){
                if(ld.inspectable && !ld.ChangeOrderQty)
                    ld.selected = true;
            });
            CommentDialog.hide();
        }
        var dialogOptions = {
            title: 'Accept Multiple',
            accept: function(comment, notUsed, items){
                reviewAll('accept', comment, items);
                resetDialog();
            },
            requireComment: false,
            placeholder: 'Comment (optional)',
            button: 'accept',
            cancel: function(){
                resetDialog();
            },
            select: {
                callback: selectCallback
            }
        };

        var sortDetails = function(){
            $scope.working = true;
            if($scope.location && $scope.location.LocationDetail){
                $scope.location.LocationDetail.sort(function(a,b){
                    if(a.IncompleteInspectionFlag < b.IncompleteInspectionFlag) return 1;
                    if(a.IncompleteInspectionFlag > b.IncompleteInspectionFlag) return -1;
                    
                    if(a.ActionWorkAction < b.ActionWorkAction) return -1;
                    if(a.ActionWorkAction > b.ActionWorkAction) return 1;
                    
                    if(a.SpecificationSpecificationNumber < b.SpecificationSpecificationNumber) return -1;
                    if(a.SpecificationSpecificationNumber > b.SpecificationSpecificationNumber) return 1;

                    return 0;
                });
            }
            $scope.working = false;
        };

        var cleanDetail = function(ld){
            if(ld.AcceptInspectionButton == 'Accept'){
                ld.inspectable = true;
            }
            ld.LDChangeOrder = Utils.enforceArray(ld.LDChangeOrder);
            if(ld.LDChangeOrder.length && ld.LDChangeOrder.find(function(co){ return co.StatusCode!='AP'&&co.StatusCode!='AI' })){
                ld.changeOrder = true;
            }
            ld.PercentComplete = Utils.fixPercent(ld.PercentComplete);
            ld.IncompleteInspectionFlag = Utils.fixUnwantedBool(ld.IncompleteInspectionFlag);
            ld.CompleteDate = new Date(ld.CompleteDate);
            return ld;
        }

        var cleanDetails = function(){
            $scope.location.LocationDetail = Utils.enforceArray($scope.location.LocationDetail);
            $scope.location.LocationDetail.map(cleanDetail);
        };

        var fetchDetails = function(){
            $scope.location = $scope.design.Location[mainNav.topPage.pushedOptions.locationIndex];
            $scope.working = true;
            ApiService.get('locationInspection', {location: $scope.location.JobLocationID}).then(fetchResult, fetchResult)
        };

        var fetchResult = function(resp){
            if(resp && resp.Location && resp.Location.LocationDetail){
                $scope.location.LocationDetail = resp.Location.LocationDetail;
                $scope.location.lastDetailFetch = new Date().getTime();
                cleanDetails();
            }
            $scope.working = false;
        };

        var getSelectedItems = function(){
            return $scope.location.LocationDetail.filter(function(ld){
                return ld.selected;
            })
        }
        $scope.canAcceptMulti = function(details){
            if(!details)
                return false;
            var canMultiAccept = details.filter(function(ld){return ld.inspectable&&ld.ChangeOrderQty==0});
            return canMultiAccept.length > 0;
        }
        $scope.canFinishSelect = function(details){
            if(!details)
                return false;
            var canMultiAccept = details.filter(function(ld){return ld.inspectable&&ld.ChangeOrderQty==0&&ld.selected});
            return canMultiAccept.length == 0;
        }
        $scope.inspectAllDialog = function(){
            if($scope.selectMode){
                dialogOptions.select.items = getSelectedItems();
                CommentDialog.show(dialogOptions);
            }else{
                selectCallback();
            }
           
        }
        $scope.cancelSelect = function(){
            resetDialog()
        }

        var reviewAll = function(action, comment, items){
            comment = comment||'';
            var obj = {location:$scope.location.JobLocationID, action:action, comment:comment, details:true};
            if(items && items.length){
                obj.items = items.map(function(ld){return ld.JobLocationDetailID}).join(',');
            }
            ApiService.post('inspectLocation', obj).then(reviewAllResult,reviewAllResult);
            $scope.reviewing = true;
        };

        var updateBadgeAndSort = function(){
            var ready = $scope.location.LocationDetail.filter(function(ld){return ld.inspectable});
            if(!ready.length){
                if($scope.job.badge){
                    $scope.job.badge--;
                    if(!$scope.job.badge){
                        $scope.tab.jobs.sort(Utils.multiSort({name:'LocationsToInspect',primer:function(val){return val>0?0:1;}},$scope.OrgType=='OW'?'JobNumber':'WorkgroupJobNumber'));
                    }
                }
            }
            $scope.location.InspectableDetails = ready.length;
        }

        var reviewAllResult = function(resp){
            if(resp && resp.Location){
                $scope.design.Location[mainNav.topPage.pushedOptions.locationIndex] = resp.Location;
                $scope.location = $scope.design.Location[mainNav.topPage.pushedOptions.locationIndex];
                $scope.location.IncompleteInspectionFlag = Utils.fixUnwantedBool(resp.Location.IncompleteInspectionFlag);
                $scope.location.lastDetailFetch = new Date().getTime();
                $scope.location.CompleteDate = $scope.location.CompleteDate?new Date($scope.location.CompleteDate):'';
                $scope.location.PercentComplete = Math.round($scope.location.PercentComplete*10000)/100;
                cleanDetails();
                updateBadgeAndSort();
            }
            $scope.reviewing = false;
        };

        var reviewDetail = function(action, detail, comment, photoUrl){
            comment = comment||'';
            var obj = {detail:detail.JobLocationDetailID, action:action, comment:comment}, files = {};
            if(photoUrl)
                files = {photo:photoUrl};
            ApiService.getFormData(obj,files).then(function(formdata){
                ApiService.post('inspectLocationDetail', obj, '', '', formdata)
                .then(function(resp){
                    reviewResult(detail, resp);
                },function(resp){
                    reviewResult(detail, resp);
                });
            });
            $scope.reviewing = true;
        };

        var reviewResult = function(detail, resp){
            if(resp && resp.Location){
                detail.InspectionPromptCodeCalc = resp.Location.LocationDetail.InspectionPromptCodeCalc;
                detail.IncompleteInspectionFlag = Utils.fixUnwantedBool(resp.Location.LocationDetail.IncompleteInspectionFlag);
                detail.inspectable = resp.Location.LocationDetail.AcceptInspectionButton == 'Accept';
                detail.PercentComplete = Utils.fixPercent(resp.Location.LocationDetail.PercentComplete);
                detail.CompleteDate = new Date(resp.Location.LocationDetail.CompleteDate);
                $scope.location.IncompleteInspectionFlag = Utils.fixUnwantedBool(resp.Location.IncompleteInspectionFlag);
                $scope.location.needsSort = true;
                updateBadgeAndSort();
            }
            $scope.reviewing = false;
        };

        $scope.detailApprove = function(detail, $event){
            if(detail.ChangeOrderQty){
                $scope.detailComment(detail, $event, 'Accept Detail','accept', 'Comment (required due to change order)');
            }else{
                reviewDetail('accept', detail);
            }
        };

        $scope.detailReject = function(detail, $event){
            $scope.detailComment(detail, $event, 'Reject Detail','reject');
        };

        $scope.detailComment = function(detail, $event, title, action, placeholder){
            title = title || 'Accept/Reject Detail';
            CommentDialog.show({
                title: title,
                accept: function(comment, photoUrl){
                    if(comment)
                        reviewDetail('accept', detail, comment, photoUrl);
                    CommentDialog.hide();
                },
                reject: function(comment, photoUrl){
                    if(comment)
                        reviewDetail('reject', detail, comment, photoUrl);
                    CommentDialog.hide();
                },
                otherHTML: detail.Comments ? ('<b>Completion Comments:</b> '+detail.Comments) : '',
                placeholder: placeholder,
                button: action,
                camera: true
            });
        };

        $scope.undoInspection = function(detail, $event){
            var obj = {detail:detail.JobLocationDetailID, action:'undo'};
            ApiService.post('inspectLocationDetail', obj).then(function(resp){reviewResult(detail, resp)}, function(resp){reviewResult(detail, resp)});
            $scope.reviewing = true;
            $event.stopPropagation();
        }

        $scope.openLocationDetail = function(detail){
            if($scope.selectMode){
                detail.selected = !detail.selected;
            }
        }

        $scope.detailOnLongPress = function(detail) {
            if(!$scope.reviewing){
                navigator.vibrate(10);
                detail.JobBillingUponCode = $scope.design.JobBillingUponCode;
                $rootScope.$emit('LocationDetailDialog', {type:'init', detail: detail, endpoint: 'locationDetailInspection'});
                Utils.openDialog($scope.infoDialog.dialog);
            }
        }

        $scope.getInspectionStatus = function(detail){
            if(detail.InspectionPromptCodeCalc == 'RI'){
                return 'Ready for Inspection';
            }
            if(detail.InspectionPromptCodeCalc == 'RJ'){
                return 'Rejected';
            }
            if(detail.InspectionPromptCodeCalc == 'IC'){
                return 'Inspection Complete';
            }
            return '';
        };

        $scope.getDetailClass = function(detail){
            if(detail.InspectionPromptCodeCalc == 'RI'){
                return 'green';
            }
            if(detail.InspectionPromptCodeCalc == 'RJ'){
                return 'red';
            }
            if(detail.InspectionPromptCodeCalc == 'IC'){
                return 'faded';
            }
            return '';
        }

        var init = function(){
            var now = new Date().getTime();
            if(!$scope.location || !$scope.location.LocationDetail || ($scope.location.lastDetailFetch-now > 1000*60*10)){
                fetchDetails();
            }else if($scope.location.needsSort){
                sortDetails();
            }
        };

        $timeout(init);

        $scope.$on('$destroy', function(){
            if($scope.infoDialog.dialog.visible){
                $scope.infoDialog.dialog.hide();
            }
            CommentDialog.hide();
            $rootScope.$emit('LocationDetailDialog', {type:'destroy'});
        });
        
    }]);

})();
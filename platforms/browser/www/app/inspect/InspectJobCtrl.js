(function(){
    'use strict';

    angular.module('app').controller('InspectJobController', ['$scope', 'ApiService', 'Utils', '$rootScope', 'CommentDialog', function($scope, ApiService, Utils, $rootScope, CommentDialog) {
        $scope.job = $scope.$parent.current.job;
        $scope.design = $scope.job.design;
        $scope.tab = $scope.$parent.current.tab;
        $scope.reviewing = false;
        $scope.infoDialog = {};
        ons.createDialog('app/dialog/location-info-dialog.html').then(function(dialog){
            $scope.infoDialog.dialog = dialog;
        });

        var loadInspectionJob = function(includeCompleted){
            $scope.working = true;
            var obj = {design: $scope.job.ActiveJobDesignID};
            if(includeCompleted)
                obj.complete = true;
            ApiService.get('jobInspection', obj).then(result, result)
        }

        var cleanLocation = function(location){
            location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
            location.PercentComplete = Utils.fixPercent(location.PercentComplete);
            location.IncompleteInspectionFlag = Utils.fixUnwantedBool(location.IncompleteInspectionFlag);
            location.LInspection = Utils.enforceArray(location.LInspection);
            if(location.LInspection.length && location.LInspection[0].Accept == 'Accept'){
                location.inspectable = true;
            }
            return location;
        };
        var cleanResponse = function(design){
            design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
            design.Location = Utils.enforceArray(design.Location);
            design.Location.map(cleanLocation);
            design.PercentComplete = Utils.fixPercent(design.PercentComplete);
            return design;
        };

        var result = function(resp){
            $scope.working = false;
            if(resp && resp.JobDesign){
                $scope.job.design = cleanResponse(resp.JobDesign);
                $scope.design = $scope.job.design;
                $scope.showLoadMore = $scope.design.Location.length==$scope.design.TotalCount?false:true;
                $scope.design.lastLocationFetch = new Date().getTime();
            }else{
                ons.notification.alert("Failed to load job data", {title: "Error"});
                mainNav.popPage();
            }
        };

        $scope.loadMore = function(){
            loadInspectionJob(true);
        }

        $scope.goToLocation = function(index, location){
            $scope.current.design = $scope.design;
            mainNav.pushPage('app/inspect/inspect-location.html', { locationIndex: index });
        };

        var reviewLocation = function(action, location, comment, photoUrl){
            comment = comment||'';
            var obj = {location:location.JobLocationID, action:action, comment:comment}, files = {};
            if(!$scope.showLoadMore)
                obj.complete = true;
            if(photoUrl)
                files = {photo:photoUrl};
            ApiService.getFormData(obj,files).then(function(formdata){
                ApiService.post('inspectLocation', obj, '', '', formdata)
                .then(function(resp){
                    reviewResult(location, resp);
                },function(resp){
                    reviewResult(location, resp);
                });
            }, function(){
                $scope.reviewing = false;
            });
            $scope.reviewing = true;
        };

        var reviewResult = function(location, resp){
            if(resp && resp.JobDesign && resp.JobDesign.Location){
                for(var i=0; i<$scope.design.Location.length; i++){
                    if($scope.design.Location[i].JobLocationID == resp.JobDesign.Location.JobLocationID){
                        $scope.design.Location[i] = cleanLocation(resp.JobDesign.Location);
                        if($scope.design.Location[i].StatusCode != 'PE' && $scope.job.badge){
                            $scope.job.badge--;
                            if(!$scope.job.badge){
                                $scope.tab.jobs.sort(Utils.multiSort({name:'LocationsToInspect',primer:function(val){return val>0?0:1;}},$scope.OrgType=='OW'?'JobNumber':'WorkgroupJobNumber'));
                            }
                        }

                        break;
                    }
                }
            }
            $scope.reviewing = false;
        };

        $scope.undoInspection = function(location, $event){
            var obj = {location:location.JobLocationID, action:'undo'};
            ApiService.post('inspectLocation', obj).then(function(resp){reviewResult(location, resp)}, function(resp){reviewResult(location, resp)});
            $scope.reviewing = true;
            $event.stopPropagation();
        }

        $scope.locationApprove = function(location, $event){
            if(location.ChangeOrderDetails){
                $scope.locationComment(location, $event, 'Accept Location','accept', 'Comment (required due to change order)');
            }else{
                reviewLocation('accept', location);
            }
            $event.stopPropagation();
        };

        $scope.locationReject = function(location, $event){
            $scope.locationComment(location, $event, 'Reject Location','reject');
            $event.stopPropagation();
        };

        $scope.locationComment = function(location, $event, title, action, placeholder){
            title = title || 'Accept/Reject Location';
            CommentDialog.show({
                title: title,
                accept: function(comment, photoUrl){
                    if(comment)
                        reviewLocation('accept', location, comment, photoUrl);
                    CommentDialog.hide();
                },
                reject: function(comment, photoUrl){
                    if(comment)
                        reviewLocation('reject', location, comment, photoUrl);
                    CommentDialog.hide();
                },
                placeholder: placeholder,
                button: action,
                camera: true
            });
            $event.stopPropagation();
        };

        $scope.getLocationClass = function (location){
            if(location.IncompleteInspectionFlag == 'A'){
                return 'faded'
            }
            if(location.IncompleteInspectionFlag == 'R'){
                return 'red'
            }
            if(location.IncompleteInspectionFlag == 'Y'){
                return 'green'
            }
            return ''
        };

        $scope.locationOnLongPress = function(location) {
            navigator.vibrate(10);
            location.JobBillingUponCode = $scope.design.JobBillingUponCode;
            $rootScope.$emit('LocationDialog', {type:'init', location: location, endpoint: 'locationInspection'});
            Utils.openDialog($scope.infoDialog.dialog);
        }

        $scope.getLocationNumber = function(location){
            return location.NumberChar;
        }

        var init = function(){
            var now = new Date().getTime();
            if(!$scope.design || ($scope.design.lastLocationFetch-now > 1000*60*10)){
                loadInspectionJob();
            }else{
                $scope.showLoadMore = $scope.design.Location.length==$scope.design.TotalCount?false:true;
            }
        };

        $scope.$on('$destroy', function(){
            if($scope.infoDialog.dialog.visible){
                $scope.infoDialog.dialog.hide();
            }
            CommentDialog.hide();
            $rootScope.$emit('LocationDialog', {type:'destroy'});
        });

        init();
    }]);

})();
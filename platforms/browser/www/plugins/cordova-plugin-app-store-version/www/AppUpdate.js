cordova.define("cordova-plugin-app-store-version.AppUpdate", function(require, exports, module) { var exec = require('cordova/exec');

exports.checkAppUpdate = function(success, error, packageName) {
    console.log("checkAppUpdate.js called!");
    exec(success, error, "AppUpdate", "checkAppUpdate",  [packageName]);
};

});

(function(){
    'use strict';

    angular.module('app').service('TimesheetService', ['Utils', '$filter', 'ApiService', '$timeout', function(Utils, $filter, ApiService, $timeout){
        
        var _calcJobEquipNumbers = function(equip, dayIndex, workweek){
          var weekEquipIndex = workweek.equipment.findIndex(function(eq){ return eq.ID == equip.EquipmentID });
          var weekEquip = workweek.equipment[weekEquipIndex];
          var dayJobEquipTotal = Math.max(equip.DirectHoursTotal, equip.DirectHours)+equip.IndirectHours;
          equip.TotalHours = dayJobEquipTotal;
          weekEquip.TotalHours += dayJobEquipTotal;
          weekEquip.days[dayIndex].TotalHours += dayJobEquipTotal;
        }
        var _getJobBucket = function(job, workweek){
          return workweek.TimeBucket.find(function(b){ return b.OverheadJobID == (job.JobID||job.ID) });
        }
        var _getBucketJob = function(bucket, workweek){
          return workweek.WorkDay[0].Job.find(function(j){ return bucket.OverheadJobID == j.ID });
        }
        var _jobHoursExcept = function(job, which){
          var total = 0;
          for(var t=0; t<_timeTypes.length; ++t){
            if(_timeTypes[t] != which && (t<2 || job.ContractAllowDoubleTimeFlag || job.AllowOHDoubleTimeFlag)){
              total += (job['Direct'+_timeTypes[t]+'HoursTotal']||0)+(job['Indirect'+_timeTypes[t]+'Hours']||0);
            }
          }
          return total;
        }
        var _getBucketJobHoursExcluding = function(job, bucket, timeType, dayIndex, workweek){
          var total = 0, period;
          if(workweek.WorkDay[dayIndex].WeekdayDate > bucket.Period1End){
            total = bucket.Period2Hours;
            period = 2;
          }else{
            total = bucket.Period1Hours;
            period = 1;
          }
          for(var d=0; d<workweek.WorkDay.length; ++d){
            if((period == 1 && workweek.WorkDay[d].WeekdayDate <= bucket.Period1End) || (period == 2 && workweek.WorkDay[d].WeekdayDate > bucket.Period1End) ){
              for(var j=0; j<workweek.WorkDay[d].WorkDayJob.length; ++j){
                if(workweek.WorkDay[d].WorkDayJob[j].TimeBucketID == bucket.OrgTimeBucketID){
                  if(!workweek.WorkDay[d].WorkDayJob[j].JobDayCrewMemberID){ //Don't count jobs linked to foreman time...already counted from PQ code
                    for(var t=0; t<_timeTypes.length; ++t){
                      if((workweek.WorkDay[d].WorkDayJob[j] !== job || _timeTypes[t] != timeType) && (t<2 || workweek.WorkDay[d].WorkDayJob[j].ContractAllowDoubleTimeFlag || workweek.WorkDay[d].WorkDayJob[j].AllowOHDoubleTimeFlag)){
                        total += (workweek.WorkDay[d].WorkDayJob[j]['Direct'+_timeTypes[t]+'HoursTotal']||0)+(workweek.WorkDay[d].WorkDayJob[j]['Indirect'+_timeTypes[t]+'Hours']||0);
                      }
                    }
                  }
                }
              }
            }
          }
          return total;
        }
        var _calcTimeBucket = function(job, dayIndex, workweek){
          var bucket = _getJobBucket(job, workweek);
          if(bucket){
            if(workweek.WorkDay[dayIndex].WeekdayDate > bucket.Period1End){
              bucket.totalPeriod2Hours -= job.TotalHours;
            }else{
              bucket.totalPeriod1Hours -= job.TotalHours;
            }
          }
        }
        var _resetBuckets = function(workweek){
          for(var b=0; b<workweek.TimeBucket.length; b++){
            workweek.TimeBucket[b].totalPeriod1Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period1Hours;
            workweek.TimeBucket[b].totalPeriod2Hours = workweek.TimeBucket[b].HourLimit-workweek.TimeBucket[b].Period2Hours;
          }
        }
        var _calcDayJobNumbers = function(job, dayIndex, workweek){
          job.TotalHours = Math.max(job.DirectHoursTotal, job.DirectHours)+job.IndirectHours;
          job.STHours = Math.max(job.DirectSTHoursTotal, job.DirectSTHours)+job.IndirectSTHours;
          job.OTHours = Math.max(job.DirectOTHoursTotal, job.DirectOTHours)+job.IndirectOTHours;
          job.DTHours = Math.max(job.DirectDTHoursTotal, job.DirectDTHours)+job.IndirectDTHours;
          job.PerDiem = Math.max(job.DirectPerDiemTotal, job.DirectPerDiem)+job.IndirectPerDiem;
          for(var e=0; e<job.WorkDayJobEquipment.length; e++){
            var equip = job.WorkDayJobEquipment[e];
            _calcJobEquipNumbers(equip, dayIndex, workweek);
          }
          if(job.TimeBucketID){
            _calcTimeBucket(job, dayIndex, workweek);
          }
        }
        var _calcWeekDayNumbers = function(day, dayIndex, workweek){
          var hours = [0,0,0,0], perDiem = 0;
          for(var i=0; i<day.WorkDayJob.length; i++){
            var job = day.WorkDayJob[i], st=0, ot=0, dt=0;
            hours[1] += st=Math.max(job.DirectSTHoursTotal, job.DirectSTHours)+job.IndirectSTHours;
            hours[2] += ot=Math.max(job.DirectOTHoursTotal, job.DirectOTHours)+job.IndirectOTHours;
            hours[3] += dt=Math.max(job.DirectDTHoursTotal, job.DirectDTHours)+job.IndirectDTHours;
            hours[0] += st+ot+dt;
            perDiem += Math.max(job.DirectPerDiemTotal, job.DirectPerDiem)+job.IndirectPerDiem;
            _calcDayJobNumbers(job, dayIndex, workweek);
          }
          day.WeekdayHours = Utils.round(hours[0], 2);
          day.STHours = Utils.round(hours[1], 2);
          day.OTHours = Utils.round(hours[2], 2);
          day.DTHours = Utils.round(hours[3], 2);
          day.TotalPerDiem = Utils.round(perDiem, 2);
          workweek.TotalHours += day.WeekdayHours;
          workweek.STHours += day.STHours;
          workweek.OTHours += day.OTHours;
          workweek.DTHours += day.DTHours;
          workweek.PerDiemTotal += day.TotalPerDiem;
        }

        var _calcWeekNumbers = function(workweek){
          _resetBuckets(workweek);
          workweek.TotalHours = 0;
          workweek.STHours = 0;
          workweek.OTHours = 0;
          workweek.DTHours = 0;
          workweek.EquipmentHoursTotal = 0;
          workweek.PerDiemTotal = 0;
          for(var i=0; i<workweek.jobs.length; i++){
            workweek.jobs[i].TotalPerDiem = 0;
            workweek.jobs[i].TotalHours = 0;
          }
          for(var i=0; i<workweek.equipment.length; i++){
            workweek.equipment[i].TotalHours = 0;
            for(var j=0; j<workweek.equipment[i].days.length; j++){
              if(workweek.equipment[i].days[j])
                workweek.equipment[i].days[j].TotalHours = 0;
            }
            for(var j=0; j<workweek.jobs.length; j++){
              if(workweek.equipment[i].jobs[j]){
                workweek.equipment[i].jobs[j].Total = 0;
                for(var k=0; k<workweek.WorkDay.length; k++){
                  if(workweek.equipment[i].jobs[j].days[k]){
                    workweek.equipment[i].jobs[j].days[k].TotalHours = 0;
                  }else{
                    workweek.equipment[i].jobs[j].days[k] = {TotalHours: 0};
                  }
                }
              }
            }
          }
          for(var i=0; i<workweek.WorkDay.length; i++){
            _calcWeekDayNumbers(workweek.WorkDay[i], i, workweek);
          }
          for(var e=0; e<workweek.equipment.length; e++){
            workweek.EquipmentHoursTotal += workweek.equipment[e].TotalHours;
          }
        }
        var _setUpCrossReferences = function(workweek){
          for(var i=0; i<workweek.equipment.length; i++){
            workweek.equipment[i].TotalHours = 0;
            for(var j=0; j<7; j++){
              workweek.equipment[i].days[j] = workweek.WorkDay[j].WorkDayEquipment.find(function(eq){ return eq.EquipmentID == workweek.equipment[i].ID});
            }
            for(var j=0; j<workweek.jobs.length; j++){
              var equip;
              for(var d=0; d<7; d++){
                var job = workweek.WorkDay[d].WorkDayJob.find(function(jb){return jb.JobID == workweek.jobs[j].ID});
                if(job){
                  equip = job.WorkDayJobEquipment.find(function(eq){return eq.EquipmentID == workweek.equipment[i].ID});
                  break;
                }
              }
              if(equip){
                workweek.equipment[i].jobs[j] = {Total: 0, days:[]};
                for(var k=0; k<workweek.WorkDay.length; k++){
                  workweek.equipment[i].jobs[j].days[k] = {TotalHours: 0};
                }
              }
            }
          }
        }
         var _fixOnHoldJob = function(job, workweek){
            if(job.OnHoldFlag){
                job.StatusCode = 'OH';
                job.StatusMeaning = 'On Hold';
            }
        }
        var _cleanJob = function(job, workweek){
          var bucket = _getJobBucket(job, workweek);
          if(bucket){
            job.TimeBucketID = bucket.OrgTimeBucketID;
          }
          if(job.JobPhoto){
              workweek.Photos.Job[job.ID] = Utils.enforceArray(job.JobPhoto);
              delete job.JobPhoto;
          }
          if(job.ActualStartDate){
            job.ActualStartDate = Utils.date(job.ActualStartDate);
          }
          _fixOnHoldJob(job, workweek);
          var status = workweek.jobStatuses.find(function(js){ return js.Code == job.StatusCode });
          if(!status){
            workweek.jobStatuses.push({Code:job.StatusCode, Meaning:job.StatusMeaning});
          }
        }
        var _setUpJobs = function(workweek, addJobs){
          workweek.Photos = workweek.Photos||{Job:{}}
          workweek.Job = Utils.enforceArray(workweek.Job);
          workweek.jobStatuses = [{Code:'ALL',Meaning:'All'}];
          for(var j=0; j<workweek.Job.length; j++){
            var job = workweek.Job[j];
            _cleanJob(job, workweek);
          }
          for(var d=0; d<7; d++){
            var jobs;
            if(!workweek.WorkDay[d].Job){
                jobs = Utils.copy(workweek.Job);
                workweek.WorkDay[d].Job = jobs;
            }else if(addJobs){
                jobs = Utils.copy(addJobs);
                var args = [workweek.WorkDay[d].Job.length,0].concat(jobs);
                Array.prototype.splice.apply(workweek.WorkDay[d].Job, args);
            }else{
                jobs = workweek.WorkDay[d].Job;
            }
            var types = [];
            for(var j=jobs.length-1; j>=0; --j){
              for(var i=0; i<workweek.WorkDay[d].WorkDayJob.length; i++){
                if(jobs[j].ID == workweek.WorkDay[d].WorkDayJob[i].JobID){
                  if(workweek.WorkDay[d].WorkDayJob[i].CrewEquipmentFlag){
                    jobs[j].crewEquipSelected = true;
                  }else{
                    jobs[j].selected = true;
                  }
                  if(jobs[j].TimesheetPerDiemAllowed != workweek.WorkDay[d].WorkDayJob[i].JobTimesheetPerDiemAllowed){
                    workweek.WorkDay[d].WorkDayJob[i].JobTimesheetPerDiemAllowed = jobs[j].TimesheetPerDiemAllowed
                  }
                  if(jobs[j].TimeBucketID){
                    workweek.WorkDay[d].WorkDayJob[i].TimeBucketID = jobs[j].TimeBucketID;
                  }
                  jobs[j].checkbox = jobs[j].checkbox2 = true
                  jobs[j].order = workweek.WorkDay[d].WorkDayJob[i].OrderNumber;
                  jobs[j].hasDirect = (workweek.WorkDay[d].WorkDayJob[i].JobDayCrewMemberID||workweek.WorkDay[d].WorkDayJob[i].DirectSTHours||workweek.WorkDay[d].WorkDayJob[i].DirectOTHours||workweek.WorkDay[d].WorkDayJob[i].DirectDTHours||!!workweek.WorkDay[d].WorkDayJob[i].WorkDayJobEquipment.find(function(eq){ return eq.DirectHours>0; }));
                  workweek.WorkDay[d].hasDoubleTime = workweek.WorkDay[d].hasDoubleTime||workweek.WorkDay[d].WorkDayJob[i].DTHours>0||jobs[j].ContractAllowDoubleTimeFlag||jobs[j].AllowOHDoubleTimeFlag;
                  found = true;
                  break;
                }
              }
              if(!found){
                if(jobs[j].Type == 'DJ'){
                  jobs.splice(j, 1);
                  continue;
                }
                if(jobs[j].ActualStartDate && jobs[j].ActualStartDate > workweek.WorkDay[d].WeekdayDate){
                  jobs.splice(j, 1);
                  continue;
                }
              }
              var type = jobs[j].Type == 'DJ'?'OT':jobs[j].Type, found = false;
              if(!types.includes(type)){
                types.push(type);
              }
            }
            if(!workweek.WorkDay[d].currentJobType && types.includes('AJ') && ['AJ','BO'].includes(workweek.PersonTimesheetJobTypeCode)){
              workweek.WorkDay[d].currentJobType = 'AJ';
            }
            if(!workweek.WorkDay[d].currentJobType && types.includes('OJ') && ['OJ','OP'].includes(workweek.PersonTimesheetJobTypeCode)){
              workweek.WorkDay[d].currentJobType = 'OJ';
            }
            if(!workweek.WorkDay[d].currentJobType && types.includes('OT')){
              workweek.WorkDay[d].currentJobType = 'OT';
            }
            if(!workweek.WorkDay[d].currentJobType){
              workweek.WorkDay[d].currentJobType = types.includes('AJ')?'AJ':(types.includes('OJ')?'OJ':(types.includes('OJ')?'OJ':'AJ'));
            }
          }
        }

        var _trackEquip = function(workweek, equip){
          var weekEquip = workweek.equipment.find(function(eq){ return eq.ID == equip.EquipmentID });
          if(!weekEquip){
            weekEquip = {ID: equip.EquipmentID, Number: equip.EquipmentEquipmentNumber, Description: equip.EquipmentDescription, Category: equip.CategoryCategory, Mine:equip.MyEquipment, TotalHours: 0, days:[], jobs:[]};
            workweek.equipment.push(weekEquip);
            workweek.hasCrewEquip = workweek.hasCrewEquip||(equip.MyEquipment=='');
          }
        }
        var _calcShowUpSites = function(workweek){
            var day = workweek.WorkDay[workweek.currentDay];
            delete day.showUpSites;
            var jobs = day.WorkDayJob;
            var tempJob;
            var sites = jobs.reduce(function(sites, job){ 
                if(job.active && (tempJob=workweek.Job.find(function(jb){
                    return jb.ID==job.JobID
                })) && tempJob.USUS){
                    sites.push(tempJob);
                } 
                return sites; 
            }, []);
            if(sites.length){
               day.showUpSites = sites; 
            }
        }
        var _cleanReferenceData = function(workweek){
            workweek.sp = workweek.sp || {};
            if(workweek.TimeWorkWeek){
                workweek.sp.timeWorkWeek = Utils.enforceArray(workweek.TimeWorkWeek);
                delete workweek.TimeWorkWeek;
            }
            workweek.TimeBucket = Utils.enforceArray(workweek.TimeBucket);
            for(var b=0; b<workweek.TimeBucket.length; ++ b){
              workweek.TimeBucket[b].Period1Start = Utils.date(workweek.TimeBucket[b].Period1Start);
              workweek.TimeBucket[b].Period1End = Utils.date(workweek.TimeBucket[b].Period1End);
              if(workweek.TimeBucket[b].Period2Start){
                workweek.TimeBucket[b].Period2Start = Utils.date(workweek.TimeBucket[b].Period2Start);
                workweek.TimeBucket[b].Period2End = Utils.date(workweek.TimeBucket[b].Period2End);
              }
            }
            workweek.Comm = Utils.enforceArray(workweek.Comm);
            for(var i=0; i<workweek.Comm.length; ++i){
              workweek.Comm[i].StartDate = Utils.date(workweek.Comm[i].StartDate);
              workweek.Comm[i].ExpirationDate = Utils.date(workweek.Comm[i].ExpirationDate);
              if(workweek.Comm[i].Owner){
                workweek.Comm[i].Owner = Utils.enforceArray(workweek.Comm[i].Owner);
              }
            }
        }
        var _hideBuckets = function(workweek){
          for(var b=0; b<workweek.TimeBucket.length; ++ b){
              if(workweek.TimeBucket[b].VisibleOnGTFlag){
                var job = _getBucketJob(workweek.TimeBucket[b], workweek);
                if(!job){
                  workweek.TimeBucket[b].VisibleOnGTFlag = false;
                }
              }
            }
        }
        var _cleanWorkweek = function(workweek, scope){
            _cleanReferenceData(workweek);
            workweek.Code = (workweek.Code||workweek.CrewWorkWeekCode||'F8');
            workweek.timeWorkWeek = workweek.sp.timeWorkWeek.find(function(tww){ return tww.SmartCode == (workweek.Code||'') });

            var autoSave = false;
            workweek.PayrollPeriodEndDate = new Date(workweek.PayrollPeriodEndDate);
            workweek.PayrollPeriodStartDate = new Date(workweek.PayrollPeriodEndDate);
            workweek.PayrollPeriodStartDate.addDays(-6);
            workweek.PersonTimesheetJobTypeCode = workweek.PersonTimesheetJobTypeCode||'AJ';

            workweek.filters = workweek.filters || {};
            workweek.jobs = [];
            workweek.equipment = [];
            workweek.hasCrewEquip = false;
            for(var i=0; i<workweek.WorkDay.length; i++){
              var day = workweek.WorkDay[i];
              day.WorkDayEquipment = Utils.enforceArray(day.WorkDayEquipment);
              for(var e=0; e<day.WorkDayEquipment.length; e++){
                day.WorkDayEquipment[e].active = true;
                _trackEquip(workweek, day.WorkDayEquipment[e]);
              }
              day.WorkDayJob = Utils.enforceArray(day.WorkDayJob);
              if(_canSubmitIfStatus.includes(workweek.StatusCode)&&workweek.MyPersonID==workweek.PersonID&&_canSubmitIfStatus.includes(day.StatusCode)){
                day.addingJob = (typeof day.addingJob == 'undefined')?(day.WorkDayJob.length==0):day.addingJob;
              }
              var doReview = false;
              for(var j=0; j<day.WorkDayJob.length; j++){
                var job = day.WorkDayJob[j];
                job.active = (typeof job.active=='undefined')?true:job.active;
                var weekJob = workweek.jobs.find(function(jb){ return jb.ID == job.JobID });
                if(!weekJob){
                  weekJob = {ID: job.JobID, Number: job.JobJobNumberChar, WGNumber: job.JobWorkgroupJobNumber, StatusCode: job.JobStatusCode, TotalHours: 0, TotalPerDiem: 0, days:[], AssignedFlag: job.AssignedFlag, TypeCode:job.TypeCode, index:workweek.jobs.length};
                  workweek.jobs.push(weekJob);
                }
                if(job.PQChange){
                    autoSave = true;
                    workweek.changes = true;
                    if(day.StatusCode != 'IP'){
                        doReview = true;
                    }
                }
                job.WorkDayJobEquipment = Utils.enforceArray(job.WorkDayJobEquipment);
                for(var e=0; e<job.WorkDayJobEquipment.length; e++){
                  var equip = job.WorkDayJobEquipment[e];
                  _trackEquip(workweek, equip);
                  if(equip.JobDayCrewEquipmentID) job.hasDirectEquip = true;
                  if(equip.PQChange){
                    autoSave = true;
                    workweek.changes = true;
                    if(day.StatusCode != 'IP'){
                      doReview = true;
                    }
                  }
                }
              }
              day.WeekdayDate = new Date(day.WeekdayDate);
              if(doReview){
                _review(workweek, 'IP', false, i);
              }
            }
            _setUpWorkWeekDays(workweek);
            _setUpCrossReferences(workweek);
            _setUpJobs(workweek);
            _hideBuckets(workweek);
            for(var i=0; i<7; i++){
            if(_canSubmitIfStatus.includes(workweek.WorkDay[i].StatusCode)){
                _reAllocateJobs(workweek, i);
                 _reAllocateJobsPD(workweek, i);
              }
             }
            _calcWeekNumbers(workweek);
            _checkChangeTimeCode(workweek);

            if(typeof workweek.currentDay == 'undefined' && workweek.InitDate){
                workweek.currentDay = workweek.WorkDay.findIndex(function(day){ return day.WeekdayDate == workweek.InitDate; })||0;
            }else{
                workweek.currentDay = workweek.currentDay||0;
            }
            _calcShowUpSites(workweek);

            if(autoSave){
                return scope.save();
            }
            if(!scope.original){
              scope.original = Utils.copy(workweek);
            }
            if(scope.current && scope.current.original){
              scope.current.original = scope.original
            }
        }

        var _updateDayComms = function(workweek){
            var day = workweek.WorkDay[workweek.currentDay];
            day.Comm = []
            day.showSafety = false;
            for(var i=0; i<workweek.Comm.length; ++i){
                var com = workweek.Comm[i];
                if(day.WeekdayDate > com.ExpirationDate || day.WeekdayDate < com.StartDate) continue;
                if(!com.SafetyCommunicationAllOwnersFlag){
                    var ok = false;
                    for(var j=0; j<day.WorkDayJob.length; ++j){
                      if(day.WorkDayJob[i].active && com.Owner.find(function(owner){ return owner.ID == day.WorkDayJob[j].JobOwnerID })){
                        ok = true;
                        continue;
                      }
                    }
                    if(!ok) continue;
                }
                day.Comm.push(com);
                day.showSafety = day.showSafety||!com.Person.ReviewedFlag;
            }
        }
        var _checkRequiredComms = function(workweek, dayIndex){
          if(typeof dayIndex == 'undefined'){
            for(var d = 0; d < 7; ++d){
              if(_checkRequiredComms(workweek, d)){
                return true;
              }
            }
            return false;
          }
          var day = workweek.WorkDay[dayIndex];
          var comms = day.Comm;
          if(comms && comms.length){
            for(var c=0; c<comms.length; ++c){
              if(comms[c].CompletionTypeCode=='REQ' && comms[c].Block){
                if(!comms[c].Person.ReviewedFlag){
                  return true;
                }
              }
            }
          }
          return false;
        }

        var _setUpWorkWeekDays = function(workweek){
          var workWeekDays = _getWorkWeekDays(workweek);
          workweek.stdHours = [];
          for(var i=0; i<7; ++i){
            if(i < workWeekDays.daysPerWeek){
              workweek.stdHours[i] = workWeekDays.hrsPerDay;
            }else{
              workweek.stdHours[i] = 0;
            }
          }
        }

        var _checkChangeTimeCode = function(workweek){
            workweek.canChangeTimeCode = true;
            /*for(var d=0; d<workweek.WorkDay.length; ++ d){
                if(!['PE','IP'].includes(workweek.WorkDay[d].StatusCode)){
                    workweek.canChangeTimeCode = false;
                    return;
                }
            }*/
        }

        var _updateJobPhoto = function(wdJob, job, workweek){
            if(workweek.Photos.Job[job.ID]){
                wdJob.photos = true;
            }else{
                if(job.Design){
                    wdJob.photos = false;
                }else{
                   delete wdJob.photos; 
                }
                
            }
        }
        var _updateJobPhotos = function(jobId, workweek){
            var job = workweek.WorkDay[workweek.currentDay].Job.find(function(j){ return j.ID == jobId });
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                for(var j=0; j<day.WorkDayJob.length; ++j){
                    if(day.WorkDayJob[j].JobID == jobId){
                        _updateJobPhoto(day.WorkDayJob[j], job, workweek);
                    }
                }
            }
        }

        var _addCrewEquipment = function(scope, equip){
          var workweek = scope.workweek
          for(var i=0; i<workweek.WorkDay.length; i++){
            var day = workweek.WorkDay[i];
            var newEquip = {
              ID: _getTempID(),
              EquipmentID: equip.EquipmentID,
              EquipmentCategoryID: equip.EquipmentCategoryID,
              CategoryCategory: equip.CategoryCategory,
              EquipmentEquipmentNumber: equip.EquipmentEquipmentNumber,
              EquipmentDescription: equip.EquipmentDescription,
              CategorySortOrder: equip.CategorySortOrder,
              MyEquipment: equip.MyEquipmentFlag||'',
              DirectHours: 0,
              DirectHoursTotal: '',
              IndirectHours: 0,
              TotalHours: 0
            };
            day.WorkDayEquipment.push(newEquip);
            day.WorkDayEquipment.sort(Utils.multiSort({name:'MyEquipment',reverse:true},'CategorySortOrder','EquipmentEquipmentNumber'));
            _trackEquip(workweek, newEquip);
            for(var j=0; j<day.WorkDayJob.length; j++){
              var job = day.WorkDayJob[j];
              if(job.active){
                _addOneEquipmentToJob(day, job, newEquip, workweek);
                job.WorkDayJobEquipment.sort(Utils.multiSort({name:'MyEquipment',reverse:true},'CategorySortOrder','EquipmentEquipmentNumber'));
              }
            }
          }
           _cleanWorkweek(workweek, scope);
        }

        var _removeCrewEquipment = function(workweek, equip){
          for(var i=0; i<workweek.WorkDay.length; i++){
            var day = workweek.WorkDay[i];
            var oldEquipIndex = day.WorkDayEquipment.findIndex(function(eq){ return eq.EquipmentID == equip.EquipmentID });
            if(oldEquipIndex > -1){
              day.WorkDayEquipment[oldEquipIndex].active = false;
            }
            for(var j=0; j<day.WorkDayJob.length; j++){
              var job = day.WorkDayJob[j];
              if(job.active){
                var oldEquipIndex = job.WorkDayJobEquipment.findIndex(function(eq){ return eq.EquipmentID == equip.EquipmentID });
                if(oldEquipIndex > -1){
                  job.WorkDayJobEquipment.splice(oldEquipIndex, 1);
                }
              }
            }
          }
          var oldEquipIndex = workweek.equipment.findIndex(function(eq){ return eq.ID == equip.EquipmentID });
          if(oldEquipIndex > -1){
            workweek.equipment.splice(oldEquipIndex, 1);
          }
          //_cleanWorkweek(workweek);
        }

        var _modifyCrewEquipment = function(workweek, equip){
          for(var i=0; i<workweek.WorkDay.length; i++){
            var day = workweek.WorkDay[i];
            var oldEquip = day.WorkDayEquipment.find(function(eq){ return eq.EquipmentID == equip.EquipmentID });
            if(oldEquip){
              oldEquip.MyEquipment = equip.MyEquipmentFlag||'';
            }
            for(var j=0; j<day.WorkDayJob.length; j++){
              var job = day.WorkDayJob[j];
              if(job.active){
                var oldEquip = job.WorkDayJobEquipment.find(function(eq){ return eq.EquipmentID == equip.EquipmentID });
                if(oldEquip){
                  oldEquip.MyEquipment = equip.MyEquipmentFlag||'';
                }
              }
            }
          }
          var oldEquip = workweek.equipment.find(function(eq){ return eq.ID == equip.EquipmentID });
          if(oldEquip){
            oldEquip.Mine = equip.MyEquipmentFlag||'';
          }
          //_cleanWorkweek(workweek);
        }

        var _totalJobHours = function(job){
          job.DirectHours = (job.DirectSTHours||0) + (job.DirectOTHours||0) + (job.DirectDTHours||0);
          job.DirectHoursTotal = '';
          if(job.DirectSTHoursTotal !== '' || job.DirectOTHoursTotal !== '' || job.DirectDTHoursTotal !== ''){
            job.DirectHoursTotal = parseFloat(job.DirectSTHoursTotal||0)+parseFloat(job.DirectOTHoursTotal||0)+parseFloat(job.DirectDTHoursTotal||0);
          }
          job.IndirectHours = job.IndirectSTHours + job.IndirectOTHours + job.IndirectDTHours;
        }

        var _totalWeekdayHours = function(day){
          day.WeekdayHours = day.STHours + day.OTHours + day.DTHours;
        }

        var _enterMyHours = function(value, dayIndex, workweek, which){
          var jobDirect = 0, allocated = 0, day = workweek.WorkDay[dayIndex];
          var hasAllocatableJob = false, numJobs = day.WorkDayJob.filter(function(jb){return jb.active && !(jb.DirectSTHours+jb.DirectOTHours+jb.DirectDTHours) && !jb.JobDayCrewMemberID}).length;
          for(var i=0; i<day.WorkDayJob.length; i++){
            if(day.WorkDayJob[i].active){
              jobDirect += day.WorkDayJob[i]['Direct'+which+'Hours'];
              if(numJobs == 1 && day.WorkDayJob[i]['Direct'+which+'HoursTotal'] > day.WorkDayJob[i]['Direct'+which+'Hours']){
                //Clear out an override if there is only one job here
                day.WorkDayJob[i]['Direct'+which+'HoursTotal'] = '';
              }else{
                allocated += day.WorkDayJob[i]['Direct'+which+'HoursTotal']||0;
              }
              hasAllocatableJob = hasAllocatableJob || (!day.WorkDayJob[i].DirectSTHours&&!day.WorkDayJob[i].DirectOTHours&&!day.WorkDayJob[i].DirectDTHours&&day.WorkDayJob[i]['Direct'+which+'HoursTotal']=='' && !day.WorkDayJob[i].JobDayCrewMemberID);
            }
          }
          if(value < jobDirect || value < allocated){
            //report the error
            if(value < jobDirect){
              var msg = 'Hours cannot be less than direct Job hours recorded via End Of Day ('+$filter('number')(jobDirect,2)+')';
              _showMessage(msg)
            }
            value = Math.max(jobDirect, allocated);
          }else if(!hasAllocatableJob && jobDirect < value){
            value = jobDirect;
          }
          day[which+'Hours'] = value;
          _totalWeekdayHours(day);
          _reAllocateJobs(workweek, dayIndex);
          _reAllocateJobsPD(workweek, dayIndex);
        }
        var _enterMyPerDiem = function(value, dayIndex, workweek){
          var day = workweek.WorkDay[dayIndex], defaultPD = workweek.DailyPerDiem;
          if(defaultPD == ''){
             workweek.DailyPerDiem = workweek.PersonPerDiemAmount;
             defaultPD = workweek.DailyPerDiem;
          }
          day.PerDiemDirectTotal = value;
          if(value === ''){
            day.PerDiemTotal = defaultPD;
          }else{
            day.PerDiemTotal = value;
          }
          _reAllocateJobsPD(workweek, dayIndex);
        }
        var _enterMyEquipHours = function(value, dayIndex, workweek, equipID){
          var equipDirect = 0, allocated = 0, day = workweek.WorkDay[dayIndex], enteredValue = value;
          var equip = day.WorkDayEquipment.find(function(eq){return eq.EquipmentID == equipID});
          if(equip){
            for(var i=0; i<day.WorkDayJob.length; i++){
              for(var j=0; j<day.WorkDayJob[i].WorkDayJobEquipment.length; j++){
                equipDirect += day.WorkDayJob[i].WorkDayJobEquipment[j].DirectHours;
                allocated += day.WorkDayJob[i].WorkDayJobEquipment[j].DirectHoursTotal + day.WorkDayJob[i].WorkDayJobEquipment[j].IndirectHours;
              }
            }
            if(enteredValue !== '' && value < equipDirect){
              value = equipDirect;
              //report the error
              var msg = 'Hours cannot be less than direct Equipment hours recorded via End Of Day ('+$filter('number')(equipDirect,2)+')';
              _showMessage(msg)
            }
            if(equip.MyEquipment && enteredValue === ''){
                equip.DirectHoursTotal = '';
                equip.IndirectHours = day.WeekdayHours;
              }else{
              equip.DirectHoursTotal = value;
                equip.IndirectHours = 0;
            }
            _reAllocateJobs(workweek, dayIndex);
          }
        }
        var _enterJobHours = function(value, dayIndex, workweek, jobID, which){
          var job = workweek.WorkDay[dayIndex].WorkDayJob.find(function(jb){ return jb.JobID == jobID });
          if(job){
            var valueCompare = value||0;
            var msg = '', title;
            if(valueCompare < job['Direct'+which+'Hours']){
              msg = 'Direct Hours cannot be less than hours recorded via End Of Day ('+$filter('number')(job['Direct'+which+'Hours'], 2)+')';
              _showMessage(msg);
              value = ''; 
            }else{
              
              if(job.TypeCode == 'OJ' &&  job.JobTimesheetMaxHours !== ''){
                var jobHoursExceptMe = _jobHoursExcept(job, which);
                if(jobHoursExceptMe+valueCompare > job.JobTimesheetMaxHours){
                  msg = 'Daily hours capped at '+$filter('number')(job.JobTimesheetMaxHours, 2)+' for this job.';
                  value = Math.max(0, job.JobTimesheetMaxHours-jobHoursExceptMe);
                  valueCompare = value;
                }
              }
              if(job.TimeBucketID){
                var bucket = _getJobBucket(job, workweek);
                var bucketField = workweek.WorkDay[dayIndex] > bucket.Period1End ? 'totalPeriod2Hours':'totalPeriod1Hours';
                var hoursExceptMe = _getBucketJobHoursExcluding(job, bucket, which, dayIndex, workweek);
                if(hoursExceptMe+valueCompare > bucket.HourLimit){
                  msg = 'Hours for '+bucket.Name+' cannot exceed '+$filter('number')(bucket.HourLimit, 2)+' '+(bucket.IntervalMeaning.toString().toLowerCase())+'.';
                  title = 'Error';
                  value = Math.max(0, bucket.HourLimit-hoursExceptMe);
                }
              }
              if(msg){
                _showMessage(msg, title);
              }
            }
            job['Direct'+which+'HoursTotal'] = value;
            job['Indirect'+which+'Hours'] = 0;
            _totalJobHours(job);
            _reAllocateJobs(workweek, dayIndex, job.JobID);
            _reAllocateJobsPD(workweek, dayIndex);
          }
        }
        var _enterJobPerDiem = function(value, dayIndex, workweek, jobID){
          var defaultPD = workweek.DailyPerDiem, day = workweek.WorkDay[dayIndex], job = workweek.WorkDay[dayIndex].WorkDayJob.find(function(jb){ return jb.JobID == jobID }), directSoFar = 0;
          if(defaultPD == ''){
             workweek.DailyPerDiem = workweek.PersonPerDiemAmount;
             defaultPD = workweek.DailyPerDiem;
          }
          if(job.JobDayCrewMemberID && job.CanChangeDirect){
            var change = value-job.DirectPerDiem
            job.DirectPerDiem += change
          }
          for(var j=0; j<day.WorkDayJob; ++j){
            if(day.WorkDayJob[j].active && day.WorkDayJob[j].JobID != jobID){
              directSoFar += Math.max(day.WorkDayJob[j].DirectPerDiem, day.WorkDayJob[j].DirectPerDiemTotal);
            }
          }
          job.DirectPerDiemTotal = value;
            job.IndirectPerDiem = 0;
          if(day.PerDiemDirectTotal != '' && directSoFar+value > day.PerDiemDirectTotal){
            day.PerDiemDirectTotal = directSoFar+value;
            day.PerDiemTotal = directSoFar+value;
          }
          _reAllocateJobsPD(workweek, dayIndex);
        }
        var _enterEquipHours = function(value, dayIndex, workweek, jobID, equipID){
          var job = workweek.WorkDay[dayIndex].WorkDayJob.find(function(jb){ return jb.JobID == jobID });
          if(job){
            var equip = job.WorkDayJobEquipment.find(function(eq){ return eq.EquipmentID == equipID });
            if(equip){
              var compareVal = value||0;
              if(compareVal < equip.DirectHours && !equip.CanChangeDirect){
                  var msg = 'Direct Hours cannot be less than hours recorded via Foreman timesheet ('+$filter('number')(equip.DirectHours,2)+')';
                  _showMessage(msg);
                  value = '';
              }
              if(equip.JobDayCrewEquipmentID && equip.CanChangeDirect){
                equip.DirectHours = value;
              }
              equip.DirectHoursTotal = value;
              equip.IndirectHours = 0;
              _reAllocateJobs(workweek, dayIndex);
            }
        }
        }
        var _enterValue = function(value, type, dayIndex, workweek, jobID, equipID){
          var typeSplit = type.split(",");
          switch(typeSplit[0]){
            case 'MyHours':
              _enterMyHours(value, dayIndex, workweek, typeSplit[1]);
            break;
            case 'MyPerDiem':
              _enterMyPerDiem(value, dayIndex, workweek);
            break;
            case 'MyEquipHours':
              _enterMyEquipHours(value, dayIndex, workweek, equipID);
            break;
            case 'JobHours':
              _enterJobHours(value, dayIndex, workweek, jobID, typeSplit[1]);
            break;
            case 'JobPerDiem':
              _enterJobPerDiem(value, dayIndex, workweek, jobID);
            break;
            case 'EquipHours':
              _enterEquipHours(value, dayIndex, workweek, jobID, equipID);
            break;
          }
          _calcWeekNumbers(workweek);
        }

    var _getTempID = (function() {
      var current = 0;
      return function() {
        ++current;
        return '<!--'+current.toString(16)+'-->';
      };
    })();
    var _isTempID = function(id){
      return id.toString().match(/^<!--/i);
    }
    var _addOneEquipmentToJob = function(day, job, equip, workweek){
      var newEquip = {
        ID: _getTempID(),
        EquipmentID: equip.EquipmentID,
                EquipmentEquipmentNumber: equip.EquipmentEquipmentNumber,
                EquipmentDescription: equip.EquipmentDescription,
                EquipmentOrganizationID: equip.EquipmentOrganizationID,
                EquipmentRentalCompanyID: equip.EquipmentRentalCompanyID,
                EquipmentCategoryID: equip.EquipmentCategoryID,
                CategoryCategory: equip.CategoryCategory,
                CategorySortOrder: equip.CategorySortOrder,
                MyEquipment: equip.MyEquipment,
                CrewTime: 0
      }
      _resetEquipment(newEquip);
      _trackEquip(workweek, newEquip);
      job.WorkDayJobEquipment.push(newEquip);
    }
    var _addEquipmentToJob = function(day, job, workweek){
      job.WorkDayJobEquipment = Utils.enforceArray(job.WorkDayJobEquipment);
      if(job.TypeCode != 'OJ' || job.JobTimesheetEquipmentAllowed){
        for(var e=0; e<day.WorkDayEquipment.length; e++){
          _addOneEquipmentToJob(day, job, day.WorkDayEquipment[e], workweek);
        }
      }
    }
    var _resetEquipment = function(equip){
      equip.DirectHoursTotal = '';
            equip.DirectHours = 0;
            equip.IndirectHours = 0;
            equip.TotalHours = 0;
    }
    var _resetJob = function(job){
      job.DirectHoursTotal = '';
      job.DirectHours = 0;
      job.IndirectHours = 0;
      job.DirectPerDiemTotal = '';
      job.DirectPerDiem = 0;
      job.IndirectPerDiem = 0;
      job.TotalHours = 0;
      job.DirectSTHoursTotal = '';
      job.DirectSTHours = 0;
      job.IndirectSTHours = 0;
      job.STHours = 0;
      job.DirectOTHoursTotal = '';
      job.DirectOTHours = 0;
      job.IndirectOTHours = 0;
      job.OTHours = 0;
      job.DirectDTHoursTotal = '';
      job.DirectDTHours = 0;
      job.IndirectDTHours = 0;
      job.DTHours = 0;
      for(var e=0; e<job.WorkDayJobEquipment.length; e++){
        _resetEquipment(job.WorkDayJobEquipment[e]);
      }
    }
        var _addJobToDay = function(day, job, workweek){
          var newJob = {
            ID: _getTempID(),
            JobID: job.ID,
            JobOwnerID: job.OwnerID,
            JobContractID: job.ContractID,
            ContractAllowDoubleTimeFlag: job.ContractAllowDoubleTimeFlag,
            AllowOHDoubleTimeFlag: job.AllowOHDoubleTimeFlag,
            JobJobNumber: job.Number,
            JobWorkgroupJobNumber: job.WorkgroupJobNumber,
            JobJobNumberChar: job.NumberChar,
            JobDescription: job.Description,
            JobTimesheetEquipmentAllowed: job.TimesheetEquipmentAllowed,
            JobTimesheetMaxHours: job.TimesheetMaxHours,
            JobTimesheetPerDiemAllowed: job.TimesheetPerDiemAllowed,
            TypeCode: job.Type=='OJ'?'OJ':'AJ',
            CrewSTTime: 0,
            CrewOTTime: 0,
            CrewDTTime: 0,
            JobStatusCode: job.StatusCode,
            JobTypeCode: job.TypeCode,
            JobTypeMeaning: job.TypeMeaning,
            StatusMeaning: job.StatusMeaning,
            AssignedFlag: job.Type=='AJ',
            QuickAddJob: job.QuickAddJob,
            TimeBucketID: job.TimeBucketID,
            JobComment: ''
          }
          _addEquipmentToJob(day, newJob, workweek);
          day.WorkDayJob.push(newJob);
          return newJob;
        }
        var _getHoursBefore = function(workweek, dayIndex){
          var hours = [0,0,0];
          for(var i=0; i<dayIndex; i++){
            hours[0] += workweek.WorkDay[i].STHours;
            hours[1] += workweek.WorkDay[i].OTHours;
            hours[2] += workweek.WorkDay[i].DTHours;
          }
          return hours;
        }
        var _getWorkWeekDays = function(workweek){
          var returnValues = {};
          switch(workweek.Code){
            case 'STD': // Standard
            default:
              returnValues.daysPerWeek = workweek.PersonStandardWorkDays;
              returnValues.hrsPerDay =  workweek.PersonStandardWorkHours;
            break;
            case 'F10': // 4 x 10
              returnValues.daysPerWeek = 4;
              returnValues.hrsPerDay = 10;
            break;
            case 'F8': // 5 x 8
              returnValues.daysPerWeek = 5;
              returnValues.hrsPerDay = 8;
            break;
            case 'STORM': // 7 x 16
              returnValues.daysPerWeek = 7;
              returnValues.hrsPerDay = 16;
            break;
          }
          return returnValues;
        }
        var _defaultDayHours = function(workweek, dayIndex){
          var day = workweek.WorkDay[dayIndex];
          var hoursSoFar = _getHoursBefore(workweek, dayIndex), hours = [ 0, 0, 0 ];
          var workWeekDays = _getWorkWeekDays(workweek)
          day.WeekdayHours = workWeekDays.hrsPerDay;
          hours[0] = day.WeekdayHours;
          if(hoursSoFar[0]+day.WeekdayHours > 40){
            hours[0] = hoursSoFar[0] >= 40?0:(hoursSoFar[0]+day.WeekdayHours-40);
            hours[1] = day.WeekdayHours - hours[0];
          }
          if(hours[0] > workWeekDays.hrsPerDay){
            hours[0] = workWeekDays.hrsPerDay;
            hours[1] = day.WeekdayHours - workWeekDays.hrsPerDay;
          }
          day.STHours = hours[0];
          day.OTHours = hours[1];
          day.DTHours = hours[2];
        }
        var _updateJobStatuses = function(changedJobs, workweek, dayIndex){
          if(!changedJobs.length) return;
          for(var d=0; d<workweek.WorkDay.length; ++d){
            var jobs = workweek.WorkDay[d].Job;
            if(d==dayIndex) jobs = workweek.Job;
            for(var j=0; j<jobs.length; ++j){
              if(changedJobs.indexOf(jobs[j].ID) > -1){
                var job = workweek.WorkDay[dayIndex].Job.find(function(jb){ return jb.ID==jobs[j].ID });
                jobs[j].StatusCode = job.StatusCode;
                jobs[j].StatusMeaning = job.StatusMeaning;
                jobs[j].Type = job.Type;
              }
            }
          }
        }
        var _addRemoveJobs = function(workweek, dayIndex){
          var day = workweek.WorkDay[dayIndex], hasDoubleTime = false, changedJobs = [];
          var jobCount = 0;
          for(var j=0; j<day.Job.length; j++){
            var job = day.WorkDayJob.find(function(jb){return jb.JobID == day.Job[j].ID});
            if(day.Job[j].selected || day.Job[j].crewEquipSelected){
              if(['OH','PS'].includes(day.Job[j].StatusCode)){
                day.Job[j].StatusCode = 'PR';
                day.Job[j].StatusMeaning = 'In Process';
                changedJobs.push(day.Job[j].ID);
              }    
              if(!job || !job.active){
                if(!job){
                  job = _addJobToDay(day, day.Job[j], workweek);
                }else if(!job.ID){
                  job.ID = _getTempID()
                }
                workweek.changes = true;
                _resetJob(job);
                _updateJobPhoto(job, day.Job[j], workweek)
              }
              jobCount++;
              workweek.changes = workweek.changes||(job.CrewEquipmentFlag!=day.Job[j].crewEquipSelected);
              hasDoubleTime = hasDoubleTime||job.ContractAllowDoubleTimeFlag||job.AllowOHDoubleTimeFlag;
              job.active = true;
              job.OrderNumber = day.Job[j].order;
              job.CrewEquipmentFlag = day.Job[j].crewEquipSelected;
            }else{
              if(job && job.active){
                job.active = false;
                workweek.changes = true;
                _resetJob(job);
              }
            }
          }
          if(day.WeekdayHours == 0){
            _defaultDayHours(workweek, dayIndex);
          }
          day.hasDoubleTime = hasDoubleTime;
          _enterMyHours(day.STHours, dayIndex, workweek, 'ST');
          _enterMyHours(day.OTHours, dayIndex, workweek, 'OT');
          if(day.hasDoubleTime)_enterMyHours(day.DTHours, dayIndex, workweek, 'DT');
          _calcWeekNumbers(workweek);
          _updateJobStatuses(changedJobs, workweek, dayIndex);
          _calcShowUpSites(workweek);
          if(jobCount > 0 && day.StatusCode == 'ZH'){
            day.StatusCode = 'IP';
            day.StatusMeaning = _statusMeanings['IP'];
            if(workweek.StatusCode == 'ZH'){
              workweek.StatusCode = 'IP';
              workweek.StatusMeaning = _statusMeanings['IP'];
            }
          }
        }
        var _autoAddJobs = function(workweek, dayIndex){
          var day = workweek.WorkDay[dayIndex], hasJob = false, workWeekDays = _getWorkWeekDays(workweek);
          if(!['OJ','OP'].includes(workweek.PersonTimesheetJobTypeCode) || !['PE','IP'].includes(day.StatusCode) || ((dayIndex+1)>workWeekDays.daysPerWeek) || day.WorkDayJob.find(function(jb){ return jb.active && !jb.JobDayCrewMemberID })){
            _updateDayComms(workweek);
            return;
          }
          var order = 0;
          for(var j=0; j<day.Job.length; j++){
            if(day.Job[j].Default){
              day.Job[j].selected = true;
              day.Job[j].order = ++order;
              hasJob = true;
            }
          }
          if(hasJob){
            _addRemoveJobs(workweek, dayIndex);
            day.addingJob = false;
          }
          _updateDayComms(workweek);
        }

        var _compileJobPercentages = function(jobs, total){
          var count = jobs.length, result = {AllEqual: true, Jobs: []};
          var last = count ? jobs[0].CrewTime : 0;
          for(var j=0; j<count; j++){
            var job = {JobID: jobs[j].JobID};
            result.Jobs.push(job);
            job.Percentage = total>0 ? (jobs[j].CrewTime/total) : (1/count);
            if(last != jobs[j].CrewTime){
              result.AllEqual = false;
            }
            last = jobs[j].CrewTime;
          }
          return result;
        }
        var _trackJobPercentage = function(job, equipID, output, total){
          var equip;
          if(equipID){
            if((equip=job.WorkDayJobEquipment.find(function(eq){return eq.EquipmentID == equipID})) && equip.DirectHoursTotal === '' && (equip.MyEquipment!=job.CrewEquipmentFlag) && !(job.DirectSTHours+job.DirectOTHours+job.DirectDTHours) && !job.JobDayCrewMemberID){
                output[0].push({CrewTime: job.CrewTime, JobID: job.JobID});
                total[0] += job.CrewTime;
              }
          }else if(!job.CrewEquipmentFlag){
            for(var w=0; w<_timeTypes.length; w++){
              if(job['Direct'+_timeTypes[w]+'HoursTotal'] === '' && (w<2 || job.ContractAllowDoubleTimeFlag || job.AllowOHDoubleTimeFlag) && !(job.DirectSTHours+job.DirectOTHours+job.DirectDTHours) && !job.JobDayCrewMemberID){
                output[w].push({CrewTime: job['Crew'+_timeTypes[w]+'Time'], JobID: job.JobID});
                total[w] += job['Crew'+_timeTypes[w]+'Time'];
              }
            }
          }
        }
        var _jobPercentageComparators = [
          function(job, workweek){ return (['AJ','BO'].includes(workweek.PersonTimesheetJobTypeCode) && job.TypeCode == 'AJ' && job.AssignedFlag) || (['OJ','OP'].includes(workweek.PersonTimesheetJobTypeCode) && job.TypeCode == 'OJ') },
          function(job, workweek){ return ['BO','OP'].includes(workweek.PersonTimesheetJobTypeCode) && (job.TypeCode == 'OJ' || (job.TypeCode == 'AJ' && job.AssignedFlag)) },
          function(job, workweek){ return (job.TypeCode == 'AJ' && !job.AssignedFlag) }
        ]
        var _jobPercentages = function(equipID, dayIndex, workweek){
          var temp = equipID?[[]]:[[],[],[]], day = workweek.WorkDay[dayIndex], total = equipID?[0]:[0,0,0];
          
          var passes = 0;
          while(passes < _jobPercentageComparators.length && !temp[0].length){
            for(var j=0; j<day.WorkDayJob.length; j++){
              var job = day.WorkDayJob[j];
              if(job.active){
                if(_jobPercentageComparators[passes](job, workweek)){
                  _trackJobPercentage(job, equipID, temp, total);
                }
              }
            }
            ++passes;
          }
          //Process results
          var result = [];
          for(var i=0; i<temp.length; i++){
            result.push(_compileJobPercentages(temp[i], total[i]));
          }
          return result;
        }
        var _checkJobLimits = function(job, timeType, amount, dayIndex, workweek){
          var limit = false;
          var jobHours = _jobHoursExcept(job, timeType);
          if(job.JobTimesheetMaxHours !== '' && jobHours+amount > job.JobTimesheetMaxHours){
            amount =  Math.max(0, job.JobTimesheetMaxHours-jobHours);
            limit = true;
          }
          var bucket = _getJobBucket(job, workweek);
          if(bucket){
            var amountExceptMe = _getBucketJobHoursExcluding(job, bucket, timeType, dayIndex, workweek);
            if(amountExceptMe+amount > bucket.HourLimit){
              amount = Math.max(0, bucket.HourLimit-amountExceptMe);
              limit = true;
            }
          }
          if(limit){
            return amount;
          }
          return false;
        }
        var _recalcPercentages = function(percentages){
          var total = 0
          for(var j=0; j<percentages.Jobs.length; ++j){
            total += percentages.Jobs[j].Percentage
          }
          if(total > 0 && total < 1){
            for(var j=0; j<percentages.Jobs.length; ++j){
              if(percentages.Jobs[j].Percentage > 0){
                percentages.Jobs[j].Percentage /= total;
              }
            }
          }
        }
        var _timeTypes = ['ST','OT','DT'];
        var _reAllocateJobsInner = function(equipID, available, dayIndex, workweek){
          var percentages = _jobPercentages(equipID, dayIndex, workweek), day = workweek.WorkDay[dayIndex];
          //Clear other jobs
          for(var i=0; i<percentages.length; i++){
            for(var j=0; j<day.WorkDayJob.length; j++){
             var job = day.WorkDayJob[j];
             var jobPercentage = percentages[i].Jobs.find(function(jb){ return jb.JobID == job.JobID });
               if(!jobPercentage){
                 if(equipID){
                  var equip = job.WorkDayJobEquipment.find(function(eq){return eq.EquipmentID == equipID});
                  if(equip){
                     equip.IndirectHours = 0;
                  }
                }else{
                    job['Indirect'+_timeTypes[i]+'Hours'] = 0;
                }
               }
             }
           }

          for(var a=0; a<available.length; a++){
            available[a] = Math.max(available[a], 0);
          }
          var left = available;
          for(var i=0; i<percentages.length; i++){
            if(!percentages[i].Jobs.length)
              continue;
            var ignoreJobs = {}
            if(percentages[i].AllEqual){
              //We want to allocate 0.25 hrs at a time until all hours are used
              for(var j=0; j<day.WorkDayJob.length; j++){
                var job = day.WorkDayJob[j];
                var jobPercentage = percentages[i].Jobs.find(function(jb){ return jb.JobID == job.JobID });
                if(jobPercentage){
                  if(equipID){
                    var equip = job.WorkDayJobEquipment.find(function(eq){return eq.EquipmentID == equipID});
                    if(equip){
                       equip.IndirectHours = 0;
                    }
                  }else{
                    job['Indirect'+_timeTypes[i]+'Hours'] = 0;
                   }
                }
              }
              var lastLeft = 0;
              while(left[i] != lastLeft){
                lastLeft = left[i]
                for(var j=0; j<day.WorkDayJob.length; j++){
                  var job = day.WorkDayJob[j];
                  if(ignoreJobs[job.JobID]) continue;
                  var jobPercentage = percentages[i].Jobs.find(function(jb){ return jb.JobID == job.JobID });
                  var amountToSet = _checkJobLimits(job, _timeTypes[i], job['Indirect'+_timeTypes[i]+'Hours']+0.25, dayIndex, workweek);
                   if(jobPercentage && left[i] > 0){
                    if(amountToSet !== false){
                      ignoreJobs[job.JobID] = true;
                    }else{
                      if(equipID){
                        var equip = job.WorkDayJobEquipment.find(function(eq){return eq.EquipmentID == equipID});
                        if(equip){
                           equip.IndirectHours += 0.25;
                        }
                      }else{
                        job['Indirect'+_timeTypes[i]+'Hours'] += 0.25;
                       }
                      left[i] -= 0.25;
                    }
                  }
                }
              }
            }else{
              //We want to allocate based on percentages
              var jobsLimitReached = percentages[i].Jobs.length;
              while(jobsLimitReached > 0){
                jobsLimitReached = 0;
                var effectiveLeft = left[i];
                for(var j=0; j<percentages[i].Jobs.length; j++){
                  if(ignoreJobs[percentages[i].Jobs[j].JobID]) continue;
                  var job = day.WorkDayJob.find(function(jb){ return jb.JobID == percentages[i].Jobs[j].JobID})
                  if(job){
                    var amount;
                    if(j == percentages[i].Jobs.length-1){
                      amount = effectiveLeft[i];
                     }else{
                      amount = percentages[i].Jobs[j].Percentage * available[i];
                    }
                    amount = Math.round(amount*4)/4;
                    var amountToSet = _checkJobLimits(job, _timeTypes[i], amount, dayIndex, workweek);
                    if(amountToSet !== false){
                      amount = amountToSet;
                      ++jobsLimitReached;
                      ignoreJobs[job.JobID] = true;
                    }
                     if(equipID){
                      var equip = job.WorkDayJobEquipment.find(function(eq){return eq.EquipmentID == equipID});
                      if(equip){
                         equip.IndirectHours = amount;
                      }
                    }else{
                      job['Indirect'+_timeTypes[i]+'Hours'] = amount;
                    }
                    effectiveLeft = Math.max(effectiveLeft-amount, 0);
                    if(jobsLimitReached){
                      _recalcPercentages(percentages[i]);
                      left[i] = Math.max(left[i]-amount, 0);
                    }
                  }
                }
              }
            }
          }
          for(var j=0; j<day.WorkDayJob.length; j++){
            _totalJobHours(day.WorkDayJob[j]);

          }
        }
        var _totalHours = function(item){
          return Math.max(item.DirectHours, item.DirectHoursTotal)+item.IndirectHours;
        }
        var _reAllocateJobs = function(workweek, dayIndex, jobID){
          var day = workweek.WorkDay[dayIndex];
          var equipIDs = [''], available = [[day.STHours,day.OTHours,day.DTHours]];

          //Job available
          for(var j=0; j<day.WorkDayJob.length; j++){
            var job = day.WorkDayJob[j];
            if(job.active){
               available[0][0] -= Math.max(job.DirectSTHoursTotal, job.DirectSTHours);
               available[0][1] -= Math.max(job.DirectOTHoursTotal, job.DirectOTHours);
               available[0][2] -= Math.max(job.DirectDTHoursTotal, job.DirectDTHours);
            }
          }
          //Do main job allocation
          _reAllocateJobsInner(equipIDs[0], available[0], dayIndex, workweek);

          //Equip available
          var dayHours = _totalWeekDayHours(day);
          for(var j=0; j<day.WorkDayJob.length; j++){
            var job = day.WorkDayJob[j];
            console.log(job)
            if(job.active){
              for(var e=0; e<job.WorkDayJobEquipment.length; e++){
                var equip = job.WorkDayJobEquipment[e];
                var dayEquip = day.WorkDayEquipment.find(function(eq){return eq.EquipmentID == equip.EquipmentID}),
                    index = equipIDs.indexOf(equip.EquipmentID);
                if(index == -1){
                  index = equipIDs.length;
                  equipIDs.push(equip.EquipmentID);
                  available.push([(dayEquip.DirectHoursTotal === '' && dayEquip.MyEquipment)?dayHours:(Math.max(dayEquip.DirectHoursTotal,dayEquip.DirectHours)+dayEquip.IndirectHours)]);
                }
                available[index][0] -= Math.max(equip.DirectHoursTotal,equip.DirectHours)
              }
            }
          }
          if(jobID){
            for(var j=0; j<day.WorkDayJob.length; j++){
              var job = day.WorkDayJob[j];
              
              if(jobID == job.JobID){
                for(var e=0; e<job.WorkDayJobEquipment.length; e++){
                  var equip = job.WorkDayJobEquipment[e];
                  var jobHours = (job.DirectHoursTotal !== '')?(_totalHours(job)):'';
                  if(equip.DirectHours <= job.DirectHoursTotal && equip.MyEquipment){
                    var index = equipIDs.indexOf(equip.EquipmentID);
                    var change = Math.min(available[index][0]+equip.DirectHoursTotal,jobHours)-equip.DirectHoursTotal;
                    if(equip.DirectHoursTotal + change == 0){
                       equip.DirectHoursTotal = job.DirectHoursTotal === '' ? '' : 0;
                    }else{
                      if(!equip.DirectHoursTotal){
                        equip.DirectHoursTotal = 0;
                      }
                      equip.DirectHoursTotal += change;
                    }
                    available[index][0] -= change;
                  }
                }
              }
            }
          }
          //Equipment allocation
          for(var i=1; i<equipIDs.length; i++){
            _reAllocateJobsInner(equipIDs[i], available[i], dayIndex, workweek);
          }
        }
        var _totalWeekDayHours = function(day){
          var hours = 0;
          for(var i=0; i<day.WorkDayJob.length; i++){
            var job = day.WorkDayJob[i];
            if(job.active){
              hours += Math.max(job.DirectHoursTotal, job.DirectHours)+job.IndirectHours;
            }
          }
        return hours;
        }
        var _jobPDPercentages = function(dayIndex, workweek){
          var result = [], day = workweek.WorkDay[dayIndex], total = 0;
          //Should be weighted based on crew time
          for(var j=0; j<day.WorkDayJob.length; j++){
            var job = day.WorkDayJob[j];
            var hours = Math.max(job.DirectHours,job.DirectHoursTotal)+job.IndirectHours;
              if(job.active && hours > 0 && job.JobTimesheetPerDiemAllowed && (!job.DirectSTHours&&!job.DirectOTHours&&!job.DirectDTHours&&!job.JobDayCrewMemberID)){
                if(job.DirectPerDiemTotal === ''){
                      result.push({JobID: job.JobID, Percentage: hours});
                      total += hours;
                  }
              }
          }
          var count = result.length;
          for(var j=0; j<count; j++){
             result[j].Percentage = total ? (result[j].Percentage/total) : (1/count);
          }
          return result;
        }
        var _reAllocateJobsPD = function(workweek, dayIndex){
          var day = workweek.WorkDay[dayIndex];
          var available = day.PerDiemTotal, totalHours = 0;
          if(available == ''){
             workweek.DailyPerDiem = workweek.PersonPerDiemAmount;
             available = workweek.DailyPerDiem;
          }
          if(day.PerDiemDirectTotal !== ''){
            available = day.PerDiemDirectTotal;
          }
          for(var j=0; j<day.WorkDayJob.length; j++){
            var job = day.WorkDayJob[j];
            if(job.active){
                if(job.DirectPerDiemTotal && (job.DirectHours+job.DirectHoursTotal+job.IndirectHours) == 0){
                  job.DirectPerDiemTotal = '';
                }
                available -= Math.max(job.DirectPerDiemTotal, job.DirectPerDiem);
                totalHours += Math.max(job.DirectHours,job.DirectHoursTotal)+job.IndirectHours;
            }
          }
          if(available < 0 || totalHours == 0){ 
             available = 0;
          }
          if(totalHours < workweek.PersonMinHoursForPerDiem){
            available = 0;
          }
          var percentages = _jobPDPercentages(dayIndex, workweek);
          for(var j=0; j<day.WorkDayJob.length; j++){
            var job = percentages.find(function(jb){ return jb.JobID == day.WorkDayJob[j].JobID });
              if(!job){
              day.WorkDayJob[j].IndirectPerDiem = 0;
              }
          }
          if(!percentages.length){
             return;
          }
          var left = available;
          //We want to allocate based on percentages
          for(var j=0; j<percentages.length; j++){
            var job = day.WorkDayJob.find(function(jb){ return jb.JobID == percentages[j].JobID });
            if(job){
                var amount = Math.min(left, Math.max(Utils.round(percentages[j].Percentage * available,0),0));
                var diff = amount - left;
                if(j==percentages.length-1 || (diff > -0.05 && diff < 0.05)){ 
                  amount = left;
                }
                job.IndirectPerDiem = amount;
                left = Math.max(left-amount, 0);
              }
          }
        }

        var _csvQuote = function(string){
          var quoted = string.replace('\n',' ');
          if(quoted.match(/[,"]/i)){
            quoted = quoted.replace(/"/ig,'""');
            return '"'+quoted+'"';
          }
          return quoted;
        }

        var _compareDay = function(current, original){
            if(!original){
                return true;
            }
          return current.WeekdayHours !== original.WeekdayHours
            || current.STHours !== original.STHours
            || current.OTHours !== original.OTHours
            || current.DTHours !== original.DTHours
            || current.PerDiemTotal !== original.PerDiemTotal
            || current.PerDiemDirectTotal !== original.PerDiemDirectTotal
            || current.StatusCode != original.StatusCode;
        }
        var _compareJob = function(current, original){
          if(!original || current.PQChange || _isTempID(current.ID) || current.QuickAddJob)
            return true;
          return current.DirectHoursTotal !== original.DirectHoursTotal 
              || current.DirectHours !== original.DirectHours
              || current.DirectPerDiemTotal !== original.DirectPerDiemTotal
              || current.DirectPerDiem !== original.DirectPerDiem
              || current.IndirectHours !== original.IndirectHours
              || current.IndirectPerDiem !== original.IndirectPerDiem
              || current.PerDiem !== original.PerDiem
              || current.TotalHours !== original.TotalHours
              || current.DirectSTHoursTotal !== original.DirectSTHoursTotal 
              || current.DirectSTHours !== original.DirectSTHours
              || current.IndirectSTHours !== original.IndirectSTHours
              || current.STHours !== original.STHours
              || current.DirectOTHoursTotal !== original.DirectOTHoursTotal 
              || current.DirectOTHours !== original.DirectOTHours
              || current.IndirectOTHours !== original.IndirectOTHours
              || current.OTHours !== original.OTHours
              || current.DirectDTHoursTotal !== original.DirectDTHoursTotal 
              || current.DirectDTHours !== original.DirectDTHours
              || current.IndirectDTHours !== original.IndirectDTHours
              || current.DTHours !== original.DTHours
              || current.CrewEquipmentFlag != original.CrewEquipmentFlag
              || current.OrderNumber != original.OrderNumber
              || current.JobComment != original.JobComment;
        }
        var _compareEquip = function(current, original){
          if(!original || _isTempID(current.ID))
            return true;
          return current.DirectHoursTotal !== original.DirectHoursTotal 
              || current.DirectHours !== original.DirectHours
              || current.IndirectHours !== original.IndirectHours
              || current.TotalHours !== original.TotalHours;
        }
        var _compareJobEquip = function(current, original){
          if(!original || current.PQChange || _isTempID(current.ID))
            return true;
          return current.DirectHoursTotal !== original.DirectHoursTotal 
              || current.DirectHours !== original.DirectHours
              || current.IndirectHours !== original.IndirectHours
              || current.TotalHours !== original.TotalHours;
        }
        var _getSafety = function(workweek){
            var comms = {}
            for(var c=0; c<workweek.Comm.length; ++c){
                var comm = comms[workweek.Comm[c].ID];
                if(!comm){
                    comm = {ID:workweek.Comm[c].ID,people:{}}
                    comms[workweek.Comm[c].ID] = comm;
                }
                var person = workweek.Comm[c].Person;
                if(person.changed){
                    if(!comm.people[person.ID]){
                        comm.people[person.ID] = 'Person,'+person.ID+','+Utils.bool(person.ReviewedFlag)+','+person.ReviewedDate;
                        comm.changed = true;
                    }
                    delete person.changed;
                }
            }
            var returnValue = []
            var keys = Object.keys(comms);
            for(var k=0; k<keys.length; ++k){
                if(comms[keys[k]].changed){
                    returnValue.push('Communication,'+keys[k])
                    var people = Object.keys(comms[keys[k]].people)
                    for(var i=0; i<people.length; ++i){
                      people[i] = comms[keys[k]].people[people[i]]
                    }
                    returnValue.push(people.join('\n'))
                }
            }
            return returnValue.length?returnValue.join('\n'):false;
        }
        var _getPostData = function(workweek, original){
          var result = ["WorkWeek,"+workweek.ID+","+workweek.TotalHours+","+workweek.EquipmentHoursTotal+","+workweek.PerDiemTotal+","+workweek.STHours+","+workweek.OTHours+","+workweek.DTHours+","+_csvQuote(workweek.Code||'')];
          if(!original || workweek.StatusCode != original.StatusCode){
            result[0] += ","+workweek.StatusCode;
            if(typeof workweek.reviewComment != 'undefined'){
              result[0] += ","+_csvQuote(workweek.reviewComment);
            }
          }
          for(var d=0; d<workweek.WorkDay.length; d++){
            var day = ["WorkDay,"+workweek.WorkDay[d].ID], dayChanged = _compareDay(workweek.WorkDay[d],original?original.WorkDay[d]:null);
            if(dayChanged){
              day[0] += ","+workweek.WorkDay[d].WeekdayHours+","+workweek.WorkDay[d].STHours+","+workweek.WorkDay[d].OTHours+","+workweek.WorkDay[d].DTHours+","+workweek.WorkDay[d].PerDiemTotal+","+workweek.WorkDay[d].PerDiemDirectTotal+","+workweek.WorkDay[d].StatusCode;
            }
            for(var e=workweek.WorkDay[d].WorkDayEquipment.length-1; e>=0; e--){
              var equip = ["WorkDayEquipment,"+workweek.WorkDay[d].WorkDayEquipment[e].ID+","+workweek.WorkDay[d].WorkDayEquipment[e].EquipmentID];
              if(workweek.WorkDay[d].WorkDayEquipment[e].active){
                var originalEquip = original?original.WorkDay[d].WorkDayEquipment.find(function(eq){return eq.EquipmentID==workweek.WorkDay[d].WorkDayEquipment[e].EquipmentID}):null;
                var equipChanged = _compareEquip(workweek.WorkDay[d].WorkDayEquipment[e],originalEquip)
                if(equipChanged){
                  equip[0] += ","+workweek.WorkDay[d].WorkDayEquipment[e].DirectHoursTotal+","+workweek.WorkDay[d].WorkDayEquipment[e].DirectHours+","+workweek.WorkDay[d].WorkDayEquipment[e].IndirectHours+","+workweek.WorkDay[d].WorkDayEquipment[e].TotalHours;
                  day.push(equip.join('\n'));
                  dayChanged = true;
                }
              }else{
                if(workweek.WorkDay[d].WorkDayEquipment[e].ID && workweek.WorkDay[d].WorkDayEquipment[e].ID.toString().substr(0,1) != '<'){
                  equip[0] += ',DELETE';
                  day.push(equip.join('\n'));
                  dayChanged = true;
                }
                workweek.WorkDay[d].WorkDayEquipment.splice(e,1);
              }
            }
            for(var j=workweek.WorkDay[d].WorkDayJob.length-1; j>=0; j--){
              var job = ["WorkDayJob,"+workweek.WorkDay[d].WorkDayJob[j].ID+","+workweek.WorkDay[d].WorkDayJob[j].JobID]
              if(workweek.WorkDay[d].WorkDayJob[j].active){
                var originalJob = original?original.WorkDay[d].WorkDayJob.find(function(jb){return jb.JobID==workweek.WorkDay[d].WorkDayJob[j].JobID}):null;
                var jobChanged = _compareJob(workweek.WorkDay[d].WorkDayJob[j],originalJob)
                if(jobChanged){
                  var quickAdd = workweek.WorkDay[d].WorkDayJob[j].QuickAddJob?true:'';
                  job[0] += ","+workweek.WorkDay[d].WorkDayJob[j].DirectHoursTotal+","+workweek.WorkDay[d].WorkDayJob[j].DirectHours+","+workweek.WorkDay[d].WorkDayJob[j].DirectPerDiemTotal+","+workweek.WorkDay[d].WorkDayJob[j].DirectPerDiem+","+workweek.WorkDay[d].WorkDayJob[j].IndirectHours+","+workweek.WorkDay[d].WorkDayJob[j].IndirectPerDiem+","+workweek.WorkDay[d].WorkDayJob[j].TotalHours+","+workweek.WorkDay[d].WorkDayJob[j].PerDiem+","+workweek.WorkDay[d].WorkDayJob[j].DirectSTHours+","+workweek.WorkDay[d].WorkDayJob[j].DirectSTHoursTotal+","+workweek.WorkDay[d].WorkDayJob[j].IndirectSTHours+","+workweek.WorkDay[d].WorkDayJob[j].STHours+","+workweek.WorkDay[d].WorkDayJob[j].DirectOTHours+","+workweek.WorkDay[d].WorkDayJob[j].DirectOTHoursTotal+","+workweek.WorkDay[d].WorkDayJob[j].IndirectOTHours+","+workweek.WorkDay[d].WorkDayJob[j].OTHours+","+workweek.WorkDay[d].WorkDayJob[j].DirectDTHours+","+workweek.WorkDay[d].WorkDayJob[j].DirectDTHoursTotal+","+workweek.WorkDay[d].WorkDayJob[j].IndirectDTHours+","+workweek.WorkDay[d].WorkDayJob[j].DTHours+","+Utils.bool(workweek.WorkDay[d].WorkDayJob[j].CrewEquipmentFlag,true)+","+workweek.WorkDay[d].WorkDayJob[j].OrderNumber+","+quickAdd;
                  if(!originalJob || workweek.WorkDay[d].WorkDayJob[j].JobComment != originalJob.JobComment){
                    job[0] += ","+_csvQuote(workweek.WorkDay[d].WorkDayJob[j].JobComment);
                  }
                  delete workweek.WorkDay[d].WorkDayJob[j].PQChange;
                  delete workweek.WorkDay[d].WorkDayJob[j].QuickAddJob;
                }
                for(var e=0; e<workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment.length; e++){
                  var equip = ("WorkDayJobEquipment,"+workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].ID+","+workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].EquipmentID),
                    originalEquip = originalJob?original.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment.find(function(eq){return eq.EquipmentID==workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].EquipmentID}):undefined;
                  var equipChanged = _compareJobEquip(workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e],originalEquip);
                  if(equipChanged){
                    equip += ","+workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].DirectHoursTotal+","+workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].DirectHours+","+workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].IndirectHours+","+workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].TotalHours;
                    delete workweek.WorkDay[d].WorkDayJob[j].WorkDayJobEquipment[e].PQChange;
                    job.push(equip);
                    dayChanged = jobChanged = true;
                  }
                }
                if(jobChanged){
                  day.push(job.join('\n'));
                  dayChanged = true;
                }
              }else{
                //Delete job
                if(workweek.WorkDay[d].WorkDayJob[j].ID && workweek.WorkDay[d].WorkDayJob[j].ID.toString().substr(0,1) != '<'){
                  job[0] += ',DELETE';
                  day.push(job.join('\n'));
                  dayChanged = true;
                }
                workweek.WorkDay[d].WorkDayJob.splice(j,1);
              }
            }
            if(dayChanged){
              result.push(day.join('\n'));
            }
          }
          var safety = _getSafety(workweek);
          if(safety) result.push(safety);
          return result.join('\n');
        }

        var _canSubmitIfStatus = ['PE', 'IP', 'RJ'];
        var _statusMeanings = {
          'PE': 'Pending',
          'IP': 'In Process',
          'SB': 'Submitted',
          'RJ': 'Rejected',
          'AP': 'Approved'
        };
        var _reviewMeanings = {
          'SB': 'Submit',
          'RJ': 'Reject',
          'AP': 'Approve'
        };
        var _validateAllocation = function(workweek, dayIndex){
          if(typeof dayIndex != 'undefined'){
            for(var e=0; e<workweek.WorkDay[dayIndex].WorkDayEquipment.length; e++){
              if(workweek.WorkDay[dayIndex].WorkDayEquipment[e].active && workweek.WorkDay[dayIndex].WorkDayEquipment[e].MyEquipment){
                if(workweek.WorkDay[dayIndex].WeekdayHours != workweek.WorkDay[dayIndex].WorkDayEquipment[e].TotalHours){
                  return false;
                }
              }
            }
          }else{
            for(var d=0; d<workweek.WorkDay.length; d++){
              if(_canSubmitIfStatus.includes(workweek.WorkDay[d].StatusCode)){
                for(var e=0; e<workweek.WorkDay[d].WorkDayEquipment.length; e++){
                  if(workweek.WorkDay[d].WorkDayEquipment[e].active && workweek.WorkDay[d].WorkDayEquipment[e].MyEquipment){
                    if(workweek.WorkDay[d].WeekdayHours != workweek.WorkDay[d].WorkDayEquipment[e].TotalHours){
                      return false;
                    }
                  }
                }
              }
            }
          }
          return true;
        }
        var _validateTimeEntries = function(workweek){
          for(var d=0; d<workweek.WorkDay.length; d++){
            var day = workweek.WorkDay[d];
          for(var j=0; j<day.WorkDayJob.length; j++){
            var job = day.WorkDayJob[j];
            //Time
            for(var t=0; t<job.JobTimeEntry; t++){
              var time = job.JobTimeEntry[t];
              if(time.BatchID || time.StatusCode == 'BI'){
                return false;
              }
            }
            //Expense
            for(var p=0; p<job.JobExpenseEntry; p++){
              var pd = job.JobExpenseEntry[p];
              if(pd.BatchID || pd.BillingStatusCode == 'BI'){
                return false;
              }
            }
            //Equipment
            for(var e=0; e<job.WorkDayJobEquipment.length; e++){
              var equip = job.WorkDayJobEquipment[e];
              //Equipment time
              for(var et=0; et<equip.EquipmentTimeEntry; et++){
                var equipTime = equip.EquipmentTimeEntry[et];
                if(equipTime.BatchID || equipTime.StatusCode == 'BI'){
                  return false;
                }
              }
            }
          }
        }
          return true;
        }

        var _calcWorkWeekStatus = function(workweek){
            var code = 'IP' //Default to In Process
            var rules = {all:['PE'],atLeast:['SB','RV','AP'],one:['RJ']}
            var ruleVals = {all:{},atLeast:{lowest:rules.atLeast.length,count:0},one:{}}
            //Tally rule matches
            for(var d=0; d<workweek.WorkDay.length; ++d){
                var day = workweek.WorkDay[d];
                if(rules.one.includes(day.StatusCode)){
                    ruleVals.one[day.StatusCode] = true;
                }else if(rules.all.includes(day.StatusCode)){
                    ruleVals.all[day.StatusCode] = (ruleVals.all[day.StatusCode]||0) + 1;
                }else{
                    var indexVal = rules.atLeast.indexOf(day.StatusCode);
                    if(indexVal > -1){
                        ruleVals.atLeast.lowest = Math.min(indexVal, ruleVals.atLeast.lowest);
                        ruleVals.atLeast.count++;
                    }
                }
            }
            //Check if we can assign a new code
            if(Object.keys(ruleVals.one).length){
                for(var o=0; o<rules.one.length; ++o){
                    if(ruleVals.one[rules.one[o]]){
                        code = rules.one[o];
                        break;
                    }
                }
            }else{
                var allKeys = Object.keys(ruleVals.all);
                if(allKeys.length == 1 && ruleVals.all[allKeys[0]] == 7){
                    code = allKeys[0];
                }else{
                    if(ruleVals.atLeast.count == 7){
                        code = rules.atLeast[ruleVals.atLeast.lowest];
                    }
                }
            }
            return code;
        }

        var _review = function(workweek, code, force, dayIndex, comment){
          if(code == 'SB' && !force){
            if(!_validateAllocation(workweek, dayIndex)){
              var msg = 'Equipment hours for some day(s) do not match My Hours. Would you like to submit anyway?';
              if(typeof dayIndex != 'undefined'){
                msg = 'Equipment hours for this day do not match My Hours. Would you like to submit anyway?';
              }
              return {title:'Hours Not Equal',message:msg}
            }
          }
          if(code == 'IP' && workweek.StatusCode == 'AP'){
            //Re-opening
            if(!_validateTimeEntries(workweek)){
              return {title:'Error',message:'Some time entries have already been billed or batched for payroll; this timesheet cannot be re-opened.', buttons: [{label:'Ok'}]}
            }
          }
          for(var d=0; d<workweek.WorkDay.length; d++){
            workweek.WorkDay[d].priorStatus = workweek.WorkDay[d].StatusCode;
            if((code!='SB' || _canSubmitIfStatus.includes(workweek.WorkDay[d].StatusCode)) && (typeof dayIndex == 'undefined' || d==dayIndex)){
              workweek.WorkDay[d].StatusCode = code;
              workweek.WorkDay[d].StatusMeaning = _statusMeanings[code];
            }
            if(_canSubmitIfStatus.includes(workweek.WorkDay[d].StatusCode)){
              _reAllocateJobs(workweek, d);
              _reAllocateJobsPD(workweek, d);
            }
          }
          var workweekCode = (typeof dayIndex == 'undefined')?code:_calcWorkWeekStatus(workweek);
          if(typeof dayIndex == 'undefined' || (workweekCode != workweek.StatusCode)){
            workweek.priorStatus = workweek.StatusCode;
            workweek.StatusCode = workweekCode;
            workweek.StatusMeaning = _statusMeanings[workweekCode];
            workweek.reviewComment = (typeof dayIndex == 'undefined')?comment:'';
          }
          return {};
        }
        var _revertReview = function(workweek){
          workweek.StatusCode = workweek.priorStatus;
          workweek.StatusMeaning = _statusMeanings[workweek.priorStatus];
          delete workweek.reviewComment;
          delete workweek.priorStatus;
          for(var d=0; d<workweek.WorkDay.length; d++){
            workweek.WorkDay[d].StatusCode = workweek.WorkDay[d].priorStatus;
            workweek.WorkDay[d].StatusMeaning = _statusMeanings[workweek.WorkDay[d].priorStatus];
            delete workweek.WorkDay[d].priorStatus;
          }
        }

        var _addJobsDefaultData = function(data, workweek){
          workweek.sp = workweek.sp||{};
            workweek.sp.jobType = Utils.enforceArray(data.JobType);
            workweek.sp.region = Utils.enforceArray(data.Region);
            workweek.sp.division = Utils.enforceArray(data.Division);
            workweek.sp.country = Utils.enforceArray(data.Country);
            workweek.sp.state = Utils.enforceArray(data.State);
            workweek.sp.project = Utils.enforceArray(data.Project);

            workweek.dialog1.job = data.Job;
            var job = workweek.dialog1.job;
            job.TypeCode = 'TE';
            job.UseDesignFlag = false;
            if(job.TypeCode){
                job.jobType = workweek.sp.jobType.find(function(jt){return jt.SmartCode==job.TypeCode});
            }
            if(job.DivisionRegionID){
                job.region = workweek.sp.region.find(function(r){return r.RegionID==job.DivisionRegionID});
            }
            if(job.DivisionID){
                job.division = workweek.sp.division.find(function(d){return d.DivisionID==job.DivisionID});
            }
            if(job.StateCountryID){
                job.country = workweek.sp.country.find(function(c){return c.ID==job.StateCountryID});
            }
            if(job.MasterProjectID){
                job.project = workweek.sp.project.find(function(cp){return cp.ID==job.MasterProjectID});
            }
            if(job.StateID){
                job.state = workweek.sp.state.find(function(s){return s.ID==job.StateID});
            }
        }
        var _selectStartJob = function(job, myTime, crewEquip, workweek){
            if((typeof myTime != "undefined" && ((myTime&&!job.selected)||(crewEquip&&!job.crewEquipSelected))) || (!job.hasDirect)){
                var selected = typeof myTime=="undefined"?(!(job.selected||job.crewEquipSelected)):(myTime?!job.selected:!job.crewEquipSelected);
                var order = job.order||0, prior = (job.selected||job.crewEquipSelected), day = workweek.WorkDay[workweek.currentDay];
                if(!selected){
                    delete job.order;
                }
                for(var j=0; j<day.Job.length; ++j){
                    var jb = day.Job[j];
                    if(jb.selected||jb.crewEquipSelected){
                        if(!selected){
                            if(jb.order > order){
                                --jb.order;
                            }
                        }else if(jb.ID != job.ID){
                            order = Math.max(order, jb.order||0);
                        }
                    }
                }
                if(!prior){
                    job.order = order+1;
                }
                job.selected = selected&&(myTime||myTime==null);
                job.crewEquipSelected = selected&&crewEquip;
                job.checkbox = job.checkbox2 = job.selected||job.crewEquipSelected;
            }
        }
        var _trackNewJob = function(job, scope){
            _setUpJobs(scope.workweek, [job]);
            var day = scope.workweek.WorkDay[scope.workweek.currentDay];
            day.currentJobType = job.Type;
            _selectStartJob(day.Job.find(function(jb){ return jb.ID == job.ID }), undefined, undefined, scope.workweek);
        }

        var _showUpSites = function(scope){
            _changes(scope.workweek, true);
            _save(scope, 'timesheet/save', {}, 'showUpSitesSaveDone', 'Loading Show Up Sites...');
            return true;
        }

        var _save = function(scope, endpoint, extraData, callbackName, loadingText){
            endpoint = endpoint||'timesheet/save';
            scope.loading = true;
            scope.loadingDialog(loadingText);

            var done = function(resp){
                if(resp.replacements && resp.replacements.length){
                    Utils.replace(scope.workweek, resp.replacements);
                }
                for(var d=0; d<scope.workweek.WorkDay.length; ++d){
                  var day = scope.workweek.WorkDay[d]
                  for(var j=0; j<day.WorkDayJob.length; ++j){
                    var job = day.WorkDayJob[j]
                    if(!job.active){
                      delete job.ID
                    }
                  }
                  for(var e=0; e<day.WorkDayEquipment.length; ++e){
                    var equip = day.WorkDayEquipment[e]
                    if(!equip.active){
                      delete equip.ID
                    }
                  }
                }
                scope.original = Utils.copy(scope.workweek)
                if(scope.current && scope.current.original){
                    scope.current.original = scope.original
                }
                scope.loading = false;
                if(scope.callbacks[callbackName]){
                    if(typeof scope.callbacks[callbackName] == "function"){
                        scope.callbacks[callbackName].apply(null, [resp, extraData]);
                    }else if(typeof scope.callbacks[callbackName].fn == "function"){
                        scope.callbacks[callbackName].fn.apply(null, [resp, extraData]);
                    }
                }
            }

            if(endpoint.match(/\/save/i)){
                _changes(scope.workweek, false);
            }
            var workWeek = Utils.copy(scope.workweek);            
            var data = _getPostData(workWeek, scope.original);
            if(extraData){
                data += '[!-- @@@@@ --]'+(typeof extraData=='string'?extraData:JSON.stringify(extraData));
            }
            var route = {route:endpoint.split('timesheet/').pop()};
            ApiService.post('gt', data, null, null, null, route).then(done,done);
            return true;
        }

        var _load = function(scope, endpoint, method, extraData, callbackName, loadingText){
            scope.loadingDialog(loadingText);
            var done = function(){
                scope.loading = false;
                if(scope.callbacks[callbackName]){
                    scope.callbacks[callbackName].apply(null, arguments);
                }
            }
            endpoint = endpoint||'timesheet/load';
            extraData.route = endpoint.split('timesheet/').pop();
            scope.loading = true;
            if(method == 'GET'){
                ApiService.get('gt', extraData).then(done,done);
            }else{
                ApiService.post('gt', extraData).then(done,done);
            }
        }

        var _changes = function(workweek, changes){
            if(!changes){
                workweek.changes = false;
            }else if(!workweek.changes && !['SB','AP','RV'].includes(workweek.WorkDay[workweek.currentDay].StatusCode)){
                workweek.changes = true;
            }
        }

        var _dialog = null;
        var _setDialog = function(dialog){
          _dialog = dialog;
        }

        var _showMessage = function(msg, title){
          $timeout(function(){
            if(_dialog){
              _dialog.open({
                title: title||'Notice',
                content: msg,
                buttons: [{
                  label: 'Ok'
                }],
                cancelable: true
              })
            }
          })
        }

        return {
          cleanWorkweek: _cleanWorkweek,
          enterValue: _enterValue,
          reAllocateJobs: _reAllocateJobs,
          reAllocateJobsPD: _reAllocateJobsPD,
          getPostData: _getPostData,
          setDialog: _setDialog,
          review: _review,
          revertReview: _revertReview,
          statusMeanings: _reviewMeanings,
          addRemoveJobs: _addRemoveJobs,
          addCrewEquipment: _addCrewEquipment,
          removeCrewEquipment: _removeCrewEquipment,
          modifyCrewEquipment: _modifyCrewEquipment,
          autoAddJobs: _autoAddJobs,
          getWorkWeekDays: _getWorkWeekDays,
          setUpWorkWeekDays: _setUpWorkWeekDays,
          selectStartJob: _selectStartJob,
          addJobsDefaultData: _addJobsDefaultData,
          trackNewJob: _trackNewJob,
          updateJobPhotos: _updateJobPhotos,
          updateDayComms: _updateDayComms,
          checkRequiredComms: _checkRequiredComms,
          calcShowUpSites: _calcShowUpSites,
          showUpSites: _showUpSites,
          load: _load,
          save: _save,
          changes: _changes
        };

    }]);


})();

(function(){
    'use strict';

    angular.module('app').controller('ReceiptsController', ['$scope', '$timeout', 'ApiService', 'StorageService', '$rootScope', 'Utils', 'Smartpick', function($scope, $timeout, ApiService, StorageService, $rootScope, Utils, Smartpick) {
        $scope.working = true;
        $scope.module = $scope.$parent.current.module;
        $scope.title = $scope.module.title;

        $scope.openingReceipt = false
        $scope.receipts = []
        $scope.jobReceipts = []
        $scope.next = null
        $scope.jobNext = null

        var data = {}

        var cleanReceipts = function(receipts){
            for(var i=0; i<receipts.length; ++i){
                receipts[i].InvoiceDate = Utils.date(receipts[i].InvoiceDate)
            }
        }
        var result = function(resp){
            if(resp){
                if(resp.JobStatus){
                    data.JobStatus = Utils.enforceArray(resp.JobStatus);
                }
                if(resp.VendorInvoice){
                    resp.VendorInvoice = Utils.enforceArray(resp.VendorInvoice);
                    cleanReceipts(resp.VendorInvoice)
                    for(var i=0; i<resp.VendorInvoice.length; ++i){
                        $scope.receipts.push(resp.VendorInvoice[i])
                    }
                    $scope.next = resp.next
                }
            }
            $scope.working = false;
            $scope.openingReceipt = false;
        }
        var loadReceipts = function(next){
            var obj = {
                route: 'mine'
            };
            if(next){
                obj.next = next.id;
                obj.date = next.date;
            }
            ApiService.get('receipts', obj).then(result, result);
        };

        $scope.loadMoreMine = function(){
            $scope.openingReceipt = true;
            loadReceipts($scope.next)
        }

        var jobResult = function(resp){
            if(resp && resp.VendorInvoice){
                resp.VendorInvoice = Utils.enforceArray(resp.VendorInvoice);
                cleanReceipts(resp.VendorInvoice)
                for(var i=0; i<resp.VendorInvoice.length; ++i){
                    $scope.jobReceipts.push(resp.VendorInvoice[i])
                }
                $scope.jobNext = resp.next
            }
            $scope.working = false;
            $scope.openingReceipt = false;
        }
        var loadJobReceipts = function(job, next){
            var obj = {
                route: 'job',
                job: job.ID
            };
            if(next){
                obj.next = next.id;
                obj.date = next.date;
            }
            ApiService.get('receipts', obj).then(jobResult, jobResult);
        };

        $scope.loadMoreJob = function(){
            $scope.openingReceipt = true;
            loadJobReceipts($scope.job, $scope.jobNext)
        }

        $scope.addReceipt = function(){
            var job = receiptsTabbar.getActiveTabIndex() == 1?$scope.job:null
            $scope.openReceipt(null, job);
        }
        $scope.openReceipt = function(receipt, job){
            $scope.current.job = job;
            $scope.current.receipt = receipt;
            $scope.current.replaceReceipt = function(newReceipt, isNew){
                var listReceipt;
                $scope.current.receipt = newReceipt;
                if(isNew){
                    $scope.receipts.unshift(newReceipt);
                    if($scope.job && newReceipt.JobID == $scope.job.ID){
                        $scope.jobReceipts.unshift(newReceipt);
                    }
                }else{
                    if((listReceipt = $scope.receipts.findIndex(function(r){ return r.ID == newReceipt.ID })) > -1){
                        $scope.receipts[listReceipt] = newReceipt;
                    }
                    if((listReceipt = $scope.jobReceipts.findIndex(function(r){ return r.ID == newReceipt.ID })) > -1){
                        if($scope.jobReceipts[listReceipt].JobID != newReceipt.JobID){
                            $scope.jobReceipts.splice(listReceipt, 1);
                        }else{
                            $scope.jobReceipts[listReceipt] = newReceipt;
                        }
                    }
                }
            }
            if(receipt && !job){
                $scope.current.job = {
                    ID: receipt.JobID,
                    NumberChar: receipt.JobJobNumberChar,
                    WorkgroupJobNumber: receipt.JobWorkgroupJobNumber
                }
            }
            mainNav.pushPage('app/receipts/edit-receipt.html');
        }

        var selectJob = function(item){
            $scope.job = item;
            $scope.jobReceipts = []
            $scope.jobNext = null
            loadJobReceipts(item);
            $scope.working = true;
        }
        $scope.findJob = function(){
            if($scope.job && Smartpick.ok()){
                Smartpick.show(null, selectJob);
            }else{
                console.log(data.JobStatus)
                Smartpick.show({
                    endpoint: 'receiptsJobSearch',
                    itemView: 'Job',
                    itemTemplate: 'job.html',
                    title: 'Find Job',
                    filters: [
                        {key: 'Status', from: 'Code', to:'StatusCode', display:'Meaning'},
                        {key: 'Owner', from: 'OrganizationID', to:'OwnerID', display:'Name'}
                    ],
                    filtersData: {
                        Status: data.JobStatus
                    },
                    defaultFilters: {
                        Status: {"Code":"PR","Meaning":"In Process"}
                    },
                    minSearchLength: 2
                }, selectJob);
            }
        }

        var statusClasses = {RB:'green',NB:'light',BI:'light-green'}
        $scope.getStatusClass = function(receipt){
            if(statusClasses[receipt.StatusCode]){
                return statusClasses[receipt.StatusCode];
            }
            return '';
        }

        var init = function(){
            $scope.loaded = true;
        };

        loadReceipts();
        $timeout(init);
        


        $scope.$on('$destroy', function(){
            Smartpick.reset();
            Smartpick.hide();
        });
    }]);

})();
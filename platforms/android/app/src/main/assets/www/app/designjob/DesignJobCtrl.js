(function(){
    'use strict';

    angular.module('app').controller('DesignJobController', ['$scope', 'ApiService', 'Utils', '$rootScope', '$timeout', '$anchorScroll', function($scope, ApiService, Utils, $rootScope, $timeout, $anchorScroll) {
        $scope.job = $scope.$parent.current.job;
        $scope.job.changed = false;
        $scope.adding = false;
        $scope.module = $scope.$parent.current.module;
        $scope.editingDetailIndex = -1;
        var dialogs = {}, data = {};
        var currentLocationRow = null;


        var loadJob = function(updateEstimate, callback){
            $scope.working = !updateEstimate;
            $scope.loading = updateEstimate;
            var obj = {design: $scope.job.JobDesignID, updateEstimate: updateEstimate};
            ApiService.get('designJob', obj).then(result, result).finally(function(){
                if(updateEstimate)
                    Utils.notification.alert('Estimate has been updated.');
                if(callback){
                    callback($scope.design);
                }
            })
        }

        var cleanFactor = function(factor){
            factor.PercentDisplay = Utils.fixPercent(factor.PercentDisplay);
            return factor;
        };
        var cleanLocation = function(location){
            location.LFactor = Utils.enforceArray(location.LFactor);
            location.LFactor.map(cleanFactor);
            location.totalFactor = calcLocationTotalFactor(location);
            return location;
        };
        var cleanResponse = function(design){
            design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
            if(design.StatusCode == 'PE'){
                design.StatusCode = 'IP';
                design.StatusMeaning = 'In Process';
            }
            design.Location = Utils.enforceArray(design.Location).map(cleanLocation);
            design.JFactor = Utils.enforceArray(design.JFactor).map(cleanFactor);
            design.JDFactor = Utils.enforceArray(design.JDFactor).map(cleanFactor);
            design.PercentComplete = Utils.fixPercent(design.PercentComplete);
            design.totalFactor = calcTotalFactor(design);
            return design;
        };

        var calcLocationTotalFactor = function(location){
            if(location.LFactor.length){
                if(location.UnfactoredHours == 0)
                    return 100;
                return (location.FactoredHours/location.UnfactoredHours)*100;
            }
            return -1;
        }

        var calcTotalFactor = function(design){
            if(design.JFactor.length||design.JDFactor.length){
                if(design.UnfactoredHours == 0)
                    return 100;
                return (design.FactoredHours/design.UnfactoredHours)*100;
            }
            return -1;
        }

        var result = function(resp){
            $scope.working = false;
            $scope.loading = false;
            if(resp && resp.JobDesign){
                $scope.job.design = cleanResponse(resp.JobDesign);
                $scope.design = $scope.job.design;
                $scope.design.lastLocationFetch = new Date().getTime();
            }else{
                ons.notification.alert("Failed to load job data", {title: "Error"});
                mainNav.popPage();
            }
        };

        $scope.updateEstimate = function(callback){
            loadJob(true, callback);
        }

        $scope.goToLocation = function(index, location, $event){
            if($scope.adding || $scope.loading || $scope.editingLocationIndex >= 0)
                return;
            currentLocationRow = $event?$event.currentTarget:document.getElementById('anchor'+location.JobLocationID);
            $scope.current.design = $scope.design;
            $scope.current.locationFactors = $scope.locationFactors;
            $scope.current.updateEstimate = $scope.updateEstimate;
            $scope.current.calcTotalFactor = function(){
                $scope.design.totalFactor = calcTotalFactor($scope.design);
            }
            mainNav.pushPage('app/designjob/design-location.html', { locationIndex: index });
        };

        $scope.getLocationClass = function (location){
            var cls = '';
            if(location.FailedInspectionFlag){
                cls += ' red';
            }else if(location.ActiveFlag && !location.Complete){
                cls += ' blue';
            }
            if(!location.ActiveFlag || $scope.adding){
                cls += ' faded';
            }
            if(location.editing){
                cls += ' selected';
            }
            return cls;
        };

        var navigateToNewItem = function(location){
            var locationID = location.JobLocationID;
            $timeout(function() {
                $anchorScroll('anchor'+locationID);
            });
        };
        var addLocationFinalResult = function(resp){
            $scope.adding = false;
            if(resp && resp.Location){
                if(!Array.isArray(resp.Location))
                    resp.Location = [resp.Location];
                resp.Location.forEach(function(l){
                    $scope.design.Location.push(cleanLocation(l));
                });
                $scope.goToLocation($scope.design.Location.length-1, resp.Location[0])
            }
        }
        var addEditLocationFinal = function(type, form){
            if(form.JobLocationID){
                var location = $scope.design.Location[$scope.editingLocationIndex];
                Object.assign(location, form);
                location.TypeCode = type.SmartCode;
            }else{
                var obj = {
                    action: 'add',
                    design: $scope.job.JobDesignID,
                    NumberChar: form.NumberChar,
                    ReferenceNumber: form.ReferenceNumber,
                    Number: form.Number,
                    Description: form.Description,
                    TypeCode: type.SmartCode
                };
                $scope.adding = true;
                ApiService.post('designLocationAction', obj).then(addLocationFinalResult, addLocationFinalResult)
            }
        };
        $scope.addLocation = function(){
            dialogs.addLocation.openDialog();
        }

        $scope.locationOnLongPress = function(location) {
            if($scope.adding || $scope.loading || $scope.editingLocationIndex >= 0)
                return;
            navigator.vibrate(10);
            location.JobBillingUponCode = $scope.design.JobBillingUponCode;
            $rootScope.$emit('LocationDialog', {type:'init', location: location, endpoint: 'designLocation'});
            Utils.openDialog(dialogs.infoDialog);
        }

        $scope.getLocationNumber = function(location){
            return location.NumberChar;
        }

        $scope.getReferenceNumber = function(location){
            return location.ReferenceNumber?(location.ReferenceNumber):'';
        }

        $scope.useAssemblies = function(){
            $scope.loading = true;
            ApiService.post('designJob', {design: $scope.job.JobDesignID, useAssemblies: $scope.design.UseAssembliesFlag}).finally(function(){ $scope.loading = false; })
        }

        $scope.canMove = {
            up: false,
            down: false,
            originalIndex: -1
        }
        var originalLocation;
        $scope.editLocation = function(location, $index, $event){
            originalLocation = Utils.copy(location);
            $scope.design.Location.forEach(function(l){l.editing = false;});
            location.editing = true;
            $scope.editingLocationIndex = $index;
            $scope.canMove.up = $index > 0;
            $scope.canMove.down = $index<($scope.design.Location.length-1);
            $scope.canMove.originalIndex = $index;

            $event.stopPropagation();
            $event.preventDefault();
            return false;
        }
        $scope.cancelEdit = function(location, $event){
            location.editing = false;
            Object.assign(location, originalLocation);
            if($scope.editingLocationIndex != $scope.canMove.originalIndex){
                var location = $scope.design.Location.splice($scope.editingLocationIndex, 1)[0];
                $scope.design.Location.splice($scope.canMove.originalIndex, 0, location);
            }
            $scope.editingLocationIndex = -1;
            if($event){
                $event.stopPropagation();
                $event.preventDefault();
            }
            return false;
        }
        $scope.deleteLocation = function(location){
            $scope.loading = true;
            var obj = {
                action: 'delete',
                location: $scope.design.Location[$scope.editingLocationIndex].JobLocationID
            };
            ApiService.post('designLocationAction', obj).then(deleteLocationResult,deleteLocationResult);
        }
        var deleteLocationResult = function(resp){
            $scope.loading = false;
            if(resp && resp.JobDesign){;
                $scope.design.Location = Utils.enforceArray(resp.JobDesign.Location).map(cleanLocation);
                $scope.design.lastLocationFetch = new Date().getTime();
                $scope.editingLocationIndex = -1;
            }else if(resp && resp.data){
                if(!resp.data.success && resp.data.message){
                    Utils.notification.alert(resp.data.message, {title:'Error'});
                }
            }
        }
        var locationHasChanges = function(location, obj){
            if($scope.canMove.originalIndex != $scope.editingLocationIndex)
                return true;
            if(originalLocation.Notes != location.Notes)
                return true;
            if(originalLocation.NumberChar != location.NumberChar)
                return true;
            if(originalLocation.ReferenceNumber != location.ReferenceNumber)
                return true;
            if(originalLocation.Description != location.Description)
                return true;
            if(originalLocation.TypeCode != location.TypeCode)
                return true;
            if(checkLocationFactors(location, obj))
                return true;
            return false;
        }
        var checkLocationFactors = function(location, obj){
            var changedFactors = [];
            location.LFactor.forEach(function(lf){
                var original = originalLocation.LFactor.find(function(olf){
                    return lf.ContractFactorTypeFactorTypeID == olf.ContractFactorTypeFactorTypeID;
                });
                if(!original || ((lf.FactorUseCode == 'EN' && lf.PercentDisplay != original.PercentDisplay)||(lf.FactorUseCode == 'SE' && lf.ContractFactorID != original.ContractFactorID))){
                    changedFactors.push({
                        FactorTypeID: lf.ContractFactorTypeFactorTypeID,
                        Percent: lf.PercentDisplay/100,
                        ContractFactorID: lf.ContractFactorID
                    });
                }
            });
            if(changedFactors.length){
                obj.LFactor = changedFactors;
                return true;
            }
            return false;
        }
        $scope.saveLocation = function(){
            var location = $scope.design.Location[$scope.editingLocationIndex];
            var obj = {
                action: 'edit',
                design: $scope.job.JobDesignID,
                location: location.JobLocationID,
                index: $scope.editingLocationIndex,
                Notes: location.Notes,
                NumberChar: location.NumberChar,
                ReferenceNumber: location.ReferenceNumber,
                Description: location.Description,
                TypeCode: location.TypeCode
            };
            if(!locationHasChanges(location, obj)){
                $scope.cancelEdit(location);
                return;
            }
            $scope.loading = true;
            ApiService.post('designLocationAction', obj).then(saveLocationResult,saveLocationResult);
        }
        var saveLocationResult = function(resp){
            $scope.loading = false;
            if(resp && resp.JobDesign){
                Object.assign($scope.design, Utils.pick(resp.JobDesign, 'FactoredHours','UnfactoredHours'));
                $scope.design.totalFactor = calcTotalFactor($scope.design);
                resp = resp.JobDesign;
            }
            if(resp && resp.Location){
                resp.Location = Utils.enforceArray(resp.Location).forEach(function(l){
                    var index = $scope.design.Location.findIndex(function(lo){ return lo.JobLocationID==l.JobLocationID});
                    if(index > -1){
                        $scope.design.Location[index] = cleanLocation(l);
                    }
                });
                $scope.design.lastLocationFetch = new Date().getTime();
                $scope.editingLocationIndex = -1;
            }
        }
        $scope.moveLocation = function(dir){
            var location = $scope.design.Location.splice($scope.editingLocationIndex, 1)[0];
            $scope.editingLocationIndex += dir;
            $scope.design.Location.splice($scope.editingLocationIndex, 0, location);
            $scope.canMove.up = $scope.editingLocationIndex > 0;
            $scope.canMove.down = $scope.editingLocationIndex<($scope.design.Location.length-1);
        }
        $scope.locationFields = function(){
            var location = $scope.design.Location[$scope.editingLocationIndex];
            dialogs.addLocation.openDialog({
                title: 'Edit Location',
                location: location
            });
        }
        $scope.locationFactors = function(fact, done){
            var factors = fact||$scope.design.Location[$scope.editingLocationIndex].LFactor;
            dialogs.factors.openDialog(factors, {
                title:'Location Factors',
                readOnly: $scope.design.StatusCode!='IP',
                done: done
            });
        }

        var saveJobFactors = function(factors){
            var obj = {
                design: $scope.job.JobDesignID,
                LFactor: factors
            };
            $scope.loading = true;
            ApiService.post('designJob', obj).then(result, result);
        }

        var checkJobFactors = function(originals,changed){
            var changedFactors = [];
            changed.forEach(function(lf){
                var original = originals.find(function(olf){
                    return lf.ContractFactorTypeFactorTypeID == olf.ContractFactorTypeFactorTypeID;
                });
                if(!original || ((lf.FactorUseCode == 'EN' && lf.PercentDisplay != original.PercentDisplay)||(lf.FactorUseCode == 'SE' && lf.ContractFactorID != original.ContractFactorID))){
                    changedFactors.push({
                        FactorTypeID: lf.ContractFactorTypeFactorTypeID,
                        Percent: lf.PercentDisplay/100,
                        ContractFactorID: lf.ContractFactorID
                    });
                }
            });
            return changedFactors;
        }

        $scope.jobFactors = function(){
            var factors = Utils.copy($scope.design.JFactor).concat(Utils.copy($scope.design.JDFactor));
            var changed = Utils.copy(factors);
            dialogs.factors.openDialog(changed, {
                title:'Job Factors',
                done: function(ok){
                    var changedFactors = checkJobFactors(factors, changed);
                    if(ok && changedFactors.length){
                        saveJobFactors(changedFactors);
                    }
                },
                readOnly: $scope.design.StatusCode!='IP'
            });
        }

        var submit = function(){
            return new Promise(function(resolve,reject){
                $scope.loading = true;
                var obj = {
                    action: 'submit',
                    design: $scope.job.JobDesignID
                }
                ApiService.post('designAction', obj).then(resolve, reject).finally(function(){ $scope.loading = false; })
            });
        }

        $scope.submitDesign = function(callback){
            Utils.notification.confirm('Would you like to submit & approve the current design?',{
                title: 'Submit',
                callback: function(button){
                    if(button){
                        submit().then(function(resp){
                            if(resp && resp.JobDesign){
                                if(callback){
                                    callback(resp);
                                }else{
                                    if(resp.JobDesign.StatusCode == 'AC'){
                                        $scope.$parent.current.removeJob($scope.job);
                                        $scope.closeJob();
                                    }
                                }
                            }else if(resp && resp.data){
                                if(!resp.data.success && resp.data.message){
                                    var msg = 'The following must be resolved in the desktop version of UPRO:<p><i>'+resp.data.message+'</i></p>'
                                    Utils.notification.alert(msg, {title:'Error'});
                                }
                            }
                        })
                    }
                }
            })
        }


        $scope.closeJob = function(){
            mainNav.popPage();
        };

        $scope.$on('$destroy', function(){
            if(dialogs.infoDialog.visible){
                dialogs.infoDialog.hide();
            }
            if(dialogs.addLocation.visible){
                dialogs.addLocation.hide();
            }
            $rootScope.$emit('LocationDialog', {type:'destroy'});
        });

        ons.createAlertDialog('app/completework/create-location-dialog.html').then(function(dialog){
            dialogs.addLocation = dialog;
            var addLocationFetchResult = function(resp){
                if(resp && resp.SmartCodes){
                    dialog.setData(resp);
                }
            }
            dialog.openDialog = function(options){
                options = options||{};
                dialog._scope.title = options.title||'New Location';
                dialog._scope.page = 0;
                if(dialog._scope.data){
                    dialog._scope.data.NumberChar = "";
                    dialog._scope.data.Description = "";
                    dialog._scope.data.Notes = "";
                }
                if(options.location){
                    if(!dialog._scope.data || !dialog._scope.data.SmartCodes){
                        var obj = {action:'add', design: $scope.job.JobDesignID, referenceDataOnly: true};
                        ApiService.post('designLocationAction', obj).then(function(resp){
                            if(resp && resp.SmartCodes){
                                dialog.setData(Object.assign(resp, options.location));
                            }
                        });
                    }else{
                        dialog.setData(Object.assign(dialog._scope.data, options.location));
                    }
                }else if(!dialog._scope.data){
                    var obj = {action:'add', design: $scope.job.JobDesignID};
                    ApiService.post('designLocationAction', obj).then(addLocationFetchResult, addLocationFetchResult);
                }
                Utils.openDialog(dialog);
            }
            dialog.setData = function(data){
                dialog._scope.data = data;
                dialog.defaultSelection();
                if(data.NumberChar) dialog._scope.form.NumberChar = data.NumberChar;
                if(data.Description) dialog._scope.form.Description = data.Description;
                if(data.ReferenceNumber) dialog._scope.form.ReferenceNumber = data.ReferenceNumber;
                if(data.Notes) dialog._scope.form.Notes = data.Notes;
                if(data.JobLocationID) dialog._scope.form.JobLocationID = data.JobLocationID;
            }
            dialog.defaultSelection = function(){
                dialog._scope.form = {
                    NumberChar: '',
                    Description: '',
                    ReferenceNumber: '-'
                };
                if(dialog._scope.data){
                    var type = dialog._scope.data.SmartCodes.find(function(t){
                        return t.SmartCode == dialog._scope.data.TypeCode;
                    });
                    if(type){
                        dialog._scope.selects.type = type;
                    }
                }
            }
            dialog._scope.goPage = function(page){
                dialog._scope.page = page;
            }
            dialog._scope.tagHandler = function(tag){
                return null;
            };
            var getLocationNumber = function(num){
                if(dialog._scope.data.Prefix != '')
                    return dialog._scope.data.Prefix + num;
                return num;
            }
            
            dialog._scope.selects = {type:{}};
            dialog._scope.done = function(add, type, form){
                if(add && type){
                    addEditLocationFinal(type, form);
                }
                dialog.hide();
            };
        });

        ons.createDialog('app/dialog/location-info-dialog.html').then(function(dialog){
            dialogs.infoDialog = dialog;
        });

        var loadContractfactors = function(){
            ApiService.get('designAction', {action:'factors', design: $scope.job.JobDesignID}).then(function(resp){
                if(resp){
                    data.contractFactors = cleanContractFactors(resp);
                    dialogs.factors.setData(data.contractFactors);
                }
            });
        }
        var cleanContractFactors = function(data){
            data.Contract.CWorkgroup.CWFactorType = Utils.enforceArray(data.Contract.CWorkgroup.CWFactorType);
            data.Contract.CWorkgroup.CWFactorType.forEach(function(cwf){
                cwf.CWFactor = Utils.enforceArray(cwf.CWFactor);
                cwf.CWFactor.forEach(function(f){
                    f.Factor = Utils.fixPercent(f.Factor);
                });
            });
            return data;
        }
        ons.createAlertDialog('app/designjob/design-jobs-factor-dialog.html').then(function(dialog){
            dialog._scope.data = null;
            dialog.openDialog = function(factors, options){
                if(!dialog._scope.data){
                    loadContractfactors();
                }
                dialog.originalFactors = factors;
                dialog._scope.options = options||{};
                dialog._scope.factors = Utils.copy(factors);
                dialog.updateFactorSmartpicks();
                Utils.openDialog(dialog);
            }
            dialog.setData = function(data){
                dialog._scope.data = data;
                dialog.updateFactorSmartpicks();
            }
            dialog.updateFactorSmartpicks = function(){
                if(!dialog._scope.data)
                    return;
                dialog._scope.factors.forEach(function(f){
                    if(f.FactorUseCode == 'SE'){
                        dialog._scope.data.Contract.CWorkgroup.CWFactorType.forEach(function(cwf){
                            if(cwf.FactorTypeID == f.ContractFactorTypeFactorTypeID){
                                f.smartpick = cwf.CWFactor;
                                var selectedFactor = f.smartpick.find(function(sf){
                                    return sf.ContractFactorID == f.ContractFactorID;
                                });
                                if(selectedFactor)
                                    f.factor = selectedFactor;
                            }
                        });
                    }
                });
            }
            dialog._scope.selectFactor = function(spItem, factor){
                factor.ContractFactorID = spItem.ContractFactorID;
                factor.PercentDisplay = spItem.Factor;
            }
            dialog._scope.editFactor = function(factor){
                if(factor.FactorUseCode != 'EN' || dialog._scope.options.readOnly)
                    return;
                dialogs.percent.openDialog(factor.PercentDisplay, {
                    title: 'Factor',
                    done: function(ok, percent){
                        if(ok && percent.toString() != ''){
                            factor.PercentDisplay = percent;
                        }
                        return true;
                    }
                });
            }
            dialog._scope.done = function(ok){
                if(ok){
                    dialog.originalFactors.forEach(function(original){
                        dialog._scope.factors.forEach(function(changed){
                            if(original.ContractFactorTypeFactorTypeID == changed.ContractFactorTypeFactorTypeID){
                                Object.assign(original, changed);
                            }
                        });
                    });
                }
                if(dialog._scope.options.done){
                    dialog._scope.options.done(ok);
                }
                dialog.hide();
            }
            dialogs.factors = dialog;
        });

        ons.createAlertDialog('app/designjob/design-jobs-percent-dialog.html').then(function(dialog){
            dialog.openDialog = function(percent, options){
                dialog._scope.percent = percent||'';
                dialog._scope.options = options||{};
                Utils.openDialog(dialog);
            }
            dialog._scope.done = function(ok, percent){
                if(dialog._scope.options.done){
                    if(dialog._scope.options.done(ok, percent)){
                        dialog.hide();
                    }
                }else{
                    dialog.hide();
                }
            }
            dialogs.percent = dialog;
        });


        loadJob();
    }]);

})();
(function(){
    'use strict';

    angular.module('app').controller('ProfileController', ['$scope','ApiService', 'StorageService', 'AuthService', function($scope, ApiService, StorageService, AuthService) {
        $scope.profile = {};
        $scope.loaded = false;

        var fetchProfile = function(){
            ApiService.get('profile').then(function(resp){
                profileResult(resp && resp.EndUsers, resp.EndUsers);
            }, function(resp){
                profileResult(false);
            });
        };

        var profileResult = function(success, profile){
            if(success && profile){
                AuthService.renew();
                $scope.profile = profile;
            }
        };


        var saveStateFields = ['profile'];
        $scope = StorageService.state.load('ProfileController', $scope, saveStateFields);
        $scope.$on('$destroy', function() {
            StorageService.state.save('ProfileController', $scope, saveStateFields, 10);
        });
        if(!$scope.loaded)
            fetchProfile();

    }]);

})();
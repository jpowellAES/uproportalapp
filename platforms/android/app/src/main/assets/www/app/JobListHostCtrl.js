(function(){
    'use strict';

    angular.module('app').controller('JobListHostController', ['$scope', '$timeout', 'ApiService', 'StorageService', '$rootScope', 'Utils', 'TECompletionService', function($scope, $timeout, ApiService, StorageService, $rootScope, Utils, TECompletionService) {
        var staticData = {
            'PreActiveJob': {title:'Pre-Active', icon:'ion-hammer', sort:2},
            'PendingStartJob': {title:'Pending Start', icon:'ion-hammer', sort:2},
            'ActiveJob': {title:'Active', icon:'ion-hammer', sort:1},
            'FieldCompleteJob': {title:'Field Complete', icon:'ion-checkmark', sort:4},
            'OnHold': {title: 'On Hold', icon: '', sort:5}
        }
        staticData.MyActiveJob = staticData.ActiveJob;
        staticData.MyPendingStartJob = staticData.PendingStartJob
        $scope.loaded = false;
        $scope.openingJob = false;
        $scope.working = true;
        $scope.data = {
            tabs: []
        };
        $scope.styles = {
            tabbar: 'display: block; overflow-x: auto !important; overflow-y: hidden !important;',
            tab: {'display': 'inline-block','box-sizing': 'border-box'},
            tabInner: 'padding: 0px 10px;'
        };
        $scope.module = $scope.$parent.current.module;
        $scope.title = $scope.module.title;
        $scope.tabIndex = 0;
        $scope.hasJobs = false;
        $scope.infoDialog = {};
        $scope.startDialog = {};
        var userData = {};
        ons.createDialog('app/dialog/job-info-dialog.html').then(function(dialog){
            dialog._scope.infoDialog = $scope.infoDialog;
            dialog._scope.OrgType = $scope.OrgType;
            $scope.infoDialog.dialog = dialog;
        });
        ons.createAlertDialog('app/dialog/pending-start-begin-job-dialog.html').then(function(dialog){
            dialog._scope.model = {
                start: Utils.date('2017-01-01')
            };
            $scope.startDialog = dialog;
        });

        $scope.current.removeJob = function(job){
            var jobIndex = $scope.data.tabs[tabbar.getActiveTabIndex()].jobs.findIndex(function(j){
                return j.JobID == job.JobID;
            });
            if(jobIndex > -1){
                $scope.data.tabs[tabbar.getActiveTabIndex()].jobs.splice(jobIndex, 1);
            }
        }

        $scope.loadJobs = function($done){
            var obj = {
                types: $scope.module.options.lists.join(','),
                module: $scope.module.key
            };
            ApiService.get('jobs', obj).then(result, result).finally(function(){
                if($done) $done();
            });
        };

        var pickOutOnHoldJobs = function(jobs){
            var tab = $scope.data.tabs.find(function(t){return t.key == 'OnHold'});
            for(var i=jobs.length-1; i>=0; i--){
                if(jobs[i].OnHoldFlag){
                    if(!tab){
                        tab = addTab('OnHold', staticData['OnHold'].title, staticData['OnHold'].icon, $scope.module.options.detailPage, false);
                    }
                    tab.jobs.push(jobs.splice(i,1)[0]);
                }
            }
        }

        var setDispatchData = function(job){
            job.DispatchWizards = job.DispatchWizards.split('¶');
            for(var i=0; i<job.DispatchWizards.length; ++i){
                var data = job.DispatchWizards[i].split('~~~');
                job.DispatchWizards[i] = {name:data[0], id:data[1], search: data[2], map: data[3]}
            }
            
        }

        var cleanJobs = function(jobs){
            if(!Array.isArray(jobs))
                jobs = jobs?[jobs]:[];
            $scope.hasJobs = $scope.hasJobs||jobs.length > 0;
            if($scope.module.key == 'Inspection'){
                jobs.forEach(function(j){
                    j.badge = j.LocationsToInspect;
                    j.redBadge = j.LocationsWithCOs;
                    if(j.DispatchWizards){
                        setDispatchData(j);
                    }
                });
                jobs.sort(Utils.multiSort({name:'LocationsToInspect',primer:function(val){return val>0?0:1;}},$scope.OrgType=='OW'?'JobNumber':'WorkgroupJobNumber'));
            }
            if($scope.module.key == 'Completion'){
                jobs.forEach(function(j){
                    if(j.DispatchWizards){
                        setDispatchData(j);
                    }
                });
            }
            pickOutOnHoldJobs(jobs);
            return jobs;
        }

        var setUserData = function(resp){
            userData = Utils.copy(resp.User);
            userData.JobStartDateDefault = Utils.date(userData.JobStartDateDefault);
            delete userData.JobDay;
        }
        var result = function(resp){
            if(resp && resp.User){
                setUserData(resp);
                var keys = Object.keys(staticData);
                for(var i=0; i<keys.length; i++){
                    if(resp.User[keys[i]]){
                        var jobs = cleanJobs(resp.User[keys[i]]);
                        if(jobs.length){
                            var tab = addTab(keys[i], staticData[keys[i]].title, staticData[keys[i]].icon, $scope.module.options.detailPage, false);
                            console.log(tab)
                            for(var j=0; j<jobs.length; ++j){
                                if(!tab.jobs.find(function(jb){ return jb.JobID == jobs[j].JobID})){
                                    tab.jobs.push(jobs[j]);
                                }
                            }
                            console.log(tab.jobs)
                        }
                    }
                }
            }else{
                $scope.hasJobs = false;
            }
            $scope.working = false;
        }

        var checkOverflowX = function(el){
           var curOverflow = el.style.overflowX;
           if ( !curOverflow || curOverflow === "visible" )
              el.style.overflowX = "hidden";
           var isOverflowing = el.clientWidth < el.scrollWidth;
           el.style.overflowX = curOverflow;
           return isOverflowing;
        };

        var checkTabbarWidth = function(){
            var tb = tabbar._element[0];
            var inner = tb.getElementsByClassName('tab-bar');
            if(inner.length){
                var tabsInner = inner[0].getElementsByClassName('tab-bar__label');
                for(var i=0; i<tabsInner.length; i++){
                    tabsInner[i].setAttribute('style', $scope.styles.tabInner);
                }
                inner[0].setAttribute('style', $scope.styles.tabbar);
                if(!checkOverflowX(inner[0])){
                    inner[0].setAttribute('style', '');
                    for(var i=0; i<tabsInner.length; i++){
                        tabsInner[i].setAttribute('style', '');
                    }
                    $scope.styles.tab = {};
                }
            }
        };

        var addTab = function(key, title, icon, page, active){
            var tab;
            if((tab=$scope.data.tabs.find(function(t){ return t.sort==staticData[key].sort}))){
                return tab;
            }
            tab = {key: key, title: title, icon: icon, page: page, active: active, jobs:[], sort: staticData[key].sort};
            $scope.data.tabs.push(tab);
            $scope.data.tabs.sort(function(a,b){ return a.sort-b.sort })
            return tab;
        }

        var getPrompt = function(){
            if($scope.OrgType == 'OW')
                return 'Job #';
            return 'WG Job #';
        }
        $scope.getOrg = function(job){
            if($scope.OrgType == 'OW')
                return job.Workgroup;
            return job.Owner;
        };
        var getJobNumber = function(number){
            return number.replace(/ - Owner Job#: .+$/i, '');
        };
        $scope.getJobNumber = function(job){
            if($scope.OrgType == 'OW')
                return job.JobNumber;
            return getJobNumber(job.WorkgroupJobNumber);
        }
        $scope.getOtherJobNumber = function(job){
            var wg = getJobNumber(job.WorkgroupJobNumber);
            if(wg == job.JobNumber)
                return '';
            if($scope.OrgType == 'OW')
                return ' - ' + wg;
            return ' - ' + job.JobNumber;
            
        };
        var loadCompletionJob = function(){
            $scope.openingJob = true;
            var obj = {design: $scope.current.job.ActiveJobDesignID};
            ApiService.get('jobCompletion', obj).then(completionJobResult, completionJobResult)
        }
        var completionJobResult = function(resp){
            $scope.openingJob = false;
            if(resp && resp.JobDesign){
                $scope.current.job.design = cleanCompletionResponse(resp.JobDesign);
                $scope.current.job.design.lastLocationFetch = new Date().getTime();
                $scope.current.job.design.process = '';
                mainNav.pushPage($scope.module.options.detailPage);
            }else{
                if(resp && resp.locked && resp.message){
                    if(resp.locked == 'schedule' || ['PendingStartJob','MyPendingStartJob'].includes($scope.data.tabs[tabbar.getActiveTabIndex()].key)){
                        ons.notification.alert(resp.message, {title: "Cannot Begin Job"});
                    }else{
                        if($scope.module.key == 'Completion')
                            $scope.current.job.CJDDLocked = true;
                        ons.notification.alert(resp.message, {title: "Job Locked"});
                    }
                }else{
                    ons.notification.alert("Failed to load job data", {title: "Error"});
                }
            }
        };
        var cleanCompletionResponse = function(design){
            design.JobActualStartDate = design.JobActualStartDate?new Date(design.JobActualStartDate):'';
            if(!design.Location)
                design.Location = [];
            if(!Array.isArray(design.Location))
                design.Location = [design.Location];
            design.Location.map(cleanCompletionLocation);
            design.PercentComplete = Utils.fixPercent(design.PercentComplete);
            return design;
        };
        var cleanCompletionLocation = function(location){
            location.CompleteDate = location.CompleteDate?new Date(location.CompleteDate):'';
            location.PercentComplete = Utils.fixPercent(location.PercentComplete);
            return location;
        };

        var loadDispatchJob = function(force, position){
            $scope.openingJob = true;
            if(!force && $scope.current.job.DispatchWizards[0].map){
                Utils.getGpsPosition(function(position){
                    loadDispatchJob(true, position)
                }, function(){
                    loadDispatchJob(true)
                })
                return;
            }
            var obj = {job: $scope.current.job.JobID, dispatch: $scope.current.job.DispatchWizards[0].id, action: 'open'};
            if(position){
                obj.lat = position.coords.latitude
                obj.lon = position.coords.longitude
            }
            ApiService.get('dispatch', obj).then(dispatchJobResult, dispatchJobResult)
        }
        var dispatchJobResult = function(resp){
            $scope.openingJob = false;
            if(resp && resp.DispatchJob){
                $scope.current.job.dispatch = cleanCompletionResponse(resp.DispatchJob);
                mainNav.pushPage('app/dispatch/dispatch-job.html');
            }else{
                if(resp && resp.locked && resp.message){
                    $scope.current.job.CJDDLocked = true;
                    ons.notification.alert(resp.message, {title: "Job Locked"});
                }else{
                    ons.notification.alert("Failed to load job data", {title: "Error"});
                }
            }
        };

        var startJobResult = function(resp){
            if(resp && resp.JobDesign){
                var jobIndex = $scope.data.tabs[tabbar.getActiveTabIndex()].jobs.findIndex(function(j){
                    return j.JobID == $scope.current.job.JobID;
                });
                if(jobIndex > -1){
                    var currentTab = tabbar.getActiveTabIndex();
                    var job = $scope.data.tabs[currentTab].jobs.splice(jobIndex, 1);
                    var tabIndex = $scope.data.tabs.findIndex(function(t){
                        return t.key == 'ActiveJob'||t.key=='MyActiveJob';
                    });
                    if(tabIndex > -1){
                        $scope.data.tabs[tabIndex].jobs.unshift(job[0]);
                        tabbar.setActiveTab(tabIndex);
                        if(!$scope.data.tabs[currentTab].jobs.length){
                            $scope.data.tabs.splice(currentTab, 1);
                        }
                    }
                }
                
            }
            completionJobResult(resp);
        }
        var startJob = function(job, date){
            $scope.openingJob = true;
            var obj = {design: $scope.current.job.ActiveJobDesignID, date: Utils.date(date,'M/d/yyyy')};
            ApiService.get('completionBegin', obj).then(startJobResult, startJobResult)
        };
        $scope.openJob = function(job, tab){
            if($scope.openingJob)
                return;
            $scope.current.job = job;
            $scope.current.tab = $scope.data.tabs[tabbar.getActiveTabIndex()];
            if($scope.module.key == 'Completion'){
                if(['PendingStartJob','MyPendingStartJob'].includes(tab.key)){
                    $scope.startDialog._scope.done = function(start, date){
                        if(start && date){
                            var checkDate = Utils.date(date)
                            var limitDate = new Date(Utils.date(new Date(), 'M/d/yyyy'));
                            limitDate.setDate(limitDate.getDate()-userData.JobPriorStartDayLimit);
                            if(checkDate < limitDate){
                                Utils.notification.alert('Cannot start job earlier than <b>'+Utils.date(limitDate, 'M/d/yyyy')+'</b>.',{title:'Bad Date'});
                                return;
                            }
                            startJob(job, checkDate);
                        }
                        $scope.startDialog.hide();
                    }
                    $scope.startDialog._scope.jobNumber = $scope.getJobNumber(job);
                    var startDate = userData.JobStartDateDefault?userData.JobStartDateDefault:new Date();
                    $scope.startDialog._scope.model.start = Utils.date(startDate, 'yyyy-MM-dd');
                    Utils.openDialog($scope.startDialog);
                }else{
                    TECompletionService.resumeProcess($scope, job, function(){
                        if(job.DispatchWizards){
                            loadDispatchJob();
                        }else{
                            loadCompletionJob();
                        }
                    }, function(){
                        $scope.openingJob = true;
                    }, function(){
                        $scope.openingJob = false;
                    });
                }
            }else{
                mainNav.pushPage($scope.module.options.detailPage);
            }
        }

        var getDialogEndpoint = function(){
            switch($scope.module.key){
                case 'Inspection':
                    return 'jobInspection';
                break;
                case 'Completion':
                    return 'jobCompletion';
                break;
                case 'DesignJobs':
                    return 'designJob';
                break;
            }
        };

        $scope.jobOnLongPress = function(job, tab){
            if($scope.openingJob)
                return;
            navigator.vibrate(10);
            $scope.infoDialog.job = job;
            $scope.infoDialog.title = getPrompt()+': '+$scope.getJobNumber(job);
            $rootScope.$emit('JobInfoDialog', {type:'init', job: job, endpoint: getDialogEndpoint(), joblist:tab.key });
            Utils.openDialog($scope.infoDialog.dialog);
        };

        var fieldCompleteResult = function(job, result){
            $scope.openingJob = false;
            if(result && result.success){
                Utils.notification.toast('Field Completed '+$scope.getJobNumber(job), {timeout: 2000 });
                $scope.current.removeJob(job);
            }
        }
        $scope.fieldCompleteJob = function(job, $event){
            Utils.notification.confirm('Mark job <b>'+$scope.getJobNumber(job)+'</b> Field Complete?', {
                callback: function(ok){
                    if(ok){
                        $scope.openingJob = true;
                        var done = function(result){ fieldCompleteResult(job, result) }
                        ApiService.get('completionFieldComplete', {job:job.JobID}).then(done,done);
                    }
                }
            })
            return false;
        }

        var init = function(){
            $scope.loaded = true;
        };

        var saveStateFields = ['data','working', 'tabIndex', 'hasJobs'];
        $scope = StorageService.state.load('JobListHostController', $scope, saveStateFields);
        if(!$scope.hasJobs)
            $scope.loadJobs();
        $timeout(init);
        
        ons.orientation.on('change', function(){
            $timeout(checkTabbarWidth,0);
        });

        $scope.$on('$destroy', function(){
            StorageService.state.clear('JobListHostController');
            ons.orientation.off('change');
            if($scope.infoDialog.dialog.visible){
                $scope.infoDialog.dialog.hide();
            }
            if($scope.startDialog.visible){
                $scope.startDialog.hide();
            }
            $rootScope.$emit('JobInfoDialog', {type:'destroy'});
        });
    }]);

})();
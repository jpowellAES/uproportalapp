(function(){
    'use strict';

    angular.module('app').controller('CompleteLocationController', ['$scope', 'CommentDialog', 'ApiService', 'Utils', '$rootScope', 'Smartpick', '$anchorScroll', '$timeout', '$filter', 'UploadDialog', function($scope, CommentDialog, ApiService, Utils, $rootScope, Smartpick, $anchorScroll, $timeout, $filter, UploadDialog) {
        $scope.job = $scope.$parent.current.job;
        $scope.design = $scope.$parent.current.design;
        $scope.module = $scope.$parent.current.module;
        $scope.completing = false;
        $scope.tabIndex = 0;
        $scope.infoDialog = {};
        $scope.radioDialog = {};
        $scope.selectMode = false;
        Smartpick.reset();
        var data = false;
        var uploadsDialog;

        var sortDetails = function(){
            return;
            $scope.working = true;
            if($scope.location && $scope.location.LocationDetail){
                $scope.location.LocationDetail.sort(function(a,b){
                    if(a.IncompleteInspectionFlag < b.IncompleteInspectionFlag) return 1;
                    if(a.IncompleteInspectionFlag > b.IncompleteInspectionFlag) return -1;
                    
                    if(a.ActionWorkAction < b.ActionWorkAction) return -1;
                    if(a.ActionWorkAction > b.ActionWorkAction) return 1;
                    
                    if(a.SpecificationSpecificationNumber < b.SpecificationSpecificationNumber) return -1;
                    if(a.SpecificationSpecificationNumber > b.SpecificationSpecificationNumber) return 1;

                    return 0;
                });
            }
            $scope.working = false;
        };

        var cleanDetail = function(ld){
            if(ld.ParentAssemblyID){
                var assembly = $scope.location.LocationDetail.find(function(assm){
                    return assm.JobLocationDetailID == ld.ParentAssemblyID;
                });
                if(assembly){
                    ld.CalcAssemblyNumber = assembly.AssemblyNumber;
                    ld.CalcAssemblyDescription = assembly.AssemblyDescription;
                }
            }
            ld.PercentComplete = Utils.fixPercent(ld.PercentComplete);
            ld.CompleteDate = ld.CompleteDate?new Date(ld.CompleteDate):'';
            ld.LDChangeOrder = Utils.enforceArray(ld.LDChangeOrder);
        };
        var cleanDetails = function(){
            if(!$scope.location.LocationDetail)
                $scope.location.LocationDetail = [];
            if(!Array.isArray($scope.location.LocationDetail))
                $scope.location.LocationDetail = [$scope.location.LocationDetail];
            $scope.location.LocationDetail.map(cleanDetail);
        };

        var summarizeDetails = function(){
            var details = [], specIds = [];
            if(!$scope.location || !$scope.location.LocationDetail || !$scope.location.LocationDetail.length)
                return details;
            $scope.location.LocationDetail.forEach(function(ld){
                if(ld.AssemblyID)
                    return true;
                var index = specIds.indexOf(ld.SpecificationActionSpecificationID);
                if(index == -1){
                    index = specIds.length;
                    specIds.push(ld.SpecificationActionSpecificationID);
                    details.push(Object.assign({isSummary: true}, ld));
                }else{
                    details[index].DesignQuantity += ld.DesignQuantity;
                    details[index].EstimatedQuantity += ld.EstimatedQuantity;
                    details[index].CompletedToDate += ld.CompletedToDate;
                    details[index].RemainingQty += ld.RemainingQty;
                    details[index].Complete &= ld.Complete;
                }
            });
            return details.sort(function(a,b){
                if(a.Complete==b.Complete){
                    return a.SpecificationSpecificationNumber<b.SpecificationSpecificationNumber?-1:a.SpecificationSpecificationNumber>b.SpecificationSpecificationNumber?1:0;
                }
                return a.Complete?1:-1;
            });
        }
        $scope.$watch('location.LocationDetail', function(){
            $scope.location.summarizedDetails = summarizeDetails();
        }, true);

        var fetchDetails = function(){
            $scope.working = true;
            ApiService.get('locationCompletion', {location: $scope.location.JobLocationID}).then(fetchResult, fetchResult)
        };

        var fetchResult = function(resp){
            if(resp && resp.Location){
                if(resp.Location.LocationPhoto) $scope.location.LocationPhoto = Utils.enforceArray(resp.Location.LocationPhoto);
                $scope.location.hasPhoto = $scope.location.LocationPhoto.find(function(f){ return f.TypeCode == 'PH'});
                $scope.location.hasDocument = $scope.location.LocationPhoto.find(function(f){ return f.TypeCode == 'DO'});
                Object.assign($scope.location, Utils.pick(resp.Location, 'UseAssembliesFlag','ConvertedForLocationTimeFlag'));
                if(resp.Location.LocationDetail){
                    $scope.location.LocationDetail = resp.Location.LocationDetail;
                    $scope.location.lastDetailFetch = new Date().getTime();
                    cleanDetails();
                }
            }
            $scope.working = false;
        };

        $scope.detailComplete = function(detail, event){
            if(!detail.EstimatedQuantity && !detail.AssemblyID && $scope.design.CompleteZeroEstimateDefaultQty > 0){
                $scope.detailPartialComplete(detail, event)
                return;
            }
            if(detail.PartialOnly){
                $scope.detailPartialComplete(detail, event)
                return;
            }
            completeDetail(detail, 'complete');
        };

        $scope.detailPartialComplete = function(detail, $event){
            if(!$scope.completing && !detail.AssemblyID && !detail.isSummary){
                navigator.vibrate(10);
                $scope.completeDialog._scope.detail = detail;
                $scope.completeDialog._scope.commentRequired = ['LOI','LDI'].includes($scope.design.JobBillingUponCode);
                $scope.completeDialog._scope.qty = $scope.design.CompleteZeroEstimateDefaultQty||null;
                $scope.completeDialog._scope.estimated = detail.EstimatedQuantity;
                $scope.completeDialog._scope.remaining = detail.RemainingQty;
                $scope.completeDialog._scope.done = function(units, complete){
                    var num = parseFloat(units);
                    if(!Number.isNaN(num)){
                        completeDetail(detail, 'complete', num, complete, $scope.completeDialog._scope.detail.Comments);
                    }
                    $scope.completeDialog.hide();
                };
                $scope.completeDialog._scope.uncomplete = function(){
                    completeDetail(detail, 'uncomplete', null, null, $scope.completeDialog._scope.detail.Comments);
                    $scope.completeDialog.hide();
                }
                $scope.completeDialog.open();
            }
        };

        var loadBulkComplete = function(scope){
            $scope.bulkDialog._scope.loading = true;
            var obj = {
                action: 'bulk',
                detail: $scope.bulkDialog._scope.detail.JobLocationDetailID
            }
            ApiService.get('completeLocationDetail', obj).then(function(resp){loadBulkCompleteResult(resp, scope)}, function(resp){loadBulkCompleteResult(resp, scope)})
        }
        var loadBulkCompleteResult = function(resp, scope){
            $scope.bulkDialog._scope.loading = false;
            if(resp.LDetail){
                resp.LDetail.IncompleteLDetail = Utils.enforceArray(resp.LDetail.IncompleteLDetail);
                $scope.bulkDialog._scope.details = resp.LDetail.IncompleteLDetail;
                $scope.bulkDialog._scope.detail.ActionWorkAction = resp.LDetail.ActionWorkAction;
            }
        }

        $scope.openBulkComplete = function(){
            $scope.bulkDialog._scope.detail = $scope.completeDialog._scope.detail;
            $scope.bulkDialog._scope.selectedCount = 0;
            $scope.completeDialog.hide();
            loadBulkComplete($scope.bulkDialog._scope);
            $scope.bulkDialog._scope.done = function(){
                var completeIDs = $scope.bulkDialog._scope.details.filter(function(detail){
                    return detail.selected;
                }).map(function(detail){
                    return detail.LocationDetailID;
                }).join(',');
                completeDetail($scope.bulkDialog._scope.detail, 'bulk', true, completeIDs, $scope.bulkDialog._scope.detail.Comments);
                $scope.bulkDialog.hide();
            }
            Utils.openDialog($scope.bulkDialog);
        };

        $scope.detailUncomplete = function(detail, event){
            completeDetail(detail, 'uncomplete');
        };

        var completeWithComment = function(detailIds, comment){
            completeDetail(detailIds, 'complete', null, null, comment);
        }
        var completeDetail = function(detail, action, units, complete, comment){
            $scope.job.changed = true;
            var detailID = typeof detail == 'string'?detail:detail.JobLocationDetailID;
            var obj = {detail:detailID, action:action, all:detail.isSummary, comment: comment};
            if(detail.isSummary){
                obj.specID = detail.SpecificationActionSpecificationID;
            }
            if($scope.design.process && $scope.design.process.name == 'EndDay'){
                obj.date = $filter('date')($scope.design.process.date,'M/d/yyyy');
                obj.crew = $scope.job.JobDayCrewID;
            }
            if(typeof units == 'number'){
                obj.units = units;
                obj.complete = typeof complete=='string'?complete:(complete == true);
            }
            ApiService.post('completeLocationDetail', obj)
            .then(function(resp){
                if(resp.errors){
                    Utils.notification.alert(resp.errors.map(function(er){
                        return '<div>'+er.message+'</div>';
                    }).join(''), {title:"Error"})
                }
                completeResult(resp);
            },function(resp){
                completeResult(resp);
            });
            $scope.completing = true;
        };
        var completeLocationPercent = function(percent){
            $scope.job.changed = true;
            var obj = {location:$scope.location.JobLocationID, percent:percent, action:'complete'};
            if($scope.design.process && $scope.design.process.name == 'EndDay'){
                obj.date = $filter('date')($scope.design.process.date,'M/d/yyyy');
                obj.crew = $scope.job.JobDayCrewID;
            }
            ApiService.post('completeLocationDetail', obj)
            .then(function(resp){
                if(resp.errors){
                    Utils.notification.alert(resp.errors.map(function(er){
                        return '<div>'+er.message+'<ul>'+er.locations.map(function(loc){
                            var location = $scope.design.Location.find(function(l){ return l.JobLocationID==loc.id })
                            if(location){
                                return '<li>'+Utils.adjustLocationNumber(location.NumberChar)+' ('+(loc.percent)+'%)</li>';
                            }
                            return '';
                        }).join('')+'</ul></div>';
                    }).join(''), {title:"Warning"})
                }
                completeResult(resp);
            },function(resp){
                completeResult(resp);
            });
            $scope.completing = true;
        }

        var completeResult = function(resp){
            if(resp){
                if(resp.JobDesign){
                    if($scope.job.Incomplete !== '' && (resp.JobDesign.IsComplete||resp.JobDesign.IsComplete===false)){
                        $scope.job.Incomplete = !resp.JobDesign.IsComplete;
                    }
                    $scope.location.needsSort = true;
                    $scope.design.PercentComplete = Utils.fixPercent(resp.JobDesign.PercentComplete);
                    if(!Array.isArray(resp.JobDesign.Location))
                        resp.JobDesign.Location = [resp.JobDesign.Location];
                    resp.JobDesign.Location.forEach(function(newLocation){
                        var i = $scope.design.Location.findIndex(function(location){
                            return location.JobLocationID == newLocation.JobLocationID;
                        });
                        if(i > -1){
                            $scope.design.Location[i].Complete = newLocation.Complete;
                            $scope.design.Location[i].CompleteDate = newLocation.CompleteDate?new Date(newLocation.CompleteDate):'';
                            $scope.design.Location[i].PercentComplete = Utils.fixPercent(newLocation.PercentComplete);
                            $scope.design.Location[i].EarnedHours = newLocation.EarnedHours;
                        
                            if(Array.isArray($scope.design.Location[i].LocationDetail)){
                                if(!Array.isArray(newLocation.LocationDetail))
                                    newLocation.LocationDetail = [newLocation.LocationDetail];
                                newLocation.LocationDetail.forEach(function(newDetail){
                                    var j = $scope.design.Location[i].LocationDetail.findIndex(function(detail){
                                        return detail.JobLocationDetailID == newDetail.JobLocationDetailID;
                                    });
                                    if(j > -1){
                                        $scope.design.Location[i].LocationDetail[j].Complete = newDetail.Complete;
                                        $scope.design.Location[i].LocationDetail[j].CompleteDate = newDetail.CompleteDate ? new Date(newDetail.CompleteDate) : '';
                                        $scope.design.Location[i].LocationDetail[j].CompletedToDate = newDetail.CompletedToDate;
                                        $scope.design.Location[i].LocationDetail[j].RemainingQty = newDetail.RemainingQty;
                                        $scope.design.Location[i].LocationDetail[j].FailedInspectionFlag = newDetail.FailedInspectionFlag;
                                        $scope.design.Location[i].LocationDetail[j].CompleteToEstimateButton = newDetail.CompleteToEstimateButton;
                                    }
                                });
                            }
                        }
                    });
                }else if(resp.locked){
                    ons.notification.alert(resp.message, {title: "Error"});
                }
            }
            $scope.completing = false;
            $scope.location.LocationDetail.sort(function(a,b){
                if(a.Level1Sort != b.Level1Sort) return a.Level1Sort-b.Level1Sort;
                if(a.Level2Sort != b.Level2Sort) return a.Level2Sort-b.Level2Sort;
                if(a.Level3Sort != b.Level3Sort) return a.Level3Sort-b.Level3Sort;
                if(a.FailedInspectionFlag != b.FailedInspectionFlag) return a.FailedInspectionFlag?-1:1;
                if(a.Complete != b.Complete) return a.Complete?1:-1;
                if(a.ActionCodeCalc != b.ActionCodeCalc) return ('' + a.ActionCodeCalc).localeCompare(b.ActionCodeCalc);
                if(a.SpecificationSpecificationNumber != b.SpecificationSpecificationNumber)  return ('' + a.SpecificationSpecificationNumber).localeCompare(b.SpecificationSpecificationNumber);
                return 0;
            })
        };

        $scope.byDetailSortFunctions = [
            function(a,b){
                if(a.type != 'object') return 0;
                //if(a.value.Complete != b.value.Complete) return a.value.Complete?1:-1;
                if(a.value.SpecificationSpecificationNumber > b.value.SpecificationSpecificationNumber) return 1;
                if(a.value.SpecificationSpecificationNumber < b.value.SpecificationSpecificationNumber) return -1;
                if(a.value.CalcAssemblyNumber > b.value.CalcAssemblyNumber) return 1;
                if(a.value.CalcAssemblyNumber < b.value.CalcAssemblyNumber) return -1;
                return 0;
            },
            function(a,b){
                if(a.type != 'object') return 0;
                //if(a.value.Complete != b.value.Complete) return a.value.Complete?1:-1;
                if(a.value.CalcAssemblyNumber > b.value.CalcAssemblyNumber) return 1;
                if(a.value.CalcAssemblyNumber < b.value.CalcAssemblyNumber) return -1;
                if(a.value.SpecificationSpecificationNumber > b.value.SpecificationSpecificationNumber) return 1;
                if(a.value.SpecificationSpecificationNumber < b.value.SpecificationSpecificationNumber) return -1;
                return 0;
            }
        ]
        $scope.byDetailSortIndex = 0;
        $scope.changeByDetailSort = function(){
            $scope.radioDialog.openDialog({
                currentIndex: $scope.byDetailSortIndex, 
                title:'Sort', 
                items: [{label:'By Detail, By Assembly', value: 0},{label:'By Assembly, By Detail', value: 1}]
            }, function(ok, index){
                $scope.byDetailSortIndex = index;
                $scope.radioDialog.hide();
            });
        }

        $scope.detailOnLongPress = function(detail) {
            if(!$scope.completing){
                navigator.vibrate(10);
                detail.JobBillingUponCode = $scope.design.JobBillingUponCode;
                $rootScope.$emit('LocationDetailDialog', {
                    type:'init', 
                    detail: detail, 
                    endpoint: 'locationDetailCompletion',
                    location: $scope.location,
                    design: $scope.design
                });
                Utils.openDialog($scope.infoDialog.dialog);
            }
        };

        var addSpecAction = function(specAction, qty, complete, comments, assemblyNum, assemblyDesc, wireSpan, action){
            $scope.job.changed = true;
            $scope.completing = true;
            var obj = {location:$scope.location.JobLocationID,specAction:specAction.ID,qty:qty, action:'add', complete: complete, comments:comments}
            if(assemblyNum){
                obj.assemblyNum = assemblyNum;
                obj.assemblyDesc = assemblyDesc;
                obj.wireSpan = wireSpan;
                obj.actionID = action.ActionID;
            }
            if($scope.design.process && $scope.design.process.name == 'EndDay'){
                obj.date = $filter('date')($scope.design.process.date,'M/d/yyyy');
                obj.crew = $scope.job.JobDayCrewID;
            }
            ApiService.post('completeLocationDetail', obj)
            .then(addSpecActionResult,addSpecActionResult);
        };

        var addSpecActionResult = function(resp){
            if(resp){
                if(resp.JobDesign){
                    if(!Array.isArray(resp.JobDesign.Location.LocationDetail))
                        resp.JobDesign.Location.LocationDetail = [resp.JobDesign.Location.LocationDetail];
                    if(!Array.isArray($scope.location.LocationDetail))
                        $scope.location.LocationDetail = [];
                    resp.JobDesign.Location.LocationDetail.forEach(function(ld){
                        cleanDetail(ld);
                        $scope.location.LocationDetail.push(ld);
                    });
                    navigateToNewItem(resp.JobDesign.Location.LocationDetail[0]);
                    $scope.design.PercentComplete = Utils.fixPercent(resp.JobDesign.PercentComplete);
                    $scope.location.Complete = resp.JobDesign.Location.Complete;
                    $scope.location.CompleteDate = resp.JobDesign.Location.CompleteDate?new Date(resp.JobDesign.Location.CompleteDate):'';
                    $scope.location.PercentComplete = Utils.fixPercent(resp.JobDesign.Location.PercentComplete);
                    $scope.location.PlannedHours = resp.JobDesign.Location.PlannedHours;
                    $scope.location.ApprovedChangeOrderHours = resp.JobDesign.Location.ApprovedChangeOrderHours;
                    $scope.location.EarnedHours = resp.JobDesign.Location.EarnedHours;
                }else if(resp.locked){
                    ons.notification.alert(resp.message, {title: "Error"});
                }
            }
            $scope.completing = false;
        };

        var navigateToNewItem = function(detail){
            if($scope.location.UseAssembliesFlag){
                $scope.goToAssembly(detail);
            }else{
                $scope.goToDetail(detail);
            }
        }

        $scope.addDetail = function(){
            if($scope.location.UseAssembliesFlag){
                addAssembly();
            }else{
                addNormalDetail();
            }
            
        };

        var getOwnerID = function(){
            return $scope.job.OwnerID
        }

        var getSpecActionData = function(){
            if(!data){
                var obj = {job:$scope.job.JobID,owner:getOwnerID(),assemblies:$scope.location.UseAssembliesFlag};
                ApiService.get('completionSpecActions', obj)
                .then(function(resp){
                    if(resp && resp.ReferenceAction){
                        data = resp;
                        $scope.addDetailDialog.setData(resp);
                        $scope.addAssemblyDialog.setData(resp);
                    }
                });
            }
        };

        var addNormalDetail = function(){
            getSpecActionData();
            $scope.addDetailDialog.openDialog();
        };

        var addAssembly = function(){
            getSpecActionData();
            $scope.addAssemblyDialog.openDialog();
        };

        $scope.getDetailClass = function(detail, byDetail){
            var cls = '';
            if(detail.FailedInspectionFlag){
                cls += ' red';
            }
            if(detail.Complete && detail.CompleteToEstimateButton != 'Uncomplete'){
                cls += ' faded';
            }
            if(!byDetail && $scope.location.UseAssembliesFlag && !detail.AssemblyID){
                cls += ' assembly-item';
            }
            if(!detail.AssemblyID && detail.LDChangeOrder && detail.LDChangeOrder.length){
                cls += ' blue';
            }
            return cls;
        }

        $scope.goToAssembly = function(detail){
            switch($scope.tabIndex){
                case 0:
                    var assemblyID = detail.AssemblyID?detail.JobLocationDetailID:detail.ParentAssemblyID;
                    $scope.location.LocationDetail.forEach(function(ld){
                        if(!ld.AssemblyID){
                            ld.visible = ld.ParentAssemblyID==assemblyID;
                        }
                    });
                    $timeout(function() {
                        $anchorScroll('anchor'+assemblyID);
                        Utils.flash('anchor'+assemblyID);
                    });
                break;
            }
        };

        $scope.goToDetail = function(detail){
            var detailID = detail.JobLocationDetailID;
            $timeout(function() {
                $anchorScroll('anchor'+detailID);
                Utils.flash('anchor'+detailID);
            });
        };

        $scope.openLocationDetail = function(detail){
            console.error('openLocationDetail')
            if(detail.AssemblyID){
                var id = detail.JobLocationDetailID;
                for(var i=0; i<$scope.location.LocationDetail.length; i++){
                    if($scope.location.LocationDetail[i].ParentAssemblyID == id){
                        $scope.location.LocationDetail[i].visible = !$scope.location.LocationDetail[i].visible;
                    }
                }
            }else{
                if($scope.selectMode && !detail.Complete && (detail.CompleteToEstimateButton == 'Complete' || detail.CompleteToEstimateButton == 'Complete All')){
                    detail.selected = !detail.selected2;
                    updateParentSelected(detail.ParentAssemblyID);
                    detail.selected2 = detail.selected;
                }
            }
        }
        var updateParentSelected = function(parentID){
            if(parentID){
                var count = 0, parent = null;
                for(var i=0; i<$scope.location.LocationDetail.length; i++){
                    var detail = $scope.location.LocationDetail[i];
                    if(detail.JobLocationDetailID == parentID){
                        parent = detail;
                    }
                    if(detail.ParentAssemblyID == parentID){
                        if(!detail.Complete && detail.CompleteToEstimateButton == 'Complete'){
                            if(!detail.selected){
                                ++count;
                            }
                        }
                    }
                }
                if(parent){
                    parent.selected = parent.selected2 = count==0;
                }
            }
        }

        $scope.showDetailRow = function(detail){
            if($scope.location.UseAssembliesFlag && !detail.AssemblyID){
                return detail.visible;
            }
            return true;
        }

        $scope.filterByDetail = function(detail){
            return !detail.AssemblyID;
        }

        $scope.multiSelect = function(mode){
            if(mode && $scope.location.ConvertedForLocationTimeFlag){
                $scope.locationPercentDialog.openDialog()
                return;
            }
            $scope.selectMode = mode;
            $scope.location.LocationDetail.forEach(function(ld){
                ld.selected = false;
                ld.selected2 = false;
            });
            CommentDialog.hide();
            completionDetailsTabbar._element[0]._tabbarBorder.style.width = mode?'50%':'33.33%';
            completionDetailsTabbar._element[0]._tabbarBorder.style.transform = 'translate3d('+(completionDetailsTabbar.getActiveTabIndex()*100)+'%, 0px, 0px)';
        }
        $scope.multiSelectBox = function(detail, $event){
            if(detail.AssemblyID){
                var selected = !detail.selected2;
                for(var i=0; i<$scope.location.LocationDetail.length; i++){
                    if($scope.location.LocationDetail[i].ParentAssemblyID == detail.JobLocationDetailID){
                        $scope.location.LocationDetail[i].selected = $scope.location.LocationDetail[i].selected2 = selected;
                    }
                }
                $timeout(function(){
                    detail.selected = detail.selected2 = selected;
                })
            }
        }
        var dialogOptions = {
            title: 'Complete Multiple',
            accept: function(comment, notUsed, items){
                completeWithComment(items.map(function(ld){return ld.JobLocationDetailID}).join(','), comment);
                $scope.multiSelect(false);
            },
            requireComment: false,
            placeholder: 'Comment (optional)',
            button: ['accept','Complete'],
            cancel: $scope.multiSelect,
            select: {
                callback: function(){$scope.multiSelect(false)}
            }
        };
        var getSelectedItems = function(){
            return $scope.location.LocationDetail.filter(function(ld){
                return !ld.AssemblyID && ld.selected;
            })
        }
        $scope.completeAllDialog = function(){
            dialogOptions.select.items = getSelectedItems();
            dialogOptions.accept('', '', dialogOptions.select.items);
            //CommentDialog.show(dialogOptions);
        }
        $scope.canFinishSelect = function(details){
            if(!details) return;
            for(var i=0; i<details.length; i++){
                if(!details[i].AssemblyID && details[i].selected){
                    return false;
                }
            }
            return true;
        }

        $scope.canNav = {
            left: true,
            right: true
        }
        var calcCanNav = function(){
            $scope.canNav.left = mainNav.topPage.pushedOptions.locationIndex > 0;
            $scope.canNav.right = mainNav.topPage.pushedOptions.locationIndex < $scope.design.Location.length-1;
        }
        $scope.navigateLocation = function(step){
            mainNav.topPage.pushedOptions.locationIndex += step;
            $scope.location = $scope.design.Location[mainNav.topPage.pushedOptions.locationIndex];
            $scope.location.LocationPhoto = Utils.enforceArray($scope.location.LocationPhoto);
            init();
        }

        $scope.endDay = function(){
            $scope.current.nextAction = 'closeJob';
            mainNav.popPage();
        }

        var uploadResult = function(resp){
            $scope.completing = false;
            uploadsDialog.setOpening(false)
            if(resp && resp.Key){
                $scope.location.LocationPhoto.push({String:resp.Key,Description:resp.Description,TypeCode:resp.TypeCode});
                $scope.location.hasPhoto = $scope.location.hasPhoto||resp.TypeCode=='PH';
                var uploads = $scope.location.LocationPhoto.map(function(photo){
                    return {Key: photo.String, Description: photo.Description, TypeCode: photo.TypeCode}
                });
                if(uploadsDialog.visible){
                    uploadsDialog.setUploads(uploads);
                }else{
                    uploadsDialog.open($scope.location.NumberChar, uploads, $scope.location.hasDocument?'PH':'');
                }
                
            }
        }
        var uploadsPrecall = function(){
            $scope.completing = true;
            if(uploadsDialog.visible){
                uploadsDialog.setOpening(true)
            }
        }
        var openUploads = function(type){
            uploadsDialog.open($scope.location.NumberChar, $scope.location.LocationPhoto.map(function(photo){
                return {Key: photo.String, Description: photo.Description, TypeCode: photo.TypeCode}
            }), type);
        }
        $scope.openPhotos = function(){
            if(!$scope.location.hasPhoto){
                $scope.$parent.current.showPhotoUpload(uploadsPrecall, uploadResult, $scope.location.JobLocationID)
            }else{
                openUploads('PH')
            }
        }
        $scope.openDocuments = function(){
            openUploads('DO')
        }

        var init = function(){
            $scope.location = $scope.design.Location[mainNav.topPage.pushedOptions.locationIndex];
            $scope.location.LocationPhoto = Utils.enforceArray($scope.location.LocationPhoto);
            completionDetailsTabbar.on('prechange', function(e){
                $timeout(function(){
                    $scope.tabIndex = e.index;
                })
                
            });
            calcCanNav();
            var now = new Date().getTime();
            fetchDetails();
        };

        $timeout(init);

        $scope.$on('$destroy', function(){
            if($scope.infoDialog.dialog.visible){
                $scope.infoDialog.dialog.hide();
            }
            if($scope.completeDialog.visible){
                $scope.completeDialog.hide();
            }
            if($scope.bulkDialog.visible){
                $scope.bulkDialog.hide();
            }
            if($scope.addDetailDialog.visible){
                $scope.addDetailDialog.hide();
            }
            if($scope.addAssemblyDialog.visible){
                $scope.addAssemblyDialog.hide();
            }
            if($scope.radioDialog.visible){
                $scope.radioDialog.hide();
            }
            if($scope.locationPercentDialog.visible){
                $scope.locationPercentDialog.hide()
            }
            if($scope.percentDialog.visible){
                $scope.percentDialog.hide()
            }
            CommentDialog.hide();
            Smartpick.reset();
            Smartpick.hide();            
            UploadDialog.hide();
            $rootScope.$emit('LocationDetailDialog', {type:'destroy'});
        });

        var getPercentComplete = function(){
            var est = 0, cmp = 0
            for(var i=0; i<$scope.location.LocationDetail.length; ++i){
                var detail = $scope.location.LocationDetail[i]
                est += detail.EstimatedQuantity
                cmp += detail.CompletedToDate
            }
            return Utils.fixPercent(est>0?(cmp/est):0)
        }

        var createDialogs = function(){
            //Uploads dialog
            ons.createDialog('app/dialog/uploads/uploads-dialog.html').then(function(dialog){
                dialog._scope.types = [{code:'PH',label:'Photos'},{code:'DO',label:'Documents'}]
                dialog._scope.canEdit = true;
                dialog._scope.settings = {};
                dialog.open = function(title, uploads, type){
                  dialog._scope.title = title;
                  dialog._scope.openingUpload = false;
                  dialog.setType(type);
                  dialog.setUploads(uploads);
                  Utils.openDialog(dialog);
                };
                dialog.setUploads = function(uploads){
                  dialog._scope.uploads = uploads;
                }
                dialog.setType = function(type){
                    dialog._scope.settings.initType = type||'';
                    dialog._scope.settings.type = type||'';
                    if(type){
                        $timeout(function(){
                            uploadsTypeTabBar.on('prechange', function(e){                            
                                dialog._scope.$apply(function(){dialog._scope.settings.type = e.index?'DO':'PH';})
                            })
                        })
                    }
                }
                dialog.setOpening = function(opening){
                    dialog._scope.openingUpload = opening;
                }
                dialog._scope.addUpload = function(){
                    $scope.$parent.current.showPhotoUpload(uploadsPrecall, uploadResult, $scope.location.JobLocationID)
                }
                dialog._scope.captionEdited = function(upload){
                    var photo;
                    if(photo = $scope.location.LocationPhoto.find(function(dp){ return dp.String == upload.Key})){
                        photo.Description = upload.Description;
                    }
                }
                dialog.on('posthide', function(){
                    dialog._scope.$apply(function(){
                        dialog._scope.settings.type = '';
                        dialog._scope.settings.initType = '';
                    });
                })
                uploadsDialog = dialog;
            });
            ons.createDialog('app/dialog/location-detail-info-dialog.html').then(function(dialog){
                $scope.infoDialog.dialog = dialog;
            });
            ons.createAlertDialog('app/completework/complete-partial-dialog.html').then(function(dialog){
                $scope.completeDialog = dialog;
                dialog.open = function(){
                    Utils.openDialog(dialog, {select:true});
                }
                dialog._scope.openBulkComplete = $scope.openBulkComplete;
            });
            ons.createDialog('app/completework/location-percent-complete-dialog.html').then(function(dialog){
                $scope.locationPercentDialog = dialog;
                dialog._scope.data = {}
                dialog.openDialog = function(){
                    var percent = getPercentComplete()
                    dialog._scope.data = {
                        showJobs: false,
                        jobs: [{
                            Location: [
                                {
                                    NumberChar: $scope.location.NumberChar,
                                    JobLocationID: $scope.location.JobLocationID,
                                    PercentComplete: percent,
                                    OriginalPercentComplete: percent
                                }
                            ]
                        }]
                    }
                    Utils.openDialog(dialog)
                }
                dialog._scope.adjustLocationNumber = Utils.adjustLocationNumber
                dialog._scope.editPercent = function(location){
                    $scope.percentDialog.open(location.PercentComplete, function(value){
                        location.PercentComplete = value
                    }, {
                        title: 'Location '+location.NumberChar
                    })
                }
                dialog._scope.done = function(){
                    if(dialog._scope.data.jobs.find(function(jb){ return jb.Location.find(function(loc){ return loc.PercentComplete != loc.OriginalPercentComplete; }); })){
                        completeLocationPercent(dialog._scope.data.jobs[0].Location[0].PercentComplete/100)
                    }
                    dialog.hide();
                }
            });
            ons.createAlertDialog('app/timesheet/timesheet-percent-dialog.html').then(function(dialog){
                $scope.percentDialog = dialog;
                dialog._scope.model = {}
                dialog.open = function(value, callback, options){
                    dialog._scope.model.value = value;
                    dialog._scope.options = options||{};
                    dialog._scope.done = function(ok){
                        if(ok){
                            dialog._scope.model.value = Math.max(0,Math.min(100,dialog._scope.model.value))
                            $timeout(function(){
                                if(dialog._scope.options.validate&&!dialog._scope.options.validate(dialog._scope.model.value)){
                                    return;
                                }
                                callback(dialog._scope.model.value);
                            }, 10);

                        }
                        dialog.hide();
                    }
                    Utils.openDialog(dialog);
                }
            });
            ons.createAlertDialog('app/completework/complete-bulk-dialog.html').then(function(dialog){
                var bulkCompleteCheckCommentRequired = function(){
                    dialog._scope.commentRequired = false;
                    if(!['LDI','LOI'].includes($scope.design.JobBillingUponCode)){
                        return;
                    }
                    var detail = dialog._scope.details.find(function(d){ return d.selected&&d.DesignQty!=d.EstimatedQty});
                    if(detail){
                        dialog._scope.commentRequired = true;
                    }
                }
                dialog._scope.updateSelectedCount = function(){
                    dialog._scope.selectedCount = dialog._scope.details.reduce(function(count, detail){
                        if(detail.selected)
                            ++count;
                        return count;
                    }, 0);
                    bulkCompleteCheckCommentRequired();
                };
                dialog._scope.selectAll = function(select){
                    dialog._scope.details.forEach(function(detail){
                        detail.selected = select;
                    });
                    dialog._scope.updateSelectedCount();
                }
                dialog._scope.selectDetail = function(detail, $event){
                    detail.selected = !detail.selected;
                    dialog._scope.updateSelectedCount();
                    $event.stopPropagation();
                    $event.preventDefault();
                    return false;
                }
                $scope.bulkDialog = dialog;
            });
            var newDetailRequireComment = function(){
                if(!['LOI','LDI'].includes($scope.design.JobBillingUponCode) && $scope.design.JobContractTypeCode == 'BL'){
                    return false;
                }
                return true;
            }
            ons.createAlertDialog('app/completework/create-location-detail-dialog.html').then(function(dialog){
                $scope.addDetailDialog = dialog;
                $scope.addDetailDialog._scope.form = {};
                $scope.addDetailDialog._scope.options = {requireComment:newDetailRequireComment()}
                $scope.addDetailDialog.openDialog = function(){
                    $scope.addDetailDialog._scope.selects = {action:{}};
                    $scope.addDetailDialog._scope.form = {};
                    $scope.addDetailDialog.defaultSelection();
                    Utils.openDialog($scope.addDetailDialog);
                }
                $scope.addDetailDialog.setData = function(data){
                    $scope.addDetailDialog._scope.data = data;
                    $scope.addDetailDialog.defaultSelection();
                }
                $scope.addDetailDialog.defaultSelection = function(){
                    $scope.addDetailDialog._scope.form.qty = 1;
                    $scope.addDetailDialog._scope.form.comments = '';
                    if($scope.addDetailDialog._scope.data){
                        var action = $scope.addDetailDialog._scope.data.ReferenceAction.find(function(a){
                            return a.WorkAction == 'Install';
                        });
                        if(action){
                            $scope.addDetailDialog._scope.selects.action = action;
                        }
                    }
                }
                $scope.addDetailDialog._scope.tagHandler = function(tag){
                    return null;
                };
                $scope.addDetailDialog._scope.selectAction = function($item, $model){
                    delete $scope.addDetailDialog._scope.selects.spec;
                };
                $scope.addDetailDialog._scope.searchSpec = function(){
                    if($scope.addDetailDialog._scope.selects.spec && Smartpick.ok()){
                        Smartpick.show();
                    }else{
                        Smartpick.show({
                            endpoint: 'completionSpecActions',
                            endpointData: {
                                actionID: $scope.addDetailDialog._scope.selects.action.ActionID,
                                job: $scope.job.JobID,
                                owner:getOwnerID(),
                                assemblies:$scope.location.UseAssembliesFlag
                            },
                            itemView: 'SpecificationAction',
                            itemTemplate: 'completion-pick-spec.html',
                            title: 'Search Specs',
                            extend: {
                                openDocument: function(injector){
                                    return function(item, $event){
                                        $event.stopPropagation();
                                        if(item.opening)return; 
                                        item.opening = true;
                                        injector.get('ImgOrPdf').load('spec/'+item.SpecificationSpecDocumentID).finally(function(){item.opening = false;});
                                    }
                                },
                                filterLimitThreshold: function(){return 75;}
                            },
                            filters: [
                                {key: 'SpecBook', label:'Spec Book', from: 'ID', to:'SpecificationBookID', display:'Name'},
                                {key: 'Section', from: 'ID', to:'SpecificationSectionID', display:'Name', dependencies:{'SpecBook':{from:'SpecBookID',to:'ID'}}},
                                {key: 'SubSection', label:'Sub Section', from: 'ID', to:'SpecificationSubSectionID', display:'Name', dependencies:{'Section':{from:'SectionID',to:'ID'}}}
                            ],
                            minSearchLength: $scope.design.OwnerMinimumSpecSearchLength||3
                        }, function(item){
                            $scope.addDetailDialog._scope.selects.spec = item;
                        });
                    }
                };
                
                $scope.addDetailDialog._scope.selects = {action:{}};
                $scope.addDetailDialog._scope.done = function(add, specAction, qty, complete, comments){
                    if(add && specAction){
                        addSpecAction(specAction, qty, complete, comments);
                    }
                    Smartpick.clearFilters();
                    $scope.addDetailDialog.hide();
                };
            });
            ons.createAlertDialog('app/completework/create-ld-assembly-dialog.html').then(function(dialog){
                $scope.addAssemblyDialog = dialog;
                $scope.addAssemblyDialog._scope.form = {};
                $scope.addAssemblyDialog._scope.options = {requireComment:newDetailRequireComment()}
                $scope.addAssemblyDialog.openDialog = function(){
                    $scope.addAssemblyDialog._scope.selects = {action:{}};
                    $scope.addAssemblyDialog._scope.form = {};
                    $scope.addAssemblyDialog._scope.step = 1;
                    $scope.addAssemblyDialog.defaultSelection();
                    Utils.openDialog($scope.addAssemblyDialog);
                }
                $scope.addAssemblyDialog.setData = function(data){
                    $scope.addAssemblyDialog._scope.data = data;
                    $scope.addAssemblyDialog.defaultSelection();
                }
                $scope.addAssemblyDialog.defaultSelection = function(){
                    $scope.addAssemblyDialog._scope.form.qty = 1;
                    if($scope.addAssemblyDialog._scope.data){
                        var action = $scope.addAssemblyDialog._scope.data.ReferenceAction.find(function(a){
                            return a.WorkAction == 'Install';
                        });
                        if(action){
                            $scope.addAssemblyDialog._scope.selects.action = action;
                        }
                    }
                }
                $scope.addAssemblyDialog._scope.tagHandler = function(tag){
                    return null;
                };
                $scope.addAssemblyDialog._scope.selectAction = function($item, $model){
                    //delete $scope.addAssemblyDialog._scope.selects.spec;
                };
                $scope.addAssemblyDialog._scope.searchSpec = function(){
                    if($scope.addAssemblyDialog._scope.selects.spec && Smartpick.ok()){
                        Smartpick.show();
                    }else{
                        Smartpick.show({
                            endpoint: 'completionSpecActions',
                            endpointData: {
                                actionID: $scope.addAssemblyDialog._scope.selects.action.ActionID,
                                job: $scope.job.JobID,
                                owner:getOwnerID(),
                                assemblies:$scope.location.UseAssembliesFlag
                            },
                            itemView: 'SpecificationAction',
                            itemTemplate: 'completion-pick-spec.html',
                            title: 'Search Assemblies',
                            extend: {
                                openDocument: function(injector){
                                    return function(item, $event){
                                        $event.stopPropagation();
                                        if(item.opening)return; 
                                        item.opening = true;
                                        injector.get('ImgOrPdf').load('spec/'+item.SpecificationSpecDocumentID).finally(function(){item.opening = false;});
                                    }
                                },
                                filterLimitThreshold: function(){return 75;}
                            },
                            filters: [
                                {key: 'SpecBook', label:'Spec Book', from: 'ID', to:'SpecificationBookID', display:'Name'},
                                {key: 'Section', from: 'ID', to:'SpecificationSectionID', display:'Name', dependencies:{'SpecBook':{from:'SpecBookID',to:'ID'}}},
                                {key: 'SubSection', label:'Sub Section', from: 'ID', to:'SpecificationSubSectionID', display:'Name', dependencies:{'Section':{from:'SectionID',to:'ID'}}}
                            ]
                        }, function(item){
                            $scope.addAssemblyDialog._scope.selects.spec = item;
                            $scope.addAssemblyDialog._scope.form.assemblyNumber = item.SpecificationSpecificationNumber;
                            $scope.addAssemblyDialog._scope.form.assemblyDesc = item.SpecificationDescription;
                        });
                    }
                };
                $scope.addAssemblyDialog._scope.selects = {action:{}};
                $scope.addAssemblyDialog._scope.form = {};
                $scope.addAssemblyDialog._scope.goStep = function(stp){
                    $scope.addAssemblyDialog._scope.step = stp;
                };
                $scope.addAssemblyDialog._scope.done = function(add, specAction, form, action, complete){
                    if(add){
                        addSpecAction(specAction, form.qty, complete, form.comments, form.assemblyNumber, form.assemblyDesc, form.wireSpan, action);
                    }
                    Smartpick.clearFilters();
                    $scope.addAssemblyDialog.hide();
                };
            });
            ons.createAlertDialog('app/dialog/radio-button-dialog.html').then(function(dialog){
                $scope.radioDialog = dialog;
                dialog._scope.data = {}
                dialog._scope.selectRow = function(index){
                    dialog._scope.data.currentIndex = index;
                }
                dialog.openDialog = function(options, done){
                    dialog._scope.data.title = options.title||'Select';
                    dialog._scope.data.currentIndex = options.currentIndex||dialog._scope.data.currentIndex||0;
                    dialog._scope.data.items = options.items||[];
                    dialog._scope.done = done;
                    Utils.openDialog(dialog);
                }
            });
        }
        createDialogs();
        
    }]);

})();
(function(){
    'use strict';

    angular.module('app').service('DispatchService', ['Utils', 'ApiService', 'SaveService', '$timeout', 'StorageService', '$anchorScroll', function(Utils, ApiService, SaveService, $timeout, StorageService, $anchorScroll){

        var _dialog = null;
        var _setDialog = function(dialog){
          _dialog = dialog;
        }

        var _showMessage = function(msg, title, buttons){
            $timeout(function(){
                if(_dialog){
                    _dialog.open({
                        title: title||'Notice',
                        content: msg,
                        buttons: buttons||[{
                            label: 'Ok'
                        }],
                        cancelable: true
                    })
                }
            });
        }
        var _showHTMLMessage = function(msg, title, buttons){
            $timeout(function(){
                if(_dialog){
                    _dialog.open({
                        title: title||'Notice',
                        html: msg,
                        buttons: buttons||[{
                            label: 'Ok'
                        }],
                        cancelable: true
                    })
                }
            });
        }

        var _changes = function(job, changes){
            if(!changes){
                job.changes = false;
            }else if(!job.changes){
                job.changes = true;
            }
        }

        var _cleanDialog = function(job, restore){
            if(!job.dialog1 || !job.dialog1.visible) return;
            //Note: use the below to deflate/inflate a dialog to persist its state (if needed)
            /*if(job.dialog1.which == '<dialog>'){
                if(restore){
                    job.dialog1.member = _currentDay(job).members.find(function(dm){ return dm.PersonID == job.dialog1.member.PersonID && dm.ClassID == job.dialog1.member.ClassID });
                }else{
                    job.dialog1.member = Utils.pick(job.dialog1.member, 'PersonID', 'ClassID');
                }
            }*/
        }
        var _stripJob = function(job){
            for(var f=0; f<job.DispatchField.length; ++f){
                delete job.DispatchField[f].updateValue;
                delete job.DispatchField[f].dependers;
            }
            for(var f=0; f<job.Field.length; ++f){
                delete job.Field[f].dependers;
            }
        }

        var _assignOption = function(job, locationField){
            if(job.fields[locationField.DispatchJobFieldID]){
                locationField.option = job.fields[locationField.DispatchJobFieldID].FieldOption.find(function(option){
                    return option.FieldValue == locationField.FieldValue
                });
            }
        }
        var _cleanLocationFieldsInner = function(locationField, location, job){
            if(job.fields[locationField.DispatchJobFieldID]){
                locationField.order = job.fields[locationField.DispatchJobFieldID].OrderNumber;
                if(['DROP','RADIO'].includes(job.fields[locationField.DispatchJobFieldID].TypeCode) && locationField.FieldValue){
                    _assignOption(job, locationField)
                }else if(['NUM','DOL'].includes(job.fields[locationField.DispatchJobFieldID].TypeCode)){
                    locationField.FieldValue = _transformNumber(locationField.FieldValue)
                }else if(job.fields[locationField.DispatchJobFieldID].TypeCode == 'CHECK'){
                    locationField.FieldValue = _transformBool(locationField.FieldValue);
                }
            }else{
                locationField.order = location.LocationField.length + f;
            }
        }
        var _cleanLocationFields = function(job, location){
            location.LocationField = Utils.enforceArray(location.LocationField)
            var jobFields = job.Field.reduce(function(obj, item){ obj[item.DispatchJobFieldID] = item; return obj;}, {});
            for(var f=0; f<location.LocationField.length; ++f){
                delete jobFields[location.LocationField[f].DispatchJobFieldID];
                _cleanLocationFieldsInner(location.LocationField[f], location, job);
            }
            var newFields = Object.keys(jobFields);
            for(var f=0; f<newFields.length; ++f){
                _populateDefaultValue(jobFields[newFields[f]], location, job)
            }
        }
        var _cleanPhotos = function(location){
            location.LPhoto = Utils.enforceArray(location.LPhoto);
            for(var i=0; i<location.LPhoto.length; ++i){
                if(location.LPhoto[i].TypeCode == 'DO') location.hasDocument = true;
                if(location.LPhoto[i].TypeCode == 'PH') location.hasPhoto = true;
                location.LPhoto[i].String = location.LPhoto[i].UploadID+'?t='+location.LPhoto[i].Token;
            }
        }
        var _cleanLocations = function(scope, copyFrom){
            var job = scope.job
            var locations = Utils.enforceArray(copyFrom?copyFrom.DispatchLocation:job.DispatchLocation);
            job.DispatchLocation = copyFrom?job.DispatchLocation:locations
            for(var l=0; l<locations.length; ++l){
                locations[l].WorkDate = Utils.date(locations[l].WorkDate)
                locations[l].WorkDate.clearTime()
                locations[l].sequence = locations[l].sequence||_getNextSequence(job)
                locations[l].LocationComplete = locations[l].LocationComplete||false;
                _cleanLocationFields(job, locations[l])
                _cleanPhotos(locations[l]);
                if(locations[l].ApplyDefaults || !locations[l].LocationField.find(function(lf){return lf.FieldValue !== ''})){
                    _populateDefaultValues(job, locations[l]);
                    delete locations[l].ApplyDefaults;
                }
                if(copyFrom){
                    job.DispatchLocation.push(locations[l]);
                    scope.original.DispatchLocation.push(Utils.copy(locations[l]));
                }
                _evaluateAllConditions(job, locations[l]);
            }
            return locations;
        }

        var _parseConditionDependencies = function(job, jobField){
            for(var f=0; f<job.Field.length; ++f){
                if(job.Field[f].DispatchJobFieldID == jobField.DispatchJobFieldID) continue;
                if(job.Field[f].Condition){
                    for(var c=0; c<job.Field[f].Condition.length; ++c){
                        if(job.Field[f].Condition[c].CompareFieldID == jobField.DispatchJobFieldID){
                            jobField.conditionDependers = jobField.conditionDependers||[]
                            if(!jobField.conditionDependers.includes(job.Field[f])){
                                jobField.conditionDependers.push(job.Field[f]);
                            }
                        }
                    }
                }
                if(['DROP','RADIO'].includes(job.Field[f].TypeCode)){
                    for(var o=0; o<job.Field[f].FieldOption.length; ++o){
                        if(job.Field[f].FieldOption[o].OptionCondition){
                            for(var c=0; c<job.Field[f].FieldOption[o].OptionCondition.length; ++c){
                                if(job.Field[f].FieldOption[o].OptionCondition[c].CompareFieldID == jobField.DispatchJobFieldID){
                                    jobField.conditionOptionDependers = jobField.conditionOptionDependers||{}
                                    if(!jobField.conditionOptionDependers[job.Field[f].FieldOption[o].DispatchJobFieldOptionID]){
                                        jobField.conditionOptionDependers[job.Field[f].FieldOption[o].DispatchJobFieldOptionID] = {
                                            field: job.Field[f],
                                            option: job.Field[f].FieldOption[o]
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        var _parseDependencies = function(job, dispatchField){
            var deps = dispatchField.DefaultValue.match(/{{[^}]+}}/ig)
            if(deps){
                var fields = {}
                for(var i=0; i<deps.length; ++i){
                    var lookup = deps[i].substr(2, deps[i].length-4)
                    var lookupLower = lookup.toLowerCase()
                    var name = lookup.split('.')[0].trim()
                    var nameLower = name.toLowerCase()
                    if(['workdate','address'].includes(nameLower)){
                        if(!fields[lookupLower]){
                            job.locationDependers = job.locationDependers||{}
                            job.locationDependers[nameLower] = job.locationDependers[nameLower]||[]
                            if(!job.locationDependers[nameLower].includes(dispatchField))job.locationDependers[nameLower].push(dispatchField);
                            fields[lookupLower] = name
                        }
                    }else{
                        var found = false;
                        for(var f=0; f<job.DispatchField.length; ++f){
                            console.log(job.DispatchField[f].Name.toLowerCase(),nameLower,dispatchField)
                            if(job.DispatchField[f].Name.toLowerCase() == nameLower && job.DispatchField[f] !== dispatchField){
                                job.DispatchField[f].dependers = job.DispatchField[f].dependers||[]
                                if(!job.DispatchField[f].dependers.includes(dispatchField))job.DispatchField[f].dependers.push(dispatchField);
                                fields[lookupLower] = job.DispatchField[f]
                                found = true;
                                break;
                            }
                        }
                        if(found) continue;
                        for(var f=0; f<job.Field.length; ++f){
                            if(job.Field[f].Name.toLowerCase() == nameLower){
                                job.Field[f].dependers = job.Field[f].dependers||[]
                                if(!job.Field[f].dependers.includes(dispatchField))job.Field[f].dependers.push(dispatchField);
                                fields[lookupLower] = job.Field[f]
                                break;
                            }
                        }
                    }
                }
                var keys = Object.keys(fields)
                if(keys.length){
                    dispatchField.updateValue = function(location){
                        var value = dispatchField.DefaultValue;
                        for(var i=0; i<keys.length; ++i){
                            if(typeof fields[keys[i]] == 'string'){
                                var locationValue = location[fields[keys[i]]]
                                if(locationValue.setDate){ locationValue = Utils.date(locationValue, 'MM/dd/yyyy') }
                                if(typeof locationValue == 'boolean'){ locationValue = locationValue?'Y':'N' }
                                value = value.replace(new RegExp('{{'+keys[i]+'}}', 'ig'), locationValue)
                            }else{
                                if(typeof fields[keys[i]].IsAddressFlag != 'undefined'){
                                    var locationFieldValue = location[fields[keys[i]].key];
                                    value = value.replace(new RegExp('{{'+keys[i]+'}}', 'ig'), locationFieldValue)
                                }else{
                                    var locationField = location.LocationField.find(function(lf){ return lf.DispatchJobFieldID == fields[keys[i]].DispatchJobFieldID })
                                    if(locationField){
                                        var fieldValue = locationField.FieldValue;
                                        if(typeof fieldValue == 'boolean'){ fieldValue = fieldValue?'Y':'N' }
                                        if(['RADIO','DROP'].includes(fields[keys[i]].TypeCode) && locationField.option){
                                            var vals = keys[i].split('.')
                                            fieldValue = vals[1] == 'display'?locationField.option.Display:locationField.option.FieldValue;
                                        }
                                        value = value.replace(new RegExp('{{'+keys[i]+'}}', 'ig'), fieldValue)
                                    }
                                }
                            }
                        }
                        location[dispatchField.key] = value;
                    }
                }
            }
        }

        var _transformNumber = function(str){
            var match;
            if(typeof str == 'number'){
                return str;
            }
            if(match = str.match(/-?\d+(\.\d*)?/)){
                return parseFloat(match[0])
            }
            return '';
        }
        var _transformBool = function(str){
            if(typeof str == 'boolean'){
                return str;
            }
            if(str == 'Y'){
                return true
            }
            if(str == 'N'){
                return false
            }
            return ''
        }
        var _transformDropdown = function(str, field, returnObject){
            var option = field.FieldOption.find(function(option){
                return str==null?option.DefaultFlag:(option.FieldValue==str||option.Display==str)
            });
            if(option){
                return returnObject?option:option.FieldValue;
            }
            return ''
        }
        var _defaultValueTransforms = {
            NUM: _transformNumber,
            DOL: _transformNumber,
            CHECK: _transformBool,
            DROP: _transformDropdown,
            RADIO: _transformDropdown
        }
        var _defaultAddress = {
            key: 'Address',
            class: 'address',
            VisibleFlag: true,
            Name: 'Address',
            EditableFlag: true,
            DefaultValue: '',
            SyncFlag: true,
            IsAddressFlag: true,
            FilterFlag: false
        }
        var _cleanFields = function(job){
            /* Parent dispatch fields*/
            job.DispatchField = Utils.enforceArray(job.DispatchField)
            job.dispatchFields = {}
            var foundAddress = false;
            for(var f=job.DispatchField.length-1; f>=0; --f){
                var key = 'Location'+job.DispatchField[f].ColumnName
                if(job.DispatchField[f].ColumnName == 'Address'){
                    foundAddress = true
                    job.DispatchField[f].class = 'address'
                    key = 'Address'
                }
                if(job.DispatchField[f].IsAddressFlag){
                    job.DispatchField[f].DefaultValue = '{{Address}}'
                }
                job.dispatchFields[key] = job.DispatchField[f]
                job.DispatchField[f].key = key
                _parseDependencies(job, job.DispatchField[f])
                if(job.DispatchField[f].EditableFlag&&job.DispatchField[f].VisibleFlag){
                    job.canAddLocations = true;
                }
            }
            if(!foundAddress){
                job.DispatchField.unshift(_defaultAddress)
                job.dispatchFields['Address'] = _defaultAddress
                job.canAddLocations = true;
            }

            /* Regular fields */
            //Set up a field map so we can lookup by field ID
            job.Field = Utils.enforceArray(job.Field)
            job.fields = {}
            for(var f=0; f<job.Field.length; ++f){
                if(job.Field[f].Condition) job.Field[f].Condition = Utils.enforceArray(job.Field[f].Condition);
                var isMulti = ['DROP','RADIO'].includes(job.Field[f].TypeCode);
                //Clean options array
                if(isMulti){
                    job.Field[f].FieldOption = Utils.enforceArray(job.Field[f].FieldOption);
                    for(var o=0; o<job.Field[f].FieldOption.length; ++o){
                        if(job.Field[f].FieldOption[o].OptionCondition){
                            job.Field[f].FieldOption[o].OptionCondition = Utils.enforceArray(job.Field[f].FieldOption[o].OptionCondition);
                        }
                    }
                }
                if(['NUM','DOL'].includes(job.Field[f].TypeCode)){
                    job.Field[f].round = parseInt(job.Field[f].Decimals) > 0 ? Math.pow(10, parseInt(job.Field[f].Decimals)): 1
                }
                job.fields[job.Field[f].DispatchJobFieldID] = job.Field[f];
                //Set up default values
                if(_defaultValueTransforms[job.Field[f].TypeCode]){
                    job.Field[f].DefaultValue = _defaultValueTransforms[job.Field[f].TypeCode](isMulti?null:job.Field[f].DefaultValue, job.Field[f]);
                }
            }
            for(var f=0; f<job.Field.length; ++f){
                _parseConditionDependencies(job, job.Field[f]);
            }
        }

        var _cleanJob = function(job, scope){
            scope.job = job
            scope.job.canComplete = !!scope.job.ActiveJobDesignID
            if(scope.job.CompleteDate){
                scope.job.CompleteDate = Utils.date(scope.job.CompleteDate)
                scope.job.CompleteDate.clearTime();
            }
            scope.job.DateFilter = (typeof scope.job.DateFilter!=='undefined')?scope.job.DateFilter:scope.job.CompleteDate
            scope.job.datesLoaded = Utils.enforceArray(scope.job.datesLoaded||[])
            scope.job.Tag = Utils.enforceArray(scope.job.Tag||[])
            if(scope.job.DateFilter){
                scope.job.DateFilter = Utils.date(scope.job.DateFilter)
                scope.job.DateFilter.clearTime();
                scope.job.currentDateFilter = new Date(scope.job.DateFilter.getTime())
            }else{
                scope.job.DateFilter = ''
            }
            if(!scope.job.initDate){
                scope.job.initDate = scope.job.initDate||scope.job.DateFilter||(new Date());
                var dateStr = Utils.date(scope.job.initDate, 'MM/dd/yyyy');
                scope.job.datesLoaded.push(dateStr);
            }
           
            
            _cleanFields(job)
            _cleanLocations(scope)
            _sortLocations(job)
            _processForemenData(scope, job)
            scope.original = Utils.copy(job);
            var callback = scope.checkCallback();
            _cleanDialog(job, true);
            return job;
        }

        var _includeLocations = function(scope, data){
            data.DispatchLocation = Utils.enforceArray(data.DispatchLocation)
            data.DispatchLocation = data.DispatchLocation.filter(function(l1){ return !scope.job.DispatchLocation.find(function(l2){ return l1.ID == l2.ID }) })
            var locations = _cleanLocations(scope, data)
            _sortLocations(scope.job)
            _processForemenData(scope, data)
            if(scope.job.uniqueLocationID){
                _checkUniqueErrorPostSearch(scope);
            }
            return locations;
        }

        var _processForemenData = function(scope, data){
            if(scope.job.DispatchWizardForemanFilterFlag && data.referenceData && data.referenceData.Foremen){
                data.referenceData.Foremen = Utils.enforceArray(data.referenceData.Foremen);
                for(var i=0; i<data.referenceData.Foremen.length; ++i){
                    if(!scope.foremanFilterOptions.find(function(o){ return o.ID == data.referenceData.Foremen[i].ID})){
                        console.log(data.referenceData.Foremen[i].ID, scope.job.person)
                        if(data.referenceData.Foremen[i].ID == StorageService.get('MyPersonID')){
                            data.referenceData.Foremen[i].bold = true;
                            if(!scope.job.hasChangedForemanFilter){
                                scope.job.foremanFilter = data.referenceData.Foremen[i]
                            }
                        }
                        scope.foremanFilterOptions.push(data.referenceData.Foremen[i]);
                    }
                }
                if(!scope.job.foremanFilter) scope.job.foremanFilter = scope.foremanFilterOptions[0];
                if(data.referenceData.Foremen.length>0)scope.job.hasForemenAssigned = true;
            }
        }

        var _populateDefaultValue = function(jobField, location, job){
            var locationField = location.LocationField.find(function(lf){ return lf.DispatchJobFieldID == jobField.DispatchJobFieldID })
            if(!locationField){
                locationField = {
                    DispatchJobFieldID: jobField.DispatchJobFieldID
               }
               location.LocationField.push(locationField);
            }
            if(typeof locationField.FieldValue == 'undefined' || locationField.FieldValue === ''){
                locationField.FieldValue = jobField.DefaultValue
                if(['DROP','RADIO'].includes(jobField.TypeCode) && locationField.FieldValue){
                    _assignOption(job, locationField)
                }
                _updateDependencyValues(job, jobField, location);
            }
            locationField.order = jobField.OrderNumber            
        }
        var _populateDefaultDispatchValue = function(dispatchField, location, job){
            var locationField = location[dispatchField.key];
            if(typeof locationField == 'undefined' || locationField === ''){
                location[dispatchField.key] = dispatchField.DefaultValue
                _updateDependencyValues(job, dispatchField, location);
            }         
        }
        var _populateDefaultValues = function(job, location){
            var keys = Object.keys(job.locationDependers)
            for(var f=0; f<job.DispatchField.length; ++f){
                if(!keys.find(function(key){ return job.locationDependers[key].find(function(dp){ return dp === job.DispatchField[f] }) })){
                    _populateDefaultDispatchValue(job.DispatchField[f], location, job);
                }
            }
            for(var f=0; f<job.Field.length; ++f){
                _populateDefaultValue(job.Field[f], location, job);
            }
        }
        var _addLocation = function(scope, options){
            options = options||{}
            var job = scope.job
            var sequence = _getNextSequence(job, -1)
            if(options.sequence){
                for(var i=0; i<scope.job.DispatchLocation.length; ++i){
                    if(options.sequence > sequence && scope.job.DispatchLocation[i].sequence <= options.sequence){
                        scope.job.DispatchLocation[i].sequence--;
                    }else if(options.sequence < sequence && scope.job.DispatchLocation[i].sequence >= options.sequence){
                        scope.job.DispatchLocation[i].sequence++;
                    }
                }
                sequence = options.sequence
            }
            var newLocation = {
                ID: _getTempID(job),
                WorkDate: new Date(),
                Address: '',
                LocationField: [],
                LocationComplete: false,
                LPhoto: [],
                sequence: sequence
            }
            if(scope.job.CompleteDate){
                newLocation.WorkDate = new Date(scope.job.CompleteDate.getTime())
            }
            newLocation.WorkDate.clearTime();
            _populateDefaultValues(job, newLocation);
            _evaluateAllConditions(job, newLocation);
            job.DispatchLocation.push(newLocation);
            if(!options.skipSort)_sortLocations(job)
            return newLocation;
        }

        var _copyLocation = function(scope, location){
            var job = scope.job;
            var newLocation = _addLocation(scope, {sequence: location.sequence-1});
            newLocation.WorkDate = new Date(location.WorkDate.getTime())
            for(var i=0; i<job.DispatchField.length; ++i){
                if(job.DispatchField[i].ClonesFlag){
                    newLocation[job.DispatchField[i].key] = location[job.DispatchField[i].key];
                    _updateDependencyValues(job, job.DispatchField[i], location);
                }
            }
            for(var i=0; i<location.LocationField.length; ++i){
                var index = newLocation.LocationField.findIndex(function(lf){ return lf.DispatchJobFieldID == location.LocationField[i].DispatchJobFieldID })
                if(index > -1){
                    if(scope.job.fields[newLocation.LocationField[index].DispatchJobFieldID].ClonesFlag){
                        newLocation.LocationField[index].FieldValue = location.LocationField[i].FieldValue;
                        if(['DROP','RADIO'].includes(scope.job.fields[newLocation.LocationField[index].DispatchJobFieldID].TypeCode) && newLocation.LocationField[index].FieldValue){
                            _assignOption(scope.job, newLocation.LocationField[index])
                        }
                        _updateDependencyValues(job, scope.job.fields[newLocation.LocationField[index].DispatchJobFieldID], location);
                    }
                }
            }
            return newLocation;
        }

        var _deleteLocation = function(scope, location, force){
            var job = scope.job, index, id = location;
            if(typeof id == "object"){
                id = location.ID
            }
            if((index = job.DispatchLocation.findIndex(function(loc){ var is = loc.ID == id; if(is && id==location)location = loc; return is; })) > -1){
                if(Utils.isTempId(location.ID) || force){
                    //If temp, just remove immediately
                    //_changes(scope.job, true);
                    //if(force){
                    //    job.DispatchLocation[index].deleted = true
                    //}else{
                        job.DispatchLocation.splice(index, 1)
                    //}
                    return index;
                }else{
                    //Otherwise we need to make sure we can remove it from the design
                    scope.loadingDialog()
                    _load(scope, 'dispatch/deleteLocation', 'POST', {job:job.JobID, dispatchJob:job.ID, dispatchLocation: location.ID}, 'locationDeleteCallback')
                }
            }
            return -1
        }

        var _getLocationAddressObj = function(location){
            var obj = {}
            if(location.LocationID) obj.ID = location.LocationID;
            if(location.LocationLongitude) obj.Lon = location.LocationLongitude.toString();
            if(location.LocationLatitude) obj.Lat = location.LocationLatitude.toString();
            if(location.LocationAddress) obj.Add = location.LocationAddress;
            if(location.LocationCity) obj.City = location.LocationCity;
            if(location.LocationStateCode) obj.State = location.LocationStateCode;
            if(location.LocationPostalCode) obj.PC = location.LocationPostalCode;
            if(location.LocationInexactAddressMatch) obj.Part = location.LocationInexactAddressMatch;
            return obj;
        }

        var _validateLocation = function(job, location){
            var returnValue = {ok: true,fields:[]}
            if(!location.WorkDate || !location.WorkDate.toString().trim()){
                returnValue.ok = false;
                returnValue.fields.push('date');
            }
            for(var f=0; f<job.DispatchField.length; ++f){
                var dispatchField = job.DispatchField[f];
                var fieldValue = location[dispatchField.key];
                if(dispatchField.Required && dispatchField.VisibleFlag && (!fieldValue && fieldValue !== 0)){
                   returnValue.ok = false;
                   returnValue.fields.push('dispatch-field-'+dispatchField.DispatchWizardFieldID.toString());
                }
            }
            for(var f=0; f<location.LocationField.length; ++f){
                var jobField = job.fields[location.LocationField[f].DispatchJobFieldID]
                var fieldValue = location.LocationField[f].FieldValue
                if(jobField && jobField.VisibleFlag && jobField.RequiredFlag && (!fieldValue && fieldValue !== 0 && (jobField.TypeCode != 'CHECK' || fieldValue === ''))){
                   returnValue.ok = false;
                   returnValue.fields.push('field-'+location.LocationField[f].DispatchJobFieldID.toString());
                }
            }
            if(!returnValue.ok){
                returnValue.location = location
            }
            return returnValue;
        }
        var _validate = function(job){
            var returnValue = {ok: true, locations:[]}
            for(var l=0; l<job.DispatchLocation.length; ++l){
                var location = job.DispatchLocation[l]
                if(!location.LocationComplete){
                    var locationResult = _validateLocation(job, location)
                    if(!locationResult.ok){
                        returnValue.locations.push(locationResult);
                        returnValue.ok = false;
                    }
                }
            }
            return returnValue;
        }

        var _evalCondition = function(jobField, locationField, operator, value){
            var fieldValue = locationField.FieldValue;
            if(typeof fieldValue == 'boolean'){ fieldValue = fieldValue?'Y':'N' }
            if((jobField.TypeCode == 'RADIO' || jobField.TypeCode == 'DROP') && locationField.option){
                fieldValue = locationField.option.FieldValue;
            }else if(jobField.TypeCode == 'NUM' || jobField.TypeCode == 'DOL'){
                fieldValue = _transformNumber(fieldValue);
                if(operator != 'IN'){
                    value = _transformNumber(value);
                }
            }
            switch(operator){
                case 'EQ':
                    return fieldValue == value;
                case 'NE':
                    return fieldValue != value;
                case 'LT':
                    return fieldValue < value;
                case 'GT':
                    return fieldValue > value;
                case 'IN':
                    var list = value.split(',');
                    if(jobField.TypeCode == 'NUM' || jobField.TypeCode == 'DOL'){
                        for(var i=0; i<list.length; ++i){
                            if(_transformNumber(list[i]) == fieldValue){
                                return true;
                            }
                        }
                        return false;
                    }
                    return list.includes(fieldValue);
            }
        }

        var _evaluateConditions = function(job, jobField, location){
            var thisLocationField = location.LocationField.find(function(lf){ return lf.DispatchJobFieldID==jobField.DispatchJobFieldID })
            if(!thisLocationField) return;
            for(var c=0; c<jobField.Condition.length; ++c){
                var condition = jobField.Condition[c];
                var locationField = location.LocationField.find(function(lf){ return lf.DispatchJobFieldID==condition.CompareFieldID });
                if(!_evalCondition(job.fields[condition.CompareFieldID], locationField, condition.OperatorCode, condition.CompareValue)){
                    var disabled = thisLocationField.disabled;
                    thisLocationField.disabled = true;
                    if(!disabled){
                        thisLocationField.FieldValue = '';
                        _populateDefaultValue(jobField, location, job);
                    }
                    return; 
                }
            }
            delete thisLocationField.disabled;
        }

        var _evaluateOptionConditions = function(job, jobField, option, location){
            var thisLocationField = location.LocationField.find(function(lf){ return lf.DispatchJobFieldID==jobField.DispatchJobFieldID })
            if(!thisLocationField) return;
            for(var c=0; c<option.OptionCondition.length; ++c){
                var condition = option.OptionCondition[c];
                var locationField = location.LocationField.find(function(lf){ return lf.DispatchJobFieldID==condition.CompareFieldID });
                if(!_evalCondition(job.fields[condition.CompareFieldID], locationField, condition.OperatorCode, condition.CompareValue)){
                    thisLocationField.disabledOptions = thisLocationField.disabledOptions||{};
                    var disabled = thisLocationField.disabledOptions[option.DispatchJobFieldOptionID];
                    thisLocationField.disabledOptions[option.DispatchJobFieldOptionID] = true;
                    if(!disabled && option.FieldValue==thisLocationField.FieldValue){
                        thisLocationField.FieldValue = '';
                        delete thisLocationField.option;
                        _populateDefaultValue(jobField, location, job);
                    }
                    return; 
                }
            }
            if(thisLocationField.disabledOptions){
                delete thisLocationField.disabledOptions[option.DispatchJobFieldOptionID];
            }
        }

        var _evaluateAllConditions = function(job, location){
            for(var f=0; f<job.Field.length; ++f){
                if(job.Field[f].Condition){
                    _evaluateConditions(job, job.Field[f], location);
                }
                if(job.Field[f].TypeCode == 'DROP' || job.Field[f].TypeCode == 'RADIO'){
                    for(var o=0; o<job.Field[f].FieldOption.length; ++o){
                        if(job.Field[f].FieldOption[o].OptionCondition){
                            _evaluateOptionConditions(job, job.Field[f], job.Field[f].FieldOption[o], location);
                        }
                    }
                }
            }
        }

        var _updateDependencyValues = function(job, field, location){
            if(typeof field == 'string' || field.ColumnName == 'Address'){
                var fieldLower = (field.ColumnName||field).toLowerCase()
                if(job.locationDependers && job.locationDependers[fieldLower]){
                    for(var d=0; d<job.locationDependers[fieldLower].length; ++d){
                        var depender = job.locationDependers[fieldLower][d];
                        if(depender.updateValue){
                            depender.updateValue(location)
                        }
                    }
                }
            }
            if(typeof field == 'object'){
                if(typeof field.IsAddressFlag != 'undefined'){
                    if(field.dependers){
                        for(var d=0; d<field.dependers.length; ++d){
                            var depender = field.dependers[d]
                            if(depender.updateValue && (Utils.isTempId(location.ID) || depender.SyncFlag)){
                                field.dependers[d].updateValue(location);
                            }
                        }
                    }
                }else{
                    if(job.fields[field.DispatchJobFieldID].dependers){
                        for(var d=0; d<job.fields[field.DispatchJobFieldID].dependers.length; ++d){
                            var depender = job.fields[field.DispatchJobFieldID].dependers[d]
                            if(depender.updateValue && (Utils.isTempId(location.ID) || depender.SyncFlag)){
                                job.fields[field.DispatchJobFieldID].dependers[d].updateValue(location);
                            }
                        }
                    }
                    if(job.fields[field.DispatchJobFieldID].conditionDependers){
                        for(var d=0; d<job.fields[field.DispatchJobFieldID].conditionDependers.length; ++d){
                            _evaluateConditions(job, job.fields[field.DispatchJobFieldID].conditionDependers[d], location);
                        }
                    }
                    if(job.fields[field.DispatchJobFieldID].conditionOptionDependers){
                        var keys = Object.keys(job.fields[field.DispatchJobFieldID].conditionOptionDependers);
                        for(var d=0; d<keys.length; ++d){
                            var depender = job.fields[field.DispatchJobFieldID].conditionOptionDependers[keys[d]];
                           _evaluateOptionConditions(job, depender.field, depender.option, location);
                        }
                    }
                }
            }
        }

        var _showExistingLocation = function(scope, location, field){
            location.forceVisible = true;
            scope.executefilters();
            $timeout(function(){
                $anchorScroll('location-'+location.ID);
                var row = document.getElementById('location-'+location.ID);
                var f = row.querySelector('td.dispatch-field-'+field.DispatchWizardFieldID+' input')||row.querySelector('td.dispatch-field-'+field.DispatchWizardFieldID+' contenteditable');
                row.className += ' table-active-row';
                if(f)f.focus();
                scope.flash('location-'+location.ID);
            })
        }
        var _uniqueFieldError = function(scope, field, newLocation, existingLocation){
            var message = 'A location already exists where '+field.Name+' is <b>'+existingLocation[field.key]+'</b>.<br><br>Would you like to continue and create a duplicate?';
            _showHTMLMessage(message, 'Duplicate Location', [{
                label: 'Duplicate',
                callback: function(){
                    _changes(scope.job, true);
                    _updateDependencyValues(scope.job, field, newLocation)
                    newLocation.touched = true;
                }
            },{
                label: 'Cancel',
                callback: function(){
                    _deleteLocation(scope, newLocation);
                    _showExistingLocation(scope, existingLocation, field);
                }
            }]);
        }
        var _uniqueField = function(scope, field, location){
            var job = scope.job;
            if(field && typeof field == 'object' && field.UniqueFlag){
                var value = (location[field.key]||'').toLowerCase();
                var existing = job.DispatchLocation.find(function(loc){ return !loc.deleted && loc.ID != location.ID && (loc[field.key]||'').toLowerCase()==value })
                if(existing){
                    _uniqueFieldError(scope, field, location, existing);
                }else{
                    job.uniqueLocationID = location.ID
                    job.uniqueFieldID = field.DispatchWizardFieldID
                    location.forceVisible = true;
                    var obj = scope.getPostObj('search',{uniqueValue:location[field.key],uniqueField:field.DispatchWizardFieldID})
                    obj.timeDate = obj.date;
                    delete obj.date;
                    _load(scope, 'dispatch/search', 'GET', obj, 'searchCallback')
                    scope.loadingDialog()
                }
                return true;
            }
        }
        var _checkUniqueErrorPostSearch = function(scope){
            if(!scope.job.uniqueLocationID) return;
            var job = scope.job;
            var field = job.DispatchField.find(function(df){ return df.DispatchWizardFieldID == job.uniqueFieldID})
            if(field){
                var location = job.DispatchLocation.find(function(loc){ return loc.ID == job.uniqueLocationID })
                if(location){
                    var existing = job.DispatchLocation.find(function(loc){ return !loc.deleted && loc.ID != location.ID && loc[field.key]==location[field.key] })
                    if(existing){
                        _uniqueFieldError(scope, field, location, existing);
                    }else{
                        $timeout(function(){
                            _changes(job, true);
                        }, 100)
                        _updateDependencyValues(job, field, location)
                        location.touched = true;
                    }
                }
            }
            delete job.uniqueLocationID;
            delete job.uniqueFieldID;
        }

        var _sortLocations = function(job, currentLocation){
            var index = currentLocation?job.DispatchLocation.findIndex(function(loc){ return loc === currentLocation}):-1;
            job.DispatchLocation.sort(function(a,b){
                if(a.LocationComplete != b.LocationComplete){
                     return a.LocationComplete?1:-1;
                }
                var t1 = a.WorkDate?a.WorkDate.getTime():0,
                    t2 = b.WorkDate?b.WorkDate.getTime():0;
                if(t1 != t2){
                    return t1 - t2;
                }
                return a.sequence - b.sequence;
            })
            if(currentLocation){
                return job.DispatchLocation.findIndex(function(loc){ return loc === currentLocation}) != index;
            }
            return false;
        }

        var _changeDateFilter = function(scope){
            scope.job.DateFilter = scope.job.currentDateFilter?new Date(scope.job.currentDateFilter.getTime()):null
            var dateStr = Utils.date(scope.job.DateFilter, 'MM/dd/yyyy')
            if(!scope.job.DispatchWizardLocationSearchFlag && !scope.job.datesLoaded.includes(dateStr) && !scope.job.datesLoaded.includes('')){
                scope.job.datesLoaded.push(dateStr)
                _load(scope, 'dispatch/search', 'GET', {job: scope.job.JobID, dispatch:scope.job.ID, date:dateStr}, 'searchCallback')
                scope.loadingDialog()
            }
        }

        var _save = function(scope, endpoint, extraData, callbackName, loadingText){
            endpoint = endpoint||'dispatch';
            scope.loading = true;
            scope.loadingDialog(loadingText);

            var done = function(resp){
                if(resp.replacements && resp.replacements.length){
                    Utils.replace(scope.job, resp.replacements);
                }
                scope.original = Utils.copy(scope.job)
                scope.loading = false;
                if(scope.callbacks[callbackName]){
                    if(typeof scope.callbacks[callbackName] == "function"){
                        scope.callbacks[callbackName].apply(null, [resp, extraData]);
                    }else if(typeof scope.callbacks[callbackName].fn == "function"){
                        scope.callbacks[callbackName].fn.apply(null, [resp, extraData]);
                    }
                }
            }

            if(extraData.action == 'save'){
                _changes(scope.job, false);
            }

            if(callbackName){
                scope.job.callback = callbackName;
                scope.job.callbackData = '<!-- callback_data -->';
            }
            var job = Utils.copy(scope.job);      
            try{
                var data = SaveService.getPostData(job, scope.original,{address:_getLocationAddressObj});
                _stripJob(job)
            }catch(e){alert(e.message)}

            extraData = extraData||{}
            extraData.session = scope.job.Session;
            if(extraData){
                data += '[!-- @@@@@ --]'+(typeof extraData=='string'?extraData:JSON.stringify(extraData));
            }
            var route = {action:extraData.action};
            $timeout(function(){
                ApiService.post(endpoint, data, null, null, null, route).then(done,done);
            });
            return true;
        }
        var _load = function(scope, endpoint, method, extraData, callbackName){
            var done = function(){
                scope.loading = false;
                if(scope.callbacks[callbackName]){
                    scope.callbacks[callbackName].apply(null, arguments);
                }
            }
            endpoint = endpoint||'dispatch/load';
            extraData.action = endpoint.split('dispatch/').pop();
            extraData.session = scope.job.Session;
            scope.loading = true;
            $timeout(function(){
                if(method == 'GET'){
                    ApiService.get('dispatch', extraData).then(done,done);
                }else{
                    ApiService.post('dispatch', extraData).then(done,done);
                }
            });
        }

        var _getNextSequence = function(job, dir){
            if(!job.lastSequence){
                job.lastSequence = 0;
                job.firstSequence = 0;
            }
            if(dir < 0){
                --job.firstSequence;
                return job.firstSequence;
            }
            ++job.lastSequence;
            return job.lastSequence;
        }
        var _getTempID = function(job) {
            if(!job.lastTemp) job.lastTemp = 0;
            ++job.lastTemp;
            return '__temp'+job.lastTemp.toString(16)+'__';
        };
        var _cleanID = function(id){
            if(typeof id == 'string'){
                if(id.length && id.substr(0,6) != '__temp')
                    return parseInt(id);
            }
            return id;
        }

        var _spMappings = {

        }

        if(!Object.entries)Object.entries = function(obj) { var arr=Object.keys(obj); for(var i=0; i<arr.length; ++i){arr[i]=[arr[i],obj[arr[i]]]} return arr; }
        function findKey(obj, fn) {
          var found = Object.entries(obj).find(fn);
          if(found) return found[0];
          return null;
        }

        return {
            setDialog: _setDialog,
            cleanJob: _cleanJob,
            getTempID: _getTempID,
            cleanID: _cleanID,
            transform: _defaultValueTransforms,

            addLocation: _addLocation,
            copyLocation: _copyLocation,
            deleteLocation: _deleteLocation,
            validate: _validate,
            validateLocation: _validateLocation,
            sortLocations: _sortLocations,
            includeLocations: _includeLocations,
            uniqueField: _uniqueField,
            checkUniqueErrorPostSearch: _checkUniqueErrorPostSearch,
            getLocationAddressObj: _getLocationAddressObj,

            updateDependencyValues: _updateDependencyValues,
            changeDatefilter: _changeDateFilter,

            changes: _changes,
            save: _save,
            load: _load,
        };

    }]);


})();

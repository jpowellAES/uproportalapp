(function(){
    'use strict';

    angular.module('app').controller('EditReceiptController', ['$scope', '$timeout', 'ApiService', 'StorageService', 'Vendors', 'Utils', 'Smartpick', 'UploadDialog', 'ImgOrPdf', function($scope, $timeout, ApiService, StorageService, Vendors, Utils, Smartpick, UploadDialog, ImgOrPdf) {
        $scope.working = true;
        $scope.job = $scope.$parent.current.job;
        $scope.receipt = {};
        $scope.title = 'New Receipt'
        $scope.editing = true;
        $scope.loading = false

        $scope.data = {
            JobStatus: Smartpick.getFiltersData('JobStatus')
        }

        var setReceipt = function(){
            changedType = true;
            newInitialized = false;
            if(['PE','RB'].includes($scope.receipt.StatusCode)){
                $scope.receipt.billable = true;
            }
            $scope.receipt = Utils.copy($scope.$parent.current.receipt);
            $scope.receipt.ReceiptPhoto = Utils.enforceArray($scope.receipt.ReceiptPhoto);
            $scope.receipt.InvoiceStatus = {Code: $scope.receipt.StatusCode, Meaning: $scope.receipt.StatusMeaning }
            checkBillable($scope.receipt)
            $timeout(function(){angular.element(document.getElementById('receiptAmount')).triggerHandler('blur')});
        }
        var newInitialized = false,
            changedType = false;
        var checkBillable = function(receipt){
            var jobStatus = ($scope.job?$scope.job.StatusCode:0)||(receipt?receipt.JobStatusCode:0)
            if(jobStatus && ['FB','CP'].includes(jobStatus)){
                receipt.billable = false;
            }else{
                receipt.billable = true;
            }
        }
        var initNewReceipt = function(){
            if(newInitialized) return;
            $scope.receipt.ReceiptPhoto = []
            $scope.receipt.StatusCode = 'PE'
            $scope.receipt.InvoiceDate = new Date()
            $scope.mapSP($scope.receipt, 'InvoiceStatus', 'StatusCode', 'Code')
            if($scope.job){
                $scope.receipt.JobID = $scope.job.ID
            }
            checkBillable($scope.receipt)
            newInitialized = true
            changedType = false;
        }

        if($scope.$parent.current.receipt){
            setReceipt()
            $scope.title = $scope.receipt.InvoiceNumber;
            $scope.editing = false;
        }

        $scope.mapSP = function(receipt, sp, dataField, spField, fromSP){
            if(fromSP){
                receipt[dataField] = receipt[sp][spField]
            }else{
                if(!$scope.data[sp]) return;
                receipt[sp] = $scope.data[sp].find(function(v){ return v[spField] == receipt[dataField] })
            }
        }
        var cleanReceipt = function(receipt){
            receipt.InvoiceDate = Utils.date(receipt.InvoiceDate)
            receipt.ReceiptPhoto = Utils.enforceArray(receipt.ReceiptPhoto)
            if(['PE','RB'].includes(receipt.StatusCode)){
                receipt.billable = true;
                checkBillable(receipt)
            }
            $scope.mapSP(receipt, 'InvoiceType', 'InvoiceTypeCode', 'Code')
            $scope.mapSP(receipt, 'InvoiceStatus', 'StatusCode', 'Code')
        }
        var result = function(resp){
            if(resp){
                if(resp.data && !resp.data.success && resp.data.message){
                    Utils.notification.alert(resp.data.message, {title: 'Error'});
                    mainNav.popPage();
                }else{
                    if(resp.JobStatus){
                        $scope.data.JobStatus = Utils.enforceArray(resp.JobStatus);
                    }
                    if(resp.InvoiceType){
                        $scope.data.InvoiceType = Utils.enforceArray(resp.InvoiceType);
                    }
                    if(resp.InvoiceStatus){
                        $scope.data.InvoiceStatus = Utils.enforceArray(resp.InvoiceStatus);
                    }
                    if(resp.Vendor){
                        $scope.data.Vendor = Utils.enforceArray(resp.Vendor);
                    }
                    if(resp.VendorInvoice){
                        cleanReceipt(resp.VendorInvoice)
                        $scope.$parent.current.replaceReceipt(resp.VendorInvoice, newInitialized);
                        setReceipt()
                    }else{
                        initNewReceipt()
                    }
                }
            }
            $scope.working = false;
            $scope.loading = false;
        }
        var load = function(){
            var obj = {
                route: 'init'
            };
            if($scope.receipt){
                obj.receipt = $scope.receipt.ID;
            }
            if(!$scope.data.JobStatus){
                obj.filters = true;
            }
            ApiService.get('receipts', obj).then(result, result);
        };

        var defaultType = function(){
            if(changedType) return;
            if($scope.receipt.VendorOrgTypeCode == 'SC'){
                $scope.receipt.InvoiceTypeCode = 'SC';
            }else{
                if($scope.receipt.VendorType == 'HT'){
                    $scope.receipt.InvoiceTypeCode = 'HT';
                }else{
                    $scope.receipt.InvoiceTypeCode = '';
                }
            }
            $scope.receipt.InvoiceType = $scope.data.InvoiceType.find(function(t){ return t.Code == $scope.receipt.InvoiceTypeCode})
        }
        $scope.changeType = function(){
            changedType = true;
            $scope.mapSP($scope.receipt, 'InvoiceType', 'InvoiceTypeCode', 'Code', true)
        }

        var selectJob = function(item){
            $scope.job = item;
            $scope.receipt.JobID = item.ID
            checkBillable($scope.receipt)
        }
        $scope.findJob = function(){
            if($scope.job && Smartpick.ok()){
                Smartpick.show(null, selectJob);
            }else{
                Smartpick.show({
                    endpoint: 'receiptsJobSearch',
                    itemView: 'Job',
                    itemTemplate: 'job.html',
                    title: 'Find Job',
                    filters: [
                        {key: 'Status', from: 'Code', to:'StatusCode', display:'Meaning'},
                        {key: 'Owner', from: 'OrganizationID', to:'OwnerID', display:'Name'}
                    ],
                    filtersData: {
                        Status: $scope.data.JobStatus
                    },
                    defaultFilters: {
                        Status: {"Code":"PR","Meaning":"In Process"}
                    },
                    minSearchLength: 2
                }, selectJob);
            }
        }
        $scope.findVendor = function(){
            Vendors.show({items:$scope.data.Vendor}, function(item){
                $scope.receipt.VendorID = item.OrganizationID;
                $scope.receipt.VendorName = item.ReportName;
                $scope.receipt.VendorOrgTypeCode = item.OrgTypeCode;
                $scope.receipt.VendorType = item.Type||'';
                defaultType();
            })
        }

        $scope.addPhoto = function(existingIndex){
            var existing = typeof existingIndex != 'undefined';
            var options = {save: function(data){
                    var newObj = {
                        thumb: data.photoUrl,
                        Description: data.description
                    }
                    if(existing){
                        $scope.receipt.ReceiptPhoto[existingIndex] = newObj
                    }else{
                        $scope.receipt.ReceiptPhoto.push(newObj)
                    }
                    UploadDialog.hide();
                },
                title: 'Add Photo',
                saveLabel: 'Ok'
            }
            if(existing){
                options.photoUrl = $scope.receipt.ReceiptPhoto[existingIndex].thumb
                options.description = $scope.receipt.ReceiptPhoto[existingIndex].Description
            }
            UploadDialog.show(options);
        }

        $scope.removePhoto = function(index){
            $scope.receipt.ReceiptPhoto.splice(index, 1);
        }
        $scope.openPhoto = function(index){
            if($scope.receipt.ReceiptPhoto[index].thumb){
                $scope.addPhoto(index)
                return;
            }
            var path =  '/upload/' + $scope.receipt.ReceiptPhoto[index].String;
            $scope.loading = true;
            ImgOrPdf.loadWithCaption(path, {Key:$scope.receipt.ReceiptPhoto[index].String,Description:$scope.receipt.ReceiptPhoto[index].Description} , {editable: true, callback: function(upl){
                $scope.receipt.ReceiptPhoto[index].Description = upl.Description;
            }}).finally(function(){
                $scope.loading = false;
            });
        }


        $scope.edit = function(){
            $scope.editing = true;
            $scope.receipt.StatusCode = 'PE';
            $scope.mapSP($scope.receipt, 'InvoiceStatus', 'StatusCode', 'Code')
        }

        $scope.cancelEditing = function(){
            $scope.editing = false;
            setReceipt()
            if($scope.job.ID != $scope.receipt.JobID){
                $scope.job = {
                    ID: $scope.receipt.JobID,
                    NumberChar: $scope.receipt.JobJobNumberChar,
                    WorkgroupJobNumber: $scope.receipt.JobWorkgroupJobNumber,
                    StatusCode: $scope.receipt.JobStatusCode
                }
            }
        }

        var _saveResult = function(resp){
            result(resp);
            changedType = true;
            if(resp && resp.VendorInvoice){
                var receipt = resp.VendorInvoice;
                $scope.editing = false;
                $scope.data.Vendor = Utils.enforceArray($scope.data.Vendor);
                $scope.title = receipt.InvoiceNumber;
                var vendor = $scope.data.Vendor.find(function(v){ return v.OrganizationID == receipt.VendorID});
                if(!vendor){
                    $scope.data.Vendor.push({
                        OrganizationID: receipt.VendorID,
                        Name: receipt.VendorName,
                        ReportName: receipt.VendorReportName,
                        OrgTypeCode: receipt.VendorOrgTypeCode,
                        OrgTypeMeaning: receipt.VendorOrgTypeMeaning,
                        Type: receipt.VendorType,
                        TypeMeaning: receipt.VendorTypeMeaning
                    })
                }
            }
        }
        var _getPostData = function(done){
            var obj = {
                route: 'save',
                Receipt: {
                    InvoiceDate: Utils.date(Utils.date($scope.receipt.InvoiceDate), 'MM/dd/yyyy'),
                    Description: $scope.receipt.Description,
                    InvoiceNumber: $scope.receipt.InvoiceNumber,
                    InvoiceTypeCode: $scope.receipt.InvoiceTypeCode,
                    JobID: $scope.receipt.JobID,
                    BaseAmount: parseFloat($scope.receipt.BaseAmount)||0,
                    StatusCode: $scope.receipt.StatusCode,
                    VendorID: $scope.receipt.VendorID,
                    VendorName: $scope.receipt.VendorName,
                    VendorTypeCode: $scope.receipt.VendorOrgTypeCode,
                    VendorType: $scope.receipt.VendorType,
                    Reimbursable: $scope.receipt.ReimbursableFlag,
                    Photo: []
                }
            }
            if($scope.receipt.ID){
                obj.Receipt.ID = $scope.receipt.ID;
            }
            var files = {}, count = 0
            for(var i=0; i<$scope.receipt.ReceiptPhoto.length; ++i){
                if($scope.receipt.ReceiptPhoto[i].thumb){
                    files['photo'+count] = $scope.receipt.ReceiptPhoto[i].thumb;
                    obj.Receipt.Photo[count] = {Description: $scope.receipt.ReceiptPhoto[i].Description, Name: $scope.receipt.ReceiptPhoto[i].thumb.split('/').pop().split('?').shift()};
                    ++count;
                }
            }
            if(count){
                ApiService.getFormData(obj, files, {base64:true}).then(function(formdata){
                    done(obj, formdata);
                });
            }else{
                done(obj);
            }
        }

        var errorField = function(item){
            var verb = item.select?'select':'enter', field = item.prompt?item.prompt:item.field;
            Utils.notification.alert('You must '+verb+' a value for <br><b>'+field+'</b>.', {title:'Error', callback:function(){
                if(item.id){
                    $timeout(function(){
                        document.getElementById(item.id).focus();
                    })
                }
            }});            
        }
        var validateLocally = function(){
            var requireNotNull = [
            {
                field:'JobID',
                prompt:'Job',
                select: true
            },{
                field:'VendorName',
                prompt:'Vendor',
                select: true
            },{
                field:'InvoiceNumber',
                prompt:'Receipt #',
                id: 'receiptInvoiceNumber'
            },{
                field:'InvoiceTypeCode',
                prompt:'Category',
                select: true,
            },{
                field:'InvoiceDate',
                prompt:'Date',
                id: 'receiptDate'
            }];
            for(var i=0; i<requireNotNull.length; i++){
                if(typeof $scope.receipt[requireNotNull[i].field] == 'undefined' || $scope.receipt[requireNotNull[i].field].toString().trim() == ''){
                    errorField(requireNotNull[i]);
                    return false;
                }
            }
            return true;
        }

        $scope.doneEditing = function(){
            if(!validateLocally()) return;
            $scope.receipt.StatusCode = $scope.receipt.billable?'RB':'NB';
            $scope.mapSP($scope.receipt, 'InvoiceStatus', 'StatusCode', 'Code');
            $scope.loading = true;
            var obj = _getPostData(function(obj, formdata){
                ApiService.post('receipts', obj, '', '', formdata).then(_saveResult,_saveResult);
            });
        }

        var closing = false;
        var closeReceipt = function(save){
            if(save){
              $scope.doneEditing();
              return;
            }
            mainNav.popPage();
          };
        $scope.closeReceipt = function(){
            if(!$scope.editing){
                closeReceipt();
                return;
            }
            if(closing || (mainNav.pages[mainNav.pages.length-1].pushedOptions.page != 'app/receipts/edit-receipt.html')) return;
            closing = true;
            Utils.notification.confirm("Save changes?", {
              callback: function(ok){
                if(ok == 2){
                  closing = false;
                  return;
                }
                closeReceipt(ok==0);
              },
              buttonLabels: ["Save","Discard","Cancel"]
          });
        }

        var statusClasses = {RB:'green',NB:'light',BI:'light-green'}
        $scope.getStatusClass = function(receipt){
            if(statusClasses[receipt.StatusCode]){
                return statusClasses[receipt.StatusCode];
            }
            return '';
        }

        var init = function(){
            Utils.focusInput('receiptInvoiceNumber')
            $scope.loaded = true;
        };

        load();
        $timeout(init);
        
         var toggleBackButton = function(on){
            if(on){
                ons.disableDeviceBackButtonHandler();
                document.addEventListener('backbutton', $scope.closeReceipt, false);
            }else{
                document.removeEventListener('backbutton', $scope.closeReceipt);
                ons.enableDeviceBackButtonHandler();
            }
          }

          $scope.hidePage = function(){
            toggleBackButton(false)
            Utils.clearBackButtonHandlers()
          }
          $scope.showPage = function(){
            toggleBackButton(true)
            Utils.setBackButtonHandlers(function(){
                toggleBackButton(false);
            }, function(){
                toggleBackButton(true);
            })

            ons.findComponent('#receipt-back-button')._element[0].onClick = $scope.closeReceipt;
          }

        $scope.$on('$destroy', function(){
            Smartpick.hide();
            Vendors.hide();
            UploadDialog.hide();
        });
    }]);

})();
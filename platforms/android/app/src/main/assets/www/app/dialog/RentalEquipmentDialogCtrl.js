(function(){
    'use strict';

    angular.module('app').controller('RentalEquipmentController', ['$scope', 'ApiService', '$timeout', '$rootScope', 'Utils', 'ImgOrPdf', 'urls', function($scope, ApiService, $timeout, $rootScope, Utils, ImgOrPdf, urls) {

        $scope.smartpicks = null;

        $scope.init = function(){
            $scope.filters = {
                show: false
            };
            $scope.loading = false;
            $scope.loadingSmartpicks = false;
            $scope.addingNew = false;
            $scope.searched = false;
            $scope.searches = {
                equipment: '',
                company: ''
            }
            $scope.form = {
                number: '',
                description: '',
                new: false,
                company: null,
                category: null,
                newCompany: ''
            };
            $scope.items = [];
        }
        $scope.reset = function(){
            $scope.smartpicks = null;
            $scope.init();
        }
        $scope.init();

        var loadSmartpicks = function(){
            return new Promise(function(resolve,reject){
                var obj = {
                    action: 'smartpicks',
                    id: $scope.orgId,
                    which: $scope.which
                }
                ApiService.get('rentalEquip', obj).then(smartpickResponse, smartpickResponse).then(resolve);
                $scope.loading = true;
                $scope.loadingSmartpicks = true;
            });
        }
        var smartpickResponse = function(resp){
            if(resp && (resp.Organization || resp.EquipmentCategory)){
                $scope.smartpicks = {
                    RentalCompany: [],
                    EquipmentCategory: []
                };
                if(resp.Organization){
                    $scope.smartpicks.RentalCompany = Array.isArray(resp.Organization)?resp.Organization:[resp.Organization];
                }
                if(resp.EquipmentCategory){
                    $scope.smartpicks.EquipmentCategory = Utils.enforceArray(resp.EquipmentCategory);
                    if(!$scope.smartpicks.EquipmentCategory.find(function(category){ return category.Category == 'Uncategorized'; })){
                        $scope.smartpicks.EquipmentCategory.unshift({
                            ID: 'uncat',
                            Category: 'Uncategorized',
                            Description: 'Uncategorized'
                        });
                    }
                    $scope.form.category = $scope.smartpicks.EquipmentCategory.find(function(category){ return category.Category == 'Uncategorized'; });
                }
            }
            $scope.loading = false;
            $scope.loadingSmartpicks = false;
        }

        $scope.executeSearch = function(){
            var obj = {
                action: 'search',
                equipment: $scope.searches.equipment,
                company: $scope.searches.company,
                id: $scope.id,
                org: $scope.orgId,
                which: $scope.which
            }
            if($scope.filters.category){
                obj.category = $scope.filters.category.ID;
            }
            ApiService.get('rentalEquip', obj).then(searchResponse, searchResponse);
            $scope.loading = true;
        }
        var searchResponse = function(resp){
            if(resp && resp.Results){
                $scope.items = Array.isArray(resp.Results)?resp.Results:[resp.Results];
            }else{
                $scope.items = [];
            }
            $scope.searched = true;
            $scope.loading = false;
        }

        var postNewRental = function(){
            var obj = {
                action: 'add',
                equipmentNumber: $scope.form.number,
                equipmentDescription: $scope.form.description,
                equipmentCategory: $scope.form.category.ID,
                companyId: $scope.form.new?'':$scope.form.company.ID,
                companyName: $scope.form.new?$scope.form.newCompany:'',
                id: $scope.id,
                which: $scope.which
            }
            if($scope.noSave){
                $scope.selectItem({
                    category: $scope.form.category,
                    company: {
                        ID: obj.companyId,
                        Name: obj.companyName
                    },
                    Description: $scope.form.description,
                    EquipmentNumber: $scope.form.number,
                    isNewCompany: $scope.form.new,
                    TypeCode: 'SR'
                });
            }else{
                ApiService.post('rentalEquip', obj).then(postNewRentalResult, postNewRentalResult);
                $scope.loading = true;
            }
        }
        var postNewRentalResult = function(resp){
            if(resp && resp.JobDayCrew && resp.JobDayCrew.Return){
                $scope.selectItem({ID: resp.JobDayCrew.Return});
            }
        }

        $scope.addNew = function(){
            if(!$scope.addingNew){
                $scope.addingNew = true;
                if(!$scope.smartpicks){
                    loadSmartpicks();
                }else{
                    $scope.form.category = $scope.smartpicks.EquipmentCategory.find(function(category){ return category.Category == 'Uncategorized'; });
                }
                return;
            }
            postNewRental();
        }
        $scope.cancelNew = function(){
            $scope.addingNew = false;
        }

        $scope.filterSmartpick = function(props, term){
            var regex = new RegExp(Utils.sanitizeTermForRegex(term), 'i');
            return function(item){
                for(var i=0; i<props.length; i++){
                    if(item[props[i]].match(regex))
                        return true;
                }
                return false;
            }
        }

        $scope.filterItems = function(){
            var filter = $scope.filter?$scope.filter():null;
            return function(item){
                return (!$scope.filters.category || $scope.filters.category.ID==item.CategoryID)&&(!filter||filter(item))
            }
        }

        $scope.toggleFilter = function(){
            if(!$scope.smartpicks){
                loadSmartpicks().then(function(){
                    $timeout(function(){
                        $scope.filters.show = true;
                    });
                });
            }else{
                $scope.filters.show = !$scope.filters.show;
            }
        }

    }]);

})();
(function(){
    'use strict';

    angular.module('app').service('SaveService', ['Utils', '$filter', function(Utils, $filter){

        var _jobData = null;
        var _utils = null;

        var _csvQuote = function(string){
            if(!string) return '';
            var quoted = string.toString();
            if(quoted.match(/[,"\n]/i)){
                quoted = quoted.replace(/"/ig,'""');
                return '"'+quoted+'"';
            }
            return quoted;
        }
        var _isTemp = function(id){
            return Utils.isTempId(id);
        }

        var _getValue = function(field, locationField){
            var value = locationField.FieldValue
            if(field.TypeCode=='CHECK'){
                value = Utils.bool(value);
            }else{
                if(['NUM','DOL'].includes(field.TypeCode) && (value || value.toString() == '0')){
                    value = $filter('number')(value, field.Decimals)
                    if(field.TypeCode == 'DOL'){
                        value = '$'+value
                    }
                }else{
                    value = Utils.null(value)
                }
            }
            return _csvQuote(value);
        }
        var _compareLocation = function(current, original){
            current.WorkDate = Utils.date(current.WorkDate, 'MM/dd/yyyy')

            if(!original || _isTemp(current.ID) || current.deleted){ return true; }
            if(current.WorkDate != Utils.date(original.WorkDate, 'MM/dd/yyyy') 
            || current.Address != original.Address
            || current.LocationComplete != original.LocationComplete){
                return true;
            }
            for(var f=0; f<current.LocationField.length; ++f){
                var field = current.LocationField[f]
                var originalField = original.LocationField.find(function(lf){ return lf.DispatchJobFieldID == field.DispatchJobFieldID })
                if(!originalField || field.FieldValue.toString() != originalField.FieldValue.toString()){
                    return true;
                }
            }
            for(var f=0; f<_jobData.DispatchField.length; ++f){
                var field = _jobData.DispatchField[f]
                if(current[field.key] != original[field.key]){
                    return true;
                }
            }
            return false;
        }
        var _getAddress = function(location){
            var obj = _utils.address(location);
            if(_isTemp(location.ID) && obj.Add){
                return _csvQuote(JSON.stringify(obj))
            }else{
                return _csvQuote(location.Address)
            }
        }
        var _getLocation = function(job, original, l){
            var me = job.DispatchLocation[l];
            var location = {
                data: ["DispatchLocation,"+me.ID],
                changed: _compareLocation(me, original.DispatchLocation.find(function(loc){ return loc.ID == me.ID}))
            }
            if(location.changed){
                if(me.deleted){
                    location.data[0] += ",DELETE"
                }else{
                    location.data[0] += ","+me.WorkDate+","+_getAddress(me)+","+Utils.bool(me.LocationComplete)+","+(_isTemp(me.ID) && me.LPhoto.length?'Y':'N')+","+Utils.bool(!me.touched)
                    for(var f=0; f<job.Field.length; ++f){
                        var field = job.Field[f]
                        var locationField = me.LocationField.find(function(lf){ return lf.DispatchJobFieldID == field.DispatchJobFieldID })
                        location.data[0] += ","+_getValue(field, locationField)
                    }
                    for(var f=0; f<job.DispatchField.length; ++f){
                        var field = job.DispatchField[f]
                        if(field.key != 'Address'){
                            location.data[0] += ","+_csvQuote(me[field.key])
                        }
                    }
                }
            }
            return location;
        }
        var _getPostData = function(job, original, utils){
            _utils = utils||{}
            _jobData = job
            var result = ["DispatchJob,"+job.ID];
            for(var l=job.DispatchLocation.length-1; l>=0; l--){
                var location = _getLocation(job, original, l);
                if(location.changed){
                    result.push(location.data.join('\n'));
                }
                if(job.DispatchLocation[l].deleted){
                    job.DispatchLocation.splice(l, 1)
                }
            }
            _jobData = null
            return result.join('\n');
        }

        return {
          getPostData: _getPostData
        };

    }]);


})();
